## Overview
### What is changing: 
<!-- Add a list describing the changes you have made in this MR -->

### What is the result of these changes:
<!-- Add a list describing the intended result of these changes -->

## Related ticket
 
<!-- Replace XXX with the ticket ID, e.g. 123 -->
 
- [XXX](https://jira.extge.co.uk/browse/XXX)
 
## Checklist
 
<!-- Replace [ ] with [x] for the items that apply -->
 
- [ ] The PR only includes changes relating to 1 ticket
- [ ] I have added labels to reflect change type
- [ ] I have checked all existing and new tests pass
- [ ] I have checked that linting passes
- [ ] I have added migration code (upgrade and downgrade) in `protocols/migration` folder
- [ ] I have added tests to cover my changes
- [ ] I have updated the documentation where required
- [ ] I have informed the team of any impactful changes
- [ ] I have run `make build__models__and__docs` and have included any html, catalog variable set, and python changes
- [ ] I have updated `protocols/util/dependency_manager.py` with a new gelreportmodel version build
- [ ] If my change is ready for a new build of gelreportmodels, I have updated `protocols/resources/builds.json`