ARG PYTHON_VERSION
ARG PYTHON_RELEASE
FROM docker.artifactory.aws.gel.ac/python:${PYTHON_VERSION}${PYTHON_RELEASE}

ENV PYTHONUNBUFFERED 1
ENV DEBIAN_FRONTEND noninteractive
ARG ALPINE_REPO_URL
RUN \
    export ALPINE_REPO_PATH=${ALPINE_REPO_URL}/alpine/v$(cut -d. -f1-2 /etc/alpine-release) && \
    echo ${ALPINE_REPO_URL} && \
    echo ${ALPINE_REPO_PATH} && \
    apk add --no-cache -X ${ALPINE_REPO_PATH}/main -X ${ALPINE_REPO_PATH}/community \
      bash openssh-client build-base unzip wget \
      openjdk8-jre \
      libsasl \
      libldap \
      openssl-dev \
      maven \
      curl \
      git;

RUN mkdir -p ~/.m2 && \
    mkdir -p /gel/GelReportModels
# # Manually update pip

COPY . /gel/GelReportModels
WORKDIR /gel/GelReportModels

RUN pip install -e ".[test,build_docs]";
