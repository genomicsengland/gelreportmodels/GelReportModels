CODE_PATHS := protocols

GITLAB_USERNAME              ?=
GITLAB_TOKEN                 ?=

PYTHON_VERSION               ?= 3.10## default python build to 3.10
PYTHON_RELEASE               ?= -alpine

# Originaly CI_PROJECT_NAME came from CI, but it is safer if this comes from setup.py
CI_PROJECT_NAME      	 	 := $(shell grep -i name= setup.py | awk -F '"' '{print $$2}' | tr "[A-Z]" "[a-z]")
CI_COMMIT_REF_NAME       	 ?= $(shell git symbolic-ref --short -q HEAD)
CI_COMMIT_REF_NAME       	 := $(subst /,-,$(CI_COMMIT_REF_NAME))
CI_COMMIT_BEFORE_SHA     	 := $(shell git rev-parse HEAD^1)
CI_COMMIT_SHA            	 ?= $(shell git rev-parse HEAD)
CI_COMMIT_SHORT_SHA      	 ?= $(shell git rev-parse --short HEAD)
CURRENT_LOCATION         	 ?= $(shell pwd)
CI_ENVIRONMENT_NAME      	 ?=
CI_DOCKER_BUILD_ARGS     	 ?=

PYPI_URL_GENOMICS            ?= https://artifactory.aws.gel.ac/artifactory/api/pypi/pypi/simple

PATH_CONTAINER_WORKDIR   	 := /protocols

CI_REGISTRY              	 ?= registry.gitlab.com
CI_REGISTRY_IMAGE    	 	 ?= ${CI_REGISTRY}/genomicsengland/gelreportmodels/gelreportmodels
ALPINE_REPO_URL				 ?= https://artifactory.aws.gel.ac/artifactory/dl-cdn.alpinelinux.org
GRM_DOCKER_IMAGE				 ?=  ${CI_REGISTRY_IMAGE}/gelreportmodels:py${PYTHON_VERSION}-${CI_COMMIT_REF_NAME}
GRM_DOCKER_IMAGE_MASTER			 ?=  ${CI_REGISTRY_IMAGE}/gelreportmodels:py${PYTHON_VERSION}-master
GRM_VERSION 				 ?= $(shell grep "VERSION =" setup.py | awk -F '"' '{print $$2}')
PYTHON_MAJOR_VERSION		 := $(shell echo ${PYTHON_VERSION} | cut -f 1 -d ".")

TEST_PARAMS ?=


.PHONY: build test

help:	## Prints this help/overview message
	@awk 'BEGIN {FS = ":.*?## "} /^[a-z0-9_-]+:.*?## / {printf "\033[36m%-17s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)


_CMD_DOCKER_BUILD := \
	DOCKER_BUILDKIT=1 docker build \
		${CI_DOCKER_BUILD_ARGS} \
		--build-arg BUILDKIT_INLINE_CACHE=1 \
		--build-arg PYTHON_VERSION=${PYTHON_VERSION} \
		--build-arg PYTHON_RELEASE=${PYTHON_RELEASE} \
		--build-arg ALPINE_REPO_URL=${ALPINE_REPO_URL} \
		--cache-from ${GRM_DOCKER_IMAGE} \
		--tag ${GRM_DOCKER_IMAGE} \
		--file Dockerfile

__DOCKER_RUN_BASE := \
	docker run \
		 -e GITLAB_CI=${GITLAB_CI} \
		 --volume $(shell pwd):/gel/GelReportModels:rw


_DOCKER_RUN := \
	${__DOCKER_RUN_BASE} ${GRM_DOCKER_IMAGE}

_DOCKER_RUN_IT := \
	${__DOCKER_RUN_BASE} -it ${GRM_DOCKER_IMAGE}

##############################################################################
############################ BASE LAYER ######################################
##############################################################################


build:  pull Dockerfile* ## build local python3 image. Define PYTHON_VERSION env variable as 2 or 3 to select
	${_CMD_DOCKER_BUILD} \
	.

push:
	docker push ${GRM_DOCKER_IMAGE}

pull:
	docker pull ${GRM_DOCKER_IMAGE} || docker pull ${GRM_DOCKER_IMAGE_MASTER} || echo "No premade image available"


##############################################################################
############################ TEST LAYER ######################################
##############################################################################

run__test: ## run pytest on mounted-in tests (change tests, no rebuild!)
	${_DOCKER_RUN} \
  	pytest tests ${TEST_PARAMS}

run__shell:  ## start an interactive shell for debugging.
	${_DOCKER_RUN_IT} \
		/bin/bash

##############################################################################
################### Build models in python code ##############################
##############################################################################
local__shell: 

	docker run -it \
		 --volume $(shell pwd):/gel/GelReportModels \
		 ${GRM_DOCKER_IMAGE} \
  		/bin/bash

build__models: ## builds all models that requires changes. builds.json saves a hash in that compares current schemas against
	${_DOCKER_RUN} \
  		python build.py 

build__docs: 
	${_DOCKER_RUN} \
  		python build.py --docs

build__rst: 
	${_DOCKER_RUN} \
  		python build.py --rst

build__latest__model: ## builds the python for the latest model
	${_DOCKER_RUN} \
  		python build.py --latest

build__latest__docs: 
	${_DOCKER_RUN} \
  		python build.py --latest --docs --skip-python

build__specific__model: ## builds the python for model specified by GRM_VERSION
	${_DOCKER_RUN} \
  		python build.py --version ${GRM_VERSION}

build__specific__docs: 
	${_DOCKER_RUN} \
  		python build.py --version ${GRM_VERSION} --docs --skip-python

idl2json: 
	${_DOCKER_RUN} \
		python build.py  --skip-python

json2python: 
	${_DOCKER_RUN} \
  		python build.py

json2java: 
	${_DOCKER_RUN} \
  		python build.py --latest --skip-python

update__version:
	${_DOCKER_RUN} \
  		python build.py --update-version

