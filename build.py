#!/usr/bin/env python
from __future__ import print_function

import argparse
import distutils.dir_util
import json
import logging
import os
import re
import shutil
import subprocess
import sys

import dictdiffer

from protocols.resources import RESOURCE_DIR
from protocols.util.avro_schema import AvroSchemaFile
from protocols.util.dependency_manager import DependencyManager
from protocols_utils.utils.conversion_tools import ConversionTools
from protocols_utils.utils.makedir import makedir

RE_NAMESPACE = re.compile('namespace\("(.*)"\)')

logging.basicConfig(level=logging.DEBUG, filename="build.log")

IDL_FOLDER = "schemas/IDLs"
CURRENT_MODEL_VERSION_FILE = os.path.join(IDL_FOLDER, "current_versions.json")
PYTHON_FOLDER = "protocols"
JSON_FOLDER = os.path.join(RESOURCE_DIR, "schemas")
AVPR_FOLDER = "schemas/AVPRs"
JAVA_FOLDER = "target/generated-sources/java"
OPENCGA_CATALOG_FOLDER = "protocols/catalog_variable_set"
DOCS_FOLDER = "docs"
DOCS_SOURCE_FOLDER = os.path.join(DOCS_FOLDER, "source")
DOCS_SCHEMA_FOLDER = os.path.join(DOCS_SOURCE_FOLDER, "schemas")
DOCS_MODEL_FILE = os.path.join(DOCS_SOURCE_FOLDER, "models.rst")
BUILDS_FILE = os.path.join(RESOURCE_DIR, "builds.json")
POM_FILE = "pom.xml"
VERSION_FILE = "VERSION"
DOC_VERSION_FILE = "docs/source/conf.py"


def get_IDLs_build_folder(build):
    """
    Creates the build folder for a given set of packages
    :param packages:
    :return:
    """

    return os.path.join(IDL_FOLDER, "build", build["version"])


def delete_folder(path, ignorepaths=[]):
    """Deletes given folder

    :param path: Path to item to delete
    :type path: str
    """
    if os.path.exists(path):
        distutils.dir_util.remove_tree(path)


def get_hash_for_idls(packages):
    data = []
    for package in sorted(packages, key=lambda x: x["python_package"]):
        source_folder = str(
            os.path.join(IDL_FOLDER, package["package"], package["version"])
        )
        data.append(
            source_folder + "=" + ConversionTools.get_md5_of_path(source_folder)
        )

    return ConversionTools.get_md5("\n".join(data))


def copy_idls_to_build_folder(build_folder, packages):
    makedir(build_folder)
    for package in packages:
        source_folder = os.path.join(IDL_FOLDER, package["package"], package["version"])
        logging.info("Copying '{}'...".format(source_folder))
        distutils.dir_util.copy_tree(source_folder, build_folder)

    return build_folder


def get_dependencies(build, packages):
    """Returns list of dependencies for the given package from the provided build.

    :param build: Json collection of package information
    :type build:
    :param package: Json data of packge
    :type package: dict[str, Any]
    :return: List of dependencies for package
    :rtype: list[dict[str, Any] ]
    """
    for package in list(packages.values()):
        for dep in package["dependencies"]:
            if dep in packages:
                continue
            packages[dep] = get_package_from_build(build, dep)
            packages.update(get_dependencies(build, packages))

    return packages


def get_package_from_build(build, package_name):
    """
    Finds a package in a build by package name
    :param build:
    :param package:
    :return:
    """
    for package in build["packages"]:
        if package["package"] == package_name:
            return package
    return None


def get_build_by_version(builds, version):
    """Extracts the build from builds with the given version

    :param builds: List of build data
    :type builds: list[dict[str, Any]]
    :param version: Version of build
    :type version: str
    :return: Build data
    :rtype: dict[str, Any] | None
    """
    for build in builds:
        if build["version"] == version:
            return build
    return None


def update_documentation_index(builds):
    redirects = {}

    for build in builds:
        build_version = build["version"]

        package_title = "Protocols Version v{}".format(build_version)
        package_content = [
            package_title,
            "{}\n".format("=" * len(package_title)),
            ".. toctree::",
            "    :maxdepth: 6\n",
        ]

        for model in build["packages"]:
            model_folder = "{}-{}".format(model["python_package"], model["version"])

            if os.path.isfile(
                os.path.join(DOCS_SCHEMA_FOLDER, build_version, model_folder)
            ):
                continue

            # ensure links to old docs redirect
            redirects[
                "{}/{}".format(model["package"], model["version"])
            ] = "{}/{}-{}/".format(
                build_version, model["python_package"], model["version"]
            )

            package_content.append("    {}/index".format(model_folder))

            namespace_title = "{} v{}".format(model["python_package"], model["version"])
            namespace_content = [
                namespace_title,
                "{}\n".format("=" * len(namespace_title)),
                ".. toctree::",
                "    :maxdepth: 6\n",
            ]

            for rst in sorted(
                os.listdir(
                    os.path.join(DOCS_SCHEMA_FOLDER, build_version, model_folder)
                )
            ):
                namespace_content.append("    {}".format(rst.split(".")[0]))

            with open(
                os.path.join(
                    DOCS_SCHEMA_FOLDER, build_version, model_folder, "index.rst"
                ),
                "w",
            ) as namespace_index:
                namespace_index.write("\n".join(namespace_content))

        with open(
            os.path.join(DOCS_SCHEMA_FOLDER, build_version, "index.rst"), "w"
        ) as package_index:
            package_index.write("\n".join(package_content))

    prefix = "gelreportmodels/GelReportModels"

    with open(os.path.join(DOCS_SCHEMA_FOLDER, "_redirects"), "w") as redirect_file:
        for path_from, path_to in redirects.items():
            redirect_file.write(
                "/{0}/html_schemas/{1}/* /{0}/schemas/{2} 301".format(
                    prefix, path_from, path_to
                )
            )
            redirect_file.write("\n")

    with open(DOCS_MODEL_FILE, "w") as schema_index:
        schema_index.write(
            "Models Documentation\n"
            "--------------------\n\n"
            ".. toctree::\n"
            "    :maxdepth: 2\n\n",
        )
        for build in builds:
            schema_index.write("    schemas/{}/index".format(build["version"]))
            schema_index.write("\n")


def create_catalog_variable_sets(package, release_version, files):
    variable_sets = package["catalog_variable_sets"]
    target_folder = os.path.join(
        OPENCGA_CATALOG_FOLDER,
        "v" + release_version,
        package["package"] + "_" + package["version"],
    )
    makedir(target_folder)
    for variable_set in variable_sets:
        logging.info("OpenCGA Catalog variable set generation for: %s", variable_set)

        assert variable_set in files, variable_set

        avro_file = AvroSchemaFile(files.get(variable_set), files)
        target_file = os.path.join(target_folder, variable_set + ".catalog_vs.json")
        with open(target_file, "w") as f:
            json.dump(avro_file.convert_variable_set(avro_file.data), f, indent=2)


def skip_tree_if_not_in_ci(path):
    if os.environ.get("GITLAB_CI"):
        logging.info("Updating index for {}".format(path))
        return

    ConversionTools.run_command(
        "git ls-files --modified -- {} | xargs git update-index --assume-unchanged".format(
            path
        )
    )


def run_build(
    build, build_docs=False, build_java=False, build_python=True, ignore_cache=False
):
    """
    Builds a build ...
    :param build:
    :return:
    """

    build_version = build["version"]
    build_version_tuple = DependencyManager.process_version(build["version"])
    build_packages = build["packages"]
    build_folder = get_IDLs_build_folder(build)

    protocol_name = DependencyManager.get_python_protocol_name(build)

    protocol_folder = os.path.join(PYTHON_FOLDER, protocol_name)
    doc_folder = os.path.join(DOCS_SCHEMA_FOLDER, build["version"])
    java_build_folder = os.path.join(JAVA_FOLDER)

    logging.info("Building build version {}".format(build["version"]))
    save_hash = False

    # generate protocol_utls hash to be used to force update
    # if the code generation files are updated

    if build_python:
        delete_folder(protocol_folder)
        delete_folder(os.path.join(OPENCGA_CATALOG_FOLDER, "v" + build_version))

    if build_docs:
        delete_folder(doc_folder)

    if build_java:
        delete_folder(java_build_folder)

    for package in build_packages:
        logging.info(package["package"])
        logging.info("-" * len(package["package"]))

        package_dependencies = json.loads(
            json.dumps(
                list(get_dependencies(build, {package["package"]: package}).values())
            )
        )

        # define folders
        idl_folder = os.path.join(build_folder, package["package"])

        python_folder = os.path.join(protocol_folder, package["python_package"])
        json_folder = os.path.join(
            JSON_FOLDER, protocol_name, package["python_package"]
        )

        for dependency in package_dependencies:
            # add build version to dependency
            dependency["build_version"] = build_version

            # determine all namespaces in for dependency
            dependency["namespaces"] = []
            source_folder = os.path.join(
                IDL_FOLDER, dependency["package"], dependency["version"]
            )
            for file in os.listdir(source_folder):
                if os.path.isfile(file) or not file.endswith(".avdl"):
                    continue
                with open(os.path.join(source_folder, file)) as f:
                    dependency["namespaces"].extend(
                        [m for m in RE_NAMESPACE.findall(f.read())]
                    )

        namespace_dict = {ns: p for p in package_dependencies for ns in p["namespaces"]}

        # gernerate idl hashes
        source_hash = get_hash_for_idls(package_dependencies)

        json_hash = ConversionTools.get_md5(
            source_hash + ConversionTools.get_md5_of_path(json_folder)
        )

        regenerate_json = json_hash != package.get("json_hash") or ignore_cache

        # delete folders

        if regenerate_json:
            delete_folder(idl_folder)
            copy_idls_to_build_folder(
                idl_folder,
                package_dependencies,
            )

            save_hash = True

            delete_folder(json_folder)
            ConversionTools.idl2json(
                idl_folder, json_folder, use_pool=build_version_tuple[0] >= 7
            )

            package["json_hash"] = ConversionTools.get_md5(
                source_hash + ConversionTools.get_md5_of_path(json_folder)
            )
            save_hash = True

            json_hash = None

            skip_tree_if_not_in_ci(json_folder)

        if build_java:
            # generate Java source code

            java_folder = os.path.join(JAVA_FOLDER)

            ConversionTools.json2java(json_folder, java_folder)

        if build_python:
            # load the avsc files
            avsc_files = {}
            for schema_name in os.listdir(json_folder):
                schema_path = os.path.join(json_folder, schema_name)
                if os.path.isdir(schema_path) or not schema_name.endswith(".avsc"):
                    continue
                with open(schema_path) as f:
                    schema = json.load(f)
                avsc_files[schema["name"]] = schema_path

            if package.get("catalog_variable_sets"):
                create_catalog_variable_sets(package, build_version, avsc_files)
            ConversionTools.json2python(
                json_folder,
                protocol_folder,
                package_dependencies[0],
                namespace_dict,
            )

        if build_docs:
            ConversionTools.json2rst(
                json_folder,
                doc_folder,
                package_dependencies[0],
                namespace_dict,
            )

        logging.info("{} {}".format(package["python_package"], package["version"]))
        logging.info(
            " - JSON hash {} {}".format(
                package.get("json_hash"),
                "Regenerated" if regenerate_json else "Cached",
            )
        )
    skip_tree_if_not_in_ci(OPENCGA_CATALOG_FOLDER)
    skip_tree_if_not_in_ci(python_folder)

    return save_hash


def check_consistency_with_last_release(builds):
    # get last tag
    last_tag = get_latest_tag_on_master()
    logging.info("Last version {}".format(last_tag))
    old_build_file = get_build_file_from_tag(last_tag)

    # assert that any builds in the old file are present have not been changed in the current one
    old_builds = {
        b["version"]: {p["package"]: p["version"] for p in b["packages"]}
        for b in old_build_file.get("builds", [])
    }

    with open(CURRENT_MODEL_VERSION_FILE) as f:
        current_model_versions = json.load(f)

    # update current build to match current_model_versions.json
    for build in builds:
        if build["version"] in old_builds:
            continue
        for package in build["packages"]:
            namespace = package["package"]

            if namespace not in current_model_versions:
                # don't do anything for external models or
                # if current version does not match the version specified in the build
                continue

            package["version"] = current_model_versions.get(namespace)

    # check model versions have been used
    for namespace, version in current_model_versions.items():
        for package in builds[0]["packages"]:
            if package["package"] == namespace and version != package["version"]:
                raise AssertionError(
                    "Latest build in 'protocols/resources/builds.json' does not use the latest version of {}.".format(
                        package["package"]
                    )
                )
    current_builds = {
        b["version"]: {p["package"]: p["version"] for p in b["packages"]}
        for b in builds
    }

    consistent_builds = True
    for version, packages in old_builds.items():
        current_packages = current_builds.get(version, {})
        differences = list(dictdiffer.diff(packages, current_packages))
        for difference in differences:
            if difference[0] in ["add", "remove"]:
                logging.error(
                    "Python Build {} has had {} version {} has been {}.".format(
                        version,
                        difference[2][0][0],
                        difference[2][0][1],
                        {"add": "added", "remove": "removed"}.get(difference[0]),
                    )
                )
            else:
                logging.error(
                    "Python Build {} has had {} changed from {} to {}".format(
                        version, difference[1][0], difference[2][0], difference[2][1]
                    )
                )

            consistent_builds = False
    if not consistent_builds:
        raise AssertionError(
            "Current build file is not consistent with previous release. Define a new build in 'protocols/resources/build.json'."
        )

    diff = get_idl_diff_against_tag(last_tag)

    consistent_idls = True
    for _, packages in old_builds.items():
        for namespace, version in packages.items():
            # check the namespace and version don't exist in the diff
            model_path = os.path.join(IDL_FOLDER, namespace, version)

            affected = [
                d for d in diff if d.startswith(model_path) and "/avro" not in d
            ]

            if current_model_versions.get(namespace) == version:
                current_model_diff = [
                    d
                    for d in diff
                    if d.startswith(os.path.join(IDL_FOLDER, namespace, "current"))
                    and "/avro" not in d
                ]

                for current_idl in current_model_diff:
                    # check for differences against last released model

                    with open(current_idl) as f:
                        current_contents = f.read()
                    with open(
                        os.path.join(model_path, os.path.basename(current_idl))
                    ) as f:
                        last_released_contents = f.read()
                    if current_contents != last_released_contents:
                        affected.append(current_idl)

            # remove checked model from diff so that the version is only logged once
            diff = [d for d in diff if not d.startswith(model_path)]

            if len(affected) > 0:
                for file in affected:
                    logging.error("Unexpected changes to: {}".format(file))

                consistent_idls = False

    if not consistent_idls:
        raise AssertionError(
            "Existing IDL files are not consistent with previous release. Make changes in the './schemas/IDL/<namespace>/current' folder and then increase version in './schemas/IDL/current_versions.json'."
        )

    # check model versions are higher or equal

    for _, packages in old_builds.items():
        for namespace, past_version in packages.items():
            if namespace not in current_model_versions:
                continue
            past_version_tuple = DependencyManager.process_version(past_version)
            current_version = current_model_versions.get(namespace)
            current_version_tuple = DependencyManager.process_version(current_version)
            assert (
                current_version_tuple >= past_version_tuple
            ), "{} has a version '{}' but this needs to be equal or higher than '{}'".format(
                namespace, current_version, past_version
            )

    # copy current to any active build that has not been released.
    for build_version, packages in current_builds.items():
        if build_version in old_builds:
            continue

        for namespace, version in packages.items():
            if namespace not in current_model_versions:
                # don't do anything for external models or
                # if current version does not match the version specified in the build
                continue

            model_path = os.path.join(IDL_FOLDER, namespace, version)

            # check any version file
            current_folder = os.path.join(IDL_FOLDER, namespace, "current")
            for file in os.listdir(current_folder):
                if "version" in file.lower() and file.endswith(".avdl"):
                    current_avdl = os.path.join(current_folder, file)
                    with open(current_avdl) as handle:
                        contents = handle.read()
                    assert (
                        '"' + version + '"' in contents
                    ), """Please update the version in {} to match "{}" (expected string enclosed with double quotes).""".format(
                        current_avdl, version
                    )
            # remove current folder for package
            delete_folder(model_path)

            # and recopy
            copy_idls_to_build_folder(
                model_path, [{"package": namespace, "version": "current"}]
            )
            skip_tree_if_not_in_ci(model_path)

    # check any new builds against last tag
    new_versions = [
        DependencyManager.process_version(b)
        for b in current_builds
        if b not in old_builds
    ]

    # changes in models should be at least a minor change
    current_release_verison = DependencyManager.process_version(last_tag)

    for version in new_versions:
        assert (
            "{}.{}".format(*version[0:2]) in current_builds
        ), "Patch version should not be included in build version."
        assert (
            version > current_release_verison
        ), "New version should be higher than the current release."

    return current_release_verison


def get_latest_tag_on_master():
    """Get the latest tag for master

    :return: tag
    :rtype: str
    """
    return (
        subprocess.check_output("git tag -l --sort=-version:refname", shell=True)
        .decode()
        .strip()
    ).splitlines()[0]


def get_build_file_from_tag(tag):
    lastbuild = subprocess.check_output(
        "git show {}:protocols/resources/builds.json".format(tag), shell=True
    ).decode()

    if lastbuild:
        return json.loads(lastbuild)
    else:
        return {"build": []}


def get_idl_diff_against_tag(tag):
    changes = [
        d.strip().split(" ", 1)[-1].strip()
        for d in (
            # and any committed files diff against  tag
            subprocess.check_output(
                "git diff --name-only {} HEAD".format(tag), shell=True
            )
            .decode()
            .splitlines()
            # and any uncommitted files
            + subprocess.check_output("git status --short", shell=True)
            .decode()
            .splitlines()
        )
    ]  # nosec
    # return only changes to IDLs

    return [c for c in changes if c.startswith(IDL_FOLDER)]


def update_version(build, current):
    """Updates the python package version in the appropriate files

    :param build: Latest build
    :type build: dict[str, Any]
    :param current: Current release tag
    :type current: tuple(int,int,int)
    :return: New version
    :rtype: str
    """
    version = DependencyManager.process_version(build["version"])
    print(version)
    if version[0:2] > current[0:2]:
        new = "{}.{}.0".format(version[0], version[1])
    else:
        new = "{}.{}.{}".format(version[0], version[1], current[2] + 1)
    with open(VERSION_FILE, "w") as f:
        f.write(new)

    with open(DOC_VERSION_FILE) as f:
        contents = f.read()

    contents = re.sub(
        r"version = .+",
        'version = "Latest"',
        contents,
    )
    contents = re.sub(r"release = .+", 'release = "Latest"', contents)
    with open(DOC_VERSION_FILE, "w") as f:
        f.write(contents)

    # now update pom for Java
    update_pom(build, new)


def update_pom(build, python_package_version):
    """Updates the pom.xml in the root of the repository

    :param build: Build data
    :type build: dict[str, Any]
    """

    package_dict = {p["package"]: p["version"] for p in build["packages"]}

    with open(POM_FILE) as f:
        contents = f.read()

    dependency_xml = re.findall(
        r"\<dependencies\>((\n|.)*)\<\/dependencies\>",
        contents,
        re.MULTILINE,
    )[0][0]
    build_xml = re.findall(
        r"\<build\>((\n|.)*)\<\/build\>",
        contents,
        re.MULTILINE,
    )[
        0
    ][0]

    contents = re.sub(
        r"\<dependencies\>((\n|.)*)\<\/dependencies\>",
        "<dependencies></dependencies>",
        contents,
        re.MULTILINE,
    )
    contents = re.sub(
        r"\<build\>((\n|.)*)\<\/build\>",
        "<build></build>",
        contents,
        re.MULTILINE,
    )
    # replace package version

    contents = re.sub(
        r"\<version\>.*\<\/version\>",
        "<version>{}</version>".format(python_package_version),
        contents,
    )
    contents = re.sub(
        r"\<package.version\>.*\<\/package.version\>",
        "<package.version>{}</package.version>".format(python_package_version),
        contents,
    )

    # update model version
    # this expects properties in pom.xml to match
    # prefix.namespace
    # prefix.version
    for match in re.finditer(
        r"\<(?P<prefix>.+).namespace\>(?P<namespace>org\..+)<\/\1.namespace\>",
        contents,
    ):
        match_dict = match.groupdict()
        for package, version in package_dict.items():
            if package.startswith(match_dict["namespace"]):
                contents = re.sub(
                    "\<{0}.version\>.*\<\/{0}.version\>".format(match_dict["prefix"]),
                    "<{0}.version>{1}</{0}.version>".format(
                        match_dict["prefix"], version
                    ),
                    contents,
                )

    contents = re.sub(
        r"\<dependencies\>\<\/dependencies\>",
        "<dependencies>{}</dependencies>".format(dependency_xml),
        contents,
        re.MULTILINE,
    )
    contents = re.sub(
        r"\<build\>\<\/build\>",
        "<build>{}</build>".format(build_xml),
        contents,
        re.MULTILINE,
    )
    with open(POM_FILE, "w") as f:
        f.write(contents)


def ensure_tidy_package(builds):
    """Ensures that there are no extra protocol folders for non-existent builds.

    :param builds: List of build information
    :type builds: list[dict[str, Any]]
    :rtype: None
    """

    expected_folders = [DependencyManager.get_python_protocol_name(b) for b in builds]
    for root in [PYTHON_FOLDER, JSON_FOLDER]:
        for name in os.listdir(root):
            path = os.path.join(root, name)
            if (
                os.path.isdir(path)
                and name.startswith("protocol_")
                and name not in expected_folders
            ):
                delete_folder(path)


def main():
    parser = argparse.ArgumentParser(
        description="GEL models build", usage="""build.py [<args>]"""
    )
    parser.add_argument(
        "--version", help="A specific build version to run (if not provided runs all)"
    )
    parser.add_argument(
        "--latest",
        help="Builds the latest version of the models",
        action="store_true",
    )
    parser.add_argument(
        "--skip-python",
        action="store_true",
        help="Skip the generation of python source code",
    )
    parser.add_argument(
        "--skip-docs",
        action="store_true",
        help="DEPRECATED: Skipped by default. Use --docs to build.",
    )
    parser.add_argument("--rst", action="store_true", help="Build rst documentation.")
    parser.add_argument("--docs", action="store_true", help="Build documentation.")
    parser.add_argument(
        "--skip-java",
        action="store_true",
        help="DEPRECATED: Skipped by default. Use --java to build.",
    )
    parser.add_argument(
        "--java",
        action="store_true",
        help="Builds java source code",
    )
    parser.add_argument(
        "--update-version",
        action="store_true",
        help="Updates the versions in appropriate places.",
    )

    parser.add_argument(
        "--tidy",
        action="store_true",
        help="Updates the versions in appropriate places.",
    )

    parser.add_argument(
        "--ignore-cache",
        action="store_true",
        help="Ignores build cache.",
    )

    args = parser.parse_args()

    ran_latest = False
    with open(BUILDS_FILE) as f:
        builds = json.load(f)["builds"]

    if args.docs or args.rst:
        delete_folder(DOCS_SCHEMA_FOLDER)
        makedir(DOCS_SCHEMA_FOLDER)

    # order builds to ensure latests model is built last. PRD-1845
    builds = sorted(
        builds,
        key=lambda x: DependencyManager.process_version(x["version"]),
        reverse=True,
    )

    last_released_version = check_consistency_with_last_release(builds)

    save_hash = False
    ensure_tidy_package(builds)
    update_version(builds[0], last_released_version)
    if args.tidy:
        return
    elif sys.version_info.major < 3:
        raise RuntimeError("Python 2 not supported.")
    elif args.latest or args.java:
        save_hash = run_build(
            builds[0],
            args.docs or args.rst,
            args.java,
            not args.skip_python,
            args.ignore_cache,
        )
        ran_latest = True
    elif args.version:
        build = get_build_by_version(builds, args.version)
        if build is None:
            build = get_build_by_version(
                builds, DependencyManager.remove_hotfix_version(args.version)
            )
            if build is None:
                raise ValueError(
                    "Build version '{}' does not exist".format(args.version)
                )
        save_hash = run_build(
            build,
            args.docs or args.rst,
            args.java,
            not args.skip_python,
            args.ignore_cache,
        )

    else:
        for build in builds:
            save_hash = (
                run_build(
                    build,
                    args.docs or args.rst,
                    args.java,
                    not args.skip_python,
                    args.ignore_cache,
                )
                or save_hash
            )

        ran_latest = True

    if save_hash:
        with open(BUILDS_FILE, "w") as f:
            json.dump({"builds": builds}, f, indent=4)

    if ran_latest:
        # ensure the modules in the root of the package point to the latest collection
        ConversionTools.update_python_linkers(
            PYTHON_FOLDER,
            builds[0],
            DependencyManager.get_python_protocol_name(builds[0]),
        )

    if args.docs or args.rst:
        update_documentation_index(builds)

    if args.docs:
        ConversionTools.sphinx_build(DOCS_FOLDER)

    logging.info("Build/s finished succesfully!")


if __name__ == "__main__":
    main()
