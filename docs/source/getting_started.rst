Getting started
===============

Serialisation to/from JSON
^^^^^^^^^^^^^^^^^^^^^^^^^^

Given that you hold your data in JSON format in a string in memory deserialise as follows:

::

    from protocols.protocol_4_0_0.reports import CancerInterpretationRequest as CancerInterpretationRequest_4_0_0
    instance = CancerInterpretationRequest_4_0_0.fromJsonString(json_string)
    # now you can access the data in the instance
    print("This interpretation request has {} variants".format(instance.tieredVariants))

If your data is hold in a Python dictionary in memory deserialise as follows:

::

    instance = CancerInterpretationRequest_4_0_0.fromJsonDict(json_dict)

The object can be serialised into JSON as follows:

::

    instance.toJsonDict()
    instance.toJsonString()


Serialisation to/from AVRO
^^^^^^^^^^^^^^^^^^^^^^^^^^

TODO

Schema validation
^^^^^^^^^^^^^^^^^

Validate any instance against the corresponding schema. This validation will ensure that all non-nullable fields are actually filled.
::

    # just Avro native validation
    is_valid = instance.validate(instance.toJsonDict())

    # verbose validation
    validation_object = instance.validate(instance.toJsonDict(), verbose=True)
    is_valid = validation_object.result
    if not is_valid
        print validation_object.messages


Mock data
^^^^^^^^^

Generate a mocked object or a batch of objects with custom fields as follows:
::

    from protocols.util.factories.avro_factory import GenericFactoryAvro

    # gets a default factory of InterpretationRequestRD for reports package 5.0.0 included in build 6.0.0
    factory = GenericFactoryAvro.get_factory_avro(
        protocols.protocol_6_0_0.reports.InterpretationRequestRD,
        version = '6.0.0'
    )
    # creates one mocked instance
    instance = factory.create()
    # creates a batch of mocked instances
    instances = factory.create_batch(5)
    # creates an instance with one custom value
    instance_with_uri = factory.create(analysisReturnUri = "myURI")

Get a factory that fills all nullable fields (nullable fields are not filled by default)
::

    factory = GenericFactoryAvro.get_factory_avro(
        protocols.protocol_6_0_0.reports.CancerInterpretedGenome,
        version = '6.0.0',
        fill_nullables=True
    )

Generate custom factories as follows
::

    from protocols.ga4gh_3_0_0 import Variant
    from protocols.util.factories.avro_factory import FactoryAvro


    class GA4GHVariantFactory(FactoryAvro):
        def __init__(self, *args, **kwargs):
            super(GA4GHVariantFactory, self).__init__(*args, **kwargs)

        class Meta:
            model = Variant

        _version = '6.0.0'

        start = factory.fuzzy.FuzzyInteger(1, 10000000)
        referenceBases = factory.fuzzy.FuzzyChoice(['A', 'T', 'C', 'G'])
        alternateBases = factory.LazyAttribute(lambda x: [factory.fuzzy.FuzzyChoice(['A', 'T', 'C', 'G']).fuzz()])
        qual = factory.fuzzy.FuzzyInteger(1, 600)
        referenceName = factory.fuzzy.FuzzyChoice(list(map(str, range(1, 23)) + ['X', 'Y', 'MT']))

    factory = GA4GHVariantFactory
    variant = factory()
    variants = factory.create_batch(3)


Migrations
^^^^^^^^^^

There a number of migration scripts in the package `protocols.migration`. They can be used as follows:

::

    from protocols.protocol_4_0_0.reports import CancerInterpretationRequest as CancerInterpretationRequest_4_0_0
    from protocols.protocol_6_1.reports import CancerInterpretationRequest as CancerInterpretationRequest_5_0_0
    from protocols.util.dependency_manager import VERSION_400
    from protocols.migration.migration_reports_4_0_0_to_reports_5_0_0 import MigrateReports400To500

    old_instance = GenericFactoryAvro.get_factory_avro(CancerInterpretationRequest_4_0_0, VERSION_400).create()
    old_instance.validate(old_instance.toJsonDict())

    migrated_instance = MigrateReports400To500().migrate_cancer_interpretation_request(
        old_instance=old_instance, assembly='GRCh38'
    )
    migrated_instance.validate(migrated_instance.toJsonDict())

