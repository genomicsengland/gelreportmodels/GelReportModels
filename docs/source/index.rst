.. g documentation master file, created by
   sphinx-quickstart on Thu Sep 22 17:00:21 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

GEL Models Documentation
========================

.. toctree::
    :maxdepth: 6

    self
    introduction
    installation
    getting_started
    models

