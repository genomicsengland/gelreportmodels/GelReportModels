Installation
============

Install built version from PyPIP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Install from pip:
::

    pip install -i $PIP_SERVER GelReportModels


Or build locally with gelreportmodels
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Install ``gelreportmodels requirements``
::

    pip install .
    pip install "."


Build the models
^^^^^^^^^^^^^^^^

To build all builds described in ``builds.json`` run:
::

    python build.py

This will create the following:
* Python source code representing the Avro records in the folder ``./protocols/models``
* Java source code representing the Avro records in the folder ``./target/generated-sources/avro``
* The models HTML documentation under ``./docs/dist/``

NOTE: Java source code is only generated for the last build version, while Python source code is generated for all versions

Building legacy versions of the models
""""""""""""""""""""""""""""""""""""""

See ``builds.json`` for the information on all legacy versions and the specific package versions and dependencies in each of those.

To build a specific version run:
::

    python build.py --version 3.0.0

Other build options
"""""""""""""""""""

Use ``--docs`` to generate documentation which affects build time greatly.

Use ``--java`` to generate Java source code.

Use ``--skip-python`` to avoid generating Python source code.

Use ``--latest`` to build latest model.

Java Packaging
^^^^^^^^^^^^^^

To pack the Java source code representing these models in a jar file use:
::

    % mvn package

To install it in your system so it is accessible as a maven dependency to other Java applications run:
::

    % mvn install

Unit tests
^^^^^^^^^^

To run some unit tests implemented in Python run:
::

    % ./pytest tests


Building Resources From a Container
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

From your starting directory, eg. ~/gel/:

Clone this repo
```
git@gitlab.com:genomicsengland/gelreportmodels/GelReportModels.git
```

Then run the following (you may need `sudo` depending on your system configuration):

```
make build__models
```

Running tests in a container
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Run the command: 
```
make run__test
```