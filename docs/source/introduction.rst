Introduction
============

GEL models project contains the data models that aim at standardizing data exchanges with Genomics England systems either internally or externally. This enables our data exchanges to be programming language and technology-agnostic.

Two illustrative examples:

* The CIPAPI exchanges data with the CIPs based on these models. This allows the CIPAPI to be completely independent of the specific implementations in the CIPs.
* The CIPAPI pushes data to CVA. While the CIPAPI is implemented as a Django application in Python, CVA is a Java application deployed in Tomcat.

The above exchanges are based on a REST interface, but any other technology could be used.

We use Avro to describe our models.

Packages
^^^^^^^^

The models are organised in packages:

* Participants: Demographic and Clinical Information
* Reports: Interpretation Request, Interpreted Genome, Clinical Reports
* Metrics: errr…. Metrics, yes
* Cva: Reported Variants and its Evidences
* Ga4gh: Variants and Alignments. Originally external from a now depricated ga4gh model, but extended.
* OpenCB: External(ish). Variants and Variant Annotation (and recently also evidences)

Main functionalities
^^^^^^^^^^^^^^^^^^^^

* Automatic code generation to hold the data (Python and Java supported out-of-the-box; others as C, C++ and C# are supported by ad hoc avro libraries).
* Enable data validation against a schema (see the validation service `https://github.com/genomicsengland/data-validation <https://github.com/genomicsengland/data-validation>` for additional validation business rules)
* Data serialisation/deserialisation to/from JSON
* Generation of mocked data
* Documentation automatically generated from the models

Resources
^^^^^^^^^

* Gitlab repository: `https://gitlab.com/genomicsengland/GelReportModels <https://gitlab.com/genomicsengland/GelReportModels>`
* Models documentation: `https://gelreportmodels.genomicsengland.co.uk <https://gelreportmodels.genomicsengland.co.uk>`
