Models Documentation
--------------------

.. toctree::
    :maxdepth: 2

    schemas/9.0/index
    schemas/8.9/index
    schemas/8.8/index
    schemas/8.7/index
    schemas/8.6/index
    schemas/8.5/index
    schemas/8.4/index
    schemas/8.3/index
    schemas/8.2/index
    schemas/8.1/index
    schemas/8.0/index
    schemas/7.13/index
    schemas/7.12/index
    schemas/7.11/index
    schemas/7.10/index
    schemas/7.9/index
    schemas/7.8/index
    schemas/7.7/index
    schemas/7.6/index
    schemas/7.4/index
    schemas/7.3/index
    schemas/7.2.1/index
    schemas/7.2/index
    schemas/7.0/index
    schemas/6.1/index
    schemas/5.0.0/index
    schemas/4.1/index
    schemas/4.0.0/index
    schemas/3.0.0/index
    schemas/2.1.0/index
    schemas/1.2.0-opencb/index
    schemas/1.0.0-metrics/index
