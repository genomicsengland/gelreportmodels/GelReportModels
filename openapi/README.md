# OpenAPI for GRM

This folder contains a utility which converts the GelReportModels AVDL files into valid
Json Schema files that can be $ref'd by OpenAPI specs. The tool handles the nuances of
GRM and its file layout and builds.json. It also validates the generated schemas using
openapi-schema-validator before writing to disk.

## Setup & Usage

This utility can be run locally on your desktop or within the GRM docker container:

### Local Setup

1. Make sure you have a recent JVM on your PATH.
1. Create and activate a Python venv using the virtual environment tool of your choice.
1. Install the dependencies: `pip install -r requirements.txt`

### Local Usage

There are lots of settings with defaults which:

1. Assume you are working in this directory
1. Point to the Avro schemas in this project and to directories within
   the target directory of this project
   
So the simplest invocation is:

```
python -m toopenapi 
```

This will build the latest build into ../target/openapi/schemas, with names
like org.ga4gh.methods-3.1.0.yaml.

For more control, check the tool's help:

```
python -m toopenapi --help
```

### Containerised Setup

To use this utility within the GRM Docker container you first need to build that container.
Assuming you are at the root of this project (not in this sub-directory) and you have Docker
installed and running, do:

```
export PYTHON_VERSION=3
make build
```

### Containerised Usage

First run an interactive session on an instance of the GRM container:

```
docker run -it -w /gel/GelReportModels/openapi \
-v "$(pwd)/target":/gel/GelReportModels/target \
registry.gitlab.com/genomicsengland/gelreportmodels/gelreportmodels/gelreportmodels-python3 bash
```

Then within that session do:

```
pip install -r requirements.txt
python -m toopenapi
```

As for local usage this will build the latest build into ../target/openapi/schemas,
with names like org.ga4gh.methods-3.1.0.yaml.

## Versions

This tool works off the same [builds.json](./protocols/resources/builds.json) as the other GRM builds.
By default it uses the highest build defined in that file, but this can be overridden by passing a 
valid build version as the '-v' or '--version' flag, e.g:

```
python -m toopenapi -v 5.0.0
```

## Testing

Unit tests can be run in the usual way:

```
python -m unittest
```

These tests include validation of an OpenAPI spec that references the JSON Schemas
produced by the tool.

### Testing ad-hoc schemas

You can also test that an OpenAPI schema can be valid when including $refs to these
schema docs, e.g. using [redocly](https://redocly.com/redocly-cli/) as long as you
put that spec in the same directory as the schema docs:

```
cp ./yourspec.yaml ../target/openapi/schemas 
redocly lint  ../target/openapi/schemas/yourspec.yaml
```
