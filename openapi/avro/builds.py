""" GelReportModels Build Utilities

    All the logic that is tied to the particular way we have organised GelReportModels is in 
    this module. It provides helper function(s) for getting details of our GRM Avro builds
    and versions, converting AVDL to IDL (.avsc) files etc.
"""
import json
import os
import re
import subprocess

import semver
from files.utils import *

BASEVERSION = re.compile(
    r"[vV]?(?P<major>0|[1-9]\d*)(\.(?P<minor>0|[1-9]\d*)(\.(?P<patch>0|[1-9]\d*))?)?",
    re.VERBOSE,
)


def get_avro_model_versions(build_version: str, build_file: str) -> dict[str, str]:
    """Find the specified build in the build_file. Simplify that build down to a dict
    that maps each model ('package') to its version.

    This returns something like:
    {'org.ga4gh.models': '3.1.0', 'org.gel.models.metrics.avro': '1.2.2', ... }
    """
    matched_build = _find_build(build_version, build_file)
    model_versions = _read_model_versions_from_build(matched_build)
    return model_versions


def get_avro_file_versions(source_files: list[str]) -> dict[str:str]:
    """Given a list of AVDL files **in their original, versioned directory structure**,
    build a dict that maps the filenames (without extensions) to their versions
    (the names of their parent directories). So if there were a source file at
    '/org/ga4gh.models/3.1.0/variants.avdl' the dict would have an entry like {'variants', '3.1.0'}.

    This returns something like:
    {'variants': '3.1.0', 'referencemethods': '3.1.0', 'readmethods': '3.1.0' ...}
    """
    file_names_to_versions = {}
    for source_file in source_files:
        version = os.path.basename(
            os.path.abspath(os.path.join(source_file, os.pardir))
        )
        file_name = get_file_name(source_file)
        file_names_to_versions[file_name] = version
    return file_names_to_versions


def parse_idl_files(idl_files: list[str]) -> dict[str:dict]:
    """Parse the supplied idl (.avsc) files and return  a dict that maps
    the filenames (without extensions) to their parsed contents

    This returns something like:
    {'variants': {"protocol" : "Variants", "namespace" : "org.ga4gh.models", "doc"...}}
    """
    file_names_to_avro = {}
    for idl_file in idl_files:
        with open(idl_file) as f:
            avro = json.load(f)
            file_names_to_avro[get_file_name(idl_file)] = avro
    return file_names_to_avro


def find_avdl_files(model_versions: dict[str, str], source_dir: str) -> list[str]:
    """Given a dict mapping models to model versions, infer the set of directories under
    the source_dir containing the relevant sets of avdl files. Return the paths to the
    avdl files in those directories.
    """
    avdl_files = []
    for model, version in model_versions.items():
        dir = os.path.join(source_dir, model, version)
        avdl_files.extend(
            [
                os.path.join(dir, file)
                for file in os.listdir(dir)
                if file.endswith(".avdl")
            ]
        )
    return avdl_files


def generate_idl_files(avro_tools_jar: str, avdl_dir: str, avsc_dir: str) -> list[str]:
    """Use Avro tools to convert the supplied avdl files to IDL (.avsc) files"""
    idl_files = []
    for file in os.listdir(avdl_dir):
        file_name = get_file_name(file)
        source_file = os.path.join(avdl_dir, file)
        target_file = os.path.join(avsc_dir, f"{file_name}.avsc")
        subprocess.run(
            ["java", "-jar", avro_tools_jar, "idl", source_file, target_file]
        )
        idl_files.append(target_file)
    return idl_files


def _coerce_sem_ver(v):
    """Coerce invalid semVers (e.g. '7.11' -> '7.11.0')"""
    match = BASEVERSION.search(v)
    if not match:
        raise Exception(f"Cannot treat {v} as a semVer")
    ver = ".".join(
        ["0" if value is None else value for key, value in match.groupdict().items()]
    )
    return ver


def _find_build(build_version: str, build_file: str) -> dict:
    """Find a build in the build_file based on a matching version
    or else 'latest'.
    """
    with open(build_file, "r") as f:
        builds = json.load(f)
        matched_build = None
        if build_version == "latest":
            v = "0.0.0"
            for build in builds["builds"]:
                if semver.compare(_coerce_sem_ver(build["version"]), v) > -1:
                    matched_build = build
                    break
                v = build["version"]
        else:
            for build in builds["builds"]:
                if build["version"] == build_version:
                    matched_build = build
                    break
        if matched_build is None:
            raise Exception(f"Cannot find build matching {build_version}")
        return matched_build


def _read_model_versions_from_build(matched_build: dict) -> dict[str, str]:
    """Given a build from the build_file, return a dict that maps model
    names ('packages') to their versions e.g. {'org.ga4gh.models': '3.0', ...}
    """
    model_versions = {}
    packages = matched_build["packages"]
    for package in packages:
        model = package["package"]
        version = package["version"]
        model_versions[model] = version
    return model_versions
