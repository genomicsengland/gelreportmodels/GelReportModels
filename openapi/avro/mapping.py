""" Avro-to-OpenAPI Mapping Utilities

    This module contains functions to convert Avro data to OpenApi (Json schema) documents.
    It has no dependency on GelReportModels itself.
"""
from model.openapi import *


def avro_to_openapi(
    file_names_to_avro_data: dict[str, dict], file_names_to_versions: dict[str, str]
) -> list[OpenApiSchemaDocument]:
    """Entry point to mapping Avro data to OpenAPI / JSon Schema documents.

    Given a dict that maps logical names to Avro data, and a dict that maps the same names
    to the versions that the Avro data represents:

    1. Infer the set of namespace+version combinations that the Avro data represents.
    2. For each distinct namespace+version, create an OpenApiSchemaDocument.
    3. Populate each OpenApiSchemaDocument with OpenAPiTypes that match the Avro
       types found in the Avro data - where those types belong to the same namespace+version.
       a. Where a type references another type within the same namespace+versions, use a
          $ref to the corresponding type within the 'definitions' of the document.
       b. Where a type references a type in another namespace+versions, use a $ref to the
          type within the 'definitions' of the corresponding OpenApiSchemaDocument, based on
          that document's file_name.
    4. Return the OpenApiSchemaDocuments.
    """
    types_to_openapi_file_names: dict[str, str] = _map_avro_types_to_openapi_file_names(
        file_names_to_avro_data, file_names_to_versions
    )
    openapi_file_names: set[str] = set(list(types_to_openapi_file_names.values()))
    openapi_file_names_to_openapi_docs: dict[
        str, OpenApiSchemaDocument
    ] = _initialise_openapi_docs(openapi_file_names)
    avro_data: list[str] = list(file_names_to_avro_data.values())
    _populate_openapi_docs(
        avro_data, types_to_openapi_file_names, openapi_file_names_to_openapi_docs
    )
    openapi_docs: list[OpenApiSchemaDocument] = list(
        openapi_file_names_to_openapi_docs.values()
    )
    return openapi_docs


def _map_avro_types_to_openapi_file_names(
    file_names_to_avro_data: dict[str, dict], file_names_to_versions: dict[str, str]
) -> dict[str, str]:
    """Build the map of type names -> target (yaml) file names for each namespace+version combination.
    This is used s the basis for building the OpenApiSchemaDocuments. It is also used when building
    $refs that point from one OpenApiSchemaDocument to another.

    This returns something like:
    {'Position': 'org.ga4gh.models-3.1.0.yaml', 'CancerParticipant': 'org.gel.models.participant-1.2.2.yaml' ...}
    """
    types_to_openapi_file_names = {}
    for avro_file_name, avro in file_names_to_avro_data.items():
        avro_namespace = avro["namespace"]
        openapi_file_name = _avro_file_name_to_openapi_file_name(
            avro_file_name, avro_namespace, file_names_to_versions
        )
        avro_types = avro["types"]
        for avro_type in avro_types:
            avro_type_namespace = avro_type.get("namespace", None)
            avro_type_name = avro_type.get("name", None)
            if avro_type_name and (
                avro_type_namespace is None or avro_type_namespace == avro_namespace
            ):
                qualified_avro_type_name = f"{avro_namespace}.{avro_type_name}"
                types_to_openapi_file_names[
                    qualified_avro_type_name
                ] = openapi_file_name
    return types_to_openapi_file_names


def _initialise_openapi_docs(
    openapi_file_names: set[str],
) -> dict[str, OpenApiSchemaDocument]:
    """Initialise a set of empty OpenApiSchemaDocuments matching the supplied list
    of file names, and return a dict that maps those file names to those OpenApiSchemaDocuments.
    """
    openapi_docs = {}
    for openapi_file_name in openapi_file_names:
        openapi_doc = OpenApiSchemaDocument(file_name=openapi_file_name)
        openapi_docs[openapi_file_name] = openapi_doc
    return openapi_docs


def _populate_openapi_docs(
    avro_data: list[dict],
    types_to_openapi_file_names: dict[str, str],
    openapi_file_names_to_openapi_docs: dict[str, OpenApiSchemaDocument],
):
    """Populate the OpenApiSchemaDocuments using the supplied Avro data:

    1. Traverse the types within the supplied avro docs, building corresponding OpenApiTypes.
    2. Look up the name of the target file for each type in types_to_openapi_file_names.
    3. Use that file name to find the OpenApiSchemaDocument in openapi_file_names_to_openapi_docs.
    4. Add the OpenApiType to the OpenApiSchemaDocument.
    """
    for avro in avro_data:
        avro_namespace = avro["namespace"]
        avro_types = avro["types"]
        for avro_type in avro_types:
            avro_type_namespace = avro_type.get("namespace", None)
            if avro_type_namespace is None or avro_type_namespace == avro_namespace:
                avro_type_name = avro_type.get("name", None)
                qualified_avro_type_name = f"{avro_namespace}.{avro_type_name}"
                openapi_file_name = types_to_openapi_file_names[
                    qualified_avro_type_name
                ]
                openapi_doc = openapi_file_names_to_openapi_docs[openapi_file_name]
                openapi_type = _avro_type_to_openapi_type(
                    avro_namespace, avro_type, avro_type, types_to_openapi_file_names
                )
                openapi_doc.add_type(key=avro_type_name, type=openapi_type)


def _avro_type_to_openapi_type(
    avro_namespace: str,
    avro_context: dict,
    avro_type: Any,
    types_to_openapi_file_names: dict[str, str],
) -> OpenApiType:
    """Given a single Avro type, return a corresponding OpenApiType"""
    if (
        type(avro_type) == dict
    ):  # an object, enum or a simple type that has some additional attributes
        return _avro_dict_to_openapi_type(
            avro_namespace, avro_type, types_to_openapi_file_names
        )
    elif type(avro_type) == list:  # a type that is a set of possible types
        types = _avro_types_to_openapi_types(
            avro_namespace, avro_context, avro_type, types_to_openapi_file_names
        )
        return OpenApiCompositeType(types=types)
    elif (
        avro_type == "map"
    ):  # an object where the 'values' indicate constraints on its properties
        avro_item_types = avro_context.get("values", None)
        openapi_array_items = _avro_types_to_openapi_types(
            avro_namespace, avro_context, avro_item_types, types_to_openapi_file_names
        )
        return OpenApiObjectType(
            namespace=avro_namespace, additionalProperties=openapi_array_items
        )
    elif avro_type == "array":
        avro_item_types = avro_context.get("items", None)
        openapi_array_items = _avro_types_to_openapi_types(
            avro_namespace, avro_context, avro_item_types, types_to_openapi_file_names
        )
        return OpenApiArrayType(items=openapi_array_items)
    elif avro_type == "string":
        return OpenApiStringType()
    elif avro_type == "bytes":
        return OpenApiBase64EncodedStringType()
    elif avro_type in ["float", "double"]:
        return OpenApiNumberType()  # limits?
    elif avro_type in ["int", "long"]:
        min = -9223372036854775808 if avro_type == "long" else -2147483648
        max = 9223372036854775807 if avro_type == "long" else 2147483647
        return OpenApiIntegerType(minimum=min, maximum=max)
    elif avro_type == "boolean":
        return OpenApiBooleanType()
    elif avro_type == "null":
        return OpenApiNullType()
    elif type(avro_type) == str and _is_known_type(
        avro_type, avro_namespace, types_to_openapi_file_names
    ):
        openapi_ref = _avro_type_to_openapi_ref(
            avro_namespace, avro_type, types_to_openapi_file_names
        )
        return OpenApiRefType(ref=openapi_ref)
    else:
        raise Exception(f"Unrecognised Avro type: {avro_type}")


def _avro_dict_to_openapi_type(
    avro_namespace: str, avro_type: dict, types_to_openapi_file_names: dict[str, str]
) -> OpenApiType:
    """Given a dict that defines an Avro type, return a corresponding OpenApiType.
    This is either an object, an enum or a simple type with additional qualifying
    attributes (in which case we recurse back to _avro_type_to_openapi_type(), after
    unpacking the type-of-the-type).
    """
    avro_type_name = avro_type.get("name", None)
    avro_type_doc = avro_type.get("doc", None)
    avro_type_type = avro_type.get("type", "record")
    if avro_type_type in ["record", "error"]:
        openapi_type = OpenApiObjectType(
            title=avro_type_name, namespace=avro_namespace, description=avro_type_doc
        )
        avro_fields = avro_type.get("fields", None)
        for avro_field in avro_fields:
            openapi_type.add_property(
                _avro_field_to_openapi_property(
                    avro_namespace, avro_field, types_to_openapi_file_names
                )
            )
        return openapi_type
    elif avro_type_type == "enum":
        obj_enum = avro_type.get("symbols", [])
        return OpenApiStringType(
            title=avro_type_name, description=avro_type_doc, enum=obj_enum
        )
    else:
        return _avro_type_to_openapi_type(
            avro_namespace, avro_type, avro_type_type, types_to_openapi_file_names
        )


def _avro_types_to_openapi_types(
    avro_namespace: str,
    avro_context: dict,
    avro_item_types: Union[str, list],
    types_to_openapi_file_names: dict[str, str],
) -> Union[None, list[OpenApiProperty]]:
    """ " Given a single Avro type or a list of Avro types, return a list of
    corresponding OpenApiTypes, or None, if no Avro types were supplied.
    """
    openapi_types = None
    if avro_item_types:
        if type(avro_item_types) != list:
            avro_item_types = [avro_item_types]
        openapi_types = []
        for avro_item_type in avro_item_types:
            openapi_types.append(
                _avro_type_to_openapi_type(
                    avro_namespace,
                    avro_context,
                    avro_item_type,
                    types_to_openapi_file_names,
                )
            )
    return openapi_types


def _avro_field_to_openapi_property(
    avro_namespace: str, avro_field: dict, types_to_openapi_file_names: dict[str, str]
) -> OpenApiProperty:
    """Given an Avro field, return a corresponding OpenApiProperty"""
    avro_field_name = avro_field.get("name", None)
    avro_field_doc = avro_field.get("doc", None)
    avro_field_default = avro_field.get("default", None)
    avro_field_type = avro_field.get("type", None)
    openapi_type = _avro_type_to_openapi_type(
        avro_namespace, avro_field, avro_field_type, types_to_openapi_file_names
    )
    openapi_property = OpenApiProperty(
        name=avro_field_name,
        description=avro_field_doc,
        default=avro_field_default,
        type=openapi_type,
    )
    return openapi_property


def _is_known_type(
    avro_type_name: str,
    avro_namespace: str,
    types_to_openapi_file_names: dict[str, str],
):
    """For type-names that are not part of the Avro spec the assumption is that these
    are references to some custom type declared in one of the Avro docs. If so, the
    type name (including its namespace) should be found in the keys of
    types_to_openapi_file_names. This function indicates if the type was found or not.
    """
    if avro_type_name.find(".") == -1:
        qualified_avro_type_name = f"{avro_namespace}.{avro_type_name}"
    else:
        qualified_avro_type_name = avro_type_name
    return qualified_avro_type_name in types_to_openapi_file_names


def _avro_type_to_openapi_ref(
    avro_namespace: str,
    avro_type_name: str,
    types_to_openapi_file_names: dict[str, str],
):
    """Any reference ot a type-name that is a) not part of the Avro spec and v) is declared
    (with namespace) in types_to_openapi_file_names must be transformed into a reference.
    This will either be a local reference within the same file, or else a remoite reference to
    another file, depending on whether the namespaces match or not.

    So, for example, if avro_type_name = 'org.gel.models.report.avro.TissueSource' and the
    avro_namespace is 'org.gel.models.report.avro' then this returns '#/definitions/TissueSource'
    (because the namespaces match, we know that TissueSource will end up being declared in the
    same file).

    Whereas, if the avro_namespace is something else this uses types_to_openapi_file_names
    to find the target file name (which will be something like 'org.gel.models.participant-1.4.0.yaml').
    In this case it returns a ref that is prefixed with that file name, e.g.
    'org.gel.models.participant-1.4.0.yaml#/definitions/TissueSource'
    """
    i = avro_type_name.rfind(".")
    definition_name = avro_type_name[i + 1 :]
    if i > -1 and not avro_type_name.startswith(avro_namespace):
        openapi_file_name = types_to_openapi_file_names.get(avro_type_name, None)
        if openapi_file_name is None:
            raise Exception(
                f"""Cannot resolve type {avro_type_name} to any schema file-name. 
                          Is there an unresolvable reference in the Avro or a missing 
                          package in the build defintion?"""
            )
        return f"{openapi_file_name}#/definitions/{definition_name}"
    else:
        if i > -1:
            type_name = avro_type_name[i + 1 :]
        else:
            type_name = avro_type_name
        return f"#/definitions/{type_name}"


def _avro_file_name_to_openapi_file_name(
    file_name: str, avro_namespace: str, file_names_to_versions: dict[str, str]
) -> str:
    """Given an Avro file name amd its top-level namespace, and a mapping of Avro
    file names to versions, derive a target OpenAPI document name.

    For example, if file_name='Bar', avro_namespace='org.gelmodels.report.avro'
    amd file_names_to_versions contains {'Bar': '3.1'}, this would use the file_name
    to find the version ('3.1') in the file_names_to_versions, and combine
    that version with a (cleaned-up) version of the avro_namespace to create a
    target document name of 'org.gelmodels.report-3.1.yaml'.
    """
    openapi_schema_name = _avro_namespace_to_openapi_schema_name(avro_namespace)
    opoenapi_version = file_names_to_versions[file_name]
    openapi_file_name = f"{openapi_schema_name}-{opoenapi_version}.yaml"
    return openapi_file_name


def _avro_namespace_to_openapi_schema_name(avro_namespace: str):
    """Strip out any '.avro' or 'avro' string from a namespace so we can use it
    in OpenAPI/Json schema doc names without confusion.
    """
    openapi_schema_name = (
        avro_namespace.replace(".avro", "").replace("avro.", "").replace("avro", "")
    )
    return openapi_schema_name
