import os
import shutil
from pathlib import Path


def mk_dirs(root_dir: str, sub_dirs: list[str]):
    """Make one or more sub-dirs under root_dir and return them"""
    target_dirs = []
    for sub_dir in sub_dirs:
        target_dir = os.path.abspath(os.path.join(root_dir, sub_dir))
        Path(target_dir).mkdir(parents=True, exist_ok=True)
        target_dirs.append(target_dir)
    return target_dirs


def get_file_name(file: str) -> str:
    """Get the part of a file name prior to the fnal period"""
    return os.path.splitext(os.path.basename(file))[0]


def get_file_extension(file) -> str:
    """Get the extension of a file name excluding the leading period"""
    return os.path.splitext(os.path.basename(file))[1][1:]


def copy_files(source_files: list[str], target_dir: str):
    """Copy a set of files to a target directory"""
    for source_file in source_files:
        target_file_name = os.path.basename(source_file)
        target_file = os.path.join(target_dir, target_file_name)
        if os.path.exists(target_file):
            os.remove(target_file)
        shutil.copyfile(source_file, target_file)
