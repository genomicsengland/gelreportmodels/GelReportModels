""" OpenAPI model classes

    The principle followed here is that these model classes make a distinction between
    their in-memory structure (which is intended to be easy to work with) and their eventual
    serialised-structure (which must comply with Json Schema). This allows us to hide the 
    complexity of things like array type 'items', object type 'additionalProperties' and 
    property 'types' which may be defined directly or wrapped im lists under 'oneOf' or 
    'anyOf' keys, depending on their multiplicity (which we finally know only at the time of
    serialisation).
"""
import os
import re
from abc import ABC
from typing import Any, Union

import yaml
from jsonschema.validators import Draft201909Validator, Draft202012Validator


class OpenApiElement(ABC):
    """Abstract base class for OpenApi model classes"""

    def __getstate__(self, suppress: list[str] = []):
        """For serialization to YAML, return only non-empty attributes that are not in the
        'suppress' list
        """
        state = self.__dict__.copy()
        for a in self.__dict__:
            if not state[a] and type(state[a]) != bool:
                del state[a]
        for a in suppress:
            if a in state:
                del state[a]
        return state

    def to_yaml(self):
        """Render this OpenApiElement to YAML.
        This suppresses typing and aliases in YAML, makes the rendering of lists
        as compact as possible, and disables the alphabetic sorting of keys (so
        that keys are rendered in the order they are  declared on the class). It
        also fixes the presentation of multi-line strings to use block literal
        style (|) - see https://yaml-multiline.info.
        """

        def str_presenter(dumper, data):
            if len(data.splitlines()) > 1:  # check for multiline string
                return dumper.represent_scalar("tag:yaml.org,2002:str", data, style="|")
            return dumper.represent_scalar("tag:yaml.org,2002:str", data)

        yaml.add_representer(str, str_presenter)
        yaml.emitter.Emitter.process_tag = (
            lambda *args, **kw: None
        )  # Don't emit type tags
        yaml.dumper.Dumper.ignore_aliases = (
            lambda self, data: True
        )  # Don't emit aliases
        return yaml.dump(self, default_flow_style=False, sort_keys=False)

    def fix_strings(self, str):
        """Replace all multiple-spaces with single space (but leave newlines alone!)"""
        if str:
            return re.sub(" +", " ", str)
        else:
            return None


class OpenApiType(OpenApiElement):
    """Base class for OpenAPI types"""

    def __init__(
        self,
        type: str = None,
        title: str = None,
        description: str = None,
        enum: list[Any] = None,
        namespace: str = None,
    ):
        self.type = type
        self.description = self.fix_strings(description)
        self.enum = enum
        self.title = title
        self.namespace = namespace

    def __getstate__(self):
        """Serialization view of OpenApiType for dumping as YAML"""
        return OpenApiElement.__getstate__(self, suppress=["namespace", "title"])


class OpenApiRefType(OpenApiType):
    """OpenAPI type where the type is replaced by a $ref to some other local
    or remote type definition.
    """

    def __init__(self, ref: str = None):
        super().__init__()
        setattr(self, "$ref", ref)


class OpenApiNullType(OpenApiType):
    def __init__(self):
        super().__init__("null")


class OpenApiStringType(OpenApiType):
    def __init__(
        self,
        title: str = None,
        description: str = None,
        enum: list[str] = None,
        minLength: int = None,
        maxLength: int = None,
        pattern: str = None,
    ):
        super().__init__("string", title=title, description=description, enum=enum)
        self.minLength = minLength
        self.maxLength = maxLength
        self.pattern = pattern


class OpenApiBase64EncodedStringType(OpenApiStringType):
    """A restriction on the raw 'string' type to ensure that it only contains
    valid base64-encoded data (i.e. 'bytes' in Avro). See also
    https://stackoverflow.com/questions/475074/regex-to-parse-or-validate-base64-data
    """

    BASE64ENC = "^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$"

    def __init__(
        self, title: str = None, description: str = None, enum: list[str] = None
    ):
        super().__init__(
            title=title,
            description=description,
            pattern=OpenApiBase64EncodedStringType.BASE64ENC,
        )


class OpenApiNumberType(OpenApiType):
    def __init__(
        self,
        type: str = "number",
        description: str = None,
        enum: list[float] = None,
        minimum: int = None,
        maximum: int = None,
        exclusiveMinimum: bool = None,
        exclusiveMaximum: bool = None,
        multipleOf: int = None,
    ):
        super().__init__(type, description=description, enum=enum)
        self.minimum = minimum
        self.maximum = maximum
        self.exclusiveMinimum = exclusiveMinimum
        self.exclusiveMaximum = exclusiveMaximum
        self.multipleOf = multipleOf


class OpenApiIntegerType(OpenApiNumberType):
    def __init__(
        self,
        description: str = None,
        minimum: int = None,
        maximum: int = None,
        exclusiveMinimum: bool = None,
        exclusiveMaximum: bool = None,
        multipleOf: int = None,
    ):
        super().__init__(
            type="integer",
            description=description,
            minimum=minimum,
            maximum=maximum,
            exclusiveMinimum=exclusiveMinimum,
            exclusiveMaximum=exclusiveMaximum,
            multipleOf=multipleOf,
        )


class OpenApiBooleanType(OpenApiType):
    def __init__(self, description: str = None):
        super().__init__(type="boolean", description=description)


class OpenApiArrayType(OpenApiType):
    def __init__(
        self,
        description: str = None,
        items: Union[OpenApiType, list[OpenApiType]] = None,
    ):
        super().__init__("array", description=description)
        if items:
            self.set_items(items)

    def set_items(self, items: Union[OpenApiType, list[OpenApiType]]):
        """Define the items of this OpenApiArrayType"""
        self.items = items

    def __getstate__(self):
        """Serialization view of OpenApiArrayType for dumping as YAML.
        This suppresses keys that are not valid Json Schema elements,
        and it pushes the 'items' down into an 'anyOf' if there is > 1.
        """
        state = OpenApiElement.__getstate__(self, suppress=["name", "types"])
        if hasattr(self, "items"):
            if type(self.items) != list:
                state["items"] = self.items.__getstate__()
            elif len(self.items) == 1:
                state["items"] = self.items[0].__getstate__()
            elif len(self.items) > 1:
                state["items"] = {"anyOf": [t.__getstate__() for t in self.items]}
        return state


class OpenApiCompositeType(OpenApiType):
    """A type that is simply a wrapper around multiple other (generally
    alternative) OpenApiTypes.
    """

    def __init__(self, types: list[OpenApiType]):
        super().__init__()  # no 'type' if it's composite
        self.types = types

    def __getstate__(self):
        """Serialization view of OpenApiCompositeType for dumping as YAML.
        This returns the serialization view of the nested types as an array.
        """
        return [t.__getstate__() for t in self.types]

    def to_yaml(self):
        """Override to_yaml() just so that rendering of this OpenApiCompositeType on
        its own matches what it would be when included in a parent YAML doc.
        This is purely for unit-testing, it's never used in real mapping.
        """
        return OpenApiType.to_yaml(self.types)


class OpenApiObjectType(OpenApiType):
    def __init__(
        self,
        title: str = None,
        description: str = None,
        namespace: str = None,
        additionalProperties: OpenApiType = None,
    ):
        super().__init__(
            "object", title=title, description=description, namespace=namespace
        )
        self.additionalProperties = additionalProperties
        self.properties = {}
        self.required = []

    def __getstate__(self):
        """Serialization view of OpenApiObjectType for dumping as YAML.
        This suppresses keys that are not valid Json Schema elements,
        and it pushes the 'additionalProperties' down into an 'anyOf'
        if there is > 1.
        """
        state = OpenApiElement.__getstate__(self, suppress=["namespace", "title"])
        if self.additionalProperties and type(self.additionalProperties) == list:
            if len(self.additionalProperties) == 1:
                state["additionalProperties"] = self.additionalProperties[
                    0
                ].__getstate__()
            else:
                state["additionalProperties"] = {
                    "anyOf": [t.__getstate__() for t in self.additionalProperties]
                }
        return state

    def add_property(self, property: "OpenApiProperty", required: bool = False):
        """Add an OpenApiProperty to this OpenApiObjectType"""
        self.properties[property.name] = property
        if required:
            self.required.append(property.name)


class OpenApiProperty(OpenApiElement):
    """An OpenAPI property is an element within the 'properties' of an
    OpenApiObjectType (i.e. where 'type' = 'object').
    """

    def __init__(
        self,
        name: str = None,
        type: OpenApiType = None,
        description: str = None,
        default: str = None,
    ):
        self.name = name
        self.type = type
        self.description = self.fix_strings(description)
        self.default = default

    def __getstate__(self):
        """Serialization view of OpenApiProperty for dumping as YAML.
        This suppresses keys that are not valid Json Schema elements,
        and it copies the attributes of its 'type' onto itself unless
        the type is a OpenApiCompositeType, in which case it pushes the
        types down into a 'oneOf' otherwise.
        """
        state = OpenApiElement.__getstate__(self, suppress=["name", "type"])
        if type(self.type) == OpenApiCompositeType:
            state["oneOf"] = self.type.__getstate__()
        else:
            ordered_state = {}
            typeState = self.type.__getstate__()
            if "type" in typeState:
                ordered_state["type"] = typeState.get("type", None)
            ordered_state.update(state)
            ordered_state.update(typeState)
            state = ordered_state
        return state


class OpenApiSchemaDocument(OpenApiElement):
    """Technically this document will be a Json Schema document which is something
    that OpenApi 3.1 fully supports - see https://spec.openapis.org/oas/v3.1.0#data-types.
    That is: OpenApi allows types defined  in these documents to be imported into OpenApi
    spec documents via $refs - see https://swagger.io/docs/specification/using-ref/
    """

    def __init__(
        self,
        file_name: str = None,
        version: str = "https://json-schema.org/draft/2020-12/schema",
        title: str = None,
    ):
        self.file_name = file_name
        setattr(self, "$schema", version)
        self.definitions = {}

    def _get_validator(self):
        """Get the appropriate validator depending on the schema's URI."""
        schema = getattr(self, "$schema")
        if schema == "https://json-schema.org/draft/2020-12/schema":
            return Draft202012Validator
        elif schema == "https://json-schema.org/draft/2019-09/schema":
            return Draft201909Validator
        else:
            raise Exception(f"Unsupported version: {schema}")

    def __getstate__(self):
        """Serialization view of OpenApiProperty for dumping as YAML.
        This just suppresses keys that are not valid Json Schema elements.
        """
        return OpenApiElement.__getstate__(self, suppress=["file_name"])

    def validate(self):
        """Validate this OpenApiSchemaDocument (in its YAML form).
        Note: does *not* validuate $refs - https://github.com/python-jsonschema/jsonschema/issues/917
        """
        spec = yaml.load(self.to_yaml(), Loader=yaml.Loader)
        self._get_validator().check_schema(spec)

    def to_file(self, target_dir):
        """Write this OpenApiSchemaDocument as Yaml to the supplied target_dir."""
        target_file = os.path.join(target_dir, self.file_name)
        with open(target_file, "w") as tf:
            tf.write(self.to_yaml())

    def add_type(self, type: OpenApiType, key: str = None):
        """Add an OpenApiType to this OpenApiSchemaDocument"""
        if not type.title and not key:
            raise Exception(
                "Missing key and type has no title, cannot add to OpenApiDocument"
            )
        if key:
            self.definitions[key] = type
        else:
            self.definitions[type.title] = type
