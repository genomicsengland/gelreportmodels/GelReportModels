import json
import os
import unittest
from operator import mod

import yaml
from avro.mapping import (
    _avro_file_name_to_openapi_file_name,
    _avro_namespace_to_openapi_schema_name,
    _avro_type_to_openapi_ref,
    _avro_type_to_openapi_type,
)


class AvroMappingTest(unittest.TestCase):
    def test_avro_types_to_openapi_types(self):
        """Test the mapping from Avro type data to OpenAPI type yaml using the cases
        that are defined in ./testdata./type_tests.yaml.
        """
        with open(
            f"{os.path.dirname(os.path.realpath(__file__))}/testdata/type_tests.yaml",
            "r",
        ) as stream:
            tests = yaml.safe_load(stream)
        for name, test in tests.items():
            description = test["description"]
            avro_type = json.loads(test["avro"])
            avro_namespace = test["namespace"]
            if "type_names_to_openapi_file_names" in test:
                type_names_to_openapi_file_names = test[
                    "type_names_to_openapi_file_names"
                ]
            else:
                type_names_to_openapi_file_names = {}
            openapi_type = _avro_type_to_openapi_type(
                avro_namespace, {}, avro_type, type_names_to_openapi_file_names
            )
            expected = test["openapi"]
            self.assertEqual(
                openapi_type.to_yaml(),
                expected,
                msg=f"test case '{name}' failed to map {description}",
            )

    def test_avro_type_to_openapi_ref(self):
        type_names_to_file_names = {
            "avro.a.b.c.Foo": "foofile.yaml",
            "avro.x.y.z.Bar": "barfile.yaml",
        }
        avro_namespace = "avro.a.b.c"
        cases = {
            "avro.a.b.c.Foo": "#/definitions/Foo",
            "avro.x.y.z.Bar": "barfile.yaml#/definitions/Bar",
        }
        for avro_type_name, expected in cases.items():
            ref = _avro_type_to_openapi_ref(
                avro_namespace, avro_type_name, type_names_to_file_names
            )
            self.assertEqual(ref, expected)

    def test_avro_namespace_to_openapi_schema_name(self):
        cases = {"a.b.c": "a.b.c", "avro.a.b.c": "a.b.c", "a.b.c.avro": "a.b.c"}
        for namespace, expected in cases.items():
            openapi_schema_name = _avro_namespace_to_openapi_schema_name(namespace)
            self.assertEqual(openapi_schema_name, expected)

    def test_avro_file_to_schema_file_name(self):
        avro_file = "foo"
        avro_namespace = "a.b.c.avro"
        file_names_to_versions = {"foo": "1.2.3", "bar": "4.5.6"}
        schema_file_name = _avro_file_name_to_openapi_file_name(
            avro_file, avro_namespace, file_names_to_versions
        )
        expected = "a.b.c-1.2.3.yaml"
        self.assertEqual(schema_file_name, expected)
