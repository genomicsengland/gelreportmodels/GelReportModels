import unittest
from operator import mod

from model.openapi import *


class OpenaApiTest(unittest.TestCase):
    def build_schema_doc(self):
        root = OpenApiObjectType(
            title="Experiment", namespace="org.opencb.biodata.models.metadata"
        )
        compositeType = OpenApiCompositeType(
            types=[OpenApiNullType(), OpenApiStringType(maxLength=2)]
        )
        center = OpenApiProperty("center", type=compositeType)
        root.add_property(center)
        count = OpenApiProperty("count", type=OpenApiIntegerType(maximum=100))
        root.add_property(count)
        arrayOfStrings = OpenApiProperty(
            "arrayOfStrings", type=OpenApiArrayType(items=OpenApiStringType())
        )
        root.add_property(arrayOfStrings)
        arrayOfStringsOrIntType = OpenApiArrayType(
            items=[OpenApiIntegerType(), OpenApiStringType()]
        )
        arrayOfStringsOrInts = OpenApiProperty(
            "arrayOfStringsOrInts", type=arrayOfStringsOrIntType
        )
        root.add_property(arrayOfStringsOrInts)
        arrOfObjectsType = OpenApiArrayType()
        nestedObjectType = OpenApiObjectType(
            title="Experiment", namespace="org.opencb.biodata.models.metadata"
        )
        nestedObjectType.add_property(
            OpenApiProperty("foo", type=OpenApiStringType(maxLength=20))
        )
        nestedObjectType.add_property(
            OpenApiProperty("bar", type=OpenApiIntegerType(maximum=10))
        )
        arrOfObjectsType.set_items(nestedObjectType)
        nestedTypesArray = OpenApiProperty("nestedTypesArray", type=arrOfObjectsType)
        root.add_property(nestedTypesArray)
        ref = OpenApiProperty("ref", type=OpenApiRefType("http://example.com/Foo.yaml"))
        root.add_property(ref)
        doc = OpenApiSchemaDocument(title="Test")
        doc.add_type(root)
        return doc

    def test_validate_schema(self):
        schema = self.build_schema_doc()
        schema.validate()  # throws Exception if invalid JSON Schema

    def test_to_yaml(self):
        schema = self.build_schema_doc()
        yaml = schema.to_yaml()
        expected = """$schema: https://json-schema.org/draft/2020-12/schema
definitions:
  Experiment:
    type: object
    properties:
      center:
        oneOf:
        - type: 'null'
        - type: string
          maxLength: 2
      count:
        type: integer
        maximum: 100
      arrayOfStrings:
        type: array
        items:
          type: string
      arrayOfStringsOrInts:
        type: array
        items:
          anyOf:
          - type: integer
          - type: string
      nestedTypesArray:
        type: array
        items:
          type: object
          properties:
            foo:
              type: string
              maxLength: 20
            bar:
              type: integer
              maximum: 10
      ref:
        $ref: http://example.com/Foo.yaml
"""
        self.assertEqual(yaml, expected)
