import os
import shutil
import unittest
from pathlib import Path

from avro.builds import parse_idl_files
from avro.mapping import avro_to_openapi
from model.openapi import OpenApiSchemaDocument
from openapi_spec_validator import validate_spec
from openapi_spec_validator.readers import read_from_filename


class OpenApiSpecTest(unittest.TestCase):
    # Versions matching the snapshot of Avro data in ./testdata/avsc. Using these ensures that
    # the file-names for each generated OpenApiSchemaDocument matches the corresponding
    # # filenames in the $refs of the OpenApi spec file ./testdata/openapi.yaml
    AVRO_FILE_VERSIONS = {
        "variants": "3.1.0",
        "referencemethods": "3.1.0",
        "readmethods": "3.1.0",
        "reads": "3.1.0",
        "methods": "3.1.0",
        "common": "3.1.0",
        "_metadata": "3.1.0",
        "references": "3.1.0",
        "variantmethods": "3.1.0",
        "sampleState": "1.2.2",
        "FamilySelection": "1.2.2",
        "SupplementaryAnalysisResults": "1.2.2",
        "individualState": "1.2.2",
        "GelVcfMetrics": "1.2.2",
        "GelBamMetrics": "1.2.2",
        "RareDiseaseInterpretationPipeline": "1.2.2",
        "ReportedVsGenetic": "1.2.2",
        "Ngis": "1.4.0",
        "Newborns": "1.4.0",
        "ParticipantSensitiveInformation": "1.4.0",
        "CommonParticipant": "1.4.0",
        "RDParticipant": "1.4.0",
        "CancerParticipant": "1.4.0",
        "VersionControl": "1.4.0",
        "InterpretationRequestCancer": "6.4.0",
        "CommonRequest": "6.4.0",
        "VariantInterpretationLog": "6.4.0",
        "MDTDeliveryProtocol": "6.4.0",
        "InterpretationRequestRD": "6.4.0",
        "ReportVersionControl": "6.4.0",
        "InterpretedGenome": "6.4.0",
        "ExitQuestionnaire": "6.4.0",
        "CommonInterpreted": "6.4.0",
        "ClinicalReport": "6.4.0",
        "HealthCheck": "0.1.0",
        "variantLegacy": "1.3.0",
        "variant": "1.3.0",
        "read": "1.3.0",
        "variantAnnotation": "1.3.0",
        "variantMetadata": "1.3.0",
        "metadata": "1.3.0",
        "evidence": "1.3.0",
        "Coverage": "0.1.0",
        "ReportEvent": "1.5.3",
        "ObservedVariant": "1.5.3",
        "DataIntake": "1.5.3",
        "Transactions": "1.5.3",
        "CvaEvidence": "1.5.3",
        "Comment": "1.5.3",
        "CvaVariant": "1.5.3",
    }

    def test_openapi_spec(self):
        """This parses the snapshot set of GRM Avro data at ./testdata/avsc and uses
        avro.mapping.avro_to_openapi() to create the corresponding OpenApiSchemaDocuments.

        It then validates these documents before writing them out to disk at
        .../../target/tests along with a copy of a pre-canned OpenAPI spec
        (./testdata/openapi.yaml) which references the (expected) schema files via $refs.

        Finally it uses openapi-spec-validator to validate the OpenAPI spec, including
        those $refs.

        If the test does not throw an Exception then we know that converted OpenApiSchemaDocuments
        are not only valid Json Schema, but are also valid when used as types in am OpenAPI 3.0 spec.
        """
        avdl_dir = os.path.join(
            os.path.dirname(os.path.realpath(__file__)), "testdata", "avsc"
        )
        idl_files = [
            os.path.join(avdl_dir, filename)
            for filename in os.listdir(avdl_dir)
            if filename.endswith(".avsc")
        ]
        avro_data: dict[str:dict] = parse_idl_files(idl_files)
        openapi_docs: list[OpenApiSchemaDocument] = avro_to_openapi(
            avro_data, OpenApiSpecTest.AVRO_FILE_VERSIONS
        )
        tmp_dir = os.path.join(
            os.path.dirname(os.path.realpath(__file__)), "..", "..", "target", "tests"
        )
        Path(tmp_dir).mkdir(parents=True, exist_ok=True)
        for openapi_doc in openapi_docs:
            openapi_doc.validate()
            openapi_doc.to_file(tmp_dir)
        spec_file = os.path.join(
            os.path.dirname(os.path.realpath(__file__)), "testdata", "openapi.yaml"
        )
        spec_file_copy = os.path.join(tmp_dir, "openapi.yaml")
        shutil.copyfile(spec_file, spec_file_copy)
        spec_dict, spec_url = read_from_filename(spec_file_copy)
        validate_spec(spec_dict, spec_url=spec_url)
