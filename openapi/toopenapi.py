""" Command-Line tool to convert GelReportModels AVDL files to OpenAPI-compatible Json Schema files
"""
import argparse
import sys

from avro.builds import (
    find_avdl_files,
    generate_idl_files,
    get_avro_file_versions,
    get_avro_model_versions,
    parse_idl_files,
)
from avro.mapping import avro_to_openapi
from files.utils import copy_files, mk_dirs
from model.openapi import OpenApiSchemaDocument


def to_openapi(
    avro_tools_jar: str,
    build_version: str,
    build_file: str,
    source_dir: str,
    target_dir: str,
):
    """Convert Avro files in a GRM 'build' to a set of corresponding OpenAPI / Json Schema files:

    1. Get the avdl files for the specifed build version in the build_file (this
       defines the collection of compatible model versions).
    2. Use these to generate intermediate IDL (AVSC) files.
    3. Use these IDL files to create the corresponding OpenAPI (Json) schema documents.
    4. Render those documents to disk as YAML.
    """
    avdl_dir, avsc_dir, schema_dir = mk_dirs(target_dir, ["avdl", "avsc", "schemas"])
    avro_model_versions: dict[str, str] = get_avro_model_versions(
        build_version, build_file
    )
    avro_source_files: list[str] = find_avdl_files(avro_model_versions, source_dir)
    avro_file_versions: dict[str:str] = get_avro_file_versions(avro_source_files)
    copy_files(avro_source_files, avdl_dir)
    idl_files: list[str] = generate_idl_files(avro_tools_jar, avdl_dir, avsc_dir)
    avro_data: dict[str:dict] = parse_idl_files(idl_files)
    openapi_docs: list[OpenApiSchemaDocument] = avro_to_openapi(
        avro_data, avro_file_versions
    )
    for openapi_doc in openapi_docs:
        openapi_doc.validate()
        openapi_doc.to_file(schema_dir)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Convert Avro schemas to OpenAPI schemas",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-a",
        "--avro_tools_jar",
        dest="avro_tools_jar",
        default="../resources/bin/avro-tools-1.11.3.jar",
        help="The location of the Avro tools jar",
    )
    parser.add_argument(
        "-s",
        "--source_dir",
        dest="source_dir",
        default="../schemas/IDLs/",
        help="The root directory containing avdl files",
    )
    parser.add_argument(
        "-t",
        "--target_dir",
        dest="target_dir",
        default="../target/openapi/",
        help="The target directory",
    )
    parser.add_argument(
        "-v",
        "--version",
        dest="build_version",
        default="latest",
        help="The build to use (see build_file)",
    )
    parser.add_argument(
        "-b",
        "--build_file",
        dest="build_file",
        default="../protocols/resources/builds.json",
        help="The build file to use",
    )
    args = parser.parse_args()
    to_openapi(**vars(args))
