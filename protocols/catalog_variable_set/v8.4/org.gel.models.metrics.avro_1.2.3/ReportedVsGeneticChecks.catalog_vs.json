[
  {
    "name": "ReportedVsGeneticOutcome",
    "required": false,
    "type": "CATEGORICAL",
    "description": "Summary of the software, versions and samples used for the RvsG checks",
    "allowedValues": [
      "familyPassesGvsRChecks",
      "familyFailsACheck",
      "familyMissingACheck"
    ],
    "multiValue": false
  },
  {
    "name": "coverageBasedSex",
    "required": false,
    "type": "OBJECT",
    "description": "Coverage-based sex metrics and inferred karyotype",
    "variableSet": [
      {
        "name": "sampleId",
        "required": false,
        "type": "TEXT",
        "description": "Sample Id (e.g, LP00012645_5GH))",
        "multiValue": false
      },
      {
        "name": "inferredKaryotype",
        "required": false,
        "type": "CATEGORICAL",
        "description": "Inferred karyotype using coverage information",
        "allowedValues": [
          "UNKNOWN",
          "XX",
          "XY",
          "XO",
          "XXY",
          "XXX",
          "XXYY",
          "XXXY",
          "XXXX",
          "XYY",
          "OTHER"
        ],
        "multiValue": false
      },
      {
        "name": "ratioChrX",
        "required": false,
        "type": "DOUBLE",
        "description": "Ratio of the average coverage of chromosome X to the average of the autosomal chromosome coverage",
        "multiValue": false
      },
      {
        "name": "ratioChrY",
        "required": false,
        "type": "DOUBLE",
        "description": "Ratio of the average coverage of chromosome Y to the average of the autosomal chromosome coverage",
        "multiValue": false
      },
      {
        "name": "avgCnvChrX",
        "required": false,
        "type": "DOUBLE",
        "description": "Number of copies of chromosome X",
        "multiValue": false
      },
      {
        "name": "avgCnvChrY",
        "required": false,
        "type": "DOUBLE",
        "description": "Number of copies of chromosome Y",
        "multiValue": false
      },
      {
        "name": "reviewedKaryotype",
        "required": false,
        "type": "CATEGORICAL",
        "description": "Reviewed sex karyotype",
        "allowedValues": [
          "UNKNOWN",
          "XX",
          "XY",
          "XO",
          "XXY",
          "XXX",
          "XXYY",
          "XXXY",
          "XXXX",
          "XYY",
          "OTHER"
        ],
        "multiValue": false
      }
    ],
    "multiValue": true
  },
  {
    "name": "mendelianInconsistencies",
    "required": false,
    "type": "OBJECT",
    "description": "Per family and per sample mendelian inconsistencies",
    "variableSet": [
      {
        "name": "perFamilyMendelErrors",
        "required": false,
        "type": "OBJECT",
        "description": "Number of mendelian inconsitencies per nuclear family. One entry per nuclear family",
        "variableSet": [
          {
            "name": "fatherId",
            "required": false,
            "type": "TEXT",
            "description": "Sample Id of the father",
            "multiValue": false
          },
          {
            "name": "motherId",
            "required": false,
            "type": "TEXT",
            "description": "Sample Id of the mother",
            "multiValue": false
          },
          {
            "name": "numberOfOffspring",
            "required": false,
            "type": "DOUBLE",
            "description": "Number of children in the nuclear family",
            "multiValue": false
          },
          {
            "name": "numberOfMendelErrors",
            "required": false,
            "type": "DOUBLE",
            "description": "Number of Mendelian errors in the nuclear family",
            "multiValue": false
          }
        ],
        "multiValue": true
      },
      {
        "name": "individualMendelErrors",
        "required": false,
        "type": "OBJECT",
        "description": "Number of mendelian inconsitencies per sample and nuclear family. One entry per sample and nuclear family",
        "variableSet": [
          {
            "name": "sampleId",
            "required": false,
            "type": "TEXT",
            "description": "Sample Id",
            "multiValue": false
          },
          {
            "name": "numberOfMendelErrors",
            "required": false,
            "type": "DOUBLE",
            "description": "Number of Mendelian errors per sample in a nuclear family",
            "multiValue": false
          },
          {
            "name": "rateOfMendelErrors",
            "required": false,
            "type": "DOUBLE",
            "description": "Rate of Mendelian errors per sample in a nuclear family to the number of sites tested (number of markers)",
            "multiValue": false
          }
        ],
        "multiValue": true
      },
      {
        "name": "totalNumberOfMendelErrors",
        "required": false,
        "type": "OBJECT",
        "description": "Aggregated number of mendelian inconsitencies per sample and family",
        "variableSet": [
          {
            "name": "familyMendelErrors",
            "required": false,
            "type": "DOUBLE",
            "description": "Total number of Mendelian errors in the family, this should be the sum of the mendelian errors in each\nnuclear family",
            "multiValue": false
          },
          {
            "name": "individualMendelErrors",
            "required": false,
            "type": "OBJECT",
            "description": "Number of Mendelian errors per sample considering all nuclear families. Should be one entry per sample in\nthe family",
            "variableSet": [
              {
                "name": "sampleId",
                "required": false,
                "type": "TEXT",
                "description": "Sample Id",
                "multiValue": false
              },
              {
                "name": "totalnumberOfMendelErrors",
                "required": false,
                "type": "DOUBLE",
                "description": "Total number of Mendelian errors per sample considering all nuclear families",
                "multiValue": false
              }
            ],
            "multiValue": true
          }
        ],
        "multiValue": false
      },
      {
        "name": "locusMendelSummary",
        "required": false,
        "type": "OBJECT",
        "description": "Summary of the type of mendelian inconstencies happening in the family. One entry per sample, chromosome and\ncode",
        "variableSet": [
          {
            "name": "sampleId",
            "required": false,
            "type": "TEXT",
            "description": "Sample Id",
            "multiValue": false
          },
          {
            "name": "chr",
            "required": false,
            "type": "TEXT",
            "description": "Chromosome (1-22, X, Y, XY)",
            "multiValue": false
          },
          {
            "name": "code",
            "required": false,
            "type": "DOUBLE",
            "description": "Numeric error code (more information here: http://zzz.bwh.harvard.edu/plink/summary.shtml#mendel)",
            "multiValue": false
          },
          {
            "name": "numberOfErrors",
            "required": false,
            "type": "DOUBLE",
            "description": "Number of errors of the type \"code\" in that chromosome",
            "multiValue": false
          }
        ],
        "multiValue": true
      }
    ],
    "multiValue": false
  },
  {
    "name": "familyRelatedness",
    "required": false,
    "type": "OBJECT",
    "description": "Within-family relatedness",
    "variableSet": [
      {
        "name": "relatedness",
        "required": false,
        "type": "OBJECT",
        "description": "Pairwise relatedness within the family. One entry per pair of samples",
        "variableSet": [
          {
            "name": "sampleId1",
            "required": false,
            "type": "TEXT",
            "description": "Sample Id of one of the samples in the pair",
            "multiValue": false
          },
          {
            "name": "sampleId2",
            "required": false,
            "type": "TEXT",
            "description": "Sample Id of the other sample in the pair",
            "multiValue": false
          },
          {
            "name": "ibd0",
            "required": false,
            "type": "DOUBLE",
            "description": "Estimated proportion of autosome with 0 alleles shared",
            "multiValue": false
          },
          {
            "name": "ibd1",
            "required": false,
            "type": "DOUBLE",
            "description": "Estimated proportion of autosome with 1 allele shared",
            "multiValue": false
          },
          {
            "name": "ibd2",
            "required": false,
            "type": "DOUBLE",
            "description": "Estimated proportion of autosome with 2 alleles shared",
            "multiValue": false
          },
          {
            "name": "piHat",
            "required": false,
            "type": "DOUBLE",
            "description": "Estimated overall proportion of shared autosomal DNA",
            "multiValue": false
          }
        ],
        "multiValue": true
      }
    ],
    "multiValue": false
  },
  {
    "name": "evaluation",
    "required": false,
    "type": "OBJECT",
    "description": "Evaluation of the reported vs genetic information",
    "variableSet": [
      {
        "name": "coverageBasedSexCheck",
        "required": false,
        "type": "OBJECT",
        "description": "Coverage-based sex evaluation. One entry per sample",
        "variableSet": [
          {
            "name": "sampleId",
            "required": false,
            "type": "TEXT",
            "description": "Sample Id",
            "multiValue": false
          },
          {
            "name": "reportedPhenotypicSex",
            "required": false,
            "type": "CATEGORICAL",
            "description": "Reported phenotypic sex",
            "allowedValues": [
              "UNKNOWN",
              "MALE",
              "FEMALE",
              "OTHER"
            ],
            "multiValue": false
          },
          {
            "name": "reportedKaryotypicSex",
            "required": false,
            "type": "CATEGORICAL",
            "description": "Reported karyotypic sex",
            "allowedValues": [
              "UNKNOWN",
              "XX",
              "XY",
              "XO",
              "XXY",
              "XXX",
              "XXYY",
              "XXXY",
              "XXXX",
              "XYY",
              "OTHER"
            ],
            "multiValue": false
          },
          {
            "name": "inferredSexKaryotype",
            "required": false,
            "type": "CATEGORICAL",
            "description": "Inferred coverage-based sex karyotype",
            "allowedValues": [
              "UNKNOWN",
              "XX",
              "XY",
              "XO",
              "XXY",
              "XXX",
              "XXYY",
              "XXXY",
              "XXXX",
              "XYY",
              "OTHER"
            ],
            "multiValue": false
          },
          {
            "name": "sexQuery",
            "required": false,
            "type": "CATEGORICAL",
            "description": "Whether the sample is a sex query (yes, no, unknown, notTested)",
            "allowedValues": [
              "yes",
              "no",
              "unknown",
              "notTested"
            ],
            "multiValue": false
          },
          {
            "name": "comments",
            "required": false,
            "type": "TEXT",
            "description": "Comments",
            "multiValue": false
          }
        ],
        "multiValue": true
      },
      {
        "name": "mendelianInconsistenciesCheck",
        "required": false,
        "type": "OBJECT",
        "description": "Mendelian inconsitencies evaluation. One entry per sample",
        "variableSet": [
          {
            "name": "sampleId",
            "required": false,
            "type": "TEXT",
            "description": "Sample Id",
            "multiValue": false
          },
          {
            "name": "mendelianInconsistenciesQuery",
            "required": false,
            "type": "CATEGORICAL",
            "description": "Whether the sample is a Mendelian inconsistencies query (yes, no, unknown, notTested)",
            "allowedValues": [
              "yes",
              "no",
              "unknown",
              "notTested"
            ],
            "multiValue": false
          },
          {
            "name": "comments",
            "required": false,
            "type": "TEXT",
            "description": "Mendelian inconsistencies cannot always be computed for all the samples in the family (depends on family\nstructure). Specify here if this is the case or there was any other issues",
            "multiValue": false
          }
        ],
        "multiValue": true
      },
      {
        "name": "familyRelatednessCheck",
        "required": false,
        "type": "OBJECT",
        "description": "Within-family relatedness evaluation. One entry per pair of samples",
        "variableSet": [
          {
            "name": "sampleId1",
            "required": false,
            "type": "TEXT",
            "description": "Sample Id of one of the samples in the pair",
            "multiValue": false
          },
          {
            "name": "sampleId2",
            "required": false,
            "type": "TEXT",
            "description": "Sample Id of the other sample in the pair",
            "multiValue": false
          },
          {
            "name": "relationshipFromPedigree",
            "required": false,
            "type": "CATEGORICAL",
            "description": "Reported relationship from sampleId1 to sampleId2 according to the pedigree provided",
            "allowedValues": [
              "TwinsMonozygous",
              "TwinsDizygous",
              "TwinsUnknown",
              "FullSibling",
              "FullSiblingF",
              "FullSiblingM",
              "Mother",
              "Father",
              "Son",
              "Daughter",
              "ChildOfUnknownSex",
              "MaternalAunt",
              "MaternalUncle",
              "MaternalUncleOrAunt",
              "PaternalAunt",
              "PaternalUncle",
              "PaternalUncleOrAunt",
              "MaternalGrandmother",
              "PaternalGrandmother",
              "MaternalGrandfather",
              "PaternalGrandfather",
              "DoubleFirstCousin",
              "MaternalCousinSister",
              "PaternalCousinSister",
              "MaternalCousinBrother",
              "PaternalCousinBrother",
              "Cousin",
              "Spouse",
              "Other",
              "RelationIsNotClear",
              "Unrelated",
              "Unknown"
            ],
            "multiValue": false
          },
          {
            "name": "possibleRelationship",
            "required": false,
            "type": "TEXT",
            "description": "Expected relationship according to IBD",
            "multiValue": false
          },
          {
            "name": "withinFamilyIBDQuery",
            "required": false,
            "type": "CATEGORICAL",
            "description": "Whether the pair of samples are a within-family query (yes, no, unknown, notTested)",
            "allowedValues": [
              "yes",
              "no",
              "unknown",
              "notTested"
            ],
            "multiValue": false
          },
          {
            "name": "comments",
            "required": false,
            "type": "TEXT",
            "description": "Comments",
            "multiValue": false
          }
        ],
        "multiValue": true
      },
      {
        "name": "ReportedVsGeneticOutcome",
        "required": false,
        "type": "CATEGORICAL",
        "description": "Final evaluation summary. Does the family passes RvsG checks or errors are present?",
        "allowedValues": [
          "familyPassesGvsRChecks",
          "familyFailsACheck",
          "familyMissingACheck"
        ],
        "multiValue": false
      }
    ],
    "multiValue": false
  }
]