[
  {
    "name": "gravidaNumber",
    "required": false,
    "type": "INTEGER",
    "description": "Gravida Number - Gravida indicates the number of times a woman is or has been pregnant, regardless of the pregnancy outcome.",
    "multiValue": false
  },
  {
    "name": "dateAndTimeOfDelivery",
    "required": false,
    "type": "TEXT",
    "description": "String capturing the date and time of the delivery in ISO 8601 format with timezone and no microsecond.\n        format: %Y-%m-%dT%H:%M:%S+00:00\n        e.g.: 2020-03-20T14:31:43+01:00",
    "multiValue": false
  },
  {
    "name": "parityNumber",
    "required": false,
    "type": "INTEGER",
    "description": "Parity Number - Parity, or \"para\", indicates the number of births (including live births and stillbirths) where pregnancies reached viable gestational age.",
    "multiValue": false
  },
  {
    "name": "numberOfBabiesDelivered",
    "required": false,
    "type": "INTEGER",
    "description": "Number of babies delivered in this pregnancy.",
    "multiValue": false
  },
  {
    "name": "pregnancyTermWeeks",
    "required": false,
    "type": "INTEGER",
    "description": "Pregnancy Term in weeks.",
    "multiValue": false
  },
  {
    "name": "pretermLabour",
    "required": false,
    "type": "CATEGORICAL",
    "description": "Preterm labour if any.",
    "allowedValues": [
      "IDIOPATHIC",
      "CHORIOAMNIONITIS",
      "ABRUPTION",
      "STRETCH"
    ],
    "multiValue": true
  },
  {
    "name": "inducedBirth",
    "required": false,
    "type": "CATEGORICAL",
    "description": "Induced birth if any.",
    "allowedValues": [
      "PROSTAGLANDIN",
      "BALLOON",
      "ARTIFICIAL_RUPTURE_OF_MEMBRANES",
      "ARTIFICIAL_RUPTURE_OF_MEMBRANES_MEMBRANE_SWEEP",
      "OXYTOCIN"
    ],
    "multiValue": true
  },
  {
    "name": "modeOfDelivery",
    "required": false,
    "type": "CATEGORICAL",
    "description": "Mode of delivery.",
    "allowedValues": [
      "SPONTANEOUS_VAGINAL",
      "INSTRUMENTAL_BIRTH",
      "OTHER_ASSISTED_DELIVERY",
      "CAESAREAN_SECTION_ELECTIVE",
      "CAESAREAN_SECTION_EMERGENCY",
      "CAESAREAN_SECTION_CATEGORY_1",
      "CAESAREAN_SECTION_CATEGORY_2",
      "CAESAREAN_SECTION_CATEGORY_3_AND_4"
    ],
    "multiValue": true
  },
  {
    "name": "membraneStatus",
    "required": false,
    "type": "CATEGORICAL",
    "description": "Membrane status.",
    "allowedValues": [
      "ARTIFICIAL_RUPTURE_FOR_INDUCTION_OF_LABOUR",
      "BEFORE_LABOUR",
      "DURING_LABOUR",
      "AT_BIRTH"
    ],
    "multiValue": true
  },
  {
    "name": "deliveryComplications",
    "required": false,
    "type": "CATEGORICAL",
    "description": "Delivery Complications.",
    "allowedValues": [
      "PLACENTA_ABRUPTION",
      "ABNORMAL_PLACENTATION",
      "PLACENTA_PRAEVIA",
      "ABNORMALLY_INVASIVE_PLACENTA",
      "CORD_RUPTURE",
      "INFECTION",
      "MANUAL_REMOVAL_OF_PLACENTA"
    ],
    "multiValue": true
  },
  {
    "name": "thirdStageOfLabour",
    "required": false,
    "type": "CATEGORICAL",
    "description": "Third stage - to establish if type of third stage impacts on the quantity of cord blood available for whole genome sequencing.",
    "allowedValues": [
      "PHYSIOLOGICAL",
      "ACTIVE",
      "REMOVAL_AT_CAESAREAN_SECTION"
    ],
    "multiValue": true
  },
  {
    "name": "durationOfLabour",
    "required": false,
    "type": "OBJECT",
    "description": "Approximate duration of labour duration (this is the onset of regular contractions until delivery of placenta).",
    "variableSet": [
      {
        "name": "firstStageHourDuration",
        "required": false,
        "type": "DOUBLE",
        "description": "First stage (from 3cm and regular contractions) in hours.",
        "multiValue": false
      },
      {
        "name": "secondStageHourDuration",
        "required": false,
        "type": "DOUBLE",
        "description": "Second stage (pushing) in hours.",
        "multiValue": false
      },
      {
        "name": "thirdStageMinuteDuration",
        "required": false,
        "type": "INTEGER",
        "description": "Third stage (delivery of placenta) in minutes.",
        "multiValue": false
      },
      {
        "name": "dateAndTimeOfLabourOnset",
        "required": false,
        "type": "TEXT",
        "description": "Date and time of labour onset.\n        String in ISO 8601 format with timezone and no microsecond.\n        format: %Y-%m-%dT%H:%M:%S+00:00\n        e.g.: 2020-03-20T14:31:43+01:00",
        "multiValue": false
      }
    ],
    "multiValue": false
  },
  {
    "name": "babyOralIntakeQuestions",
    "required": false,
    "type": "OBJECT",
    "description": "Baby oral intake questions before sample (e.g. buccal swab, buccal sponge) was collected.",
    "variableSet": [
      {
        "name": "neverBeenFed",
        "required": true,
        "type": "BOOLEAN",
        "description": "Has the baby been fed or taken medication before sample was collected.",
        "multiValue": false
      },
      {
        "name": "minutesSinceOralIntake",
        "required": false,
        "type": "INTEGER",
        "description": "Time since last oral intake (feed or medication) in minutes. NOTE: Only fill in if baby was fed before sample collection.",
        "multiValue": false
      },
      {
        "name": "babyIntake",
        "required": false,
        "type": "CATEGORICAL",
        "description": "Type of last intake (feed or medication). NOTE: Only fill in if baby was fed before sample collection.",
        "allowedValues": [
          "MATERNAL_BREAST_MILK",
          "DONOR_BREAST_MILK",
          "INFANT_FORMULA",
          "MEDICATION"
        ],
        "multiValue": false
      }
    ],
    "multiValue": false
  },
  {
    "name": "mumsTimeSinceLastOralIntake",
    "required": false,
    "type": "INTEGER",
    "description": "How long ago (in minutes) since mum had oral intake of food or fluids.",
    "multiValue": false
  }
]