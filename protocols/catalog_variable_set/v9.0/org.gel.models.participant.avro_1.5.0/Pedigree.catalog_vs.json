[
  {
    "name": "versionControl",
    "required": false,
    "type": "OBJECT",
    "description": "Model version number",
    "variableSet": [
      {
        "name": "GitVersionControl",
        "required": true,
        "type": "TEXT",
        "description": "This is the version for the entire set of data models as referred to the Git release tag",
        "multiValue": false,
        "defaultValue": "1.5.0"
      }
    ],
    "multiValue": false
  },
  {
    "name": "LDPCode",
    "required": false,
    "type": "TEXT",
    "description": "LDP Code (Local Delivery Partner)",
    "multiValue": false
  },
  {
    "name": "familyId",
    "required": true,
    "type": "TEXT",
    "description": "Family identifier which internally translate to a sample set",
    "multiValue": false
  },
  {
    "name": "members",
    "required": true,
    "type": "OBJECT",
    "description": "List of members of a pedigree",
    "variableSet": [
      {
        "name": "pedigreeId",
        "required": false,
        "type": "INTEGER",
        "description": "Numbering used to refer to each member of the pedigree",
        "multiValue": false
      },
      {
        "name": "isProband",
        "required": false,
        "type": "BOOLEAN",
        "description": "If this field is true, the member should be considered the proband of this family",
        "multiValue": false
      },
      {
        "name": "participantId",
        "required": false,
        "type": "TEXT",
        "description": "participantId.  This is the human readable ID in GMS",
        "multiValue": false
      },
      {
        "name": "participantQCState",
        "required": false,
        "type": "CATEGORICAL",
        "description": "participantQCState",
        "allowedValues": [
          "noState",
          "passedMedicalReviewReadyForInterpretation",
          "passedMedicalReviewNotReadyForInterpretation",
          "queryToGel",
          "queryToGMC",
          "failed"
        ],
        "multiValue": false
      },
      {
        "name": "gelSuperFamilyId",
        "required": false,
        "type": "TEXT",
        "description": "superFamily id, this id is built as a concatenation of all\nfamilies id in this superfamily i.e, fam10024_fam100457.\nNOTE: Retired for GMS",
        "multiValue": false
      },
      {
        "name": "sex",
        "required": true,
        "type": "CATEGORICAL",
        "description": "Sex of the Participant",
        "allowedValues": [
          "MALE",
          "FEMALE",
          "UNKNOWN"
        ],
        "multiValue": false
      },
      {
        "name": "personKaryotypicSex",
        "required": false,
        "type": "CATEGORICAL",
        "description": "Karyotypic sex of the participant as previously established or by looking at the GEL genome",
        "allowedValues": [
          "UNKNOWN",
          "XX",
          "XY",
          "XO",
          "XXY",
          "XXX",
          "XXYY",
          "XXXY",
          "XXXX",
          "XYY",
          "OTHER"
        ],
        "multiValue": false
      },
      {
        "name": "yearOfBirth",
        "required": false,
        "type": "INTEGER",
        "description": "Year of Birth",
        "multiValue": false
      },
      {
        "name": "fatherId",
        "required": false,
        "type": "INTEGER",
        "description": "refers to the pedigreeId of the father\nId of the parent, if unknown then no parent is referenced. Parents may need to be entered even if no data is known\nabout them in order to unambiguously reconstruct the pedigree.",
        "multiValue": false
      },
      {
        "name": "motherId",
        "required": false,
        "type": "INTEGER",
        "description": "refers to the pedigreeId of the mother\nId of the parent, if unknown then no parent is referenced. Parents may need to be entered even if no data is known\nabout them in order to unambiguously reconstruct the pedigree.",
        "multiValue": false
      },
      {
        "name": "superFatherId",
        "required": false,
        "type": "INTEGER",
        "description": "this id is built using the original familyId and the original pedigreeId of the father\nNOTE: retired for GMS",
        "multiValue": false
      },
      {
        "name": "superMotherId",
        "required": false,
        "type": "INTEGER",
        "description": "this id is built using the original familyId and the original pedigreeId of the mother\nNOTE: Retired for GMS",
        "multiValue": false
      },
      {
        "name": "twinGroup",
        "required": false,
        "type": "INTEGER",
        "description": "Each twin group is numbered, i.e. all members of a group of multiparous births receive the same number",
        "multiValue": false
      },
      {
        "name": "monozygotic",
        "required": false,
        "type": "CATEGORICAL",
        "description": "A property of the twinning group but should be entered for all members",
        "allowedValues": [
          "yes",
          "no",
          "unknown"
        ],
        "multiValue": false
      },
      {
        "name": "adoptedStatus",
        "required": false,
        "type": "CATEGORICAL",
        "description": "Adopted Status",
        "allowedValues": [
          "notadopted",
          "adoptedin",
          "adoptedout"
        ],
        "multiValue": false
      },
      {
        "name": "lifeStatus",
        "required": false,
        "type": "CATEGORICAL",
        "description": "Life Status",
        "allowedValues": [
          "ALIVE",
          "ABORTED",
          "DECEASED",
          "UNBORN",
          "STILLBORN",
          "MISCARRIAGE"
        ],
        "multiValue": false
      },
      {
        "name": "consanguineousParents",
        "required": false,
        "type": "CATEGORICAL",
        "description": "The parents of this participant has a consanguineous relationship",
        "allowedValues": [
          "yes",
          "no",
          "unknown"
        ],
        "multiValue": false
      },
      {
        "name": "affectionStatus",
        "required": false,
        "type": "CATEGORICAL",
        "description": "Affection Status",
        "allowedValues": [
          "UNAFFECTED",
          "AFFECTED",
          "UNCERTAIN"
        ],
        "multiValue": false
      },
      {
        "name": "disorderList",
        "required": false,
        "type": "OBJECT",
        "description": "Clinical Data (disorders). If the family member is unaffected as per affectionStatus then this list is empty.",
        "variableSet": [
          {
            "name": "diseaseGroup",
            "required": false,
            "type": "TEXT",
            "description": "This is Level2 Title for this disorder",
            "multiValue": false
          },
          {
            "name": "diseaseSubGroup",
            "required": false,
            "type": "TEXT",
            "description": "This is Level3 Title for this disorder",
            "multiValue": false
          },
          {
            "name": "specificDisease",
            "required": false,
            "type": "TEXT",
            "description": "This is Level4 Title for this disorder.\nIn GMS, this is the clinicalIndicationFullName/clinicalIndicationCode.",
            "multiValue": false
          },
          {
            "name": "ageOfOnset",
            "required": false,
            "type": "DOUBLE",
            "description": "Age of onset in years",
            "multiValue": false
          }
        ],
        "multiValue": true
      },
      {
        "name": "hpoTermList",
        "required": false,
        "type": "OBJECT",
        "description": "Clinical Data (HPO terms)",
        "variableSet": [
          {
            "name": "term",
            "required": true,
            "type": "TEXT",
            "description": "Identifier of the HPO term",
            "multiValue": false
          },
          {
            "name": "termPresence",
            "required": false,
            "type": "CATEGORICAL",
            "description": "This is whether the term is present in the participant (default is unknown) yes=term is present in participant,\nno=term is not present",
            "allowedValues": [
              "yes",
              "no",
              "unknown"
            ],
            "multiValue": false
          },
          {
            "name": "hpoBuildNumber",
            "required": false,
            "type": "TEXT",
            "description": "hpoBuildNumber",
            "multiValue": false
          },
          {
            "name": "modifiers",
            "required": false,
            "type": "OBJECT",
            "description": "Modifier associated with the HPO term",
            "variableSet": [
              {
                "name": "laterality",
                "required": false,
                "type": "CATEGORICAL",
                "description": "",
                "allowedValues": [
                  "RIGHT",
                  "UNILATERAL",
                  "BILATERAL",
                  "LEFT"
                ],
                "multiValue": false
              },
              {
                "name": "progression",
                "required": false,
                "type": "CATEGORICAL",
                "description": "",
                "allowedValues": [
                  "PROGRESSIVE",
                  "NONPROGRESSIVE"
                ],
                "multiValue": false
              },
              {
                "name": "severity",
                "required": false,
                "type": "CATEGORICAL",
                "description": "",
                "allowedValues": [
                  "BORDERLINE",
                  "MILD",
                  "MODERATE",
                  "SEVERE",
                  "PROFOUND"
                ],
                "multiValue": false
              },
              {
                "name": "spatialPattern",
                "required": false,
                "type": "CATEGORICAL",
                "description": "",
                "allowedValues": [
                  "DISTAL",
                  "GENERALIZED",
                  "LOCALIZED",
                  "PROXIMAL"
                ],
                "multiValue": false
              },
              {
                "name": "hpoModifierCode",
                "required": false,
                "type": "TEXT",
                "description": "",
                "multiValue": false
              },
              {
                "name": "hpoModifierVersion",
                "required": false,
                "type": "TEXT",
                "description": "",
                "multiValue": false
              }
            ],
            "multiValue": true
          },
          {
            "name": "ageOfOnset",
            "required": false,
            "type": "CATEGORICAL",
            "description": "Age of onset in months",
            "allowedValues": [
              "EMBRYONAL_ONSET",
              "FETAL_ONSET",
              "NEONATAL_ONSET",
              "INFANTILE_ONSET",
              "CHILDHOOD_ONSET",
              "JUVENILE_ONSET",
              "YOUNG_ADULT_ONSET",
              "LATE_ONSET",
              "MIDDLE_AGE_ONSET"
            ],
            "multiValue": false
          }
        ],
        "multiValue": true
      },
      {
        "name": "ancestries",
        "required": false,
        "type": "OBJECT",
        "description": "Participant's ancestries, defined as Mother's/Father's Ethnic Origin and Chi-square test for goodness of fit of this sample to 1000 Genomes Phase 3 populations.\nNOTE: for GMS this field has been deprecated in favour of clinicalEthnicities",
        "variableSet": [
          {
            "name": "mothersEthnicOrigin",
            "required": false,
            "type": "CATEGORICAL",
            "description": "Mother's Ethnic Origin",
            "allowedValues": [
              "D",
              "E",
              "F",
              "G",
              "A",
              "B",
              "C",
              "L",
              "M",
              "N",
              "H",
              "J",
              "K",
              "P",
              "S",
              "R",
              "Z"
            ],
            "multiValue": false
          },
          {
            "name": "mothersOtherRelevantAncestry",
            "required": false,
            "type": "TEXT",
            "description": "Mother's Ethnic Origin Description",
            "multiValue": false
          },
          {
            "name": "fathersEthnicOrigin",
            "required": false,
            "type": "CATEGORICAL",
            "description": "Father's Ethnic Origin",
            "allowedValues": [
              "D",
              "E",
              "F",
              "G",
              "A",
              "B",
              "C",
              "L",
              "M",
              "N",
              "H",
              "J",
              "K",
              "P",
              "S",
              "R",
              "Z"
            ],
            "multiValue": false
          },
          {
            "name": "fathersOtherRelevantAncestry",
            "required": false,
            "type": "TEXT",
            "description": "Father's Ethnic Origin Description",
            "multiValue": false
          },
          {
            "name": "chiSquare1KGenomesPhase3Pop",
            "required": false,
            "type": "OBJECT",
            "description": "Chi-square test for goodness of fit of this sample to 1000 Genomes Phase 3 populations",
            "variableSet": [
              {
                "name": "kgSuperPopCategory",
                "required": true,
                "type": "CATEGORICAL",
                "description": "1K Super Population",
                "allowedValues": [
                  "AFR",
                  "AMR",
                  "EAS",
                  "EUR",
                  "SAS"
                ],
                "multiValue": false
              },
              {
                "name": "kgPopCategory",
                "required": false,
                "type": "CATEGORICAL",
                "description": "1K Population",
                "allowedValues": [
                  "ACB",
                  "ASW",
                  "BEB",
                  "CDX",
                  "CEU",
                  "CHB",
                  "CHS",
                  "CLM",
                  "ESN",
                  "FIN",
                  "GBR",
                  "GIH",
                  "GWD",
                  "IBS",
                  "ITU",
                  "JPT",
                  "KHV",
                  "LWK",
                  "MSL",
                  "MXL",
                  "PEL",
                  "PJL",
                  "PUR",
                  "STU",
                  "TSI",
                  "YRI"
                ],
                "multiValue": false
              },
              {
                "name": "chiSquare",
                "required": true,
                "type": "DOUBLE",
                "description": "Chi-square test for goodness of fit of this sample to this 1000 Genomes Phase 3 population",
                "multiValue": false
              }
            ],
            "multiValue": true
          }
        ],
        "multiValue": false
      },
      {
        "name": "consentStatus",
        "required": false,
        "type": "OBJECT",
        "description": "What has this participant consented to?\nA participant that has been consented to the programme should also have sequence data associated with them; however\nthis needs to be programmatically checked",
        "variableSet": [
          {
            "name": "programmeConsent",
            "required": true,
            "type": "BOOLEAN",
            "description": "Is this individual consented to the programme?\nIt could simply be a family member that is not consented but for whom affection status is known",
            "multiValue": false,
            "defaultValue": false
          },
          {
            "name": "primaryFindingConsent",
            "required": true,
            "type": "BOOLEAN",
            "description": "Consent for feedback of primary findings?",
            "multiValue": false,
            "defaultValue": false
          },
          {
            "name": "secondaryFindingConsent",
            "required": true,
            "type": "BOOLEAN",
            "description": "Consent for secondary finding lookup",
            "multiValue": false,
            "defaultValue": false
          },
          {
            "name": "carrierStatusConsent",
            "required": true,
            "type": "BOOLEAN",
            "description": "Consent for carrier status check?",
            "multiValue": false,
            "defaultValue": false
          }
        ],
        "multiValue": false
      },
      {
        "name": "testConsentStatus",
        "required": false,
        "type": "OBJECT",
        "description": "What has this participant consented in the context of a Genomic Test?",
        "variableSet": [
          {
            "name": "programmeConsent",
            "required": true,
            "type": "CATEGORICAL",
            "description": "Is this individual consented to the programme? It could simply be a family member that is not consented\nbut for whom affection status is known",
            "allowedValues": [
              "yes",
              "no",
              "undefined",
              "not_applicable"
            ],
            "multiValue": false
          },
          {
            "name": "primaryFindingConsent",
            "required": true,
            "type": "CATEGORICAL",
            "description": "Consent for feedback of primary findings?\nRD: Primary Findings\nCancer: PrimaryFindings is somatic + pertinent germline findings",
            "allowedValues": [
              "yes",
              "no",
              "undefined",
              "not_applicable"
            ],
            "multiValue": false
          },
          {
            "name": "researchConsent",
            "required": true,
            "type": "CATEGORICAL",
            "description": "Research Consent",
            "allowedValues": [
              "yes",
              "no",
              "undefined",
              "not_applicable"
            ],
            "multiValue": false
          },
          {
            "name": "healthRelatedFindingConsent",
            "required": true,
            "type": "CATEGORICAL",
            "description": "Consent for secondary health related findings?",
            "allowedValues": [
              "yes",
              "no",
              "undefined",
              "not_applicable"
            ],
            "multiValue": false
          },
          {
            "name": "carrierStatusConsent",
            "required": true,
            "type": "CATEGORICAL",
            "description": "Consent for carrier status check?",
            "allowedValues": [
              "yes",
              "no",
              "undefined",
              "not_applicable"
            ],
            "multiValue": false
          },
          {
            "name": "pharmacogenomicsFindingConsent",
            "required": true,
            "type": "CATEGORICAL",
            "description": "Consent for pharmacogenomics consent as secondary findings?",
            "allowedValues": [
              "yes",
              "no",
              "undefined",
              "not_applicable"
            ],
            "multiValue": false
          }
        ],
        "multiValue": false
      },
      {
        "name": "samples",
        "required": false,
        "type": "OBJECT",
        "description": "This is an array containing all the samples that belong to this individual, e.g [\"LP00002255_GA4\"]",
        "variableSet": [
          {
            "name": "sampleId",
            "required": true,
            "type": "TEXT",
            "description": "Sample identifier (e.g, LP00012645_5GH))",
            "multiValue": false
          },
          {
            "name": "labSampleId",
            "required": true,
            "type": "TEXT",
            "description": "Lab sample identifier",
            "multiValue": false
          },
          {
            "name": "LDPCode",
            "required": false,
            "type": "TEXT",
            "description": "LDP Code (Local Delivery Partner)",
            "multiValue": false
          },
          {
            "name": "source",
            "required": false,
            "type": "CATEGORICAL",
            "description": "Source of the sample",
            "allowedValues": [
              "AMNIOTIC_FLUID",
              "BLOOD",
              "BLOOD_CAPILLARY_HEEL_PRICK",
              "BLOOD_CLOT",
              "BLOOD_CORD",
              "BLOOD_DRIED_BLOOD_SPOT",
              "BONE_MARROW",
              "BONE_MARROW_ASPIRATE_TUMOUR_CELLS",
              "BONE_MARROW_ASPIRATE_TUMOUR_SORTED_CELLS",
              "BUCCAL_SPONGE",
              "BUCCAL_SWAB",
              "BUFFY_COAT",
              "CHORIONIC_VILLUS_SAMPLE",
              "FIBROBLAST",
              "FLUID",
              "FRESH_TISSUE_IN_CULTURE_MEDIUM",
              "OTHER",
              "SALIVA",
              "TISSUE",
              "TUMOUR",
              "URINE"
            ],
            "multiValue": false
          },
          {
            "name": "product",
            "required": false,
            "type": "CATEGORICAL",
            "description": "Product of the sample",
            "allowedValues": [
              "DNA",
              "RNA"
            ],
            "multiValue": false
          },
          {
            "name": "preparationMethod",
            "required": false,
            "type": "CATEGORICAL",
            "description": "Preparation method\nNOTE: In GMS, this has been deprecated in favour of Method and storageMedium",
            "allowedValues": [
              "ASPIRATE",
              "CD128_SORTED_CELLS",
              "CD138_SORTED_CELLS",
              "EDTA",
              "FF",
              "FFPE",
              "LI_HEP",
              "ORAGENE"
            ],
            "multiValue": false
          },
          {
            "name": "programmePhase",
            "required": false,
            "type": "CATEGORICAL",
            "description": "Genomics England programme phase",
            "allowedValues": [
              "CRUK",
              "OXFORD",
              "CLL",
              "IIP",
              "MAIN",
              "EXPT"
            ],
            "multiValue": false
          },
          {
            "name": "clinicalSampleDateTime",
            "required": false,
            "type": "TEXT",
            "description": "The time when the sample was received. In the format YYYY-MM-DDTHH:MM:SS+0000",
            "multiValue": false
          },
          {
            "name": "participantId",
            "required": false,
            "type": "TEXT",
            "description": "",
            "multiValue": false
          },
          {
            "name": "participantUid",
            "required": false,
            "type": "TEXT",
            "description": "Participant UId of the sample",
            "multiValue": false
          },
          {
            "name": "sampleUid",
            "required": false,
            "type": "TEXT",
            "description": "",
            "multiValue": false
          },
          {
            "name": "maskedPid",
            "required": false,
            "type": "TEXT",
            "description": "",
            "multiValue": false
          },
          {
            "name": "method",
            "required": false,
            "type": "CATEGORICAL",
            "description": "In GMS, this is how the sample was extracted from the participant",
            "allowedValues": [
              "ASPIRATE",
              "BIOPSY",
              "NOT_APPLICABLE",
              "RESECTION",
              "SORTED_OTHER",
              "UNKNOWN",
              "UNSORTED",
              "CD138_SORTED"
            ],
            "multiValue": false
          },
          {
            "name": "storageMedium",
            "required": false,
            "type": "CATEGORICAL",
            "description": "In GMS, this is what solvent/medium the sample was stored in",
            "allowedValues": [
              "EDTA",
              "FF",
              "LI_HEP",
              "ORAGENE",
              "FFPE",
              "PAXGENE",
              "RNALater"
            ],
            "multiValue": false
          },
          {
            "name": "sampleType",
            "required": false,
            "type": "TEXT",
            "description": "In GMS, this is the sampleType as entered by the clinician in TOMs",
            "multiValue": false
          },
          {
            "name": "sampleState",
            "required": false,
            "type": "TEXT",
            "description": "In GMS, this is the sampleState as entered by the clinician in TOMs",
            "multiValue": false
          },
          {
            "name": "sampleCollectionOperationsQuestions",
            "required": false,
            "type": "OBJECT",
            "description": "In Newborns BaMMs, this is operational sample collection questions",
            "variableSet": [
              {
                "name": "easeOfSampleCollection",
                "required": false,
                "type": "CATEGORICAL",
                "description": "Ease of successful sample collection",
                "allowedValues": [
                  "EASY",
                  "DIFFICULT",
                  "NOT_APPLICABLE"
                ],
                "multiValue": false
              },
              {
                "name": "minutes",
                "required": false,
                "type": "INTEGER",
                "description": "Time taken to collect the sample (from opening collection kit to completion) in minutes",
                "multiValue": false
              }
            ],
            "multiValue": false
          }
        ],
        "multiValue": true
      },
      {
        "name": "inbreedingCoefficient",
        "required": false,
        "type": "OBJECT",
        "description": "Inbreeding Coefficient Estimation",
        "variableSet": [
          {
            "name": "sampleId",
            "required": true,
            "type": "TEXT",
            "description": "This is the sample id against which the coefficient was estimated",
            "multiValue": false
          },
          {
            "name": "program",
            "required": true,
            "type": "TEXT",
            "description": "Name of program used to calculate the coefficient",
            "multiValue": false
          },
          {
            "name": "version",
            "required": true,
            "type": "TEXT",
            "description": "Version of the programme",
            "multiValue": false
          },
          {
            "name": "estimationMethod",
            "required": true,
            "type": "TEXT",
            "description": "Where various methods for estimation exist, which method was used.",
            "multiValue": false
          },
          {
            "name": "coefficient",
            "required": true,
            "type": "DOUBLE",
            "description": "Inbreeding coefficient ideally a real number in [0,1]",
            "multiValue": false
          },
          {
            "name": "standardError",
            "required": false,
            "type": "DOUBLE",
            "description": "Standard error of the Inbreeding coefficient",
            "multiValue": false
          }
        ],
        "multiValue": false
      },
      {
        "name": "additionalInformation",
        "required": false,
        "type": "OBJECT",
        "description": "We could add a map here to store additional information for example URIs to images, ECGs, etc\nNull by default",
        "multiValue": false
      },
      {
        "name": "lastMenstrualPeriod",
        "required": false,
        "type": "TEXT",
        "description": "Last Menstrual Period",
        "multiValue": false
      },
      {
        "name": "diagnosticDetails",
        "required": false,
        "type": "OBJECT",
        "description": "Additional set of diagnostic ontology terms",
        "variableSet": [
          {
            "name": "diagnosisCodingSystem",
            "required": true,
            "type": "TEXT",
            "description": "Diagnosis coding system",
            "multiValue": false
          },
          {
            "name": "diagnosisCodingSystemVersion",
            "required": false,
            "type": "TEXT",
            "description": "Diagnosis Coding System Version",
            "multiValue": false
          },
          {
            "name": "diagnosisCode",
            "required": true,
            "type": "TEXT",
            "description": "Diagnosis Code",
            "multiValue": false
          },
          {
            "name": "diagnosisDescription",
            "required": false,
            "type": "TEXT",
            "description": "Diagnosis description",
            "multiValue": false
          },
          {
            "name": "diagnosisCertainty",
            "required": false,
            "type": "TEXT",
            "description": "Diagnosis Certainty",
            "multiValue": false
          },
          {
            "name": "ageAtOnsetInYears",
            "required": false,
            "type": "DOUBLE",
            "description": "Age at diagnosis",
            "multiValue": false
          }
        ],
        "multiValue": true
      },
      {
        "name": "participantUid",
        "required": false,
        "type": "TEXT",
        "description": "ParticipantGUID in GMS",
        "multiValue": false
      },
      {
        "name": "clinicalEthnicities",
        "required": false,
        "type": "CATEGORICAL",
        "description": "ClinicalEthnicity as defined for GMS",
        "allowedValues": [
          "A",
          "B",
          "C",
          "C2",
          "C3",
          "CA",
          "CB",
          "CC",
          "CD",
          "CE",
          "CF",
          "CG",
          "CH",
          "CJ",
          "CK",
          "CL",
          "CM",
          "CN",
          "CP",
          "CQ",
          "CR",
          "CS",
          "CT",
          "CU",
          "CV",
          "CW",
          "CX",
          "CY",
          "D",
          "E",
          "F",
          "G",
          "GA",
          "GB",
          "GC",
          "GD",
          "GE",
          "GF",
          "H",
          "J",
          "K",
          "L",
          "LA",
          "LB",
          "LC",
          "LD",
          "LE",
          "LF",
          "LG",
          "LH",
          "LJ",
          "LK",
          "M",
          "N",
          "P",
          "PA",
          "PB",
          "PC",
          "PD",
          "PE",
          "R",
          "S",
          "S1",
          "S2",
          "SA",
          "SB",
          "SC",
          "SD",
          "SE",
          "Z"
        ],
        "multiValue": true
      },
      {
        "name": "previousTreatment",
        "required": false,
        "type": "OBJECT",
        "description": "For GMS cases, previous Treatment History",
        "variableSet": [
          {
            "name": "previousTreatmentType",
            "required": false,
            "type": "TEXT",
            "description": "",
            "multiValue": false
          },
          {
            "name": "previousTreatmentName",
            "required": false,
            "type": "TEXT",
            "description": "",
            "multiValue": false
          },
          {
            "name": "previousTreatmentDate",
            "required": false,
            "type": "OBJECT",
            "description": "",
            "variableSet": [
              {
                "name": "year",
                "required": true,
                "type": "INTEGER",
                "description": "Format YYYY",
                "multiValue": false
              },
              {
                "name": "month",
                "required": false,
                "type": "INTEGER",
                "description": "Format MM. e.g June is 06",
                "multiValue": false
              },
              {
                "name": "day",
                "required": false,
                "type": "INTEGER",
                "description": "Format DD e.g. 12th of October is 12",
                "multiValue": false
              }
            ],
            "multiValue": false
          }
        ],
        "multiValue": true
      }
    ],
    "multiValue": true
  },
  {
    "name": "analysisPanels",
    "required": false,
    "type": "OBJECT",
    "description": "List of panels",
    "variableSet": [
      {
        "name": "specificDisease",
        "required": true,
        "type": "TEXT",
        "description": "The specific disease that a panel tests",
        "multiValue": false
      },
      {
        "name": "panelName",
        "required": true,
        "type": "TEXT",
        "description": "The name of the panel",
        "multiValue": false
      },
      {
        "name": "panelId",
        "required": false,
        "type": "TEXT",
        "description": "Id of the panel",
        "multiValue": false
      },
      {
        "name": "panelVersion",
        "required": false,
        "type": "TEXT",
        "description": "The version of the panel",
        "multiValue": false
      },
      {
        "name": "reviewOutcome",
        "required": false,
        "type": "TEXT",
        "description": "Deprecated",
        "multiValue": false
      },
      {
        "name": "multipleGeneticOrigins",
        "required": false,
        "type": "TEXT",
        "description": "Deprecated",
        "multiValue": false
      }
    ],
    "multiValue": true
  },
  {
    "name": "diseasePenetrances",
    "required": false,
    "type": "OBJECT",
    "description": "List of disease penetrances. Moved to referralTest for GMS",
    "variableSet": [
      {
        "name": "specificDisease",
        "required": true,
        "type": "TEXT",
        "description": "The disease to which the penetrance applies",
        "multiValue": false
      },
      {
        "name": "penetrance",
        "required": true,
        "type": "CATEGORICAL",
        "description": "The penetrance",
        "allowedValues": [
          "complete",
          "incomplete"
        ],
        "multiValue": false
      }
    ],
    "multiValue": true
  },
  {
    "name": "readyForAnalysis",
    "required": true,
    "type": "BOOLEAN",
    "description": "Flag indicating if a pedigree is ready for analysis",
    "multiValue": false
  },
  {
    "name": "familyQCState",
    "required": false,
    "type": "CATEGORICAL",
    "description": "The family quality control status",
    "allowedValues": [
      "noState",
      "passedMedicalReviewReadyForInterpretation",
      "passedMedicalReviewNotReadyForInterpretation",
      "queryToGel",
      "queryToGMC",
      "failed"
    ],
    "multiValue": false
  }
]