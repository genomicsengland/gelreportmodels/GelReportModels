from protocols.migration.base_migration import BaseMigration
from protocols.protocol_8_6 import metrics as metrics_1_2_4
from protocols.protocol_8_7 import metrics as metrics_1_2_5


class MigrateMetrics124To125(BaseMigration):
    old_model = metrics_1_2_4
    new_model = metrics_1_2_5

    def migrate_variants_coverage(self, old_instance):
        """
        Migrates a metrics_1_2_4.VariantsCoverage into a metrics_1_2_5.VariantsCoverage
        :type old_instance: metrics_1_2_4.VariantsCoverage
        :rtype: metrics_1_2_5.VariantsCoverage
        """
        old_instance.raise_if_invalid()

        new_instance = self.new_model.VariantsCoverage(**old_instance.toJsonDict())

        new_instance.coverageSummary = self.convert_collection(
            things=old_instance.coverageSummary,
            migrate_function=self.migrate_coverage_summary,
        )

        new_instance.raise_if_invalid()

        return new_instance

    def migrate_exome_coverage(self, old_instance):
        """
        Migrates a metrics_1_2_4.ExomeCoverage into a metrics_1_2_5.ExomeCoverage
        :type old_instance: metrics_1_2_4.ExomeCoverage
        :rtype: metrics_1_2_5.ExomeCoverage
        """
        old_instance.raise_if_invalid()

        new_instance = self.new_model.ExomeCoverage(**old_instance.toJsonDict())

        new_instance.coverageSummary = self.convert_collection(
            things=old_instance.coverageSummary,
            migrate_function=self.migrate_coverage_summary,
        )

        new_instance.raise_if_invalid()

        return new_instance

    def migrate_whole_genome_coverage(self, old_instance):
        """
        Migrates a metrics_1_2_4.WholeGenomeCoverage into a metrics_1_2_5.WholeGenomeCoverage
        :type old_instance: metrics_1_2_4.WholeGenomeCoverage
        :rtype: metrics_1_2_5.WholeGenomeCoverage
        """
        old_instance.raise_if_invalid()

        new_instance = self.new_model.WholeGenomeCoverage(**old_instance.toJsonDict())

        new_instance.coverageSummary = self.convert_collection(
            things=old_instance.coverageSummary,
            migrate_function=self.migrate_coverage_summary,
        )

        new_instance.raise_if_invalid()

        return new_instance

    def migrate_coverage_summary(self, old_instance):
        """
        Migrates a metrics_1_2_4.CoverageSummary into a metrics_1_2_5.CoverageSummary
        :type old_instance: metrics_1_2_4.CoverageSummary
        :rtype: metrics_1_2_5.CoverageSummary
        """
        old_instance.raise_if_invalid()

        new_instance = self.new_model.CoverageSummary(
            validate=True, **old_instance.toJsonDict()
        )

        return new_instance
