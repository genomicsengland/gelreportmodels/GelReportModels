from protocols.migration.base_migration import BaseMigration
from protocols.protocol_8_6 import metrics as metrics_1_2_4
from protocols.protocol_8_7 import metrics as metrics_1_2_5


class MigrateMetrics125To124(BaseMigration):
    old_model = metrics_1_2_5
    new_model = metrics_1_2_4

    def migrate_variants_coverage(self, old_instance):
        """
        Migrates a metrics_1_2_5.VariantsCoverage into a metrics_1_2_4.VariantsCoverage
        :type old_instance: metrics_1_2_5.VariantsCoverage
        :rtype: metrics_1_2_4.VariantsCoverage
        """
        old_instance.raise_if_invalid()

        new_instance = self.new_model.VariantsCoverage(**old_instance.toJsonDict())

        new_instance.coverageSummary = self.convert_collection(
            things=old_instance.coverageSummary,
            migrate_function=self.migrate_coverage_summary,
        )

        new_instance.raise_if_invalid()

        return new_instance

    def migrate_exome_coverage(self, old_instance):
        """
        Migrates a metrics_1_2_5.ExomeCoverage into a metrics_1_2_4.ExomeCoverage
        :type old_instance: metrics_1_2_5.ExomeCoverage
        :rtype: metrics_1_2_4.ExomeCoverage
        """
        old_instance.raise_if_invalid()

        new_instance = self.new_model.ExomeCoverage(**old_instance.toJsonDict())

        new_instance.coverageSummary = self.convert_collection(
            things=old_instance.coverageSummary,
            migrate_function=self.migrate_coverage_summary,
        )

        new_instance.raise_if_invalid()

        return new_instance

    def migrate_whole_genome_coverage(self, old_instance):
        """
        Migrates a metrics_1_2_5.WholeGenomeCoverage into a metrics_1_2_4.WholeGenomeCoverage
        :type old_instance: metrics_1_2_5.WholeGenomeCoverage
        :rtype: metrics_1_2_4.WholeGenomeCoverage
        """
        old_instance.raise_if_invalid()

        new_instance = self.new_model.WholeGenomeCoverage(**old_instance.toJsonDict())

        new_instance.coverageSummary = self.convert_collection(
            things=old_instance.coverageSummary,
            migrate_function=self.migrate_coverage_summary,
        )

        new_instance.raise_if_invalid()

        return new_instance

    def migrate_coverage_summary(self, old_instance):
        """
        Migrates a metrics_1_2_5.CoverageSummary into a metrics_1_2_4.CoverageSummary
        :type old_instance: metrics_1_2_5.CoverageSummary
        :rtype: metrics_1_2_4.CoverageSummary
        """
        old_instance.raise_if_invalid()

        new_instance_dict = old_instance.toJsonDict()

        if "normstep005" in new_instance_dict:
            del new_instance_dict["normstep005"]

        if "chunksize" in new_instance_dict:
            del new_instance_dict["chunksize"]

        new_instance = self.new_model.CoverageSummary(
            validate=True, **new_instance_dict
        )

        return new_instance
