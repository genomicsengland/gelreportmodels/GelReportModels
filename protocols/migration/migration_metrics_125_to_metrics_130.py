from protocols.migration.base_migration import BaseMigration
from protocols.protocol_8_7 import metrics as metrics_1_2_5
from protocols.protocol_8_8 import metrics as metrics_1_3_0


class MigrateMetrics125To130(BaseMigration):
    old_model = metrics_1_2_5
    new_model = metrics_1_3_0

    def migrate_cancer_summary_metrics(self, old_instance):
        """
        Migrates a metrics_1_2_5.CancerSummaryMetrics into a metrics_1_3_0.CancerSummaryMetrics
        :type old_instance: metrics_1_3_0.CancerSummaryMetrics
        :rtype: metrics_1_3_0.CancerSummaryMetrics
        """
        old_instance.raise_if_invalid()

        new_dict = old_instance.toJsonDict()
        new_dict["norm_step_005_normal"] = 0
        new_dict["norm_step_005"] = 0

        new_instance = self.new_model.CancerSummaryMetrics(validate=True, **new_dict)

        return new_instance
