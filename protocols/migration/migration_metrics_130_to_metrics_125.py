from protocols.migration.base_migration import BaseMigration
from protocols.protocol_8_7 import metrics as metrics_1_2_5
from protocols.protocol_8_8 import metrics as metrics_1_3_0


class MigrateMetrics130To125(BaseMigration):
    old_model = metrics_1_3_0
    new_model = metrics_1_2_5

    def migrate_cancer_summary_metrics(self, old_instance):
        """
        Migrates a metrics_1_3_0.CancerSummaryMetrics into a metrics_1_2_5.CancerSummaryMetrics
        :type old_instance: metrics_1_3_0.CancerSummaryMetrics
        :rtype: metrics_1_2_5.CancerSummaryMetrics
        """
        old_instance.raise_if_invalid()

        new_dict = old_instance.toJsonDict()

        # local_rmsd fields are nullable in 1.3.0, but required in 1.2.5
        if not new_dict["local_rmsd"]:
            new_dict["local_rmsd"] = 0

        if not new_dict["local_rmsd_normal"]:
            new_dict["local_rmsd_normal"] = 0

        new_instance = self.new_model.CancerSummaryMetrics(validate=True, **new_dict)

        return new_instance
