from protocols.migration.base_migration import BaseMigration
from protocols.protocol_3_0_0 import reports as participant_old
from protocols.protocol_4_0_0 import participant as participant_1_0_0


class MigrationParticipants100ToReports(BaseMigration):
    old_participant = participant_1_0_0
    new_participant = participant_old

    def migrate_pedigree(self, old_instance):
        """
        :param old_instance: org.gel.models.participant.avro.Pedigree 1.0.0
        :rtype: org.gel.models.report.avro RDParticipant.Pedigree 3.0.0
        """
        new_instance = self.convert_class(self.new_participant.Pedigree, old_instance)
        new_instance.versionControl = self.new_participant.VersionControl()
        new_instance.gelFamilyId = old_instance.familyId
        new_instance.participants = self.convert_collection(
            old_instance.members,
            self._migrate_member_to_participant,
            family_id=old_instance.familyId,
        )
        return self.validate_object(
            object_to_validate=new_instance, object_type=self.new_participant.Pedigree
        )

    def _migrate_member_to_participant(self, old_member, family_id):
        new_instance = self.convert_class(
            self.new_participant.RDParticipant, old_member
        )
        new_instance.gelFamilyId = family_id
        new_instance.pedigreeId = old_member.pedigreeId or 0
        new_instance.isProband = old_member.isProband or False
        new_instance.gelId = old_member.participantId
        new_instance.sex = self._migrate_sex(old_sex=old_member.sex)
        new_instance.personKaryotipicSex = self._migrate_person_karyotypic_sex(
            old_pks=old_member.personKaryotypicSex
        )
        if old_member.yearOfBirth is not None:
            new_instance.yearOfBirth = str(old_member.yearOfBirth)
        new_instance.adoptedStatus = self._migrate_adopted_status(
            old_status=old_member.adoptedStatus
        )
        new_instance.lifeStatus = self._migrate_life_status(
            old_status=old_member.lifeStatus
        )
        new_instance.affectionStatus = self._migrate_affection_status(
            old_status=old_member.affectionStatus
        )
        new_instance.hpoTermList = self.convert_collection(
            old_member.hpoTermList, self._migrate_hpo_term, default=[]
        )
        new_instance.samples = self.convert_collection(
            old_member.samples, lambda s: s.sampleId
        )
        new_instance.versionControl = self.new_participant.VersionControl()
        if old_member.consentStatus is None:
            new_instance.consentStatus = self.new_participant.ConsentStatus(
                programmeConsent=True,
                primaryFindingConsent=True,
                secondaryFindingConsent=True,
                carrierStatusConsent=True,
            )
        if old_member.ancestries is None:
            new_instance.ancestries = self.new_participant.Ancestries()
        if old_member.consanguineousParents is None:
            new_instance.consanguineousParents = (
                self.new_participant.TernaryOption.unknown
            )
        if new_instance.disorderList is None:
            new_instance.disorderList = []
        return new_instance

    def _migrate_hpo_term(self, old_term):
        new_instance = self.convert_class(
            target_klass=self.new_participant.HpoTerm, instance=old_term
        )  # type: self.new_participant.HpoTerm
        new_instance.termPresence = self._migrate_ternary_option_to_boolean(
            ternary_option=old_term.termPresence
        )
        return new_instance

    def _migrate_ternary_option_to_boolean(self, ternary_option):
        ternary_map = {
            self.old_participant.TernaryOption.no: False,
            self.old_participant.TernaryOption.yes: True,
        }
        return ternary_map.get(ternary_option, None)

    def _migrate_affection_status(self, old_status):
        status_map = {
            self.old_participant.AffectionStatus.AFFECTED: self.new_participant.AffectionStatus.affected,
            self.old_participant.AffectionStatus.UNAFFECTED: self.new_participant.AffectionStatus.unaffected,
            self.old_participant.AffectionStatus.UNCERTAIN: self.new_participant.AffectionStatus.unknown,
        }
        return status_map.get(old_status, self.new_participant.AffectionStatus.unknown)

    def _migrate_life_status(self, old_status):
        status_map = {
            self.old_participant.LifeStatus.ABORTED: self.new_participant.LifeStatus.aborted,
            self.old_participant.LifeStatus.ALIVE: self.new_participant.LifeStatus.alive,
            self.old_participant.LifeStatus.DECEASED: self.new_participant.LifeStatus.deceased,
            self.old_participant.LifeStatus.UNBORN: self.new_participant.LifeStatus.unborn,
            self.old_participant.LifeStatus.STILLBORN: self.new_participant.LifeStatus.stillborn,
            self.old_participant.LifeStatus.MISCARRIAGE: self.new_participant.LifeStatus.miscarriage,
        }
        return status_map.get(old_status, self.new_participant.LifeStatus.alive)

    def _migrate_adopted_status(self, old_status):
        status_map = {
            self.old_participant.AdoptedStatus.notadopted: self.new_participant.AdoptedStatus.not_adopted,
            self.old_participant.AdoptedStatus.adoptedin: self.new_participant.AdoptedStatus.adoptedin,
            self.old_participant.AdoptedStatus.adoptedout: self.new_participant.AdoptedStatus.adoptedout,
        }
        return status_map.get(
            old_status, self.new_participant.AdoptedStatus.not_adopted
        )

    def _migrate_person_karyotypic_sex(self, old_pks):
        pks_map = {
            self.old_participant.PersonKaryotipicSex.UNKNOWN: self.new_participant.PersonKaryotipicSex.unknown,
            self.old_participant.PersonKaryotipicSex.XX: self.new_participant.PersonKaryotipicSex.XX,
            self.old_participant.PersonKaryotipicSex.XY: self.new_participant.PersonKaryotipicSex.XY,
            self.old_participant.PersonKaryotipicSex.XO: self.new_participant.PersonKaryotipicSex.XO,
            self.old_participant.PersonKaryotipicSex.XXY: self.new_participant.PersonKaryotipicSex.XXY,
            self.old_participant.PersonKaryotipicSex.XXX: self.new_participant.PersonKaryotipicSex.XXX,
            self.old_participant.PersonKaryotipicSex.XXYY: self.new_participant.PersonKaryotipicSex.XXYY,
            self.old_participant.PersonKaryotipicSex.XXXY: self.new_participant.PersonKaryotipicSex.XXXY,
            self.old_participant.PersonKaryotipicSex.XXXX: self.new_participant.PersonKaryotipicSex.XXXX,
            self.old_participant.PersonKaryotipicSex.XYY: self.new_participant.PersonKaryotipicSex.XYY,
            self.old_participant.PersonKaryotipicSex.OTHER: self.new_participant.PersonKaryotipicSex.other,
        }
        return pks_map.get(old_pks)

    def _migrate_sex(self, old_sex):
        sex_map = {
            self.old_participant.Sex.MALE: self.new_participant.Sex.male,
            self.old_participant.Sex.FEMALE: self.new_participant.Sex.female,
            self.old_participant.Sex.UNKNOWN: self.new_participant.Sex.unknown,
        }
        return sex_map.get(old_sex, self.new_participant.Sex.undetermined)
