"""
WARNING: This migration script will potentially cause a loss of data in labSampleId
Use with caution
 """
from protocols.migration.base_migration import BaseMigration
from protocols.protocol_7_2 import participant as participant_1_1_2
from protocols.protocol_7_2_1 import participant as participant_1_1_3


class MigrateParticipant113To112(BaseMigration):
    old_participant = participant_1_1_3
    new_participant = participant_1_1_2

    def migrate_pedigree(self, old_pedigree):
        """
        :type old_pedigree: Participant 1.1.3 Pedigree
        :rtype: Participant 1.1.2 Pedigree
        """
        new_instance = self.convert_class(
            target_klass=self.new_participant.Pedigree, instance=old_pedigree
        )
        new_instance.versionControl = self.new_participant.VersionControl()
        new_instance.members = self.convert_collection(
            old_pedigree.members, self._migrate_pedigree_member
        )
        return self.validate_object(
            object_to_validate=new_instance, object_type=self.new_participant.Pedigree
        )

    def migrate_cancer_participant(self, old_cancer_participant):
        """
        :type old_cancer_participant: Participant 1.1.3 CancerParticipant
        :rtype: Participant 1_1_2 CancerParticipant
        """
        new_instance = self.convert_class(
            target_klass=self.new_participant.CancerParticipant,
            instance=old_cancer_participant,
        )
        new_instance.versionControl = self.new_participant.VersionControl()
        new_instance.germlineSamples = self.convert_collection(
            old_cancer_participant.germlineSamples, self._migrate_germline_sample
        )
        new_instance.tumourSamples = self.convert_collection(
            old_cancer_participant.tumourSamples, self._migrate_tumour_sample
        )
        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_participant.CancerParticipant,
        )

    def _migrate_pedigree_member(self, old_pedigree_member):
        new_instance = self.convert_class(
            target_klass=self.new_participant.PedigreeMember,
            instance=old_pedigree_member,
        )
        new_instance.samples = self.convert_collection(
            old_pedigree_member.samples, self._migrate_sample
        )
        return new_instance

    def _migrate_sample(self, old_sample):
        source = self.get_valid_enum_value(
            old_sample.source, self.new_participant.SampleSource
        )
        product = self.get_valid_enum_value(
            old_sample.product, self.new_participant.Product
        )
        preparationMethod = self.get_valid_enum_value(
            old_sample.preparationMethod, self.new_participant.PreparationMethod
        )

        sample_dict = {
            "sampleId": old_sample.sampleId,
            "labSampleId": self.convert_string_to_integer(
                old_sample.labSampleId, default_value=-1, fail=False
            ),
            "source": source,
            "product": product,
            "preparationMethod": preparationMethod,
        }
        new_instance = self.new_participant.Sample.fromJsonDict(sample_dict)
        return new_instance

    def _migrate_germline_sample(self, old_sample):
        new_instance = self.convert_class(
            target_klass=self.new_participant.GermlineSample, instance=old_sample
        )
        new_instance.labSampleId = self.convert_string_to_integer(
            old_sample.labSampleId, default_value=-1, fail=False
        )
        return new_instance

    def _migrate_tumour_sample(self, old_sample):
        new_instance = self.convert_class(
            target_klass=self.new_participant.TumourSample, instance=old_sample
        )
        new_instance.labSampleId = self.convert_string_to_integer(
            old_sample.labSampleId, default_value=-1, fail=False
        )
        return new_instance
