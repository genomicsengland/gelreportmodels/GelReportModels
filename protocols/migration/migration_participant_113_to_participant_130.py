from protocols.migration.base_migration import BaseMigration
from protocols.protocol_7_2_1 import participant as participant_1_1_3
from protocols.protocol_7_7 import participant as participant_1_3_0


class MigrateParticipant113To130(BaseMigration):
    old_participant = participant_1_1_3
    new_participant = participant_1_3_0

    def migrate_pedigree(self, old_pedigree):
        """
        :type old_pedigree: Participant 1.1.3 Pedigree
        :rtype: Participant 1.3.0 Pedigree
        """
        new_instance = self.convert_class(
            target_klass=self.new_participant.Pedigree, instance=old_pedigree
        )
        new_instance.versionControl = self.new_participant.VersionControl()
        new_instance.members = self.convert_collection(
            old_pedigree.members, self._migrate_pedigree_member
        )
        return self.validate_object(
            object_to_validate=new_instance, object_type=self.new_participant.Pedigree
        )

    def _migrate_pedigree_member(self, old_pedigree_member):
        new_instance = self.convert_class(
            target_klass=self.new_participant.PedigreeMember,
            instance=old_pedigree_member,
        )
        new_instance.hpoTermList = self.convert_collection(
            old_pedigree_member.hpoTermList, self._migrate_hpo_term
        )
        new_instance.samples = self.convert_collection(
            old_pedigree_member.samples, self._migrate_germline_sample
        )
        return new_instance

    def _migrate_hpo_term(self, old_hpo_term):
        new_instance = self.convert_class(
            target_klass=self.new_participant.HpoTerm, instance=old_hpo_term
        )
        new_instance.modifiers = (
            [self.convert_class(
                target_klass=self.new_participant.HpoTermModifiers, instance=old_hpo_term.modifiers
            )] if old_hpo_term.modifiers else None
        )
        return new_instance

    def _migrate_germline_sample(self, old_sample):
        new_instance = self.convert_class(
            target_klass=self.new_participant.GermlineSample, instance=old_sample
        )
        return new_instance

    # TODO: migrations other than pedigree

    def migrate_cancer_participant(self, old_participant):
        new_instance = self.convert_class(
            target_klass=self.new_participant.CancerParticipant,
            instance=old_participant,
        )

        new_instance.versionControl = self.new_participant.VersionControl()
        # Should be moved to ReferralTest
        del old_participant.matchedSamples
        # Should be moved to ReferralTest after migration
        new_instance.tumourSamples = self.convert_collection(
            old_participant.tumourSamples, self.migrate_tumour_sample
        )

        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_participant.CancerParticipant,
        )

    def migrate_tumour_sample(self, old_sample):
        new_instance = self.convert_class(
            target_klass=self.new_participant.TumourSample, instance=old_sample
        )
        new_instance.diseaseType = self.get_valid_enum_value(
            old_sample.diseaseType, self.new_participant.diseaseType
        )
        if new_instance.sampleMorphologies:
            new_instance.sampleMorphologies.append(
                covert_removed_fields(
                    old_sample,
                    "morphologyICD",
                    old_sample.morphologyICD,
                    self.new_participant.Morphology,
                )
            )
            new_instance.sampleMorphologies.append(
                covert_removed_fields(
                    old_sample,
                    "morphologySnomedCT",
                    old_sample.morphologySnomedCT,
                    self.new_participant.Morphology,
                )
            )
            new_instance.sampleMorphologies.append(
                covert_removed_fields(
                    old_sample,
                    "morphologySnomedRT",
                    old_sample.morphologySnomedRT,
                    self.new_participant.Morphology,
                )
            )

        if new_instance.sampleTopographies:
            new_instance.sampleTopographies.append(
                covert_removed_fields(
                    old_sample,
                    "topographyICD",
                    old_sample.topographyICD,
                    self.new_participant.Topography,
                )
            )
            new_instance.sampleTopographies.append(
                covert_removed_fields(
                    old_sample,
                    "topographySnomedCT",
                    old_sample.topographySnomedCT,
                    self.new_participant.Topography,
                )
            )
            new_instance.sampleTopographies.append(
                covert_removed_fields(
                    old_sample,
                    "topographySnomedRT",
                    old_sample.topographySnomedRT,
                    self.new_participant.Topography,
                )
            )

        return new_instance


def covert_removed_fields(old_sample, field, value, model):
    if hasattr(old_sample, field) and value:
        return model.fromJsonDict({"name": field, "value": value})
