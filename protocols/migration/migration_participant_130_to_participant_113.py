from protocols.migration.base_migration import BaseMigration
from protocols.protocol_7_2_1 import participant as participant_1_1_3
from protocols.protocol_7_10 import participant as participant_1_3_0


class MigrateParticipant130To113(BaseMigration):
    old_participant = participant_1_3_0
    new_participant = participant_1_1_3

    def migrate_pedigree(self, old_pedigree):
        """
        :type old_pedigree: Participant 1.1.3 Pedigree
        :rtype: Participant 1.3.0 Pedigree
        """
        new_instance = self.convert_class(
            target_klass=self.new_participant.Pedigree, instance=old_pedigree
        )
        new_instance.versionControl = self.new_participant.VersionControl()
        new_instance.members = self.convert_collection(
            old_pedigree.members, self._migrate_pedigree_member
        )

        return self.validate_object(
            object_to_validate=new_instance, object_type=self.new_participant.Pedigree
        )

    def _migrate_pedigree_member(self, old_pedigree_member):
        new_instance = self.convert_class(
            target_klass=self.new_participant.PedigreeMember,
            instance=old_pedigree_member,
        )
        new_instance.hpoTermList = self.convert_collection(
            old_pedigree_member.hpoTermList, self._migrate_hpo_term
        )
        new_instance.samples = self.convert_collection(
            old_pedigree_member.samples, self._migrate_sample
        )
        return new_instance

    def _migrate_hpo_term(self, old_hpo_term):
        new_instance = self.convert_class(
            target_klass=self.new_participant.HpoTerm, instance=old_hpo_term
        )
        new_instance.modifiers = (
            self.convert_class(
                target_klass=self.new_participant.HpoTermModifiers,
                instance=old_hpo_term.modifiers[0],
            )
            if old_hpo_term.modifiers
            else None
        )
        return new_instance

    def _migrate_sample(self, old_sample):
        source = self.get_valid_enum_value(
            old_sample.source, self.new_participant.SampleSource
        )
        product = self.get_valid_enum_value(
            old_sample.product, self.new_participant.Product
        )
        preparationMethod = self.get_valid_enum_value(
            old_sample.preparationMethod, self.new_participant.PreparationMethod
        )

        sample_dict = {
            "sampleId": old_sample.sampleId,
            "labSampleId": old_sample.labSampleId,
            "source": source,
            "product": product,
            "preparationMethod": preparationMethod,
        }
        new_instance = self.new_participant.Sample.fromJsonDict(sample_dict)
        return new_instance

    # TODO: migrations other than pedigree

    def migrate_cancer_participant(self, old_participant, organisation_code):
        new_instance = self.convert_class(
            target_klass=self.new_participant.CancerParticipant,
            instance=old_participant,
        )
        new_instance.versionControl = self.new_participant.VersionControl()
        new_instance.matchedSamples = []

        if not (
            hasattr(old_participant, "individualId") and old_participant.individualId
        ):
            new_instance.individualId = ""

        if hasattr(old_participant, "tumourSamples") and old_participant.tumourSamples:
            new_instance.tumourSamples = self.convert_collection(
                old_participant.tumourSamples,
                self.migrate_tumour_sample,
                organisation_code=organisation_code,
            )
        else:
            new_instance.tumourSamples = []

        if (
            hasattr(old_participant, "germlineSamples")
            and old_participant.germlineSamples
        ):
            new_instance.germlineSamples = self.convert_collection(
                old_participant.germlineSamples, self.migrate_germline_sample
            )
        else:
            new_instance.germlineSamples = []

        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_participant.CancerParticipant,
        )

    # Organisation Code should come from OrganisationNgis.organisationCode
    def migrate_tumour_sample(self, old_sample, organisation_code):
        new_instance = self.convert_class(
            target_klass=self.new_participant.TumourSample, instance=old_sample
        )
        new_instance.preparationMethod = self.get_valid_enum_value(
            old_sample.preparationMethod, self.new_participant.PreparationMethod
        )
        new_instance.programmePhase = self.get_valid_enum_value(
            old_sample.programmePhase, self.new_participant.ProgrammePhase
        )
        new_instance.source = self.get_valid_enum_value(
            old_sample.source, self.new_participant.SampleSource
        )
        new_instance.tissueSource = self.get_valid_enum_value(
            old_sample.tissueSource, self.new_participant.TissueSource
        )
        new_instance.tumourType = self.get_valid_enum_value(
            old_sample.tumourType, self.new_participant.TumourType
        )

        if hasattr(old_sample, "sampleMorphologies") and old_sample.sampleMorphologies:
            for morph in old_sample.sampleMorphologies:
                if morph.name and morph.value:
                    setattr(new_instance, morph.name, morph.value)

        if hasattr(old_sample, "sampleTopographies") and old_sample.sampleTopographies:
            for top in old_sample.sampleTopographies:
                if top.name and top.value:
                    setattr(new_instance, top.name, top.value)

        if not new_instance.LDPCode:
            new_instance.LDPCode = organisation_code

        return new_instance

    def migrate_germline_sample(self, old_sample):
        new_instance = self.convert_class(
            target_klass=self.new_participant.GermlineSample, instance=old_sample
        )
        new_instance.preparationMethod = self.get_valid_enum_value(
            old_sample.preparationMethod, self.new_participant.PreparationMethod
        )
        new_instance.product = self.get_valid_enum_value(
            old_sample.product, self.new_participant.Product
        )
        new_instance.programmePhase = self.get_valid_enum_value(
            old_sample.programmePhase, self.new_participant.ProgrammePhase
        )
        new_instance.source = self.get_valid_enum_value(
            old_sample.source, self.new_participant.SampleSource
        )

        return new_instance
