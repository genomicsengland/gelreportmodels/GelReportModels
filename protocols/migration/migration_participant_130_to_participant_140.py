from protocols.migration.base_migration import BaseMigration
from protocols.protocol_7_7 import participant as participant_1_3_0
from protocols.protocol_7_11 import participant as participant_1_4_0


class MigrateParticipant130To140(BaseMigration):

    """
    Participant 140 only adds things on top of Participant 130. This means there is no actual data to migrate.
    Simply cast participant 130 as 140 and it will work.
    """

    old_participant = participant_1_3_0
    new_participant = participant_1_4_0

    def migrate_pedigree(self, old_pedigree):
        """
        Migrates a participant_1_3_0.Pedigree into a participant_1_4_0.Pedigree.

        :type old_pedigree: Participant 1.3.0 Pedigree
        :rtype: Participant 1.4.0 Pedigree
        """
        new_instance = self.convert_class(
            target_klass=self.new_participant.Pedigree, instance=old_pedigree
        )
        return self.validate_object(
            object_to_validate=new_instance, object_type=self.new_participant.Pedigree
        )

    def migrate_cancer_participant(self, old_cancer_participant):
        """
        Migrates a participant_1_3_0.CancerParticipant into a participant_1_4_0.CancerParticipant.

        :type old_cancer_participant: Participant 1.3.0 CancerParticipant
        :rtype: Participant 1.4.0 CancerParticipant
        """
        new_instance = self.convert_class(
            target_klass=self.new_participant.CancerParticipant,
            instance=old_cancer_participant,
        )
        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_participant.CancerParticipant,
        )

    def migrate_referral(self, old_referral):
        """
        Migrates a participant_1_3_0.Referral into a participant_1_4_0.Referral.

        :type old_referral: Participant 1.3.0 Referral
        :rtype: Participant 1.4.0 Referral
        """

        new_instance = self.convert_class(
            target_klass=self.new_participant.Referral, instance=old_referral
        )
        return self.validate_object(
            object_to_validate=new_instance, object_type=self.new_participant.Referral
        )
