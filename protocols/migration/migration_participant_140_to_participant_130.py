from protocols.migration.base_migration import BaseMigration
from protocols.protocol_7_7 import participant as participant_1_3_0
from protocols.protocol_7_11 import participant as participant_1_4_0


class MigrateParticipant140To130(BaseMigration):
    old_participant = participant_1_4_0
    new_participant = participant_1_3_0

    conversion_dict = {
        "BLOOD_CLOT": "BLOOD",
        "BLOOD_CORD": "BLOOD",
        "BLOOD_DRIED_BLOOD_SPOT": "BLOOD",
        "BLOOD_CAPILLARY_HEEL_PRICK": "BLOOD",
        "BUCCAL_SPONGE": "BUCCAL_SWAB",
        "BUFFY_COAT": "BLOOD",
    }

    def migrate_pedigree(self, old_pedigree):
        """
        Migrates a participant_1_4_0.Pedigree into a participant_1_3_0.Pedigree.

        :type old_pedigree: Participant 1.4.0 Pedigree
        :rtype: Participant 1.3.0 Pedigree
        """
        new_instance = self.convert_class(
            target_klass=self.new_participant.Pedigree, instance=old_pedigree
        )
        new_instance.versionControl = self.new_participant.VersionControl()
        new_instance.members = self.convert_collection(
            old_pedigree.members, self._migrate_pedigree_member
        )
        return self.validate_object(
            object_to_validate=new_instance, object_type=self.new_participant.Pedigree
        )

    def migrate_cancer_participant(self, old_cancer_participant):
        """
        Migrates a participant_1_4_0.CancerParticipant into a participant_1_3_0.CancerParticipant.

        :type old_cancer_participant: Participant 1.4.0 CancerParticipant
        :rtype: Participant 1.3.0 CancerParticipant
        """
        new_instance = self.convert_class(
            target_klass=self.new_participant.CancerParticipant,
            instance=old_cancer_participant,
        )
        new_instance.versionControl = self.new_participant.VersionControl()
        new_instance.germlineSamples = self.convert_collection(
            old_cancer_participant.germlineSamples, self._migrate_germline_sample
        )
        new_instance.tumourSamples = self.convert_collection(
            old_cancer_participant.tumourSamples, self._migrate_tumour_sample
        )
        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_participant.CancerParticipant,
        )

    def _migrate_pedigree_member(self, old_pedigree_member):
        new_instance = self.convert_class(
            target_klass=self.new_participant.PedigreeMember,
            instance=old_pedigree_member,
        )
        new_instance.samples = self.convert_collection(
            old_pedigree_member.samples, self._migrate_germline_sample
        )
        return new_instance

    def _migrate_germline_sample(self, old_sample):
        """
        Migrates a participant_1_4_0.GermlineSample into a participant_1_3_0.GermlineSample, mapping new enums to
        old ones according to the conversion_dict.

        :type old_sample: Participant 1.4.0 GermlineSample
        :rtype: Participant 1.3.0 GermlineSample
        """
        new_instance = self.convert_class(
            target_klass=self.new_participant.GermlineSample, instance=old_sample
        )
        if old_sample.source in self.conversion_dict:
            new_instance.source = self.conversion_dict[old_sample.source]
        return new_instance

    def _migrate_tumour_sample(self, old_sample):
        """
        Migrates a participant_1_4_0.TumourSample into a participant_1_3_0.TumourSample, mapping new enums to
        old ones according to the conversion_dict.

        :type old_sample: Participant 1.4.0 TumourSample
        :rtype: Participant 1.3.0 TumourSample
        """
        new_instance = self.convert_class(
            target_klass=self.new_participant.TumourSample, instance=old_sample
        )
        if old_sample.source in self.conversion_dict:
            new_instance.source = self.conversion_dict[old_sample.source]
        return new_instance

    def migrate_referral(self, old_referral):
        """
        Migrates a participant_1_4_0.Referral into a participant_1_3_0.Referral.

        :type old_referral: Participant 1.4.0 Referral
        :rtype: Participant 1.3.0 Referral
        """

        new_instance = self.convert_class(
            target_klass=self.new_participant.Referral, instance=old_referral
        )
        new_instance.versionControl = self.new_participant.VersionControl()
        new_instance.referralTests = self.convert_collection(
            old_referral.referralTests, self._migrate_referral_test
        )
        if old_referral.cancerParticipant:
            new_instance.cancerParticipant = self.migrate_cancer_participant(
                old_referral.cancerParticipant
            )
        new_instance.pedigree = self.migrate_pedigree(old_referral.pedigree)
        return self.validate_object(
            object_to_validate=new_instance, object_type=self.new_participant.Referral
        )

    def _migrate_referral_test(self, old_referral_test):
        """
        Migrates a participant_1_4_0.ReferralTest into a participant_1_3_0.ReferralTest

        :type old_referral_test: Participant 1.4.0 ReferralTest
        :rtype: Participant 1.3.0 ReferralTest
        """

        new_instance = self.convert_class(
            target_klass=self.new_participant.ReferralTest, instance=old_referral_test
        )
        new_instance.germlineSamples = self.convert_collection(
            old_referral_test.germlineSamples, self._migrate_germline_sample
        )
        new_instance.tumourSamples = self.convert_collection(
            old_referral_test.tumourSamples, self._migrate_tumour_sample
        )
        return new_instance
