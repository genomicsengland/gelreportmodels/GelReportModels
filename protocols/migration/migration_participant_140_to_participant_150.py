import logging
from protocols.protocol_7_11 import participant as participant_1_4_0
from protocols.protocol_8_0 import participant as participant_1_5_0

from protocols.migration.base_migration import BaseMigration


class MigrateParticipant140To150(BaseMigration):
    old_participant = participant_1_4_0
    new_participant = participant_1_5_0

    def migrate_germline_sample(self, old_sample):
        """Migrates a participant_1_4_0.GermlineSample into a participant_1_5_0.GermlineSample.

        :type old_pedigree: Participant 1.4.0 GermlineSample
        :rtype: Participant 1.5.0 GermlineSample
        """

        return self._migrate_storage_medium(
            old_sample, self.new_participant.GermlineSample
        )

    def migrate_tumour_sample(self, old_sample):
        """Migrates a participant_1_4_0.TumourSample into a participant_1_5_0.TumourSample.

        :type old_pedigree: Participant 1.4.0 TumourSample
        :rtype: Participant 1.5.0 TumourSample
        """

        return self._migrate_storage_medium(
            old_sample, self.new_participant.TumourSample
        )

    def _migrate_storage_medium(self, old_sample, clazz):
        """Returns migrated sample class.

        :param old_sample: _description_
        :type old_sample: _type_
        :return: Migrated sample class
        :rtype: str | None
        """

        new_instance = self.convert_class(target_klass=clazz, instance=old_sample)
        return self.validate_object(object_to_validate=new_instance, object_type=clazz)
