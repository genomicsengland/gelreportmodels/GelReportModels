from protocols.migration.base_migration import BaseMigration
from protocols.protocol_7_0 import reports as reports_6_0_0
from protocols.protocol_7_2 import reports as reports_6_0_1


class MigrateReports600To601(BaseMigration):
    old_reports = reports_6_0_0
    new_reports = reports_6_0_1

    def migrate_interpretation_request_rd(self, old_instance):
        """
        Migrates a reports_6_0_0.InterpretationRequestRD into a reports_6_0_1.InterpretationRequestRD
        :type old_instance: reports_6_0_0.InterpretationRequestRD
        :rtype: reports_6_0_1.InterpretationRequestRD
        """
        new_instance = self.convert_class(
            target_klass=self.new_reports.InterpretationRequestRD, instance=old_instance
        )
        new_instance.versionControl = self.new_reports.ReportVersionControl()
        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_reports.InterpretationRequestRD,
        )

    def migrate_clinical_report_rd(self, old_instance):
        new_instance = self.convert_class(
            target_klass=self.new_reports.ClinicalReport, instance=old_instance
        )
        new_instance.versionControl = self.new_reports.ReportVersionControl()
        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_reports.ClinicalReport,
        )

    def migrate_clinical_report_cancer(self, old_instance):
        return self.migrate_clinical_report_rd(old_instance)