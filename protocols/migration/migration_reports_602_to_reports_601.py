"""
WARNING: This migration script will potentially cause a loss of data in labSampleId
Use with caution
 """
from protocols.migration.base_migration import BaseMigration
from protocols.migration.migration_participant_113_to_participant_112 import (
    MigrateParticipant113To112,
)
from protocols.protocol_7_2 import reports as reports_6_0_1
from protocols.protocol_7_2_1 import reports as reports_6_0_2


class MigrateReports602To601(BaseMigration):
    old_reports = reports_6_0_2
    new_reports = reports_6_0_1

    def migrate_interpretation_request_rd(self, old_instance):
        """
        Migrates a reports_6_0_2.InterpretationRequestRD into a reports_6_0_1.InterpretationRequestRD
        :type old_instance: reports_6_0_2.InterpretationRequestRD
        :rtype: reports_6_0_1.InterpretationRequestRD
        """
        new_instance = self.convert_class(
            target_klass=self.new_reports.InterpretationRequestRD, instance=old_instance
        )
        new_instance.versionControl = self.new_reports.ReportVersionControl()
        participant_migration_class = MigrateParticipant113To112()
        new_instance.pedigree = participant_migration_class.migrate_pedigree(
            old_pedigree=old_instance.pedigree
        )
        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_reports.InterpretationRequestRD,
        )

    def migrate_interpretation_request_cancer(self, old_instance):
        """
        Migrates a reports_6_0_2.CancerInterpretationRequest into a reports_6_0_1.CancerInterpretationRequest
        :type old_instance: reports_6_0_2.CancerInterpretationRequest
        :rtype: reports_6_0_1.CancerInterpretationRequest
        """
        new_instance = self.convert_class(
            target_klass=self.new_reports.CancerInterpretationRequest,
            instance=old_instance,
        )
        new_instance.versionControl = self.new_reports.ReportVersionControl()
        participant_migration_class = MigrateParticipant113To112()
        new_instance.cancerParticipant = (
            participant_migration_class.migrate_cancer_participant(
                old_cancer_participant=old_instance.cancerParticipant
            )
        )
        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_reports.CancerInterpretationRequest,
        )
