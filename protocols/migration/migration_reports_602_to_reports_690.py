from protocols.migration.base_migration import BaseMigration
from protocols.protocol_7_2_1 import reports as reports_6_0_2
from protocols.protocol_8_6 import reports as reports_6_9_0


class MigrateReports602To690(BaseMigration):
    old_reports = reports_6_0_2
    new_reports = reports_6_9_0

    def migrate_exit_questionnaire_rd(self, old_instance):
        """Migrates a reports_6_0_2.RareDiseaseExitQuestionnaire to a reports_6_9_0.RareDiseaseExitQuestionnaire
        :param old_instance: The original RareDiseaseExitQuestionnaire instance to migrate
        :type old_instance: reports_6_0_2.RareDiseaseExitQuestionnaire
        :return: The newly migrated RareDiseaseExitQuestionnaire
        :rtype: reports_6_9_0.RareDiseaseExitQuestionnaire
        """
        new_instance = self.convert_class(
            target_klass=self.new_reports.RareDiseaseExitQuestionnaire,
            instance=old_instance,
        )

        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_reports.RareDiseaseExitQuestionnaire,
        )

    def migrate_exit_questionnaire_cancer(self, old_instance):
        """Migrates a reports_6_0_2.CancerExitQuestionnaire to a reports_6_9_0.CancerExitQuestionnaire
        :param old_instance: The original CancerExitQuestionnaire instance to migrate
        :type old_instance: reports_6_0_2.CancerExitQuestionnaire
        :return: The newly migrated CancerExitQuestionnaire
        :rtype: reports_6_9_0.CancerExitQuestionnaire
        """
        new_instance = self.convert_class(
            target_klass=self.new_reports.CancerExitQuestionnaire, instance=old_instance
        )

        if new_instance.caseLevelQuestions.actionableVariants == "na":
            new_instance.caseLevelQuestions.actionableVariants = "no"

        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_reports.CancerExitQuestionnaire,
        )

    def migrate_clinical_report_rd(self, old_instance):
        """Migrates a reports_6_0_2.ClinicalReport to a reports_6_9_0.ClinicalReport
        :param old_instance: The original ClinicalReport instance to migrate
        :type old_instance: reports_6_0_2.ClinicalReport
        :return: The newly migrated ClinicalReport
        :rtype: reports_6_9_0.ClinicalReport
        """
        new_instance = self.convert_class(
            target_klass=self.new_reports.ClinicalReport, instance=old_instance
        )

        new_instance.chromosomalRearrangements = self.convert_collection(
            old_instance.chromosomalRearrangements, self._migrate_variants_report_events
        )
        new_instance.shortTandemRepeats = self.convert_collection(
            old_instance.shortTandemRepeats, self._migrate_variants_report_events
        )

        new_instance.structuralVariants = self.convert_collection(
            old_instance.structuralVariants, self._migrate_variants_report_events
        )

        new_instance.variants = self.convert_collection(
            old_instance.variants, self._migrate_variants_report_events
        )

        return self.validate_object(
            object_to_validate=new_instance, object_type=self.new_reports.ClinicalReport
        )

    def migrate_clinical_report_cancer(self, old_instance):
        """A pass through method to allow for backwards consistency. Cancer & RD CR are
        the same model in v6.
        Migrates a reports_6_0_2.ClinicalReport to a reports_6_9_0.ClinicalReport.
        :param old_instance: The original ClinicalReport instance to migrate
        :type old_instance: reports_6_0_2.ClinicalReport
        :return: The newly migrated ClinicalReport
        :rtype: reports_6_9_0.ClinicalReport
        """
        return self.migrate_clinical_report_rd(old_instance)

    def _migrate_variants_report_events(self, variants):
        new_instance = variants
        new_instance.reportEvents = self.convert_collection(
            variants.reportEvents, self.migrate_report_events
        )

        target_klass_mapper = {
            self.old_reports.SmallVariant.__name__: self.new_reports.SmallVariant,
            self.old_reports.ChromosomalRearrangement.__name__: self.new_reports.ChromosomalRearrangement,
            self.old_reports.StructuralVariant.__name__: self.new_reports.StructuralVariant,
            self.old_reports.ShortTandemRepeat.__name__: self.new_reports.ShortTandemRepeat,
        }
        try:
            target_klass = target_klass_mapper[type(variants).__name__]
        except KeyError:
            raise KeyError((type(variants).__name__, target_klass_mapper.keys()))

        new_instance = self.convert_class(
            instance=new_instance, target_klass=target_klass
        )

        return new_instance

    def migrate_report_events(self, old_instance):
        # Copied from MigrateReports650To660
        new_instance = self.convert_class(
            target_klass=self.new_reports.ReportEvent,
            instance=old_instance,
            consequences=[
                self.new_reports.Consequence(
                    sequenceOntologyTerms=[
                        self.new_reports.SequenceOntologyTerm(
                            accession=term.id,
                            name=term.name,
                        )
                    ]
                ).toJsonDict()
                for term in old_instance.variantConsequences
            ],
        )

        if (
            new_instance.guidelineBasedVariantClassification
            and new_instance.guidelineBasedVariantClassification.acmgVariantClassification
        ):
            for (
                acmgEvidence
            ) in (
                new_instance.guidelineBasedVariantClassification.acmgVariantClassification.acmgEvidences
            ):
                if not acmgEvidence.activationStrength:
                    acmgEvidence.activationStrength = acmgEvidence.weight
                if acmgEvidence.type == "bening":
                    acmgEvidence.type = "benign"
        return new_instance
