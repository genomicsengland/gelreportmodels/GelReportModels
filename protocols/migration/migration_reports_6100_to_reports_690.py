from protocols.migration.base_migration import BaseMigration
from protocols.protocol_8_8 import reports as reports_6_9_0
from protocols.protocol_8_9 import reports as reports_6_10_0


class MigrateReports6100To690(BaseMigration):
    old_reports = reports_6_10_0
    new_reports = reports_6_9_0

    def migrate_interpreted_genome(
        self, old_instance: reports_6_10_0.InterpretedGenome
    ) -> reports_6_9_0.InterpretedGenome:
        """Migrates a reports_6_10_0.InterpretedGenome into a reports_6_9_0.InterpretedGenome.
        Note: "reports" was not changed between 6.9.0 and 6.7.2
        :param old_instance: The GRM instance to be migrated.
        :type old_instance: reports_6_10_0.InterpretedGenome
        :return A migrated instance of the given GRM.
        :rtype: reports_6_9_0.InterpretedGenome
        """
        return self.convert_class(
            target_klass=self.new_reports.InterpretedGenome,
            instance=old_instance,
            # Pass updated nested models as kwargs
            versionControl=self.new_reports.ReportVersionControl(),
            # Models with VariantAttributes defined - the model that was changed.
            chromosomalRearrangements=self.convert_collection(
                things=old_instance.chromosomalRearrangements,
                migrate_function=self._migrate_variant,
                default=[],
            ),
            variants=self.convert_collection(
                things=old_instance.variants,
                migrate_function=self._migrate_variant,
                default=[],
            ),
            shortTandemRepeats=self.convert_collection(
                things=old_instance.shortTandemRepeats,
                migrate_function=self._migrate_variant,
                default=[],
            ),
            structuralVariants=self.convert_collection(
                things=old_instance.structuralVariants,
                migrate_function=self._migrate_variant,
                default=[],
            ),
        )

    def _migrate_variant(self, grm_instance) -> dict:
        """Downgrade a 6.10.0 GRM variant to a 6.9.0 representation.
        :param grm_instance: GRM variant to be migrated.
        :type grm_instance:
        :return: Dictionary representation of the migrated GRM.
        :rtype: dict
        """
        # For ChromosomalRearrangements this is nullable
        if grm_instance.variantAttributes:
            grm_instance.variantAttributes = self.migrate_variant_attributes(
                grm_instance.variantAttributes
            )

        return grm_instance.toJsonDict()

    def migrate_variant_attributes(
        self, old_instance: reports_6_10_0.VariantAttributes
    ) -> reports_6_9_0.VariantAttributes:
        """Migrates a reports_6_10_0.VariantAttributes to a reports_6_9_0.VariantAttributes.
        :param old_instance: The original VariantAttributes instance to migrate.
        :type old_instance: reports_6_10_0.VariantAttributes
        :return: The newly migrated VariantAttributes.
        :rtype: reports_6_9_0.VariantAttributes
        """
        old_instance_dict = old_instance.toJsonDict()
        old_instance_dict.pop("consequences")
        return self.new_reports.VariantAttributes.migrateFromJsonDict(
            jsonDict=old_instance_dict, validate=True
        )
