from protocols.migration.base_migration import BaseMigration
from protocols.migration.migration_participant_120_to_participant_130 import (
    MigrateParticipant120To130,
)
from protocols.protocol_7_3 import reports as reports_6_1_0
from protocols.protocol_7_7 import reports as reports_6_1_1


class MigrateReports610To611(BaseMigration):
    old_reports = reports_6_1_0
    new_reports = reports_6_1_1

    def migrate_interpretation_request_rd(self, old_instance):
        """
        Migrates a reports_6_1_0.InterpretationRequestRD into a reports_6_1_1.InterpretationRequestRD
        :type old_instance: reports_6_1.0.InterpretationRequestRD
        :rtype: reports_6_1_1.InterpretationRequestRD
        """
        new_instance = self.convert_class(
            target_klass=self.new_reports.InterpretationRequestRD, instance=old_instance
        )
        new_instance.versionControl = self.new_reports.ReportVersionControl()
        participant_migration_class = MigrateParticipant120To130()
        new_instance.pedigree = participant_migration_class.migrate_pedigree(
            old_pedigree=old_instance.pedigree
        )
        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_reports.InterpretationRequestRD,
        )

    def migrate_interpretation_request_cancer(self, old_instance):
        """
        Migrates a reports_6_1_0.CancerInterpretationRequest into a reports_6_1_1.CancerInterpretationRequest
        :type old_instance: reports_6_1_0.CancerInterpretationRequest
        :rtype: reports_6_1_1.CancerInterpretationRequest
        """
        new_instance = self.convert_class(
            target_klass=self.new_reports.CancerInterpretationRequest,
            instance=old_instance,
        )
        new_instance.versionControl = self.new_reports.ReportVersionControl()
        participant_migration_class = MigrateParticipant120To130()
        new_instance.cancerParticipant = (
            participant_migration_class.migrate_cancer_participant(
                old_cancer_participant=old_instance.cancerParticipant
            )
        )
        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_reports.CancerInterpretationRequest,
        )
