from protocols.migration.base_migration import BaseMigration
from protocols.migration.migration_participant_130_to_participant_113 import (
    MigrateParticipant130To113,
)
from protocols.protocol_7_2_1 import reports as reports_6_0_2
from protocols.protocol_7_7 import reports as reports_6_1_1


class MigrateReports611To602(BaseMigration):
    old_reports = reports_6_1_1
    new_reports = reports_6_0_2

    def migrate_interpretation_request_rd(self, old_instance):
        """
        Migrates a reports_6_1_1.InterpretationRequestRD into a reports_6_0_2.InterpretationRequestRD
        :type old_instance: reports_6_1_1.InterpretationRequestRD
        :rtype: reports_6_0_2.InterpretationRequestRD
        """
        new_instance = self.convert_class(
            target_klass=self.new_reports.InterpretationRequestRD, instance=old_instance
        )
        new_instance.versionControl = self.new_reports.ReportVersionControl()
        new_instance.pedigree = MigrateParticipant130To113().migrate_pedigree(
            old_pedigree=old_instance.pedigree
        )
        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_reports.InterpretationRequestRD,
        )

    def migrate_cancer_interpretation_request(self, old_instance, organisation_code):
        """
        Migrates a reports_6_1_1.CancerInterpretationRequest into a reports_6_0_2.CancerInterpretationRequest
        :type old_instance: reports_6_1_1.CancerInterpretationRequest
        :rtype: reports_6_0_2.CancerInterpretationRequest
        """
        new_instance = self.convert_class(
            target_klass=self.new_reports.CancerInterpretationRequest,
            instance=old_instance,
        )
        new_instance.versionControl = self.new_reports.ReportVersionControl()
        new_instance.cancerParticipant = (
            MigrateParticipant130To113().migrate_cancer_participant(
                old_participant=old_instance.cancerParticipant,
                organisation_code=organisation_code,
            )
        )
        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_reports.CancerInterpretationRequest,
        )
