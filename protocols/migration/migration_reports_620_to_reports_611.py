from protocols.migration.base_migration import BaseMigration
from protocols.protocol_7_7 import reports as reports_6_1_1
from protocols.protocol_7_8 import reports as reports_6_2_0


class MigrateReports620To611(BaseMigration):
    old_reports = reports_6_2_0
    new_reports = reports_6_1_1

    def migrate_variant_interpretation_log(self, old_instance):
        """
        Migrates a reports_6_2_0.VariantInterpretationLog into a reports_6_1_1.VariantInterpretationLog
        :type old_instance: reports_6_2_0.VariantInterpretationLog
        :rtype: reports_6_1_1.VariantInterpretationLog
        """
        new_instance = self.convert_class(
            target_klass=self.new_reports.VariantInterpretationLog,
            instance=old_instance,
        )
        if not new_instance.user.role:
            new_instance.user.role = ""
        if not new_instance.user.groups:
            new_instance.user.groups = []
        if (
            new_instance.variantValidation
            and new_instance.variantValidation.validationResult == "Pending"
        ):
            new_instance.variantValidation.validationResult = "NotPerformed"
        if new_instance.variantClassification.acmgVariantClassification:
            new_instance.variantClassification = (
                self._migrate_acmgVariantClassification(
                    new_instance.variantClassification
                )
            )
        if old_instance.comments:
            new_instance.comments = [
                comment.comment for comment in old_instance.comments
            ]
        new_instance.familyId = old_instance.groupId

        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_reports.VariantInterpretationLog,
        )

    def migrate_interpreted_genome(self, old_instance):
        """
        Migrates a reports_6_2_0.InterpretedGenome into a reports_6_1_1.InterpretedGenome
        :type old_instance: reports_6_2_0.InterpretedGenome
        :rtype: reports_6_1_1.InterpretedGenome
        """
        new_instance = self.convert_class(
            target_klass=self.new_reports.InterpretedGenome, instance=old_instance
        )
        new_instance.versionControl = self.new_reports.ReportVersionControl()
        new_instance.variants = self.convert_collection(
            old_instance.variants, self._migrate_variant, default=[]
        )

        new_instance.chromosomalRearrangements = self.convert_collection(
            old_instance.chromosomalRearrangements, self._migrate_variant, default=[]
        )

        new_instance.shortTandemRepeats = self.convert_collection(
            old_instance.shortTandemRepeats, self._migrate_variant, default=[]
        )

        new_instance.structuralVariants = self.convert_collection(
            old_instance.structuralVariants, self._migrate_variant, default=[]
        )

        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_reports.InterpretedGenome,
        )

    def _migrate_acmgVariantClassification(_self, old_variant_classification):
        """
        As both the reports_6_1_1.VariantInterpretationLog.variantClassification and
        the reports_6_1_1.InterpretedGenome.variant.reportEvent.guidelineBasedVariantClassification appear to share the same
        signature and defects, we will use this method mutate both objects
        """
        if not old_variant_classification:
            return None
        new_instance = old_variant_classification
        if new_instance.acmgVariantClassification:
            # There was no support for excluded variants in 6.1.1, so need to drop the whole ACMG score section
            if (
                new_instance.acmgVariantClassification.clinicalSignificance
                == "excluded"
            ):
                new_instance.acmgVariantClassification = None
            else:
                for (
                    acmg_evidence
                ) in new_instance.acmgVariantClassification.acmgEvidences:
                    if acmg_evidence.type == "benign":
                        acmg_evidence.type = "bening"
                    # 6.2.0 has values available for activation strength that don't map to 6.1.1, so drop this field
                    acmg_evidence.activationStrength = None

        return new_instance

    def _migrate_VariantClassification(self, old_variant_classification):
        if not old_variant_classification:
            return None
        new_variant_classification = old_variant_classification
        new_variant_classification.clinicalSignificance = self.get_valid_enum_value(
            old_variant_classification.clinicalSignificance,
            self.new_reports.ClinicalSignificance,
        )
        return new_variant_classification

    def _migrate_variant(self, variant):
        new_instance = self.convert_class(target_klass=getattr(reports_6_1_1, type(variant).__name__), instance=variant)
        new_instance.reportEvents = self.convert_collection(
            new_instance.reportEvents, self._migrate_report_events
        )

        return new_instance

    def _migrate_report_events(self, report_event):
        new_instance = report_event
        new_instance.guidelineBasedVariantClassification = (
            self._migrate_acmgVariantClassification(
                new_instance.guidelineBasedVariantClassification
            )
        )
        new_instance.variantClassification = self._migrate_VariantClassification(
            new_instance.variantClassification
        )

        return new_instance
