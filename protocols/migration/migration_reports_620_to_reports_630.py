import datetime

from protocols.migration.base_migration import BaseMigration
from protocols.protocol_7_8 import reports as reports_6_2_0
from protocols.protocol_7_9 import reports as reports_6_3_0


class MigrateReports620To630(BaseMigration):
    old_reports = reports_6_2_0
    new_reports = reports_6_3_0

    def migrate_variant_interpretation_log(self, old_instance):
        """
        Migrates a reports_6_2_0.VariantInterpretationLog into a reports_6_3_0.VariantInterpretationLog
        :type old_instance: reports_6_2_0.VariantInterpretationLog
        :rtype: reports_6_3_0.VariantInterpretationLog
        """
        new_instance = self.convert_class(
            target_klass=self.new_reports.VariantInterpretationLog,
            instance=old_instance,
        )
        new_instance.versionControl = self.new_reports.ReportVersionControl()
        # In 6.2.0 timestamp was just a date. In 6.3.0 it also includes time. When migrating to new model, set time to 00:00:00.
        old_timestamp = datetime.datetime.strptime(old_instance.timestamp, "%Y-%m-%d")
        new_instance.timestamp = old_timestamp.strftime("%Y-%m-%dT00:00:00+00:00")
        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_reports.VariantInterpretationLog,
        )
