import pytz
from dateutil import parser

from protocols.migration.base_migration import BaseMigration
from protocols.protocol_7_8 import reports as reports_6_2_0
from protocols.protocol_7_9 import reports as reports_6_3_0


class MigrateReports630To620(BaseMigration):
    old_reports = reports_6_3_0
    new_reports = reports_6_2_0

    def migrate_variant_interpretation_log(self, old_instance):
        """
        Migrates a reports_6_3_0.VariantInterpretationLog into a reports_6_2_0.VariantInterpretationLog
        :type old_instance: reports_6_3_0.VariantInterpretationLog
        :rtype: reports_6_2_0.VariantInterpretationLog
        """
        new_instance = self.convert_class(
            target_klass=self.new_reports.VariantInterpretationLog,
            instance=old_instance,
        )
        # In 6.2.0 timestamp was just a date. In 6.3.0 it also includes time. When migrating back to 6.2.0, strip out the time.
        old_timestamp = parser.parse(old_instance.timestamp)
        new_instance.timestamp = old_timestamp.astimezone(pytz.utc).strftime("%Y-%m-%d")
        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_reports.VariantInterpretationLog,
        )
