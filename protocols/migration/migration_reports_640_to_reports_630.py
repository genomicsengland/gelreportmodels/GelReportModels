from protocols.migration.base_migration import BaseMigration
from protocols.protocol_7_9 import reports as reports_6_3_0
from protocols.protocol_7_10 import reports as reports_6_4_0


class MigrateReports640To630(BaseMigration):
    old_reports = reports_6_4_0
    new_reports = reports_6_3_0

    def migrate_interpreted_genome(self, old_instance):
        """
        Migrates a reports_6_4_0.InterpretedGenome into a reports_6_3_0.InterpretedGenome
        :type old_instance: reports_6_4_0.InterpretedGenome
        :rtype: reports_6_2_0.InterpretedGenome
        """

        new_instance = self.convert_class(
            target_klass=self.new_reports.InterpretedGenome, instance=old_instance
        )
        new_instance.versionControl = self.new_reports.ReportVersionControl()
        new_instance.variants = self.convert_collection(
            old_instance.variants, self._migrate_variants, default=[]
        )

        new_instance.chromosomalRearrangements = self.convert_collection(
            old_instance.chromosomalRearrangements, self._migrate_variants, default=[]
        )

        new_instance.shortTandemRepeats = self.convert_collection(
            old_instance.shortTandemRepeats, self._migrate_variants, default=[]
        )

        new_instance.structuralVariants = self.convert_collection(
            old_instance.structuralVariants, self._migrate_variants, default=[]
        )

        new_instance.structuralVariants = self.convert_collection(
            old_instance.structuralVariants,
            self._migrate_structural_variant,
            default=[],
        )

        return new_instance

    def _migrate_structural_variant(self, old_structural_variant):
        type_reports = self.new_reports.StructuralVariantType
        # Need to change unknown values to deletion as 'cnloh' enum value has been
        # added in this version and it maps to 'deletion'
        # This enum is not nullable
        old_structural_variant.variantType = self.get_valid_enum_value(
            old_structural_variant.variantType, type_reports, type_reports.deletion
        )
        return old_structural_variant

    def _migrate_variants(self, variants):
        new_instance = variants
        new_instance.reportEvents = self.convert_collection(
            variants.reportEvents, self.migrate_report_events
        )
        new_instance.variantAttributes = self.migrate_variant_attributes(
            variants.variantAttributes
        )
        return new_instance

    def migrate_report_events(self, old_instance):
        """
        Migrates a reports_6_4_0.ReportEvent into a reports_6_3_0.ReportEvent.

        :param reports_6_4_0.ReportEvent old_instance:
        :return reports_6_3_0.ReportEvent:
        """

        # the higher version has an additional key that needs to be removed so we can migrate properly.
        temp_instance = old_instance
        temp_instance.evidenceEntries = None

        new_instance = self.convert_class(
            target_klass=self.new_reports.ReportEvent, instance=temp_instance
        )

        return new_instance

    def migrate_variant_attributes(self, old_instance):
        """
        Migrates a reports_6_4_0.VariantAttributes into a reports_6_3_0.VariantAttributes.

        :param reports_6_4_0.VariantAttributes old_instance:
        :return reports_6_3_0.VariantAttributes:
        """

        # the higher version has an additional key that needs to be removed so we can migrate properly.
        temp_instance = old_instance
        temp_instance.evidenceEntries = None

        new_instance = self.convert_class(
            target_klass=self.new_reports.VariantAttributes, instance=temp_instance
        )

        return new_instance
