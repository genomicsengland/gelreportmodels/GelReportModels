from protocols.migration.base_migration import BaseMigration
from protocols.protocol_7_12 import reports as reports_6_4_0
from protocols.protocol_7_13 import reports as reports_6_5_0


class MigrateReports650To640(BaseMigration):
    old_reports = reports_6_5_0
    new_reports = reports_6_4_0

    def migrate_interpreted_genome(
        self, old_instance: reports_6_5_0.InterpretedGenome
    ) -> reports_6_4_0.InterpretedGenome:
        """Migrates a reports_6_5_0.InterpretedGenome into a reports_6_4_0.InterpretedGenome.
        :param old_instance: The GRM instance to be migrated.
        :type old_instance: reports_6_5_0.InterpretedGenome
        :return A migrated instance of the given GRM.
        :rtype: reports_6_4_0.InterpretedGenome
        """
        return self.convert_class(
            target_klass=self.new_reports.InterpretedGenome,
            instance=old_instance,
            # Pass updated nested models as kwargs
            versionControl=self.new_reports.ReportVersionControl(),
            chromosomalRearrangements=self.convert_collection(
                things=old_instance.chromosomalRearrangements,
                migrate_function=self._migrate_variant,
                default=[],
            ),
            variants=self.convert_collection(
                things=old_instance.variants,
                migrate_function=self._migrate_variant,
                default=[],
            ),
            shortTandemRepeats=self.convert_collection(
                things=old_instance.shortTandemRepeats,
                migrate_function=self._migrate_variant,
                default=[],
            ),
            structuralVariants=self.convert_collection(
                things=old_instance.structuralVariants,
                migrate_function=self._migrate_struct_variant,
                default=[],
            ),
        )

    def _migrate_struct_variant(
        self, structural_variant: reports_6_5_0.StructuralVariant
    ) -> dict:
        """Downgrade a 6.5.0 STR variant to a 6.4.0 representation. This changes "unknown" values to "deletion" as
            "cnloh" enum value has been added in newer versions - which maps to "deletion". This enum value is not
            nullable.
        :param structural_variant: The GRM instance to be migrated.
        :type structural_variant: reports_6_5_0.StructuralVariant
        :return A dictionary representation of the migrated GRM.
        :rtype: dict
        """
        structural_variant.variantType = self.get_valid_enum_value(
            value=structural_variant.variantType,
            enum=self.new_reports.StructuralVariantType,
            default=self.new_reports.StructuralVariantType.deletion,
        )
        return self._migrate_variant(grm_instance=structural_variant)

    def _migrate_variant(self, grm_instance) -> dict:
        """Downgrade a 6.5.0 GRM variant to a 6.4.0 representation.
        :param grm_instance: GRM variant to be migrated.
        :type grm_instance:
        :return: Dictionary representation of the migrated GRM.
        :rtype: dict
        """
        grm_instance.reportEvents = self.convert_collection(
            grm_instance.reportEvents, self.migrate_report_events
        )
        grm_instance.variantAttributes = self.migrate_variant_attributes(
            grm_instance.variantAttributes
        )
        return grm_instance.toJsonDict()

    def migrate_report_events(
        self, old_instance: reports_6_5_0.ReportEvent
    ) -> reports_6_4_0.ReportEvent:
        """Migrates a reports_6_5_0.ReportEvent into a reports_6_4_0.ReportEvent.
        :param old_instance: The original ReportEvent instance to migrate.
        :type old_instance: reports_6_5_0.ReportEvent
        :return: The newly migrated ReportEvent.
        :rtype: reports_6_4_0.ReportEvent
        """
        return self.convert_class(
            target_klass=self.new_reports.ReportEvent, instance=old_instance
        )

    def migrate_variant_attributes(
        self, old_instance: reports_6_5_0.VariantAttributes
    ) -> reports_6_4_0.VariantAttributes:
        """Migrates a reports_6_5_0.VariantAttributes to a reports_6_4_0.VariantAttributes.
        :param old_instance: The original VariantAttributes instance to migrate.
        :type old_instance: reports_6_5_0.VariantAttributes
        :return: The newly migrated VariantAttributes.
        :rtype: reports_6_4_0.VariantAttributes
        """
        return self.convert_class(
            target_klass=self.new_reports.VariantAttributes, instance=old_instance
        )
