from protocols.migration.base_migration import BaseMigration
from protocols.protocol_7_13 import reports as reports_6_5_0
from protocols.protocol_8_0 import reports as reports_6_6_0


class MigrateReports650To660(BaseMigration):
    new_model = reports_6_6_0
    old_model = reports_6_5_0

    def migrate_interpreted_genome(self, old_instance):
        """
        Migrates a reports_6_5_0.InterpretedGenome into a reports_6_6_0.InterpretedGenome

        :param old_instance:
        :type old_instance: reports_6_5_0.InterpretedGenome
        :rtype: reports_6_6_0.InterpretedGenome
        """

        new_instance = self.convert_class(
            target_klass=self.new_model.InterpretedGenome, instance=old_instance
        )
        new_instance.versionControl = self.new_model.ReportVersionControl()
        new_instance.variants = self.convert_collection(
            old_instance.variants, self._migrate_variants, default=[]
        )

        new_instance.chromosomalRearrangements = self.convert_collection(
            old_instance.chromosomalRearrangements, self._migrate_variants, default=[]
        )

        new_instance.shortTandemRepeats = self.convert_collection(
            old_instance.shortTandemRepeats, self._migrate_variants, default=[]
        )

        new_instance.structuralVariants = self.convert_collection(
            old_instance.structuralVariants, self._migrate_variants, default=[]
        )

        new_instance.structuralVariants = self.convert_collection(
            old_instance.structuralVariants,
            self._migrate_structural_variant,
            default=[],
        )

        return new_instance

    def _migrate_structural_variant(self, old_structural_variant):
        new_instance = self.convert_class(
            target_klass=self.new_model.StructuralVariant,
            instance=old_structural_variant,
        )
        coordinates = new_instance.coordinates
        location = self.new_model.Location(coordinates=coordinates)
        new_instance.reportEvents = self.convert_collection(
            old_structural_variant.reportEvents, self.migrate_report_events
        )
        new_instance.location = location

        return new_instance

    def _migrate_variants(self, variants):
        new_instance = variants
        new_instance.reportEvents = self.convert_collection(
            variants.reportEvents, self.migrate_report_events
        )
        return new_instance

    def migrate_report_events(self, old_instance):
        """
        Migrates a reports_6_5_0.ReportEvent into a reports_6_6_0.ReportEvent.

        :param old_instance:
        :type reports_6_5_0.ReportEvent:
        :return: reports_6_6_0.ReportEvent
        :rtype: reports_6_6_0.ReportEvent
        """

        new_instance = self.convert_class(
            target_klass=self.new_model.ReportEvent,
            instance=old_instance,
            consequences=[
                self.new_model.Consequence(
                    sequenceOntologyTerms=[
                        self.new_model.SequenceOntologyTerm(
                            accession=term.id,
                            name=term.name,
                        )
                    ]
                ).toJsonDict()
                for term in old_instance.variantConsequences
            ],
        )

        return new_instance
