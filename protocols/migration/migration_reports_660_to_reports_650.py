import logging
from protocols.migration.base_migration import BaseMigration
from protocols.protocol_7_13 import reports as reports_6_5_0
from protocols.protocol_8_0 import reports as reports_6_6_0


class MigrateReports660To650(BaseMigration):
    old_model = reports_6_6_0
    new_model = reports_6_5_0

    def migrate_interpreted_genome(self, old_instance):
        """
        Migrates a reports_6_6_0.InterpretedGenome into a reports_6_5_0.InterpretedGenome
        :type old_instance: reports_6_6_0.InterpretedGenome
        :rtype: reports_6_5_0.InterpretedGenome
        """

        new_instance = self.convert_class(
            target_klass=self.new_model.InterpretedGenome, instance=old_instance
        )
        new_instance.versionControl = self.new_model.ReportVersionControl()
        new_instance.variants = self.convert_collection(
            old_instance.variants, self._migrate_variants, default=[]
        )

        new_instance.chromosomalRearrangements = self.convert_collection(
            old_instance.chromosomalRearrangements, self._migrate_variants, default=[]
        )

        new_instance.shortTandemRepeats = self.convert_collection(
            old_instance.shortTandemRepeats, self._migrate_variants, default=[]
        )

        new_instance.structuralVariants = self.convert_collection(
            old_instance.structuralVariants, self._migrate_variants, default=[]
        )

        new_instance.structuralVariants = self.convert_collection(
            old_instance.structuralVariants,
            self._migrate_structural_variant,
            default=[],
        )

        return new_instance

    def _migrate_structural_variant(self, old_structural_variant):
        if not self._has_exact_coordinates(
            old_structural_variant=old_structural_variant
        ):
            return None

        new_instance = self.convert_class(
            target_klass=self.new_model.StructuralVariant,
            instance=old_structural_variant,
        )
        if not new_instance.coordinates:
            coordinates = old_structural_variant.location.coordinates
            coordinates_in_old_model = self.convert_class(
                target_klass=self.new_model.Coordinates, instance=coordinates
            )
            new_instance.coordinates = coordinates_in_old_model

        return new_instance

    def _has_exact_coordinates(self, old_structural_variant):
        return old_structural_variant.coordinates is not None or (
            old_structural_variant.location is not None
            and old_structural_variant.location.coordinates is not None
        )

    def _migrate_variants(self, variants):
        new_instance = variants
        new_instance.reportEvents = self.convert_collection(
            variants.reportEvents, self.migrate_report_events
        )
        return new_instance

    def migrate_report_events(self, old_instance):
        """
        Migrates a reports_6_6_0.ReportEvent into a reports_6_5_0.ReportEvent.

        :param old_instance:
        :type old_instance: reports_6_6_0.ReportEvent
        :return: reports_6_5_0.ReportEvent
        :rtype: reports_6_5_0.ReportEvent
        """
        old_consequences = []
        old_genomic_entities = []
        if old_instance.consequences:
            for consequence in old_instance.consequences:
                for term in consequence.sequenceOntologyTerms:
                    old_consequences.append(
                        self.new_model.VariantConsequence(
                            id=term.accession,
                            name=term.name,
                        ).toJsonDict()
                    )
                if consequence.ensemblGeneId:
                    old_genomic_entities.append(
                        self.new_model.GenomicEntity(
                            ensemblId=consequence.ensemblGeneId,
                            geneSymbol=consequence.geneName,
                            type=self.new_model.GenomicEntityType.gene,
                        ).toJsonDict()
                    )

                if consequence.ensemblTranscriptId:
                    old_genomic_entities.append(
                        self.new_model.GenomicEntity(
                            ensemblId=consequence.ensemblTranscriptId,
                            geneSymbol=consequence.geneName,
                            type=self.new_model.GenomicEntityType.transcript,
                        ).toJsonDict()
                    )

                if consequence.ensemblProteinId:
                    logging.warning(
                        "Lost protein Id {} from ReportEvent".format(
                            consequence.ensemblProteinId
                        )
                    )

        # the higher version has an additional key that needs to be removed so we can migrate properly.
        new_instance = self.convert_class(
            target_klass=self.new_model.ReportEvent,
            instance=old_instance,
            variantConsequences=old_consequences,
            genomicEntities=old_genomic_entities,
        )

        if hasattr(new_instance, "reportEventFlags"):
            del new_instance.reportEventFlags

        return new_instance
