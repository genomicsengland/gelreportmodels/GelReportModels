from protocols.migration.base_migration import BaseMigration
from protocols.protocol_8_0 import reports as reports_6_6_0
from protocols.protocol_8_1 import reports as reports_6_7_0


class MigrateReports670To660(BaseMigration):
    old_reports = reports_6_7_0
    new_reports = reports_6_6_0

    def migrate_interpreted_genome(
        self, old_instance: reports_6_7_0.InterpretedGenome
    ) -> reports_6_6_0.InterpretedGenome:
        """Migrates a reports_6_7_0.InterpretedGenome into a reports_6_6_0.InterpretedGenome.
        :param old_instance: The GRM instance to be migrated.
        :type old_instance: reports_6_7_0.InterpretedGenome
        :return A migrated instance of the given GRM.
        :rtype: reports_6_6_0.InterpretedGenome
        """
        return self.convert_class(
            target_klass=self.new_reports.InterpretedGenome,
            instance=old_instance,
            # Pass updated nested models as kwargs
            versionControl=self.new_reports.ReportVersionControl(),
            # Models with VariantAttributes defined
            chromosomalRearrangements=self.convert_collection(
                things=old_instance.chromosomalRearrangements,
                migrate_function=self._migrate_variant,
                default=[],
            ),
            variants=self.convert_collection(
                things=old_instance.variants,
                migrate_function=self._migrate_variant,
                default=[],
            ),
            shortTandemRepeats=self.convert_collection(
                things=old_instance.shortTandemRepeats,
                migrate_function=self._migrate_variant,
                default=[],
            ),
            structuralVariants=self.convert_collection(
                things=old_instance.structuralVariants,
                migrate_function=self._migrate_variant,
                default=[],
            ),
        )

    def _migrate_variant(self, grm_instance) -> dict:
        """Downgrade a 6.7.0 GRM variant to a 6.6.0 representation.
        :param grm_instance: GRM variant to be migrated.
        :type grm_instance:
        :return: Dictionary representation of the migrated GRM.
        :rtype: dict
        """
        grm_instance.reportEvents = self.convert_collection(
            things=grm_instance.reportEvents,
            migrate_function=self.migrate_report_events,
        )
        # For ChromosomalRearrangements this is nullable
        if grm_instance.variantAttributes:
            grm_instance.variantAttributes = self.migrate_variant_attributes(
                grm_instance.variantAttributes
            )
        if grm_instance.variantCalls:
            for var_call in grm_instance.variantCalls:
                if var_call.alleleOrigins:
                    var_call.alleleOrigins = self._migrate_allele_origins(
                        allele_origins=var_call.alleleOrigins
                    )

        return grm_instance.toJsonDict()

    def migrate_report_events(
        self, old_instance: reports_6_7_0.ReportEvent
    ) -> reports_6_6_0.ReportEvent:
        """Migrates a reports_6_7_0.ReportEvent into a reports_6_6_0.ReportEvent.
        AlleleOrigin.unknown was not supported in reports 6.6.0 and before so here we this value to None.
        :param old_instance: ReportEvent instance to be migrated.
        :type old_instance: reports_6_7_0.ReportEvent
        :return: The newly migrated ReportEvent.
        :rtype: reports_6_6_0.ReportEvent
        """
        tmp_instance_dict = old_instance.toJsonDict()
        if old_instance.evidenceEntry and old_instance.evidenceEntry.alleleOrigin:
            tmp_instance_dict["evidenceEntry"][
                "alleleOrigin"
            ] = self._migrate_allele_origins(
                allele_origins=tmp_instance_dict["evidenceEntry"]["alleleOrigin"]
            )
        return self.new_reports.ReportEvent.migrateFromJsonDict(
            jsonDict=tmp_instance_dict, validate=True
        )

    def migrate_variant_attributes(
        self, old_instance: reports_6_7_0.VariantAttributes
    ) -> reports_6_6_0.VariantAttributes:
        """Migrates a reports_6_7_0.VariantAttributes to a reports_6_6_0.VariantAttributes.
        :param old_instance: The original VariantAttributes instance to migrate.
        :type old_instance: reports_6_7_0.VariantAttributes
        :return: The newly migrated VariantAttributes.
        :rtype: reports_6_6_0.VariantAttributes
        """
        old_instance_dict = old_instance.toJsonDict()
        if evidences := old_instance_dict.get("evidenceEntries"):
            for evidence_entry in evidences:
                if allele_origin_list := evidence_entry.get("alleleOrigin"):
                    evidence_entry["alleleOrigin"] = self._migrate_allele_origins(
                        allele_origins=allele_origin_list
                    )
        if old_instance_dict.get("alleleOrigins"):
            old_instance_dict["alleleOrigins"] = self._migrate_allele_origins(
                allele_origins=old_instance_dict["alleleOrigins"]
            )
        return self.new_reports.VariantAttributes.migrateFromJsonDict(
            jsonDict=old_instance_dict, validate=True
        )

    @staticmethod
    def _migrate_allele_origins(allele_origins: list) -> list | None:
        """Migrates a list of allele origins to reports_6_6_0.EvidenceEntry by removing
            AlleleOrigin.unknown, which is not supported in reports 6.6.0 and before.
        :param allele_origins: The original list of AlleleOrigin instances to migrate.
        :type allele_origins: list
        :return: The newly migrated allele origins, or None if list is now empty.
        :rtype: list | None
        """
        return [
            ao for ao in allele_origins if ao != reports_6_7_0.AlleleOrigin.unknown
        ] or None
