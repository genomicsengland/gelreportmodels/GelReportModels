from protocols.migration.base_migration import BaseMigration
from protocols.protocol_8_2 import reports as reports_6_7_1
from protocols.protocol_8_3 import reports as reports_6_7_2


class MigrateReports672To671(BaseMigration):
    old_reports = reports_6_7_2
    new_reports = reports_6_7_1

    def migrate_interpreted_genome(
        self, old_instance: reports_6_7_2.InterpretedGenome
    ) -> reports_6_7_1.InterpretedGenome:
        """Migrates a reports_6_7_2.InterpretedGenome into a reports_6_7_1.InterpretedGenome.
        :param old_instance: The GRM instance to be migrated.
        :type old_instance: reports_6_7_2.InterpretedGenome
        :return A migrated instance of the given GRM.
        :rtype: reports_6_7_1.InterpretedGenome
        """
        return self.convert_class(
            target_klass=self.new_reports.InterpretedGenome,
            instance=old_instance,
            # Pass updated nested models as kwargs
            versionControl=self.new_reports.ReportVersionControl(),
            # Models with VariantAttributes defined - the model that was changed.
            structuralVariants=self.convert_collection(
                things=old_instance.structuralVariants,
                migrate_function=self._migrate_struct_variant,
                default=[],
            ),
        )

    @staticmethod
    def _migrate_struct_variant(
        structural_variant: reports_6_7_2.StructuralVariant,
    ) -> dict | None:
        """Migrates a StructuralVariant to reports_6_7_1 equivalent. Note: StructuralVariantType.unknown was not
        supported in earlier versions, so in theses cases the variant will not be reported.
        :param structural_variant: The GRM instance to be migrated.
        :type structural_variant: reports_6_7_2.StructuralVariant
        :return A dictionary representation of the migrated GRM.
        :rtype: dict | None
        """
        if (
            structural_variant.variantType
            == reports_6_7_2.StructuralVariantType.unknown
        ):
            # StructuralVariantType is required, but unknown isn't supported so we shouldn't report this variant.
            return None
        return structural_variant.toJsonDict()
