from protocols.migration.base_migration import BaseMigration
from protocols.migration.migration_reports_611_to_reports_602 import (
    MigrateReports611To602,
)
from protocols.migration.migration_reports_620_to_reports_611 import (
    MigrateReports620To611,
)
from protocols.migration.migration_reports_630_to_reports_620 import (
    MigrateReports630To620,
)
from protocols.migration.migration_reports_640_to_reports_630 import (
    MigrateReports640To630,
)
from protocols.migration.migration_reports_650_to_reports_640 import (
    MigrateReports650To640,
)
from protocols.migration.migration_reports_660_to_reports_650 import (
    MigrateReports660To650,
)
from protocols.migration.migration_reports_670_to_reports_660 import (
    MigrateReports670To660,
)
from protocols.migration.migration_reports_672_to_reports_671 import (
    MigrateReports672To671,
)
from protocols.migration.migration_reports_690_to_672 import MigrateReports690To672
from protocols.protocol_7_2_1 import reports as reports_6_0_2
from protocols.protocol_8_6 import reports as reports_6_9_0


class MigrateReports690To602(BaseMigration):
    old_reports = reports_6_9_0
    new_reports = reports_6_0_2

    def migrate_interpreted_genome(self, old_instance):
        """Migrates a reports_6_9_0.InterpretedGenome to a reports_6_0_2.InterpretedGenome
        :param old_instance: The original InterpretedGenome instance to migrate
        :type old_instance: reports_6_9_0.InterpretedGenome
        :return: The newly migrated InterpretedGenome
        :rtype: reports_6_0_2.InterpretedGenome
        """
        migration_sequence = [
            MigrateReports690To672,
            MigrateReports672To671,
            MigrateReports670To660,
            MigrateReports660To650,
            MigrateReports650To640,
            MigrateReports640To630,  # No need to migrate from 630 to 620.
            MigrateReports620To611,  # No need to migrate from 611 to 602.
        ]
        intermediate_instance = old_instance

        for migration in migration_sequence:
            intermediate_instance = migration().migrate_interpreted_genome(
                old_instance=intermediate_instance
            )

        new_instance = self.convert_class(
            target_klass=self.new_reports.InterpretedGenome,
            instance=intermediate_instance,
        )

        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_reports.InterpretedGenome,
        )

    def migrate_exit_questionnaire_rd(self, old_instance):
        """Migrates a reports_6_9_0.RareDiseaseExitQuestionnaire to a reports_6_0_2.RareDiseaseExitQuestionnaire
        :param old_instance: The original RareDiseaseExitQuestionnaire instance to migrate
        :type old_instance: reports_6_9_0.RareDiseaseExitQuestionnaire
        :return: The newly migrated RareDiseaseExitQuestionnaire
        :rtype: reports_6_0_2.RareDiseaseExitQuestionnaire
        """
        new_instance = self.convert_class(
            target_klass=self.new_reports.RareDiseaseExitQuestionnaire,
            instance=old_instance,
        )

        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_reports.RareDiseaseExitQuestionnaire,
        )

    def migrate_exit_questionnaire_cancer(self, old_instance):
        """Migrates a reports_6_0_2.CancerExitQuestionnaire to a reports_6_9_0.CancerExitQuestionnaire
        :param old_instance: The original CancerExitQuestionnaire instance to migrate
        :type old_instance: reports_6_0_2.CancerExitQuestionnaire
        :return: The newly migrated CancerExitQuestionnaire
        :rtype: reports_6_9_0.CancerExitQuestionnaire
        """
        new_instance = self.convert_class(
            target_klass=self.new_reports.CancerExitQuestionnaire, instance=old_instance
        )

        if new_instance.caseLevelQuestions.actionableVariants == "na":
            new_instance.caseLevelQuestions.actionableVariants = "no"

        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_reports.CancerExitQuestionnaire,
        )

    def migrate_clinical_report_rd(self, old_instance):
        """Migrates a reports_6_9_0.ClinicalReport to a reports_6_0_2.ClinicalReport
        :param old_instance: The original ClinicalReport instance to migrate
        :type old_instance: reports_6_9_0.ClinicalReport
        :return: The newly migrated ClinicalReport
        :rtype: reports_6_0_2.ClinicalReport
        """
        new_instance = self.convert_class(
            target_klass=self.new_reports.ClinicalReport, instance=old_instance
        )

        new_instance.chromosomalRearrangements = self.convert_collection(
            old_instance.chromosomalRearrangements, self._migrate_variants_report_events
        )
        new_instance.shortTandemRepeats = self.convert_collection(
            old_instance.shortTandemRepeats, self._migrate_variants_report_events
        )

        new_instance.structuralVariants = self.convert_collection(
            old_instance.structuralVariants, self._migrate_variants_report_events
        )

        new_instance.variants = self.convert_collection(
            old_instance.variants, self._migrate_variants_report_events
        )

        return self.validate_object(
            object_to_validate=new_instance, object_type=self.new_reports.ClinicalReport
        )

    def migrate_clinical_report_cancer(self, old_instance):
        """A pass through method to allow for backwards consistency. Cancer & RD CR are
        the same model in v6.
        Migrates a reports_6_0_2.ClinicalReport to a reports_6_9_0.ClinicalReport.
        :param old_instance: The original ClinicalReport instance to migrate
        :type old_instance: reports_6_0_2.ClinicalReport
        :return: The newly migrated ClinicalReport
        :rtype: reports_6_9_0.ClinicalReport
        """
        return self.migrate_clinical_report_rd(old_instance)

    def _migrate_variants_report_events(self, variants):
        new_instance = variants
        new_instance.reportEvents = self.convert_collection(
            variants.reportEvents, self.migrate_report_events
        )

        target_klass_mapper = {
            self.old_reports.SmallVariant.__name__: self.new_reports.SmallVariant,
            self.old_reports.ChromosomalRearrangement.__name__: self.new_reports.ChromosomalRearrangement,
            self.old_reports.StructuralVariant.__name__: self.new_reports.StructuralVariant,
            self.old_reports.ShortTandemRepeat.__name__: self.new_reports.ShortTandemRepeat,
        }
        try:
            target_klass = target_klass_mapper[type(variants).__name__]
        except KeyError:
            raise KeyError((type(variants).__name__, target_klass_mapper.keys()))

        new_instance = self.convert_class(
            instance=new_instance, target_klass=target_klass
        )

        return new_instance

    def migrate_report_events(self, old_instance: reports_6_9_0.ReportEvent):

        consequence: reports_6_9_0.Consequence
        term: reports_6_9_0.SequenceOntologyTerm
        variant_consequences: list[reports_6_0_2.VariantConsequence] = []
        for consequence in old_instance.consequences:
            for term in consequence.sequenceOntologyTerms:
                if term.accession is not None and term.name is not None:
                    variant_consequences.append(self.new_reports.VariantConsequence(id=term.accession, name=term.name))
                    assert variant_consequences[-1].id is not None
                    assert variant_consequences[-1].name is not None

        new_instance: reports_6_0_2.ReportEvent = self.convert_class(
            target_klass=self.new_reports.ReportEvent,
            instance=old_instance,
        )
        new_instance.variantConsequences = variant_consequences

        if (
            new_instance.guidelineBasedVariantClassification
            and new_instance.guidelineBasedVariantClassification.acmgVariantClassification
        ):
            for (
                acmgEvidence
            ) in (
                new_instance.guidelineBasedVariantClassification.acmgVariantClassification.acmgEvidences
            ):
                if not acmgEvidence.activationStrength:
                    acmgEvidence.activationStrength = acmgEvidence.weight
                if acmgEvidence.type == "benign":
                    acmgEvidence.type = "bening"

        new_instance.raise_if_invalid()
        return new_instance
