from protocols.migration.base_migration import BaseMigration
from protocols.protocol_8_3 import reports as reports_6_7_2
from protocols.protocol_8_6 import reports as reports_6_9_0


class MigrateReports690To672(BaseMigration):
    old_reports = reports_6_9_0
    new_reports = reports_6_7_2

    def migrate_interpreted_genome(self, old_instance):
        """Migrates a reports_6_9_0.InterpretedGenome to a reports_6_7_2.InterpretedGenome
        :param old_instance: The original InterpretedGenome instance to migrate
        :type old_instance: reports_6_9_0.InterpretedGenome
        :return: The newly migrated InterpretedGenome
        :rtype: reports_6_7_2.InterpretedGenome
        """
        new_instance = self.convert_class(
            target_klass=self.new_reports.InterpretedGenome, instance=old_instance
        )

        return self.validate_object(
            object_to_validate=new_instance,
            object_type=self.new_reports.InterpretedGenome,
        )
