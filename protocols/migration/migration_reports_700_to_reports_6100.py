from protocols.migration.base_migration import BaseMigration
from protocols.protocol_8_9 import reports as reports_6_10_0
from protocols.protocol_9_0 import reports as reports_7_0_0


class MigrateReports700To6100(BaseMigration):
    old_reports = reports_7_0_0
    new_reports = reports_6_10_0

    def migrate_rd_exit_questionnaire(
        self, old_instance: reports_7_0_0.RareDiseaseExitQuestionnaire
    ) -> reports_7_0_0.RareDiseaseExitQuestionnaire:
        """Migrates a reports_7_0_0.RareDiseaseExitQuestionnaire into a reports_6_10_0.RareDiseaseExitQuestionnaire.
        :param old_instance: The GRM instance to be migrated.
        :type old_instance: reports_7_0_0.RareDiseaseExitQuestionnaire
        :return A migrated instance of the given GRM.
        :rtype: reports_6_10_0.RareDiseaseExitQuestionnaire
        """
        new_instance = self.convert_class(
            target_klass=self.new_reports.RareDiseaseExitQuestionnaire,
            instance=old_instance,
        )
        new_instance.familyLevelQuestions = self._migrate_family_level_questions(
            old_instance.familyLevelQuestions
        )
        new_instance.variantGroupLevelQuestions = self.convert_collection(
            old_instance.variantGroupLevelQuestions,
            self._migrate_variant_group_level_questions,
            default=[],
        )

        return new_instance

    def _migrate_family_level_questions(self, instance: reports_7_0_0.FamilyLevelQuestions) -> dict:
        """Downgrade a 7.0.0 GRM family_level_questions to a 6.10.0 representation.
        :param instance: GRM family_level_questions to be migrated.
        :type instance:
        :return: Dictionary representation of the migrated GRM.
        :rtype: dict
        """
        if not instance.segregationQuestion:
            instance.segregationQuestion = "no"

        return instance

    def _migrate_variant_group_level_questions(
        self, instance: reports_7_0_0.VariantGroupLevelQuestions
    ) -> reports_6_10_0.VariantGroupLevelQuestions:
        """Downgrade a 7.0.0 GRM variant_group_level_questions to a 6.10.0 representation.
        :param instance: GRM variant_group_level_questions to be migrated.
        :type instance:
        :return: Dictionary representation of the migrated GRM.
        :rtype: dict
        """
        if not instance.actionability:
            instance.actionability = "na"
        if not instance.clinicalUtility:
            instance.clinicalUtility = []
        instance.variantLevelQuestions = self.convert_collection(
                things=instance.variantLevelQuestions,
                migrate_function=self._migrate_variant_questions,
                default=[],
            )
        instance.structuralVariantLevelQuestions = self.convert_collection(
                things=instance.structuralVariantLevelQuestions,
                migrate_function=self._migrate_variant_questions,
                default=[],
            )
        instance.shortTandemRepeatLevelQuestions = self.convert_collection(
                things=instance.shortTandemRepeatLevelQuestions,
                migrate_function=self._migrate_variant_questions,
                default=[],
            )

        return instance

    def _migrate_variant_questions(self, instance):
        """Downgrade a 7.0.0 GRM family_level_questions to a 6.10.0 representation.
        :param instance: GRM family_level_questions to be migrated.
        :type instance:
        :return: Dictionary representation of the migrated GRM.
        :rtype: dict
        """
        if not instance.confirmationDecision:
            instance.confirmationDecision = "na"
        if not instance.confirmationOutcome:
            instance.confirmationOutcome = "na"
        if not instance.reportingQuestion:
            instance.reportingQuestion = "na"
        if not instance.acmgClassification:
            instance.acmgClassification = "na"
        if not instance.publications:
            instance.publications = ""

        return instance

