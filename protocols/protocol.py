"""
Definitions of the GA4GH protocol types.
"""
from __future__ import division, print_function, unicode_literals

import inspect
import json
import logging
import os
import re
import sys

import avro.io
import avro.schema
import dictdiffer
from avro.schema import ArraySchema, RecordSchema, UnionSchema

from functools import wraps
from past.builtins import basestring

from protocols.resources import RESOURCE_DIR

INT_MIN_VALUE = -(1 << 31)
INT_MAX_VALUE = (1 << 31) - 1
LONG_MIN_VALUE = -(1 << 63)
LONG_MAX_VALUE = (1 << 63) - 1

def load_schema(path):
    """Loads the schema for the provided path for the schema folder located in the same parent directory.

    :param path: Path to python file
    :type path: str
    :return: _description_
    :rtype: str
    """
    base = os.path.dirname(__file__)

    schema_path = os.path.join(
        RESOURCE_DIR,
        "schemas",
        os.path.dirname(path.replace(base, "")).lstrip("/"),
        re.sub(r"\.py(c)?$", ".avsc", os.path.basename(path)),
    )
    with open(schema_path) as f:
        return f.read()


class DefaultValidation:
    __validate__ = False

    @classmethod
    def is_enabled(cls):
        return cls.__validate__

    @classmethod
    def enable(cls):
        cls.__validate__ = True

    @classmethod
    def disable(cls):
        cls.__validate__ = False


class ValidationError(Exception):
    """
    Error to be raised if validation fails.
    """


class ProtocolElementEncoder(json.JSONEncoder):
    """
    Class responsible for encoding ProtocolElements as JSON.
    """

    def default(self, obj):
        if isinstance(obj, ProtocolElement):
            ret = {a: getattr(obj, a) for a in obj.__slots__}
        else:
            ret = super(ProtocolElementEncoder, self).default(obj)
        return ret


class ValidationResult(object):
    def __init__(self, result=None, messages=None):
        self.result = result or True
        self.messages = messages or []
        self.msg = "Class: [{class_name}] expects field: [{field_name}] "
        self.msg += "with schema type: [{schema_type}] but received value: [{value}]"
        self.schema_type_msg = "Schema: [{expected_schema}] has type: [{schema_type}] "
        self.schema_type_msg += "but received datum: [{datum}]"

    def update_class(self, class_name, field_name, schema_type, value):
        self.messages.append(
            self.msg.format(
                class_name=class_name,
                field_name=field_name,
                schema_type=schema_type,
                value=value,
            )
        )

    def update_simple(self, expected_schema, schema_type, datum):
        self.result = False
        self.messages.append(
            self.schema_type_msg.format(
                expected_schema=expected_schema,
                schema_type=schema_type,
                datum=datum,
            )
        )

    def update_custom(self, custom_message):
        self.result = False
        self.messages.append(custom_message)

    def short_messages(self, characters=80):
        return [message[0:characters] for message in self.messages]

    def __str__(self):
        return "\n".join(self.messages)

    def __repr__(self):
        return "ValidationResult(result={})".format(self.result)


class ProtocolElement(object):
    """
    Superclass of GA4GH protocol elements. These elements are in one-to-one
    correspondence with the Avro definitions, and provide the basic elements
    of the on-the-wire protocol.
    """

    def __init__(self, validate=None, **kwargs):
        embedded = self.getEmbeddedTypes()
        for k, v in kwargs.items():
            if type(v) is type:
                if embedded.get(k) == v:
                    setattr(self, k, v(validate=False))
                else:
                    setattr(self, k, v())
            else:
                setattr(self, k, v)

        if validate is None:
            validate = DefaultValidation.is_enabled()
        if validate:
            self.raise_if_invalid()

    def __str__(self):
        return "{0}({1})".format(self.__class__.__name__, self.toJsonString())

    def __hash__(self):
        return self.toJsonString().__hash__()

    def __eq__(self, other):
        """
        Returns True if all fields in this protocol element are equal to the
        fields in the specified protocol element.
        """

        if not isinstance(other, self.__class__):
            return False

        fields = set(map(lambda f: f.name, self.schema.fields + other.schema.fields))

        return all(
            hasattr(self, k)
            and hasattr(other, k)
            and getattr(self, k) == getattr(other, k)
            for k in fields
        )

    def __ne__(self, other):
        return not self == other

    def raise_if_invalid(self):
        """This validates the instance of GRM model

        :param instance: GRM class that has ProtocolELment as its base
        :type instance: ProtocolElement
        :raises AttributeError: If the model is missing an attribute
        :raises ValidationError: If the model fails validation
        """
        try:
            
            validation = self.validate_fields()
        except AttributeError as error:
            raise ValidationError(
                "Failed to convert {name} to json. {error}".format(
                    name=type(self).__name__, error=error 
                )
            )
        if validation:
            messages = "\n".join(validation)
            raise ValidationError(
                "Invalid {name} found.\n{messages}".format(
                    name=type(self).__name__, messages=messages
                )
            )

    def toJsonString(self, validate=None):
        """
        Returns a JSON encoded string representation of this ProtocolElement.
        """

        return json.dumps(
            self.toJsonDict(validate=validate),
            cls=ProtocolElementEncoder,
            sort_keys=True,
        )

    def toJsonDict(self, validate=None):
        """
        Returns a JSON dictionary representation of this ProtocolElement.
        """

        if validate is None:
            validate = DefaultValidation.is_enabled()

        if validate:
            self.raise_if_invalid()

        out = {}
        for field in self.schema.fields:
            val = getattr(self, field.name)
            if val is None:
                out[field.name] = None
            elif self.isEmbeddedType(field.name):
                if isinstance(val, list):
                    out[field.name] = list(
                        el.toJsonDict(validate=validate) for el in val
                    )
                elif isinstance(val, dict):
                    out[field.name] = {
                        key: el.toJsonDict(validate=validate) for key, el in val.items()
                    }
                else:
                    out[field.name] = val.toJsonDict(validate=validate)
            else:
                out[field.name] = val

        return out

    def equals(self, instance):
        """
        Method to compare entities
        :return:
        """
        if not isinstance(instance, ProtocolElement):
            logging.error(
                "Comparing instance of type {} with instance of type {}".format(
                    type(self), type(instance)
                )
            )
            return False
        differences = list(dictdiffer.diff(self.toJsonDict(), instance.toJsonDict()))
        if differences is None or differences == []:
            return True
        return differences

    def validate_fields(self, dotpath=None):
        
        result = []

        if not dotpath: 
            dotpath = self.schema.name 

        for field in self.schema.fields:         
            
            result += self.validate_type(field.type, getattr(self, field.name),  "{}.{}".format(dotpath, field.name) )

        return result
 
    def avro_type_to_python(self, schema):

        if schema.type in ["record", "error", "request"]:
            return (self.getEmbeddedByNamespace()["{}.{}".format(schema.namespace,schema.name)] ,)
        else:
            clazzes = {
                "null": type(None),
                "int": int,
                "long": int,
                "float": (int, float),
                "double": (int, float),
                "boolean": bool,
                "array": list,
                "map": dict
            }
            clazz = clazzes.get(schema.type, basestring) 
            return (clazz,) if not isinstance(clazz, tuple ) else clazz

    def validate_type(self, schema, datum, dotpath=None):

        if not dotpath: 
            dotpath = schema.name 
        
        if schema.type in ["union", "error_union"]:
            clazzes = tuple([c for s in schema.schemas for c in self.avro_type_to_python(s)])
        else:
            clazzes = self.avro_type_to_python(schema)
        
        if not isinstance(datum, clazzes):
            return ["{} expects a type of {} but received {}.".format(
                dotpath,
                (clazzes),
                type(datum)
            )]

        # specific error messages
        if schema.type == "int":
            if not (
                (isinstance(datum, int)) and INT_MIN_VALUE <= datum <= INT_MAX_VALUE
            ):
                return ["{} expects int between {} and {} but received {}.".format(
                    dotpath,
                    INT_MIN_VALUE,
                    INT_MAX_VALUE,
                    datum
                )]
        elif schema.type == "long" :
            if not (
                (isinstance(datum, int)) and LONG_MIN_VALUE <= datum <= LONG_MAX_VALUE
            ):
                return ["{} expects long between {} and {} but received {}.".format(
                    dotpath,
                    LONG_MIN_VALUE,
                    INT_MAX_VALUE,
                    datum
                )]
        elif schema.type == "fixed":
            if not (isinstance(datum, basestring) and len(datum) == schema.size):
                return ["{} expects fixed str of size {} but got {}.".format(
                    dotpath,
                    schema.size,
                    len(datum) 
                )]
        elif schema.type == "enum":
            if datum not in schema.symbols:
                return ["{} expects one of {} but got {}.".format(
                    dotpath,
                    schema.symbols,
                    datum
                )]
        elif schema.type == "array":
            for index, data in enumerate(datum):
                return self.validate_type(schema.items, data, "{}[{}]".format(dotpath, index))
        elif schema.type == "map":

            for key, data in datum.items():
                if not isinstance(key, basestring):
                    return ["{} expects keys to be strings but got {} ({}).".format(
                        dotpath,
                        key,
                        type(key)
                    )]

                return self.validate_type(schema.values, data, "{}.{}".format(dotpath, key))
                
        elif schema.type in ["union", "error_union"]:
            for expected_schema in schema.schemas:
                if not isinstance(datum, self.avro_type_to_python(expected_schema) ):
                    continue
                return self.validate_type(
                    expected_schema,
                    datum, 
                    dotpath,
                )
            
        elif schema.type in ["record", "error", "request"]:
            datum.raise_if_invalid()

        return []
            
    def validate_parts(self):
        out = {}

        for field in self.schema.fields:
            val = getattr(self, field.name)
            if self.isEmbeddedType(field.name):
                if isinstance(val, list):
                    out[field.name] = list(el.validate_parts() for el in val)
                elif isinstance(val, dict):
                    out[field.name] = {
                        key: el.validate_parts() for (key, el) in val.items()
                    }
                elif val is None:
                    if isinstance(field.type, UnionSchema) and "null" in [
                        t.type for t in field.type.schemas
                    ]:
                        out[field.name] = True
                    else:
                        out[field.name] = False

                else:
                    out[field.name] = val.validate_parts()
            elif isinstance(val, list):
                if isinstance(field.type, UnionSchema):
                    out[field.name] = False
                    for sc in field.type.schemas:
                        if isinstance(sc, ArraySchema):
                            out[field.name] = list(
                                avro_validate(sc.items, el) for el in val
                            )
                else:
                    if isinstance(field.type, ArraySchema):
                        out[field.name] = list(
                            avro_validate(field.type.items, el) for el in val
                        )
                    else:
                        out[field.name] = False
            else:
                out[field.name] = avro_validate(field.type, val)

        return out

    def extended_validation(self):
        for field in self.schema.fields:
            val = getattr(self, field.name)
            if field.type.type == "string":
                if val == "":
                    return False
        return self.validate(self.toJsonDict())

    @classmethod
    def validate(cls, jsonDict, verbose=False):
        """
        Validates the specified JSON dictionary to determine if it is an
        instance of this element's schema.
        """
        if verbose:
            return cls.validate_debug(
                jsonDict=jsonDict, validation_result=ValidationResult()
            )
        return avro_validate(expected_schema=cls.schema, datum=jsonDict)

    @classmethod
    def validate_debug(cls, jsonDict, validation_result=None, expected_schema=None):
        """
        Returns ValidationResult with fields:
                - result (True or False)
                - messages (List of message strings ideally to help debug the problem)
        """
        INT_MIN_VALUE = -(1 << 31)
        INT_MAX_VALUE = (1 << 31) - 1
        LONG_MIN_VALUE = -(1 << 63)
        LONG_MAX_VALUE = (1 << 63) - 1
        expected_schema = expected_schema or cls.schema
        datum = jsonDict
        schema_type = expected_schema.type

        if not isinstance(validation_result, ValidationResult):
            validation_result = ValidationResult()

        if schema_type == "null":
            if not (datum is None):
                validation_result.update_simple(
                    expected_schema=expected_schema,
                    schema_type=schema_type,
                    datum=datum,
                )
        elif schema_type == "boolean":
            if not isinstance(datum, bool):
                validation_result.update_simple(
                    expected_schema=expected_schema,
                    schema_type=schema_type,
                    datum=datum,
                )
        elif schema_type == "string":
            if not isinstance(datum, basestring):
                validation_result.update_simple(
                    expected_schema=expected_schema,
                    schema_type=schema_type,
                    datum=datum,
                )
        elif schema_type == "bytes":
            if not isinstance(datum, str):
                validation_result.update_simple(
                    expected_schema=expected_schema,
                    schema_type=schema_type,
                    datum=datum,
                )
        elif schema_type == "int":
            if not (
                (isinstance(datum, int)) and INT_MIN_VALUE <= datum <= INT_MAX_VALUE
            ):
                validation_result.update_simple(
                    expected_schema=expected_schema,
                    schema_type=schema_type,
                    datum=datum,
                )
                custom_message = "{INT_MIN_VALUE} <= {datum} <= {INT_MAX_VALUE}".format(
                    INT_MIN_VALUE=INT_MIN_VALUE,
                    datum=datum,
                    INT_MAX_VALUE=INT_MAX_VALUE,
                )
                validation_result.update_custom(custom_message=custom_message)
        elif schema_type == "long":
            if not (
                (isinstance(datum, int)) and LONG_MIN_VALUE <= datum <= LONG_MAX_VALUE
            ):
                validation_result.update_simple(
                    expected_schema=expected_schema,
                    schema_type=schema_type,
                    datum=datum,
                )
                custom_message = (
                    "{LONG_MIN_VALUE} <= {datum} <= {LONG_MAX_VALUE}".format(
                        LONG_MIN_VALUE=LONG_MIN_VALUE,
                        datum=datum,
                        LONG_MAX_VALUE=LONG_MAX_VALUE,
                    )
                )
                validation_result.update_custom(custom_message=custom_message)
        elif schema_type in ["float", "double"]:
            if not (isinstance(datum, int) or isinstance(datum, float)):
                validation_result.update_simple(
                    expected_schema=expected_schema,
                    schema_type=schema_type,
                    datum=datum,
                )
        elif schema_type == "fixed":
            if not (isinstance(datum, str) and len(datum) == expected_schema.size):
                validation_result.update_simple(
                    expected_schema=expected_schema,
                    schema_type=schema_type,
                    datum=datum,
                )
                message_template = "Length of datum: {datum_length} does not match expected schema size: {schema_size}"
                custom_message = message_template.format(
                    datum_length=len(datum),
                    schema_size=expected_schema.size,
                )
                validation_result.update_custom(custom_message=custom_message)
        elif schema_type == "enum":
            if datum not in expected_schema.symbols:
                validation_result.update_simple(
                    expected_schema=expected_schema,
                    schema_type=schema_type,
                    datum=datum,
                )
                custom_message = (
                    "datum: [{datum}] not contained within symbols: [{enum}]".format(
                        datum=datum,
                        enum=expected_schema.symbols,
                    )
                )
                validation_result.update_custom(custom_message=custom_message)
        elif schema_type == "array":
            if not isinstance(datum, list):
                validation_result.update_simple(
                    expected_schema=expected_schema,
                    schema_type=schema_type,
                    datum=datum,
                )
            elif isinstance(datum, list):
                for data in datum:
                    if not avro_validate(
                        expected_schema=expected_schema.items, datum=data
                    ):
                        validation_result.update_simple(
                            expected_schema=expected_schema.items,
                            schema_type=expected_schema.items.type,
                            datum=data,
                        )
        elif schema_type == "map":
            if not isinstance(datum, dict):
                validation_result.update_simple(
                    expected_schema=expected_schema,
                    schema_type=schema_type,
                    datum=datum,
                )
            elif isinstance(datum, dict):
                for key in datum.keys():
                    if not isinstance(key, basestring):
                        custom_message = "key: {key} must be of type str but it of type: {key_type}".format(
                            key=key,
                            key_type=type(key),
                        )
                        validation_result.update_custom(custom_message=custom_message)
                for value in datum.values():
                    if not avro_validate(
                        expected_schema=expected_schema.values, datum=value
                    ):
                        validation_result.update_simple(
                            expected_schema=expected_schema.values,
                            schema_type=expected_schema.values.type,
                            datum=value,
                        )
        elif schema_type in ["union", "error_union"]:
            if not any([avro_validate(s, datum) for s in expected_schema.schemas]):
                for expected_schema in expected_schema.schemas:
                    if not avro_validate(expected_schema=expected_schema, datum=datum):
                        if hasattr(expected_schema, "values"):
                            validation_result.update_simple(
                                expected_schema=expected_schema.values,
                                schema_type=expected_schema.values.type,
                                datum=datum,
                            )
                        else:
                            validation_result.update_simple(
                                expected_schema=expected_schema,
                                schema_type=expected_schema,
                                datum=datum,
                            )
        elif schema_type in ["record", "error", "request"]:
            if isinstance(datum, dict):
                for f in expected_schema.fields:
                    if not avro_validate(
                        expected_schema=f.type, datum=datum.get(f.name)
                    ):
                        cls.validate_debug(
                            jsonDict=datum.get(f.name),
                            validation_result=validation_result,
                            expected_schema=f.type,
                        )
                        validation_result.update_class(
                            class_name=expected_schema.name,
                            field_name=f.name,
                            schema_type=f.type,
                            value=datum.get(f.name),
                        )
            else:
                validation_result.update_simple(
                    expected_schema=expected_schema,
                    schema_type=expected_schema,
                    datum=datum,
                )

        return validation_result

    @classmethod
    def fromJsonString(cls, jsonStr, validate=None):
        """
        Returns a decoded ProtocolElement from the specified JSON string.
        """
        jsonDict = json.loads(jsonStr)
        return cls.fromJsonDict(jsonDict, validate=validate)

    @classmethod
    def fromJsonDict(cls, jsonDict, key_mapper=(lambda x: x), validate=None, **kwargs):
        """
        Returns a decoded ProtocolElement from the specified JSON dictionary.
        """
        if jsonDict is None:
            raise ValueError("Required values not set in {0}".format(cls))

        if isinstance(jsonDict, dict):
            json_key_mapping = {key_mapper(key): key for key in jsonDict.keys()}
        else:
            json_key_mapping = dict()

        protocol_key_mapping = {key_mapper(key): key for key in cls.__slots__}

        mapped_keys = {}
        for field in cls.schema.fields:
            if field.has_default:
                instanceVal = field.default
            else:
                instanceVal = None
            if key_mapper(field.name) in json_key_mapping:
                json_mapped_name = json_key_mapping[key_mapper(field.name)]
                protocol_mapped_name = protocol_key_mapping[key_mapper(field.name)]
                val = jsonDict[json_mapped_name]
                if cls.isEmbeddedType(protocol_mapped_name):
                    instanceVal = cls._decodeEmbedded(
                        field, val, key_mapper=key_mapper, validate=validate
                    )
                else:
                    instanceVal = val
            mapped_keys[field.name] = instanceVal

        
        mapped_keys["validate"] = False

        kwargs.update(mapped_keys)

        instance = cls(**kwargs)

        if validate is None:
            validate = DefaultValidation.is_enabled()
        if validate:
            instance.raise_if_invalid()

        return instance

    @classmethod
    def migrateFromJsonDict(cls, jsonDict, validate=None):
        """
        like fromJsonDict but applies some fuzzy rules
        """
        return cls.fromJsonDict(
            jsonDict, lambda s: s.lower().replace("_", ""), validate=validate
        )

    def updateWithJsonDict(self, jsonDict, validate=None):
        """
        Updates this object from a dict
        """
        if jsonDict is None:
            raise ValueError("Required values not set in {0}".format(self))

        for field in self.schema.fields:
            if field.name in jsonDict:
                val = jsonDict[field.name]
                if self.isEmbeddedType(field.name):
                    instanceVal = self._decodeEmbedded(field, val, validate=validate)
                else:
                    instanceVal = val
                if instanceVal is not None:
                    setattr(self, field.name, instanceVal)

    @classmethod
    def _decodeEmbedded(cls, field, val, key_mapper=(lambda x: x), validate=None):
        if val is None:
            return None

        embeddedType = cls.getEmbeddedType(field.name)
        if isinstance(field.type, UnionSchema):
            if isinstance(field.type.schemas[1], ArraySchema):
                return list(
                    embeddedType.fromJsonDict(
                        elem, key_mapper=key_mapper, validate=validate
                    )
                    for elem in val
                )
            elif isinstance(field.type.schemas[1], avro.schema.MapSchema):
                return {
                    key: embeddedType.fromJsonDict(
                        elem, key_mapper=key_mapper, validate=validate
                    )
                    for (key, elem) in val.items()
                }
            else:
                return embeddedType.fromJsonDict(
                    val, key_mapper=key_mapper, validate=validate
                )

        elif isinstance(field.type, avro.schema.ArraySchema):
            return list(
                embeddedType.fromJsonDict(
                    elem, key_mapper=key_mapper, validate=validate
                )
                for elem in val
            )
        elif isinstance(field.type, avro.schema.MapSchema):
            return {
                key: embeddedType.fromJsonDict(
                    elem, key_mapper=key_mapper, validate=validate
                )
                for (key, elem) in val.items()
            }
        else:
            return embeddedType.fromJsonDict(
                val, key_mapper=key_mapper, validate=validate
            )

    @classmethod
    def isEmbeddedType(cls, fieldName):
        """Checks if field contains embedded type

        :param fieldName: Name of field containing embedded type
        :type fieldName: str
        :return: True if field is an embedded type
        :rtype: bool
        """
        return fieldName in cls.getEmbeddedTypes()

    @classmethod
    def getEmbeddedType(cls, fieldName):
        """Gets the embeddedType of given field

        :param fieldName: Name of field containing embedded type
        :type fieldName: str
        :return: Will return embedded type
        :rtype: Type
        :raises: KeyError if fieldName is not an embedded Type
        """
        return cls.getEmbeddedTypes()[fieldName]

    @classmethod
    def getEmbeddedTypes(cls):
        """Gets the embeddedType dict.
        Should be overriden by child class that has embedded types

        :return: Dictionary of embedded types.
        :rtype: dict
        """
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        """Gets the embeddedType by Namespace

        :return: Dictionary of embedded types.
        :rtype: dict
        """
        return {}


class SearchRequest(ProtocolElement):
    """
    The superclass of all SearchRequest classes in the protocol.
    """


class SearchResponse(ProtocolElement):
    """
    The superclass of all SearchResponse classes in the protocol.
    """

    @classmethod
    def getValueListName(cls):
        """
        Returns the name of the list used to store the values held
        in a page of results.
        """
        return cls._valueListName


def getProtocolClasses(superclass=ProtocolElement):
    """
    Returns all the protocol classes that are subclasses of the
    specified superclass. Only 'leaf' classes are returned,
    corresponding directly to the classes defined in the protocol.
    """
    # We keep a manual list of the superclasses that we define here
    # so we can filter them out when we're getting the protocol
    # classes.
    superclasses = {ProtocolElement, SearchRequest, SearchResponse}
    thisModule = sys.modules[__name__]
    subclasses = []
    for name, class_ in inspect.getmembers(thisModule):
        if (
            inspect.isclass(class_)
            and issubclass(class_, superclass)
            and class_ not in superclasses
        ):
            subclasses.append(class_)
    return subclasses


def avro_parse(schema):
    return avro.schema.parse(schema)


def avro_validate(expected_schema, datum):
    return avro.io.validate(expected_schema=expected_schema, datum=datum)
