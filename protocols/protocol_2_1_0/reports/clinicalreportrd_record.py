"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class ClinicalReportRD(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "candidateStructuralVariants",
        "candidateVariants",
        "genomicInterpretation",
        "interpretationRequestAnalysisVersion",
        "interpretationRequestID",
        "interpretationRequestVersion",
        "referenceDatabasesVersions",
        "reportingDate",
        "softwareVersions",
        "user",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_2_1_0 import reports as reports_2_1_0

        return {
            "candidateStructuralVariants": reports_2_1_0.ReportedStructuralVariant,
            "candidateVariants": reports_2_1_0.ReportedVariant,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_2_1_0 import reports as reports_2_1_0

        return {
            "Gel_BioInf_Models.ReportedStructuralVariant": reports_2_1_0.ReportedStructuralVariant,
            "Gel_BioInf_Models.ReportedVariant": reports_2_1_0.ReportedVariant,
            "Gel_BioInf_Models.ClinicalReportRD": reports_2_1_0.ClinicalReportRD,
        }

    __slots__ = [
        "candidateStructuralVariants",
        "candidateVariants",
        "genomicInterpretation",
        "interpretationRequestAnalysisVersion",
        "interpretationRequestID",
        "interpretationRequestVersion",
        "referenceDatabasesVersions",
        "reportingDate",
        "softwareVersions",
        "user",
        "supportingEvidence",
    ]

    def __init__(
        self,
        candidateStructuralVariants,
        candidateVariants,
        genomicInterpretation,
        interpretationRequestAnalysisVersion,
        interpretationRequestID,
        interpretationRequestVersion,
        referenceDatabasesVersions,
        reportingDate,
        softwareVersions,
        user,
        supportingEvidence=None,
        validate=None,
        **kwargs
    ):
        """Initialise the reports_2_1_0.ClinicalReportRD model.

        :param candidateStructuralVariants:
            Candidate Structural Variants - as defined in CommonInterpreted
        :type candidateStructuralVariants: list[ReportedStructuralVariant]
        :param candidateVariants:
            Candidate Variants - as defined in CommonInterpreted
        :type candidateVariants: list[ReportedVariant]
        :param genomicInterpretation:
            Summary of the interpretation, this should reflects the positive
            conclusions of this interpretation
        :type genomicInterpretation: str
        :param interpretationRequestAnalysisVersion:
            This is the version of the analytical approach that was carried out on
            the same interpretationRequest.     For example provider
            produces two types of analysis on the data and end user
            produces different summaries depending     on the analysis
            version
        :type interpretationRequestAnalysisVersion: str
        :param interpretationRequestID:
            This is the interpretation Request Id, first number in XXX-123-1
        :type interpretationRequestID: str
        :param interpretationRequestVersion:
            This is the version of the interpretation Request Id, second number in
            XXX-123-2
        :type interpretationRequestVersion: str
        :param referenceDatabasesVersions:
            This map should contains the version of the different DBs used in the
            process
        :type referenceDatabasesVersions: dict[str, str]
        :param reportingDate:
            Date of this report
        :type reportingDate: str
        :param softwareVersions:
            This map should contains the version of the different software in the
            process
        :type softwareVersions: dict[str, str]
        :param user:
            Author of this report
        :type user: str
        :param supportingEvidence:
            Supporting evidence (pubmed Ids)
        :type supportingEvidence: None | list[str]
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "2.1.0"
