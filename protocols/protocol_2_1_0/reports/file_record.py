"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class File(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "URIFile",
        "fileType",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_2_1_0 import reports as reports_2_1_0

        return {
            "fileType": reports_2_1_0.FileType,
        }

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_2_1_0 import reports as reports_2_1_0

        return {
            "Gel_BioInf_Models.FileType": reports_2_1_0.FileType,
            "Gel_BioInf_Models.File": reports_2_1_0.File,
        }

    __slots__ = [
        "URIFile",
        "fileType",
        "SampleId",
    ]

    def __init__(
        self,
        URIFile,
        fileType,
        SampleId=None,
        validate=None,
        **kwargs
    ):
        """Initialise the reports_2_1_0.File model.

        :param URIFile:
            URI PATH
        :type URIFile: str
        :param fileType:
        :type fileType: FileType
        :param SampleId:
            Unique ID(s) of the Sample, for example in a multisample vcf this
            would have an array of all the smaple ids
        :type SampleId: None | str | list[str]
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "2.1.0"
