"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class WholeGenomeCoverage(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "coverageSummary",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_2_1_0 import reports as reports_2_1_0

        return {
            "coverageSummary": reports_2_1_0.CoverageSummary,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_2_1_0 import reports as reports_2_1_0

        return {
            "Gel_BioInf_Models.CoverageSummary": reports_2_1_0.CoverageSummary,
            "Gel_BioInf_Models.WholeGenomeCoverage": reports_2_1_0.WholeGenomeCoverage,
        }

    __slots__ = [
        "coverageSummary",
    ]

    def __init__(
        self,
        coverageSummary,
        validate=None,
        **kwargs
    ):
        """Initialise the reports_2_1_0.WholeGenomeCoverage model.

        :param coverageSummary:
        :type coverageSummary: list[CoverageSummary]
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "2.1.0"
