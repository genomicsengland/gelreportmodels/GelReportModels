"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class ClinicalReportCancer(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "cancerParticipant",
        "candidateStructuralVariants",
        "candidateVariants",
        "genePanelsCoverage",
        "genomicInterpretation",
        "interpretationRequestID",
        "interpretationRequestVersion",
        "referenceDatabasesVersions",
        "reportingDate",
        "softwareVersions",
        "user",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_3_0_0 import reports as reports_3_0_0

        return {
            "cancerParticipant": reports_3_0_0.CancerParticipant,
            "candidateStructuralVariants": reports_3_0_0.ReportedSomaticStructuralVariants,
            "candidateVariants": reports_3_0_0.ReportedSomaticVariants,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_3_0_0 import reports as reports_3_0_0

        return {
            "org.gel.models.report.avro.CancerParticipant": reports_3_0_0.CancerParticipant,
            "org.gel.models.report.avro.ReportedSomaticStructuralVariants": reports_3_0_0.ReportedSomaticStructuralVariants,
            "org.gel.models.report.avro.ReportedSomaticVariants": reports_3_0_0.ReportedSomaticVariants,
            "org.gel.models.report.avro.ClinicalReportCancer": reports_3_0_0.ClinicalReportCancer,
        }

    __slots__ = [
        "cancerParticipant",
        "candidateStructuralVariants",
        "candidateVariants",
        "genePanelsCoverage",
        "genomicInterpretation",
        "interpretationRequestID",
        "interpretationRequestVersion",
        "referenceDatabasesVersions",
        "reportingDate",
        "softwareVersions",
        "user",
        "references",
    ]

    def __init__(
        self,
        cancerParticipant,
        candidateStructuralVariants,
        candidateVariants,
        genePanelsCoverage,
        genomicInterpretation,
        interpretationRequestID,
        interpretationRequestVersion,
        referenceDatabasesVersions,
        reportingDate,
        softwareVersions,
        user,
        references=None,
        validate=None,
        **kwargs
    ):
        """Initialise the reports_3_0_0.ClinicalReportCancer model.

        :param cancerParticipant:
        :type cancerParticipant: CancerParticipant
        :param candidateStructuralVariants:
            Candidate Structural Variants - as defined in CommonInterpreted
        :type candidateStructuralVariants: list[ReportedSomaticStructuralVariants]
        :param candidateVariants:
            Candidate Variants - as defined in CommonInterpreted
        :type candidateVariants: list[ReportedSomaticVariants]
        :param genePanelsCoverage:
            This map of key: panel_name, value: (arrays of (map of key: gene,
            value: gene coverage))
        :type genePanelsCoverage: dict[str, list[dict[str, str]]]
        :param genomicInterpretation:
            Summary of the interpretation, this should reflects the positive
            conclusions of this interpretation
        :type genomicInterpretation: str
        :param interpretationRequestID:
            This is the interpretation Request Id, first number in XXX-123-1
        :type interpretationRequestID: str
        :param interpretationRequestVersion:
            This is the version of the interpretation Request Id, second number in
            XXX-123-2
        :type interpretationRequestVersion: str
        :param referenceDatabasesVersions:
            This map should contains the version of the different DBs used in the
            process
        :type referenceDatabasesVersions: dict[str, str]
        :param reportingDate:
            Date of this report
        :type reportingDate: str
        :param softwareVersions:
            This map should contains the version of the different DBs software in
            the process
        :type softwareVersions: dict[str, str]
        :param user:
            Author of this report
        :type user: str
        :param references:
            References (pubmed Ids)
        :type references: None | list[str]
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "3.0.0"
