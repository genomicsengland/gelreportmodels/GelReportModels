"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class KGPopCategory(object):
    """
    1K Population
    """

    ACB = "ACB"
    ASW = "ASW"
    BEB = "BEB"
    CDX = "CDX"
    CEU = "CEU"
    CHB = "CHB"
    CHS = "CHS"
    CLM = "CLM"
    ESN = "ESN"
    FIN = "FIN"
    GBR = "GBR"
    GIH = "GIH"
    GWD = "GWD"
    IBS = "IBS"
    ITU = "ITU"
    JPT = "JPT"
    KHV = "KHV"
    LWK = "LWK"
    MSL = "MSL"
    MXL = "MXL"
    PEL = "PEL"
    PJL = "PJL"
    PUR = "PUR"
    STU = "STU"
    TSI = "TSI"
    YRI = "YRI"

    def __hash__(self):
        return str(self).__hash__()

    @classmethod
    def _namespace_version(cls):
        return "3.0.0"
