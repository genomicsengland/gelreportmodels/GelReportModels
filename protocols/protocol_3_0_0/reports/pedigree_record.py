"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class Pedigree(ProtocolElement):
    """
    This is the concept of a family with associated phenotypes as present
    in the record RDParticipant
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "gelFamilyId",
        "participants",
        "versionControl",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_3_0_0 import reports as reports_3_0_0

        return {
            "analysisPanels": reports_3_0_0.AnalysisPanel,
            "diseasePenetrances": reports_3_0_0.DiseasePenetrance,
            "participants": reports_3_0_0.RDParticipant,
            "versionControl": reports_3_0_0.VersionControl,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_3_0_0 import reports as reports_3_0_0

        return {
            "org.gel.models.report.avro.AnalysisPanel": reports_3_0_0.AnalysisPanel,
            "org.gel.models.report.avro.DiseasePenetrance": reports_3_0_0.DiseasePenetrance,
            "org.gel.models.report.avro.RDParticipant": reports_3_0_0.RDParticipant,
            "org.gel.models.report.avro.VersionControl": reports_3_0_0.VersionControl,
            "org.gel.models.report.avro.Pedigree": reports_3_0_0.Pedigree,
        }

    __slots__ = [
        "gelFamilyId",
        "participants",
        "versionControl",
        "analysisPanels",
        "diseasePenetrances",
    ]

    def __init__(
        self,
        gelFamilyId,
        participants,
        versionControl,
        analysisPanels=None,
        diseasePenetrances=None,
        validate=None,
        **kwargs
    ):
        """Initialise the reports_3_0_0.Pedigree model.

        :param gelFamilyId:
            Family id which internally translate to a sample set
        :type gelFamilyId: str
        :param participants:
        :type participants: list[RDParticipant]
        :param versionControl:
            Model version number
        :type versionControl: VersionControl
        :param analysisPanels:
        :type analysisPanels: None | list[AnalysisPanel]
        :param diseasePenetrances:
        :type diseasePenetrances: None | list[DiseasePenetrance]
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "3.0.0"
