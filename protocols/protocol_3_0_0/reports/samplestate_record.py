"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class sampleState(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "reason",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_3_0_0 import reports as reports_3_0_0

        return {
            "reason": reports_3_0_0.Reason,
            "state": reports_3_0_0.State,
        }

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_3_0_0 import reports as reports_3_0_0

        return {
            "org.gel.models.report.avro.Reason": reports_3_0_0.Reason,
            "org.gel.models.report.avro.State": reports_3_0_0.State,
            "org.gel.models.report.avro.sampleState": reports_3_0_0.sampleState,
        }

    __slots__ = [
        "reason",
        "state",
    ]

    def __init__(
        self,
        reason,
        state=None,
        validate=None,
        **kwargs
    ):
        """Initialise the reports_3_0_0.sampleState model.

        :param reason:
        :type reason: list[Reason]
        :param state:
        :type state: None | State
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "3.0.0"
