"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class VariantClassification(object):
    """
    This is the classification of the variant according to standard
    practice guidelines (e.g. ACMG)
    """

    BENIGN = "BENIGN"
    LIKELY_BENIGN = "LIKELY_BENIGN"
    VUS = "VUS"
    LIKELY_PATHOGENIC = "LIKELY_PATHOGENIC"
    PATHOGENIC = "PATHOGENIC"

    def __hash__(self):
        return str(self).__hash__()

    @classmethod
    def _namespace_version(cls):
        return "3.0.0"
