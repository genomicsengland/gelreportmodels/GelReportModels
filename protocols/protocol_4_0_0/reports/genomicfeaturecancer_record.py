"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class GenomicFeatureCancer(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "ensemblId",
        "featureType",
        "geneName",
        "refSeqProteinId",
        "refSeqTranscriptId",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_4_0_0 import reports as reports_4_0_0

        return {
            "featureType": reports_4_0_0.FeatureTypes,
            "roleInCancer": reports_4_0_0.RoleInCancer,
        }

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_4_0_0 import reports as reports_4_0_0

        return {
            "org.gel.models.report.avro.FeatureTypes": reports_4_0_0.FeatureTypes,
            "org.gel.models.report.avro.RoleInCancer": reports_4_0_0.RoleInCancer,
            "org.gel.models.report.avro.GenomicFeatureCancer": reports_4_0_0.GenomicFeatureCancer,
        }

    __slots__ = [
        "ensemblId",
        "featureType",
        "geneName",
        "refSeqProteinId",
        "refSeqTranscriptId",
        "roleInCancer",
    ]

    def __init__(
        self,
        ensemblId,
        featureType,
        geneName,
        refSeqProteinId,
        refSeqTranscriptId,
        roleInCancer=None,
        validate=None,
        **kwargs
    ):
        """Initialise the reports_4_0_0.GenomicFeatureCancer model.

        :param ensemblId:
            Transcript used, this should be a feature ID from Ensembl, (i.e,
            ENST00000544455)
        :type ensemblId: str
        :param featureType:
            Feature Type
        :type featureType: FeatureTypes
        :param geneName:
            Gene used in tier
        :type geneName: str
        :param refSeqProteinId:
            Refseq protein
        :type refSeqProteinId: str
        :param refSeqTranscriptId:
            Refseq transcript
        :type refSeqTranscriptId: str
        :param roleInCancer:
            Role in cancer: oncogene, TSG or both
        :type roleInCancer: None | RoleInCancer
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "4.0.0"
