"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class StructuralVariantType(ProtocolElement):
    """
    Structural variant type as defined by the VCF specification 4.2 for
    field ID.
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "firstLevelType",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_4_0_0 import reports as reports_4_0_0

        return {
            "firstLevelType": reports_4_0_0.StructuralVariantFirstLevelType,
        }

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_4_0_0 import reports as reports_4_0_0

        return {
            "org.gel.models.report.avro.StructuralVariantFirstLevelType": reports_4_0_0.StructuralVariantFirstLevelType,
            "org.gel.models.report.avro.StructuralVariantType": reports_4_0_0.StructuralVariantType,
        }

    __slots__ = [
        "firstLevelType",
        "subtype",
    ]

    def __init__(
        self,
        firstLevelType,
        subtype=None,
        validate=None,
        **kwargs
    ):
        """Initialise the reports_4_0_0.StructuralVariantType model.

        :param firstLevelType:
        :type firstLevelType: StructuralVariantFirstLevelType
        :param subtype:
        :type subtype: None | str
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "4.0.0"
