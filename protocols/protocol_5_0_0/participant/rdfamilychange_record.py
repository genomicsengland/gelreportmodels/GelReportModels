"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class RDFamilyChange(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "Family",
        "FamilyId",
        "code",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_5_0_0 import participant as participant_1_0_3

        return {
            "Family": participant_1_0_3.Pedigree,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_5_0_0 import participant as participant_1_0_3

        return {
            "code": participant_1_0_3.RDFamilyChangeCode,
        }

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_5_0_0 import participant as participant_1_0_3

        return {
            "org.gel.models.participant.avro.Pedigree": participant_1_0_3.Pedigree,
            "org.gel.models.participant.avro.RDFamilyChangeCode": participant_1_0_3.RDFamilyChangeCode,
            "org.gel.models.participant.avro.RDFamilyChange": participant_1_0_3.RDFamilyChange,
        }

    __slots__ = [
        "Family",
        "FamilyId",
        "code",
    ]

    def __init__(
        self,
        Family,
        FamilyId,
        code,
        validate=None,
        **kwargs
    ):
        """Initialise the participant_1_0_3.RDFamilyChange model.

        :param Family:
            This is the family data tha need to be ingested
        :type Family: Pedigree
        :param FamilyId:
            This is the FamilyId - it is expected to be unique across the whole
            project
        :type FamilyId: str
        :param code:
            This code define a change type
        :type code: RDFamilyChangeCode
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "1.0.3"
