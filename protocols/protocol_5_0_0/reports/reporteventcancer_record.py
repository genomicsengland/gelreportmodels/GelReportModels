"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class ReportEventCancer(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "genomicFeatureCancer",
        "reportEventId",
        "soTerms",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_5_0_0 import reports as reports_4_2_0

        return {
            "actions": reports_4_2_0.Action,
            "genomicFeatureCancer": reports_4_2_0.GenomicFeatureCancer,
            "soTerms": reports_4_2_0.SoTerm,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_5_0_0 import reports as reports_4_2_0

        return {
            "tier": reports_4_2_0.Tier,
        }

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_5_0_0 import reports as reports_4_2_0

        return {
            "org.gel.models.report.avro.Action": reports_4_2_0.Action,
            "org.gel.models.report.avro.GenomicFeatureCancer": reports_4_2_0.GenomicFeatureCancer,
            "org.gel.models.report.avro.SoTerm": reports_4_2_0.SoTerm,
            "org.gel.models.report.avro.Tier": reports_4_2_0.Tier,
            "org.gel.models.report.avro.ReportEventCancer": reports_4_2_0.ReportEventCancer,
        }

    __slots__ = [
        "genomicFeatureCancer",
        "reportEventId",
        "soTerms",
        "actions",
        "eventJustification",
        "groupOfVariants",
        "tier",
    ]

    def __init__(
        self,
        genomicFeatureCancer,
        reportEventId,
        soTerms,
        actions=None,
        eventJustification=None,
        groupOfVariants=None,
        tier=None,
        validate=None,
        **kwargs
    ):
        """Initialise the reports_4_2_0.ReportEventCancer model.

        :param genomicFeatureCancer:
            This is the genomicsFeature of interest for this reported variant,
            please note that one variant can overlap more that one
            gene/transcript         If more than one gene/transcript
            is considered interesting for this particular variant,
            should be reported in two different ReportEvents
        :type genomicFeatureCancer: GenomicFeatureCancer
        :param reportEventId:
            Unique identifier for each report event, this has to be unique across
            the whole report, and it will be used by GEL         to
            validate the report
        :type reportEventId: str
        :param soTerms:
            Sequence Ontology terms used in tier
        :type soTerms: list[SoTerm]
        :param actions:
            Types of actionability
        :type actions: None | list[Action]
        :param eventJustification:
            This is the description of why this variant would be reported, for
            example that it affects the protein in this way
            and that this gene has been implicated in this disorder in
            these publications. Publications should be provided as
            PMIDs         using the format [PMID:8075643]. Other
            sources can be used in the same manner, e.g.
            [OMIM:163500]. Brackets need to be included.
        :type eventJustification: None | str
        :param groupOfVariants:
            This value groups variants that together could explain the phenotype
            according to the         mode of inheritance used. All the
            variants sharing the same value will be considered in the
            same group.         This value is an integer unique in the
            whole analysis.
        :type groupOfVariants: None | int
        :param tier:
            Gel Tier
        :type tier: None | Tier
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "4.2.0"
