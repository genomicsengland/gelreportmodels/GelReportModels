"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class StructuralVariantFirstLevelType(object):
    """
    The first level type must be one of the following:  * `DEL` Deletion
    relative to the reference * `INS` Insertion of novel sequence
    relative to the reference * `DUP` Region of elevated copy number
    relative to the reference * `INV` Inversion of reference sequence
    * `CNV` Copy number variable region (may be both deletion and
    duplication) The CNV category should not be used when a more
    specific category can be applied.  Reserved subtypes include: *
    `DUP:TANDEM` Tandem duplication * `DEL:ME` Deletion of mobile
    element relative to the reference * `INS:ME` Insertion of a mobile
    element relative to the reference
    """

    DEL = "DEL"
    INS = "INS"
    DUP = "DUP"
    INV = "INV"
    CNV = "CNV"
    DUP_TANDEM = "DUP_TANDEM"
    DEL_ME = "DEL_ME"
    INS_ME = "INS_ME"

    def __hash__(self):
        return str(self).__hash__()

    @classmethod
    def _namespace_version(cls):
        return "4.2.0"
