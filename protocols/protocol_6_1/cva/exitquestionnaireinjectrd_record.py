"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class ExitQuestionnaireInjectRD(ProtocolElement):
    """
    Record for exit questionnaire injection as part of the data intake for
    CVA
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "assembly",
        "author",
        "cohortId",
        "groupId",
        "id",
        "parentId",
        "parentVersion",
        "reportModelVersion",
        "version",
        "workspace",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_6_1 import cva as cva_1_0_0
        from protocols.protocol_6_1 import reports as reports_5_0_0

        return {
            "exitQuestionnaireRd": cva_1_0_0.ExitQuestionnaireRD,
            "rareDiseaseExitQuestionnaire": reports_5_0_0.RareDiseaseExitQuestionnaire,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_6_1 import reports as reports_5_0_0

        return {
            "assembly": reports_5_0_0.Assembly,
        }

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_6_1 import cva as cva_1_0_0
        from protocols.protocol_6_1 import reports as reports_5_0_0

        return {
            "org.gel.models.report.avro.Assembly": reports_5_0_0.Assembly,
            "org.gel.models.cva.avro.ExitQuestionnaireRD": cva_1_0_0.ExitQuestionnaireRD,
            "org.gel.models.report.avro.RareDiseaseExitQuestionnaire": reports_5_0_0.RareDiseaseExitQuestionnaire,
            "org.gel.models.cva.avro.ExitQuestionnaireInjectRD": cva_1_0_0.ExitQuestionnaireInjectRD,
        }

    __slots__ = [
        "assembly",
        "author",
        "cohortId",
        "groupId",
        "id",
        "parentId",
        "parentVersion",
        "reportModelVersion",
        "version",
        "workspace",
        "authorVersion",
        "exitQuestionnaireRd",
        "rareDiseaseExitQuestionnaire",
    ]

    def __init__(
        self,
        assembly,
        author,
        cohortId,
        groupId,
        id,
        parentId,
        parentVersion,
        reportModelVersion,
        version,
        workspace,
        authorVersion=None,
        exitQuestionnaireRd=None,
        rareDiseaseExitQuestionnaire=None,
        validate=None,
        **kwargs
    ):
        """Initialise the cva_1_0_0.ExitQuestionnaireInjectRD model.

        :param assembly:
            The assembly to which the variants refer
        :type assembly: Assembly
        :param author:
            The author of the ReportedVariant, either tiering, exomiser, a given
            cip (e.g.: omicia) or a given GMCs user name
        :type author: str
        :param cohortId:
            The cohort identifier (the same family can have several cohorts)
        :type cohortId: str
        :param groupId:
            The family identifier
        :type groupId: str
        :param id:
            The identifier for the clinical report.
        :type id: str
        :param parentId:
            The identifier for the interpretation request
        :type parentId: str
        :param parentVersion:
            The version for the interpretation request
        :type parentVersion: int
        :param reportModelVersion:
            Report avro models version
        :type reportModelVersion: str
        :param version:
            The version for the clinical report. This is a correlative number
            being the highest         value the latest version.
        :type version: int
        :param workspace:
            The genome shall be assigned to the workspaces(projects or domains
            with a predefined set of users) to control user access
        :type workspace: list[str]
        :param authorVersion:
            The author version of the ReportedVariant, either tiering, exomiser or
            a given cip. Only applicable for automated processes.
        :type authorVersion: None | str
        :param exitQuestionnaireRd:
            Exit questionnaire for rare disease
        :type exitQuestionnaireRd: None | ExitQuestionnaireRD
        :param rareDiseaseExitQuestionnaire:
            Rare disease exit questionnaire
        :type rareDiseaseExitQuestionnaire: None | RareDiseaseExitQuestionnaire
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "1.0.0"
