"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class ObservedVariant(ProtocolElement):
    """
    A variant observed in a specific sample. The information about the
    observation is contained within a CalledGenotype, it is linked to
    one abstract Variant.  Every ObservedVariant is uniquely
    identified by:  * sample id * variant identifier (being a variant
    identifier formed by chromosome + position + reference +
    alternate)  Duplication of the prior fields is not to be
    supported.
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "assembly",
        "date",
        "variant",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_6_1 import cva as cva_1_0_0
        from protocols.protocol_6_1 import opencb as opencb_1_3_0
        from protocols.protocol_6_1 import reports as reports_5_0_0

        return {
            "additionalProperties": opencb_1_3_0.Property,
            "variant": cva_1_0_0.Variant,
            "variantCall": reports_5_0_0.VariantCall,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_6_1 import reports as reports_5_0_0

        return {
            "assembly": reports_5_0_0.Assembly,
        }

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_6_1 import cva as cva_1_0_0
        from protocols.protocol_6_1 import opencb as opencb_1_3_0
        from protocols.protocol_6_1 import reports as reports_5_0_0

        return {
            "org.opencb.biodata.models.variant.avro.Property": opencb_1_3_0.Property,
            "org.gel.models.report.avro.Assembly": reports_5_0_0.Assembly,
            "org.gel.models.cva.avro.Variant": cva_1_0_0.Variant,
            "org.gel.models.report.avro.VariantCall": reports_5_0_0.VariantCall,
            "org.gel.models.cva.avro.ObservedVariant": cva_1_0_0.ObservedVariant,
        }

    __slots__ = [
        "assembly",
        "date",
        "variant",
        "additionalProperties",
        "validated",
        "variantCall",
    ]

    def __init__(
        self,
        assembly,
        date,
        variant,
        additionalProperties=list,
        validated=False,
        variantCall=None,
        validate=None,
        **kwargs
    ):
        """Initialise the cva_1_0_0.ObservedVariant model.

        :param assembly:
            The assembly to which the variant refers
        :type assembly: Assembly
        :param date:
            The registration date
        :type date: str
        :param variant:
            The abstract variant
        :type variant: Variant
        :param additionalProperties:
            A list of additional properties in the form name-value.
        :type additionalProperties: list[Property]
        :param validated:
            Validation flag
        :type validated: boolean
        :param variantCall:
            Variant call
        :type variantCall: None | VariantCall
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "1.0.0"
