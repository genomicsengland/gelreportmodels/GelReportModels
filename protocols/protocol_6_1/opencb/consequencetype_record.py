"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class ConsequenceType(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "sequenceOntologyTerms",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_6_1 import opencb as opencb_1_3_0

        return {
            "exonOverlap": opencb_1_3_0.ExonOverlap,
            "proteinVariantAnnotation": opencb_1_3_0.ProteinVariantAnnotation,
            "sequenceOntologyTerms": opencb_1_3_0.SequenceOntologyTerm,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_6_1 import opencb as opencb_1_3_0

        return {
            "org.opencb.biodata.models.variant.avro.ExonOverlap": opencb_1_3_0.ExonOverlap,
            "org.opencb.biodata.models.variant.avro.ProteinVariantAnnotation": opencb_1_3_0.ProteinVariantAnnotation,
            "org.opencb.biodata.models.variant.avro.SequenceOntologyTerm": opencb_1_3_0.SequenceOntologyTerm,
            "org.opencb.biodata.models.variant.avro.ConsequenceType": opencb_1_3_0.ConsequenceType,
        }

    __slots__ = [
        "sequenceOntologyTerms",
        "biotype",
        "cdnaPosition",
        "cdsPosition",
        "codon",
        "ensemblGeneId",
        "ensemblTranscriptId",
        "exonOverlap",
        "geneName",
        "proteinVariantAnnotation",
        "strand",
        "transcriptAnnotationFlags",
    ]

    def __init__(
        self,
        sequenceOntologyTerms,
        biotype=None,
        cdnaPosition=None,
        cdsPosition=None,
        codon=None,
        ensemblGeneId=None,
        ensemblTranscriptId=None,
        exonOverlap=None,
        geneName=None,
        proteinVariantAnnotation=None,
        strand=None,
        transcriptAnnotationFlags=None,
        validate=None,
        **kwargs
    ):
        """Initialise the opencb_1_3_0.ConsequenceType model.

        :param sequenceOntologyTerms:
        :type sequenceOntologyTerms: list[SequenceOntologyTerm]
        :param biotype:
        :type biotype: None | str
        :param cdnaPosition:
        :type cdnaPosition: None | int
        :param cdsPosition:
        :type cdsPosition: None | int
        :param codon:
        :type codon: None | str
        :param ensemblGeneId:
        :type ensemblGeneId: None | str
        :param ensemblTranscriptId:
        :type ensemblTranscriptId: None | str
        :param exonOverlap:
        :type exonOverlap: None | list[ExonOverlap]
        :param geneName:
        :type geneName: None | str
        :param proteinVariantAnnotation:
        :type proteinVariantAnnotation: None | ProteinVariantAnnotation
        :param strand:
        :type strand: None | str
        :param transcriptAnnotationFlags:
        :type transcriptAnnotationFlags: None | list[str]
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "1.3.0"
