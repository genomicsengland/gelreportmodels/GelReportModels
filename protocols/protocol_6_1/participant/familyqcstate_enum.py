"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class FamilyQCState(object):
    """
    FamilyQCState
    """

    noState = "noState"
    passedMedicalReviewReadyForInterpretation = "passedMedicalReviewReadyForInterpretation"
    passedMedicalReviewNotReadyForInterpretation = "passedMedicalReviewNotReadyForInterpretation"
    queryToGel = "queryToGel"
    queryToGMC = "queryToGMC"
    failed = "failed"

    def __hash__(self):
        return str(self).__hash__()

    @classmethod
    def _namespace_version(cls):
        return "1.1.0"
