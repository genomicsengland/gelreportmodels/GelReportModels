"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    SearchRequest,
    avro_parse,
    load_schema,
)


class SearchVariantSetsRequest(SearchRequest):
    """
    This request maps to the body of `POST /variantsets/search` as JSON.
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "datasetId",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_0 import ga4gh as ga4gh_3_0_0

        return {
            "org.ga4gh.methods.SearchVariantSetsRequest": ga4gh_3_0_0.SearchVariantSetsRequest,
        }

    __slots__ = [
        "datasetId",
        "pageSize",
        "pageToken",
    ]

    def __init__(
        self,
        datasetId,
        pageSize=None,
        pageToken=None,
        validate=None,
        **kwargs
    ):
        """Initialise the ga4gh_3_0_0.SearchVariantSetsRequest model.

        :param datasetId:
            The `Dataset` to search.
        :type datasetId: str
        :param pageSize:
            Specifies the maximum number of results to return in a single page.
            If unspecified, a system default will be used.
        :type pageSize: None | int
        :param pageToken:
            The continuation token, which is used to page through large result
            sets.   To get the next page of results, set this
            parameter to the value of   `nextPageToken` from the
            previous response.
        :type pageToken: None | str
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "3.0.0"
