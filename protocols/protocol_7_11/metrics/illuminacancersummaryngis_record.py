"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class IlluminaCancerSummaryNGIS(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "ANALYSIS_VERSION",
        "AUTOSOME_EXON_MEAN_COVERAGE",
        "AUTOSOME_MEAN_COVERAGE",
        "DIVERSITY",
        "FRAGMENT_LENGTH_MAX",
        "FRAGMENT_LENGTH_MEDIAN",
        "FRAGMENT_LENGTH_MIN",
        "FRAGMENT_LENGTH_SD",
        "MAPQ_GT_10_AUTOSOME_MEDIAN_COVERAGE",
        "MEAN_COVERAGE",
        "MEDIAN_READ_LENGTH",
        "MEDIAN_READ_LENGTH_READ_1",
        "MEDIAN_READ_LENGTH_READ_2",
        "METRICS_DELIVERABLE",
        "METRICS_VERSION",
        "PAIRED_END",
        "PERCENT_ADAPTER_BASES",
        "PERCENT_ALIGNED_BASES",
        "PERCENT_ALIGNED_BASES_READ_1",
        "PERCENT_ALIGNED_BASES_READ_2",
        "PERCENT_ALIGNED_READS",
        "PERCENT_ALIGNED_READ_1",
        "PERCENT_ALIGNED_READ_2",
        "PERCENT_AT_DROPOUT",
        "PERCENT_AT_DROPOUT_ALIGNED",
        "PERCENT_AUTOSOME_COVERAGE_AT_10X",
        "PERCENT_AUTOSOME_COVERAGE_AT_15X",
        "PERCENT_AUTOSOME_COVERAGE_AT_1X",
        "PERCENT_AUTOSOME_EXON_COVERAGE_AT_10X",
        "PERCENT_AUTOSOME_EXON_COVERAGE_AT_15X",
        "PERCENT_AUTOSOME_EXON_COVERAGE_AT_1X",
        "PERCENT_COVERAGE_AT_10X",
        "PERCENT_COVERAGE_AT_15X",
        "PERCENT_COVERAGE_AT_1X",
        "PERCENT_DUPLICATE_ALIGNED_READS",
        "PERCENT_DUPLICATE_PROPER_READ_PAIRS",
        "PERCENT_GC_DROPOUT",
        "PERCENT_GC_DROPOUT_ALIGNED",
        "PERCENT_MAPQ_GT_10_AUTOSOME_COVERAGE_AT_15X",
        "PERCENT_MAPQ_GT_10_AUTOSOME_EXON_COVERAGE_AT_15X",
        "PERCENT_MISMATCHES",
        "PERCENT_MISMATCHES_READ_1",
        "PERCENT_MISMATCHES_READ_2",
        "PERCENT_NONSPATIAL_DUPLICATE_READ_PAIRS",
        "PERCENT_OVERLAPPING_BASES",
        "PERCENT_Q25_BASES_READ_1",
        "PERCENT_Q25_BASES_READ_2",
        "PERCENT_Q30_BASES",
        "PERCENT_Q30_BASES_READ_1",
        "PERCENT_Q30_BASES_READ_2",
        "PERCENT_READ_PAIRS_ALIGNED_TO_DIFFERENT_CHROMOSOMES",
        "PERCENT_SOFTCLIPPED_BASES",
        "PROVIDED_SEX_CHROMOSOME_PLOIDY",
        "Q30_GIGABASES_EXCLUDING_CLIPPED_AND_DUPLICATE_READ_BASES",
        "READ_ENRICHMENT_AT_75_PERCENT_GC",
        "READ_ENRICHMENT_AT_80_PERCENT_GC",
        "REFERENCE_GENOME",
        "SAMPLE_ID",
        "SAMPLE_NAME",
        "TOTAL_ALIGNED_BASES",
        "TOTAL_ALIGNED_BASES_READ_1",
        "TOTAL_ALIGNED_BASES_READ_2",
        "TOTAL_ALIGNED_READS",
        "TOTAL_ALIGNED_READ_1",
        "TOTAL_ALIGNED_READ_2",
        "TOTAL_ALIGNED_READ_PAIRS",
        "TOTAL_DUPLICATE_ALIGNED_READS",
        "TOTAL_DUPLICATE_PROPER_READ_PAIRS",
        "TOTAL_GIGABASES_EXCLUDING_CLIPPED_AND_DUPLICATE_READ_BASES",
        "TOTAL_MAPQ_GT_10_READS",
        "TOTAL_PF_BASES",
        "TOTAL_PF_BASES_READ_1",
        "TOTAL_PF_BASES_READ_2",
        "TOTAL_PF_READS",
        "TOTAL_PF_READ_1",
        "TOTAL_PF_READ_2",
        "TOTAL_PROPER_READ_PAIRS",
        "UNIQUE_ALIGNED_READS",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_11 import metrics as metrics_1_2_2

        return {
            "org.gel.models.report.avro.IlluminaCancerSummaryNGIS": metrics_1_2_2.IlluminaCancerSummaryNGIS,
        }

    __slots__ = [
        "ANALYSIS_VERSION",
        "AUTOSOME_EXON_MEAN_COVERAGE",
        "AUTOSOME_MEAN_COVERAGE",
        "DIVERSITY",
        "FRAGMENT_LENGTH_MAX",
        "FRAGMENT_LENGTH_MEDIAN",
        "FRAGMENT_LENGTH_MIN",
        "FRAGMENT_LENGTH_SD",
        "MAPQ_GT_10_AUTOSOME_MEDIAN_COVERAGE",
        "MEAN_COVERAGE",
        "MEDIAN_READ_LENGTH",
        "MEDIAN_READ_LENGTH_READ_1",
        "MEDIAN_READ_LENGTH_READ_2",
        "METRICS_DELIVERABLE",
        "METRICS_VERSION",
        "PAIRED_END",
        "PERCENT_ADAPTER_BASES",
        "PERCENT_ALIGNED_BASES",
        "PERCENT_ALIGNED_BASES_READ_1",
        "PERCENT_ALIGNED_BASES_READ_2",
        "PERCENT_ALIGNED_READS",
        "PERCENT_ALIGNED_READ_1",
        "PERCENT_ALIGNED_READ_2",
        "PERCENT_AT_DROPOUT",
        "PERCENT_AT_DROPOUT_ALIGNED",
        "PERCENT_AUTOSOME_COVERAGE_AT_10X",
        "PERCENT_AUTOSOME_COVERAGE_AT_15X",
        "PERCENT_AUTOSOME_COVERAGE_AT_1X",
        "PERCENT_AUTOSOME_EXON_COVERAGE_AT_10X",
        "PERCENT_AUTOSOME_EXON_COVERAGE_AT_15X",
        "PERCENT_AUTOSOME_EXON_COVERAGE_AT_1X",
        "PERCENT_COVERAGE_AT_10X",
        "PERCENT_COVERAGE_AT_15X",
        "PERCENT_COVERAGE_AT_1X",
        "PERCENT_DUPLICATE_ALIGNED_READS",
        "PERCENT_DUPLICATE_PROPER_READ_PAIRS",
        "PERCENT_GC_DROPOUT",
        "PERCENT_GC_DROPOUT_ALIGNED",
        "PERCENT_MAPQ_GT_10_AUTOSOME_COVERAGE_AT_15X",
        "PERCENT_MAPQ_GT_10_AUTOSOME_EXON_COVERAGE_AT_15X",
        "PERCENT_MISMATCHES",
        "PERCENT_MISMATCHES_READ_1",
        "PERCENT_MISMATCHES_READ_2",
        "PERCENT_NONSPATIAL_DUPLICATE_READ_PAIRS",
        "PERCENT_OVERLAPPING_BASES",
        "PERCENT_Q25_BASES_READ_1",
        "PERCENT_Q25_BASES_READ_2",
        "PERCENT_Q30_BASES",
        "PERCENT_Q30_BASES_READ_1",
        "PERCENT_Q30_BASES_READ_2",
        "PERCENT_READ_PAIRS_ALIGNED_TO_DIFFERENT_CHROMOSOMES",
        "PERCENT_SOFTCLIPPED_BASES",
        "PROVIDED_SEX_CHROMOSOME_PLOIDY",
        "Q30_GIGABASES_EXCLUDING_CLIPPED_AND_DUPLICATE_READ_BASES",
        "READ_ENRICHMENT_AT_75_PERCENT_GC",
        "READ_ENRICHMENT_AT_80_PERCENT_GC",
        "REFERENCE_GENOME",
        "SAMPLE_ID",
        "SAMPLE_NAME",
        "TOTAL_ALIGNED_BASES",
        "TOTAL_ALIGNED_BASES_READ_1",
        "TOTAL_ALIGNED_BASES_READ_2",
        "TOTAL_ALIGNED_READS",
        "TOTAL_ALIGNED_READ_1",
        "TOTAL_ALIGNED_READ_2",
        "TOTAL_ALIGNED_READ_PAIRS",
        "TOTAL_DUPLICATE_ALIGNED_READS",
        "TOTAL_DUPLICATE_PROPER_READ_PAIRS",
        "TOTAL_GIGABASES_EXCLUDING_CLIPPED_AND_DUPLICATE_READ_BASES",
        "TOTAL_MAPQ_GT_10_READS",
        "TOTAL_PF_BASES",
        "TOTAL_PF_BASES_READ_1",
        "TOTAL_PF_BASES_READ_2",
        "TOTAL_PF_READS",
        "TOTAL_PF_READ_1",
        "TOTAL_PF_READ_2",
        "TOTAL_PROPER_READ_PAIRS",
        "UNIQUE_ALIGNED_READS",
    ]

    def __init__(
        self,
        ANALYSIS_VERSION,
        AUTOSOME_EXON_MEAN_COVERAGE,
        AUTOSOME_MEAN_COVERAGE,
        DIVERSITY,
        FRAGMENT_LENGTH_MAX,
        FRAGMENT_LENGTH_MEDIAN,
        FRAGMENT_LENGTH_MIN,
        FRAGMENT_LENGTH_SD,
        MAPQ_GT_10_AUTOSOME_MEDIAN_COVERAGE,
        MEAN_COVERAGE,
        MEDIAN_READ_LENGTH,
        MEDIAN_READ_LENGTH_READ_1,
        MEDIAN_READ_LENGTH_READ_2,
        METRICS_DELIVERABLE,
        METRICS_VERSION,
        PAIRED_END,
        PERCENT_ADAPTER_BASES,
        PERCENT_ALIGNED_BASES,
        PERCENT_ALIGNED_BASES_READ_1,
        PERCENT_ALIGNED_BASES_READ_2,
        PERCENT_ALIGNED_READS,
        PERCENT_ALIGNED_READ_1,
        PERCENT_ALIGNED_READ_2,
        PERCENT_AT_DROPOUT,
        PERCENT_AT_DROPOUT_ALIGNED,
        PERCENT_AUTOSOME_COVERAGE_AT_10X,
        PERCENT_AUTOSOME_COVERAGE_AT_15X,
        PERCENT_AUTOSOME_COVERAGE_AT_1X,
        PERCENT_AUTOSOME_EXON_COVERAGE_AT_10X,
        PERCENT_AUTOSOME_EXON_COVERAGE_AT_15X,
        PERCENT_AUTOSOME_EXON_COVERAGE_AT_1X,
        PERCENT_COVERAGE_AT_10X,
        PERCENT_COVERAGE_AT_15X,
        PERCENT_COVERAGE_AT_1X,
        PERCENT_DUPLICATE_ALIGNED_READS,
        PERCENT_DUPLICATE_PROPER_READ_PAIRS,
        PERCENT_GC_DROPOUT,
        PERCENT_GC_DROPOUT_ALIGNED,
        PERCENT_MAPQ_GT_10_AUTOSOME_COVERAGE_AT_15X,
        PERCENT_MAPQ_GT_10_AUTOSOME_EXON_COVERAGE_AT_15X,
        PERCENT_MISMATCHES,
        PERCENT_MISMATCHES_READ_1,
        PERCENT_MISMATCHES_READ_2,
        PERCENT_NONSPATIAL_DUPLICATE_READ_PAIRS,
        PERCENT_OVERLAPPING_BASES,
        PERCENT_Q25_BASES_READ_1,
        PERCENT_Q25_BASES_READ_2,
        PERCENT_Q30_BASES,
        PERCENT_Q30_BASES_READ_1,
        PERCENT_Q30_BASES_READ_2,
        PERCENT_READ_PAIRS_ALIGNED_TO_DIFFERENT_CHROMOSOMES,
        PERCENT_SOFTCLIPPED_BASES,
        PROVIDED_SEX_CHROMOSOME_PLOIDY,
        Q30_GIGABASES_EXCLUDING_CLIPPED_AND_DUPLICATE_READ_BASES,
        READ_ENRICHMENT_AT_75_PERCENT_GC,
        READ_ENRICHMENT_AT_80_PERCENT_GC,
        REFERENCE_GENOME,
        SAMPLE_ID,
        SAMPLE_NAME,
        TOTAL_ALIGNED_BASES,
        TOTAL_ALIGNED_BASES_READ_1,
        TOTAL_ALIGNED_BASES_READ_2,
        TOTAL_ALIGNED_READS,
        TOTAL_ALIGNED_READ_1,
        TOTAL_ALIGNED_READ_2,
        TOTAL_ALIGNED_READ_PAIRS,
        TOTAL_DUPLICATE_ALIGNED_READS,
        TOTAL_DUPLICATE_PROPER_READ_PAIRS,
        TOTAL_GIGABASES_EXCLUDING_CLIPPED_AND_DUPLICATE_READ_BASES,
        TOTAL_MAPQ_GT_10_READS,
        TOTAL_PF_BASES,
        TOTAL_PF_BASES_READ_1,
        TOTAL_PF_BASES_READ_2,
        TOTAL_PF_READS,
        TOTAL_PF_READ_1,
        TOTAL_PF_READ_2,
        TOTAL_PROPER_READ_PAIRS,
        UNIQUE_ALIGNED_READS,
        validate=None,
        **kwargs
    ):
        """Initialise the metrics_1_2_2.IlluminaCancerSummaryNGIS model.

        :param ANALYSIS_VERSION:
        :type ANALYSIS_VERSION: str
        :param AUTOSOME_EXON_MEAN_COVERAGE:
        :type AUTOSOME_EXON_MEAN_COVERAGE: double
        :param AUTOSOME_MEAN_COVERAGE:
        :type AUTOSOME_MEAN_COVERAGE: double
        :param DIVERSITY:
        :type DIVERSITY: int
        :param FRAGMENT_LENGTH_MAX:
        :type FRAGMENT_LENGTH_MAX: int
        :param FRAGMENT_LENGTH_MEDIAN:
        :type FRAGMENT_LENGTH_MEDIAN: int
        :param FRAGMENT_LENGTH_MIN:
        :type FRAGMENT_LENGTH_MIN: int
        :param FRAGMENT_LENGTH_SD:
        :type FRAGMENT_LENGTH_SD: int
        :param MAPQ_GT_10_AUTOSOME_MEDIAN_COVERAGE:
        :type MAPQ_GT_10_AUTOSOME_MEDIAN_COVERAGE: double
        :param MEAN_COVERAGE:
        :type MEAN_COVERAGE: double
        :param MEDIAN_READ_LENGTH:
        :type MEDIAN_READ_LENGTH: int
        :param MEDIAN_READ_LENGTH_READ_1:
        :type MEDIAN_READ_LENGTH_READ_1: int
        :param MEDIAN_READ_LENGTH_READ_2:
        :type MEDIAN_READ_LENGTH_READ_2: int
        :param METRICS_DELIVERABLE:
        :type METRICS_DELIVERABLE: str
        :param METRICS_VERSION:
        :type METRICS_VERSION: str
        :param PAIRED_END:
        :type PAIRED_END: boolean
        :param PERCENT_ADAPTER_BASES:
        :type PERCENT_ADAPTER_BASES: double
        :param PERCENT_ALIGNED_BASES:
        :type PERCENT_ALIGNED_BASES: double
        :param PERCENT_ALIGNED_BASES_READ_1:
        :type PERCENT_ALIGNED_BASES_READ_1: double
        :param PERCENT_ALIGNED_BASES_READ_2:
        :type PERCENT_ALIGNED_BASES_READ_2: double
        :param PERCENT_ALIGNED_READS:
        :type PERCENT_ALIGNED_READS: double
        :param PERCENT_ALIGNED_READ_1:
        :type PERCENT_ALIGNED_READ_1: double
        :param PERCENT_ALIGNED_READ_2:
        :type PERCENT_ALIGNED_READ_2: double
        :param PERCENT_AT_DROPOUT:
        :type PERCENT_AT_DROPOUT: double
        :param PERCENT_AT_DROPOUT_ALIGNED:
        :type PERCENT_AT_DROPOUT_ALIGNED: double
        :param PERCENT_AUTOSOME_COVERAGE_AT_10X:
        :type PERCENT_AUTOSOME_COVERAGE_AT_10X: double
        :param PERCENT_AUTOSOME_COVERAGE_AT_15X:
        :type PERCENT_AUTOSOME_COVERAGE_AT_15X: double
        :param PERCENT_AUTOSOME_COVERAGE_AT_1X:
        :type PERCENT_AUTOSOME_COVERAGE_AT_1X: double
        :param PERCENT_AUTOSOME_EXON_COVERAGE_AT_10X:
        :type PERCENT_AUTOSOME_EXON_COVERAGE_AT_10X: double
        :param PERCENT_AUTOSOME_EXON_COVERAGE_AT_15X:
        :type PERCENT_AUTOSOME_EXON_COVERAGE_AT_15X: double
        :param PERCENT_AUTOSOME_EXON_COVERAGE_AT_1X:
        :type PERCENT_AUTOSOME_EXON_COVERAGE_AT_1X: double
        :param PERCENT_COVERAGE_AT_10X:
        :type PERCENT_COVERAGE_AT_10X: double
        :param PERCENT_COVERAGE_AT_15X:
        :type PERCENT_COVERAGE_AT_15X: double
        :param PERCENT_COVERAGE_AT_1X:
        :type PERCENT_COVERAGE_AT_1X: double
        :param PERCENT_DUPLICATE_ALIGNED_READS:
        :type PERCENT_DUPLICATE_ALIGNED_READS: double
        :param PERCENT_DUPLICATE_PROPER_READ_PAIRS:
        :type PERCENT_DUPLICATE_PROPER_READ_PAIRS: double
        :param PERCENT_GC_DROPOUT:
        :type PERCENT_GC_DROPOUT: double
        :param PERCENT_GC_DROPOUT_ALIGNED:
        :type PERCENT_GC_DROPOUT_ALIGNED: double
        :param PERCENT_MAPQ_GT_10_AUTOSOME_COVERAGE_AT_15X:
        :type PERCENT_MAPQ_GT_10_AUTOSOME_COVERAGE_AT_15X: double
        :param PERCENT_MAPQ_GT_10_AUTOSOME_EXON_COVERAGE_AT_15X:
        :type PERCENT_MAPQ_GT_10_AUTOSOME_EXON_COVERAGE_AT_15X: double
        :param PERCENT_MISMATCHES:
        :type PERCENT_MISMATCHES: double
        :param PERCENT_MISMATCHES_READ_1:
        :type PERCENT_MISMATCHES_READ_1: double
        :param PERCENT_MISMATCHES_READ_2:
        :type PERCENT_MISMATCHES_READ_2: double
        :param PERCENT_NONSPATIAL_DUPLICATE_READ_PAIRS:
        :type PERCENT_NONSPATIAL_DUPLICATE_READ_PAIRS: double
        :param PERCENT_OVERLAPPING_BASES:
        :type PERCENT_OVERLAPPING_BASES: double
        :param PERCENT_Q25_BASES_READ_1:
        :type PERCENT_Q25_BASES_READ_1: double
        :param PERCENT_Q25_BASES_READ_2:
        :type PERCENT_Q25_BASES_READ_2: double
        :param PERCENT_Q30_BASES:
        :type PERCENT_Q30_BASES: double
        :param PERCENT_Q30_BASES_READ_1:
        :type PERCENT_Q30_BASES_READ_1: double
        :param PERCENT_Q30_BASES_READ_2:
        :type PERCENT_Q30_BASES_READ_2: double
        :param PERCENT_READ_PAIRS_ALIGNED_TO_DIFFERENT_CHROMOSOMES:
        :type PERCENT_READ_PAIRS_ALIGNED_TO_DIFFERENT_CHROMOSOMES: double
        :param PERCENT_SOFTCLIPPED_BASES:
        :type PERCENT_SOFTCLIPPED_BASES: double
        :param PROVIDED_SEX_CHROMOSOME_PLOIDY:
        :type PROVIDED_SEX_CHROMOSOME_PLOIDY: str
        :param Q30_GIGABASES_EXCLUDING_CLIPPED_AND_DUPLICATE_READ_BASES:
        :type Q30_GIGABASES_EXCLUDING_CLIPPED_AND_DUPLICATE_READ_BASES: double
        :param READ_ENRICHMENT_AT_75_PERCENT_GC:
        :type READ_ENRICHMENT_AT_75_PERCENT_GC: double
        :param READ_ENRICHMENT_AT_80_PERCENT_GC:
        :type READ_ENRICHMENT_AT_80_PERCENT_GC: double
        :param REFERENCE_GENOME:
        :type REFERENCE_GENOME: str
        :param SAMPLE_ID:
        :type SAMPLE_ID: str
        :param SAMPLE_NAME:
        :type SAMPLE_NAME: str
        :param TOTAL_ALIGNED_BASES:
        :type TOTAL_ALIGNED_BASES: int
        :param TOTAL_ALIGNED_BASES_READ_1:
        :type TOTAL_ALIGNED_BASES_READ_1: int
        :param TOTAL_ALIGNED_BASES_READ_2:
        :type TOTAL_ALIGNED_BASES_READ_2: int
        :param TOTAL_ALIGNED_READS:
        :type TOTAL_ALIGNED_READS: int
        :param TOTAL_ALIGNED_READ_1:
        :type TOTAL_ALIGNED_READ_1: int
        :param TOTAL_ALIGNED_READ_2:
        :type TOTAL_ALIGNED_READ_2: int
        :param TOTAL_ALIGNED_READ_PAIRS:
        :type TOTAL_ALIGNED_READ_PAIRS: int
        :param TOTAL_DUPLICATE_ALIGNED_READS:
        :type TOTAL_DUPLICATE_ALIGNED_READS: int
        :param TOTAL_DUPLICATE_PROPER_READ_PAIRS:
        :type TOTAL_DUPLICATE_PROPER_READ_PAIRS: int
        :param TOTAL_GIGABASES_EXCLUDING_CLIPPED_AND_DUPLICATE_READ_BASES:
        :type TOTAL_GIGABASES_EXCLUDING_CLIPPED_AND_DUPLICATE_READ_BASES: double
        :param TOTAL_MAPQ_GT_10_READS:
        :type TOTAL_MAPQ_GT_10_READS: int
        :param TOTAL_PF_BASES:
        :type TOTAL_PF_BASES: int
        :param TOTAL_PF_BASES_READ_1:
        :type TOTAL_PF_BASES_READ_1: int
        :param TOTAL_PF_BASES_READ_2:
        :type TOTAL_PF_BASES_READ_2: int
        :param TOTAL_PF_READS:
        :type TOTAL_PF_READS: int
        :param TOTAL_PF_READ_1:
        :type TOTAL_PF_READ_1: int
        :param TOTAL_PF_READ_2:
        :type TOTAL_PF_READ_2: int
        :param TOTAL_PROPER_READ_PAIRS:
        :type TOTAL_PROPER_READ_PAIRS: int
        :param UNIQUE_ALIGNED_READS:
        :type UNIQUE_ALIGNED_READS: int
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "1.2.2"
