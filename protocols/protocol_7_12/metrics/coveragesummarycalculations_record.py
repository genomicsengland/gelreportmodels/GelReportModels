"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class CoverageSummaryCalculations(ProtocolElement):
    """
    Stats calculated from values stored in CoverageSummary
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "lt30x_percent",
        "scope",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_12 import metrics as metrics_1_2_2

        return {
            "org.gel.models.report.avro.CoverageSummaryCalculations": metrics_1_2_2.CoverageSummaryCalculations,
        }

    __slots__ = [
        "lt30x_percent",
        "scope",
    ]

    def __init__(
        self,
        lt30x_percent,
        scope,
        validate=None,
        **kwargs
    ):
        """Initialise the metrics_1_2_2.CoverageSummaryCalculations model.

        :param lt30x_percent:
            Percentage of Cosmic variants with coverage below 30x
        :type lt30x_percent: double
        :param scope:
            Scope to which these summary stats refer to. *   Examples: "allchrs"
            (all chromosomes), "X", "chr6", "6", "autosomes"
        :type scope: str
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "1.2.2"
