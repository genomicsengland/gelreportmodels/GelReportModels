"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class StructuralVariation(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {}

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_13 import opencb as opencb_1_3_1

        return {
            "type": opencb_1_3_1.StructuralVariantType,
        }

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_13 import opencb as opencb_1_3_1

        return {
            "org.opencb.biodata.models.variant.avro.StructuralVariantType": opencb_1_3_1.StructuralVariantType,
            "org.opencb.biodata.models.variant.avro.StructuralVariation": opencb_1_3_1.StructuralVariation,
        }

    __slots__ = [
        "ciEndLeft",
        "ciEndRight",
        "ciStartLeft",
        "ciStartRight",
        "copyNumber",
        "leftSvInsSeq",
        "rightSvInsSeq",
        "type",
    ]

    def __init__(
        self,
        ciEndLeft=None,
        ciEndRight=None,
        ciStartLeft=None,
        ciStartRight=None,
        copyNumber=None,
        leftSvInsSeq=None,
        rightSvInsSeq=None,
        type=None,
        validate=None,
        **kwargs
    ):
        """Initialise the opencb_1_3_1.StructuralVariation model.

        :param ciEndLeft:
        :type ciEndLeft: None | int
        :param ciEndRight:
        :type ciEndRight: None | int
        :param ciStartLeft:
        :type ciStartLeft: None | int
        :param ciStartRight:
        :type ciStartRight: None | int
        :param copyNumber:
            * Number of copies for CNV variants.
        :type copyNumber: None | int
        :param leftSvInsSeq:
            * Inserted sequence for long INS         *
        :type leftSvInsSeq: None | str
        :param rightSvInsSeq:
        :type rightSvInsSeq: None | str
        :param type:
            * Structural variation type: COPY_NUMBER_GAIN, COPY_NUMBER_LOSS,
            TANDEM_DUPLICATION, ...
        :type type: None | StructuralVariantType
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "1.3.1"
