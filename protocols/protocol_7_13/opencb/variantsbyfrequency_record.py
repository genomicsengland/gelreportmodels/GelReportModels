"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class VariantsByFrequency(ProtocolElement):
    """
    Counts the number of variants within a certain frequency range.
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "count",
        "endFrequency",
        "startFrequency",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_13 import opencb as opencb_1_3_1

        return {
            "org.opencb.biodata.models.variant.metadata.VariantsByFrequency": opencb_1_3_1.VariantsByFrequency,
        }

    __slots__ = [
        "count",
        "endFrequency",
        "startFrequency",
    ]

    def __init__(
        self,
        count,
        endFrequency,
        startFrequency,
        validate=None,
        **kwargs
    ):
        """Initialise the opencb_1_3_1.VariantsByFrequency model.

        :param count:
            Number of variants with this frequency
        :type count: int
        :param endFrequency:
            Exclusive frequency range end
        :type endFrequency: float
        :param startFrequency:
            Inclusive frequency range start
        :type startFrequency: float
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "1.3.1"
