"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class CancerInterpretationRequestGMS(ProtocolElement):
    """
    This record represents basic information for this report
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "genomeAssembly",
        "internalStudyId",
        "interpretationRequestId",
        "interpretationRequestVersion",
        "referral",
        "versionControl",
        "workspace",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_13 import participant as participant_1_4_0
        from protocols.protocol_7_13 import reports as reports_6_5_0

        return {
            "annotationFiles": reports_6_5_0.File,
            "bams": reports_6_5_0.File,
            "bigWigs": reports_6_5_0.File,
            "interpretationFlags": reports_6_5_0.InterpretationFlag,
            "otherFamilyHistory": reports_6_5_0.OtherFamilyHistory,
            "otherFiles": reports_6_5_0.File,
            "referral": participant_1_4_0.Referral,
            "vcfs": reports_6_5_0.File,
            "versionControl": reports_6_5_0.ReportVersionControl,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_13 import reports as reports_6_5_0

        return {
            "genomeAssembly": reports_6_5_0.Assembly,
        }

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_13 import participant as participant_1_4_0
        from protocols.protocol_7_13 import reports as reports_6_5_0

        return {
            "org.gel.models.report.avro.File": reports_6_5_0.File,
            "org.gel.models.report.avro.File": reports_6_5_0.File,
            "org.gel.models.report.avro.File": reports_6_5_0.File,
            "org.gel.models.report.avro.Assembly": reports_6_5_0.Assembly,
            "org.gel.models.report.avro.InterpretationFlag": reports_6_5_0.InterpretationFlag,
            "org.gel.models.report.avro.OtherFamilyHistory": reports_6_5_0.OtherFamilyHistory,
            "org.gel.models.report.avro.File": reports_6_5_0.File,
            "org.gel.models.participant.avro.Referral": participant_1_4_0.Referral,
            "org.gel.models.report.avro.File": reports_6_5_0.File,
            "org.gel.models.report.avro.ReportVersionControl": reports_6_5_0.ReportVersionControl,
            "org.gel.models.report.avro.CancerInterpretationRequestGMS": reports_6_5_0.CancerInterpretationRequestGMS,
        }

    __slots__ = [
        "genomeAssembly",
        "internalStudyId",
        "interpretationRequestId",
        "interpretationRequestVersion",
        "referral",
        "versionControl",
        "workspace",
        "additionalInfo",
        "annotationFiles",
        "bams",
        "bigWigs",
        "genePanelsCoverage",
        "interpretationFlags",
        "otherFamilyHistory",
        "otherFiles",
        "participantInternalId",
        "vcfs",
    ]

    def __init__(
        self,
        genomeAssembly,
        internalStudyId,
        interpretationRequestId,
        interpretationRequestVersion,
        referral,
        versionControl,
        workspace,
        additionalInfo=None,
        annotationFiles=None,
        bams=None,
        bigWigs=None,
        genePanelsCoverage=None,
        interpretationFlags=None,
        otherFamilyHistory=None,
        otherFiles=None,
        participantInternalId=None,
        vcfs=None,
        validate=None,
        **kwargs
    ):
        """Initialise the reports_6_5_0.CancerInterpretationRequestGMS model.

        :param genomeAssembly:
            This is the version of the assembly used to align the reads
        :type genomeAssembly: Assembly
        :param internalStudyId:
            Internal study identifier
        :type internalStudyId: str
        :param interpretationRequestId:
            Identifier for this interpretation request
        :type interpretationRequestId: str
        :param interpretationRequestVersion:
            Version for this interpretation request
        :type interpretationRequestVersion: int
        :param referral:
            Referral data for this interpretation request.
        :type referral: Referral
        :param versionControl:
            Model version number
        :type versionControl: ReportVersionControl
        :param workspace:
            The genome shall be assigned to the workspaces(projects or domains
            with a predefined set of users) to control user access
        :type workspace: list[str]
        :param additionalInfo:
            Additional information
        :type additionalInfo: None | dict[str, str]
        :param annotationFiles:
            Variant Annotation Files from CellBase.
        :type annotationFiles: None | list[File]
        :param bams:
            BAMs Files
        :type bams: None | list[File]
        :param bigWigs:
            BigWig Files
        :type bigWigs: None | list[File]
        :param genePanelsCoverage:
            This map of key: panel_name, value: (map of key: gene, value: (map of
            metrics of key: metric name, value: float)) That is: a map
            of tables of genes and metrics
        :type genePanelsCoverage: None | dict[str, dict[str, dict[str, float]]]
        :param interpretationFlags:
            Flags about this case relevant for interpretation
        :type interpretationFlags: None | list[InterpretationFlag]
        :param otherFamilyHistory:
            It is paternal or maternal with reference to the participant.
        :type otherFamilyHistory: None | OtherFamilyHistory
        :param otherFiles:
            Other files that may be vendor specific map of key: type of file,
            value: record of type File
        :type otherFiles: None | dict[str, File]
        :param participantInternalId:
            Participant internal identifier
        :type participantInternalId: None | str
        :param vcfs:
            VCFs Files where SVs and CNVs are represented
        :type vcfs: None | list[File]
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "6.5.0"
