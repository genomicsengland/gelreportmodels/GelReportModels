"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class GeneTraitAssociation(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "id",
        "name",
        "source",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_2 import opencb as opencb_1_3_0

        return {
            "org.opencb.biodata.models.variant.avro.GeneTraitAssociation": opencb_1_3_0.GeneTraitAssociation,
        }

    __slots__ = [
        "id",
        "name",
        "source",
        "associationTypes",
        "hpo",
        "numberOfPubmeds",
        "score",
        "sources",
    ]

    def __init__(
        self,
        id,
        name,
        source,
        associationTypes=None,
        hpo=None,
        numberOfPubmeds=None,
        score=None,
        sources=None,
        validate=None,
        **kwargs
    ):
        """Initialise the opencb_1_3_0.GeneTraitAssociation model.

        :param id:
        :type id: str
        :param name:
        :type name: str
        :param source:
        :type source: str
        :param associationTypes:
        :type associationTypes: None | list[str]
        :param hpo:
        :type hpo: None | str
        :param numberOfPubmeds:
        :type numberOfPubmeds: None | int
        :param score:
        :type score: None | float
        :param sources:
        :type sources: None | list[str]
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "1.3.0"
