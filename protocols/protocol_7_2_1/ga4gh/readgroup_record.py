"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class ReadGroup(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "id",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_2_1 import ga4gh as ga4gh_3_0_0

        return {
            "experiment": ga4gh_3_0_0.Experiment,
            "programs": ga4gh_3_0_0.Program,
            "stats": ga4gh_3_0_0.ReadStats,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_2_1 import ga4gh as ga4gh_3_0_0

        return {
            "org.ga4gh.models.Experiment": ga4gh_3_0_0.Experiment,
            "org.ga4gh.models.Program": ga4gh_3_0_0.Program,
            "org.ga4gh.models.ReadStats": ga4gh_3_0_0.ReadStats,
            "org.ga4gh.models.ReadGroup": ga4gh_3_0_0.ReadGroup,
        }

    __slots__ = [
        "id",
        "created",
        "datasetId",
        "description",
        "experiment",
        "info",
        "name",
        "predictedInsertSize",
        "programs",
        "referenceSetId",
        "sampleId",
        "stats",
        "updated",
    ]

    def __init__(
        self,
        id,
        created=None,
        datasetId=None,
        description=None,
        experiment=None,
        info=dict,
        name=None,
        predictedInsertSize=None,
        programs=list,
        referenceSetId=None,
        sampleId=None,
        stats=None,
        updated=None,
        validate=None,
        **kwargs
    ):
        """Initialise the ga4gh_3_0_0.ReadGroup model.

        :param id:
            The read group ID.
        :type id: str
        :param created:
            The time at which this read group was created in milliseconds from the
            epoch.
        :type created: None | int
        :param datasetId:
            The ID of the dataset this read group belongs to.
        :type datasetId: None | str
        :param description:
            The read group description.
        :type description: None | str
        :param experiment:
            The experiment used to generate this read group.
        :type experiment: None | Experiment
        :param info:
            A map of additional read group information.
        :type info: dict[str, list[str]]
        :param name:
            The read group name.
        :type name: None | str
        :param predictedInsertSize:
            The predicted insert size of this read group.
        :type predictedInsertSize: None | int
        :param programs:
            The programs used to generate this read group.
        :type programs: list[Program]
        :param referenceSetId:
            The reference set the reads in this read group are aligned to.
            Required if there are any read alignments.
        :type referenceSetId: None | str
        :param sampleId:
            The sample this read group's data was generated from.
        :type sampleId: None | str
        :param stats:
            Statistical data on reads in this read group.
        :type stats: None | ReadStats
        :param updated:
            The time at which this read group was last updated in milliseconds
            from the epoch.
        :type updated: None | int
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "3.0.0"
