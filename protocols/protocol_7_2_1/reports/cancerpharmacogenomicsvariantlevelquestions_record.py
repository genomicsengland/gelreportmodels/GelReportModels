"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class CancerPharmacogenomicsVariantLevelQuestions(ProtocolElement):
    """
    The questions for the cancer program exit questionnaire for PGX
    variants
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "validationAssayType",
        "variantActionability",
        "variantCoordinates",
        "variantTested",
        "variantUsability",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_2_1 import reports as reports_6_0_2

        return {
            "variantCoordinates": reports_6_0_2.VariantCoordinates,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_2_1 import reports as reports_6_0_2

        return {
            "variantActionability": reports_6_0_2.CancerActionabilityPharmacogenomics,
            "variantTested": reports_6_0_2.CancerTested,
            "variantUsability": reports_6_0_2.CancerUsabilityPharmacogenomics,
        }

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_2_1 import reports as reports_6_0_2

        return {
            "org.gel.models.report.avro.CancerActionabilityPharmacogenomics": reports_6_0_2.CancerActionabilityPharmacogenomics,
            "org.gel.models.report.avro.VariantCoordinates": reports_6_0_2.VariantCoordinates,
            "org.gel.models.report.avro.CancerTested": reports_6_0_2.CancerTested,
            "org.gel.models.report.avro.CancerUsabilityPharmacogenomics": reports_6_0_2.CancerUsabilityPharmacogenomics,
            "org.gel.models.report.avro.CancerPharmacogenomicsVariantLevelQuestions": reports_6_0_2.CancerPharmacogenomicsVariantLevelQuestions,
        }

    __slots__ = [
        "validationAssayType",
        "variantActionability",
        "variantCoordinates",
        "variantTested",
        "variantUsability",
    ]

    def __init__(
        self,
        validationAssayType,
        variantActionability,
        variantCoordinates,
        variantTested,
        variantUsability,
        validate=None,
        **kwargs
    ):
        """Initialise the reports_6_0_2.CancerPharmacogenomicsVariantLevelQuestions model.

        :param validationAssayType:
            Please enter validation assay type e.g Pyrosequencing, NGS panel,
            COBAS, Sanger sequencing. If not applicable enter NA;
        :type validationAssayType: str
        :param variantActionability:
            Type of potential actionability:
        :type variantActionability: list[CancerActionabilityPharmacogenomics]
        :param variantCoordinates:
            Variant coordinates following format
            `chromosome:position:reference:alternate`
        :type variantCoordinates: VariantCoordinates
        :param variantTested:
            Has this variant been tested by another method (either prior to or
            following receipt of this WGA)?
        :type variantTested: CancerTested
        :param variantUsability:
            How has/will this potentially actionable variant been/be used?
        :type variantUsability: CancerUsabilityPharmacogenomics
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "6.0.2"
