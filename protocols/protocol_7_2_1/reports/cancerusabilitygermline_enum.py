"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class CancerUsabilityGermline(object):
    """
    Variant usability for germline variants: * `already_actioned`: Already
    actioned (i.e. prior to receiving this WGA) *
    `actioned_result_of_this_wga`: actioned as a result of receiving
    this WGA
    """

    already_actioned = "already_actioned"
    actioned_result_of_this_wga = "actioned_result_of_this_wga"

    def __hash__(self):
        return str(self).__hash__()

    @classmethod
    def _namespace_version(cls):
        return "6.0.2"
