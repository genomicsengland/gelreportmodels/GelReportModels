"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class Therapy(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "referenceUrl",
        "variantActionable",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_2_1 import reports as reports_6_0_2

        return {
            "drugResponse": reports_6_0_2.DrugResponse,
            "otherInterventions": reports_6_0_2.Intervention,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_2_1 import reports as reports_6_0_2

        return {
            "org.gel.models.report.avro.DrugResponse": reports_6_0_2.DrugResponse,
            "org.gel.models.report.avro.Intervention": reports_6_0_2.Intervention,
            "org.gel.models.report.avro.Therapy": reports_6_0_2.Therapy,
        }

    __slots__ = [
        "referenceUrl",
        "variantActionable",
        "conditions",
        "drugResponse",
        "otherInterventions",
        "references",
        "source",
    ]

    def __init__(
        self,
        referenceUrl,
        variantActionable,
        conditions=None,
        drugResponse=None,
        otherInterventions=None,
        references=None,
        source=None,
        validate=None,
        **kwargs
    ):
        """Initialise the reports_6_0_2.Therapy model.

        :param referenceUrl:
            URL where reference information for this therapy association can be
            found
        :type referenceUrl: str
        :param variantActionable:
            If true, the association was made at the variant level, if not the
            association was made at Genomic Entity level
        :type variantActionable: boolean
        :param conditions:
            Conditions
        :type conditions: None | list[str]
        :param drugResponse:
            Drug responses
        :type drugResponse: None | list[DrugResponse]
        :param otherInterventions:
            Any other clinical intervention
        :type otherInterventions: None | list[Intervention]
        :param references:
            References
        :type references: None | list[str]
        :param source:
            Source
        :type source: None | str
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "6.0.2"
