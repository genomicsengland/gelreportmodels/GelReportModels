"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class ListReferenceBasesResponse(ProtocolElement):
    """
    The response from `GET /references/{id}/bases` expressed as JSON.
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "sequence",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_3 import ga4gh as ga4gh_3_0_0

        return {
            "org.ga4gh.methods.ListReferenceBasesResponse": ga4gh_3_0_0.ListReferenceBasesResponse,
        }

    __slots__ = [
        "sequence",
        "nextPageToken",
        "offset",
    ]

    def __init__(
        self,
        sequence,
        nextPageToken=None,
        offset=0,
        validate=None,
        **kwargs
    ):
        """Initialise the ga4gh_3_0_0.ListReferenceBasesResponse model.

        :param sequence:
            A substring of the bases that make up this reference. Bases are
            represented   as IUPAC-IUB codes; this string matches the
            regexp `[ACGTMRWSYKVHDBN]*`.
        :type sequence: str
        :param nextPageToken:
            The continuation token, which is used to page through large result
            sets.   Provide this value in a subsequent request to
            return the next page of   results. This field will be
            empty if there aren't any additional results.
        :type nextPageToken: None | str
        :param offset:
            The offset position (0-based) of the given sequence from the start of
            this   `Reference`. This value will differ for each page
            in a paginated request.
        :type offset: int
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "3.0.0"
