"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class RareDiseaseInterpretationStatus(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "cohortName",
        "groupId",
        "piepelineId",
        "pipelineMainFolder",
        "pipelineParameters",
        "readyToDispatch",
        "startDate",
        "tieringConfigurationFile",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_3 import metrics as metrics_1_2_1

        return {
            "listOfFilesToDispatch": metrics_1_2_1.File,
            "steps": metrics_1_2_1.Step,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_3 import metrics as metrics_1_2_1

        return {
            "org.gel.models.metrics.avro.File": metrics_1_2_1.File,
            "org.gel.models.metrics.avro.Step": metrics_1_2_1.Step,
            "org.gel.models.metrics.avro.RareDiseaseInterpretationStatus": metrics_1_2_1.RareDiseaseInterpretationStatus,
        }

    __slots__ = [
        "cohortName",
        "groupId",
        "piepelineId",
        "pipelineMainFolder",
        "pipelineParameters",
        "readyToDispatch",
        "startDate",
        "tieringConfigurationFile",
        "dataBaseVersions",
        "lastStepDate",
        "listOfFilesToDispatch",
        "listOfSamples",
        "softwareVersions",
        "steps",
    ]

    def __init__(
        self,
        cohortName,
        groupId,
        piepelineId,
        pipelineMainFolder,
        pipelineParameters,
        readyToDispatch,
        startDate,
        tieringConfigurationFile,
        dataBaseVersions=None,
        lastStepDate=None,
        listOfFilesToDispatch=None,
        listOfSamples=None,
        softwareVersions=None,
        steps=None,
        validate=None,
        **kwargs
    ):
        """Initialise the metrics_1_2_1.RareDiseaseInterpretationStatus model.

        :param cohortName:
        :type cohortName: str
        :param groupId:
        :type groupId: str
        :param piepelineId:
        :type piepelineId: str
        :param pipelineMainFolder:
        :type pipelineMainFolder: str
        :param pipelineParameters:
        :type pipelineParameters: dict[str, str]
        :param readyToDispatch:
        :type readyToDispatch: boolean
        :param startDate:
        :type startDate: str
        :param tieringConfigurationFile:
        :type tieringConfigurationFile: str
        :param dataBaseVersions:
        :type dataBaseVersions: None | dict[str, str]
        :param lastStepDate:
        :type lastStepDate: None | str
        :param listOfFilesToDispatch:
        :type listOfFilesToDispatch: None | list[File]
        :param listOfSamples:
        :type listOfSamples: None | list[str]
        :param softwareVersions:
        :type softwareVersions: None | dict[str, str]
        :param steps:
        :type steps: None | list[Step]
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "1.2.1"
