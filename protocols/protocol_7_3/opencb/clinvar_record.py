"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class ClinVar(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {}

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_3 import opencb as opencb_1_3_0

        return {
            "org.opencb.biodata.models.variant.avro.ClinVar": opencb_1_3_0.ClinVar,
        }

    __slots__ = [
        "accession",
        "clinicalSignificance",
        "geneNames",
        "reviewStatus",
        "traits",
    ]

    def __init__(
        self,
        accession=None,
        clinicalSignificance=None,
        geneNames=None,
        reviewStatus=None,
        traits=None,
        validate=None,
        **kwargs
    ):
        """Initialise the opencb_1_3_0.ClinVar model.

        :param accession:
        :type accession: None | str
        :param clinicalSignificance:
        :type clinicalSignificance: None | str
        :param geneNames:
        :type geneNames: None | list[str]
        :param reviewStatus:
        :type reviewStatus: None | str
        :param traits:
        :type traits: None | list[str]
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "1.3.0"
