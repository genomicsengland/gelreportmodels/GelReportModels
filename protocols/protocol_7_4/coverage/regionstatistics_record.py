"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class RegionStatistics(ProtocolElement):
    """
    Represents a group of coverage statistics over a genomic region
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "avg",
        "gte15x",
        "gte30x",
        "gte50x",
        "lt15x",
        "med",
        "pct25",
        "pct75",
        "sd",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_4 import coverage as coverage_0_1_0

        return {
            "org.gel.models.coverage.avro.RegionStatistics": coverage_0_1_0.RegionStatistics,
        }

    __slots__ = [
        "avg",
        "gte15x",
        "gte30x",
        "gte50x",
        "lt15x",
        "med",
        "pct25",
        "pct75",
        "sd",
        "bases",
        "bases_gte_15x",
        "bases_gte_30x",
        "bases_gte_50x",
        "bases_lt_15x",
        "gc",
        "rmsd",
    ]

    def __init__(
        self,
        avg,
        gte15x,
        gte30x,
        gte50x,
        lt15x,
        med,
        pct25,
        pct75,
        sd,
        bases=None,
        bases_gte_15x=None,
        bases_gte_30x=None,
        bases_gte_50x=None,
        bases_lt_15x=None,
        gc=None,
        rmsd=None,
        validate=None,
        **kwargs
    ):
        """Initialise the coverage_0_1_0.RegionStatistics model.

        :param avg:
            The average depth of coverage
        :type avg: float
        :param gte15x:
            The number of positions with a depth of coverage greater than or equal
            than 15x         (the original name is %>=15x, but this
            cannot be used as a property name in Avro)
        :type gte15x: float
        :param gte30x:
            The number of positions with a depth of coverage greater than or equal
            than 30x         (the original name is %>=30x, but this
            cannot be used as a property name in Avro)
        :type gte30x: float
        :param gte50x:
            The number of positions with a depth of coverage greater than or equal
            than 50x         (the original name is %>=50x, but this
            cannot be used as a property name in Avro)
        :type gte50x: float
        :param lt15x:
            The number of positions with a depth of coverage less than 15x
            (the original name is %<15x, but this cannot be used as a
            property name in Avro)
        :type lt15x: float
        :param med:
            The median depth of coverage
        :type med: float
        :param pct25:
            The 25th percentile of coverage distribution
        :type pct25: float
        :param pct75:
            The 75th percentile of coverage distribution
        :type pct75: float
        :param sd:
            The depth of coverage standard deviation
        :type sd: float
        :param bases:
            The number of bases in the region (this is nullable as it can be
            calculated from coordinates of the region)
        :type bases: None | int
        :param bases_gte_15x:
            The number of bases in the region with a coverage greater than or
            equal 15x (this is nullable as it can be calculated from
            coordinates of the region)
        :type bases_gte_15x: None | int
        :param bases_gte_30x:
            The number of bases in the region with a coverage greater than or
            equal 30x (this is nullable as it can be calculated from
            coordinates of the region)
        :type bases_gte_30x: None | int
        :param bases_gte_50x:
            The number of bases in the region with a coverage greater than or
            equal 50x (this is nullable as it can be calculated from
            coordinates of the region)
        :type bases_gte_50x: None | int
        :param bases_lt_15x:
            The number of bases in the region with a coverage less than 15x (this
            is nullable as it can be calculated from coordinates of
            the region)
        :type bases_lt_15x: None | int
        :param gc:
            The GC content
        :type gc: None | float
        :param rmsd:
            The squared root sum of squares of the deviation from the mean
        :type rmsd: None | float
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "0.1.0"
