"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class Genotype(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "alternate",
        "phased",
        "reference",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_4 import opencb as opencb_1_3_0

        return {
            "org.opencb.biodata.models.variant.avro.Genotype": opencb_1_3_0.Genotype,
        }

    __slots__ = [
        "alternate",
        "phased",
        "reference",
        "allelesIdx",
    ]

    def __init__(
        self,
        alternate,
        phased,
        reference,
        allelesIdx=list,
        validate=None,
        **kwargs
    ):
        """Initialise the opencb_1_3_0.Genotype model.

        :param alternate:
        :type alternate: str
        :param phased:
        :type phased: boolean
        :param reference:
        :type reference: str
        :param allelesIdx:
        :type allelesIdx: list[int]
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "1.3.0"
