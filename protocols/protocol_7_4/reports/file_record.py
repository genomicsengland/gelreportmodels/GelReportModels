"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class File(ProtocolElement):
    """
    This defines a file     This record is uniquely defined by the sample
    identfier and an URI     Currently sample identifier can be a
    single string or a list of strings if multiple samples are
    associated with the same file     *
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "fileType",
        "uriFile",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_4 import reports as reports_6_1_0

        return {
            "fileType": reports_6_1_0.FileType,
        }

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_4 import reports as reports_6_1_0

        return {
            "org.gel.models.report.avro.FileType": reports_6_1_0.FileType,
            "org.gel.models.report.avro.File": reports_6_1_0.File,
        }

    __slots__ = [
        "fileType",
        "uriFile",
        "md5Sum",
        "sampleId",
    ]

    def __init__(
        self,
        fileType,
        uriFile,
        md5Sum=None,
        sampleId=None,
        validate=None,
        **kwargs
    ):
        """Initialise the reports_6_1_0.File model.

        :param fileType:
            The type of the file
        :type fileType: FileType
        :param uriFile:
            URI path of the file
        :type uriFile: str
        :param md5Sum:
            The MD5 checksum
        :type md5Sum: None | str
        :param sampleId:
            Unique identifier(s) of the sample. For example in a multisample vcf
            this would have an array of all the sample identifiers
        :type sampleId: None | list[str]
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "6.1.0"
