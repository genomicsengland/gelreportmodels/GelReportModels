"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class Cytoband(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {}

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_6 import opencb as opencb_1_3_0

        return {
            "org.opencb.biodata.models.variant.avro.Cytoband": opencb_1_3_0.Cytoband,
        }

    __slots__ = [
        "chromosome",
        "end",
        "name",
        "stain",
        "start",
    ]

    def __init__(
        self,
        chromosome=None,
        end=None,
        name=None,
        stain=None,
        start=None,
        validate=None,
        **kwargs
    ):
        """Initialise the opencb_1_3_0.Cytoband model.

        :param chromosome:
        :type chromosome: None | str
        :param end:
        :type end: None | int
        :param name:
        :type name: None | str
        :param stain:
        :type stain: None | str
        :param start:
        :type start: None | int
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "1.3.0"
