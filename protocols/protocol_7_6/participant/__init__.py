from protocols.protocol_7_6.participant.adoptedstatus_enum import (
    AdoptedStatus,
)
from protocols.protocol_7_6.participant.affectionstatus_enum import (
    AffectionStatus,
)
from protocols.protocol_7_6.participant.ageofonset_enum import (
    AgeOfOnset,
)
from protocols.protocol_7_6.participant.analysispanel_record import (
    AnalysisPanel,
)
from protocols.protocol_7_6.participant.ancestries_record import (
    Ancestries,
)
from protocols.protocol_7_6.participant.cancerparticipant_record import (
    CancerParticipant,
)
from protocols.protocol_7_6.participant.chisquare1kgenomesphase3pop_record import (
    ChiSquare1KGenomesPhase3Pop,
)
from protocols.protocol_7_6.participant.clinicalethnicity_enum import (
    ClinicalEthnicity,
)
from protocols.protocol_7_6.participant.clinicalindication_record import (
    ClinicalIndication,
)
from protocols.protocol_7_6.participant.clinicalindicationtest_record import (
    ClinicalIndicationTest,
)
from protocols.protocol_7_6.participant.consentstatus_record import (
    ConsentStatus,
)
from protocols.protocol_7_6.participant.date_record import (
    Date,
)
from protocols.protocol_7_6.participant.diagnosticdetail_record import (
    DiagnosticDetail,
)
from protocols.protocol_7_6.participant.diseasepenetrance_record import (
    DiseasePenetrance,
)
from protocols.protocol_7_6.participant.diseasetype_enum import (
    diseaseType,
)
from protocols.protocol_7_6.participant.disorder_record import (
    Disorder,
)
from protocols.protocol_7_6.participant.ethniccategory_enum import (
    EthnicCategory,
)
from protocols.protocol_7_6.participant.familiarrelationship_enum import (
    FamiliarRelationship,
)
from protocols.protocol_7_6.participant.familyqcstate_enum import (
    FamilyQCState,
)
from protocols.protocol_7_6.participant.genericconsent_enum import (
    GenericConsent,
)
from protocols.protocol_7_6.participant.germlinesample_record import (
    GermlineSample,
)
from protocols.protocol_7_6.participant.gmsconsentstatus_record import (
    GmsConsentStatus,
)
from protocols.protocol_7_6.participant.haematologicalcancerlineage_enum import (
    HaematologicalCancerLineage,
)
from protocols.protocol_7_6.participant.hpoterm_record import (
    HpoTerm,
)
from protocols.protocol_7_6.participant.hpotermmodifiers_record import (
    HpoTermModifiers,
)
from protocols.protocol_7_6.participant.inbreedingcoefficient_record import (
    InbreedingCoefficient,
)
from protocols.protocol_7_6.participant.kgpopcategory_enum import (
    KgPopCategory,
)
from protocols.protocol_7_6.participant.kgsuperpopcategory_enum import (
    KgSuperPopCategory,
)
from protocols.protocol_7_6.participant.laterality_enum import (
    Laterality,
)
from protocols.protocol_7_6.participant.lifestatus_enum import (
    LifeStatus,
)
from protocols.protocol_7_6.participant.matchedsamples_record import (
    MatchedSamples,
)
from protocols.protocol_7_6.participant.method_enum import (
    Method,
)
from protocols.protocol_7_6.participant.morphology_record import (
    Morphology,
)
from protocols.protocol_7_6.participant.organisationngis_record import (
    OrganisationNgis,
)
from protocols.protocol_7_6.participant.participantqcstate_enum import (
    ParticipantQCState,
)
from protocols.protocol_7_6.participant.pedigree_record import (
    Pedigree,
)
from protocols.protocol_7_6.participant.pedigreemember_record import (
    PedigreeMember,
)
from protocols.protocol_7_6.participant.penetrance_enum import (
    Penetrance,
)
from protocols.protocol_7_6.participant.personkaryotipicsex_enum import (
    PersonKaryotipicSex,
)
from protocols.protocol_7_6.participant.preparationmethod_enum import (
    PreparationMethod,
)
from protocols.protocol_7_6.participant.previoustreatment_record import (
    PreviousTreatment,
)
from protocols.protocol_7_6.participant.primaryormetastatic_enum import (
    PrimaryOrMetastatic,
)
from protocols.protocol_7_6.participant.priority_enum import (
    Priority,
)
from protocols.protocol_7_6.participant.product_enum import (
    Product,
)
from protocols.protocol_7_6.participant.programmephase_enum import (
    ProgrammePhase,
)
from protocols.protocol_7_6.participant.progression_enum import (
    Progression,
)
from protocols.protocol_7_6.participant.referral_record import (
    Referral,
)
from protocols.protocol_7_6.participant.referraltest_record import (
    ReferralTest,
)
from protocols.protocol_7_6.participant.samplesource_enum import (
    SampleSource,
)
from protocols.protocol_7_6.participant.sensitiveinformation_record import (
    SensitiveInformation,
)
from protocols.protocol_7_6.participant.severity_enum import (
    Severity,
)
from protocols.protocol_7_6.participant.sex_enum import (
    Sex,
)
from protocols.protocol_7_6.participant.spatialpattern_enum import (
    SpatialPattern,
)
from protocols.protocol_7_6.participant.storagemedium_enum import (
    StorageMedium,
)
from protocols.protocol_7_6.participant.technology_record import (
    Technology,
)
from protocols.protocol_7_6.participant.ternaryoption_enum import (
    TernaryOption,
)
from protocols.protocol_7_6.participant.tissuesource_enum import (
    TissueSource,
)
from protocols.protocol_7_6.participant.topography_record import (
    Topography,
)
from protocols.protocol_7_6.participant.tumour_record import (
    Tumour,
)
from protocols.protocol_7_6.participant.tumourcontent_enum import (
    TumourContent,
)
from protocols.protocol_7_6.participant.tumourpresentation_enum import (
    TumourPresentation,
)
from protocols.protocol_7_6.participant.tumoursample_record import (
    TumourSample,
)
from protocols.protocol_7_6.participant.tumourtype_enum import (
    TumourType,
)
from protocols.protocol_7_6.participant.versioncontrol_record import (
    VersionControl,
)

__all__ = [
    "AdoptedStatus",
    "AffectionStatus",
    "AgeOfOnset",
    "AnalysisPanel",
    "Ancestries",
    "CancerParticipant",
    "ChiSquare1KGenomesPhase3Pop",
    "ClinicalEthnicity",
    "ClinicalIndication",
    "ClinicalIndicationTest",
    "ConsentStatus",
    "Date",
    "DiagnosticDetail",
    "DiseasePenetrance",
    "diseaseType",
    "Disorder",
    "EthnicCategory",
    "FamiliarRelationship",
    "FamilyQCState",
    "GenericConsent",
    "GermlineSample",
    "GmsConsentStatus",
    "HaematologicalCancerLineage",
    "HpoTerm",
    "HpoTermModifiers",
    "InbreedingCoefficient",
    "KgPopCategory",
    "KgSuperPopCategory",
    "Laterality",
    "LifeStatus",
    "MatchedSamples",
    "Method",
    "Morphology",
    "OrganisationNgis",
    "ParticipantQCState",
    "Pedigree",
    "PedigreeMember",
    "Penetrance",
    "PersonKaryotipicSex",
    "PreparationMethod",
    "PreviousTreatment",
    "PrimaryOrMetastatic",
    "Priority",
    "Product",
    "ProgrammePhase",
    "Progression",
    "Referral",
    "ReferralTest",
    "SampleSource",
    "SensitiveInformation",
    "Severity",
    "Sex",
    "SpatialPattern",
    "StorageMedium",
    "Technology",
    "TernaryOption",
    "TissueSource",
    "Topography",
    "Tumour",
    "TumourContent",
    "TumourPresentation",
    "TumourSample",
    "TumourType",
    "VersionControl",
]
