"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class EvidenceEntryAndVariants(ProtocolElement):
    """
    An evidence entry and the variants coordinates to which it corresponds
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "evidenceEntry",
        "variantsCoordinates",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_7 import cva as cva_1_5_1
        from protocols.protocol_7_7 import opencb as opencb_1_3_0
        from protocols.protocol_7_7 import reports as reports_6_1_1

        return {
            "actions": reports_6_1_1.Actions,
            "evidenceEntry": opencb_1_3_0.EvidenceEntry,
            "markersCoordinates": cva_1_5_1.VariantsCoordinates,
            "variantsCoordinates": cva_1_5_1.VariantsCoordinates,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_7 import cva as cva_1_5_1
        from protocols.protocol_7_7 import opencb as opencb_1_3_0
        from protocols.protocol_7_7 import reports as reports_6_1_1

        return {
            "org.gel.models.report.avro.Actions": reports_6_1_1.Actions,
            "org.opencb.biodata.models.variant.avro.EvidenceEntry": opencb_1_3_0.EvidenceEntry,
            "org.gel.models.cva.avro.VariantsCoordinates": cva_1_5_1.VariantsCoordinates,
            "org.gel.models.cva.avro.VariantsCoordinates": cva_1_5_1.VariantsCoordinates,
            "org.gel.models.cva.avro.EvidenceEntryAndVariants": cva_1_5_1.EvidenceEntryAndVariants,
        }

    __slots__ = [
        "evidenceEntry",
        "variantsCoordinates",
        "actions",
        "markersCoordinates",
    ]

    def __init__(
        self,
        evidenceEntry,
        variantsCoordinates,
        actions=None,
        markersCoordinates=None,
        validate=None,
        **kwargs
    ):
        """Initialise the cva_1_5_1.EvidenceEntryAndVariants model.

        :param evidenceEntry:
        :type evidenceEntry: EvidenceEntry
        :param variantsCoordinates:
        :type variantsCoordinates: VariantsCoordinates
        :param actions:
        :type actions: None | Actions
        :param markersCoordinates:
        :type markersCoordinates: None | VariantsCoordinates
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "1.5.1"
