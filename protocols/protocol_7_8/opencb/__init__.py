from protocols.protocol_7_8.opencb.additionalattribute_record import (
    AdditionalAttribute,
)
from protocols.protocol_7_8.opencb.aggregation_enum import (
    Aggregation,
)
from protocols.protocol_7_8.opencb.alleleorigin_enum import (
    AlleleOrigin,
)
from protocols.protocol_7_8.opencb.allelescode_enum import (
    AllelesCode,
)
from protocols.protocol_7_8.opencb.alternatecoordinate_record import (
    AlternateCoordinate,
)
from protocols.protocol_7_8.opencb.chromosomestats_record import (
    ChromosomeStats,
)
from protocols.protocol_7_8.opencb.clinicalsignificance_enum import (
    ClinicalSignificance,
)
from protocols.protocol_7_8.opencb.clinvar_record import (
    ClinVar,
)
from protocols.protocol_7_8.opencb.cohort_record import (
    Cohort,
)
from protocols.protocol_7_8.opencb.confidence_enum import (
    Confidence,
)
from protocols.protocol_7_8.opencb.consequencetype_record import (
    ConsequenceType,
)
from protocols.protocol_7_8.opencb.consistencystatus_enum import (
    ConsistencyStatus,
)
from protocols.protocol_7_8.opencb.cosmic_record import (
    Cosmic,
)
from protocols.protocol_7_8.opencb.cytoband_record import (
    Cytoband,
)
from protocols.protocol_7_8.opencb.drug_record import (
    Drug,
)
from protocols.protocol_7_8.opencb.drugresponseclassification_enum import (
    DrugResponseClassification,
)
from protocols.protocol_7_8.opencb.ethniccategory_enum import (
    EthnicCategory,
)
from protocols.protocol_7_8.opencb.evidenceentry_record import (
    EvidenceEntry,
)
from protocols.protocol_7_8.opencb.evidenceimpact_enum import (
    EvidenceImpact,
)
from protocols.protocol_7_8.opencb.evidencesource_record import (
    EvidenceSource,
)
from protocols.protocol_7_8.opencb.evidencesubmission_record import (
    EvidenceSubmission,
)
from protocols.protocol_7_8.opencb.exonoverlap_record import (
    ExonOverlap,
)
from protocols.protocol_7_8.opencb.experiment_record import (
    Experiment,
)
from protocols.protocol_7_8.opencb.expression_record import (
    Expression,
)
from protocols.protocol_7_8.opencb.expressioncall_enum import (
    ExpressionCall,
)
from protocols.protocol_7_8.opencb.featuretypes_enum import (
    FeatureTypes,
)
from protocols.protocol_7_8.opencb.fileentry_record import (
    FileEntry,
)
from protocols.protocol_7_8.opencb.genedruginteraction_record import (
    GeneDrugInteraction,
)
from protocols.protocol_7_8.opencb.genetraitassociation_record import (
    GeneTraitAssociation,
)
from protocols.protocol_7_8.opencb.genomicfeature_record import (
    GenomicFeature,
)
from protocols.protocol_7_8.opencb.genotype_record import (
    Genotype,
)
from protocols.protocol_7_8.opencb.gwas_record import (
    Gwas,
)
from protocols.protocol_7_8.opencb.heritabletrait_record import (
    HeritableTrait,
)
from protocols.protocol_7_8.opencb.individual_record import (
    Individual,
)
from protocols.protocol_7_8.opencb.modeofinheritance_enum import (
    ModeOfInheritance,
)
from protocols.protocol_7_8.opencb.penetrance_enum import (
    Penetrance,
)
from protocols.protocol_7_8.opencb.populationfrequency_record import (
    PopulationFrequency,
)
from protocols.protocol_7_8.opencb.program_record import (
    Program,
)
from protocols.protocol_7_8.opencb.property_record import (
    Property,
)
from protocols.protocol_7_8.opencb.proteinfeature_record import (
    ProteinFeature,
)
from protocols.protocol_7_8.opencb.proteinvariantannotation_record import (
    ProteinVariantAnnotation,
)
from protocols.protocol_7_8.opencb.read_record import (
    Read,
)
from protocols.protocol_7_8.opencb.repeat_record import (
    Repeat,
)
from protocols.protocol_7_8.opencb.sample_record import (
    Sample,
)
from protocols.protocol_7_8.opencb.samplesettype_enum import (
    SampleSetType,
)
from protocols.protocol_7_8.opencb.score_record import (
    Score,
)
from protocols.protocol_7_8.opencb.sequenceontologyterm_record import (
    SequenceOntologyTerm,
)
from protocols.protocol_7_8.opencb.somaticinformation_record import (
    SomaticInformation,
)
from protocols.protocol_7_8.opencb.species_record import (
    Species,
)
from protocols.protocol_7_8.opencb.structuralvarianttype_enum import (
    StructuralVariantType,
)
from protocols.protocol_7_8.opencb.structuralvariation_record import (
    StructuralVariation,
)
from protocols.protocol_7_8.opencb.studyentry_record import (
    StudyEntry,
)
from protocols.protocol_7_8.opencb.traitassociation_enum import (
    TraitAssociation,
)
from protocols.protocol_7_8.opencb.tumorigenesisclassification_enum import (
    TumorigenesisClassification,
)
from protocols.protocol_7_8.opencb.variantannotation_record import (
    VariantAnnotation,
)
from protocols.protocol_7_8.opencb.variantavro_record import (
    VariantAvro,
)
from protocols.protocol_7_8.opencb.variantclassification_record import (
    VariantClassification,
)
from protocols.protocol_7_8.opencb.variantfileheader_record import (
    VariantFileHeader,
)
from protocols.protocol_7_8.opencb.variantfileheadercomplexline_record import (
    VariantFileHeaderComplexLine,
)
from protocols.protocol_7_8.opencb.variantfileheadersimpleline_record import (
    VariantFileHeaderSimpleLine,
)
from protocols.protocol_7_8.opencb.variantfilemetadata_record import (
    VariantFileMetadata,
)
from protocols.protocol_7_8.opencb.variantfunctionaleffect_enum import (
    VariantFunctionalEffect,
)
from protocols.protocol_7_8.opencb.variantglobalstats_record import (
    VariantGlobalStats,
)
from protocols.protocol_7_8.opencb.varianthardyweinbergstats_record import (
    VariantHardyWeinbergStats,
)
from protocols.protocol_7_8.opencb.variantmetadata_record import (
    VariantMetadata,
)
from protocols.protocol_7_8.opencb.variantsbyfrequency_record import (
    VariantsByFrequency,
)
from protocols.protocol_7_8.opencb.variantsetstats_record import (
    VariantSetStats,
)
from protocols.protocol_7_8.opencb.variantsource_record import (
    VariantSource,
)
from protocols.protocol_7_8.opencb.variantstats_record import (
    VariantStats,
)
from protocols.protocol_7_8.opencb.variantstudymetadata_record import (
    VariantStudyMetadata,
)
from protocols.protocol_7_8.opencb.variantstudystats_record import (
    VariantStudyStats,
)
from protocols.protocol_7_8.opencb.varianttraitassociation_record import (
    VariantTraitAssociation,
)
from protocols.protocol_7_8.opencb.varianttype_enum import (
    VariantType,
)
from protocols.protocol_7_8.opencb.vcfheader_record import (
    VcfHeader,
)
from protocols.protocol_7_8.opencb.xref_record import (
    Xref,
)

__all__ = [
    "AdditionalAttribute",
    "Aggregation",
    "AlleleOrigin",
    "AllelesCode",
    "AlternateCoordinate",
    "ChromosomeStats",
    "ClinicalSignificance",
    "ClinVar",
    "Cohort",
    "Confidence",
    "ConsequenceType",
    "ConsistencyStatus",
    "Cosmic",
    "Cytoband",
    "Drug",
    "DrugResponseClassification",
    "EthnicCategory",
    "EvidenceEntry",
    "EvidenceImpact",
    "EvidenceSource",
    "EvidenceSubmission",
    "ExonOverlap",
    "Experiment",
    "Expression",
    "ExpressionCall",
    "FeatureTypes",
    "FileEntry",
    "GeneDrugInteraction",
    "GeneTraitAssociation",
    "GenomicFeature",
    "Genotype",
    "Gwas",
    "HeritableTrait",
    "Individual",
    "ModeOfInheritance",
    "Penetrance",
    "PopulationFrequency",
    "Program",
    "Property",
    "ProteinFeature",
    "ProteinVariantAnnotation",
    "Read",
    "Repeat",
    "Sample",
    "SampleSetType",
    "Score",
    "SequenceOntologyTerm",
    "SomaticInformation",
    "Species",
    "StructuralVariantType",
    "StructuralVariation",
    "StudyEntry",
    "TraitAssociation",
    "TumorigenesisClassification",
    "VariantAnnotation",
    "VariantAvro",
    "VariantClassification",
    "VariantFileHeader",
    "VariantFileHeaderComplexLine",
    "VariantFileHeaderSimpleLine",
    "VariantFileMetadata",
    "VariantFunctionalEffect",
    "VariantGlobalStats",
    "VariantHardyWeinbergStats",
    "VariantMetadata",
    "VariantsByFrequency",
    "VariantSetStats",
    "VariantSource",
    "VariantStats",
    "VariantStudyMetadata",
    "VariantStudyStats",
    "VariantTraitAssociation",
    "VariantType",
    "VcfHeader",
    "Xref",
]
