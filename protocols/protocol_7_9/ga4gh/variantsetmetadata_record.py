"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class VariantSetMetadata(ProtocolElement):
    """
    This metadata represents VCF header information.
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "description",
        "id",
        "key",
        "number",
        "type",
        "value",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_9 import ga4gh as ga4gh_3_1_0

        return {
            "org.ga4gh.models.VariantSetMetadata": ga4gh_3_1_0.VariantSetMetadata,
        }

    __slots__ = [
        "description",
        "id",
        "key",
        "number",
        "type",
        "value",
        "info",
    ]

    def __init__(
        self,
        description,
        id,
        key,
        number,
        type,
        value,
        info=dict,
        validate=None,
        **kwargs
    ):
        """Initialise the ga4gh_3_1_0.VariantSetMetadata model.

        :param description:
            A textual description of this metadata.
        :type description: str
        :param id:
            User-provided ID field, not enforced by this API.   Two or more pieces
            of structured metadata with identical   id and key fields
            are considered equivalent.
        :type id: str
        :param key:
            The top-level key.
        :type key: str
        :param number:
            The number of values that can be included in a field described by this
            metadata.
        :type number: str
        :param type:
            The type of data.
        :type type: str
        :param value:
            The value field for simple metadata.
        :type value: str
        :param info:
            Remaining structured metadata key-value pairs.
        :type info: dict[str, list[str]]
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "3.1.0"
