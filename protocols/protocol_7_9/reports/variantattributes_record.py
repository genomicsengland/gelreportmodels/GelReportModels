"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class VariantAttributes(ProtocolElement):
    """
    Some additional variant attributes
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {}

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_9 import reports as reports_6_3_0

        return {
            "alleleFrequencies": reports_6_3_0.AlleleFrequency,
            "variantIdentifiers": reports_6_3_0.VariantIdentifiers,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_9 import reports as reports_6_3_0

        return {
            "alleleOrigins": reports_6_3_0.AlleleOrigin,
        }

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_7_9 import reports as reports_6_3_0

        return {
            "org.gel.models.report.avro.AlleleFrequency": reports_6_3_0.AlleleFrequency,
            "org.gel.models.report.avro.AlleleOrigin": reports_6_3_0.AlleleOrigin,
            "org.gel.models.report.avro.VariantIdentifiers": reports_6_3_0.VariantIdentifiers,
            "org.gel.models.report.avro.VariantAttributes": reports_6_3_0.VariantAttributes,
        }

    __slots__ = [
        "additionalNumericVariantAnnotations",
        "additionalTextualVariantAnnotations",
        "alleleFrequencies",
        "alleleOrigins",
        "cdnaChanges",
        "comments",
        "fdp50",
        "genomicChanges",
        "ihp",
        "others",
        "proteinChanges",
        "recurrentlyReported",
        "references",
        "variantIdentifiers",
    ]

    def __init__(
        self,
        additionalNumericVariantAnnotations=None,
        additionalTextualVariantAnnotations=None,
        alleleFrequencies=None,
        alleleOrigins=None,
        cdnaChanges=None,
        comments=None,
        fdp50=None,
        genomicChanges=None,
        ihp=None,
        others=None,
        proteinChanges=None,
        recurrentlyReported=None,
        references=None,
        variantIdentifiers=None,
        validate=None,
        **kwargs
    ):
        """Initialise the reports_6_3_0.VariantAttributes model.

        :param additionalNumericVariantAnnotations:
            Additional numeric variant annotations for this variant. For Example
            (Allele Frequency, sift, polyphen,         mutationTaster,
            CADD. ..)
        :type additionalNumericVariantAnnotations: None | dict[str, float]
        :param additionalTextualVariantAnnotations:
            Any additional information in a free text field. For example a quote
            from a paper
        :type additionalTextualVariantAnnotations: None | dict[str, str]
        :param alleleFrequencies:
            A list of population allele frequencies
        :type alleleFrequencies: None | list[AlleleFrequency]
        :param alleleOrigins:
            List of allele origins for this variant in this report
        :type alleleOrigins: None | list[AlleleOrigin]
        :param cdnaChanges:
            cDNA change, HGVS nomenclature (e.g.: c.76A>T)
        :type cdnaChanges: None | list[str]
        :param comments:
            Comments
        :type comments: None | list[str]
        :param fdp50:
            Average tier1 number of basecalls filtered from original read depth
            within 50 bases
        :type fdp50: None | float
        :param genomicChanges:
            gDNA change, HGVS nomenclature (e.g.: g.476A>T)
        :type genomicChanges: None | list[str]
        :param ihp:
            Largest reference interrupted homopolymer length intersecting with the
            indel
        :type ihp: None | int
        :param others:
            Map of other attributes where keys are the attribute names and values
            are the attributes
        :type others: None | dict[str, str]
        :param proteinChanges:
            Protein change, HGVS nomenclature (e.g.: p.Lys76Asn)
        :type proteinChanges: None | list[str]
        :param recurrentlyReported:
            Flag indicating if the variant is recurrently reported
        :type recurrentlyReported: None | boolean
        :param references:
            Additional references for ths variant. For example HGMD ID or Pubmed
            Id
        :type references: None | dict[str, str]
        :param variantIdentifiers:
        :type variantIdentifiers: None | VariantIdentifiers
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "6.3.0"
