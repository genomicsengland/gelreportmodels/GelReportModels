"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class OrganisationNgis(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "organisationCode",
        "organisationId",
        "organisationName",
        "organisationNationalGroupingId",
        "organisationNationalGroupingName",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_0 import participant as participant_1_5_0

        return {
            "org.gel.models.participant.avro.OrganisationNgis": participant_1_5_0.OrganisationNgis,
        }

    __slots__ = [
        "organisationCode",
        "organisationId",
        "organisationName",
        "organisationNationalGroupingId",
        "organisationNationalGroupingName",
    ]

    def __init__(
        self,
        organisationCode,
        organisationId,
        organisationName,
        organisationNationalGroupingId,
        organisationNationalGroupingName,
        validate=None,
        **kwargs
    ):
        """Initialise the participant_1_5_0.OrganisationNgis model.

        :param organisationCode:
            Ods code
        :type organisationCode: str
        :param organisationId:
            Organisation Id
        :type organisationId: str
        :param organisationName:
            Organisation Name
        :type organisationName: str
        :param organisationNationalGroupingId:
            National Grouping (GLH) Id
        :type organisationNationalGroupingId: str
        :param organisationNationalGroupingName:
            National Grouping (GLH) Name
        :type organisationNationalGroupingName: str
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "1.5.0"
