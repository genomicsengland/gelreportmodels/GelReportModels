from protocols.protocol_8_1.cva.authors_enum import (
    Authors,
)
from protocols.protocol_8_1.cva.cancergermlinevariantlevelquestionnaire_record import (
    CancerGermlineVariantLevelQuestionnaire,
)
from protocols.protocol_8_1.cva.cancerparticipantinject_record import (
    CancerParticipantInject,
)
from protocols.protocol_8_1.cva.cancersomaticvariantlevelquestionnaire_record import (
    CancerSomaticVariantLevelQuestionnaire,
)
from protocols.protocol_8_1.cva.category_enum import (
    Category,
)
from protocols.protocol_8_1.cva.clinicalreportinject_record import (
    ClinicalReportInject,
)
from protocols.protocol_8_1.cva.comment_record import (
    Comment,
)
from protocols.protocol_8_1.cva.curation_record import (
    Curation,
)
from protocols.protocol_8_1.cva.curationandvariants_record import (
    CurationAndVariants,
)
from protocols.protocol_8_1.cva.curationentry_record import (
    CurationEntry,
)
from protocols.protocol_8_1.cva.curationhistoryentry_record import (
    CurationHistoryEntry,
)
from protocols.protocol_8_1.cva.evidenceentryandvariants_record import (
    EvidenceEntryAndVariants,
)
from protocols.protocol_8_1.cva.exitquestionnaireinjectcancer_record import (
    ExitQuestionnaireInjectCancer,
)
from protocols.protocol_8_1.cva.exitquestionnaireinjectrd_record import (
    ExitQuestionnaireInjectRD,
)
from protocols.protocol_8_1.cva.exitquestionnairerd_record import (
    ExitQuestionnaireRD,
)
from protocols.protocol_8_1.cva.injectionmetadata_record import (
    InjectionMetadata,
)
from protocols.protocol_8_1.cva.interpretedgenomeinject_record import (
    InterpretedGenomeInject,
)
from protocols.protocol_8_1.cva.observedvariant_record import (
    ObservedVariant,
)
from protocols.protocol_8_1.cva.organisation_record import (
    Organisation,
)
from protocols.protocol_8_1.cva.pedigreeinjectrd_record import (
    PedigreeInjectRD,
)
from protocols.protocol_8_1.cva.reportedvariantquestionnairerd_record import (
    ReportedVariantQuestionnaireRD,
)
from protocols.protocol_8_1.cva.reportevententry_record import (
    ReportEventEntry,
)
from protocols.protocol_8_1.cva.reporteventquestionnairecancer_record import (
    ReportEventQuestionnaireCancer,
)
from protocols.protocol_8_1.cva.reporteventquestionnairerd_record import (
    ReportEventQuestionnaireRD,
)
from protocols.protocol_8_1.cva.reporteventtype_enum import (
    ReportEventType,
)
from protocols.protocol_8_1.cva.requestdetails_record import (
    RequestDetails,
)
from protocols.protocol_8_1.cva.transaction_record import (
    Transaction,
)
from protocols.protocol_8_1.cva.transactiondetails_record import (
    TransactionDetails,
)
from protocols.protocol_8_1.cva.transactionstatus_enum import (
    TransactionStatus,
)
from protocols.protocol_8_1.cva.transactionstatuschange_record import (
    TransactionStatusChange,
)
from protocols.protocol_8_1.cva.variant_record import (
    Variant,
)
from protocols.protocol_8_1.cva.variantinterpretationloginject_record import (
    VariantInterpretationLogInject,
)
from protocols.protocol_8_1.cva.variantrepresentation_record import (
    VariantRepresentation,
)
from protocols.protocol_8_1.cva.variantscoordinates_record import (
    VariantsCoordinates,
)

__all__ = [
    "Authors",
    "CancerGermlineVariantLevelQuestionnaire",
    "CancerParticipantInject",
    "CancerSomaticVariantLevelQuestionnaire",
    "Category",
    "ClinicalReportInject",
    "Comment",
    "Curation",
    "CurationAndVariants",
    "CurationEntry",
    "CurationHistoryEntry",
    "EvidenceEntryAndVariants",
    "ExitQuestionnaireInjectCancer",
    "ExitQuestionnaireInjectRD",
    "ExitQuestionnaireRD",
    "InjectionMetadata",
    "InterpretedGenomeInject",
    "ObservedVariant",
    "Organisation",
    "PedigreeInjectRD",
    "ReportedVariantQuestionnaireRD",
    "ReportEventEntry",
    "ReportEventQuestionnaireCancer",
    "ReportEventQuestionnaireRD",
    "ReportEventType",
    "RequestDetails",
    "Transaction",
    "TransactionDetails",
    "TransactionStatus",
    "TransactionStatusChange",
    "Variant",
    "VariantInterpretationLogInject",
    "VariantRepresentation",
    "VariantsCoordinates",
]
