"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class CancerInterpretationRequest(ProtocolElement):
    """
    This record represents basic information for this report
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "genomeAssembly",
        "internalStudyId",
        "interpretationRequestId",
        "interpretationRequestVersion",
        "versionControl",
        "workspace",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_2 import participant as participant_1_5_0
        from protocols.protocol_8_2 import reports as reports_6_7_1

        return {
            "annotationFile": reports_6_7_1.File,
            "bams": reports_6_7_1.File,
            "bigWigs": reports_6_7_1.File,
            "cancerParticipant": participant_1_5_0.CancerParticipant,
            "interpretationFlags": reports_6_7_1.InterpretationFlag,
            "otherFamilyHistory": reports_6_7_1.OtherFamilyHistory,
            "otherFiles": reports_6_7_1.File,
            "vcfs": reports_6_7_1.File,
            "versionControl": reports_6_7_1.ReportVersionControl,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_2 import reports as reports_6_7_1

        return {
            "genomeAssembly": reports_6_7_1.Assembly,
        }

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_2 import participant as participant_1_5_0
        from protocols.protocol_8_2 import reports as reports_6_7_1

        return {
            "org.gel.models.report.avro.File": reports_6_7_1.File,
            "org.gel.models.report.avro.File": reports_6_7_1.File,
            "org.gel.models.report.avro.File": reports_6_7_1.File,
            "org.gel.models.participant.avro.CancerParticipant": participant_1_5_0.CancerParticipant,
            "org.gel.models.report.avro.Assembly": reports_6_7_1.Assembly,
            "org.gel.models.report.avro.InterpretationFlag": reports_6_7_1.InterpretationFlag,
            "org.gel.models.report.avro.OtherFamilyHistory": reports_6_7_1.OtherFamilyHistory,
            "org.gel.models.report.avro.File": reports_6_7_1.File,
            "org.gel.models.report.avro.File": reports_6_7_1.File,
            "org.gel.models.report.avro.ReportVersionControl": reports_6_7_1.ReportVersionControl,
            "org.gel.models.report.avro.CancerInterpretationRequest": reports_6_7_1.CancerInterpretationRequest,
        }

    __slots__ = [
        "genomeAssembly",
        "internalStudyId",
        "interpretationRequestId",
        "interpretationRequestVersion",
        "versionControl",
        "workspace",
        "additionalInfo",
        "annotationFile",
        "bams",
        "bigWigs",
        "cancerParticipant",
        "genePanelsCoverage",
        "interpretationFlags",
        "otherFamilyHistory",
        "otherFiles",
        "participantInternalId",
        "vcfs",
    ]

    def __init__(
        self,
        genomeAssembly,
        internalStudyId,
        interpretationRequestId,
        interpretationRequestVersion,
        versionControl,
        workspace,
        additionalInfo=None,
        annotationFile=None,
        bams=None,
        bigWigs=None,
        cancerParticipant=None,
        genePanelsCoverage=None,
        interpretationFlags=None,
        otherFamilyHistory=None,
        otherFiles=None,
        participantInternalId=None,
        vcfs=None,
        validate=None,
        **kwargs
    ):
        """Initialise the reports_6_7_1.CancerInterpretationRequest model.

        :param genomeAssembly:
            This is the version of the assembly used to align the reads
        :type genomeAssembly: Assembly
        :param internalStudyId:
            Internal study identifier
        :type internalStudyId: str
        :param interpretationRequestId:
            Identifier for this interpretation request
        :type interpretationRequestId: str
        :param interpretationRequestVersion:
            Version for this interpretation request
        :type interpretationRequestVersion: int
        :param versionControl:
            Model version number
        :type versionControl: ReportVersionControl
        :param workspace:
            The genome shall be assigned to the workspaces(projects or domains
            with a predefined set of users) to control user access
        :type workspace: list[str]
        :param additionalInfo:
            Additional information
        :type additionalInfo: None | dict[str, str]
        :param annotationFile:
            Variant Annotation File
        :type annotationFile: None | File
        :param bams:
            BAMs Files
        :type bams: None | list[File]
        :param bigWigs:
            BigWig Files
        :type bigWigs: None | list[File]
        :param cancerParticipant:
            Cancer Particiapnt Data.
        :type cancerParticipant: None | CancerParticipant
        :param genePanelsCoverage:
            This map of key: panel_name, value: (map of key: gene, value: (map of
            metrics of key: metric name, value: float)) That is: a map
            of tables of genes and metrics
        :type genePanelsCoverage: None | dict[str, dict[str, dict[str, float]]]
        :param interpretationFlags:
            Flags about this case relevant for interpretation
        :type interpretationFlags: None | list[InterpretationFlag]
        :param otherFamilyHistory:
            It is paternal or maternal with reference to the participant.
        :type otherFamilyHistory: None | OtherFamilyHistory
        :param otherFiles:
            Other files that may be vendor specific map of key: type of file,
            value: record of type File
        :type otherFiles: None | dict[str, File]
        :param participantInternalId:
            Participant internal identifier
        :type participantInternalId: None | str
        :param vcfs:
            VCFs Files where SVs and CNVs are represented
        :type vcfs: None | list[File]
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "6.7.1"
