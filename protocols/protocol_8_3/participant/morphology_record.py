"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class Morphology(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {}

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_3 import participant as participant_1_5_0

        return {
            "org.gel.models.participant.avro.Morphology": participant_1_5_0.Morphology,
        }

    __slots__ = [
        "id",
        "name",
        "value",
        "version",
    ]

    def __init__(
        self,
        id=None,
        name=None,
        value=None,
        version=None,
        validate=None,
        **kwargs
    ):
        """Initialise the participant_1_5_0.Morphology model.

        :param id:
            The ontology term id or accession in OBO format
            ${ONTOLOGY_ID}:${TERM_ID}
            (http://www.obofoundry.org/id-policy.html)
        :type id: None | str
        :param name:
            The ontology term name
        :type name: None | str
        :param value:
            Optional value for the ontology term, the type of the value is not
            checked         (i.e.: we could set the pvalue term to
            "significant" or to "0.0001")
        :type value: None | str
        :param version:
            Ontology version
        :type version: None | str
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "1.5.0"
