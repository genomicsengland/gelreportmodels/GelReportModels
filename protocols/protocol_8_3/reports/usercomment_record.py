"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class UserComment(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "comment",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_3 import reports as reports_6_7_2

        return {
            "user": reports_6_7_2.User,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_3 import reports as reports_6_7_2

        return {
            "org.gel.models.report.avro.User": reports_6_7_2.User,
            "org.gel.models.report.avro.UserComment": reports_6_7_2.UserComment,
        }

    __slots__ = [
        "comment",
        "timestamp",
        "user",
    ]

    def __init__(
        self,
        comment,
        timestamp=None,
        user=None,
        validate=None,
        **kwargs
    ):
        """Initialise the reports_6_7_2.UserComment model.

        :param comment:
            Comment text
        :type comment: str
        :param timestamp:
            Date and time comment was created (ISO 8601 datetime with seconds and
            timezone e.g. 2020-11-23T15:52:36+00:00)
        :type timestamp: None | str
        :param user:
            User who created comment
        :type user: None | User
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "6.7.2"
