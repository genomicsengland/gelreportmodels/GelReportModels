"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class Chromosome(ProtocolElement):
    """
    All coverage information about a given chromosome
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "avg",
        "bases",
        "chr",
        "gte15x",
        "gte30x",
        "gte50x",
        "lt15x",
        "med",
        "pct25",
        "pct75",
        "sd",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_4 import coverage as coverage_0_1_0

        return {
            "org.gel.models.coverage.avro.Chromosome": coverage_0_1_0.Chromosome,
        }

    __slots__ = [
        "avg",
        "bases",
        "chr",
        "gte15x",
        "gte30x",
        "gte50x",
        "lt15x",
        "med",
        "pct25",
        "pct75",
        "sd",
        "gc",
        "rmsd",
    ]

    def __init__(
        self,
        avg,
        bases,
        chr,
        gte15x,
        gte30x,
        gte50x,
        lt15x,
        med,
        pct25,
        pct75,
        sd,
        gc=None,
        rmsd=None,
        validate=None,
        **kwargs
    ):
        """Initialise the coverage_0_1_0.Chromosome model.

        :param avg:
            The average depth of coverage
        :type avg: float
        :param bases:
            The number of bases in the region
        :type bases: int
        :param chr:
            The chromosome identifier (may have "chr" prefix or not depending on
            input data)
        :type chr: str
        :param gte15x:
            The number of positions with a depth of coverage greater than or equal
            than 15x
        :type gte15x: float
        :param gte30x:
            The number of positions with a depth of coverage greater than or equal
            than 30x
        :type gte30x: float
        :param gte50x:
            The number of positions with a depth of coverage greater than or equal
            than 50x
        :type gte50x: float
        :param lt15x:
            The number of positions with a depth of coverage less than 15x
        :type lt15x: float
        :param med:
            The median depth of coverage
        :type med: float
        :param pct25:
            The 25th percentile of coverage distribution
        :type pct25: float
        :param pct75:
            The 75th percentile of coverage distribution
        :type pct75: float
        :param sd:
            The depth of coverage standard deviation
        :type sd: float
        :param gc:
            The GC content
        :type gc: None | float
        :param rmsd:
            The squared root sum of squares of the deviation from the mean
        :type rmsd: None | float
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "0.1.0"
