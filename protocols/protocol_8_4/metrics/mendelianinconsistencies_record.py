"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class MendelianInconsistencies(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {}

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_4 import metrics as metrics_1_2_3

        return {
            "individualMendelErrors": metrics_1_2_3.IndividualMendelErrors,
            "locusMendelSummary": metrics_1_2_3.LocusMendelSummary,
            "perFamilyMendelErrors": metrics_1_2_3.PerFamilyMendelErrors,
            "totalNumberOfMendelErrors": metrics_1_2_3.TotalNumberOfMendelErrors,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_4 import metrics as metrics_1_2_3

        return {
            "org.gel.models.metrics.avro.IndividualMendelErrors": metrics_1_2_3.IndividualMendelErrors,
            "org.gel.models.metrics.avro.LocusMendelSummary": metrics_1_2_3.LocusMendelSummary,
            "org.gel.models.metrics.avro.PerFamilyMendelErrors": metrics_1_2_3.PerFamilyMendelErrors,
            "org.gel.models.metrics.avro.TotalNumberOfMendelErrors": metrics_1_2_3.TotalNumberOfMendelErrors,
            "org.gel.models.metrics.avro.MendelianInconsistencies": metrics_1_2_3.MendelianInconsistencies,
        }

    __slots__ = [
        "individualMendelErrors",
        "locusMendelSummary",
        "perFamilyMendelErrors",
        "totalNumberOfMendelErrors",
    ]

    def __init__(
        self,
        individualMendelErrors=None,
        locusMendelSummary=None,
        perFamilyMendelErrors=None,
        totalNumberOfMendelErrors=None,
        validate=None,
        **kwargs
    ):
        """Initialise the metrics_1_2_3.MendelianInconsistencies model.

        :param individualMendelErrors:
            Number of mendelian inconsitencies per sample and nuclear family. One
            entry per sample and nuclear family
        :type individualMendelErrors: None | list[IndividualMendelErrors]
        :param locusMendelSummary:
            Summary of the type of mendelian inconstencies happening in the
            family. One entry per sample, chromosome and code
        :type locusMendelSummary: None | list[LocusMendelSummary]
        :param perFamilyMendelErrors:
            Number of mendelian inconsitencies per nuclear family. One entry per
            nuclear family
        :type perFamilyMendelErrors: None | list[PerFamilyMendelErrors]
        :param totalNumberOfMendelErrors:
            Aggregated number of mendelian inconsitencies per sample and family
        :type totalNumberOfMendelErrors: None | TotalNumberOfMendelErrors
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "1.2.3"
