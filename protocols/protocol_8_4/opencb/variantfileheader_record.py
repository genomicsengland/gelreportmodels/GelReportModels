"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class VariantFileHeader(ProtocolElement):
    """
    Variant File Header. Contains simple and complex metadata lines
    describing the content of the file.     This header matches with
    the VCF header.     A header may have multiple Simple or Complex
    lines with the same key
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "version",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_4 import opencb as opencb_2_9_3

        return {
            "complexLines": opencb_2_9_3.VariantFileHeaderComplexLine,
            "simpleLines": opencb_2_9_3.VariantFileHeaderSimpleLine,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_4 import opencb as opencb_2_9_3

        return {
            "org.opencb.biodata.models.variant.metadata.VariantFileHeaderComplexLine": opencb_2_9_3.VariantFileHeaderComplexLine,
            "org.opencb.biodata.models.variant.metadata.VariantFileHeaderSimpleLine": opencb_2_9_3.VariantFileHeaderSimpleLine,
            "org.opencb.biodata.models.variant.metadata.VariantFileHeader": opencb_2_9_3.VariantFileHeader,
        }

    __slots__ = [
        "version",
        "complexLines",
        "simpleLines",
    ]

    def __init__(
        self,
        version,
        complexLines=list,
        simpleLines=list,
        validate=None,
        **kwargs
    ):
        """Initialise the opencb_2_9_3.VariantFileHeader model.

        :param version:
        :type version: str
        :param complexLines:
            complex lines, e.g.
            INFO=<ID=NS,Number=1,Type=Integer,Description="Number of
            samples with data">
        :type complexLines: list[VariantFileHeaderComplexLine]
        :param simpleLines:
            simple lines, e.g. fileDate=20090805
        :type simpleLines: list[VariantFileHeaderSimpleLine]
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "2.9.3-GEL"
