"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class VariantSource(ProtocolElement):
    """
    The source of the variant call
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {}

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_5 import reports as reports_6_8_0

        return {
            "org.gel.models.report.avro.VariantSource": reports_6_8_0.VariantSource,
        }

    __slots__ = [
        "date",
        "name",
        "version",
    ]

    def __init__(
        self,
        date=None,
        name=None,
        version=None,
        validate=None,
        **kwargs
    ):
        """Initialise the reports_6_8_0.VariantSource model.

        :param date:
            The source date.
        :type date: None | str
        :param name:
            Name of source
        :type name: None | str
        :param version:
            Version of source
        :type version: None | str
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "6.8.0"
