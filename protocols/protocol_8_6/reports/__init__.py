from protocols.protocol_8_6.reports.acmgclassification_enum import (
    ACMGClassification,
)
from protocols.protocol_8_6.reports.acmgevidence_record import (
    AcmgEvidence,
)
from protocols.protocol_8_6.reports.acmgevidencecategory_enum import (
    AcmgEvidenceCategory,
)
from protocols.protocol_8_6.reports.acmgevidencetype_enum import (
    AcmgEvidenceType,
)
from protocols.protocol_8_6.reports.acmgevidenceweight_enum import (
    AcmgEvidenceWeight,
)
from protocols.protocol_8_6.reports.acmgvariantclassification_record import (
    AcmgVariantClassification,
)
from protocols.protocol_8_6.reports.actionability_enum import (
    Actionability,
)
from protocols.protocol_8_6.reports.actions_record import (
    Actions,
)
from protocols.protocol_8_6.reports.activationstrength_enum import (
    ActivationStrength,
)
from protocols.protocol_8_6.reports.additionalanalysispanel_record import (
    AdditionalAnalysisPanel,
)
from protocols.protocol_8_6.reports.additionalfindingsclinicalreport_record import (
    AdditionalFindingsClinicalReport,
)
from protocols.protocol_8_6.reports.additionalfindingsexitquestionnaire_record import (
    AdditionalFindingsExitQuestionnaire,
)
from protocols.protocol_8_6.reports.additionalfindingsvariantgrouplevelquestions_record import (
    AdditionalFindingsVariantGroupLevelQuestions,
)
from protocols.protocol_8_6.reports.additionalvariantsquestions_record import (
    AdditionalVariantsQuestions,
)
from protocols.protocol_8_6.reports.agerange_record import (
    AgeRange,
)
from protocols.protocol_8_6.reports.algorithmbasedvariantclassification_record import (
    AlgorithmBasedVariantClassification,
)
from protocols.protocol_8_6.reports.allelefrequency_record import (
    AlleleFrequency,
)
from protocols.protocol_8_6.reports.alleleorigin_enum import (
    AlleleOrigin,
)
from protocols.protocol_8_6.reports.ampclincialorexperimentalevidence_record import (
    AmpClincialOrExperimentalEvidence,
)
from protocols.protocol_8_6.reports.ampclinicalorexperimentalevidencecategory_enum import (
    AmpClinicalOrExperimentalEvidenceCategory,
)
from protocols.protocol_8_6.reports.ampclinicalorexperimentalevidencelevel_enum import (
    AmpClinicalOrExperimentalEvidenceLevel,
)
from protocols.protocol_8_6.reports.ampevidence_record import (
    AmpEvidence,
)
from protocols.protocol_8_6.reports.ampevidencetype_enum import (
    AmpEvidenceType,
)
from protocols.protocol_8_6.reports.amptier_enum import (
    AmpTier,
)
from protocols.protocol_8_6.reports.ampvariantclassification_record import (
    AmpVariantClassification,
)
from protocols.protocol_8_6.reports.aneuploidy_record import (
    Aneuploidy,
)
from protocols.protocol_8_6.reports.assembly_enum import (
    Assembly,
)
from protocols.protocol_8_6.reports.breakpoint_record import (
    BreakPoint,
)
from protocols.protocol_8_6.reports.call_record import (
    Call,
)
from protocols.protocol_8_6.reports.callset_record import (
    CallSet,
)
from protocols.protocol_8_6.reports.canceractionability_enum import (
    CancerActionability,
)
from protocols.protocol_8_6.reports.canceractionabilitysomatic_enum import (
    CancerActionabilitySomatic,
)
from protocols.protocol_8_6.reports.canceractionablevariants_enum import (
    CancerActionableVariants,
)
from protocols.protocol_8_6.reports.cancercaselevelquestions_record import (
    CancerCaseLevelQuestions,
)
from protocols.protocol_8_6.reports.cancerexitquestionnaire_record import (
    CancerExitQuestionnaire,
)
from protocols.protocol_8_6.reports.cancergermlinevariantlevelquestions_record import (
    CancerGermlineVariantLevelQuestions,
)
from protocols.protocol_8_6.reports.cancerinterpretationrequest_record import (
    CancerInterpretationRequest,
)
from protocols.protocol_8_6.reports.cancerinterpretationrequestgms_record import (
    CancerInterpretationRequestGMS,
)
from protocols.protocol_8_6.reports.cancerreporteventflag_enum import (
    CancerReportEventFlag,
)
from protocols.protocol_8_6.reports.cancersomaticvariantlevelquestions_record import (
    CancerSomaticVariantLevelQuestions,
)
from protocols.protocol_8_6.reports.cancertested_enum import (
    CancerTested,
)
from protocols.protocol_8_6.reports.cancertestedadditional_enum import (
    CancerTestedAdditional,
)
from protocols.protocol_8_6.reports.cancerusabilitygermline_enum import (
    CancerUsabilityGermline,
)
from protocols.protocol_8_6.reports.cancerusabilitysomatic_enum import (
    CancerUsabilitySomatic,
)
from protocols.protocol_8_6.reports.casesolvedfamily_enum import (
    CaseSolvedFamily,
)
from protocols.protocol_8_6.reports.chromosomalrearrangement_record import (
    ChromosomalRearrangement,
)
from protocols.protocol_8_6.reports.chromosomalrearrangementinterpretationlog_record import (
    ChromosomalRearrangementInterpretationLog,
)
from protocols.protocol_8_6.reports.clinicalreport_record import (
    ClinicalReport,
)
from protocols.protocol_8_6.reports.clinicalsignificance_enum import (
    ClinicalSignificance,
)
from protocols.protocol_8_6.reports.clinicalutility_enum import (
    ClinicalUtility,
)
from protocols.protocol_8_6.reports.confidence_enum import (
    Confidence,
)
from protocols.protocol_8_6.reports.confidenceinterval_record import (
    ConfidenceInterval,
)
from protocols.protocol_8_6.reports.confirmationdecision_enum import (
    ConfirmationDecision,
)
from protocols.protocol_8_6.reports.confirmationoutcome_enum import (
    ConfirmationOutcome,
)
from protocols.protocol_8_6.reports.consequence_record import (
    Consequence,
)
from protocols.protocol_8_6.reports.consistencystatus_enum import (
    ConsistencyStatus,
)
from protocols.protocol_8_6.reports.coordinates_record import (
    Coordinates,
)
from protocols.protocol_8_6.reports.demographicelegibilitycriteria_record import (
    DemographicElegibilityCriteria,
)
from protocols.protocol_8_6.reports.diagnostic_record import (
    Diagnostic,
)
from protocols.protocol_8_6.reports.domain_enum import (
    Domain,
)
from protocols.protocol_8_6.reports.drugresponse_record import (
    DrugResponse,
)
from protocols.protocol_8_6.reports.drugresponseclassification_enum import (
    DrugResponseClassification,
)
from protocols.protocol_8_6.reports.evidenceentry_record import (
    EvidenceEntry,
)
from protocols.protocol_8_6.reports.evidenceimpact_enum import (
    EvidenceImpact,
)
from protocols.protocol_8_6.reports.evidencesource_record import (
    EvidenceSource,
)
from protocols.protocol_8_6.reports.evidencesubmission_record import (
    EvidenceSubmission,
)
from protocols.protocol_8_6.reports.exonoverlap_record import (
    ExonOverlap,
)
from protocols.protocol_8_6.reports.familyhistorycondition_enum import (
    FamilyHistoryCondition,
)
from protocols.protocol_8_6.reports.familyhistoryfamily_enum import (
    FamilyHistoryFamily,
)
from protocols.protocol_8_6.reports.familyhistorypatient_enum import (
    FamilyHistoryPatient,
)
from protocols.protocol_8_6.reports.familylevelquestions_record import (
    FamilyLevelQuestions,
)
from protocols.protocol_8_6.reports.featuretypes_enum import (
    FeatureTypes,
)
from protocols.protocol_8_6.reports.file_record import (
    File,
)
from protocols.protocol_8_6.reports.filetype_enum import (
    FileType,
)
from protocols.protocol_8_6.reports.genepanel_record import (
    GenePanel,
)
from protocols.protocol_8_6.reports.genomicentity_record import (
    GenomicEntity,
)
from protocols.protocol_8_6.reports.genomicentitytype_enum import (
    GenomicEntityType,
)
from protocols.protocol_8_6.reports.genomicfeature_record import (
    GenomicFeature,
)
from protocols.protocol_8_6.reports.guidelinebasedvariantclassification_record import (
    GuidelineBasedVariantClassification,
)
from protocols.protocol_8_6.reports.heritabletrait_record import (
    HeritableTrait,
)
from protocols.protocol_8_6.reports.identifier_record import (
    Identifier,
)
from protocols.protocol_8_6.reports.identitybydescent_record import (
    IdentityByDescent,
)
from protocols.protocol_8_6.reports.indel_enum import (
    Indel,
)
from protocols.protocol_8_6.reports.interpretationdatacancer_record import (
    InterpretationDataCancer,
)
from protocols.protocol_8_6.reports.interpretationdatard_record import (
    InterpretationDataRd,
)
from protocols.protocol_8_6.reports.interpretationflag_record import (
    InterpretationFlag,
)
from protocols.protocol_8_6.reports.interpretationflags_enum import (
    InterpretationFlags,
)
from protocols.protocol_8_6.reports.interpretationrequestrd_record import (
    InterpretationRequestRD,
)
from protocols.protocol_8_6.reports.interpretedgenome_record import (
    InterpretedGenome,
)
from protocols.protocol_8_6.reports.intervention_record import (
    Intervention,
)
from protocols.protocol_8_6.reports.interventiontype_enum import (
    InterventionType,
)
from protocols.protocol_8_6.reports.karyotype_record import (
    Karyotype,
)
from protocols.protocol_8_6.reports.location_record import (
    Location,
)
from protocols.protocol_8_6.reports.modeofinheritance_enum import (
    ModeOfInheritance,
)
from protocols.protocol_8_6.reports.numberofcopies_record import (
    NumberOfCopies,
)
from protocols.protocol_8_6.reports.ontology_record import (
    Ontology,
)
from protocols.protocol_8_6.reports.orientation_enum import (
    Orientation,
)
from protocols.protocol_8_6.reports.otheraction_record import (
    OtherAction,
)
from protocols.protocol_8_6.reports.otherfamilyhistory_record import (
    OtherFamilyHistory,
)
from protocols.protocol_8_6.reports.participantinterpretationflags_record import (
    ParticipantInterpretationFlags,
)
from protocols.protocol_8_6.reports.phasegenotype_record import (
    PhaseGenotype,
)
from protocols.protocol_8_6.reports.phenotypes_record import (
    Phenotypes,
)
from protocols.protocol_8_6.reports.phenotypessolved_enum import (
    PhenotypesSolved,
)
from protocols.protocol_8_6.reports.primarypurpose_enum import (
    PrimaryPurpose,
)
from protocols.protocol_8_6.reports.prognosis_record import (
    Prognosis,
)
from protocols.protocol_8_6.reports.prognosisclassification_enum import (
    PrognosisClassification,
)
from protocols.protocol_8_6.reports.program_enum import (
    Program,
)
from protocols.protocol_8_6.reports.property_record import (
    Property,
)
from protocols.protocol_8_6.reports.publication_record import (
    Publication,
)
from protocols.protocol_8_6.reports.rarediseaseexitquestionnaire_record import (
    RareDiseaseExitQuestionnaire,
)
from protocols.protocol_8_6.reports.rearrangement_record import (
    Rearrangement,
)
from protocols.protocol_8_6.reports.reportcategory_enum import (
    ReportCategory,
)
from protocols.protocol_8_6.reports.reportevent_record import (
    ReportEvent,
)
from protocols.protocol_8_6.reports.reportingquestion_enum import (
    ReportingQuestion,
)
from protocols.protocol_8_6.reports.reportversioncontrol_record import (
    ReportVersionControl,
)
from protocols.protocol_8_6.reports.reviewedparts_enum import (
    ReviewedParts,
)
from protocols.protocol_8_6.reports.roleincancer_enum import (
    RoleInCancer,
)
from protocols.protocol_8_6.reports.segregationpattern_enum import (
    SegregationPattern,
)
from protocols.protocol_8_6.reports.segregationquestion_enum import (
    SegregationQuestion,
)
from protocols.protocol_8_6.reports.selectedreportevent_record import (
    SelectedReportEvent,
)
from protocols.protocol_8_6.reports.sequenceontologyterm_record import (
    SequenceOntologyTerm,
)
from protocols.protocol_8_6.reports.shorttandemrepeat_record import (
    ShortTandemRepeat,
)
from protocols.protocol_8_6.reports.shorttandemrepeatdetails_record import (
    ShortTandemRepeatDetails,
)
from protocols.protocol_8_6.reports.shorttandemrepeatinterpretationlog_record import (
    ShortTandemRepeatInterpretationLog,
)
from protocols.protocol_8_6.reports.shorttandemrepeatlevelquestions_record import (
    ShortTandemRepeatLevelQuestions,
)
from protocols.protocol_8_6.reports.shorttandemrepeatreferencedata_record import (
    ShortTandemRepeatReferenceData,
)
from protocols.protocol_8_6.reports.smallvariant_record import (
    SmallVariant,
)
from protocols.protocol_8_6.reports.somaticinformation_record import (
    SomaticInformation,
)
from protocols.protocol_8_6.reports.splicescores_record import (
    SpliceScores,
)
from protocols.protocol_8_6.reports.standardphenotype_record import (
    StandardPhenotype,
)
from protocols.protocol_8_6.reports.structuralvariant_record import (
    StructuralVariant,
)
from protocols.protocol_8_6.reports.structuralvariantdetails_record import (
    StructuralVariantDetails,
)
from protocols.protocol_8_6.reports.structuralvariantinterpretationlog_record import (
    StructuralVariantInterpretationLog,
)
from protocols.protocol_8_6.reports.structuralvariantlevelquestions_record import (
    StructuralVariantLevelQuestions,
)
from protocols.protocol_8_6.reports.structuralvarianttype_enum import (
    StructuralVariantType,
)
from protocols.protocol_8_6.reports.studyphase_enum import (
    StudyPhase,
)
from protocols.protocol_8_6.reports.studytype_enum import (
    StudyType,
)
from protocols.protocol_8_6.reports.supportingreadtype_enum import (
    SupportingReadType,
)
from protocols.protocol_8_6.reports.therapy_record import (
    Therapy,
)
from protocols.protocol_8_6.reports.tier_enum import (
    Tier,
)
from protocols.protocol_8_6.reports.timeunit_enum import (
    TimeUnit,
)
from protocols.protocol_8_6.reports.traitassociation_enum import (
    TraitAssociation,
)
from protocols.protocol_8_6.reports.trial_record import (
    Trial,
)
from protocols.protocol_8_6.reports.triallocation_record import (
    TrialLocation,
)
from protocols.protocol_8_6.reports.tumorigenesisclassification_enum import (
    TumorigenesisClassification,
)
from protocols.protocol_8_6.reports.uniparentaldisomy_record import (
    UniparentalDisomy,
)
from protocols.protocol_8_6.reports.uniparentaldisomydetails_record import (
    UniparentalDisomyDetails,
)
from protocols.protocol_8_6.reports.uniparentaldisomyevidences_record import (
    UniparentalDisomyEvidences,
)
from protocols.protocol_8_6.reports.uniparentaldisomyfragment_record import (
    UniparentalDisomyFragment,
)
from protocols.protocol_8_6.reports.uniparentaldisomyinterpretationlog_record import (
    UniparentalDisomyInterpretationLog,
)
from protocols.protocol_8_6.reports.uniparentaldisomyorigin_enum import (
    UniparentalDisomyOrigin,
)
from protocols.protocol_8_6.reports.uniparentaldisomytype_enum import (
    UniparentalDisomyType,
)
from protocols.protocol_8_6.reports.user_record import (
    User,
)
from protocols.protocol_8_6.reports.usercomment_record import (
    UserComment,
)
from protocols.protocol_8_6.reports.validationresult_enum import (
    ValidationResult,
)
from protocols.protocol_8_6.reports.variant_record import (
    Variant,
)
from protocols.protocol_8_6.reports.variantattributes_record import (
    VariantAttributes,
)
from protocols.protocol_8_6.reports.variantcall_record import (
    VariantCall,
)
from protocols.protocol_8_6.reports.variantclassification_record import (
    VariantClassification,
)
from protocols.protocol_8_6.reports.variantconsequence_record import (
    VariantConsequence,
)
from protocols.protocol_8_6.reports.variantcoordinates_record import (
    VariantCoordinates,
)
from protocols.protocol_8_6.reports.variantfunctionaleffect_enum import (
    VariantFunctionalEffect,
)
from protocols.protocol_8_6.reports.variantgrouplevelquestions_record import (
    VariantGroupLevelQuestions,
)
from protocols.protocol_8_6.reports.variantidentifiers_record import (
    VariantIdentifiers,
)
from protocols.protocol_8_6.reports.variantinterpretationlog_record import (
    VariantInterpretationLog,
)
from protocols.protocol_8_6.reports.variantlevelquestions_record import (
    VariantLevelQuestions,
)
from protocols.protocol_8_6.reports.variantset_record import (
    VariantSet,
)
from protocols.protocol_8_6.reports.variantsetmetadata_record import (
    VariantSetMetadata,
)
from protocols.protocol_8_6.reports.variantsource_record import (
    VariantSource,
)
from protocols.protocol_8_6.reports.variantvalidation_record import (
    VariantValidation,
)
from protocols.protocol_8_6.reports.zygosity_enum import (
    Zygosity,
)

__all__ = [
    "ACMGClassification",
    "AcmgEvidence",
    "AcmgEvidenceCategory",
    "AcmgEvidenceType",
    "AcmgEvidenceWeight",
    "AcmgVariantClassification",
    "Actionability",
    "Actions",
    "ActivationStrength",
    "AdditionalAnalysisPanel",
    "AdditionalFindingsClinicalReport",
    "AdditionalFindingsExitQuestionnaire",
    "AdditionalFindingsVariantGroupLevelQuestions",
    "AdditionalVariantsQuestions",
    "AgeRange",
    "AlgorithmBasedVariantClassification",
    "AlleleFrequency",
    "AlleleOrigin",
    "AmpClincialOrExperimentalEvidence",
    "AmpClinicalOrExperimentalEvidenceCategory",
    "AmpClinicalOrExperimentalEvidenceLevel",
    "AmpEvidence",
    "AmpEvidenceType",
    "AmpTier",
    "AmpVariantClassification",
    "Aneuploidy",
    "Assembly",
    "BreakPoint",
    "Call",
    "CallSet",
    "CancerActionability",
    "CancerActionabilitySomatic",
    "CancerActionableVariants",
    "CancerCaseLevelQuestions",
    "CancerExitQuestionnaire",
    "CancerGermlineVariantLevelQuestions",
    "CancerInterpretationRequest",
    "CancerInterpretationRequestGMS",
    "CancerReportEventFlag",
    "CancerSomaticVariantLevelQuestions",
    "CancerTested",
    "CancerTestedAdditional",
    "CancerUsabilityGermline",
    "CancerUsabilitySomatic",
    "CaseSolvedFamily",
    "ChromosomalRearrangement",
    "ChromosomalRearrangementInterpretationLog",
    "ClinicalReport",
    "ClinicalSignificance",
    "ClinicalUtility",
    "Confidence",
    "ConfidenceInterval",
    "ConfirmationDecision",
    "ConfirmationOutcome",
    "Consequence",
    "ConsistencyStatus",
    "Coordinates",
    "DemographicElegibilityCriteria",
    "Diagnostic",
    "Domain",
    "DrugResponse",
    "DrugResponseClassification",
    "EvidenceEntry",
    "EvidenceImpact",
    "EvidenceSource",
    "EvidenceSubmission",
    "ExonOverlap",
    "FamilyHistoryCondition",
    "FamilyHistoryFamily",
    "FamilyHistoryPatient",
    "FamilyLevelQuestions",
    "FeatureTypes",
    "File",
    "FileType",
    "GenePanel",
    "GenomicEntity",
    "GenomicEntityType",
    "GenomicFeature",
    "GuidelineBasedVariantClassification",
    "HeritableTrait",
    "Identifier",
    "IdentityByDescent",
    "Indel",
    "InterpretationDataCancer",
    "InterpretationDataRd",
    "InterpretationFlag",
    "InterpretationFlags",
    "InterpretationRequestRD",
    "InterpretedGenome",
    "Intervention",
    "InterventionType",
    "Karyotype",
    "Location",
    "ModeOfInheritance",
    "NumberOfCopies",
    "Ontology",
    "Orientation",
    "OtherAction",
    "OtherFamilyHistory",
    "ParticipantInterpretationFlags",
    "PhaseGenotype",
    "Phenotypes",
    "PhenotypesSolved",
    "PrimaryPurpose",
    "Prognosis",
    "PrognosisClassification",
    "Program",
    "Property",
    "Publication",
    "RareDiseaseExitQuestionnaire",
    "Rearrangement",
    "ReportCategory",
    "ReportEvent",
    "ReportingQuestion",
    "ReportVersionControl",
    "ReviewedParts",
    "RoleInCancer",
    "SegregationPattern",
    "SegregationQuestion",
    "SelectedReportEvent",
    "SequenceOntologyTerm",
    "ShortTandemRepeat",
    "ShortTandemRepeatDetails",
    "ShortTandemRepeatInterpretationLog",
    "ShortTandemRepeatLevelQuestions",
    "ShortTandemRepeatReferenceData",
    "SmallVariant",
    "SomaticInformation",
    "SpliceScores",
    "StandardPhenotype",
    "StructuralVariant",
    "StructuralVariantDetails",
    "StructuralVariantInterpretationLog",
    "StructuralVariantLevelQuestions",
    "StructuralVariantType",
    "StudyPhase",
    "StudyType",
    "SupportingReadType",
    "Therapy",
    "Tier",
    "TimeUnit",
    "TraitAssociation",
    "Trial",
    "TrialLocation",
    "TumorigenesisClassification",
    "UniparentalDisomy",
    "UniparentalDisomyDetails",
    "UniparentalDisomyEvidences",
    "UniparentalDisomyFragment",
    "UniparentalDisomyInterpretationLog",
    "UniparentalDisomyOrigin",
    "UniparentalDisomyType",
    "User",
    "UserComment",
    "ValidationResult",
    "Variant",
    "VariantAttributes",
    "VariantCall",
    "VariantClassification",
    "VariantConsequence",
    "VariantCoordinates",
    "VariantFunctionalEffect",
    "VariantGroupLevelQuestions",
    "VariantIdentifiers",
    "VariantInterpretationLog",
    "VariantLevelQuestions",
    "VariantSet",
    "VariantSetMetadata",
    "VariantSource",
    "VariantValidation",
    "Zygosity",
]
