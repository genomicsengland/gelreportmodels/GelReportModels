"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class MendelianInconsistenciesCheck(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {}

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_7 import metrics as metrics_1_2_5

        return {
            "mendelianInconsistenciesQuery": metrics_1_2_5.Query,
        }

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_7 import metrics as metrics_1_2_5

        return {
            "org.gel.models.metrics.avro.Query": metrics_1_2_5.Query,
            "org.gel.models.metrics.avro.MendelianInconsistenciesCheck": metrics_1_2_5.MendelianInconsistenciesCheck,
        }

    __slots__ = [
        "comments",
        "mendelianInconsistenciesQuery",
        "sampleId",
    ]

    def __init__(
        self,
        comments=None,
        mendelianInconsistenciesQuery=None,
        sampleId=None,
        validate=None,
        **kwargs
    ):
        """Initialise the metrics_1_2_5.MendelianInconsistenciesCheck model.

        :param comments:
            Mendelian inconsistencies cannot always be computed for all the
            samples in the family (depends on family structure).
            Specify here if this is the case or there was any other
            issues
        :type comments: None | str
        :param mendelianInconsistenciesQuery:
            Whether the sample is a Mendelian inconsistencies query (yes, no,
            unknown, notTested)
        :type mendelianInconsistenciesQuery: None | Query
        :param sampleId:
            Sample Id
        :type sampleId: None | str
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "1.2.5"
