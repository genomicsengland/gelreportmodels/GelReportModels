"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class SelectionTest(object):
    """
    The set of tests that are performed in Family Selection
    """

    ALIGNMENT_STATS = "ALIGNMENT_STATS"
    ANALYSIS_PANELS = "ANALYSIS_PANELS"
    BAM_STATS_CHECK = "BAM_STATS_CHECK"
    BLACKLIST = "BLACKLIST"
    CNV_QC_STATUS = "CNV_QC_STATUS"
    DELIVERY_CHECK = "DELIVERY_CHECK"
    DISEASE_PENETRANCE = "DISEASE_PENETRANCE"
    FAMILY_SELECTION = "FAMILY_SELECTION"
    GEL_STATUS = "GEL_STATUS"
    HPO_CHECK = "HPO_CHECK"
    PANEL_VERSION = "PANEL_VERSION"
    RVG_RESULTS = "RVG_RESULTS"
    SEX_CHECK = "SEX_CHECK"

    def __hash__(self):
        return str(self).__hash__()

    @classmethod
    def _namespace_version(cls):
        return "1.2.5"
