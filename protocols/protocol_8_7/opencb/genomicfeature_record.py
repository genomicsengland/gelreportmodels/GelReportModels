"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class GenomicFeature(ProtocolElement):
    """
    The genomic feature
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {}

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_7 import opencb as opencb_2_9_3

        return {
            "featureType": opencb_2_9_3.FeatureTypes,
        }

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_7 import opencb as opencb_2_9_3

        return {
            "org.opencb.biodata.models.variant.avro.FeatureTypes": opencb_2_9_3.FeatureTypes,
            "org.opencb.biodata.models.variant.avro.GenomicFeature": opencb_2_9_3.GenomicFeature,
        }

    __slots__ = [
        "ensemblId",
        "featureType",
        "xrefs",
    ]

    def __init__(
        self,
        ensemblId=None,
        featureType=None,
        xrefs=None,
        validate=None,
        **kwargs
    ):
        """Initialise the opencb_2_9_3.GenomicFeature model.

        :param ensemblId:
            Feature used, this should be a feature ID from Ensembl, (i.e,
            ENST00000544455)
        :type ensemblId: None | str
        :param featureType:
            Feature Type
        :type featureType: None | FeatureTypes
        :param xrefs:
            Others IDs. Fields like the HGNC symbol if available should be added
            here
        :type xrefs: None | dict[str, str]
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "2.9.3-GEL"
