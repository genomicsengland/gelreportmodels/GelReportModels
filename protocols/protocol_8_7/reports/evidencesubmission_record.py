"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class EvidenceSubmission(ProtocolElement):
    """
    The submission information
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {}

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_7 import reports as reports_6_9_0

        return {
            "org.gel.models.report.avro.EvidenceSubmission": reports_6_9_0.EvidenceSubmission,
        }

    __slots__ = [
        "date",
        "id",
        "submitter",
    ]

    def __init__(
        self,
        date=None,
        id=None,
        submitter=None,
        validate=None,
        **kwargs
    ):
        """Initialise the reports_6_9_0.EvidenceSubmission model.

        :param date:
            The submission date
        :type date: None | str
        :param id:
            The submission id
        :type id: None | str
        :param submitter:
            The submitter
        :type submitter: None | str
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "6.9.0"
