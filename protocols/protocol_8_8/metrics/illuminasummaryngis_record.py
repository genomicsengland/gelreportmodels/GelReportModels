"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class IlluminaSummaryNGIS(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "ANALYSIS_VERSION",
        "AUTOSOME_EXON_MEAN_COVERAGE",
        "AUTOSOME_MEAN_COVERAGE",
        "CNVS",
        "CNVS_ALL",
        "CNVS_IN_GENES",
        "DELETIONS",
        "DELETIONS_ALL",
        "DELETIONS_IN_CODING_REGIONS",
        "DELETIONS_IN_EXONS",
        "DELETIONS_IN_GENES",
        "DELETIONS_IN_MATURE_MIRNA",
        "DELETIONS_IN_SPLICE_SITE_REGIONS",
        "DELETIONS_IN_UTR_REGIONS",
        "DELETIONS_PERCENT_FOUND_IN_DBSNP",
        "DELETION_HET_HOM_RATIO",
        "DE_NOVO_CNVS",
        "DE_NOVO_SVS",
        "DIVERSITY",
        "FILTERED_HET___HOM_LARGE_ROH",
        "FRAGMENT_LENGTH_MAX",
        "FRAGMENT_LENGTH_MEDIAN",
        "FRAGMENT_LENGTH_MIN",
        "FRAGMENT_LENGTH_SD",
        "FRAMESHIFT_DELETIONS",
        "FRAMESHIFT_INSERTIONS",
        "INDELS",
        "INDELS_ALL",
        "INDELS_PERCENT_FOUND_IN_DBSNP",
        "INDEL_HET_HOM_RATIO",
        "INSERTIONS",
        "INSERTIONS_ALL",
        "INSERTIONS_IN_CODING_REGIONS",
        "INSERTIONS_IN_EXONS",
        "INSERTIONS_IN_GENES",
        "INSERTIONS_IN_MATURE_MIRNA",
        "INSERTIONS_IN_SPLICE_SITE_REGIONS",
        "INSERTIONS_IN_UTR_REGIONS",
        "INSERTIONS_PERCENT_FOUND_IN_DBSNP",
        "INSERTION_HET_HOM_RATIO",
        "MAPQ_GT_10_AUTOSOME_MEDIAN_COVERAGE",
        "MEAN_COVERAGE",
        "MEDIAN_READ_LENGTH",
        "MEDIAN_READ_LENGTH_READ_1",
        "MEDIAN_READ_LENGTH_READ_2",
        "METRICS_DELIVERABLE",
        "METRICS_VERSION",
        "NONSYNONYMOUS_DELETIONS",
        "NONSYNONYMOUS_INSERTIONS",
        "NONSYNONYMOUS_SNVS",
        "NUMBER_OF_LARGE_ROH",
        "PAIRED_END",
        "PERCENT_ADAPTER_BASES",
        "PERCENT_ALIGNED_BASES",
        "PERCENT_ALIGNED_BASES_READ_1",
        "PERCENT_ALIGNED_BASES_READ_2",
        "PERCENT_ALIGNED_READS",
        "PERCENT_ALIGNED_READ_1",
        "PERCENT_ALIGNED_READ_2",
        "PERCENT_AT_DROPOUT",
        "PERCENT_AT_DROPOUT_ALIGNED",
        "PERCENT_AUTOSOME_CALLABILITY",
        "PERCENT_AUTOSOME_COVERAGE_AT_10X",
        "PERCENT_AUTOSOME_COVERAGE_AT_15X",
        "PERCENT_AUTOSOME_COVERAGE_AT_1X",
        "PERCENT_AUTOSOME_EXON_CALLABILITY",
        "PERCENT_AUTOSOME_EXON_COVERAGE_AT_10X",
        "PERCENT_AUTOSOME_EXON_COVERAGE_AT_15X",
        "PERCENT_AUTOSOME_EXON_COVERAGE_AT_1X",
        "PERCENT_CALLABILITY",
        "PERCENT_CNVS_IN_GENES",
        "PERCENT_COVERAGE_AT_10X",
        "PERCENT_COVERAGE_AT_15X",
        "PERCENT_COVERAGE_AT_1X",
        "PERCENT_DUPLICATE_ALIGNED_READS",
        "PERCENT_DUPLICATE_PROPER_READ_PAIRS",
        "PERCENT_GC_DROPOUT",
        "PERCENT_GC_DROPOUT_ALIGNED",
        "PERCENT_MAPQ_GT_10_AUTOSOME_COVERAGE_AT_15X",
        "PERCENT_MAPQ_GT_10_AUTOSOME_EXON_COVERAGE_AT_15X",
        "PERCENT_MISMATCHES",
        "PERCENT_MISMATCHES_READ_1",
        "PERCENT_MISMATCHES_READ_2",
        "PERCENT_NOISE_BASES",
        "PERCENT_NOISE_SITES",
        "PERCENT_NONSPATIAL_DUPLICATE_READ_PAIRS",
        "PERCENT_OVERLAPPING_BASES",
        "PERCENT_Q25_BASES_READ_1",
        "PERCENT_Q25_BASES_READ_2",
        "PERCENT_Q30_BASES",
        "PERCENT_Q30_BASES_READ_1",
        "PERCENT_Q30_BASES_READ_2",
        "PERCENT_READ_PAIRS_ALIGNED_TO_DIFFERENT_CHROMOSOMES",
        "PERCENT_SNVS_IN_LARGE_ROH",
        "PERCENT_SOFTCLIPPED_BASES",
        "PREDICTED_SEX_CHROMOSOME_PLOIDY",
        "PROVIDED_SEX_CHROMOSOME_PLOIDY",
        "Q30_GIGABASES_EXCLUDING_CLIPPED_AND_DUPLICATE_READ_BASES",
        "READ_ENRICHMENT_AT_75_PERCENT_GC",
        "READ_ENRICHMENT_AT_80_PERCENT_GC",
        "REFERENCE_GENOME",
        "SAMPLE_ID",
        "SAMPLE_NAME",
        "SNVS",
        "SNVS_ALL",
        "SNVS_IN_CODING_REGIONS",
        "SNVS_IN_EXONS",
        "SNVS_IN_GENES",
        "SNVS_IN_MATURE_MIRNA",
        "SNVS_IN_SPLICE_SITE_REGIONS",
        "SNVS_IN_UTR_REGIONS",
        "SNVS_PERCENT_FOUND_IN_DBSNP",
        "SNV_HET_HOM_RATIO",
        "SNV_TS_TV_RATIO",
        "STOP_GAINED_DELETIONS",
        "STOP_GAINED_INSERTIONS",
        "STOP_GAINED_SNVS",
        "STOP_LOST_DELETIONS",
        "STOP_LOST_INSERTIONS",
        "STOP_LOST_SNVS",
        "SV_BREAKENDS",
        "SV_BREAKENDS_ALL",
        "SV_BREAKENDS_IN_GENES",
        "SV_DELETIONS",
        "SV_DELETIONS_ALL",
        "SV_DELETIONS_IN_GENES",
        "SV_INSERTIONS",
        "SV_INSERTIONS_ALL",
        "SV_INSERTIONS_IN_GENES",
        "SV_INVERSIONS",
        "SV_INVERSIONS_ALL",
        "SV_INVERSIONS_IN_GENES",
        "SV_PERCENT_BREAKENDS_IN_GENES",
        "SV_PERCENT_DELETIONS_IN_GENES",
        "SV_PERCENT_INSERTIONS_IN_GENES",
        "SV_PERCENT_INVERSIONS_IN_GENES",
        "SV_PERCENT_TANDEM_DUPLICATIONS_IN_GENES",
        "SV_TANDEM_DUPLICATIONS",
        "SV_TANDEM_DUPLICATIONS_ALL",
        "SV_TANDEM_DUPLICATIONS_IN_GENES",
        "SYNONYMOUS_SNVS",
        "TOTAL_ALIGNED_BASES",
        "TOTAL_ALIGNED_BASES_READ_1",
        "TOTAL_ALIGNED_BASES_READ_2",
        "TOTAL_ALIGNED_READS",
        "TOTAL_ALIGNED_READ_1",
        "TOTAL_ALIGNED_READ_2",
        "TOTAL_ALIGNED_READ_PAIRS",
        "TOTAL_DUPLICATE_ALIGNED_READS",
        "TOTAL_DUPLICATE_PROPER_READ_PAIRS",
        "TOTAL_GIGABASES_EXCLUDING_CLIPPED_AND_DUPLICATE_READ_BASES",
        "TOTAL_MAPQ_GT_10_READS",
        "TOTAL_PF_BASES",
        "TOTAL_PF_BASES_READ_1",
        "TOTAL_PF_BASES_READ_2",
        "TOTAL_PF_READS",
        "TOTAL_PF_READ_1",
        "TOTAL_PF_READ_2",
        "TOTAL_PROPER_READ_PAIRS",
        "UNIQUE_ALIGNED_READS",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_8 import metrics as metrics_1_3_0

        return {
            "org.gel.models.report.avro.IlluminaSummaryNGIS": metrics_1_3_0.IlluminaSummaryNGIS,
        }

    __slots__ = [
        "ANALYSIS_VERSION",
        "AUTOSOME_EXON_MEAN_COVERAGE",
        "AUTOSOME_MEAN_COVERAGE",
        "CNVS",
        "CNVS_ALL",
        "CNVS_IN_GENES",
        "DELETIONS",
        "DELETIONS_ALL",
        "DELETIONS_IN_CODING_REGIONS",
        "DELETIONS_IN_EXONS",
        "DELETIONS_IN_GENES",
        "DELETIONS_IN_MATURE_MIRNA",
        "DELETIONS_IN_SPLICE_SITE_REGIONS",
        "DELETIONS_IN_UTR_REGIONS",
        "DELETIONS_PERCENT_FOUND_IN_DBSNP",
        "DELETION_HET_HOM_RATIO",
        "DE_NOVO_CNVS",
        "DE_NOVO_SVS",
        "DIVERSITY",
        "FILTERED_HET___HOM_LARGE_ROH",
        "FRAGMENT_LENGTH_MAX",
        "FRAGMENT_LENGTH_MEDIAN",
        "FRAGMENT_LENGTH_MIN",
        "FRAGMENT_LENGTH_SD",
        "FRAMESHIFT_DELETIONS",
        "FRAMESHIFT_INSERTIONS",
        "INDELS",
        "INDELS_ALL",
        "INDELS_PERCENT_FOUND_IN_DBSNP",
        "INDEL_HET_HOM_RATIO",
        "INSERTIONS",
        "INSERTIONS_ALL",
        "INSERTIONS_IN_CODING_REGIONS",
        "INSERTIONS_IN_EXONS",
        "INSERTIONS_IN_GENES",
        "INSERTIONS_IN_MATURE_MIRNA",
        "INSERTIONS_IN_SPLICE_SITE_REGIONS",
        "INSERTIONS_IN_UTR_REGIONS",
        "INSERTIONS_PERCENT_FOUND_IN_DBSNP",
        "INSERTION_HET_HOM_RATIO",
        "MAPQ_GT_10_AUTOSOME_MEDIAN_COVERAGE",
        "MEAN_COVERAGE",
        "MEDIAN_READ_LENGTH",
        "MEDIAN_READ_LENGTH_READ_1",
        "MEDIAN_READ_LENGTH_READ_2",
        "METRICS_DELIVERABLE",
        "METRICS_VERSION",
        "NONSYNONYMOUS_DELETIONS",
        "NONSYNONYMOUS_INSERTIONS",
        "NONSYNONYMOUS_SNVS",
        "NUMBER_OF_LARGE_ROH",
        "PAIRED_END",
        "PERCENT_ADAPTER_BASES",
        "PERCENT_ALIGNED_BASES",
        "PERCENT_ALIGNED_BASES_READ_1",
        "PERCENT_ALIGNED_BASES_READ_2",
        "PERCENT_ALIGNED_READS",
        "PERCENT_ALIGNED_READ_1",
        "PERCENT_ALIGNED_READ_2",
        "PERCENT_AT_DROPOUT",
        "PERCENT_AT_DROPOUT_ALIGNED",
        "PERCENT_AUTOSOME_CALLABILITY",
        "PERCENT_AUTOSOME_COVERAGE_AT_10X",
        "PERCENT_AUTOSOME_COVERAGE_AT_15X",
        "PERCENT_AUTOSOME_COVERAGE_AT_1X",
        "PERCENT_AUTOSOME_EXON_CALLABILITY",
        "PERCENT_AUTOSOME_EXON_COVERAGE_AT_10X",
        "PERCENT_AUTOSOME_EXON_COVERAGE_AT_15X",
        "PERCENT_AUTOSOME_EXON_COVERAGE_AT_1X",
        "PERCENT_CALLABILITY",
        "PERCENT_CNVS_IN_GENES",
        "PERCENT_COVERAGE_AT_10X",
        "PERCENT_COVERAGE_AT_15X",
        "PERCENT_COVERAGE_AT_1X",
        "PERCENT_DUPLICATE_ALIGNED_READS",
        "PERCENT_DUPLICATE_PROPER_READ_PAIRS",
        "PERCENT_GC_DROPOUT",
        "PERCENT_GC_DROPOUT_ALIGNED",
        "PERCENT_MAPQ_GT_10_AUTOSOME_COVERAGE_AT_15X",
        "PERCENT_MAPQ_GT_10_AUTOSOME_EXON_COVERAGE_AT_15X",
        "PERCENT_MISMATCHES",
        "PERCENT_MISMATCHES_READ_1",
        "PERCENT_MISMATCHES_READ_2",
        "PERCENT_NOISE_BASES",
        "PERCENT_NOISE_SITES",
        "PERCENT_NONSPATIAL_DUPLICATE_READ_PAIRS",
        "PERCENT_OVERLAPPING_BASES",
        "PERCENT_Q25_BASES_READ_1",
        "PERCENT_Q25_BASES_READ_2",
        "PERCENT_Q30_BASES",
        "PERCENT_Q30_BASES_READ_1",
        "PERCENT_Q30_BASES_READ_2",
        "PERCENT_READ_PAIRS_ALIGNED_TO_DIFFERENT_CHROMOSOMES",
        "PERCENT_SNVS_IN_LARGE_ROH",
        "PERCENT_SOFTCLIPPED_BASES",
        "PREDICTED_SEX_CHROMOSOME_PLOIDY",
        "PROVIDED_SEX_CHROMOSOME_PLOIDY",
        "Q30_GIGABASES_EXCLUDING_CLIPPED_AND_DUPLICATE_READ_BASES",
        "READ_ENRICHMENT_AT_75_PERCENT_GC",
        "READ_ENRICHMENT_AT_80_PERCENT_GC",
        "REFERENCE_GENOME",
        "SAMPLE_ID",
        "SAMPLE_NAME",
        "SNVS",
        "SNVS_ALL",
        "SNVS_IN_CODING_REGIONS",
        "SNVS_IN_EXONS",
        "SNVS_IN_GENES",
        "SNVS_IN_MATURE_MIRNA",
        "SNVS_IN_SPLICE_SITE_REGIONS",
        "SNVS_IN_UTR_REGIONS",
        "SNVS_PERCENT_FOUND_IN_DBSNP",
        "SNV_HET_HOM_RATIO",
        "SNV_TS_TV_RATIO",
        "STOP_GAINED_DELETIONS",
        "STOP_GAINED_INSERTIONS",
        "STOP_GAINED_SNVS",
        "STOP_LOST_DELETIONS",
        "STOP_LOST_INSERTIONS",
        "STOP_LOST_SNVS",
        "SV_BREAKENDS",
        "SV_BREAKENDS_ALL",
        "SV_BREAKENDS_IN_GENES",
        "SV_DELETIONS",
        "SV_DELETIONS_ALL",
        "SV_DELETIONS_IN_GENES",
        "SV_INSERTIONS",
        "SV_INSERTIONS_ALL",
        "SV_INSERTIONS_IN_GENES",
        "SV_INVERSIONS",
        "SV_INVERSIONS_ALL",
        "SV_INVERSIONS_IN_GENES",
        "SV_PERCENT_BREAKENDS_IN_GENES",
        "SV_PERCENT_DELETIONS_IN_GENES",
        "SV_PERCENT_INSERTIONS_IN_GENES",
        "SV_PERCENT_INVERSIONS_IN_GENES",
        "SV_PERCENT_TANDEM_DUPLICATIONS_IN_GENES",
        "SV_TANDEM_DUPLICATIONS",
        "SV_TANDEM_DUPLICATIONS_ALL",
        "SV_TANDEM_DUPLICATIONS_IN_GENES",
        "SYNONYMOUS_SNVS",
        "TOTAL_ALIGNED_BASES",
        "TOTAL_ALIGNED_BASES_READ_1",
        "TOTAL_ALIGNED_BASES_READ_2",
        "TOTAL_ALIGNED_READS",
        "TOTAL_ALIGNED_READ_1",
        "TOTAL_ALIGNED_READ_2",
        "TOTAL_ALIGNED_READ_PAIRS",
        "TOTAL_DUPLICATE_ALIGNED_READS",
        "TOTAL_DUPLICATE_PROPER_READ_PAIRS",
        "TOTAL_GIGABASES_EXCLUDING_CLIPPED_AND_DUPLICATE_READ_BASES",
        "TOTAL_MAPQ_GT_10_READS",
        "TOTAL_PF_BASES",
        "TOTAL_PF_BASES_READ_1",
        "TOTAL_PF_BASES_READ_2",
        "TOTAL_PF_READS",
        "TOTAL_PF_READ_1",
        "TOTAL_PF_READ_2",
        "TOTAL_PROPER_READ_PAIRS",
        "UNIQUE_ALIGNED_READS",
    ]

    def __init__(
        self,
        ANALYSIS_VERSION,
        AUTOSOME_EXON_MEAN_COVERAGE,
        AUTOSOME_MEAN_COVERAGE,
        CNVS,
        CNVS_ALL,
        CNVS_IN_GENES,
        DELETIONS,
        DELETIONS_ALL,
        DELETIONS_IN_CODING_REGIONS,
        DELETIONS_IN_EXONS,
        DELETIONS_IN_GENES,
        DELETIONS_IN_MATURE_MIRNA,
        DELETIONS_IN_SPLICE_SITE_REGIONS,
        DELETIONS_IN_UTR_REGIONS,
        DELETIONS_PERCENT_FOUND_IN_DBSNP,
        DELETION_HET_HOM_RATIO,
        DE_NOVO_CNVS,
        DE_NOVO_SVS,
        DIVERSITY,
        FILTERED_HET___HOM_LARGE_ROH,
        FRAGMENT_LENGTH_MAX,
        FRAGMENT_LENGTH_MEDIAN,
        FRAGMENT_LENGTH_MIN,
        FRAGMENT_LENGTH_SD,
        FRAMESHIFT_DELETIONS,
        FRAMESHIFT_INSERTIONS,
        INDELS,
        INDELS_ALL,
        INDELS_PERCENT_FOUND_IN_DBSNP,
        INDEL_HET_HOM_RATIO,
        INSERTIONS,
        INSERTIONS_ALL,
        INSERTIONS_IN_CODING_REGIONS,
        INSERTIONS_IN_EXONS,
        INSERTIONS_IN_GENES,
        INSERTIONS_IN_MATURE_MIRNA,
        INSERTIONS_IN_SPLICE_SITE_REGIONS,
        INSERTIONS_IN_UTR_REGIONS,
        INSERTIONS_PERCENT_FOUND_IN_DBSNP,
        INSERTION_HET_HOM_RATIO,
        MAPQ_GT_10_AUTOSOME_MEDIAN_COVERAGE,
        MEAN_COVERAGE,
        MEDIAN_READ_LENGTH,
        MEDIAN_READ_LENGTH_READ_1,
        MEDIAN_READ_LENGTH_READ_2,
        METRICS_DELIVERABLE,
        METRICS_VERSION,
        NONSYNONYMOUS_DELETIONS,
        NONSYNONYMOUS_INSERTIONS,
        NONSYNONYMOUS_SNVS,
        NUMBER_OF_LARGE_ROH,
        PAIRED_END,
        PERCENT_ADAPTER_BASES,
        PERCENT_ALIGNED_BASES,
        PERCENT_ALIGNED_BASES_READ_1,
        PERCENT_ALIGNED_BASES_READ_2,
        PERCENT_ALIGNED_READS,
        PERCENT_ALIGNED_READ_1,
        PERCENT_ALIGNED_READ_2,
        PERCENT_AT_DROPOUT,
        PERCENT_AT_DROPOUT_ALIGNED,
        PERCENT_AUTOSOME_CALLABILITY,
        PERCENT_AUTOSOME_COVERAGE_AT_10X,
        PERCENT_AUTOSOME_COVERAGE_AT_15X,
        PERCENT_AUTOSOME_COVERAGE_AT_1X,
        PERCENT_AUTOSOME_EXON_CALLABILITY,
        PERCENT_AUTOSOME_EXON_COVERAGE_AT_10X,
        PERCENT_AUTOSOME_EXON_COVERAGE_AT_15X,
        PERCENT_AUTOSOME_EXON_COVERAGE_AT_1X,
        PERCENT_CALLABILITY,
        PERCENT_CNVS_IN_GENES,
        PERCENT_COVERAGE_AT_10X,
        PERCENT_COVERAGE_AT_15X,
        PERCENT_COVERAGE_AT_1X,
        PERCENT_DUPLICATE_ALIGNED_READS,
        PERCENT_DUPLICATE_PROPER_READ_PAIRS,
        PERCENT_GC_DROPOUT,
        PERCENT_GC_DROPOUT_ALIGNED,
        PERCENT_MAPQ_GT_10_AUTOSOME_COVERAGE_AT_15X,
        PERCENT_MAPQ_GT_10_AUTOSOME_EXON_COVERAGE_AT_15X,
        PERCENT_MISMATCHES,
        PERCENT_MISMATCHES_READ_1,
        PERCENT_MISMATCHES_READ_2,
        PERCENT_NOISE_BASES,
        PERCENT_NOISE_SITES,
        PERCENT_NONSPATIAL_DUPLICATE_READ_PAIRS,
        PERCENT_OVERLAPPING_BASES,
        PERCENT_Q25_BASES_READ_1,
        PERCENT_Q25_BASES_READ_2,
        PERCENT_Q30_BASES,
        PERCENT_Q30_BASES_READ_1,
        PERCENT_Q30_BASES_READ_2,
        PERCENT_READ_PAIRS_ALIGNED_TO_DIFFERENT_CHROMOSOMES,
        PERCENT_SNVS_IN_LARGE_ROH,
        PERCENT_SOFTCLIPPED_BASES,
        PREDICTED_SEX_CHROMOSOME_PLOIDY,
        PROVIDED_SEX_CHROMOSOME_PLOIDY,
        Q30_GIGABASES_EXCLUDING_CLIPPED_AND_DUPLICATE_READ_BASES,
        READ_ENRICHMENT_AT_75_PERCENT_GC,
        READ_ENRICHMENT_AT_80_PERCENT_GC,
        REFERENCE_GENOME,
        SAMPLE_ID,
        SAMPLE_NAME,
        SNVS,
        SNVS_ALL,
        SNVS_IN_CODING_REGIONS,
        SNVS_IN_EXONS,
        SNVS_IN_GENES,
        SNVS_IN_MATURE_MIRNA,
        SNVS_IN_SPLICE_SITE_REGIONS,
        SNVS_IN_UTR_REGIONS,
        SNVS_PERCENT_FOUND_IN_DBSNP,
        SNV_HET_HOM_RATIO,
        SNV_TS_TV_RATIO,
        STOP_GAINED_DELETIONS,
        STOP_GAINED_INSERTIONS,
        STOP_GAINED_SNVS,
        STOP_LOST_DELETIONS,
        STOP_LOST_INSERTIONS,
        STOP_LOST_SNVS,
        SV_BREAKENDS,
        SV_BREAKENDS_ALL,
        SV_BREAKENDS_IN_GENES,
        SV_DELETIONS,
        SV_DELETIONS_ALL,
        SV_DELETIONS_IN_GENES,
        SV_INSERTIONS,
        SV_INSERTIONS_ALL,
        SV_INSERTIONS_IN_GENES,
        SV_INVERSIONS,
        SV_INVERSIONS_ALL,
        SV_INVERSIONS_IN_GENES,
        SV_PERCENT_BREAKENDS_IN_GENES,
        SV_PERCENT_DELETIONS_IN_GENES,
        SV_PERCENT_INSERTIONS_IN_GENES,
        SV_PERCENT_INVERSIONS_IN_GENES,
        SV_PERCENT_TANDEM_DUPLICATIONS_IN_GENES,
        SV_TANDEM_DUPLICATIONS,
        SV_TANDEM_DUPLICATIONS_ALL,
        SV_TANDEM_DUPLICATIONS_IN_GENES,
        SYNONYMOUS_SNVS,
        TOTAL_ALIGNED_BASES,
        TOTAL_ALIGNED_BASES_READ_1,
        TOTAL_ALIGNED_BASES_READ_2,
        TOTAL_ALIGNED_READS,
        TOTAL_ALIGNED_READ_1,
        TOTAL_ALIGNED_READ_2,
        TOTAL_ALIGNED_READ_PAIRS,
        TOTAL_DUPLICATE_ALIGNED_READS,
        TOTAL_DUPLICATE_PROPER_READ_PAIRS,
        TOTAL_GIGABASES_EXCLUDING_CLIPPED_AND_DUPLICATE_READ_BASES,
        TOTAL_MAPQ_GT_10_READS,
        TOTAL_PF_BASES,
        TOTAL_PF_BASES_READ_1,
        TOTAL_PF_BASES_READ_2,
        TOTAL_PF_READS,
        TOTAL_PF_READ_1,
        TOTAL_PF_READ_2,
        TOTAL_PROPER_READ_PAIRS,
        UNIQUE_ALIGNED_READS,
        validate=None,
        **kwargs
    ):
        """Initialise the metrics_1_3_0.IlluminaSummaryNGIS model.

        :param ANALYSIS_VERSION:
        :type ANALYSIS_VERSION: str
        :param AUTOSOME_EXON_MEAN_COVERAGE:
        :type AUTOSOME_EXON_MEAN_COVERAGE: double
        :param AUTOSOME_MEAN_COVERAGE:
        :type AUTOSOME_MEAN_COVERAGE: double
        :param CNVS:
        :type CNVS: int
        :param CNVS_ALL:
        :type CNVS_ALL: int
        :param CNVS_IN_GENES:
        :type CNVS_IN_GENES: int
        :param DELETIONS:
        :type DELETIONS: int
        :param DELETIONS_ALL:
        :type DELETIONS_ALL: int
        :param DELETIONS_IN_CODING_REGIONS:
        :type DELETIONS_IN_CODING_REGIONS: int
        :param DELETIONS_IN_EXONS:
        :type DELETIONS_IN_EXONS: int
        :param DELETIONS_IN_GENES:
        :type DELETIONS_IN_GENES: int
        :param DELETIONS_IN_MATURE_MIRNA:
        :type DELETIONS_IN_MATURE_MIRNA: int
        :param DELETIONS_IN_SPLICE_SITE_REGIONS:
        :type DELETIONS_IN_SPLICE_SITE_REGIONS: int
        :param DELETIONS_IN_UTR_REGIONS:
        :type DELETIONS_IN_UTR_REGIONS: int
        :param DELETIONS_PERCENT_FOUND_IN_DBSNP:
        :type DELETIONS_PERCENT_FOUND_IN_DBSNP: double
        :param DELETION_HET_HOM_RATIO:
        :type DELETION_HET_HOM_RATIO: double
        :param DE_NOVO_CNVS:
        :type DE_NOVO_CNVS: int
        :param DE_NOVO_SVS:
        :type DE_NOVO_SVS: int
        :param DIVERSITY:
        :type DIVERSITY: int
        :param FILTERED_HET___HOM_LARGE_ROH:
        :type FILTERED_HET___HOM_LARGE_ROH: double
        :param FRAGMENT_LENGTH_MAX:
        :type FRAGMENT_LENGTH_MAX: int
        :param FRAGMENT_LENGTH_MEDIAN:
        :type FRAGMENT_LENGTH_MEDIAN: int
        :param FRAGMENT_LENGTH_MIN:
        :type FRAGMENT_LENGTH_MIN: int
        :param FRAGMENT_LENGTH_SD:
        :type FRAGMENT_LENGTH_SD: int
        :param FRAMESHIFT_DELETIONS:
        :type FRAMESHIFT_DELETIONS: int
        :param FRAMESHIFT_INSERTIONS:
        :type FRAMESHIFT_INSERTIONS: int
        :param INDELS:
        :type INDELS: int
        :param INDELS_ALL:
        :type INDELS_ALL: int
        :param INDELS_PERCENT_FOUND_IN_DBSNP:
        :type INDELS_PERCENT_FOUND_IN_DBSNP: double
        :param INDEL_HET_HOM_RATIO:
        :type INDEL_HET_HOM_RATIO: double
        :param INSERTIONS:
        :type INSERTIONS: int
        :param INSERTIONS_ALL:
        :type INSERTIONS_ALL: int
        :param INSERTIONS_IN_CODING_REGIONS:
        :type INSERTIONS_IN_CODING_REGIONS: int
        :param INSERTIONS_IN_EXONS:
        :type INSERTIONS_IN_EXONS: int
        :param INSERTIONS_IN_GENES:
        :type INSERTIONS_IN_GENES: int
        :param INSERTIONS_IN_MATURE_MIRNA:
        :type INSERTIONS_IN_MATURE_MIRNA: int
        :param INSERTIONS_IN_SPLICE_SITE_REGIONS:
        :type INSERTIONS_IN_SPLICE_SITE_REGIONS: int
        :param INSERTIONS_IN_UTR_REGIONS:
        :type INSERTIONS_IN_UTR_REGIONS: int
        :param INSERTIONS_PERCENT_FOUND_IN_DBSNP:
        :type INSERTIONS_PERCENT_FOUND_IN_DBSNP: double
        :param INSERTION_HET_HOM_RATIO:
        :type INSERTION_HET_HOM_RATIO: double
        :param MAPQ_GT_10_AUTOSOME_MEDIAN_COVERAGE:
        :type MAPQ_GT_10_AUTOSOME_MEDIAN_COVERAGE: double
        :param MEAN_COVERAGE:
        :type MEAN_COVERAGE: double
        :param MEDIAN_READ_LENGTH:
        :type MEDIAN_READ_LENGTH: int
        :param MEDIAN_READ_LENGTH_READ_1:
        :type MEDIAN_READ_LENGTH_READ_1: int
        :param MEDIAN_READ_LENGTH_READ_2:
        :type MEDIAN_READ_LENGTH_READ_2: int
        :param METRICS_DELIVERABLE:
        :type METRICS_DELIVERABLE: str
        :param METRICS_VERSION:
        :type METRICS_VERSION: str
        :param NONSYNONYMOUS_DELETIONS:
        :type NONSYNONYMOUS_DELETIONS: int
        :param NONSYNONYMOUS_INSERTIONS:
        :type NONSYNONYMOUS_INSERTIONS: int
        :param NONSYNONYMOUS_SNVS:
        :type NONSYNONYMOUS_SNVS: int
        :param NUMBER_OF_LARGE_ROH:
        :type NUMBER_OF_LARGE_ROH: int
        :param PAIRED_END:
        :type PAIRED_END: boolean
        :param PERCENT_ADAPTER_BASES:
        :type PERCENT_ADAPTER_BASES: double
        :param PERCENT_ALIGNED_BASES:
        :type PERCENT_ALIGNED_BASES: double
        :param PERCENT_ALIGNED_BASES_READ_1:
        :type PERCENT_ALIGNED_BASES_READ_1: double
        :param PERCENT_ALIGNED_BASES_READ_2:
        :type PERCENT_ALIGNED_BASES_READ_2: double
        :param PERCENT_ALIGNED_READS:
        :type PERCENT_ALIGNED_READS: double
        :param PERCENT_ALIGNED_READ_1:
        :type PERCENT_ALIGNED_READ_1: double
        :param PERCENT_ALIGNED_READ_2:
        :type PERCENT_ALIGNED_READ_2: double
        :param PERCENT_AT_DROPOUT:
        :type PERCENT_AT_DROPOUT: double
        :param PERCENT_AT_DROPOUT_ALIGNED:
        :type PERCENT_AT_DROPOUT_ALIGNED: double
        :param PERCENT_AUTOSOME_CALLABILITY:
        :type PERCENT_AUTOSOME_CALLABILITY: double
        :param PERCENT_AUTOSOME_COVERAGE_AT_10X:
        :type PERCENT_AUTOSOME_COVERAGE_AT_10X: double
        :param PERCENT_AUTOSOME_COVERAGE_AT_15X:
        :type PERCENT_AUTOSOME_COVERAGE_AT_15X: double
        :param PERCENT_AUTOSOME_COVERAGE_AT_1X:
        :type PERCENT_AUTOSOME_COVERAGE_AT_1X: double
        :param PERCENT_AUTOSOME_EXON_CALLABILITY:
        :type PERCENT_AUTOSOME_EXON_CALLABILITY: double
        :param PERCENT_AUTOSOME_EXON_COVERAGE_AT_10X:
        :type PERCENT_AUTOSOME_EXON_COVERAGE_AT_10X: double
        :param PERCENT_AUTOSOME_EXON_COVERAGE_AT_15X:
        :type PERCENT_AUTOSOME_EXON_COVERAGE_AT_15X: double
        :param PERCENT_AUTOSOME_EXON_COVERAGE_AT_1X:
        :type PERCENT_AUTOSOME_EXON_COVERAGE_AT_1X: double
        :param PERCENT_CALLABILITY:
        :type PERCENT_CALLABILITY: double
        :param PERCENT_CNVS_IN_GENES:
        :type PERCENT_CNVS_IN_GENES: double
        :param PERCENT_COVERAGE_AT_10X:
        :type PERCENT_COVERAGE_AT_10X: double
        :param PERCENT_COVERAGE_AT_15X:
        :type PERCENT_COVERAGE_AT_15X: double
        :param PERCENT_COVERAGE_AT_1X:
        :type PERCENT_COVERAGE_AT_1X: double
        :param PERCENT_DUPLICATE_ALIGNED_READS:
        :type PERCENT_DUPLICATE_ALIGNED_READS: double
        :param PERCENT_DUPLICATE_PROPER_READ_PAIRS:
        :type PERCENT_DUPLICATE_PROPER_READ_PAIRS: double
        :param PERCENT_GC_DROPOUT:
        :type PERCENT_GC_DROPOUT: double
        :param PERCENT_GC_DROPOUT_ALIGNED:
        :type PERCENT_GC_DROPOUT_ALIGNED: double
        :param PERCENT_MAPQ_GT_10_AUTOSOME_COVERAGE_AT_15X:
        :type PERCENT_MAPQ_GT_10_AUTOSOME_COVERAGE_AT_15X: double
        :param PERCENT_MAPQ_GT_10_AUTOSOME_EXON_COVERAGE_AT_15X:
        :type PERCENT_MAPQ_GT_10_AUTOSOME_EXON_COVERAGE_AT_15X: double
        :param PERCENT_MISMATCHES:
        :type PERCENT_MISMATCHES: double
        :param PERCENT_MISMATCHES_READ_1:
        :type PERCENT_MISMATCHES_READ_1: double
        :param PERCENT_MISMATCHES_READ_2:
        :type PERCENT_MISMATCHES_READ_2: double
        :param PERCENT_NOISE_BASES:
        :type PERCENT_NOISE_BASES: double
        :param PERCENT_NOISE_SITES:
        :type PERCENT_NOISE_SITES: double
        :param PERCENT_NONSPATIAL_DUPLICATE_READ_PAIRS:
        :type PERCENT_NONSPATIAL_DUPLICATE_READ_PAIRS: double
        :param PERCENT_OVERLAPPING_BASES:
        :type PERCENT_OVERLAPPING_BASES: double
        :param PERCENT_Q25_BASES_READ_1:
        :type PERCENT_Q25_BASES_READ_1: double
        :param PERCENT_Q25_BASES_READ_2:
        :type PERCENT_Q25_BASES_READ_2: double
        :param PERCENT_Q30_BASES:
        :type PERCENT_Q30_BASES: double
        :param PERCENT_Q30_BASES_READ_1:
        :type PERCENT_Q30_BASES_READ_1: double
        :param PERCENT_Q30_BASES_READ_2:
        :type PERCENT_Q30_BASES_READ_2: double
        :param PERCENT_READ_PAIRS_ALIGNED_TO_DIFFERENT_CHROMOSOMES:
        :type PERCENT_READ_PAIRS_ALIGNED_TO_DIFFERENT_CHROMOSOMES: double
        :param PERCENT_SNVS_IN_LARGE_ROH:
        :type PERCENT_SNVS_IN_LARGE_ROH: double
        :param PERCENT_SOFTCLIPPED_BASES:
        :type PERCENT_SOFTCLIPPED_BASES: double
        :param PREDICTED_SEX_CHROMOSOME_PLOIDY:
        :type PREDICTED_SEX_CHROMOSOME_PLOIDY: str
        :param PROVIDED_SEX_CHROMOSOME_PLOIDY:
        :type PROVIDED_SEX_CHROMOSOME_PLOIDY: str
        :param Q30_GIGABASES_EXCLUDING_CLIPPED_AND_DUPLICATE_READ_BASES:
        :type Q30_GIGABASES_EXCLUDING_CLIPPED_AND_DUPLICATE_READ_BASES: double
        :param READ_ENRICHMENT_AT_75_PERCENT_GC:
        :type READ_ENRICHMENT_AT_75_PERCENT_GC: double
        :param READ_ENRICHMENT_AT_80_PERCENT_GC:
        :type READ_ENRICHMENT_AT_80_PERCENT_GC: double
        :param REFERENCE_GENOME:
        :type REFERENCE_GENOME: str
        :param SAMPLE_ID:
        :type SAMPLE_ID: str
        :param SAMPLE_NAME:
        :type SAMPLE_NAME: str
        :param SNVS:
        :type SNVS: int
        :param SNVS_ALL:
        :type SNVS_ALL: int
        :param SNVS_IN_CODING_REGIONS:
        :type SNVS_IN_CODING_REGIONS: int
        :param SNVS_IN_EXONS:
        :type SNVS_IN_EXONS: int
        :param SNVS_IN_GENES:
        :type SNVS_IN_GENES: int
        :param SNVS_IN_MATURE_MIRNA:
        :type SNVS_IN_MATURE_MIRNA: int
        :param SNVS_IN_SPLICE_SITE_REGIONS:
        :type SNVS_IN_SPLICE_SITE_REGIONS: int
        :param SNVS_IN_UTR_REGIONS:
        :type SNVS_IN_UTR_REGIONS: int
        :param SNVS_PERCENT_FOUND_IN_DBSNP:
        :type SNVS_PERCENT_FOUND_IN_DBSNP: double
        :param SNV_HET_HOM_RATIO:
        :type SNV_HET_HOM_RATIO: double
        :param SNV_TS_TV_RATIO:
        :type SNV_TS_TV_RATIO: double
        :param STOP_GAINED_DELETIONS:
        :type STOP_GAINED_DELETIONS: int
        :param STOP_GAINED_INSERTIONS:
        :type STOP_GAINED_INSERTIONS: int
        :param STOP_GAINED_SNVS:
        :type STOP_GAINED_SNVS: int
        :param STOP_LOST_DELETIONS:
        :type STOP_LOST_DELETIONS: int
        :param STOP_LOST_INSERTIONS:
        :type STOP_LOST_INSERTIONS: int
        :param STOP_LOST_SNVS:
        :type STOP_LOST_SNVS: int
        :param SV_BREAKENDS:
        :type SV_BREAKENDS: int
        :param SV_BREAKENDS_ALL:
        :type SV_BREAKENDS_ALL: int
        :param SV_BREAKENDS_IN_GENES:
        :type SV_BREAKENDS_IN_GENES: int
        :param SV_DELETIONS:
        :type SV_DELETIONS: int
        :param SV_DELETIONS_ALL:
        :type SV_DELETIONS_ALL: int
        :param SV_DELETIONS_IN_GENES:
        :type SV_DELETIONS_IN_GENES: int
        :param SV_INSERTIONS:
        :type SV_INSERTIONS: int
        :param SV_INSERTIONS_ALL:
        :type SV_INSERTIONS_ALL: int
        :param SV_INSERTIONS_IN_GENES:
        :type SV_INSERTIONS_IN_GENES: int
        :param SV_INVERSIONS:
        :type SV_INVERSIONS: int
        :param SV_INVERSIONS_ALL:
        :type SV_INVERSIONS_ALL: int
        :param SV_INVERSIONS_IN_GENES:
        :type SV_INVERSIONS_IN_GENES: int
        :param SV_PERCENT_BREAKENDS_IN_GENES:
        :type SV_PERCENT_BREAKENDS_IN_GENES: double
        :param SV_PERCENT_DELETIONS_IN_GENES:
        :type SV_PERCENT_DELETIONS_IN_GENES: double
        :param SV_PERCENT_INSERTIONS_IN_GENES:
        :type SV_PERCENT_INSERTIONS_IN_GENES: double
        :param SV_PERCENT_INVERSIONS_IN_GENES:
        :type SV_PERCENT_INVERSIONS_IN_GENES: double
        :param SV_PERCENT_TANDEM_DUPLICATIONS_IN_GENES:
        :type SV_PERCENT_TANDEM_DUPLICATIONS_IN_GENES: double
        :param SV_TANDEM_DUPLICATIONS:
        :type SV_TANDEM_DUPLICATIONS: int
        :param SV_TANDEM_DUPLICATIONS_ALL:
        :type SV_TANDEM_DUPLICATIONS_ALL: int
        :param SV_TANDEM_DUPLICATIONS_IN_GENES:
        :type SV_TANDEM_DUPLICATIONS_IN_GENES: int
        :param SYNONYMOUS_SNVS:
        :type SYNONYMOUS_SNVS: int
        :param TOTAL_ALIGNED_BASES:
        :type TOTAL_ALIGNED_BASES: int
        :param TOTAL_ALIGNED_BASES_READ_1:
        :type TOTAL_ALIGNED_BASES_READ_1: int
        :param TOTAL_ALIGNED_BASES_READ_2:
        :type TOTAL_ALIGNED_BASES_READ_2: int
        :param TOTAL_ALIGNED_READS:
        :type TOTAL_ALIGNED_READS: int
        :param TOTAL_ALIGNED_READ_1:
        :type TOTAL_ALIGNED_READ_1: int
        :param TOTAL_ALIGNED_READ_2:
        :type TOTAL_ALIGNED_READ_2: int
        :param TOTAL_ALIGNED_READ_PAIRS:
        :type TOTAL_ALIGNED_READ_PAIRS: int
        :param TOTAL_DUPLICATE_ALIGNED_READS:
        :type TOTAL_DUPLICATE_ALIGNED_READS: int
        :param TOTAL_DUPLICATE_PROPER_READ_PAIRS:
        :type TOTAL_DUPLICATE_PROPER_READ_PAIRS: int
        :param TOTAL_GIGABASES_EXCLUDING_CLIPPED_AND_DUPLICATE_READ_BASES:
        :type TOTAL_GIGABASES_EXCLUDING_CLIPPED_AND_DUPLICATE_READ_BASES: double
        :param TOTAL_MAPQ_GT_10_READS:
        :type TOTAL_MAPQ_GT_10_READS: int
        :param TOTAL_PF_BASES:
        :type TOTAL_PF_BASES: int
        :param TOTAL_PF_BASES_READ_1:
        :type TOTAL_PF_BASES_READ_1: int
        :param TOTAL_PF_BASES_READ_2:
        :type TOTAL_PF_BASES_READ_2: int
        :param TOTAL_PF_READS:
        :type TOTAL_PF_READS: int
        :param TOTAL_PF_READ_1:
        :type TOTAL_PF_READ_1: int
        :param TOTAL_PF_READ_2:
        :type TOTAL_PF_READ_2: int
        :param TOTAL_PROPER_READ_PAIRS:
        :type TOTAL_PROPER_READ_PAIRS: int
        :param UNIQUE_ALIGNED_READS:
        :type UNIQUE_ALIGNED_READS: int
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "1.3.0"
