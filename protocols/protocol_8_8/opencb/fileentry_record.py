"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class FileEntry(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "data",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_8 import opencb as opencb_2_9_3

        return {
            "call": opencb_2_9_3.OriginalCall,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_8 import opencb as opencb_2_9_3

        return {
            "org.opencb.biodata.models.variant.avro.OriginalCall": opencb_2_9_3.OriginalCall,
            "org.opencb.biodata.models.variant.avro.FileEntry": opencb_2_9_3.FileEntry,
        }

    __slots__ = [
        "data",
        "call",
        "fileId",
    ]

    def __init__(
        self,
        data,
        call=None,
        fileId=None,
        validate=None,
        **kwargs
    ):
        """Initialise the opencb_2_9_3.FileEntry model.

        :param data:
            Optional data that probably depend on the format of the file the
            variant was initially read from.
        :type data: dict[str, str]
        :param call:
            Original call position for the variant, if the file was normalized.
            {position}:{reference}:{alternate}(,{other_alternate})*:{allele_index}
        :type call: None | OriginalCall
        :param fileId:
            Unique identifier of the source file.
        :type fileId: None | str
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "2.9.3-GEL"
