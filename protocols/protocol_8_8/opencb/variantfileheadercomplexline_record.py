"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class VariantFileHeaderComplexLine(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "id",
        "key",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_8 import opencb as opencb_2_9_3

        return {
            "org.opencb.biodata.models.variant.metadata.VariantFileHeaderComplexLine": opencb_2_9_3.VariantFileHeaderComplexLine,
        }

    __slots__ = [
        "id",
        "key",
        "description",
        "genericFields",
        "number",
        "type",
    ]

    def __init__(
        self,
        id,
        key,
        description=None,
        genericFields=dict,
        number=None,
        type=None,
        validate=None,
        **kwargs
    ):
        """Initialise the opencb_2_9_3.VariantFileHeaderComplexLine model.

        :param id:
            ID or Name of the line
        :type id: str
        :param key:
            Key of group of the Complex Header Line, e.g. INFO, FORMAT, FILTER,
            ALT, ...
        :type key: str
        :param description:
            The description
        :type description: None | str
        :param genericFields:
            Other optional fields
        :type genericFields: dict[str, str]
        :param number:
            Arity of the values associated with this metadata line. Only present
            if the metadata line describes data fields, i.e. key ==
            INFO or FORMAT Accepted values:   - <Integer>: The field
            has always this number of values.   - A: The field has one
            value per alternate allele.   - R: The field has one value
            for each possible allele, including the reference.   - G:
            The field has one value for each possible genotype   - .:
            The number of possible values varies, is unknown or
            unbounded.
        :type number: None | str
        :param type:
            Type of the values associated with this metadata line. Only present if
            the metadata line describes data fields, i.e. key == INFO
            or FORMAT Accepted values:   - Integer   - Float   -
            String   - Character   - Flag
        :type type: None | str
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "2.9.3-GEL"
