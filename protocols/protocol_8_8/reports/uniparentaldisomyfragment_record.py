"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class UniparentalDisomyFragment(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "uniparentalDisomyType",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_8 import reports as reports_6_9_0

        return {
            "coordinates": reports_6_9_0.Coordinates,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_8 import reports as reports_6_9_0

        return {
            "uniparentalDisomyType": reports_6_9_0.UniparentalDisomyType,
        }

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_8 import reports as reports_6_9_0

        return {
            "org.gel.models.report.avro.Coordinates": reports_6_9_0.Coordinates,
            "org.gel.models.report.avro.UniparentalDisomyType": reports_6_9_0.UniparentalDisomyType,
            "org.gel.models.report.avro.UniparentalDisomyFragment": reports_6_9_0.UniparentalDisomyFragment,
        }

    __slots__ = [
        "uniparentalDisomyType",
        "coordinates",
    ]

    def __init__(
        self,
        uniparentalDisomyType,
        coordinates=None,
        validate=None,
        **kwargs
    ):
        """Initialise the reports_6_9_0.UniparentalDisomyFragment model.

        :param uniparentalDisomyType:
            indicates whether the UPD event involves `isodisomy`, `heterodisomy`
            or `both`
        :type uniparentalDisomyType: UniparentalDisomyType
        :param coordinates:
            Coordinates can be specified to indicate the part of the chromosome
            affected
        :type coordinates: None | Coordinates
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "6.9.0"
