"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class PlinkROH(ProtocolElement):
    """
    Plink runs of homozygosity
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {
        "FID",
        "IID",
        "KB",
        "KBAVG",
        "NSEG",
        "PHE",
    }

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        return {}

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_9 import metrics as metrics_1_3_0

        return {
            "org.gel.models.report.avro.PlinkROH": metrics_1_3_0.PlinkROH,
        }

    __slots__ = [
        "FID",
        "IID",
        "KB",
        "KBAVG",
        "NSEG",
        "PHE",
    ]

    def __init__(
        self,
        FID,
        IID,
        KB,
        KBAVG,
        NSEG,
        PHE,
        validate=None,
        **kwargs
    ):
        """Initialise the metrics_1_3_0.PlinkROH model.

        :param FID:
        :type FID: str
        :param IID:
        :type IID: str
        :param KB:
        :type KB: double
        :param KBAVG:
        :type KBAVG: double
        :param NSEG:
        :type NSEG: double
        :param PHE:
        :type PHE: double
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "1.3.0"
