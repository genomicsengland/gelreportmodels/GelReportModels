"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class HeritableTrait(ProtocolElement):
    """
    The entity representing a phenotype and its inheritance pattern.
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {}

    @classmethod
    def getEmbeddedTypes(cls):
        return {}

    @classmethod
    def getEmbeddedEnums(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_9 import opencb as opencb_2_9_3

        return {
            "inheritanceMode": opencb_2_9_3.ModeOfInheritance,
        }

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_8_9 import opencb as opencb_2_9_3

        return {
            "org.opencb.biodata.models.variant.avro.ModeOfInheritance": opencb_2_9_3.ModeOfInheritance,
            "org.opencb.biodata.models.variant.avro.HeritableTrait": opencb_2_9_3.HeritableTrait,
        }

    __slots__ = [
        "inheritanceMode",
        "trait",
    ]

    def __init__(
        self,
        inheritanceMode=None,
        trait=None,
        validate=None,
        **kwargs
    ):
        """Initialise the opencb_2_9_3.HeritableTrait model.

        :param inheritanceMode:
            The mode of inheritance
        :type inheritanceMode: None | ModeOfInheritance
        :param trait:
            The trait (e.g.: HPO term, MIM term, DO term etc.)
        :type trait: None | str
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "2.9.3-GEL"
