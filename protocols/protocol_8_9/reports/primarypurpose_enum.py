"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class PrimaryPurpose(object):
    """
    Treatment: One or more interventions are being evaluated for treating
    a disease, syndrome, or condition.     Prevention: One or more
    interventions are being assessed for preventing the development of
    a specific disease or health condition.     Diagnostic: One or
    more interventions are being evaluated for identifying a disease
    or health condition.     Supportive Care: One or more
    interventions are evaluated for maximizing comfort, minimizing
    side effects, or mitigating against a decline in the participant's
    health or function.     Screening: One or more interventions are
    assessed or examined for identifying a condition, or risk factors
    for a condition, in people who are not yet known to have the
    condition or risk factor.     Health Services Research: One or
    more interventions for evaluating the delivery, processes,
    management, organization, or financing of healthcare.     Basic
    Science: One or more interventions for examining the basic
    mechanism of action (for example, physiology or biomechanics of an
    intervention).     Device Feasibility: An intervention of a device
    product is being evaluated in a small clinical trial (generally
    fewer than 10 participants) to determine the feasibility of the
    product; or a clinical trial to test a prototype device for
    feasibility and not health outcomes. Such studies are conducted to
    confirm the design and operating specifications of a device before
    beginning a full clinical trial.     Other: None of the other
    options applies.      Ref.
    https://prsinfo.clinicaltrials.gov/definitions.htm
    """

    treatment = "treatment"
    prevention = "prevention"
    diagnostic = "diagnostic"
    supportive_care = "supportive_care"
    screening = "screening"
    health_services_research = "health_services_research"
    basic_science = "basic_science"
    device_feasibility = "device_feasibility"
    other = "other"

    def __hash__(self):
        return str(self).__hash__()

    @classmethod
    def _namespace_version(cls):
        return "6.10.0"
