"""
DO NOT EDIT THIS FILE!!
This file is automatically generated by the process_schemas.py program
in the scripts directory. It is not intended to be edited directly. If
you need to update the GEL protocol classes, please run the script
on the appropriate schema version.
"""
from protocols.protocol import (
    ProtocolElement,
    avro_parse,
    load_schema,
)


class AdditionalVariantsQuestions(ProtocolElement):
    """
    No documentation
    """

    # load schema from file
    _schemaSource = load_schema(__file__)
    # parse into avro classes
    schema = avro_parse(_schemaSource)

    requiredFields = {}

    @classmethod
    def getEmbeddedTypes(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_9_0 import reports as reports_7_0_0

        return {
            "variantCoordinates": reports_7_0_0.VariantCoordinates,
        }

    @classmethod
    def getEmbeddedEnums(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_9_0 import reports as reports_7_0_0

        return {
            "typeOfAdditionalFinding": reports_7_0_0.TypeOfAdditionalFinding,
            "variantActionability": reports_7_0_0.CancerActionability,
            "variantTested": reports_7_0_0.CancerTestedAdditional,
            "variantUsability": reports_7_0_0.CancerUsabilitySomatic,
        }

    @classmethod
    def getEmbeddedByNamespace(cls):
        # import here to prevent circular imports in python 2.7.
        from protocols.protocol_9_0 import reports as reports_7_0_0

        return {
            "org.gel.models.report.avro.TypeOfAdditionalFinding": reports_7_0_0.TypeOfAdditionalFinding,
            "org.gel.models.report.avro.CancerActionability": reports_7_0_0.CancerActionability,
            "org.gel.models.report.avro.VariantCoordinates": reports_7_0_0.VariantCoordinates,
            "org.gel.models.report.avro.CancerTestedAdditional": reports_7_0_0.CancerTestedAdditional,
            "org.gel.models.report.avro.CancerUsabilitySomatic": reports_7_0_0.CancerUsabilitySomatic,
            "org.gel.models.report.avro.AdditionalVariantsQuestions": reports_7_0_0.AdditionalVariantsQuestions,
        }

    __slots__ = [
        "findingDescription",
        "otherVariantActionability",
        "typeOfAdditionalFinding",
        "usabilityComment",
        "validationAssayType",
        "variantActionability",
        "variantCoordinates",
        "variantTested",
        "variantUsability",
    ]

    def __init__(
        self,
        findingDescription=None,
        otherVariantActionability=None,
        typeOfAdditionalFinding=None,
        usabilityComment=None,
        validationAssayType=None,
        variantActionability=None,
        variantCoordinates=None,
        variantTested=None,
        variantUsability=None,
        validate=None,
        **kwargs
    ):
        """Initialise the reports_7_0_0.AdditionalVariantsQuestions model.

        :param findingDescription:
        :type findingDescription: None | str
        :param otherVariantActionability:
        :type otherVariantActionability: None | str
        :param typeOfAdditionalFinding:
        :type typeOfAdditionalFinding: None | TypeOfAdditionalFinding
        :param usabilityComment:
            Comment on the variant usability
        :type usabilityComment: None | str
        :param validationAssayType:
            Please enter validation assay type e.g Pyrosequencing, NGS panel,
            COBAS, Sanger sequencing. If not applicable enter NA;
        :type validationAssayType: None | str
        :param variantActionability:
            Type of potential actionability:
        :type variantActionability: None | list[CancerActionability]
        :param variantCoordinates:
            Chr: Pos Ref > Alt
        :type variantCoordinates: None | VariantCoordinates
        :param variantTested:
            Has this variant been tested by another method (either prior to or
            following receipt of this WGA)?
        :type variantTested: None | CancerTestedAdditional
        :param variantUsability:
            How has/will this potentially actionable variant been/be used?
        :type variantUsability: None | CancerUsabilitySomatic
        """

        attributes = {k: v for k, v in locals().items() if k not in ["self", "unknown"]}
        ProtocolElement.__init__(self, **attributes)

    @classmethod
    def _namespace_version(cls):
        return "7.0.0"
