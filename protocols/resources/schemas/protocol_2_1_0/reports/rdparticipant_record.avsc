{
    "doc": "This defines a RD Participant (demographics and pedigree information)",
    "fields": [
        {
            "doc": "Model version number",
            "name": "versionControl",
            "type": {
                "fields": [
                    {
                        "default": "2.1.0",
                        "doc": "This is the version for the entire set of data models as referred to the Git release tag",
                        "name": "GitVersionControl",
                        "type": "string"
                    }
                ],
                "name": "VersionControl",
                "type": "record"
            }
        },
        {
            "doc": "Numbering used to refer to each member of the pedigree",
            "name": "pedigreeId",
            "type": "int"
        },
        {
            "doc": "If this field is true, the member should be consider the proband of this family",
            "name": "isProband",
            "type": "boolean"
        },
        {
            "doc": "gel internal identifier, only required when the participant belongs to the program",
            "name": "gelId",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Family id which internally translate to a sample set",
            "name": "gelFamilyId",
            "type": "string"
        },
        {
            "doc": "superFamily id, this id is built as a concatenation of all families id in this superfamily i.e, fam10024_fam100457",
            "name": "gelSuperFamilyId",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Sex of the Participant",
            "name": "sex",
            "type": {
                "doc": "Sex",
                "name": "Sex",
                "symbols": [
                    "male",
                    "female",
                    "unknown",
                    "undetermined"
                ],
                "type": "enum"
            }
        },
        {
            "doc": "Karyotypic sex of the participant as previously established or by looking at the GEL genome",
            "name": "personKaryotipicSex",
            "type": [
                "null",
                {
                    "doc": "Karyotipic Sex",
                    "name": "PersonKaryotipicSex",
                    "symbols": [
                        "unknown",
                        "XX",
                        "XY",
                        "XO",
                        "XXY",
                        "XXX",
                        "XXYY",
                        "XXXY",
                        "XXXX",
                        "XYY",
                        "other"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "refers to the pedigreeId of the father\n    Id of the parent, if unknown then no parent is referenced. Parents may need to be entered even if no data is known\n    about them in order to unambiguously reconstruct the pedigree.",
            "name": "fatherId",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "doc": "refers to the pedigreeId of the mother\n    Id of the parent, if unknown then no parent is referenced. Parents may need to be entered even if no data is known\n    about them in order to unambiguously reconstruct the pedigree.",
            "name": "motherId",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "doc": "this id is built using the original familyId and the original pedigreeId of the father",
            "name": "superFatherId",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "doc": "this id is built using the original familyId and the original pedigreeId of the mother",
            "name": "superMotherId",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "doc": "Each twin group is numbered, i.e. all members of a group of multiparous births receive the same number",
            "name": "twinGroup",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "doc": "A property of the twinning group but should be entered for all members",
            "name": "monozygotic",
            "type": [
                "null",
                {
                    "doc": "This the define a yes/no/unknown case",
                    "name": "TernaryOption",
                    "symbols": [
                        "yes",
                        "no",
                        "unknown"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "Adopted Status",
            "name": "adoptedStatus",
            "type": {
                "doc": "adoptedin means adopted into the family\nadoptedout means child belonged to the family and was adopted out",
                "name": "AdoptedStatus",
                "symbols": [
                    "not_adopted",
                    "adoptedin",
                    "adoptedout"
                ],
                "type": "enum"
            }
        },
        {
            "doc": "Life Status",
            "name": "lifeStatus",
            "type": {
                "doc": "Life Status",
                "name": "LifeStatus",
                "symbols": [
                    "alive",
                    "aborted",
                    "deceased",
                    "unborn",
                    "stillborn",
                    "miscarriage"
                ],
                "type": "enum"
            }
        },
        {
            "doc": "The parents of this participant has a consanguineous relationship",
            "name": "consanguineousParents",
            "type": "TernaryOption"
        },
        {
            "doc": "Offspring from a consanguineous population",
            "name": "consanguineousPopulation",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Affection Status",
            "name": "affectionStatus",
            "type": {
                "doc": "Affection Status",
                "name": "AffectionStatus",
                "symbols": [
                    "unaffected",
                    "affected",
                    "unknown"
                ],
                "type": "enum"
            }
        },
        {
            "doc": "Clinical Data (disorders). If the family member is unaffected as per affectionStatus then this list is empty",
            "name": "disorderList",
            "type": {
                "items": {
                    "doc": "This is quite GEL specific. This is the way is stored in ModelCatalogue and PanelApp.\nCurrently all specific disease titles are assigned to a disease subgroup so really only specificDisease needs to be\ncompleted but we add the others for generality",
                    "fields": [
                        {
                            "doc": "This is Level2 Title for this disorder",
                            "name": "diseaseGroup",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "This is Level3 Title for this disorder",
                            "name": "diseaseSubGroup",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "This is Level4 Title for this disorder",
                            "name": "specificDisease",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Age of onset in months",
                            "name": "ageOfOnset",
                            "type": [
                                "null",
                                "string"
                            ]
                        }
                    ],
                    "name": "Disorder",
                    "type": "record"
                },
                "type": "array"
            }
        },
        {
            "doc": "Clinical Data (HPO terms)",
            "name": "hpoTermList",
            "type": {
                "items": {
                    "doc": "This defines an HPO term and its modifiers (possibly multiple)\nIf HPO term presence is unknown we don't have a entry on the list",
                    "fields": [
                        {
                            "doc": "Identifier of the HPO term",
                            "name": "term",
                            "type": "string"
                        },
                        {
                            "doc": "This is whether the term is present in the participant (default is null=unknown) true=term is present in participant,\n    false=term is not present",
                            "name": "termPresence",
                            "type": [
                                "null",
                                "boolean"
                            ]
                        },
                        {
                            "doc": "Modifier associated with the HPO term",
                            "name": "modifiers",
                            "type": [
                                "null",
                                {
                                    "type": "map",
                                    "values": "string"
                                }
                            ]
                        },
                        {
                            "doc": "Age of onset in months",
                            "name": "ageOfOnset",
                            "type": [
                                "null",
                                "string"
                            ]
                        }
                    ],
                    "name": "HpoTerm",
                    "type": "record"
                },
                "type": "array"
            }
        },
        {
            "doc": "Participant's ancestries, defined as Mother's/Father's Ethnic Origin and Chi-square test for goodness of fit of this sample to 1000 Genomes Phase 3 populations",
            "name": "ancestries",
            "type": {
                "fields": [
                    {
                        "doc": "Mother's Ethnic Origin",
                        "name": "mothersEthnicOrigin",
                        "type": [
                            "null",
                            {
                                "doc": "This is the list of ethnics in ONS16\n\n* `D`:  Mixed: White and Black Caribbean\n* `E`:  Mixed: White and Black African\n* `F`:  Mixed: White and Asian\n* `G`:  Mixed: Any other mixed background\n* `A`:  White: British\n* `B`:  White: Irish\n* `C`:  White: Any other White background\n* `L`:  Asian or Asian British: Any other Asian background\n* `M`:  Black or Black British: Caribbean\n* `N`:  Black or Black British: African\n* `H`:  Asian or Asian British: Indian\n* `J`:  Asian or Asian British: Pakistani\n* `K`:  Asian or Asian British: Bangladeshi\n* `P`:  Black or Black British: Any other Black background\n* `S`:  Other Ethnic Groups: Any other ethnic group\n* `R`:  Other Ethnic Groups: Chinese\n* `Z`:  Not stated",
                                "name": "EthnicCategory",
                                "symbols": [
                                    "D",
                                    "E",
                                    "F",
                                    "G",
                                    "A",
                                    "B",
                                    "C",
                                    "L",
                                    "M",
                                    "N",
                                    "H",
                                    "J",
                                    "K",
                                    "P",
                                    "S",
                                    "R",
                                    "Z"
                                ],
                                "type": "enum"
                            }
                        ]
                    },
                    {
                        "doc": "Mother's Ethnic Origin Description",
                        "name": "mothersOtherRelevantAncestry",
                        "type": [
                            "null",
                            "string"
                        ]
                    },
                    {
                        "doc": "Father's Ethnic Origin",
                        "name": "fathersEthnicOrigin",
                        "type": [
                            "null",
                            "EthnicCategory"
                        ]
                    },
                    {
                        "doc": "Father's Ethnic Origin Description",
                        "name": "fathersOtherRelevantAncestry",
                        "type": [
                            "null",
                            "string"
                        ]
                    },
                    {
                        "doc": "Chi-square test for goodness of fit of this sample to 1000 Genomes Phase 3 populations",
                        "name": "chiSquare1KGenomesPhase3Pop",
                        "type": [
                            "null",
                            {
                                "items": {
                                    "doc": "Chi-square test for goodness of fit of this sample to 1000 Genomes Phase 3 populations",
                                    "fields": [
                                        {
                                            "doc": "1K Super Population",
                                            "name": "kGSuperPopCategory",
                                            "type": {
                                                "doc": "1K Population",
                                                "name": "KGSuperPopCategory",
                                                "symbols": [
                                                    "AFR",
                                                    "AMR",
                                                    "EAS",
                                                    "EUR",
                                                    "SAS"
                                                ],
                                                "type": "enum"
                                            }
                                        },
                                        {
                                            "doc": "1K Population",
                                            "name": "kGPopCategory",
                                            "type": [
                                                "null",
                                                {
                                                    "doc": "1K Super Population",
                                                    "name": "KGPopCategory",
                                                    "symbols": [
                                                        "ACB",
                                                        "ASW",
                                                        "BEB",
                                                        "CDX",
                                                        "CEU",
                                                        "CHB",
                                                        "CHS",
                                                        "CLM",
                                                        "ESN",
                                                        "FIN",
                                                        "GBR",
                                                        "GIH",
                                                        "GWD",
                                                        "IBS",
                                                        "ITU",
                                                        "JPT",
                                                        "KHV",
                                                        "LWK",
                                                        "MSL",
                                                        "MXL",
                                                        "PEL",
                                                        "PJL",
                                                        "PUR",
                                                        "STU",
                                                        "TSI",
                                                        "YRI"
                                                    ],
                                                    "type": "enum"
                                                }
                                            ]
                                        },
                                        {
                                            "doc": "Chi-square test for goodness of fit of this sample to this 1000 Genomes Phase 3 population",
                                            "name": "chiSquare",
                                            "type": "float"
                                        }
                                    ],
                                    "name": "ChiSquare1KGenomesPhase3Pop",
                                    "type": "record"
                                },
                                "type": "array"
                            }
                        ]
                    }
                ],
                "name": "Ancestries",
                "type": "record"
            }
        },
        {
            "default": "v4.2",
            "doc": "Version of the Data Catalogue against which the information is being provided\n    The data catalogue version should internally keep track of the version for the HPO ontology",
            "name": "dataModelCatalogueVersion",
            "type": "string"
        },
        {
            "doc": "What has this participant consented to?\n    A participant that has been consented to the programme should also have sequence data associated with them; however\n    this needs to be programmatically checked",
            "name": "consentStatus",
            "type": {
                "doc": "Consent Status",
                "fields": [
                    {
                        "default": false,
                        "doc": "Is this individual consented to the programme?\n    It could simple be a family member that is not consented but for whom affection status is known",
                        "name": "programmeConsent",
                        "type": "boolean"
                    },
                    {
                        "default": false,
                        "doc": "Consent for feedback of primary findings?",
                        "name": "primaryFindingConsent",
                        "type": "boolean"
                    },
                    {
                        "default": false,
                        "doc": "Consent for secondary finding lookup",
                        "name": "secondaryFindingConsent",
                        "type": "boolean"
                    },
                    {
                        "default": false,
                        "doc": "Consent for carrier status check?",
                        "name": "carrierStatusConsent",
                        "type": "boolean"
                    }
                ],
                "name": "ConsentStatus",
                "type": "record"
            }
        },
        {
            "doc": "This is an array containing all the samples that belong to this individual, e.g [\"LP00002255_GA4\"]",
            "name": "samples",
            "type": [
                "null",
                {
                    "items": "string",
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Inbreeding Coefficient Estimation",
            "name": "inbreedingCoefficient",
            "type": [
                "null",
                {
                    "doc": "Inbreeding coefficient",
                    "fields": [
                        {
                            "doc": "This is the sample id against which the coefficient was estimated",
                            "name": "sampleId",
                            "type": "string"
                        },
                        {
                            "doc": "Name of program used to calculate the coefficient",
                            "name": "program",
                            "type": "string"
                        },
                        {
                            "doc": "Version of the programme",
                            "name": "version",
                            "type": "string"
                        },
                        {
                            "doc": "Where various methods for estimation exist, which method was used.",
                            "name": "estimationMethod",
                            "type": "string"
                        },
                        {
                            "doc": "Inbreeding coefficient ideally a real number in [0,1]",
                            "name": "coefficient",
                            "type": "double"
                        },
                        {
                            "doc": "Standard error of the Inbreeding coefficient",
                            "name": "standardError",
                            "type": [
                                "null",
                                "double"
                            ]
                        }
                    ],
                    "name": "InbreedingCoefficient",
                    "type": "record"
                }
            ]
        },
        {
            "doc": "We could add a map here to store additional information for example URIs to images, ECGs, etc\n    Null by default",
            "name": "additionalInformation",
            "type": [
                "null",
                {
                    "type": "map",
                    "values": "string"
                }
            ]
        }
    ],
    "name": "RDParticipant",
    "namespace": "Gel_BioInf_Models",
    "type": "record"
}