{
    "doc": "This defines a Cancer Participant (demographics and sample information)",
    "fields": [
        {
            "doc": "Model version number",
            "name": "versionControl",
            "type": {
                "fields": [
                    {
                        "default": "3.0.0",
                        "doc": "This is the version for the entire set of data models as referred to the Git release tag",
                        "name": "GitVersionControl",
                        "type": "string"
                    }
                ],
                "name": "VersionControl",
                "type": "record"
            }
        },
        {
            "doc": "This contains all the demographic information",
            "name": "cancerDemographics",
            "type": {
                "doc": "This defines a Cancer Demographics",
                "fields": [
                    {
                        "doc": "Individual Id (this is the individual id (gel ID))",
                        "name": "gelId",
                        "type": "string"
                    },
                    {
                        "doc": "Center",
                        "name": "center",
                        "type": "string"
                    },
                    {
                        "doc": "Center patient ID",
                        "name": "centerPatientId",
                        "type": [
                            "null",
                            "string"
                        ]
                    },
                    {
                        "doc": "Labkey ID - this field together with the study should be a unique id to find the labkey data",
                        "name": "labkeyParticipantId",
                        "type": [
                            "null",
                            "string"
                        ]
                    },
                    {
                        "doc": "This should be an enumeration when it is well defined\n    blood, breast, prostate, colorectal, cll, aml, renal, ovarian, skin, lymphNode, bone, saliva //for individual - there could be more than I have listed here, in fact there definitely will.",
                        "name": "primaryDiagnosis",
                        "type": [
                            "null",
                            "string"
                        ]
                    },
                    {
                        "doc": "Sex",
                        "name": "sex",
                        "type": [
                            "null",
                            {
                                "name": "Sex",
                                "symbols": [
                                    "M",
                                    "F"
                                ],
                                "type": "enum"
                            }
                        ]
                    },
                    {
                        "doc": "What has this participant consented to?\n    A participant that has been consented to the programme should also have sequence data associated with them; however\n    this needs to be programmatically checked",
                        "name": "consentStatus",
                        "type": {
                            "doc": "Consent Status",
                            "fields": [
                                {
                                    "default": false,
                                    "doc": "Is this individual consented to the programme?\n    It could simple be a family member that is not consented but for whom affection status is known",
                                    "name": "programmeConsent",
                                    "type": "boolean"
                                },
                                {
                                    "default": false,
                                    "doc": "Consent for feedback of primary findings?",
                                    "name": "primaryFindingConsent",
                                    "type": "boolean"
                                },
                                {
                                    "default": false,
                                    "doc": "Consent for secondary finding lookup",
                                    "name": "secondaryFindingConsent",
                                    "type": "boolean"
                                },
                                {
                                    "default": false,
                                    "doc": "Consent for carrier status check?",
                                    "name": "carrierStatusConsent",
                                    "type": "boolean"
                                }
                            ],
                            "name": "ConsentStatus",
                            "type": "record"
                        }
                    },
                    {
                        "doc": "We could add a map here to store additional information for example URIs to images, ECGs, etc",
                        "name": "additionalInformation",
                        "type": [
                            "null",
                            {
                                "type": "map",
                                "values": "string"
                            }
                        ]
                    },
                    {
                        "doc": "Sample Id (e.g, LP00012645_5GH))",
                        "name": "sampleId",
                        "type": [
                            "null",
                            {
                                "items": "string",
                                "type": "array"
                            }
                        ]
                    },
                    {
                        "doc": "assigned ICD10 code",
                        "name": "assignedICD10",
                        "type": [
                            "null",
                            "string"
                        ]
                    }
                ],
                "name": "CancerDemographics",
                "type": "record"
            }
        },
        {
            "doc": "List of samples (normal and tumor sample)",
            "name": "cancerSamples",
            "type": {
                "items": {
                    "fields": [
                        {
                            "doc": "Sample Id (e.g, LP00012645_5GH))",
                            "name": "sampleId",
                            "type": "string"
                        },
                        {
                            "doc": "Lab Id (this is from the bio-repository e.g D14.25586 or another format based on which center)",
                            "name": "labId",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "gelPhase (eg cruk, brc, cll, iip, main)",
                            "name": "gelPhase",
                            "type": [
                                "null",
                                {
                                    "name": "GelPhase",
                                    "symbols": [
                                        "CRUK",
                                        "OXFORD",
                                        "CLL",
                                        "IIP",
                                        "MAIN",
                                        "EXPT"
                                    ],
                                    "type": "enum"
                                }
                            ]
                        },
                        {
                            "doc": "Sample Type",
                            "name": "sampleType",
                            "type": {
                                "name": "SampleType",
                                "symbols": [
                                    "germline",
                                    "tumor"
                                ],
                                "type": "enum"
                            }
                        },
                        {
                            "doc": "Sample Diagnosis (we need this because primary diagnosis or patient might be CLL, but this sample might be a different tumor?)",
                            "name": "sampleDiagnosis",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Tumor Type\n    blood, breast, prostate, colorectal, cll, aml, renal, ovarian, skin, lymphNode, bone, blood, saliva",
                            "name": "tumorType",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Tumor Subtype",
                            "name": "tumorSubType",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Preservation method",
                            "name": "preservationMethod",
                            "type": [
                                "null",
                                {
                                    "name": "PreservationMethod",
                                    "symbols": [
                                        "FFPE",
                                        "FF",
                                        "UNKNOWN",
                                        "BLOOD",
                                        "GL",
                                        "SALIVA",
                                        "LEUK"
                                    ],
                                    "type": "enum"
                                }
                            ]
                        },
                        {
                            "doc": "Tumor Phase",
                            "name": "phase",
                            "type": [
                                "null",
                                {
                                    "name": "Phase",
                                    "symbols": [
                                        "PRIMARY",
                                        "METASTATIC",
                                        "RECURRENCE"
                                    ],
                                    "type": "enum"
                                }
                            ]
                        },
                        {
                            "doc": "Method",
                            "name": "method",
                            "type": [
                                "null",
                                {
                                    "name": "Method",
                                    "symbols": [
                                        "RESECTION",
                                        "BIOPSY",
                                        "BLOOD"
                                    ],
                                    "type": "enum"
                                }
                            ]
                        },
                        {
                            "doc": "Sample tumorPurity",
                            "name": "tumorPurity",
                            "type": [
                                "null",
                                "double"
                            ]
                        },
                        {
                            "doc": "Sample TumorContent will be a string",
                            "name": "tumorContent",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Grade of tumour - a lot of records gave this",
                            "name": "grade",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "TMN version",
                            "name": "tnm_stage_version",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "TMN stage grouping",
                            "name": "tmn_stage_grouping",
                            "type": [
                                "null",
                                "string"
                            ]
                        }
                    ],
                    "name": "CancerSample",
                    "type": "record"
                },
                "type": "array"
            }
        },
        {
            "doc": "List of Matched Samples (this is, all the normal - tumor pairs should be analyzed)",
            "name": "matchedSamples",
            "type": {
                "items": {
                    "doc": "This define a pair of germline and tumor, this pair should/must be analyzed together",
                    "fields": [
                        {
                            "doc": "Sample Id (e.g, LP00012645_5GH)) for the germline",
                            "name": "germlineSampleId",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Sample Id (e.g, LP00012643_7JS)) for the tumor",
                            "name": "tumorSampleId",
                            "type": [
                                "null",
                                "string"
                            ]
                        }
                    ],
                    "name": "MatchedSamples",
                    "type": "record"
                },
                "type": "array"
            }
        }
    ],
    "name": "CancerParticipant",
    "namespace": "org.gel.models.report.avro",
    "type": "record"
}