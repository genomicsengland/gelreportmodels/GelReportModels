{
    "doc": "This request maps to the body of `POST /reads/search` as JSON.\n\nIf a reference is specified, all queried `ReadGroup`s must be aligned\nto `ReferenceSet`s containing that same `Reference`. If no reference is\nspecified, all `ReadGroup`s must be aligned to the same `ReferenceSet`.",
    "fields": [
        {
            "doc": "The ReadGroups to search. At least one readGroupId must be specified.",
            "name": "readGroupIds",
            "type": {
                "items": "string",
                "type": "array"
            }
        },
        {
            "default": null,
            "doc": "The reference to query. Leaving blank returns results from all\n  references, including unmapped reads - this could be very large.",
            "name": "referenceId",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "default": null,
            "doc": "The start position (0-based) of this query.\n  If a reference is specified, this defaults to 0.\n  Genomic positions are non-negative integers less than reference length.\n  Requests spanning the join of circular genomes are represented as\n  two requests one on each side of the join (position 0).",
            "name": "start",
            "type": [
                "null",
                "long"
            ]
        },
        {
            "default": null,
            "doc": "The end position (0-based, exclusive) of this query.\n  If a reference is specified, this defaults to the\n  reference's length.",
            "name": "end",
            "type": [
                "null",
                "long"
            ]
        },
        {
            "default": null,
            "doc": "Specifies the maximum number of results to return in a single page.\n  If unspecified, a system default will be used.",
            "name": "pageSize",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "default": null,
            "doc": "The continuation token, which is used to page through large result sets.\n  To get the next page of results, set this parameter to the value of\n  `nextPageToken` from the previous response.",
            "name": "pageToken",
            "type": [
                "null",
                "string"
            ]
        }
    ],
    "name": "SearchReadsRequest",
    "namespace": "org.ga4gh.methods",
    "type": "record"
}