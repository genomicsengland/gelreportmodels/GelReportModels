{
    "doc": "A `Variant` represents a change in DNA sequence relative to some reference.\nFor example, a variant could represent a SNP or an insertion.\nVariants belong to a `VariantSet`.\nThis is equivalent to a row in VCF.",
    "fields": [
        {
            "doc": "The variant ID.",
            "name": "id",
            "type": "string"
        },
        {
            "doc": "The ID of the `VariantSet` this variant belongs to. This transitively defines\n  the `ReferenceSet` against which the `Variant` is to be interpreted.",
            "name": "variantSetId",
            "type": "string"
        },
        {
            "default": [],
            "doc": "Names for the variant, for example a RefSNP ID.",
            "name": "names",
            "type": {
                "items": "string",
                "type": "array"
            }
        },
        {
            "default": null,
            "doc": "The date this variant was created in milliseconds from the epoch.",
            "name": "created",
            "type": [
                "null",
                "long"
            ]
        },
        {
            "default": null,
            "doc": "The time at which this variant was last updated in\n  milliseconds from the epoch.",
            "name": "updated",
            "type": [
                "null",
                "long"
            ]
        },
        {
            "doc": "The reference on which this variant occurs.\n  (e.g. `chr20` or `X`)",
            "name": "referenceName",
            "type": "string"
        },
        {
            "doc": "The start position at which this variant occurs (0-based).\n  This corresponds to the first base of the string of reference bases.\n  Genomic positions are non-negative integers less than reference length.\n  Variants spanning the join of circular genomes are represented as\n  two variants one on each side of the join (position 0).",
            "name": "start",
            "type": "long"
        },
        {
            "doc": "The end position (exclusive), resulting in [start, end) closed-open interval.\n  This is typically calculated by `start + referenceBases.length`.",
            "name": "end",
            "type": "long"
        },
        {
            "doc": "The reference bases for this variant. They start at the given start position.",
            "name": "referenceBases",
            "type": "string"
        },
        {
            "default": [],
            "doc": "The bases that appear instead of the reference bases. Multiple alternate\n  alleles are possible.",
            "name": "alternateBases",
            "type": {
                "items": "string",
                "type": "array"
            }
        },
        {
            "default": {},
            "doc": "A map of additional variant information.",
            "name": "info",
            "type": {
                "type": "map",
                "values": {
                    "items": "string",
                    "type": "array"
                }
            }
        },
        {
            "default": [],
            "doc": "The variant calls for this particular variant. Each one represents the\n  determination of genotype with respect to this variant. `Call`s in this array\n  are implicitly associated with this `Variant`.",
            "name": "calls",
            "type": {
                "items": {
                    "doc": "A `Call` represents the determination of genotype with respect to a\nparticular `Variant`.\n\nIt may include associated information such as quality\nand phasing. For example, a call might assign a probability of 0.32 to\nthe occurrence of a SNP named rs1234 in a call set with the name NA12345.",
                    "fields": [
                        {
                            "default": null,
                            "doc": "The name of the call set this variant call belongs to.\n  If this field is not present, the ordering of the call sets from a\n  `SearchCallSetsRequest` over this `VariantSet` is guaranteed to match\n  the ordering of the calls on this `Variant`.\n  The number of results will also be the same.",
                            "name": "callSetName",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "default": null,
                            "doc": "The ID of the call set this variant call belongs to.\n\n  If this field is not present, the ordering of the call sets from a\n  `SearchCallSetsRequest` over this `VariantSet` is guaranteed to match\n  the ordering of the calls on this `Variant`.\n  The number of results will also be the same.",
                            "name": "callSetId",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "default": [],
                            "doc": "The genotype of this variant call.\n\n  A 0 value represents the reference allele of the associated `Variant`. Any\n  other value is a 1-based index into the alternate alleles of the associated\n  `Variant`.\n\n  If a variant had a referenceBases field of \"T\", an alternateBases\n  value of [\"A\", \"C\"], and the genotype was [2, 1], that would mean the call\n  represented the heterozygous value \"CA\" for this variant. If the genotype\n  was instead [0, 1] the represented value would be \"TA\". Ordering of the\n  genotype values is important if the phaseset field is present.",
                            "name": "genotype",
                            "type": {
                                "items": "int",
                                "type": "array"
                            }
                        },
                        {
                            "default": null,
                            "doc": "If this field is not null, this variant call's genotype ordering implies\n  the phase of the bases and is consistent with any other variant calls on\n  the same contig which have the same phaseset string.",
                            "name": "phaseset",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "default": [],
                            "doc": "The genotype likelihoods for this variant call. Each array entry\n  represents how likely a specific genotype is for this call as\n  log10(P(data | genotype)), analogous to the GL tag in the VCF spec. The\n  value ordering is defined by the GL tag in the VCF spec.",
                            "name": "genotypeLikelihood",
                            "type": {
                                "items": "double",
                                "type": "array"
                            }
                        },
                        {
                            "default": {},
                            "doc": "A map of additional variant call information.",
                            "name": "info",
                            "type": {
                                "type": "map",
                                "values": {
                                    "items": "string",
                                    "type": "array"
                                }
                            }
                        }
                    ],
                    "name": "Call",
                    "type": "record"
                },
                "type": "array"
            }
        }
    ],
    "name": "Variant",
    "namespace": "org.ga4gh.models",
    "type": "record"
}