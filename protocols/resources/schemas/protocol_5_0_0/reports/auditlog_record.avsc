{
    "fields": [
        {
            "doc": "This is the interpretation Request Id, first number in XXX-123-1",
            "name": "interpretationRequestId",
            "type": "string"
        },
        {
            "doc": "This is the version of the interpretation Request Id, second number in XXX-123-2",
            "name": "interpretationRequestVersion",
            "type": "string"
        },
        {
            "name": "code",
            "type": {
                "doc": "This code define the change type, it can define a general change in the case as CLOSED or can define\na change in one or more variants:\n\n* `C0`: **Case Closed successfully**: Clinical Report was generated with **one or more Candidate Variants**.\n* `C1`: **Case Closed unsuccessfully**: Clinical Report couldn't be generated because **no Candidate Variants were found**.\n* `C2`: **Case Blocked**: Errors were found in this cases and was sent to quarantine for further investigation\n* `C3`: **Case Shared**: This cases was shared with other group of users.\n* `C4`: **Supporting evidence change**: One or More supporting evidence were modified to the cases __(See ClinicalReport)__.\n* `C5`: **Variant added**: One or more variant were selected as Candidate Variants.\n* `C6`: **Variant removed**: One or more variant were removed as Candidate Variants.\n* `C7`: **Variant modified**: One or more Candidate Variants were modified __(Any change or comment over this variants should be capture)__.",
                "name": "Code",
                "symbols": [
                    "C0",
                    "C1",
                    "C2",
                    "C3",
                    "C4",
                    "C5",
                    "C6",
                    "C7"
                ],
                "type": "enum"
            }
        },
        {
            "name": "caseShared",
            "type": [
                "null",
                {
                    "fields": [
                        {
                            "name": "previousGroups",
                            "type": {
                                "items": "string",
                                "type": "array"
                            }
                        },
                        {
                            "name": "modifiedGroups",
                            "type": {
                                "items": "string",
                                "type": "array"
                            }
                        }
                    ],
                    "name": "CaseShared",
                    "type": "record"
                }
            ]
        },
        {
            "name": "supportingEvidences",
            "type": [
                "null",
                {
                    "fields": [
                        {
                            "name": "previousSupportingEvidences",
                            "type": {
                                "items": "string",
                                "type": "array"
                            }
                        },
                        {
                            "name": "modifiedSupportingEvidences",
                            "type": {
                                "items": "string",
                                "type": "array"
                            }
                        }
                    ],
                    "name": "SupportingEvidences",
                    "type": "record"
                }
            ]
        },
        {
            "name": "modifiedVariants",
            "type": [
                "null",
                {
                    "items": {
                        "fields": [
                            {
                                "name": "previousVariant",
                                "type": {
                                    "fields": [
                                        {
                                            "doc": "chromosome, named as: 1-22,X,Y,MT or other contig names as defined in the BAM header",
                                            "name": "chromosome",
                                            "type": "string"
                                        },
                                        {
                                            "doc": "variant ID in dbSNP",
                                            "name": "dbSnpId",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "doc": "position: Position 1-based",
                                            "name": "position",
                                            "type": "int"
                                        },
                                        {
                                            "doc": "reference: Reference Allele sequence, the same provided in vcf",
                                            "name": "reference",
                                            "type": "string"
                                        },
                                        {
                                            "doc": "alternate: Alternate Allele sequence, the same provided in vcf",
                                            "name": "alternate",
                                            "type": "string"
                                        },
                                        {
                                            "doc": "array of genotypes for the family",
                                            "name": "calledGenotypes",
                                            "type": {
                                                "items": {
                                                    "doc": "This is intended to hold the genotypes for the family members\n    In principle it is a phased zygosity as in VCF spec and called by the analysis provider if further phasing is conducted",
                                                    "fields": [
                                                        {
                                                            "doc": "Participant id of the family member:",
                                                            "name": "gelId",
                                                            "type": "string"
                                                        },
                                                        {
                                                            "doc": "LP Number of the family member:",
                                                            "name": "sampleId",
                                                            "type": "string"
                                                        },
                                                        {
                                                            "doc": "Zygosity",
                                                            "name": "genotype",
                                                            "type": {
                                                                "doc": "It is a representation of the zygosity\n\n* `reference_homozygous`: 0/0, 0|0\n* `heterozygous`: 0/1, 1/0, 1|0, 0|1\n* `alternate_homozygous`: 1/1, 1|1\n* `missing`: ./., .|.\n* `half_missing_reference`: ./0, 0/., 0|., .|0\n* `half_missing_alternate`: ./1, 1/., 1|., .|1\n* `alternate_hemizigous`: 1\n* `reference_hemizigous`: 0\n* `unk`: Anything unexpected",
                                                                "name": "Zygosity",
                                                                "symbols": [
                                                                    "reference_homozygous",
                                                                    "heterozygous",
                                                                    "alternate_homozygous",
                                                                    "missing",
                                                                    "half_missing_reference",
                                                                    "half_missing_alternate",
                                                                    "alternate_hemizigous",
                                                                    "reference_hemizigous",
                                                                    "unk"
                                                                ],
                                                                "type": "enum"
                                                            }
                                                        },
                                                        {
                                                            "doc": "Phase set of variants when variants are phased",
                                                            "name": "phaseSet",
                                                            "type": [
                                                                "null",
                                                                "int"
                                                            ]
                                                        },
                                                        {
                                                            "doc": "Depth for Reference Allele",
                                                            "name": "depthReference",
                                                            "type": [
                                                                "null",
                                                                "int"
                                                            ]
                                                        },
                                                        {
                                                            "doc": "Depth for Alternate Allele",
                                                            "name": "depthAlternate",
                                                            "type": [
                                                                "null",
                                                                "int"
                                                            ]
                                                        },
                                                        {
                                                            "doc": "Copy number genotype for imprecise events",
                                                            "name": "copyNumber",
                                                            "type": [
                                                                "null",
                                                                "int"
                                                            ]
                                                        }
                                                    ],
                                                    "name": "CalledGenotype",
                                                    "type": "record"
                                                },
                                                "type": "array"
                                            }
                                        },
                                        {
                                            "doc": "This is the scores across multiple modes of inheritance, for each model of inheritance analyzed,\n        the variants can have only one Report event.",
                                            "name": "reportEvents",
                                            "type": {
                                                "items": {
                                                    "fields": [
                                                        {
                                                            "doc": "Unique identifier for each report event, this has to be unique across the whole report, and it will be used by GEL\n        to validate the report",
                                                            "name": "reportEventId",
                                                            "type": "string"
                                                        },
                                                        {
                                                            "doc": "This is the phenotype (usually the HPO term or the disorder name) considered to report this variant",
                                                            "name": "phenotype",
                                                            "type": "string"
                                                        },
                                                        {
                                                            "doc": "Gene Panel used from panelApp",
                                                            "name": "panelName",
                                                            "type": [
                                                                "null",
                                                                "string"
                                                            ]
                                                        },
                                                        {
                                                            "doc": "Gene Panel Version",
                                                            "name": "panelVersion",
                                                            "type": [
                                                                "null",
                                                                "string"
                                                            ]
                                                        },
                                                        {
                                                            "doc": "Mode of inheritance used to analyze the family",
                                                            "name": "modeOfInheritance",
                                                            "type": {
                                                                "doc": "An enumeration for the different mode of inheritances:\n\n* `monoallelic_not_imprinted`: MONOALLELIC, autosomal or pseudoautosomal, not imprinted\n* `monoallelic_maternally_imprinted`: MONOALLELIC, autosomal or pseudoautosomal, maternally imprinted (paternal allele expressed)\n* `monoallelic_paternally_imprinted`: MONOALLELIC, autosomal or pseudoautosomal, paternally imprinted (maternal allele expressed)\n* `monoallelic`: MONOALLELIC, autosomal or pseudoautosomal, imprinted status unknown\n* `biallelic`: BIALLELIC, autosomal or pseudoautosomal\n* `monoallelic_and_biallelic`: BOTH monoallelic and biallelic, autosomal or pseudoautosomal\n* `monoallelic_and_more_severe_biallelic`: BOTH monoallelic and biallelic, autosomal or pseudoautosomal (but BIALLELIC mutations cause a more SEVERE disease form), autosomal or pseudoautosomal\n* `xlinked_biallelic`: X-LINKED: hemizygous mutation in males, biallelic mutations in females\n* `xlinked_monoallelic`: X linked: hemizygous mutation in males, monoallelic mutations in females may cause disease (may be less severe, later onset than males)\n* `mitochondrial`: MITOCHONDRIAL\n* `unknown`: Unknown",
                                                                "name": "ReportedModeOfInheritance",
                                                                "symbols": [
                                                                    "monoallelic",
                                                                    "monoallelic_not_imprinted",
                                                                    "monoallelic_maternally_imprinted",
                                                                    "monoallelic_paternally_imprinted",
                                                                    "biallelic",
                                                                    "monoallelic_and_biallelic",
                                                                    "monoallelic_and_more_severe_biallelic",
                                                                    "xlinked_biallelic",
                                                                    "xlinked_monoallelic",
                                                                    "mitochondrial",
                                                                    "unknown"
                                                                ],
                                                                "type": "enum"
                                                            }
                                                        },
                                                        {
                                                            "doc": "This is the genomicsFeature of interest for this reported variant, please note that one variant can overlap more that one gene/transcript\n        If more than one gene/transcript is considered interesting for this particular variant, should be reported in two different ReportEvents",
                                                            "name": "genomicFeature",
                                                            "type": {
                                                                "fields": [
                                                                    {
                                                                        "doc": "Feature Type",
                                                                        "name": "featureType",
                                                                        "type": {
                                                                            "name": "FeatureTypes",
                                                                            "symbols": [
                                                                                "RegulatoryRegion",
                                                                                "Gene",
                                                                                "Transcript"
                                                                            ],
                                                                            "type": "enum"
                                                                        }
                                                                    },
                                                                    {
                                                                        "doc": "Transcript used, this should be a feature ID from Ensembl, (i.e, ENST00000544455)",
                                                                        "name": "ensemblId",
                                                                        "type": "string"
                                                                    },
                                                                    {
                                                                        "doc": "This field is optional, BUT it should be filled if possible",
                                                                        "name": "hgnc",
                                                                        "type": [
                                                                            "null",
                                                                            "string"
                                                                        ]
                                                                    },
                                                                    {
                                                                        "doc": "Others IDs",
                                                                        "name": "otherIds",
                                                                        "type": [
                                                                            "null",
                                                                            {
                                                                                "type": "map",
                                                                                "values": "string"
                                                                            }
                                                                        ]
                                                                    }
                                                                ],
                                                                "name": "GenomicFeature",
                                                                "type": "record"
                                                            }
                                                        },
                                                        {
                                                            "doc": "This is the penetrance assumed for scoring or classifying this variant",
                                                            "name": "penetrance",
                                                            "type": {
                                                                "doc": "Penetrance assumed in the analysis",
                                                                "name": "Penetrance",
                                                                "namespace": "org.gel.models.participant.avro",
                                                                "symbols": [
                                                                    "complete",
                                                                    "incomplete"
                                                                ],
                                                                "type": "enum"
                                                            }
                                                        },
                                                        {
                                                            "doc": "This is the score provided by the company to reflect a variant's likelihood of explaining the phenotype using\n        a specific mode of Inheritance",
                                                            "name": "score",
                                                            "type": "float"
                                                        },
                                                        {
                                                            "doc": "Other scores that the interpretation provider may add (for example phenotypically informed or family informed scores)",
                                                            "name": "vendorSpecificScores",
                                                            "type": [
                                                                "null",
                                                                {
                                                                    "type": "map",
                                                                    "values": "float"
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            "doc": "Classification of the pathogenicity of this variant with respect to the phenotype",
                                                            "name": "variantClassification",
                                                            "type": [
                                                                "null",
                                                                {
                                                                    "doc": "This is the classification of the variant according to standard practice guidelines (e.g. ACMG)",
                                                                    "name": "VariantClassification",
                                                                    "symbols": [
                                                                        "pathogenic_variant",
                                                                        "likely_pathogenic_variant",
                                                                        "variant_of_unknown_clinical_significance",
                                                                        "likely_benign_variant",
                                                                        "benign_variant",
                                                                        "not_assessed"
                                                                    ],
                                                                    "type": "enum"
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            "doc": "This variant using this mode of inheritance can fully explain the phenotype? true or false",
                                                            "name": "fullyExplainsPhenotype",
                                                            "type": [
                                                                "null",
                                                                "boolean"
                                                            ]
                                                        },
                                                        {
                                                            "doc": "This value groups variants that together could explain the phenotype according to the\n        mode of inheritance used. All the variants sharing the same value will be considered in the same group.\n        This value is an integer unique in the whole analysis.",
                                                            "name": "groupOfVariants",
                                                            "type": [
                                                                "null",
                                                                "int"
                                                            ]
                                                        },
                                                        {
                                                            "doc": "This is the description of why this variant would be reported, for example that it affects the protein in this way\n        and that this gene has been implicated in this disorder in these publications. Publications should be provided as PMIDs\n        using the format [PMID:8075643]. Other sources can be used in the same manner, e.g. [OMIM:163500]. Brackets need to be included.",
                                                            "name": "eventJustification",
                                                            "type": [
                                                                "null",
                                                                "string"
                                                            ]
                                                        },
                                                        {
                                                            "doc": "Tier is a property of the model of inheritance and therefore is subject to change depending on the inheritance assumptions\n        This should be filled with the information provided by GEL",
                                                            "name": "tier",
                                                            "type": [
                                                                "null",
                                                                {
                                                                    "doc": "Possible tiers as defined by Genomics England",
                                                                    "name": "Tier",
                                                                    "symbols": [
                                                                        "NONE",
                                                                        "TIER1",
                                                                        "TIER2",
                                                                        "TIER3"
                                                                    ],
                                                                    "type": "enum"
                                                                }
                                                            ]
                                                        }
                                                    ],
                                                    "name": "ReportEvent",
                                                    "type": "record"
                                                },
                                                "type": "array"
                                            }
                                        },
                                        {
                                            "doc": "For example a quote from a paper",
                                            "name": "additionalTextualVariantAnnotations",
                                            "type": [
                                                "null",
                                                {
                                                    "type": "map",
                                                    "values": "string"
                                                }
                                            ]
                                        },
                                        {
                                            "doc": "For example HGMD ID, dbSNP ID or Pubmed Id",
                                            "name": "evidenceIds",
                                            "type": [
                                                "null",
                                                {
                                                    "type": "map",
                                                    "values": "string"
                                                }
                                            ]
                                        },
                                        {
                                            "doc": "For Example (Allele Frequency, sift, polyphen, mutationTaster, CADD. ..)",
                                            "name": "additionalNumericVariantAnnotations",
                                            "type": [
                                                "null",
                                                {
                                                    "type": "map",
                                                    "values": "float"
                                                }
                                            ]
                                        },
                                        {
                                            "doc": "Comments",
                                            "name": "comments",
                                            "type": [
                                                "null",
                                                {
                                                    "items": "string",
                                                    "type": "array"
                                                }
                                            ]
                                        }
                                    ],
                                    "name": "ReportedVariant",
                                    "type": "record"
                                }
                            },
                            {
                                "name": "modifiedVariant",
                                "type": "ReportedVariant"
                            }
                        ],
                        "name": "ModifiedVariant",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "name": "addedVariants",
            "type": [
                "null",
                {
                    "items": "ReportedVariant",
                    "type": "array"
                }
            ]
        },
        {
            "name": "removedVariants",
            "type": [
                "null",
                {
                    "items": "ReportedVariant",
                    "type": "array"
                }
            ]
        }
    ],
    "name": "AuditLog",
    "namespace": "org.gel.models.report.avro",
    "type": "record"
}