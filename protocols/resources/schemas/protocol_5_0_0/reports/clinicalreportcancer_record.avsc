{
    "fields": [
        {
            "doc": "This is the interpretation Request Id, first number in XXX-123-1",
            "name": "interpretationRequestId",
            "type": "string"
        },
        {
            "doc": "This is the version of the interpretation Request Id, second number in XXX-123-2",
            "name": "interpretationRequestVersion",
            "type": "string"
        },
        {
            "doc": "Date of this report YYYY-MM-DD",
            "name": "reportingDate",
            "type": "string"
        },
        {
            "doc": "Author of this report",
            "name": "user",
            "type": "string"
        },
        {
            "doc": "Candidate Variants - as defined in CommonInterpreted",
            "name": "candidateVariants",
            "type": [
                "null",
                {
                    "items": {
                        "fields": [
                            {
                                "doc": "chromosome, named as: 1-22,X,Y,MT(other contigs name)",
                                "name": "chromosome",
                                "type": "string"
                            },
                            {
                                "doc": "position: Position 1-based",
                                "name": "position",
                                "type": "int"
                            },
                            {
                                "doc": "reference: Reference Allele sequence, the same provided in vcf",
                                "name": "reference",
                                "type": "string"
                            },
                            {
                                "doc": "alternate: Alternate Allele sequence, the same provided in vcf",
                                "name": "alternate",
                                "type": "string"
                            },
                            {
                                "doc": "variant ID in Cosmic",
                                "name": "cosmicIds",
                                "type": [
                                    "null",
                                    {
                                        "items": "string",
                                        "type": "array"
                                    }
                                ]
                            },
                            {
                                "doc": "variant ID in ClinVar",
                                "name": "clinVarIds",
                                "type": [
                                    "null",
                                    {
                                        "items": "string",
                                        "type": "array"
                                    }
                                ]
                            },
                            {
                                "doc": "variant ID in dbSNP",
                                "name": "dbSnpId",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "cDNA change, HGVS nomenclature (e.g.)",
                                "name": "cdnaChange",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "protein change, HGVS nomenclature (e.g.)",
                                "name": "proteinChange",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "AF in germline DBs",
                                "name": "commonAf",
                                "type": [
                                    "null",
                                    "int"
                                ]
                            },
                            {
                                "doc": "Largest reference interrupted homopolymer length intersecting with the indel",
                                "name": "ihp",
                                "type": [
                                    "null",
                                    "int"
                                ]
                            },
                            {
                                "doc": "For example HGMD ID)",
                                "name": "additionalTextualVariantAnnotations",
                                "type": [
                                    "null",
                                    {
                                        "type": "map",
                                        "values": "string"
                                    }
                                ]
                            },
                            {
                                "doc": "For Example (Allele Frequency, sift, polyphen, mutationTaster, CADD. ..)",
                                "name": "additionalNumericVariantAnnotations",
                                "type": [
                                    "null",
                                    {
                                        "type": "map",
                                        "values": "float"
                                    }
                                ]
                            },
                            {
                                "doc": "Comments",
                                "name": "comments",
                                "type": [
                                    "null",
                                    {
                                        "items": "string",
                                        "type": "array"
                                    }
                                ]
                            },
                            {
                                "doc": "The report events",
                                "name": "reportEvents",
                                "type": {
                                    "items": {
                                        "fields": [
                                            {
                                                "doc": "Unique identifier for each report event, this has to be unique across the whole report, and it will be used by GEL\n        to validate the report",
                                                "name": "reportEventId",
                                                "type": "string"
                                            },
                                            {
                                                "doc": "This is the genomicsFeature of interest for this reported variant, please note that one variant can overlap more that one gene/transcript\n        If more than one gene/transcript is considered interesting for this particular variant, should be reported in two different ReportEvents",
                                                "name": "genomicFeatureCancer",
                                                "type": {
                                                    "fields": [
                                                        {
                                                            "doc": "Feature Type",
                                                            "name": "featureType",
                                                            "type": {
                                                                "doc": "The genomic feature type\n\n* regulatory_region: A region of sequence that is involved in the control of a biological process. SO:0005836\n* gene: A region (or regions) that includes all of the sequence elements necessary to encode a functional transcript. A gene may include regulatory regions, transcribed regions and/or other functional sequence regions. SO:0000704\n* transcript: An RNA synthesized on a DNA or RNA template by an RNA polymerase. SO:0000673",
                                                                "name": "FeatureTypeCancer",
                                                                "symbols": [
                                                                    "regulatory_region",
                                                                    "gene",
                                                                    "transcript"
                                                                ],
                                                                "type": "enum"
                                                            }
                                                        },
                                                        {
                                                            "doc": "Transcript used, this should be a feature ID from Ensembl, (i.e, ENST00000544455)",
                                                            "name": "ensemblId",
                                                            "type": "string"
                                                        },
                                                        {
                                                            "doc": "Refseq transcript",
                                                            "name": "refSeqTranscriptId",
                                                            "type": "string"
                                                        },
                                                        {
                                                            "doc": "Refseq protein",
                                                            "name": "refSeqProteinId",
                                                            "type": "string"
                                                        },
                                                        {
                                                            "doc": "Gene used in tier",
                                                            "name": "geneName",
                                                            "type": "string"
                                                        },
                                                        {
                                                            "doc": "Role in cancer: oncogene, TSG or both",
                                                            "name": "roleInCancer",
                                                            "type": [
                                                                "null",
                                                                {
                                                                    "doc": "The role of a given genomic feature in cancer\n\n* oncogene: A gene that is a mutated (changed) form of a gene involved in normal cell growth. Oncogenes may cause the growth of cancer cells. Mutations in genes that become oncogenes can be inherited or caused by being exposed to substances in the environment that cause cancer. http://purl.obolibrary.org/obo/NCIT_C16936\n* tumor_suppressor_gene: A type of gene that makes a protein called a tumor suppressor protein that helps control cell growth. Mutations (changes in DNA) in antioncogenes may lead to cancer. http://purl.obolibrary.org/obo/NCIT_C17362",
                                                                    "name": "RoleInCancer",
                                                                    "symbols": [
                                                                        "oncogene",
                                                                        "tumor_suppressor_gene",
                                                                        "both"
                                                                    ],
                                                                    "type": "enum"
                                                                }
                                                            ]
                                                        }
                                                    ],
                                                    "name": "GenomicFeatureCancer",
                                                    "type": "record"
                                                }
                                            },
                                            {
                                                "doc": "Sequence Ontology terms used in tier",
                                                "name": "soTerms",
                                                "type": {
                                                    "items": {
                                                        "doc": "A Sequence Ontology term identifier by its id and name (e.g.: id = SO:0001816 ; name = non synonymous)",
                                                        "fields": [
                                                            {
                                                                "doc": "The SO term identifier (e.g.: SO:0001816)",
                                                                "name": "id",
                                                                "type": "string"
                                                            },
                                                            {
                                                                "doc": "The SO term name (e.g.: non synonymous)",
                                                                "name": "name",
                                                                "type": "string"
                                                            }
                                                        ],
                                                        "name": "SoTerm",
                                                        "type": "record"
                                                    },
                                                    "type": "array"
                                                }
                                            },
                                            {
                                                "doc": "Types of actionability",
                                                "name": "actions",
                                                "type": [
                                                    "null",
                                                    {
                                                        "items": {
                                                            "fields": [
                                                                {
                                                                    "name": "actionType",
                                                                    "type": [
                                                                        "null",
                                                                        {
                                                                            "doc": "this is the type of actionability for the reported event",
                                                                            "name": "ActionType",
                                                                            "symbols": [
                                                                                "therapy",
                                                                                "therapeutic",
                                                                                "prognosis",
                                                                                "diagnosis"
                                                                            ],
                                                                            "type": "enum"
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    "doc": "array of PMIDs",
                                                                    "name": "evidences",
                                                                    "type": [
                                                                        "null",
                                                                        {
                                                                            "items": "string",
                                                                            "type": "array"
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    "doc": "drug",
                                                                    "name": "drug",
                                                                    "type": [
                                                                        "null",
                                                                        "string"
                                                                    ]
                                                                },
                                                                {
                                                                    "doc": "status i.e. clinical, pre-clinical",
                                                                    "name": "status",
                                                                    "type": [
                                                                        "null",
                                                                        {
                                                                            "doc": "this is the type of actionability for the reported event",
                                                                            "name": "ActionStatus",
                                                                            "symbols": [
                                                                                "clinical",
                                                                                "pre_clinical"
                                                                            ],
                                                                            "type": "enum"
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    "doc": "Is variant level actionable",
                                                                    "name": "variantActionable",
                                                                    "type": "boolean"
                                                                },
                                                                {
                                                                    "doc": "comments",
                                                                    "name": "comments",
                                                                    "type": [
                                                                        "null",
                                                                        {
                                                                            "items": "string",
                                                                            "type": "array"
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    "doc": "link to original source",
                                                                    "name": "url",
                                                                    "type": [
                                                                        "null",
                                                                        "string"
                                                                    ]
                                                                },
                                                                {
                                                                    "doc": "type of evidence",
                                                                    "name": "evidenceType",
                                                                    "type": [
                                                                        "null",
                                                                        "string"
                                                                    ]
                                                                },
                                                                {
                                                                    "doc": "source of the information",
                                                                    "name": "source",
                                                                    "type": "string"
                                                                }
                                                            ],
                                                            "name": "Action",
                                                            "type": "record"
                                                        },
                                                        "type": "array"
                                                    }
                                                ]
                                            },
                                            {
                                                "doc": "This value groups variants that together could explain the phenotype according to the\n        mode of inheritance used. All the variants sharing the same value will be considered in the same group.\n        This value is an integer unique in the whole analysis.",
                                                "name": "groupOfVariants",
                                                "type": [
                                                    "null",
                                                    "int"
                                                ]
                                            },
                                            {
                                                "doc": "This is the description of why this variant would be reported, for example that it affects the protein in this way\n        and that this gene has been implicated in this disorder in these publications. Publications should be provided as PMIDs\n        using the format [PMID:8075643]. Other sources can be used in the same manner, e.g. [OMIM:163500]. Brackets need to be included.",
                                                "name": "eventJustification",
                                                "type": [
                                                    "null",
                                                    "string"
                                                ]
                                            },
                                            {
                                                "doc": "Gel Tier",
                                                "name": "tier",
                                                "type": [
                                                    "null",
                                                    {
                                                        "doc": "Possible tiers as defined by Genomics England",
                                                        "name": "Tier",
                                                        "symbols": [
                                                            "NONE",
                                                            "TIER1",
                                                            "TIER2",
                                                            "TIER3"
                                                        ],
                                                        "type": "enum"
                                                    }
                                                ]
                                            }
                                        ],
                                        "name": "ReportEventCancer",
                                        "type": "record"
                                    },
                                    "type": "array"
                                }
                            },
                            {
                                "doc": "List of variant calls",
                                "name": "variantCalls",
                                "type": [
                                    "null",
                                    {
                                        "items": {
                                            "fields": [
                                                {
                                                    "doc": "LP Number of the family member:",
                                                    "name": "sampleId",
                                                    "type": "string"
                                                },
                                                {
                                                    "doc": "Depth for Reference Allele",
                                                    "name": "depthReference",
                                                    "type": [
                                                        "null",
                                                        "int"
                                                    ]
                                                },
                                                {
                                                    "doc": "Depth for Alternate Allele",
                                                    "name": "depthAlternate",
                                                    "type": [
                                                        "null",
                                                        "int"
                                                    ]
                                                },
                                                {
                                                    "doc": "Variant Allele Frequency",
                                                    "name": "vaf",
                                                    "type": [
                                                        "null",
                                                        "double"
                                                    ]
                                                }
                                            ],
                                            "name": "VariantCall",
                                            "type": "record"
                                        },
                                        "type": "array"
                                    }
                                ]
                            },
                            {
                                "doc": "Describe whether this is a somatic or Germline variant",
                                "name": "alleleOrigins",
                                "type": {
                                    "items": {
                                        "doc": "Variant origin.\n\n* `SO_0001781`: de novo variant. http://purl.obolibrary.org/obo/SO_0001781\n* `SO_0001778`: germline variant. http://purl.obolibrary.org/obo/SO_0001778\n* `SO_0001775`: maternal variant. http://purl.obolibrary.org/obo/SO_0001775\n* `SO_0001776`: paternal variant. http://purl.obolibrary.org/obo/SO_0001776\n* `SO_0001779`: pedigree specific variant. http://purl.obolibrary.org/obo/SO_0001779\n* `SO_0001780`: population specific variant. http://purl.obolibrary.org/obo/SO_0001780\n* `SO_0001777`: somatic variant. http://purl.obolibrary.org/obo/SO_0001777",
                                        "name": "AlleleOrigin",
                                        "symbols": [
                                            "de_novo_variant",
                                            "germline_variant",
                                            "maternal_variant",
                                            "paternal_variant",
                                            "pedigree_specific_variant",
                                            "population_specific_variant",
                                            "somatic_variant"
                                        ],
                                        "type": "enum"
                                    },
                                    "type": "array"
                                }
                            }
                        ],
                        "name": "ReportedVariantCancer",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Candidate Structural Variants - as defined in CommonInterpreted",
            "name": "candidateStructuralVariants",
            "type": [
                "null",
                {
                    "items": {
                        "fields": [
                            {
                                "doc": "chromosome, named as: 1-22,X,Y,MT(other contigs name)",
                                "name": "chromosome",
                                "type": "string"
                            },
                            {
                                "doc": "start: start position 1-based",
                                "name": "start",
                                "type": "int"
                            },
                            {
                                "doc": "end: end position 1-based",
                                "name": "end",
                                "type": "int"
                            },
                            {
                                "doc": "The structural variant type",
                                "name": "type",
                                "type": {
                                    "doc": "Structural variant type as defined by the VCF specification 4.2 for field ID.",
                                    "fields": [
                                        {
                                            "name": "firstLevelType",
                                            "type": {
                                                "doc": "The first level type must be one\nof the following:\n\n* `DEL` Deletion relative to the reference\n* `INS` Insertion of novel sequence relative to the reference\n* `DUP` Region of elevated copy number relative to the reference\n* `INV` Inversion of reference sequence\n* `CNV` Copy number variable region (may be both deletion and duplication) The CNV category should not be used when a\nmore specific category can be applied.  Reserved subtypes include:\n* `DUP:TANDEM` Tandem duplication\n* `DEL:ME` Deletion of mobile element relative to the reference\n* `INS:ME` Insertion of a mobile element relative to the reference",
                                                "name": "StructuralVariantFirstLevelType",
                                                "symbols": [
                                                    "DEL",
                                                    "INS",
                                                    "DUP",
                                                    "INV",
                                                    "CNV",
                                                    "DUP_TANDEM",
                                                    "DEL_ME",
                                                    "INS_ME"
                                                ],
                                                "type": "enum"
                                            }
                                        },
                                        {
                                            "name": "subtype",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        }
                                    ],
                                    "name": "StructuralVariantType",
                                    "type": "record"
                                }
                            },
                            {
                                "doc": "reference: Reference Allele sequence, the same provided in vcf",
                                "name": "reference",
                                "type": "string"
                            },
                            {
                                "doc": "alternate: Alternate Allele sequence, the same provided in vcf",
                                "name": "alternate",
                                "type": "string"
                            },
                            {
                                "doc": "For example HGMD ID)",
                                "name": "additionalTextualVariantAnnotations",
                                "type": [
                                    "null",
                                    {
                                        "type": "map",
                                        "values": "string"
                                    }
                                ]
                            },
                            {
                                "doc": "For Example (Allele Frequency, sift, polyphen, mutationTaster, CADD. ..)",
                                "name": "additionalNumericVariantAnnotations",
                                "type": [
                                    "null",
                                    {
                                        "type": "map",
                                        "values": "float"
                                    }
                                ]
                            },
                            {
                                "doc": "Comments",
                                "name": "comments",
                                "type": [
                                    "null",
                                    {
                                        "items": "string",
                                        "type": "array"
                                    }
                                ]
                            },
                            {
                                "doc": "Describe whether this is a somatic or Germline variant",
                                "name": "alleleOrigins",
                                "type": {
                                    "items": "AlleleOrigin",
                                    "type": "array"
                                }
                            }
                        ],
                        "name": "ReportedStructuralVariantCancer",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Summary of the interpretation, this should reflects the positive conclusions of this interpretation",
            "name": "genomicInterpretation",
            "type": "string"
        },
        {
            "doc": "References (pubmed Ids)",
            "name": "references",
            "type": [
                "null",
                {
                    "items": "string",
                    "type": "array"
                }
            ]
        },
        {
            "doc": "This map should contains the version of the different DBs used in the process",
            "name": "referenceDatabasesVersions",
            "type": {
                "type": "map",
                "values": "string"
            }
        },
        {
            "doc": "This map should contains the version of the different DBs software in the process",
            "name": "softwareVersions",
            "type": {
                "type": "map",
                "values": "string"
            }
        },
        {
            "doc": "This map of key: panel_name, value: (arrays of (map of key: gene, value: gene coverage))",
            "name": "genePanelsCoverage",
            "type": {
                "type": "map",
                "values": {
                    "items": {
                        "type": "map",
                        "values": "string"
                    },
                    "type": "array"
                }
            }
        }
    ],
    "name": "ClinicalReportCancer",
    "namespace": "org.gel.models.report.avro",
    "type": "record"
}