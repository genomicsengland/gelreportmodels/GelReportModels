{
    "doc": "The output of a coverage analysis",
    "fields": [
        {
            "doc": "The coverage analysis results",
            "name": "results",
            "type": {
                "doc": "Coverage analysis results",
                "fields": [
                    {
                        "doc": "The coverage statistics for each gene (only filled when coding region analysis enabled)",
                        "name": "genes",
                        "type": {
                            "items": {
                                "doc": "All coverage information about a given gene",
                                "fields": [
                                    {
                                        "doc": "The coverage statistics for each transcript",
                                        "name": "trs",
                                        "type": {
                                            "items": {
                                                "doc": "All coverage information about a given transcript",
                                                "fields": [
                                                    {
                                                        "doc": "The transcript's Ensembl identifier",
                                                        "name": "id",
                                                        "type": "string"
                                                    },
                                                    {
                                                        "doc": "The coverage statistics across this transcript",
                                                        "name": "stats",
                                                        "type": {
                                                            "doc": "Represents a group of coverage statistics over a genomic region",
                                                            "fields": [
                                                                {
                                                                    "doc": "The average depth of coverage",
                                                                    "name": "avg",
                                                                    "type": "float"
                                                                },
                                                                {
                                                                    "doc": "The depth of coverage standard deviation",
                                                                    "name": "sd",
                                                                    "type": "float"
                                                                },
                                                                {
                                                                    "doc": "The median depth of coverage",
                                                                    "name": "med",
                                                                    "type": "float"
                                                                },
                                                                {
                                                                    "doc": "The GC content",
                                                                    "name": "gc",
                                                                    "type": [
                                                                        "null",
                                                                        "float"
                                                                    ]
                                                                },
                                                                {
                                                                    "doc": "The 75th percentile of coverage distribution",
                                                                    "name": "pct75",
                                                                    "type": "float"
                                                                },
                                                                {
                                                                    "doc": "The 25th percentile of coverage distribution",
                                                                    "name": "pct25",
                                                                    "type": "float"
                                                                },
                                                                {
                                                                    "doc": "The number of bases in the region (this is nullable as it can be calculated from coordinates of the region)",
                                                                    "name": "bases",
                                                                    "type": [
                                                                        "null",
                                                                        "int"
                                                                    ]
                                                                },
                                                                {
                                                                    "doc": "The number of bases in the region with a coverage less than 15x (this is nullable as it can be calculated from coordinates of the region)",
                                                                    "name": "bases_lt_15x",
                                                                    "type": [
                                                                        "null",
                                                                        "int"
                                                                    ]
                                                                },
                                                                {
                                                                    "doc": "The number of bases in the region with a coverage greater than or equal 15x (this is nullable as it can be calculated from coordinates of the region)",
                                                                    "name": "bases_gte_15x",
                                                                    "type": [
                                                                        "null",
                                                                        "int"
                                                                    ]
                                                                },
                                                                {
                                                                    "doc": "The number of bases in the region with a coverage greater than or equal 30x (this is nullable as it can be calculated from coordinates of the region)",
                                                                    "name": "bases_gte_30x",
                                                                    "type": [
                                                                        "null",
                                                                        "int"
                                                                    ]
                                                                },
                                                                {
                                                                    "doc": "The number of bases in the region with a coverage greater than or equal 50x (this is nullable as it can be calculated from coordinates of the region)",
                                                                    "name": "bases_gte_50x",
                                                                    "type": [
                                                                        "null",
                                                                        "int"
                                                                    ]
                                                                },
                                                                {
                                                                    "doc": "The number of positions with a depth of coverage greater than or equal than 50x\n        (the original name is %>=50x, but this cannot be used as a property name in Avro)",
                                                                    "name": "gte50x",
                                                                    "type": "float"
                                                                },
                                                                {
                                                                    "doc": "The number of positions with a depth of coverage greater than or equal than 30x\n        (the original name is %>=30x, but this cannot be used as a property name in Avro)",
                                                                    "name": "gte30x",
                                                                    "type": "float"
                                                                },
                                                                {
                                                                    "doc": "The number of positions with a depth of coverage greater than or equal than 15x\n        (the original name is %>=15x, but this cannot be used as a property name in Avro)",
                                                                    "name": "gte15x",
                                                                    "type": "float"
                                                                },
                                                                {
                                                                    "doc": "The number of positions with a depth of coverage less than 15x\n        (the original name is %<15x, but this cannot be used as a property name in Avro)",
                                                                    "name": "lt15x",
                                                                    "type": "float"
                                                                },
                                                                {
                                                                    "doc": "The squared root sum of squares of the deviation from the mean",
                                                                    "name": "rmsd",
                                                                    "type": [
                                                                        "null",
                                                                        "float"
                                                                    ]
                                                                }
                                                            ],
                                                            "name": "RegionStatistics",
                                                            "type": "record"
                                                        }
                                                    },
                                                    {
                                                        "doc": "The list of exons in this transcript",
                                                        "name": "exons",
                                                        "type": [
                                                            "null",
                                                            {
                                                                "items": {
                                                                    "doc": "All coverage information about a given exon",
                                                                    "fields": [
                                                                        {
                                                                            "doc": "An exon unique name within each transcript (e.g.: exon1, exon2, etc.)",
                                                                            "name": "exon",
                                                                            "type": "string"
                                                                        },
                                                                        {
                                                                            "doc": "The exon start position. Inclusive, 0-based",
                                                                            "name": "s",
                                                                            "type": "int"
                                                                        },
                                                                        {
                                                                            "doc": "The exon padded start. Inclusive, 0-based\n        (i.e.: for a gene in the positive strand and a padding of 15bp, this will be the exon start positions minus 15 bp)\n        DEPRECATED",
                                                                            "name": "padded_s",
                                                                            "type": [
                                                                                "null",
                                                                                "int"
                                                                            ]
                                                                        },
                                                                        {
                                                                            "doc": "The exon end position. Exclusive, 0-based",
                                                                            "name": "e",
                                                                            "type": "int"
                                                                        },
                                                                        {
                                                                            "doc": "The exon padded end. Exclusive, 0-based\n        (i.e.: for a gene in the positive strand and a padding of 15bp, this will be the exon end positions plus 15 bp)\n        DEPRECATED",
                                                                            "name": "padded_e",
                                                                            "type": [
                                                                                "null",
                                                                                "int"
                                                                            ]
                                                                        },
                                                                        {
                                                                            "doc": "The exon length calculated without padding (this field is redundant)\n        DEPRECATED",
                                                                            "name": "l",
                                                                            "type": [
                                                                                "null",
                                                                                "int"
                                                                            ]
                                                                        },
                                                                        {
                                                                            "doc": "The list of coverage gaps",
                                                                            "name": "gaps",
                                                                            "type": {
                                                                                "items": {
                                                                                    "doc": "A gap in coverage. A gap is a contiguous region under a certain depth of coverage.\n    There are two thresholds to define at analysis time:\n* The depth of coverage threshold under which a gap is considered\n* The number of consecutive positions under the depth of coverage threshold to call a gap\n\n    e.g.: we may consider a gap those regions of more than 5 consecutive bp under 15x",
                                                                                    "fields": [
                                                                                        {
                                                                                            "doc": "Gap start position. Inclusive, 0-based",
                                                                                            "name": "s",
                                                                                            "type": "int"
                                                                                        },
                                                                                        {
                                                                                            "doc": "Gap end position. Exclusive, 0-based",
                                                                                            "name": "e",
                                                                                            "type": "int"
                                                                                        },
                                                                                        {
                                                                                            "doc": "Gap length (this information is redundant as can be calculated from start and end)\n        DEPRECATED",
                                                                                            "name": "l",
                                                                                            "type": [
                                                                                                "null",
                                                                                                "int"
                                                                                            ]
                                                                                        }
                                                                                    ],
                                                                                    "name": "CoverageGap",
                                                                                    "type": "record"
                                                                                },
                                                                                "type": "array"
                                                                            }
                                                                        },
                                                                        {
                                                                            "doc": "The coverage statistics across this exon",
                                                                            "name": "stats",
                                                                            "type": "RegionStatistics"
                                                                        }
                                                                    ],
                                                                    "name": "Exon",
                                                                    "type": "record"
                                                                },
                                                                "type": "array"
                                                            }
                                                        ]
                                                    }
                                                ],
                                                "name": "Transcript",
                                                "type": "record"
                                            },
                                            "type": "array"
                                        }
                                    },
                                    {
                                        "doc": "The coverage statistics for the union transcript",
                                        "name": "union_tr",
                                        "type": "Transcript"
                                    },
                                    {
                                        "doc": "The HGNC gene symbol",
                                        "name": "name",
                                        "type": "string"
                                    },
                                    {
                                        "doc": "The chromosome identifier (may have \"chr\" prefix or not depending on input data)",
                                        "name": "chr",
                                        "type": "string"
                                    }
                                ],
                                "name": "Gene",
                                "type": "record"
                            },
                            "type": "array"
                        }
                    },
                    {
                        "doc": "The coverage statistics across the whole coding region (only filled when coding region analysis enabled)\n        (i.e.: the coding region may refer to the whole exome, to a provided list of genes or to the genes belonging\n        to a PanelApp panel)",
                        "name": "coding_region",
                        "type": [
                            "null",
                            {
                                "doc": "All coverage information about the coding region",
                                "fields": [
                                    {
                                        "doc": "The coverage statistics across the whole coding region",
                                        "name": "stats",
                                        "type": "RegionStatistics"
                                    },
                                    {
                                        "doc": "The list of chromosome in this coding region analysis",
                                        "name": "chrs",
                                        "type": {
                                            "items": {
                                                "doc": "All coverage information about a given chromosome",
                                                "fields": [
                                                    {
                                                        "doc": "The chromosome identifier (may have \"chr\" prefix or not depending on input data)",
                                                        "name": "chr",
                                                        "type": "string"
                                                    },
                                                    {
                                                        "doc": "The average depth of coverage",
                                                        "name": "avg",
                                                        "type": "float"
                                                    },
                                                    {
                                                        "doc": "The depth of coverage standard deviation",
                                                        "name": "sd",
                                                        "type": "float"
                                                    },
                                                    {
                                                        "doc": "The median depth of coverage",
                                                        "name": "med",
                                                        "type": "float"
                                                    },
                                                    {
                                                        "doc": "The GC content",
                                                        "name": "gc",
                                                        "type": [
                                                            "null",
                                                            "float"
                                                        ]
                                                    },
                                                    {
                                                        "doc": "The 75th percentile of coverage distribution",
                                                        "name": "pct75",
                                                        "type": "float"
                                                    },
                                                    {
                                                        "doc": "The 25th percentile of coverage distribution",
                                                        "name": "pct25",
                                                        "type": "float"
                                                    },
                                                    {
                                                        "doc": "The number of bases in the region",
                                                        "name": "bases",
                                                        "type": "int"
                                                    },
                                                    {
                                                        "doc": "The number of positions with a depth of coverage greater than or equal than 50x",
                                                        "name": "gte50x",
                                                        "type": "float"
                                                    },
                                                    {
                                                        "doc": "The number of positions with a depth of coverage greater than or equal than 30x",
                                                        "name": "gte30x",
                                                        "type": "float"
                                                    },
                                                    {
                                                        "doc": "The number of positions with a depth of coverage greater than or equal than 15x",
                                                        "name": "gte15x",
                                                        "type": "float"
                                                    },
                                                    {
                                                        "doc": "The number of positions with a depth of coverage less than 15x",
                                                        "name": "lt15x",
                                                        "type": "float"
                                                    },
                                                    {
                                                        "doc": "The squared root sum of squares of the deviation from the mean",
                                                        "name": "rmsd",
                                                        "type": [
                                                            "null",
                                                            "float"
                                                        ]
                                                    }
                                                ],
                                                "name": "Chromosome",
                                                "type": "record"
                                            },
                                            "type": "array"
                                        }
                                    }
                                ],
                                "name": "CodingRegion",
                                "type": "record"
                            }
                        ]
                    },
                    {
                        "doc": "The coverage statistics across the whole genome (only filled when whole genome analysis enabled)",
                        "name": "whole_genome",
                        "type": [
                            "null",
                            {
                                "fields": [
                                    {
                                        "doc": "The coverage statistics across the whole genome",
                                        "name": "stats",
                                        "type": "RegionStatistics"
                                    },
                                    {
                                        "doc": "The coverage statistics disaggregated by chromosome. The aggregation of all autosomes is in this list as another chromosome with chr=\"autosomes\"",
                                        "name": "chrs",
                                        "type": {
                                            "items": "Chromosome",
                                            "type": "array"
                                        }
                                    }
                                ],
                                "name": "WholeGenome",
                                "type": "record"
                            }
                        ]
                    },
                    {
                        "doc": "The coding regions for which the average coverage is 0.0.",
                        "name": "uncovered_genes",
                        "type": {
                            "items": {
                                "doc": "A gene for which there is no information about coverage. A low covered gene will not be identified in this list,\n    only genes for which there is no coverage data.",
                                "fields": [
                                    {
                                        "doc": "The chromosome to which the gene belongs",
                                        "name": "chr",
                                        "type": "string"
                                    },
                                    {
                                        "doc": "The gene symbol of the gene",
                                        "name": "name",
                                        "type": "string"
                                    },
                                    {
                                        "doc": "The exon start position. Inclusive, 0-based (nullable to maintain backward compatibility)",
                                        "name": "s",
                                        "type": [
                                            "null",
                                            "int"
                                        ]
                                    },
                                    {
                                        "doc": "The exon end position. Exclusive, 0-based (nullable to maintain backward compatibility)",
                                        "name": "e",
                                        "type": [
                                            "null",
                                            "int"
                                        ]
                                    }
                                ],
                                "name": "UncoveredGene",
                                "type": "record"
                            },
                            "type": "array"
                        }
                    }
                ],
                "name": "CoverageAnalysisResults",
                "type": "record"
            }
        },
        {
            "doc": "The analysis parameters for this analysis",
            "name": "parameters",
            "type": {
                "doc": "The configuration parameters used for the analysis",
                "fields": [
                    {
                        "doc": "The flag indicating if coding region analysis is to run",
                        "name": "coding_region_stats_enabled",
                        "type": "boolean"
                    },
                    {
                        "doc": "The flag indicating if the exon stats are to be maintained in the results (these increase the size of the output sensibly).",
                        "name": "exon_stats_enabled",
                        "type": "boolean"
                    },
                    {
                        "doc": "The flag indicating if whole genome analysis is to run",
                        "name": "wg_stats_enabled",
                        "type": "boolean"
                    },
                    {
                        "doc": "The list of genes to analyse",
                        "name": "gene_list",
                        "type": [
                            "null",
                            {
                                "items": "string",
                                "type": "array"
                            }
                        ]
                    },
                    {
                        "doc": "The panel name",
                        "name": "panel",
                        "type": [
                            "null",
                            "string"
                        ]
                    },
                    {
                        "doc": "The panel version",
                        "name": "panel_version",
                        "type": [
                            "null",
                            "string"
                        ]
                    },
                    {
                        "doc": "The PanelApp host",
                        "name": "panelapp_host",
                        "type": [
                            "null",
                            "string"
                        ]
                    },
                    {
                        "doc": "The comma-separated list of PanelApp gene confidences that will be analysed\n        (i.e.: \"HighEvidence\" will imply that only genes with \"HighEvidence\" in a given panel will be analysed)",
                        "name": "panelapp_gene_confidence",
                        "type": [
                            "null",
                            "string"
                        ]
                    },
                    {
                        "doc": "The comma-separated list of Havana transcript biotypes that are valid for analysis. Any transcript with a different biotype will not be analysed.\n        Biotypes are defined here http://vega.archive.ensembl.org/info/about/gene_and_transcript_types.html\n        Common configuration is: IG_C_gene,IG_D_gene,IG_J_gene,IG_V_gene,IG_V_gene,protein_coding,nonsense_mediated_decay,non_stop_decay,TR_C_gene,TR_D_gene,TR_J_gene,TR_V_gene",
                        "name": "transcript_filtering_biotypes",
                        "type": "string"
                    },
                    {
                        "doc": "The comma-separated list of transcript flags that are valid for anlysis.\n        Common configuration: basic",
                        "name": "transcript_filtering_flags",
                        "type": "string"
                    },
                    {
                        "doc": "The CellBase host",
                        "name": "cellbase_host",
                        "type": "string"
                    },
                    {
                        "doc": "The CellBase version",
                        "name": "cellbase_version",
                        "type": "string"
                    },
                    {
                        "doc": "The assembly used to fetch data from CellBase",
                        "name": "grch37",
                        "type": "string"
                    },
                    {
                        "doc": "Cellbase configuration for the species (e.g.: hsapiens)",
                        "name": "species",
                        "type": "string"
                    },
                    {
                        "doc": "The exon padding measured in base pairs",
                        "name": "exon_padding",
                        "type": "int"
                    },
                    {
                        "doc": "The depth of coverage under which a gap is considered",
                        "name": "gap_coverage_threshold",
                        "type": "int"
                    },
                    {
                        "doc": "The length of contiguous positions with low coverage to consider a gap",
                        "name": "gap_length_threshold",
                        "type": "int"
                    },
                    {
                        "doc": "The input bigwig file",
                        "name": "input_file",
                        "type": "string"
                    },
                    {
                        "doc": "The input configuration file",
                        "name": "configuration_file",
                        "type": "string"
                    },
                    {
                        "doc": "The input BED file determining the whole genome regions to be analysed (this is usually used to discard regions containing ambiguous bases from the coverage analysis by providing a non N regions BED file)",
                        "name": "wg_regions",
                        "type": [
                            "null",
                            "string"
                        ]
                    }
                ],
                "name": "AnalysisParameters",
                "type": "record"
            }
        }
    ],
    "name": "AnalysisResults",
    "namespace": "org.gel.models.coverage.avro",
    "type": "record"
}