{
    "doc": "The variant group level questions",
    "fields": [
        {
            "doc": "This value groups variants that together could explain the phenotype according to the mode of inheritance used.\n        (e.g.: compound heterozygous). All the variants in the same report sharing the same value will be considered in\n        the same group (i.e.: reported together). This value is an integer unique in the whole report.\n        These values are only relevant within the same report.",
            "name": "variantGroup",
            "type": "int"
        },
        {
            "doc": "The variant level questions for each of the variants in the group",
            "name": "variantLevelQuestions",
            "type": {
                "items": {
                    "doc": "The variant level questions",
                    "fields": [
                        {
                            "doc": "Variant coordinates following format `chromosome:position:reference:alternate`",
                            "name": "variantDetails",
                            "type": "string"
                        },
                        {
                            "doc": "Did you carry out technical confirmation of this variant via an alternative test?",
                            "name": "confirmationDecision",
                            "type": {
                                "name": "ConfirmationDecision",
                                "symbols": [
                                    "yes",
                                    "no",
                                    "na"
                                ],
                                "type": "enum"
                            }
                        },
                        {
                            "doc": "Did the test confirm that the variant is present?",
                            "name": "confirmationOutcome",
                            "type": {
                                "name": "ConfirmationOutcome",
                                "symbols": [
                                    "yes",
                                    "no",
                                    "na"
                                ],
                                "type": "enum"
                            }
                        },
                        {
                            "doc": "Did you include the variant in your report to the clinician?",
                            "name": "reportingQuestion",
                            "type": {
                                "name": "ReportingQuestion",
                                "symbols": [
                                    "yes",
                                    "no",
                                    "na"
                                ],
                                "type": "enum"
                            }
                        },
                        {
                            "doc": "What ACMG pathogenicity score (1-5) did you assign to this variant?",
                            "name": "acmgClassification",
                            "type": {
                                "name": "ACMGClassification",
                                "symbols": [
                                    "pathogenic_variant",
                                    "likely_pathogenic_variant",
                                    "variant_of_unknown_clinical_significance",
                                    "likely_benign_variant",
                                    "benign_variant",
                                    "not_assessed"
                                ],
                                "type": "enum"
                            }
                        },
                        {
                            "doc": "Please provide PMIDs for papers which you have used to inform your assessment for this variant, separated by a `;` for multiple papers",
                            "name": "publications",
                            "type": "string"
                        }
                    ],
                    "name": "VariantLevelQuestions",
                    "type": "record"
                },
                "type": "array"
            }
        },
        {
            "doc": "Is evidence for this variant/variant pair sufficient to use it for clinical purposes such as prenatal diagnosis or predictive testing?",
            "name": "actionability",
            "type": {
                "name": "Actionability",
                "symbols": [
                    "yes",
                    "no",
                    "not_yet",
                    "na"
                ],
                "type": "enum"
            }
        },
        {
            "doc": "Has the clinical team identified any changes to clinical care which could potentially arise as a result of this variant/variant pair?",
            "name": "clinicalUtility",
            "type": {
                "items": {
                    "name": "ClinicalUtility",
                    "symbols": [
                        "none",
                        "change_in_medication",
                        "surgical_option",
                        "additional_surveillance_for_proband_or_relatives",
                        "clinical_trial_eligibility",
                        "informs_reproductive_choice",
                        "unknown",
                        "other"
                    ],
                    "type": "enum"
                },
                "type": "array"
            }
        },
        {
            "doc": "Did you report the variant(s) as being partially or completely causative of the family's presenting phenotype(s)?",
            "name": "phenotypesSolved",
            "type": {
                "name": "PhenotypesSolved",
                "symbols": [
                    "yes",
                    "no",
                    "partially",
                    "unknown"
                ],
                "type": "enum"
            }
        },
        {
            "doc": "If you indicated that the variant(s) only partially explained the family\u2019s presenting phenotypes, please indicate which HPO terms you are confident that they DO explain",
            "name": "phenotypesExplained",
            "type": [
                "null",
                {
                    "items": "string",
                    "type": "array"
                }
            ]
        }
    ],
    "name": "VariantGroupLevelQuestions",
    "namespace": "org.gel.models.report.avro",
    "type": "record"
}