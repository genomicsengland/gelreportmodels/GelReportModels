{
    "doc": "This is the response from `POST /variantsets/search` expressed as JSON.",
    "fields": [
        {
            "default": [],
            "doc": "The list of matching variant sets.",
            "name": "variantSets",
            "type": {
                "items": {
                    "doc": "`Variant` and `CallSet` both belong to a `VariantSet`.\n`VariantSet` belongs to a `Dataset`.\nThe variant set is equivalent to a VCF file.",
                    "fields": [
                        {
                            "doc": "The variant set ID.",
                            "name": "id",
                            "type": "string"
                        },
                        {
                            "doc": "The ID of the dataset this variant set belongs to.",
                            "name": "datasetId",
                            "type": "string"
                        },
                        {
                            "doc": "The reference set the variants in this variant set are using.",
                            "name": "referenceSetId",
                            "type": "string"
                        },
                        {
                            "default": [],
                            "doc": "The metadata associated with this variant set. This is equivalent to\n  the VCF header information not already presented in first class fields.",
                            "name": "metadata",
                            "type": {
                                "items": {
                                    "doc": "This metadata represents VCF header information.",
                                    "fields": [
                                        {
                                            "doc": "The top-level key.",
                                            "name": "key",
                                            "type": "string"
                                        },
                                        {
                                            "doc": "The value field for simple metadata.",
                                            "name": "value",
                                            "type": "string"
                                        },
                                        {
                                            "doc": "User-provided ID field, not enforced by this API.\n  Two or more pieces of structured metadata with identical\n  id and key fields are considered equivalent.",
                                            "name": "id",
                                            "type": "string"
                                        },
                                        {
                                            "doc": "The type of data.",
                                            "name": "type",
                                            "type": "string"
                                        },
                                        {
                                            "doc": "The number of values that can be included in a field described by this\n  metadata.",
                                            "name": "number",
                                            "type": "string"
                                        },
                                        {
                                            "doc": "A textual description of this metadata.",
                                            "name": "description",
                                            "type": "string"
                                        },
                                        {
                                            "default": {},
                                            "doc": "Remaining structured metadata key-value pairs.",
                                            "name": "info",
                                            "type": {
                                                "type": "map",
                                                "values": {
                                                    "items": "string",
                                                    "type": "array"
                                                }
                                            }
                                        }
                                    ],
                                    "name": "VariantSetMetadata",
                                    "type": "record"
                                },
                                "type": "array"
                            }
                        }
                    ],
                    "name": "VariantSet",
                    "namespace": "org.ga4gh.models",
                    "type": "record"
                },
                "type": "array"
            }
        },
        {
            "default": null,
            "doc": "The continuation token, which is used to page through large result sets.\n  Provide this value in a subsequent request to return the next page of\n  results. This field will be empty if there aren't any additional results.",
            "name": "nextPageToken",
            "type": [
                "null",
                "string"
            ]
        }
    ],
    "name": "SearchVariantSetsResponse",
    "namespace": "org.ga4gh.methods",
    "type": "record"
}