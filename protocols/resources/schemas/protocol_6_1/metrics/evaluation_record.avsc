{
    "doc": "Evaluation\n========================================\nreportedVsGeneticSummary: familyPassesGvsRChecks, familyFailsACheck, familyMissingACheck",
    "fields": [
        {
            "doc": "Coverae-based sex evaluation. One entry per sample",
            "name": "coverageBasedSexCheck",
            "type": [
                "null",
                {
                    "items": {
                        "doc": "Evaluation\n========================================",
                        "fields": [
                            {
                                "doc": "Sample Id",
                                "name": "sampleId",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Reported phenotypic sex",
                                "name": "reportedPhenotypicSex",
                                "type": [
                                    "null",
                                    {
                                        "doc": "Phenotypic sex",
                                        "name": "Sex",
                                        "symbols": [
                                            "UNKNOWN",
                                            "MALE",
                                            "FEMALE",
                                            "OTHER"
                                        ],
                                        "type": "enum"
                                    }
                                ]
                            },
                            {
                                "doc": "Reported karyotypic sex",
                                "name": "reportedKaryotypicSex",
                                "type": [
                                    "null",
                                    {
                                        "doc": "Kariotypic sex\nTODO: Check if we want to have different karyotype definitions for XO clearcut/doubtful",
                                        "name": "KaryotypicSex",
                                        "symbols": [
                                            "UNKNOWN",
                                            "XX",
                                            "XY",
                                            "XO",
                                            "XXY",
                                            "XXX",
                                            "XXYY",
                                            "XXXY",
                                            "XXXX",
                                            "XYY",
                                            "OTHER"
                                        ],
                                        "type": "enum"
                                    }
                                ]
                            },
                            {
                                "doc": "Inferred coverage-based sex karyotype",
                                "name": "inferredSexKaryotype",
                                "type": [
                                    "null",
                                    "KaryotypicSex"
                                ]
                            },
                            {
                                "doc": "Whether the sample is a sex query (yes, no, unknown, notTested)",
                                "name": "sexQuery",
                                "type": [
                                    "null",
                                    {
                                        "doc": "A query",
                                        "name": "Query",
                                        "symbols": [
                                            "yes",
                                            "no",
                                            "unknown",
                                            "notTested"
                                        ],
                                        "type": "enum"
                                    }
                                ]
                            },
                            {
                                "doc": "Comments",
                                "name": "comments",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            }
                        ],
                        "name": "CoverageBasedSexCheck",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Mendelian inconsitencies evaluation. One entry per sample",
            "name": "mendelianInconsistenciesCheck",
            "type": [
                "null",
                {
                    "items": {
                        "fields": [
                            {
                                "doc": "Sample Id",
                                "name": "sampleId",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Whether the sample is a Mendelian inconsistencies query (yes, no, unknown, notTested)",
                                "name": "mendelianInconsistenciesQuery",
                                "type": [
                                    "null",
                                    "Query"
                                ]
                            },
                            {
                                "doc": "Mendelian inconsistencies cannot always be computed for all the samples in the family (depends on family\nstructure). Specify here if this is the case or there was any other issues",
                                "name": "comments",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            }
                        ],
                        "name": "MendelianInconsistenciesCheck",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Within-family relatedness evaluation. One entry per pair of samples",
            "name": "familyRelatednessCheck",
            "type": [
                "null",
                {
                    "items": {
                        "fields": [
                            {
                                "doc": "Sample Id of one of the samples in the pair",
                                "name": "sampleId1",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Sample Id of the other sample in the pair",
                                "name": "sampleId2",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Reported relationship from sampleId1 to sampleId2 according to the pedigree provided",
                                "name": "relationshipFromPedigree",
                                "type": [
                                    "null",
                                    {
                                        "doc": "Familiar relationship from pedrigree",
                                        "name": "FamiliarRelationship",
                                        "namespace": "org.gel.models.participant.avro",
                                        "symbols": [
                                            "TwinsMonozygous",
                                            "TwinsDizygous",
                                            "TwinsUnknown",
                                            "FullSibling",
                                            "FullSiblingF",
                                            "FullSiblingM",
                                            "Mother",
                                            "Father",
                                            "Son",
                                            "Daughter",
                                            "ChildOfUnknownSex",
                                            "MaternalAunt",
                                            "MaternalUncle",
                                            "MaternalUncleOrAunt",
                                            "PaternalAunt",
                                            "PaternalUncle",
                                            "PaternalUncleOrAunt",
                                            "MaternalGrandmother",
                                            "PaternalGrandmother",
                                            "MaternalGrandfather",
                                            "PaternalGrandfather",
                                            "DoubleFirstCousin",
                                            "MaternalCousinSister",
                                            "PaternalCousinSister",
                                            "MaternalCousinBrother",
                                            "PaternalCousinBrother",
                                            "Cousin",
                                            "Spouse",
                                            "Other",
                                            "RelationIsNotClear",
                                            "Unknown"
                                        ],
                                        "type": "enum"
                                    }
                                ]
                            },
                            {
                                "doc": "Expected relationship according to IBD",
                                "name": "possibleRelationship",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Whether the pair of samples are a within-family query (yes, no, unknown, notTested)",
                                "name": "withinFamilyIBDQuery",
                                "type": [
                                    "null",
                                    "Query"
                                ]
                            },
                            {
                                "doc": "Comments",
                                "name": "comments",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            }
                        ],
                        "name": "FamilyRelatednessCheck",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Final evaluation summary. Does the family passes RvsG checks or errors are present?",
            "name": "reportedVsGeneticSummary",
            "type": [
                "null",
                {
                    "doc": "Reported vs Genetic Summary",
                    "name": "reportedVsGeneticSummary",
                    "symbols": [
                        "familyPassesGvsRChecks",
                        "familyFailsACheck",
                        "familyMissingACheck"
                    ],
                    "type": "enum"
                }
            ]
        }
    ],
    "name": "Evaluation",
    "namespace": "org.gel.models.metrics.avro",
    "type": "record"
}