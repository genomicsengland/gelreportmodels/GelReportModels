{
    "doc": "This defines a RD Participant (demographics and pedigree information)",
    "fields": [
        {
            "doc": "Numbering used to refer to each member of the pedigree",
            "name": "pedigreeId",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "doc": "If this field is true, the member should be considered the proband of this family",
            "name": "isProband",
            "type": [
                "null",
                "boolean"
            ]
        },
        {
            "doc": "participantId",
            "name": "participantId",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "participantQCState",
            "name": "participantQCState",
            "type": [
                "null",
                {
                    "doc": "QCState Status",
                    "name": "ParticipantQCState",
                    "symbols": [
                        "noState",
                        "passedMedicalReviewReadyForInterpretation",
                        "passedMedicalReviewNotReadyForInterpretation",
                        "queryToGel",
                        "queryToGMC",
                        "failed"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "superFamily id, this id is built as a concatenation of all families id in this superfamily i.e, fam10024_fam100457",
            "name": "gelSuperFamilyId",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Sex of the Participant",
            "name": "sex",
            "type": {
                "doc": "Sex",
                "name": "Sex",
                "symbols": [
                    "MALE",
                    "FEMALE",
                    "UNKNOWN"
                ],
                "type": "enum"
            }
        },
        {
            "doc": "Karyotypic sex of the participant as previously established or by looking at the GEL genome",
            "name": "personKaryotypicSex",
            "type": [
                "null",
                {
                    "doc": "Karyotipic Sex",
                    "name": "PersonKaryotipicSex",
                    "symbols": [
                        "UNKNOWN",
                        "XX",
                        "XY",
                        "XO",
                        "XXY",
                        "XXX",
                        "XXYY",
                        "XXXY",
                        "XXXX",
                        "XYY",
                        "OTHER"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "Year of Birth",
            "name": "yearOfBirth",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "doc": "refers to the pedigreeId of the father\n        Id of the parent, if unknown then no parent is referenced. Parents may need to be entered even if no data is known\n        about them in order to unambiguously reconstruct the pedigree.",
            "name": "fatherId",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "doc": "refers to the pedigreeId of the mother\n        Id of the parent, if unknown then no parent is referenced. Parents may need to be entered even if no data is known\n        about them in order to unambiguously reconstruct the pedigree.",
            "name": "motherId",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "doc": "this id is built using the original familyId and the original pedigreeId of the father",
            "name": "superFatherId",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "doc": "this id is built using the original familyId and the original pedigreeId of the mother",
            "name": "superMotherId",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "doc": "Each twin group is numbered, i.e. all members of a group of multiparous births receive the same number",
            "name": "twinGroup",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "doc": "A property of the twinning group but should be entered for all members",
            "name": "monozygotic",
            "type": [
                "null",
                {
                    "doc": "This defines a yes/no/unknown case",
                    "name": "TernaryOption",
                    "symbols": [
                        "yes",
                        "no",
                        "unknown"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "Adopted Status",
            "name": "adoptedStatus",
            "type": [
                "null",
                {
                    "doc": "adoptedin means adopted into the family\n    adoptedout means child belonged to the family and was adopted out",
                    "name": "AdoptedStatus",
                    "symbols": [
                        "notadopted",
                        "adoptedin",
                        "adoptedout"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "Life Status",
            "name": "lifeStatus",
            "type": [
                "null",
                {
                    "doc": "Life Status",
                    "name": "LifeStatus",
                    "symbols": [
                        "ALIVE",
                        "ABORTED",
                        "DECEASED",
                        "UNBORN",
                        "STILLBORN",
                        "MISCARRIAGE"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "The parents of this participant has a consanguineous relationship",
            "name": "consanguineousParents",
            "type": [
                "null",
                "TernaryOption"
            ]
        },
        {
            "doc": "Affection Status",
            "name": "affectionStatus",
            "type": [
                "null",
                {
                    "doc": "Affection Status",
                    "name": "AffectionStatus",
                    "symbols": [
                        "UNAFFECTED",
                        "AFFECTED",
                        "UNCERTAIN"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "Clinical Data (disorders). If the family member is unaffected as per affectionStatus then this list is empty",
            "name": "disorderList",
            "type": [
                "null",
                {
                    "items": {
                        "doc": "This is quite GEL specific. This is the way is stored in ModelCatalogue and PanelApp.\n    Currently all specific disease titles are assigned to a disease subgroup so really only specificDisease needs to be\n    completed but we add the others for generality",
                        "fields": [
                            {
                                "doc": "This is Level2 Title for this disorder",
                                "name": "diseaseGroup",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "This is Level3 Title for this disorder",
                                "name": "diseaseSubGroup",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "This is Level4 Title for this disorder",
                                "name": "specificDisease",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Age of onset in years",
                                "name": "ageOfOnset",
                                "type": [
                                    "null",
                                    "float"
                                ]
                            }
                        ],
                        "name": "Disorder",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Clinical Data (HPO terms)",
            "name": "hpoTermList",
            "type": [
                "null",
                {
                    "items": {
                        "doc": "This defines an HPO term and its modifiers (possibly multiple)\n    If HPO term presence is unknown we don't have a entry on the list",
                        "fields": [
                            {
                                "doc": "Identifier of the HPO term",
                                "name": "term",
                                "type": "string"
                            },
                            {
                                "doc": "This is whether the term is present in the participant (default is unknown) yes=term is present in participant,\n        no=term is not present",
                                "name": "termPresence",
                                "type": [
                                    "null",
                                    "TernaryOption"
                                ]
                            },
                            {
                                "doc": "hpoBuildNumber",
                                "name": "hpoBuildNumber",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Modifier associated with the HPO term",
                                "name": "modifiers",
                                "type": [
                                    "null",
                                    {
                                        "fields": [
                                            {
                                                "name": "laterality",
                                                "type": [
                                                    "null",
                                                    {
                                                        "name": "Laterality",
                                                        "symbols": [
                                                            "RIGHT",
                                                            "UNILATERAL",
                                                            "BILATERAL",
                                                            "LEFT"
                                                        ],
                                                        "type": "enum"
                                                    }
                                                ]
                                            },
                                            {
                                                "name": "progression",
                                                "type": [
                                                    "null",
                                                    {
                                                        "name": "Progression",
                                                        "symbols": [
                                                            "PROGRESSIVE",
                                                            "NONPROGRESSIVE"
                                                        ],
                                                        "type": "enum"
                                                    }
                                                ]
                                            },
                                            {
                                                "name": "severity",
                                                "type": [
                                                    "null",
                                                    {
                                                        "name": "Severity",
                                                        "symbols": [
                                                            "BORDERLINE",
                                                            "MILD",
                                                            "MODERATE",
                                                            "SEVERE",
                                                            "PROFOUND"
                                                        ],
                                                        "type": "enum"
                                                    }
                                                ]
                                            },
                                            {
                                                "name": "spatialPattern",
                                                "type": [
                                                    "null",
                                                    {
                                                        "name": "SpatialPattern",
                                                        "symbols": [
                                                            "DISTAL",
                                                            "GENERALIZED",
                                                            "LOCALIZED",
                                                            "PROXIMAL"
                                                        ],
                                                        "type": "enum"
                                                    }
                                                ]
                                            }
                                        ],
                                        "name": "HpoTermModifiers",
                                        "type": "record"
                                    }
                                ]
                            },
                            {
                                "doc": "Age of onset in months",
                                "name": "ageOfOnset",
                                "type": [
                                    "null",
                                    {
                                        "name": "AgeOfOnset",
                                        "symbols": [
                                            "EMBRYONAL_ONSET",
                                            "FETAL_ONSET",
                                            "NEONATAL_ONSET",
                                            "INFANTILE_ONSET",
                                            "CHILDHOOD_ONSET",
                                            "JUVENILE_ONSET",
                                            "YOUNG_ADULT_ONSET",
                                            "LATE_ONSET",
                                            "MIDDLE_AGE_ONSET"
                                        ],
                                        "type": "enum"
                                    }
                                ]
                            }
                        ],
                        "name": "HpoTerm",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Participant's ancestries, defined as Mother's/Father's Ethnic Origin and Chi-square test for goodness of fit of this sample to 1000 Genomes Phase 3 populations",
            "name": "ancestries",
            "type": [
                "null",
                {
                    "doc": "Ancestries, defined as Ethnic category(ies) and Chi-square test",
                    "fields": [
                        {
                            "doc": "Mother's Ethnic Origin",
                            "name": "mothersEthnicOrigin",
                            "type": [
                                "null",
                                {
                                    "doc": "This is the list of ethnicities in ONS16\n\n    * `D`:  Mixed: White and Black Caribbean\n    * `E`:  Mixed: White and Black African\n    * `F`:  Mixed: White and Asian\n    * `G`:  Mixed: Any other mixed background\n    * `A`:  White: British\n    * `B`:  White: Irish\n    * `C`:  White: Any other White background\n    * `L`:  Asian or Asian British: Any other Asian background\n    * `M`:  Black or Black British: Caribbean\n    * `N`:  Black or Black British: African\n    * `H`:  Asian or Asian British: Indian\n    * `J`:  Asian or Asian British: Pakistani\n    * `K`:  Asian or Asian British: Bangladeshi\n    * `P`:  Black or Black British: Any other Black background\n    * `S`:  Other Ethnic Groups: Any other ethnic group\n    * `R`:  Other Ethnic Groups: Chinese\n    * `Z`:  Not stated",
                                    "name": "EthnicCategory",
                                    "symbols": [
                                        "D",
                                        "E",
                                        "F",
                                        "G",
                                        "A",
                                        "B",
                                        "C",
                                        "L",
                                        "M",
                                        "N",
                                        "H",
                                        "J",
                                        "K",
                                        "P",
                                        "S",
                                        "R",
                                        "Z"
                                    ],
                                    "type": "enum"
                                }
                            ]
                        },
                        {
                            "doc": "Mother's Ethnic Origin Description",
                            "name": "mothersOtherRelevantAncestry",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Father's Ethnic Origin",
                            "name": "fathersEthnicOrigin",
                            "type": [
                                "null",
                                "EthnicCategory"
                            ]
                        },
                        {
                            "doc": "Father's Ethnic Origin Description",
                            "name": "fathersOtherRelevantAncestry",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Chi-square test for goodness of fit of this sample to 1000 Genomes Phase 3 populations",
                            "name": "chiSquare1KGenomesPhase3Pop",
                            "type": [
                                "null",
                                {
                                    "items": {
                                        "doc": "Chi-square test for goodness of fit of this sample to 1000 Genomes Phase 3 populations",
                                        "fields": [
                                            {
                                                "doc": "1K Super Population",
                                                "name": "kgSuperPopCategory",
                                                "type": {
                                                    "doc": "1K Genomes project super populations",
                                                    "name": "KgSuperPopCategory",
                                                    "symbols": [
                                                        "AFR",
                                                        "AMR",
                                                        "EAS",
                                                        "EUR",
                                                        "SAS"
                                                    ],
                                                    "type": "enum"
                                                }
                                            },
                                            {
                                                "doc": "1K Population",
                                                "name": "kgPopCategory",
                                                "type": [
                                                    "null",
                                                    {
                                                        "doc": "1K Genomes project populations",
                                                        "name": "KgPopCategory",
                                                        "symbols": [
                                                            "ACB",
                                                            "ASW",
                                                            "BEB",
                                                            "CDX",
                                                            "CEU",
                                                            "CHB",
                                                            "CHS",
                                                            "CLM",
                                                            "ESN",
                                                            "FIN",
                                                            "GBR",
                                                            "GIH",
                                                            "GWD",
                                                            "IBS",
                                                            "ITU",
                                                            "JPT",
                                                            "KHV",
                                                            "LWK",
                                                            "MSL",
                                                            "MXL",
                                                            "PEL",
                                                            "PJL",
                                                            "PUR",
                                                            "STU",
                                                            "TSI",
                                                            "YRI"
                                                        ],
                                                        "type": "enum"
                                                    }
                                                ]
                                            },
                                            {
                                                "doc": "Chi-square test for goodness of fit of this sample to this 1000 Genomes Phase 3 population",
                                                "name": "chiSquare",
                                                "type": "double"
                                            }
                                        ],
                                        "name": "ChiSquare1KGenomesPhase3Pop",
                                        "type": "record"
                                    },
                                    "type": "array"
                                }
                            ]
                        }
                    ],
                    "name": "Ancestries",
                    "type": "record"
                }
            ]
        },
        {
            "doc": "What has this participant consented to?\n        A participant that has been consented to the programme should also have sequence data associated with them; however\n        this needs to be programmatically checked",
            "name": "consentStatus",
            "type": [
                "null",
                {
                    "doc": "Consent Status",
                    "fields": [
                        {
                            "default": false,
                            "doc": "Is this individual consented to the programme?\n        It could simply be a family member that is not consented but for whom affection status is known",
                            "name": "programmeConsent",
                            "type": "boolean"
                        },
                        {
                            "default": false,
                            "doc": "Consent for feedback of primary findings?",
                            "name": "primaryFindingConsent",
                            "type": "boolean"
                        },
                        {
                            "default": false,
                            "doc": "Consent for secondary finding lookup",
                            "name": "secondaryFindingConsent",
                            "type": "boolean"
                        },
                        {
                            "default": false,
                            "doc": "Consent for carrier status check?",
                            "name": "carrierStatusConsent",
                            "type": "boolean"
                        }
                    ],
                    "name": "ConsentStatus",
                    "type": "record"
                }
            ]
        },
        {
            "doc": "This is an array containing all the samples that belong to this individual, e.g [\"LP00002255_GA4\"]",
            "name": "samples",
            "type": [
                "null",
                {
                    "items": {
                        "fields": [
                            {
                                "doc": "Sample Id (e.g, LP00012645_5GH))",
                                "name": "sampleId",
                                "type": "string"
                            },
                            {
                                "doc": "Lab Sample Id",
                                "name": "labSampleId",
                                "type": "int"
                            },
                            {
                                "doc": "Source",
                                "name": "source",
                                "type": [
                                    "null",
                                    {
                                        "doc": "The source of the sample",
                                        "name": "SampleSource",
                                        "symbols": [
                                            "TUMOUR",
                                            "BONE_MARROW_ASPIRATE_TUMOUR_SORTED_CELLS",
                                            "BONE_MARROW_ASPIRATE_TUMOUR_CELLS",
                                            "BLOOD",
                                            "SALIVA",
                                            "FIBROBLAST",
                                            "TISSUE"
                                        ],
                                        "type": "enum"
                                    }
                                ]
                            },
                            {
                                "doc": "Product",
                                "name": "product",
                                "type": [
                                    "null",
                                    {
                                        "name": "Product",
                                        "symbols": [
                                            "DNA",
                                            "RNA"
                                        ],
                                        "type": "enum"
                                    }
                                ]
                            },
                            {
                                "doc": "preparationMethod",
                                "name": "preparationMethod",
                                "type": [
                                    "null",
                                    {
                                        "name": "PreparationMethod",
                                        "symbols": [
                                            "EDTA",
                                            "ORAGENE",
                                            "FF",
                                            "FFPE",
                                            "CD128_SORTED_CELLS",
                                            "ASPIRATE"
                                        ],
                                        "type": "enum"
                                    }
                                ]
                            }
                        ],
                        "name": "Sample",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Inbreeding Coefficient Estimation",
            "name": "inbreedingCoefficient",
            "type": [
                "null",
                {
                    "doc": "Inbreeding coefficient",
                    "fields": [
                        {
                            "doc": "This is the sample id against which the coefficient was estimated",
                            "name": "sampleId",
                            "type": "string"
                        },
                        {
                            "doc": "Name of program used to calculate the coefficient",
                            "name": "program",
                            "type": "string"
                        },
                        {
                            "doc": "Version of the programme",
                            "name": "version",
                            "type": "string"
                        },
                        {
                            "doc": "Where various methods for estimation exist, which method was used.",
                            "name": "estimationMethod",
                            "type": "string"
                        },
                        {
                            "doc": "Inbreeding coefficient ideally a real number in [0,1]",
                            "name": "coefficient",
                            "type": "double"
                        },
                        {
                            "doc": "Standard error of the Inbreeding coefficient",
                            "name": "standardError",
                            "type": [
                                "null",
                                "double"
                            ]
                        }
                    ],
                    "name": "InbreedingCoefficient",
                    "type": "record"
                }
            ]
        },
        {
            "doc": "We could add a map here to store additional information for example URIs to images, ECGs, etc\n        Null by default",
            "name": "additionalInformation",
            "type": [
                "null",
                {
                    "type": "map",
                    "values": "string"
                }
            ]
        }
    ],
    "name": "PedigreeMember",
    "namespace": "org.gel.models.participant.avro",
    "type": "record"
}