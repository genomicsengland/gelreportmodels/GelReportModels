{
    "doc": "This defines a Cancer Participant",
    "fields": [
        {
            "doc": "Year of birth for the cancer participant",
            "name": "yearOfBirth",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "doc": "TODO",
            "name": "morphology",
            "type": [
                "null",
                {
                    "items": "string",
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Flag indicating if the participant is ready for analysis",
            "name": "readyForAnalysis",
            "type": "boolean"
        },
        {
            "doc": "What has this participant consented to?\n        A participant that has been consented to the programme should also have sequence data associated with them; however\n        this needs to be programmatically checked",
            "name": "consentStatus",
            "type": [
                "null",
                {
                    "doc": "Consent Status",
                    "fields": [
                        {
                            "default": false,
                            "doc": "Is this individual consented to the programme?\n        It could simply be a family member that is not consented but for whom affection status is known",
                            "name": "programmeConsent",
                            "type": "boolean"
                        },
                        {
                            "default": false,
                            "doc": "Consent for feedback of primary findings?",
                            "name": "primaryFindingConsent",
                            "type": "boolean"
                        },
                        {
                            "default": false,
                            "doc": "Consent for secondary finding lookup",
                            "name": "secondaryFindingConsent",
                            "type": "boolean"
                        },
                        {
                            "default": false,
                            "doc": "Consent for carrier status check?",
                            "name": "carrierStatusConsent",
                            "type": "boolean"
                        }
                    ],
                    "name": "ConsentStatus",
                    "type": "record"
                }
            ]
        },
        {
            "doc": "Center",
            "name": "center",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Individual identifier",
            "name": "individualId",
            "type": "string"
        },
        {
            "doc": "This should be an enumeration when it is well defined\n        blood, breast, prostate, colorectal, cll, aml, renal, ovarian, skin, lymphNode, bone, saliva //for individual - there could be more than I have listed here, in fact there definitely will.",
            "name": "primaryDiagnosisDisease",
            "type": [
                "null",
                {
                    "items": "string",
                    "type": "array"
                }
            ]
        },
        {
            "doc": "This should be an enumeration when it is well defined\n        blood, breast, prostate, colorectal, cll, aml, renal, ovarian, skin, lymphNode, bone, saliva //for individual - there could be more than I have listed here, in fact there definitely will.",
            "name": "primaryDiagnosisSubDisease",
            "type": [
                "null",
                {
                    "items": "string",
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Sex",
            "name": "sex",
            "type": {
                "doc": "Sex",
                "name": "Sex",
                "symbols": [
                    "MALE",
                    "FEMALE",
                    "UNKNOWN"
                ],
                "type": "enum"
            }
        },
        {
            "doc": "We could add a map here to store additional information for example URIs to images, ECGs, etc",
            "name": "additionalInformation",
            "type": [
                "null",
                {
                    "type": "map",
                    "values": "string"
                }
            ]
        },
        {
            "doc": "assigned ICD10 code",
            "name": "assignedICD10",
            "type": [
                "null",
                {
                    "items": "string",
                    "type": "array"
                }
            ]
        },
        {
            "doc": "List of tumour samples",
            "name": "tumourSamples",
            "type": {
                "items": {
                    "doc": "A tumour sample",
                    "fields": [
                        {
                            "doc": "Sample identifier (e.g, LP00012645_5GH))",
                            "name": "sampleId",
                            "type": "string"
                        },
                        {
                            "doc": "Lab sample identifier",
                            "name": "labSampleId",
                            "type": "int"
                        },
                        {
                            "doc": "LDP Code (Local Delivery Partner)",
                            "name": "LDPCode",
                            "type": "string"
                        },
                        {
                            "doc": "Identifier of each one of the tumours for a participant",
                            "name": "tumourId",
                            "type": "string"
                        },
                        {
                            "doc": "Genomics England programme phase",
                            "name": "programmePhase",
                            "type": [
                                "null",
                                {
                                    "name": "ProgrammePhase",
                                    "symbols": [
                                        "CRUK",
                                        "OXFORD",
                                        "CLL",
                                        "IIP",
                                        "MAIN",
                                        "EXPT"
                                    ],
                                    "type": "enum"
                                }
                            ]
                        },
                        {
                            "doc": "Disease type",
                            "name": "diseaseType",
                            "type": [
                                "null",
                                {
                                    "name": "diseaseType",
                                    "symbols": [
                                        "ADULT_GLIOMA",
                                        "BLADDER",
                                        "BREAST",
                                        "CARCINOMA_OF_UNKNOWN_PRIMARY",
                                        "CHILDHOOD",
                                        "COLORECTAL",
                                        "ENDOMETRIAL_CARCINOMA",
                                        "HAEMONC",
                                        "HEPATOPANCREATOBILIARY",
                                        "LUNG",
                                        "MALIGNANT_MELANOMA",
                                        "NASOPHARYNGEAL",
                                        "ORAL_OROPHARYNGEAL",
                                        "OVARIAN",
                                        "PROSTATE",
                                        "RENAL",
                                        "SARCOMA",
                                        "SINONASAL",
                                        "TESTICULAR_GERM_CELL_TUMOURS",
                                        "UPPER_GASTROINTESTINAL",
                                        "NON_HODGKINS_B_CELL_LYMPHOMA_LOW_MOD_GRADE",
                                        "CLASSICAL_HODGKINS",
                                        "NODULAR_LYMPHOCYTE_PREDOMINANT_HODGKINS",
                                        "T_CELL_LYMPHOMA"
                                    ],
                                    "type": "enum"
                                }
                            ]
                        },
                        {
                            "doc": "Disease subtype",
                            "name": "diseaseSubType",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "The time when the sample was recieved. In the format YYYY-MM-DDTHH:MM:SS+0000",
                            "name": "clinicalSampleDateTime",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Tumor type",
                            "name": "tumourType",
                            "type": [
                                "null",
                                {
                                    "name": "TumourType",
                                    "symbols": [
                                        "PRIMARY",
                                        "METASTATIC_RECURRENCE",
                                        "RECURRENCE_OF_PRIMARY_TUMOUR",
                                        "METASTASES"
                                    ],
                                    "type": "enum"
                                }
                            ]
                        },
                        {
                            "doc": "Tumour content",
                            "name": "tumourContent",
                            "type": [
                                "null",
                                {
                                    "name": "TumourContent",
                                    "symbols": [
                                        "High",
                                        "Medium",
                                        "Low"
                                    ],
                                    "type": "enum"
                                }
                            ]
                        },
                        {
                            "doc": "Source of the sample",
                            "name": "source",
                            "type": [
                                "null",
                                {
                                    "doc": "The source of the sample",
                                    "name": "SampleSource",
                                    "symbols": [
                                        "TUMOUR",
                                        "BONE_MARROW_ASPIRATE_TUMOUR_SORTED_CELLS",
                                        "BONE_MARROW_ASPIRATE_TUMOUR_CELLS",
                                        "BLOOD",
                                        "SALIVA",
                                        "FIBROBLAST",
                                        "TISSUE"
                                    ],
                                    "type": "enum"
                                }
                            ]
                        },
                        {
                            "doc": "The preparation method",
                            "name": "preparationMethod",
                            "type": [
                                "null",
                                {
                                    "name": "PreparationMethod",
                                    "symbols": [
                                        "EDTA",
                                        "ORAGENE",
                                        "FF",
                                        "FFPE",
                                        "CD128_SORTED_CELLS",
                                        "ASPIRATE"
                                    ],
                                    "type": "enum"
                                }
                            ]
                        },
                        {
                            "doc": "The tissue source",
                            "name": "tissueSource",
                            "type": [
                                "null",
                                {
                                    "name": "TissueSource",
                                    "symbols": [
                                        "BMA_TUMOUR_SORTED_CELLS",
                                        "CT_GUIDED_BIOPSY",
                                        "ENDOSCOPIC_BIOPSY",
                                        "ENDOSCOPIC_ULTRASOUND_GUIDED_BIOPSY",
                                        "ENDOSCOPIC_ULTRASOUND_GUIDED_FNA",
                                        "LAPAROSCOPIC_BIOPSY",
                                        "LAPAROSCOPIC_EXCISION",
                                        "MRI_GUIDED_BIOPSY",
                                        "NON_GUIDED_BIOPSY",
                                        "SURGICAL_RESECTION",
                                        "STEREOTACTICALLY_GUIDED_BIOPSY",
                                        "USS_GUIDED_BIOPSY",
                                        "NON_STANDARD_BIOPSY"
                                    ],
                                    "type": "enum"
                                }
                            ]
                        },
                        {
                            "doc": "Product of the sample",
                            "name": "product",
                            "type": [
                                "null",
                                {
                                    "name": "Product",
                                    "symbols": [
                                        "DNA",
                                        "RNA"
                                    ],
                                    "type": "enum"
                                }
                            ]
                        },
                        {
                            "doc": "Tumour morphology as defined by ICD (at least one morphology definition by either ICD, Snomed CT or Snomed RT must be provided)",
                            "name": "morphologyICD",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Tumour morphology as defined by Snomed CT (at least one morphology definition by either ICD, Snomed CT or Snomed RT must be provided)",
                            "name": "morphologySnomedCT",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Tumour morphology as defined by Snomed RT (at least one morphology definition by either ICD, Snomed CT or Snomed RT must be provided)",
                            "name": "morphologySnomedRT",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Tumour topography as defined by ICD (at least one topography definition by either ICD, Snomed CT or Snomed RT must be provided)",
                            "name": "topographyICD",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Tumour topography as defined by Snomed CT (at least one topography definition by either ICD, Snomed CT or Snomed RT must be provided)",
                            "name": "topographySnomedCT",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Tumour topography as defined by Snomed RT (at least one topography definition by either ICD, Snomed CT or Snomed RT must be provided)",
                            "name": "topographySnomedRT",
                            "type": [
                                "null",
                                "string"
                            ]
                        }
                    ],
                    "name": "TumourSample",
                    "type": "record"
                },
                "type": "array"
            }
        },
        {
            "doc": "List of germline samples",
            "name": "germlineSamples",
            "type": {
                "items": {
                    "doc": "A germline sample",
                    "fields": [
                        {
                            "doc": "Sample identifier (e.g, LP00012645_5GH))",
                            "name": "sampleId",
                            "type": "string"
                        },
                        {
                            "doc": "Lab sample identifier",
                            "name": "labSampleId",
                            "type": "int"
                        },
                        {
                            "doc": "LDP Code (Local Delivery Partner)",
                            "name": "LDPCode",
                            "type": "string"
                        },
                        {
                            "doc": "Source of the sample",
                            "name": "source",
                            "type": [
                                "null",
                                "SampleSource"
                            ]
                        },
                        {
                            "doc": "Product of the sample",
                            "name": "product",
                            "type": [
                                "null",
                                "Product"
                            ]
                        },
                        {
                            "doc": "Preparation method",
                            "name": "preparationMethod",
                            "type": [
                                "null",
                                "PreparationMethod"
                            ]
                        },
                        {
                            "doc": "Genomics England programme phase",
                            "name": "programmePhase",
                            "type": [
                                "null",
                                "ProgrammePhase"
                            ]
                        },
                        {
                            "doc": "The time when the sample was received. In the format YYYY-MM-DDTHH:MM:SS+0000",
                            "name": "clinicalSampleDateTime",
                            "type": [
                                "null",
                                "string"
                            ]
                        }
                    ],
                    "name": "GermlineSample",
                    "type": "record"
                },
                "type": "array"
            }
        },
        {
            "doc": "List of matched samples (i.e.: pairs tumour-germline)",
            "name": "matchedSamples",
            "type": {
                "items": {
                    "doc": "This defines a pair of germline and tumor, this pair should/must be analyzed together",
                    "fields": [
                        {
                            "doc": "Sample identifier (e.g, LP00012645_5GH)) for the germline",
                            "name": "germlineSampleId",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Sample identifier (e.g, LP00012643_7JS)) for the tumor",
                            "name": "tumourSampleId",
                            "type": [
                                "null",
                                "string"
                            ]
                        }
                    ],
                    "name": "MatchedSamples",
                    "type": "record"
                },
                "type": "array"
            }
        },
        {
            "doc": "Model version number",
            "name": "versionControl",
            "type": [
                "null",
                {
                    "fields": [
                        {
                            "default": "1.1.0",
                            "doc": "This is the version for the entire set of data models as referred to the Git release tag",
                            "name": "GitVersionControl",
                            "type": "string"
                        }
                    ],
                    "name": "VersionControl",
                    "type": "record"
                }
            ]
        }
    ],
    "name": "CancerParticipant",
    "namespace": "org.gel.models.participant.avro",
    "type": "record"
}