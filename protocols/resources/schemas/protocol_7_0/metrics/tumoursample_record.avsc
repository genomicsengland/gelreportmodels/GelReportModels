{
    "doc": "A tumour sample",
    "fields": [
        {
            "doc": "Sample identifier (e.g, LP00012645_5GH))",
            "name": "sampleId",
            "type": "string"
        },
        {
            "doc": "Lab sample identifier",
            "name": "labSampleId",
            "type": "int"
        },
        {
            "doc": "LDP Code (Local Delivery Partner)",
            "name": "LDPCode",
            "type": "string"
        },
        {
            "doc": "Identifier of each one of the tumours for a participant",
            "name": "tumourId",
            "type": "string"
        },
        {
            "doc": "Genomics England programme phase",
            "name": "programmePhase",
            "type": [
                "null",
                {
                    "name": "ProgrammePhase",
                    "symbols": [
                        "CRUK",
                        "OXFORD",
                        "CLL",
                        "IIP",
                        "MAIN",
                        "EXPT"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "Disease type",
            "name": "diseaseType",
            "type": [
                "null",
                {
                    "name": "diseaseType",
                    "symbols": [
                        "ADULT_GLIOMA",
                        "BLADDER",
                        "BREAST",
                        "CARCINOMA_OF_UNKNOWN_PRIMARY",
                        "CHILDHOOD",
                        "COLORECTAL",
                        "ENDOCRINE",
                        "ENDOMETRIAL_CARCINOMA",
                        "HAEMONC",
                        "HEPATOPANCREATOBILIARY",
                        "LUNG",
                        "MALIGNANT_MELANOMA",
                        "NASOPHARYNGEAL",
                        "ORAL_OROPHARYNGEAL",
                        "OVARIAN",
                        "PROSTATE",
                        "RENAL",
                        "SARCOMA",
                        "SINONASAL",
                        "TESTICULAR_GERM_CELL_TUMOURS",
                        "UPPER_GASTROINTESTINAL",
                        "OTHER",
                        "NON_HODGKINS_B_CELL_LYMPHOMA_LOW_MOD_GRADE",
                        "CLASSICAL_HODGKINS",
                        "NODULAR_LYMPHOCYTE_PREDOMINANT_HODGKINS",
                        "T_CELL_LYMPHOMA"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "Disease subtype",
            "name": "diseaseSubType",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "The time when the sample was recieved. In the format YYYY-MM-DDTHH:MM:SS+0000",
            "name": "clinicalSampleDateTime",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Tumor type",
            "name": "tumourType",
            "type": [
                "null",
                {
                    "name": "TumourType",
                    "symbols": [
                        "PRIMARY",
                        "METASTATIC_RECURRENCE",
                        "RECURRENCE_OF_PRIMARY_TUMOUR",
                        "METASTASES"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "Tumour content",
            "name": "tumourContent",
            "type": [
                "null",
                {
                    "name": "TumourContent",
                    "symbols": [
                        "High",
                        "Medium",
                        "Low"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "Source of the sample",
            "name": "source",
            "type": [
                "null",
                {
                    "doc": "The source of the sample",
                    "name": "SampleSource",
                    "symbols": [
                        "TUMOUR",
                        "BONE_MARROW_ASPIRATE_TUMOUR_SORTED_CELLS",
                        "BONE_MARROW_ASPIRATE_TUMOUR_CELLS",
                        "BLOOD",
                        "SALIVA",
                        "FIBROBLAST",
                        "TISSUE"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "The preparation method",
            "name": "preparationMethod",
            "type": [
                "null",
                {
                    "name": "PreparationMethod",
                    "symbols": [
                        "EDTA",
                        "ORAGENE",
                        "FF",
                        "FFPE",
                        "CD128_SORTED_CELLS",
                        "ASPIRATE"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "The tissue source",
            "name": "tissueSource",
            "type": [
                "null",
                {
                    "name": "TissueSource",
                    "symbols": [
                        "BMA_TUMOUR_SORTED_CELLS",
                        "CT_GUIDED_BIOPSY",
                        "ENDOSCOPIC_BIOPSY",
                        "ENDOSCOPIC_ULTRASOUND_GUIDED_BIOPSY",
                        "ENDOSCOPIC_ULTRASOUND_GUIDED_FNA",
                        "LAPAROSCOPIC_BIOPSY",
                        "LAPAROSCOPIC_EXCISION",
                        "MRI_GUIDED_BIOPSY",
                        "NON_GUIDED_BIOPSY",
                        "SURGICAL_RESECTION",
                        "STEREOTACTICALLY_GUIDED_BIOPSY",
                        "USS_GUIDED_BIOPSY",
                        "NON_STANDARD_BIOPSY",
                        "NOT_SPECIFIED"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "Product of the sample",
            "name": "product",
            "type": [
                "null",
                {
                    "name": "Product",
                    "symbols": [
                        "DNA",
                        "RNA"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "Tumour morphology as defined by ICD (at least one morphology definition by either ICD, Snomed CT or Snomed RT must be provided)",
            "name": "morphologyICD",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Tumour morphology as defined by Snomed CT (at least one morphology definition by either ICD, Snomed CT or Snomed RT must be provided)",
            "name": "morphologySnomedCT",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Tumour morphology as defined by Snomed RT (at least one morphology definition by either ICD, Snomed CT or Snomed RT must be provided)",
            "name": "morphologySnomedRT",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Tumour topography as defined by ICD (at least one topography definition by either ICD, Snomed CT or Snomed RT must be provided)",
            "name": "topographyICD",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Tumour topography as defined by Snomed CT (at least one topography definition by either ICD, Snomed CT or Snomed RT must be provided)",
            "name": "topographySnomedCT",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Tumour topography as defined by Snomed RT (at least one topography definition by either ICD, Snomed CT or Snomed RT must be provided)",
            "name": "topographySnomedRT",
            "type": [
                "null",
                "string"
            ]
        }
    ],
    "name": "TumourSample",
    "namespace": "org.gel.models.participant.avro",
    "type": "record"
}