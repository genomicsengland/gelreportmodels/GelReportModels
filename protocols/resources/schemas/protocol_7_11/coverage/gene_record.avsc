{
    "doc": "All coverage information about a given gene",
    "fields": [
        {
            "doc": "The coverage statistics for each transcript",
            "name": "trs",
            "type": {
                "items": {
                    "doc": "All coverage information about a given transcript",
                    "fields": [
                        {
                            "doc": "The transcript's Ensembl identifier",
                            "name": "id",
                            "type": "string"
                        },
                        {
                            "doc": "The coverage statistics across this transcript",
                            "name": "stats",
                            "type": {
                                "doc": "Represents a group of coverage statistics over a genomic region",
                                "fields": [
                                    {
                                        "doc": "The average depth of coverage",
                                        "name": "avg",
                                        "type": "float"
                                    },
                                    {
                                        "doc": "The depth of coverage standard deviation",
                                        "name": "sd",
                                        "type": "float"
                                    },
                                    {
                                        "doc": "The median depth of coverage",
                                        "name": "med",
                                        "type": "float"
                                    },
                                    {
                                        "doc": "The GC content",
                                        "name": "gc",
                                        "type": [
                                            "null",
                                            "float"
                                        ]
                                    },
                                    {
                                        "doc": "The 75th percentile of coverage distribution",
                                        "name": "pct75",
                                        "type": "float"
                                    },
                                    {
                                        "doc": "The 25th percentile of coverage distribution",
                                        "name": "pct25",
                                        "type": "float"
                                    },
                                    {
                                        "doc": "The number of bases in the region (this is nullable as it can be calculated from coordinates of the region)",
                                        "name": "bases",
                                        "type": [
                                            "null",
                                            "int"
                                        ]
                                    },
                                    {
                                        "doc": "The number of bases in the region with a coverage less than 15x (this is nullable as it can be calculated from coordinates of the region)",
                                        "name": "bases_lt_15x",
                                        "type": [
                                            "null",
                                            "int"
                                        ]
                                    },
                                    {
                                        "doc": "The number of bases in the region with a coverage greater than or equal 15x (this is nullable as it can be calculated from coordinates of the region)",
                                        "name": "bases_gte_15x",
                                        "type": [
                                            "null",
                                            "int"
                                        ]
                                    },
                                    {
                                        "doc": "The number of bases in the region with a coverage greater than or equal 30x (this is nullable as it can be calculated from coordinates of the region)",
                                        "name": "bases_gte_30x",
                                        "type": [
                                            "null",
                                            "int"
                                        ]
                                    },
                                    {
                                        "doc": "The number of bases in the region with a coverage greater than or equal 50x (this is nullable as it can be calculated from coordinates of the region)",
                                        "name": "bases_gte_50x",
                                        "type": [
                                            "null",
                                            "int"
                                        ]
                                    },
                                    {
                                        "doc": "The number of positions with a depth of coverage greater than or equal than 50x\n        (the original name is %>=50x, but this cannot be used as a property name in Avro)",
                                        "name": "gte50x",
                                        "type": "float"
                                    },
                                    {
                                        "doc": "The number of positions with a depth of coverage greater than or equal than 30x\n        (the original name is %>=30x, but this cannot be used as a property name in Avro)",
                                        "name": "gte30x",
                                        "type": "float"
                                    },
                                    {
                                        "doc": "The number of positions with a depth of coverage greater than or equal than 15x\n        (the original name is %>=15x, but this cannot be used as a property name in Avro)",
                                        "name": "gte15x",
                                        "type": "float"
                                    },
                                    {
                                        "doc": "The number of positions with a depth of coverage less than 15x\n        (the original name is %<15x, but this cannot be used as a property name in Avro)",
                                        "name": "lt15x",
                                        "type": "float"
                                    },
                                    {
                                        "doc": "The squared root sum of squares of the deviation from the mean",
                                        "name": "rmsd",
                                        "type": [
                                            "null",
                                            "float"
                                        ]
                                    }
                                ],
                                "name": "RegionStatistics",
                                "type": "record"
                            }
                        },
                        {
                            "doc": "The list of exons in this transcript",
                            "name": "exons",
                            "type": [
                                "null",
                                {
                                    "items": {
                                        "doc": "All coverage information about a given exon",
                                        "fields": [
                                            {
                                                "doc": "An exon unique name within each transcript (e.g.: exon1, exon2, etc.)",
                                                "name": "exon",
                                                "type": "string"
                                            },
                                            {
                                                "doc": "The exon start position. Inclusive, 0-based",
                                                "name": "s",
                                                "type": "int"
                                            },
                                            {
                                                "doc": "The exon padded start. Inclusive, 0-based\n        (i.e.: for a gene in the positive strand and a padding of 15bp, this will be the exon start positions minus 15 bp)\n        DEPRECATED",
                                                "name": "padded_s",
                                                "type": [
                                                    "null",
                                                    "int"
                                                ]
                                            },
                                            {
                                                "doc": "The exon end position. Exclusive, 0-based",
                                                "name": "e",
                                                "type": "int"
                                            },
                                            {
                                                "doc": "The exon padded end. Exclusive, 0-based\n        (i.e.: for a gene in the positive strand and a padding of 15bp, this will be the exon end positions plus 15 bp)\n        DEPRECATED",
                                                "name": "padded_e",
                                                "type": [
                                                    "null",
                                                    "int"
                                                ]
                                            },
                                            {
                                                "doc": "The exon length calculated without padding (this field is redundant)\n        DEPRECATED",
                                                "name": "l",
                                                "type": [
                                                    "null",
                                                    "int"
                                                ]
                                            },
                                            {
                                                "doc": "The list of coverage gaps",
                                                "name": "gaps",
                                                "type": {
                                                    "items": {
                                                        "doc": "A gap in coverage. A gap is a contiguous region under a certain depth of coverage.\n    There are two thresholds to define at analysis time:\n* The depth of coverage threshold under which a gap is considered\n* The number of consecutive positions under the depth of coverage threshold to call a gap\n\n    e.g.: we may consider a gap those regions of more than 5 consecutive bp under 15x",
                                                        "fields": [
                                                            {
                                                                "doc": "Gap start position. Inclusive, 0-based",
                                                                "name": "s",
                                                                "type": "int"
                                                            },
                                                            {
                                                                "doc": "Gap end position. Exclusive, 0-based",
                                                                "name": "e",
                                                                "type": "int"
                                                            },
                                                            {
                                                                "doc": "Gap length (this information is redundant as can be calculated from start and end)\n        DEPRECATED",
                                                                "name": "l",
                                                                "type": [
                                                                    "null",
                                                                    "int"
                                                                ]
                                                            }
                                                        ],
                                                        "name": "CoverageGap",
                                                        "type": "record"
                                                    },
                                                    "type": "array"
                                                }
                                            },
                                            {
                                                "doc": "The coverage statistics across this exon",
                                                "name": "stats",
                                                "type": "RegionStatistics"
                                            }
                                        ],
                                        "name": "Exon",
                                        "type": "record"
                                    },
                                    "type": "array"
                                }
                            ]
                        }
                    ],
                    "name": "Transcript",
                    "type": "record"
                },
                "type": "array"
            }
        },
        {
            "doc": "The coverage statistics for the union transcript",
            "name": "union_tr",
            "type": "Transcript"
        },
        {
            "doc": "The HGNC gene symbol",
            "name": "name",
            "type": "string"
        },
        {
            "doc": "The chromosome identifier (may have \"chr\" prefix or not depending on input data)",
            "name": "chr",
            "type": "string"
        }
    ],
    "name": "Gene",
    "namespace": "org.gel.models.coverage.avro",
    "type": "record"
}