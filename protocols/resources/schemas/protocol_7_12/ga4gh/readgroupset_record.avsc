{
    "fields": [
        {
            "doc": "The read group set ID.",
            "name": "id",
            "type": "string"
        },
        {
            "default": null,
            "doc": "The ID of the dataset this read group set belongs to.",
            "name": "datasetId",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "default": null,
            "doc": "The read group set name.",
            "name": "name",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "default": null,
            "doc": "Statistical data on reads in this read group set.",
            "name": "stats",
            "type": [
                "null",
                {
                    "fields": [
                        {
                            "default": null,
                            "doc": "The number of aligned reads.",
                            "name": "alignedReadCount",
                            "type": [
                                "null",
                                "long"
                            ]
                        },
                        {
                            "default": null,
                            "doc": "The number of unaligned reads.",
                            "name": "unalignedReadCount",
                            "type": [
                                "null",
                                "long"
                            ]
                        },
                        {
                            "default": null,
                            "doc": "The total number of bases.\n  This is equivalent to the sum of `alignedSequence.length` for all reads.",
                            "name": "baseCount",
                            "type": [
                                "null",
                                "long"
                            ]
                        }
                    ],
                    "name": "ReadStats",
                    "type": "record"
                }
            ]
        },
        {
            "default": [],
            "doc": "The read groups in this set.",
            "name": "readGroups",
            "type": {
                "items": {
                    "fields": [
                        {
                            "doc": "The read group ID.",
                            "name": "id",
                            "type": "string"
                        },
                        {
                            "default": null,
                            "doc": "The ID of the dataset this read group belongs to.",
                            "name": "datasetId",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "default": null,
                            "doc": "The read group name.",
                            "name": "name",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "default": null,
                            "doc": "The read group description.",
                            "name": "description",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "The sample this read group's data was generated from.",
                            "name": "sampleId",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "The experiment used to generate this read group.",
                            "name": "experiment",
                            "type": [
                                "null",
                                {
                                    "doc": "An experimental preparation of a `Sample`.",
                                    "fields": [
                                        {
                                            "doc": "The experiment UUID. This is globally unique.",
                                            "name": "id",
                                            "type": "string"
                                        },
                                        {
                                            "default": null,
                                            "doc": "The name of the experiment.",
                                            "name": "name",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "default": null,
                                            "doc": "A description of the experiment.",
                                            "name": "description",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "doc": "The time at which this record was created. \n  Format: ISO 8601, YYYY-MM-DDTHH:MM:SS.SSS (e.g. 2015-02-10T00:03:42.123Z)",
                                            "name": "recordCreateTime",
                                            "type": "string"
                                        },
                                        {
                                            "doc": "The time at which this record was last updated.\n  Format: ISO 8601, YYYY-MM-DDTHH:MM:SS.SSS (e.g. 2015-02-10T00:03:42.123Z)",
                                            "name": "recordUpdateTime",
                                            "type": "string"
                                        },
                                        {
                                            "default": null,
                                            "doc": "The time at which this experiment was performed.\n  Granularity here is variabel (e.g. date only).\n  Format: ISO 8601, YYYY-MM-DDTHH:MM:SS (e.g. 2015-02-10T00:03:42)",
                                            "name": "runTime",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "default": null,
                                            "doc": "The molecule examined in this experiment. (e.g. genomics DNA, total RNA)",
                                            "name": "molecule",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "default": null,
                                            "doc": "The experiment technique or strategy applied to the sample.\n  (e.g. whole genome sequencing, RNA-seq, RIP-seq)",
                                            "name": "strategy",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "default": null,
                                            "doc": "The method used to enrich the target. (e.g. immunoprecipitation, size\n  fractionation, MNase digestion)",
                                            "name": "selection",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "default": null,
                                            "doc": "The name of the library used as part of this experiment.",
                                            "name": "library",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "default": null,
                                            "doc": "The configuration of sequenced reads. (e.g. Single or Paired)",
                                            "name": "libraryLayout",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "doc": "The instrument model used as part of this experiment.\n    This maps to sequencing technology in BAM.",
                                            "name": "instrumentModel",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "default": null,
                                            "doc": "The data file generated by the instrument.\n  TODO: This isn't actually a file is it?\n  Should this be `instrumentData` instead?",
                                            "name": "instrumentDataFile",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "doc": "The sequencing center used as part of this experiment.",
                                            "name": "sequencingCenter",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "default": null,
                                            "doc": "The platform unit used as part of this experiment. This is a flowcell-barcode\n  or slide unique identifier.",
                                            "name": "platformUnit",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "default": {},
                                            "doc": "A map of additional experiment information.",
                                            "name": "info",
                                            "type": {
                                                "type": "map",
                                                "values": {
                                                    "items": "string",
                                                    "type": "array"
                                                }
                                            }
                                        }
                                    ],
                                    "name": "Experiment",
                                    "type": "record"
                                }
                            ]
                        },
                        {
                            "default": null,
                            "doc": "The predicted insert size of this read group.",
                            "name": "predictedInsertSize",
                            "type": [
                                "null",
                                "int"
                            ]
                        },
                        {
                            "default": null,
                            "doc": "The time at which this read group was created in milliseconds from the epoch.",
                            "name": "created",
                            "type": [
                                "null",
                                "long"
                            ]
                        },
                        {
                            "default": null,
                            "doc": "The time at which this read group was last updated in milliseconds\n  from the epoch.",
                            "name": "updated",
                            "type": [
                                "null",
                                "long"
                            ]
                        },
                        {
                            "default": null,
                            "doc": "Statistical data on reads in this read group.",
                            "name": "stats",
                            "type": [
                                "null",
                                "ReadStats"
                            ]
                        },
                        {
                            "default": [],
                            "doc": "The programs used to generate this read group.",
                            "name": "programs",
                            "type": {
                                "items": {
                                    "fields": [
                                        {
                                            "default": null,
                                            "doc": "The command line used to run this program.",
                                            "name": "commandLine",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "default": null,
                                            "doc": "The user specified ID of the program.",
                                            "name": "id",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "default": null,
                                            "doc": "The name of the program.",
                                            "name": "name",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "default": null,
                                            "doc": "The ID of the program run before this one.",
                                            "name": "prevProgramId",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "default": null,
                                            "doc": "The version of the program run.",
                                            "name": "version",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        }
                                    ],
                                    "name": "Program",
                                    "type": "record"
                                },
                                "type": "array"
                            }
                        },
                        {
                            "default": null,
                            "doc": "The reference set the reads in this read group are aligned to.\n  Required if there are any read alignments.",
                            "name": "referenceSetId",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "default": {},
                            "doc": "A map of additional read group information.",
                            "name": "info",
                            "type": {
                                "type": "map",
                                "values": {
                                    "items": "string",
                                    "type": "array"
                                }
                            }
                        }
                    ],
                    "name": "ReadGroup",
                    "type": "record"
                },
                "type": "array"
            }
        }
    ],
    "name": "ReadGroupSet",
    "namespace": "org.ga4gh.models",
    "type": "record"
}