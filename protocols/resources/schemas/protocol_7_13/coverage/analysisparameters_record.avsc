{
    "doc": "The configuration parameters used for the analysis",
    "fields": [
        {
            "doc": "The flag indicating if coding region analysis is to run",
            "name": "coding_region_stats_enabled",
            "type": "boolean"
        },
        {
            "doc": "The flag indicating if the exon stats are to be maintained in the results (these increase the size of the output sensibly).",
            "name": "exon_stats_enabled",
            "type": "boolean"
        },
        {
            "doc": "The flag indicating if whole genome analysis is to run",
            "name": "wg_stats_enabled",
            "type": "boolean"
        },
        {
            "doc": "The list of genes to analyse",
            "name": "gene_list",
            "type": [
                "null",
                {
                    "items": "string",
                    "type": "array"
                }
            ]
        },
        {
            "doc": "The panel name",
            "name": "panel",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "The panel version",
            "name": "panel_version",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "The PanelApp host",
            "name": "panelapp_host",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "The comma-separated list of PanelApp gene confidences that will be analysed\n        (i.e.: \"HighEvidence\" will imply that only genes with \"HighEvidence\" in a given panel will be analysed)",
            "name": "panelapp_gene_confidence",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "The comma-separated list of Havana transcript biotypes that are valid for analysis. Any transcript with a different biotype will not be analysed.\n        Biotypes are defined here http://vega.archive.ensembl.org/info/about/gene_and_transcript_types.html\n        Common configuration is: IG_C_gene,IG_D_gene,IG_J_gene,IG_V_gene,IG_V_gene,protein_coding,nonsense_mediated_decay,non_stop_decay,TR_C_gene,TR_D_gene,TR_J_gene,TR_V_gene",
            "name": "transcript_filtering_biotypes",
            "type": "string"
        },
        {
            "doc": "The comma-separated list of transcript flags that are valid for anlysis.\n        Common configuration: basic",
            "name": "transcript_filtering_flags",
            "type": "string"
        },
        {
            "doc": "The CellBase host",
            "name": "cellbase_host",
            "type": "string"
        },
        {
            "doc": "The CellBase version",
            "name": "cellbase_version",
            "type": "string"
        },
        {
            "doc": "The assembly used to fetch data from CellBase",
            "name": "grch37",
            "type": "string"
        },
        {
            "doc": "Cellbase configuration for the species (e.g.: hsapiens)",
            "name": "species",
            "type": "string"
        },
        {
            "doc": "The exon padding measured in base pairs",
            "name": "exon_padding",
            "type": "int"
        },
        {
            "doc": "The depth of coverage under which a gap is considered",
            "name": "gap_coverage_threshold",
            "type": "int"
        },
        {
            "doc": "The length of contiguous positions with low coverage to consider a gap",
            "name": "gap_length_threshold",
            "type": "int"
        },
        {
            "doc": "The input bigwig file",
            "name": "input_file",
            "type": "string"
        },
        {
            "doc": "The input configuration file",
            "name": "configuration_file",
            "type": "string"
        },
        {
            "doc": "The input BED file determining the whole genome regions to be analysed (this is usually used to discard regions containing ambiguous bases from the coverage analysis by providing a non N regions BED file)",
            "name": "wg_regions",
            "type": [
                "null",
                "string"
            ]
        }
    ],
    "name": "AnalysisParameters",
    "namespace": "org.gel.models.coverage.avro",
    "type": "record"
}