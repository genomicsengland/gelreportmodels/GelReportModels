{
    "doc": "A transaction having all necessary data to process it into the database",
    "fields": [
        {
            "doc": "The identifier of the transaction",
            "name": "id",
            "type": "string"
        },
        {
            "doc": "Timestamp of last transaction status modification",
            "name": "lastModified",
            "type": "string"
        },
        {
            "doc": "Transaction status",
            "name": "status",
            "type": {
                "doc": "The transaction status:\n\n* PENDING: a transaction in the queue pending to be processed\n* BLOCKED: a transaction already being processed\n* PROCESSING: a transaction being processed (normalised, lifted over and annotated)\n* PERSISTING: a transaction being persisted in the database\n* BLOCKED: a transaction already being processed\n* DONE: a transaction has been successfully processed\n* CANCELLING: a transaction is being rolled back\n* CANCELLED: a transaction has been rolled back\n* ERROR: erroneous transaction that cannot be processed, nor retried (this is caused by reported variants already in the database)\n* ROLLBACK_ERROR: a transaction failed to roll back (this may leave the database in an inconsistent state)\n* DELETED: a transaction has been deleted by a user (same effect as CANCELLED but user triggered)\n\n    The happy path is PENDING -> BLOCKED -> PROCESSING -> PERSISTING -> DONE",
                "name": "TransactionStatus",
                "symbols": [
                    "PENDING",
                    "BLOCKED",
                    "PROCESSING",
                    "PERSISTING",
                    "DONE",
                    "CANCELLING",
                    "CANCELLED",
                    "ERROR",
                    "ROLLBACK_ERROR",
                    "DELETED"
                ],
                "type": "enum"
            }
        },
        {
            "doc": "The data to be ingested in CVA compressed",
            "name": "compressedData",
            "type": [
                "null",
                "bytes"
            ]
        },
        {
            "doc": "A MD5 hash signature of the transaction used to discard identical requests.\nTo have a 50% chance of a collision by the birthday paradox we need 2**64 transactions",
            "name": "requestSignature",
            "type": "string"
        },
        {
            "doc": "Options to process the transaction",
            "name": "options",
            "type": {
                "type": "map",
                "values": "string"
            }
        },
        {
            "doc": "The number of milliseconds to process the transaction.",
            "name": "processingMilli",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "doc": "The details of a transaction",
            "name": "transactionDetails",
            "type": {
                "doc": "Details about the content of a transaction and some logs.",
                "fields": [
                    {
                        "doc": "The type of the transaction (e.g.: org.gel.models.cva.avro.InterpretedGenomeInject)",
                        "name": "type",
                        "type": "string"
                    },
                    {
                        "doc": "The number of elements contained in the transaction",
                        "name": "numberOfElements",
                        "type": "int"
                    },
                    {
                        "doc": "Metadata on the injection data",
                        "name": "metadata",
                        "type": {
                            "doc": "Metadata about injected data",
                            "fields": [
                                {
                                    "doc": "Report avro models version",
                                    "name": "reportModelVersion",
                                    "type": "string"
                                },
                                {
                                    "doc": "The entity identifier",
                                    "name": "id",
                                    "type": "string"
                                },
                                {
                                    "doc": "The entity version. This is a correlative number being the highest value the latest version.",
                                    "name": "version",
                                    "type": "int"
                                },
                                {
                                    "doc": "The case identifier",
                                    "name": "caseId",
                                    "type": "string"
                                },
                                {
                                    "doc": "The case version. This is a correlative number being the highest value the latest version.",
                                    "name": "caseVersion",
                                    "type": "int"
                                },
                                {
                                    "doc": "The family identifier",
                                    "name": "groupId",
                                    "type": "string"
                                },
                                {
                                    "doc": "The cohort identifier (the same family can have several cohorts)",
                                    "name": "cohortId",
                                    "type": "string"
                                },
                                {
                                    "doc": "The author of the ReportedVariant, either tiering, exomiser, a given cip (e.g.: omicia) or a given GMCs user name",
                                    "name": "author",
                                    "type": "string"
                                },
                                {
                                    "doc": "The author version of the ReportedVariant, either tiering, exomiser or a given cip. Only applicable for automated processes.",
                                    "name": "authorVersion",
                                    "type": [
                                        "null",
                                        "string"
                                    ]
                                },
                                {
                                    "doc": "The assembly to which the variants refer",
                                    "name": "assembly",
                                    "type": [
                                        "null",
                                        {
                                            "doc": "The reference genome assembly",
                                            "name": "Assembly",
                                            "namespace": "org.gel.models.report.avro",
                                            "symbols": [
                                                "GRCh38",
                                                "GRCh37"
                                            ],
                                            "type": "enum"
                                        }
                                    ]
                                },
                                {
                                    "doc": "The 100K Genomes program to which the reported variant belongs.",
                                    "name": "program",
                                    "type": {
                                        "doc": "The Genomics England program",
                                        "name": "Program",
                                        "namespace": "org.gel.models.report.avro",
                                        "symbols": [
                                            "cancer",
                                            "rare_disease"
                                        ],
                                        "type": "enum"
                                    }
                                },
                                {
                                    "doc": "The category to which the case belongs.",
                                    "name": "category",
                                    "type": {
                                        "name": "Category",
                                        "symbols": [
                                            "HundredK",
                                            "NGIS"
                                        ],
                                        "type": "enum"
                                    }
                                },
                                {
                                    "doc": "The creation date of the case (ISO-8601)",
                                    "name": "caseCreationDate",
                                    "type": [
                                        "null",
                                        "string"
                                    ]
                                },
                                {
                                    "doc": "The last modified date of the case (ISO-8601)",
                                    "name": "caseLastModifiedDate",
                                    "type": [
                                        "null",
                                        "string"
                                    ]
                                },
                                {
                                    "doc": "The organisation responsible for this payload (Pedigree and CancerParticipant will correspond to the case\nowner and the ClinicalReport will correspond to the case assignee)",
                                    "name": "organisation",
                                    "type": [
                                        "null",
                                        {
                                            "doc": "An organisation which may own or be assigned to a case",
                                            "fields": [
                                                {
                                                    "doc": "ODS code",
                                                    "name": "ods",
                                                    "type": "string"
                                                },
                                                {
                                                    "doc": "The GMC name",
                                                    "name": "gmc",
                                                    "type": [
                                                        "null",
                                                        "string"
                                                    ]
                                                },
                                                {
                                                    "doc": "The site name",
                                                    "name": "site",
                                                    "type": [
                                                        "null",
                                                        "string"
                                                    ]
                                                }
                                            ],
                                            "name": "Organisation",
                                            "type": "record"
                                        }
                                    ]
                                },
                                {
                                    "doc": "The NGIS organisation responsible for this payload",
                                    "name": "organisationNgis",
                                    "type": [
                                        "null",
                                        {
                                            "fields": [
                                                {
                                                    "doc": "Organisation Id",
                                                    "name": "organisationId",
                                                    "type": "string"
                                                },
                                                {
                                                    "doc": "Ods code",
                                                    "name": "organisationCode",
                                                    "type": "string"
                                                },
                                                {
                                                    "doc": "Organisation Name",
                                                    "name": "organisationName",
                                                    "type": "string"
                                                },
                                                {
                                                    "doc": "National Grouping (GLH) Id",
                                                    "name": "organisationNationalGroupingId",
                                                    "type": "string"
                                                },
                                                {
                                                    "doc": "National Grouping (GLH) Name",
                                                    "name": "organisationNationalGroupingName",
                                                    "type": "string"
                                                }
                                            ],
                                            "name": "OrganisationNgis",
                                            "namespace": "org.gel.models.participant.avro",
                                            "type": "record"
                                        }
                                    ]
                                },
                                {
                                    "doc": "Test unique identifier (only sent for NGIS cases)",
                                    "name": "referralTestId",
                                    "type": [
                                        "null",
                                        "string"
                                    ]
                                },
                                {
                                    "doc": "Referral unique identifier (only sent for NGIS cases)",
                                    "name": "referralId",
                                    "type": [
                                        "null",
                                        "string"
                                    ]
                                }
                            ],
                            "name": "InjectionMetadata",
                            "type": "record"
                        }
                    },
                    {
                        "doc": "Messages",
                        "name": "history",
                        "type": {
                            "items": {
                                "doc": "Keeps track of a transaction status change",
                                "fields": [
                                    {
                                        "doc": "The new transaction status",
                                        "name": "to",
                                        "type": "TransactionStatus"
                                    },
                                    {
                                        "doc": "A timestamp with the status change",
                                        "name": "timestamp",
                                        "type": "string"
                                    },
                                    {
                                        "doc": "A message",
                                        "name": "message",
                                        "type": [
                                            "null",
                                            "string"
                                        ]
                                    },
                                    {
                                        "doc": "An error message in case the transaction ingestion failed",
                                        "name": "errorMessage",
                                        "type": [
                                            "null",
                                            "string"
                                        ]
                                    },
                                    {
                                        "doc": "The stracktrace in case the transaction ingestion failed",
                                        "name": "stackTrace",
                                        "type": [
                                            "null",
                                            "string"
                                        ]
                                    },
                                    {
                                        "doc": "The CVA version that processed the transaction",
                                        "name": "cvaVersion",
                                        "type": [
                                            "null",
                                            "string"
                                        ]
                                    }
                                ],
                                "name": "TransactionStatusChange",
                                "type": "record"
                            },
                            "type": "array"
                        }
                    }
                ],
                "name": "TransactionDetails",
                "type": "record"
            }
        },
        {
            "doc": "The details of a request",
            "name": "requestDetails",
            "type": [
                "null",
                {
                    "doc": "Details about the transaction sender",
                    "fields": [
                        {
                            "doc": "IP address",
                            "name": "ip",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Hostname",
                            "name": "host",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Port",
                            "name": "port",
                            "type": [
                                "null",
                                "int"
                            ]
                        },
                        {
                            "doc": "User",
                            "name": "user",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "URI",
                            "name": "uri",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "URL",
                            "name": "url",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Authentication type",
                            "name": "authType",
                            "type": [
                                "null",
                                "string"
                            ]
                        }
                    ],
                    "name": "RequestDetails",
                    "type": "record"
                }
            ]
        }
    ],
    "name": "Transaction",
    "namespace": "org.gel.models.cva.avro",
    "type": "record"
}