{
    "doc": "Variant statistics for a set of variants.\n     The variants set can be contain a whole study, a cohort, a sample, a region, ...",
    "fields": [
        {
            "doc": "Number of variants in the variants set",
            "name": "numVariants",
            "type": "int"
        },
        {
            "doc": "Number of samples in the variants set",
            "name": "numSamples",
            "type": "int"
        },
        {
            "doc": "Number of variants with PASS filter",
            "name": "numPass",
            "type": "int"
        },
        {
            "doc": "TiTvRatio = num. transitions / num. transversions",
            "name": "tiTvRatio",
            "type": "float"
        },
        {
            "doc": "Mean Quality for all the variants with quality",
            "name": "meanQuality",
            "type": "float"
        },
        {
            "doc": "Standard Deviation of the quality",
            "name": "stdDevQuality",
            "type": "float"
        },
        {
            "default": [],
            "doc": "array of elements to classify variants according to their 'rarity'\n         Typical frequency ranges:\n          - very rare     -> from 0 to 0.001\n          - rare          -> from 0.001 to 0.005\n          - low frequency -> from 0.005 to 0.05\n          - common        -> from 0.05",
            "name": "numRareVariants",
            "type": {
                "items": {
                    "doc": "Counts the number of variants within a certain frequency range.",
                    "fields": [
                        {
                            "doc": "Inclusive frequency range start",
                            "name": "startFrequency",
                            "type": "float"
                        },
                        {
                            "doc": "Exclusive frequency range end",
                            "name": "endFrequency",
                            "type": "float"
                        },
                        {
                            "doc": "Number of variants with this frequency",
                            "name": "count",
                            "type": "int"
                        }
                    ],
                    "name": "VariantsByFrequency",
                    "type": "record"
                },
                "type": "array"
            }
        },
        {
            "default": {},
            "doc": "Variants count group by type. e.g. SNP, INDEL, MNP, SNV, ...",
            "name": "variantTypeCounts",
            "type": {
                "type": "map",
                "values": "int"
            }
        },
        {
            "default": {},
            "doc": "Variants count group by biotype. e.g. protein-coding, miRNA, lncRNA, ...",
            "name": "variantBiotypeCounts",
            "type": {
                "type": "map",
                "values": "int"
            }
        },
        {
            "default": {},
            "doc": "Variants count group by consequence type. e.g. synonymous_variant, missense_variant, stop_lost, ...",
            "name": "consequenceTypesCounts",
            "type": {
                "type": "map",
                "values": "int"
            }
        },
        {
            "default": {},
            "doc": "Statistics per chromosome.",
            "name": "chromosomeStats",
            "type": {
                "type": "map",
                "values": {
                    "fields": [
                        {
                            "doc": "Number of variants within this chromosome",
                            "name": "count",
                            "type": "int"
                        },
                        {
                            "doc": "Total density of variants within the chromosome. counts / chromosome.length",
                            "name": "density",
                            "type": "float"
                        }
                    ],
                    "name": "ChromosomeStats",
                    "type": "record"
                }
            }
        }
    ],
    "name": "VariantSetStats",
    "namespace": "org.opencb.biodata.models.variant.metadata",
    "type": "record"
}