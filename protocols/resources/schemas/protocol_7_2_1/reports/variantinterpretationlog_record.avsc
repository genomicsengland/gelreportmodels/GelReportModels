{
    "fields": [
        {
            "name": "variantCoordinates",
            "type": {
                "doc": "The variant coordinates representing uniquely a small variant.\n    No multi-allelic variant supported, alternate only represents one alternate allele.",
                "fields": [
                    {
                        "doc": "Chromosome",
                        "name": "chromosome",
                        "type": "string"
                    },
                    {
                        "doc": "Genomic position",
                        "name": "position",
                        "type": "int"
                    },
                    {
                        "doc": "The reference bases.",
                        "name": "reference",
                        "type": "string"
                    },
                    {
                        "doc": "The alternate bases",
                        "name": "alternate",
                        "type": "string"
                    },
                    {
                        "doc": "The assembly to which this variant corresponds",
                        "name": "assembly",
                        "type": {
                            "doc": "The reference genome assembly",
                            "name": "Assembly",
                            "symbols": [
                                "GRCh38",
                                "GRCh37"
                            ],
                            "type": "enum"
                        }
                    }
                ],
                "name": "VariantCoordinates",
                "type": "record"
            }
        },
        {
            "name": "user",
            "type": {
                "fields": [
                    {
                        "name": "username",
                        "type": "string"
                    },
                    {
                        "name": "role",
                        "type": "string"
                    },
                    {
                        "name": "email",
                        "type": "string"
                    },
                    {
                        "name": "groups",
                        "type": {
                            "items": "string",
                            "type": "array"
                        }
                    }
                ],
                "name": "User",
                "type": "record"
            }
        },
        {
            "name": "timestamp",
            "type": "string"
        },
        {
            "name": "familyId",
            "type": "string"
        },
        {
            "name": "caseId",
            "type": "string"
        },
        {
            "name": "variantValidation",
            "type": [
                "null",
                {
                    "fields": [
                        {
                            "name": "validationTechnology",
                            "type": "string"
                        },
                        {
                            "name": "validationResult",
                            "type": {
                                "name": "ValidationResult",
                                "symbols": [
                                    "NotPerformed",
                                    "Confirmed",
                                    "NotConfirmed"
                                ],
                                "type": "enum"
                            }
                        }
                    ],
                    "name": "VariantValidation",
                    "type": "record"
                }
            ]
        },
        {
            "name": "comments",
            "type": [
                "null",
                {
                    "items": "string",
                    "type": "array"
                }
            ]
        },
        {
            "name": "variantClassification",
            "type": {
                "doc": "Variant classification based on guidlines, AMP and ACMG are supported",
                "fields": [
                    {
                        "name": "acmgVariantClassification",
                        "type": [
                            "null",
                            {
                                "doc": "Full record for the ACMG variant clasiffication, including all selectedd evidences and the final classification.",
                                "fields": [
                                    {
                                        "name": "acmgEvidences",
                                        "type": {
                                            "items": {
                                                "doc": "AcmgEvidence. This should be buit for each one of the evidences assing to a variants following the ACMG guidelines.\n    An AcmgEvidence, should map with one of the criteria defined, i.e, PVS1, BA1, PM1...",
                                                "fields": [
                                                    {
                                                        "doc": "Evidence category as defined in ACMG guidelines",
                                                        "name": "category",
                                                        "type": {
                                                            "doc": "Each ACMG criterion is classified in one of these categories",
                                                            "name": "AcmgEvidenceCategory",
                                                            "symbols": [
                                                                "population_data",
                                                                "computational_and_predictive_data",
                                                                "functional_data",
                                                                "segregation_data",
                                                                "de_novo_data",
                                                                "allelic_data",
                                                                "other_database",
                                                                "other_data"
                                                            ],
                                                            "type": "enum"
                                                        }
                                                    },
                                                    {
                                                        "doc": "Evidence type: bening or pathogenic",
                                                        "name": "type",
                                                        "type": {
                                                            "doc": "Each ACMG cirterion will be classifed as bening or pathogenic",
                                                            "name": "AcmgEvidenceType",
                                                            "symbols": [
                                                                "bening",
                                                                "pathogenic"
                                                            ],
                                                            "type": "enum"
                                                        }
                                                    },
                                                    {
                                                        "doc": "Weight categories as described in ACMG guideline",
                                                        "name": "weight",
                                                        "type": {
                                                            "doc": "Each ACMG criterion is weighted using the following terms:\n\n* `stand_alone`: `A`, stand-alone applied for benign variant critieria `(BA1)`\n* `supporting`: `P`, supporting applied for benign variant critieria `(BP1-6)` and pathogenic variant criteria `(PP1-5)`\n* `moderate`: `M`, moderate applied for pathogenic variant critieria (PM1-6)\n* `strong`: `S`, strong applied for pathogenic variant critieria (PS1-4)\n* `very_strong`: `S`, Very Stong applied for pathogenic variant critieria (PVS1)",
                                                            "name": "AcmgEvidenceWeight",
                                                            "symbols": [
                                                                "stand_alone",
                                                                "supporting",
                                                                "moderate",
                                                                "strong",
                                                                "very_strong"
                                                            ],
                                                            "type": "enum"
                                                        }
                                                    },
                                                    {
                                                        "doc": "modifier of the strength, together define each creteria, i.e the 2 in PM2",
                                                        "name": "modifier",
                                                        "type": "int"
                                                    },
                                                    {
                                                        "doc": "Description of the evidence",
                                                        "name": "description",
                                                        "type": [
                                                            "null",
                                                            "string"
                                                        ]
                                                    }
                                                ],
                                                "name": "AcmgEvidence",
                                                "type": "record"
                                            },
                                            "type": "array"
                                        }
                                    },
                                    {
                                        "name": "clinicalSignificance",
                                        "type": {
                                            "name": "ClinicalSignificance",
                                            "symbols": [
                                                "benign",
                                                "likely_benign",
                                                "likely_pathogenic",
                                                "pathogenic",
                                                "uncertain_significance"
                                            ],
                                            "type": "enum"
                                        }
                                    },
                                    {
                                        "name": "assessment",
                                        "type": [
                                            "null",
                                            "string"
                                        ]
                                    }
                                ],
                                "name": "AcmgVariantClassification",
                                "type": "record"
                            }
                        ]
                    },
                    {
                        "name": "ampVariantClassification",
                        "type": [
                            "null",
                            {
                                "doc": "Full Variant classification acording to AMP guideline, including all supporting evidences and the final\n    assessment",
                                "fields": [
                                    {
                                        "doc": "List of AMP evidences",
                                        "name": "ampEvidences",
                                        "type": {
                                            "items": {
                                                "doc": "Evidences as defined in AMP guidelines, they are composed by a evidence type (first column in the evidence table of\n    the guidlines) and a assessment of the evicence, this last one will define the streght of the evidence, supporting\n    the variant to be classified as TierI-IV",
                                                "fields": [
                                                    {
                                                        "doc": "AMP evidence type according to Guidlines, i.e germline_database_presence",
                                                        "name": "type",
                                                        "type": {
                                                            "doc": "Type of evidence in tge AMP guideline",
                                                            "name": "AmpEvidenceType",
                                                            "symbols": [
                                                                "mutation_type",
                                                                "therapies",
                                                                "variant_frequencies",
                                                                "potential_germline",
                                                                "population_database_presence",
                                                                "germline_database_presence",
                                                                "somatic_database_presence",
                                                                "impact_predictive_software",
                                                                "pathway_involvement",
                                                                "publications"
                                                            ],
                                                            "type": "enum"
                                                        }
                                                    },
                                                    {
                                                        "doc": "Assessment for AMP evidence, i.e Present in ClinVar",
                                                        "name": "evidenceAssessment",
                                                        "type": "string"
                                                    }
                                                ],
                                                "name": "AmpEvidence",
                                                "type": "record"
                                            },
                                            "type": "array"
                                        }
                                    },
                                    {
                                        "doc": "Final Clasification taken in account the evidences",
                                        "name": "ampTier",
                                        "type": {
                                            "doc": "AMP tier:\n* `TierI`: Variants of Strong Clinical Significance\n* `TierII`: Variants of Potential Clinical Significance\n* `TierIII`: Variants of Unknown Clinical Significance\n* `TierIV`: Benign or Likely Benign Variants",
                                            "name": "AmpTier",
                                            "symbols": [
                                                "tierI",
                                                "tierII",
                                                "tierIII",
                                                "tierIV"
                                            ],
                                            "type": "enum"
                                        }
                                    },
                                    {
                                        "doc": "Clinical or Experimental evicence",
                                        "name": "ampClincialOrExperimentalEvidence",
                                        "type": [
                                            "null",
                                            {
                                                "items": {
                                                    "doc": "Amp Clinical or Experimental Evidence, the level will define the overal clasification of the variant together with\n    the tiering.",
                                                    "fields": [
                                                        {
                                                            "doc": "As denined in AMP guidelines: therapeutic, diagnosis or prognosis",
                                                            "name": "category",
                                                            "type": {
                                                                "doc": "Categories of Clinical and/or Experimental Evidence as defined in AMP guidelines",
                                                                "name": "AmpClinicalOrExperimentalEvidenceCategory",
                                                                "symbols": [
                                                                    "therapeutic",
                                                                    "diagnosis",
                                                                    "prognosis"
                                                                ],
                                                                "type": "enum"
                                                            }
                                                        },
                                                        {
                                                            "doc": "As denined in AMP guidelines: levelA, levelB, levelC, levelD",
                                                            "name": "level",
                                                            "type": {
                                                                "doc": "Levels for categories of Clinical and/or Experimental Evidence as defined in AMP guidelines",
                                                                "name": "AmpClinicalOrExperimentalEvidenceLevel",
                                                                "symbols": [
                                                                    "levelA",
                                                                    "levelB",
                                                                    "levelC",
                                                                    "levelD"
                                                                ],
                                                                "type": "enum"
                                                            }
                                                        },
                                                        {
                                                            "doc": "Description of the evidence",
                                                            "name": "description",
                                                            "type": [
                                                                "null",
                                                                "string"
                                                            ]
                                                        }
                                                    ],
                                                    "name": "AmpClincialOrExperimentalEvidence",
                                                    "type": "record"
                                                },
                                                "type": "array"
                                            }
                                        ]
                                    },
                                    {
                                        "doc": "Final Assessment",
                                        "name": "assessment",
                                        "type": [
                                            "null",
                                            "string"
                                        ]
                                    }
                                ],
                                "name": "AmpVariantClassification",
                                "type": "record"
                            }
                        ]
                    }
                ],
                "name": "GuidelineBasedVariantClassification",
                "type": "record"
            }
        },
        {
            "name": "VariantValidation",
            "type": [
                "null",
                "VariantValidation"
            ]
        },
        {
            "name": "Artifact",
            "type": [
                "null",
                "boolean"
            ]
        },
        {
            "name": "decisionSupportSystemFilters",
            "type": [
                "null",
                {
                    "type": "map",
                    "values": "string"
                }
            ]
        }
    ],
    "name": "VariantInterpretationLog",
    "namespace": "org.gel.models.report.avro",
    "type": "record"
}