{
    "doc": "A germline sample",
    "fields": [
        {
            "doc": "Sample identifier (e.g, LP00012645_5GH))",
            "name": "sampleId",
            "type": "string"
        },
        {
            "doc": "Lab sample identifier",
            "name": "labSampleId",
            "type": "int"
        },
        {
            "doc": "LDP Code (Local Delivery Partner)",
            "name": "LDPCode",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Source of the sample",
            "name": "source",
            "type": [
                "null",
                {
                    "doc": "The source of the sample\nNOTE: IN GMS, BONE_MARROW_ASPIRATE_TUMOUR_CELLS and BONE_MARROW_ASPIRATE_TUMOUR_SORTED_CELLS are deprecated as they have been separated into their respective biotypes",
                    "name": "SampleSource",
                    "symbols": [
                        "AMNIOTIC_FLUID",
                        "BLOOD",
                        "BONE_MARROW",
                        "BONE_MARROW_ASPIRATE_TUMOUR_CELLS",
                        "BONE_MARROW_ASPIRATE_TUMOUR_SORTED_CELLS",
                        "BUCCAL_SWAB",
                        "CHORIONIC_VILLUS_SAMPLE",
                        "FIBROBLAST",
                        "FLUID",
                        "FRESH_TISSUE_IN_CULTURE_MEDIUM",
                        "OTHER",
                        "SALIVA",
                        "TISSUE",
                        "TUMOUR",
                        "URINE"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "Product of the sample",
            "name": "product",
            "type": [
                "null",
                {
                    "name": "Product",
                    "symbols": [
                        "DNA",
                        "RNA"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "Preparation method\nNOTE: In GMS, this has been deprecated in favour of Method and storageMedium",
            "name": "preparationMethod",
            "type": [
                "null",
                {
                    "doc": "In 100K, preparation Method of sample\nNOTE: In GMS, this field is deprecated in favour of StorageMedium and Method",
                    "name": "PreparationMethod",
                    "symbols": [
                        "ASPIRATE",
                        "CD128_SORTED_CELLS",
                        "CD138_SORTED_CELLS",
                        "EDTA",
                        "FF",
                        "FFPE",
                        "LI_HEP",
                        "ORAGENE"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "Genomics England programme phase",
            "name": "programmePhase",
            "type": [
                "null",
                {
                    "name": "ProgrammePhase",
                    "symbols": [
                        "CRUK",
                        "OXFORD",
                        "CLL",
                        "IIP",
                        "MAIN",
                        "EXPT"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "The time when the sample was received. In the format YYYY-MM-DDTHH:MM:SS+0000",
            "name": "clinicalSampleDateTime",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "name": "participantId",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Participant UId of the sample",
            "name": "participantUid",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "name": "sampleUid",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "name": "maskedPid",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "In GMS, this is how the sample was extracted from the participant",
            "name": "method",
            "type": [
                "null",
                {
                    "doc": "In GMS, Method is defined as how the sample was taken directly from the patient",
                    "name": "Method",
                    "symbols": [
                        "ASPIRATE",
                        "BIOPSY",
                        "NOT_APPLICABLE",
                        "RESECTION",
                        "SORTED_OTHER",
                        "UNKNOWN",
                        "UNSORTED",
                        "CD138_SORTED"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "In GMS, this is what solvent/medium the sample was stored in",
            "name": "storageMedium",
            "type": [
                "null",
                {
                    "doc": "In GMS, storage medium of sample",
                    "name": "StorageMedium",
                    "symbols": [
                        "EDTA",
                        "FF",
                        "LI_HEP",
                        "ORAGENE",
                        "FFPE"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "In GMS, this is the sampleType as entered by the clinician in TOMs",
            "name": "sampleType",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "In GMS, this is the sampleState as entered by the clinician in TOMs",
            "name": "sampleState",
            "type": [
                "null",
                "string"
            ]
        }
    ],
    "name": "GermlineSample",
    "namespace": "org.gel.models.participant.avro",
    "type": "record"
}