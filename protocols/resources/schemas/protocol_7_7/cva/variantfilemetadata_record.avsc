{
    "fields": [
        {
            "doc": "File id. Will match with the {@link org.opencb.biodata.models.variant.avro.FileEntry#getFileId}",
            "name": "id",
            "type": "string"
        },
        {
            "default": null,
            "doc": "Path to the original file",
            "name": "path",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "default": [],
            "doc": "Ordered list of sample ids contained in the file",
            "name": "sampleIds",
            "type": {
                "items": "string",
                "type": "array"
            }
        },
        {
            "default": null,
            "doc": "Global statistics calculated for this file",
            "name": "stats",
            "type": [
                "null",
                {
                    "doc": "Variant statistics for a set of variants.\n     The variants set can be contain a whole study, a cohort, a sample, a region, ...",
                    "fields": [
                        {
                            "doc": "Number of variants in the variants set",
                            "name": "numVariants",
                            "type": "int"
                        },
                        {
                            "doc": "Number of samples in the variants set",
                            "name": "numSamples",
                            "type": "int"
                        },
                        {
                            "doc": "Number of variants with PASS filter",
                            "name": "numPass",
                            "type": "int"
                        },
                        {
                            "doc": "TiTvRatio = num. transitions / num. transversions",
                            "name": "tiTvRatio",
                            "type": "float"
                        },
                        {
                            "doc": "Mean Quality for all the variants with quality",
                            "name": "meanQuality",
                            "type": "float"
                        },
                        {
                            "doc": "Standard Deviation of the quality",
                            "name": "stdDevQuality",
                            "type": "float"
                        },
                        {
                            "default": [],
                            "doc": "array of elements to classify variants according to their 'rarity'\n         Typical frequency ranges:\n          - very rare     -> from 0 to 0.001\n          - rare          -> from 0.001 to 0.005\n          - low frequency -> from 0.005 to 0.05\n          - common        -> from 0.05",
                            "name": "numRareVariants",
                            "type": {
                                "items": {
                                    "doc": "Counts the number of variants within a certain frequency range.",
                                    "fields": [
                                        {
                                            "doc": "Inclusive frequency range start",
                                            "name": "startFrequency",
                                            "type": "float"
                                        },
                                        {
                                            "doc": "Exclusive frequency range end",
                                            "name": "endFrequency",
                                            "type": "float"
                                        },
                                        {
                                            "doc": "Number of variants with this frequency",
                                            "name": "count",
                                            "type": "int"
                                        }
                                    ],
                                    "name": "VariantsByFrequency",
                                    "type": "record"
                                },
                                "type": "array"
                            }
                        },
                        {
                            "default": {},
                            "doc": "Variants count group by type. e.g. SNP, INDEL, MNP, SNV, ...",
                            "name": "variantTypeCounts",
                            "type": {
                                "type": "map",
                                "values": "int"
                            }
                        },
                        {
                            "default": {},
                            "doc": "Variants count group by biotype. e.g. protein-coding, miRNA, lncRNA, ...",
                            "name": "variantBiotypeCounts",
                            "type": {
                                "type": "map",
                                "values": "int"
                            }
                        },
                        {
                            "default": {},
                            "doc": "Variants count group by consequence type. e.g. synonymous_variant, missense_variant, stop_lost, ...",
                            "name": "consequenceTypesCounts",
                            "type": {
                                "type": "map",
                                "values": "int"
                            }
                        },
                        {
                            "default": {},
                            "doc": "Statistics per chromosome.",
                            "name": "chromosomeStats",
                            "type": {
                                "type": "map",
                                "values": {
                                    "fields": [
                                        {
                                            "doc": "Number of variants within this chromosome",
                                            "name": "count",
                                            "type": "int"
                                        },
                                        {
                                            "doc": "Total density of variants within the chromosome. counts / chromosome.length",
                                            "name": "density",
                                            "type": "float"
                                        }
                                    ],
                                    "name": "ChromosomeStats",
                                    "type": "record"
                                }
                            }
                        }
                    ],
                    "name": "VariantSetStats",
                    "type": "record"
                }
            ]
        },
        {
            "default": null,
            "doc": "The Variant File Header",
            "name": "header",
            "type": [
                "null",
                {
                    "doc": "Variant File Header. Contains simple and complex metadata lines describing the content of the file.\n    This header matches with the VCF header.\n    A header may have multiple Simple or Complex lines with the same key",
                    "fields": [
                        {
                            "name": "version",
                            "type": "string"
                        },
                        {
                            "default": [],
                            "doc": "complex lines, e.g. INFO=<ID=NS,Number=1,Type=Integer,Description=\"Number of samples with data\">",
                            "name": "complexLines",
                            "type": {
                                "items": {
                                    "fields": [
                                        {
                                            "doc": "Key of group of the Complex Header Line, e.g. INFO, FORMAT, FILTER, ALT, ...",
                                            "name": "key",
                                            "type": "string"
                                        },
                                        {
                                            "doc": "ID or Name of the line",
                                            "name": "id",
                                            "type": "string"
                                        },
                                        {
                                            "default": null,
                                            "doc": "The description",
                                            "name": "description",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "default": null,
                                            "doc": "Arity of the values associated with this metadata line.\n        Only present if the metadata line describes data fields, i.e. key == INFO or FORMAT\n        Accepted values:\n          - <Integer>: The field has always this number of values.\n          - A: The field has one value per alternate allele.\n          - R: The field has one value for each possible allele, including the reference.\n          - G: The field has one value for each possible genotype\n          - .: The number of possible values varies, is unknown or unbounded.",
                                            "name": "number",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "default": null,
                                            "doc": "Type of the values associated with this metadata line.\n        Only present if the metadata line describes data fields, i.e. key == INFO or FORMAT\n        Accepted values:\n          - Integer\n          - Float\n          - String\n          - Character\n          - Flag",
                                            "name": "type",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "default": {},
                                            "doc": "Other optional fields",
                                            "name": "genericFields",
                                            "type": {
                                                "type": "map",
                                                "values": "string"
                                            }
                                        }
                                    ],
                                    "name": "VariantFileHeaderComplexLine",
                                    "type": "record"
                                },
                                "type": "array"
                            }
                        },
                        {
                            "default": [],
                            "doc": "simple lines, e.g. fileDate=20090805",
                            "name": "simpleLines",
                            "type": {
                                "items": {
                                    "fields": [
                                        {
                                            "doc": "Key of group of the Simple Header Line, e.g. source, assembly, pedigreeDB, ...",
                                            "name": "key",
                                            "type": "string"
                                        },
                                        {
                                            "doc": "Value",
                                            "name": "value",
                                            "type": "string"
                                        }
                                    ],
                                    "name": "VariantFileHeaderSimpleLine",
                                    "type": "record"
                                },
                                "type": "array"
                            }
                        }
                    ],
                    "name": "VariantFileHeader",
                    "type": "record"
                }
            ]
        },
        {
            "default": {},
            "doc": "Other user defined attributes related with the file",
            "name": "attributes",
            "type": {
                "type": "map",
                "values": "string"
            }
        }
    ],
    "name": "VariantFileMetadata",
    "namespace": "org.opencb.biodata.models.variant.metadata",
    "type": "record"
}