{
    "fields": [
        {
            "doc": "Test UID",
            "name": "referralTestId",
            "type": "string"
        },
        {
            "doc": "The date of which the referralTest was sent to Bioinformatics",
            "name": "referralTestOrderingDate",
            "type": [
                "null",
                {
                    "doc": "This defines a date record",
                    "fields": [
                        {
                            "doc": "Format YYYY",
                            "name": "year",
                            "type": "int"
                        },
                        {
                            "doc": "Format MM. e.g June is 06",
                            "name": "month",
                            "type": [
                                "null",
                                "int"
                            ]
                        },
                        {
                            "doc": "Format DD e.g. 12th of October is 12",
                            "name": "day",
                            "type": [
                                "null",
                                "int"
                            ]
                        }
                    ],
                    "name": "Date",
                    "type": "record"
                }
            ]
        },
        {
            "doc": "Clinical indication test",
            "name": "clinicalIndicationTest",
            "type": {
                "fields": [
                    {
                        "doc": "Clinical indication Test type ID",
                        "name": "clinicalIndicationTestTypeId",
                        "type": "string"
                    },
                    {
                        "doc": "Clinical indication Test code (e.g. R13-1)",
                        "name": "clinicalIndicationTestTypeCode",
                        "type": "string"
                    },
                    {
                        "doc": "Test Type Id",
                        "name": "testTypeId",
                        "type": "string"
                    },
                    {
                        "doc": "Test Type Name",
                        "name": "testTypeName",
                        "type": "string"
                    },
                    {
                        "doc": "Technology used in ClinicalIndicationTest",
                        "name": "technology",
                        "type": {
                            "fields": [
                                {
                                    "doc": "Technology unique identifier",
                                    "name": "testTechnologyId",
                                    "type": "string"
                                },
                                {
                                    "doc": "Technology description",
                                    "name": "testTechnologyDescription",
                                    "type": "string"
                                }
                            ],
                            "name": "Technology",
                            "type": "record"
                        }
                    }
                ],
                "name": "ClinicalIndicationTest",
                "type": "record"
            }
        },
        {
            "doc": "List of all somatic samples applicable to this test",
            "name": "tumourSamples",
            "type": [
                "null",
                {
                    "items": {
                        "doc": "A tumour sample",
                        "fields": [
                            {
                                "doc": "Sample identifier (e.g, LP00012645_5GH))",
                                "name": "sampleId",
                                "type": "string"
                            },
                            {
                                "doc": "Lab sample identifier",
                                "name": "labSampleId",
                                "type": "string"
                            },
                            {
                                "doc": "LDP Code (Local Delivery Partner)",
                                "name": "LDPCode",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "This is the ID of the tumour from which this tumour sample was taken from",
                                "name": "tumourId",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Genomics England programme phase",
                                "name": "programmePhase",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Disease type.\n        NOTE: Deprecated in GMS",
                                "name": "diseaseType",
                                "type": [
                                    "null",
                                    {
                                        "name": "diseaseType",
                                        "symbols": [
                                            "ADULT_GLIOMA",
                                            "BLADDER",
                                            "BREAST",
                                            "CARCINOMA_OF_UNKNOWN_PRIMARY",
                                            "CHILDHOOD",
                                            "COLORECTAL",
                                            "ENDOCRINE",
                                            "ENDOMETRIAL_CARCINOMA",
                                            "HAEMONC",
                                            "HEPATOPANCREATOBILIARY",
                                            "LUNG",
                                            "MALIGNANT_MELANOMA",
                                            "NASOPHARYNGEAL",
                                            "ORAL_OROPHARYNGEAL",
                                            "OVARIAN",
                                            "PROSTATE",
                                            "RENAL",
                                            "SARCOMA",
                                            "SINONASAL",
                                            "TESTICULAR_GERM_CELL_TUMOURS",
                                            "UPPER_GASTROINTESTINAL",
                                            "OTHER",
                                            "NON_HODGKINS_B_CELL_LYMPHOMA_LOW_MOD_GRADE",
                                            "CLASSICAL_HODGKINS",
                                            "NODULAR_LYMPHOCYTE_PREDOMINANT_HODGKINS",
                                            "T_CELL_LYMPHOMA"
                                        ],
                                        "type": "enum"
                                    }
                                ]
                            },
                            {
                                "doc": "Disease subtype.\n        NOTE: Deprecated in GMS",
                                "name": "diseaseSubType",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "True or false if this sample is of type: Haematological Cancer",
                                "name": "haematologicalCancer",
                                "type": [
                                    "null",
                                    "boolean"
                                ]
                            },
                            {
                                "doc": "This is the Haematological cancer lineage of the tumourSample if this sample is from a haematological cancer",
                                "name": "haematologicalCancerLineage",
                                "type": [
                                    "null",
                                    {
                                        "name": "HaematologicalCancerLineage",
                                        "symbols": [
                                            "MYELOID",
                                            "LYMPHOID",
                                            "UNKNOWN"
                                        ],
                                        "type": "enum"
                                    }
                                ]
                            },
                            {
                                "doc": "The time when the sample was received. In the format YYYY-MM-DDTHH:MM:SS+0000",
                                "name": "clinicalSampleDateTime",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Tumor type.\n        NOTE: Deprecated in GMS in tumourSample but available in tumour record",
                                "name": "tumourType",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "This is the tumour content",
                                "name": "tumourContent",
                                "type": [
                                    "null",
                                    {
                                        "name": "TumourContent",
                                        "symbols": [
                                            "High",
                                            "Medium",
                                            "Low"
                                        ],
                                        "type": "enum"
                                    }
                                ]
                            },
                            {
                                "doc": "This is the tumour content percentage",
                                "name": "tumourContentPercentage",
                                "type": [
                                    "null",
                                    "float"
                                ]
                            },
                            {
                                "doc": "Source of the sample",
                                "name": "source",
                                "type": [
                                    "null",
                                    {
                                        "doc": "The source of the sample\n    NOTE: IN GMS, BONE_MARROW_ASPIRATE_TUMOUR_CELLS and BONE_MARROW_ASPIRATE_TUMOUR_SORTED_CELLS are deprecated as they have been separated into their respective biotypes",
                                        "name": "SampleSource",
                                        "symbols": [
                                            "AMNIOTIC_FLUID",
                                            "BLOOD",
                                            "BONE_MARROW",
                                            "BONE_MARROW_ASPIRATE_TUMOUR_CELLS",
                                            "BONE_MARROW_ASPIRATE_TUMOUR_SORTED_CELLS",
                                            "BUCCAL_SWAB",
                                            "CHORIONIC_VILLUS_SAMPLE",
                                            "FIBROBLAST",
                                            "FLUID",
                                            "FRESH_TISSUE_IN_CULTURE_MEDIUM",
                                            "OTHER",
                                            "SALIVA",
                                            "TISSUE",
                                            "TUMOUR",
                                            "URINE"
                                        ],
                                        "type": "enum"
                                    }
                                ]
                            },
                            {
                                "doc": "The preparation method of the sample\n        NOTE: Deprecated in GMS in replace of Method and storageMedium record",
                                "name": "preparationMethod",
                                "type": [
                                    "null",
                                    {
                                        "doc": "In 100K, preparation Method of sample\n    NOTE: In GMS, this field is deprecated in favour of StorageMedium and Method",
                                        "name": "PreparationMethod",
                                        "symbols": [
                                            "ASPIRATE",
                                            "CD128_SORTED_CELLS",
                                            "CD138_SORTED_CELLS",
                                            "EDTA",
                                            "FF",
                                            "FFPE",
                                            "LI_HEP",
                                            "ORAGENE"
                                        ],
                                        "type": "enum"
                                    }
                                ]
                            },
                            {
                                "doc": "The tissue source of the sample.\n        NOTE: DEPRECATED IN GMS in replace of method record",
                                "name": "tissueSource",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Product of the sample",
                                "name": "product",
                                "type": [
                                    "null",
                                    {
                                        "name": "Product",
                                        "symbols": [
                                            "DNA",
                                            "RNA"
                                        ],
                                        "type": "enum"
                                    }
                                ]
                            },
                            {
                                "doc": "Morphology according to the sample taken",
                                "name": "sampleMorphologies",
                                "type": [
                                    "null",
                                    {
                                        "items": {
                                            "fields": [
                                                {
                                                    "doc": "The ontology term id or accession in OBO format ${ONTOLOGY_ID}:${TERM_ID} (http://www.obofoundry.org/id-policy.html)",
                                                    "name": "id",
                                                    "type": [
                                                        "null",
                                                        "string"
                                                    ]
                                                },
                                                {
                                                    "doc": "The ontology term name",
                                                    "name": "name",
                                                    "type": [
                                                        "null",
                                                        "string"
                                                    ]
                                                },
                                                {
                                                    "doc": "Optional value for the ontology term, the type of the value is not checked\n        (i.e.: we could set the pvalue term to \"significant\" or to \"0.0001\")",
                                                    "name": "value",
                                                    "type": [
                                                        "null",
                                                        "string"
                                                    ]
                                                },
                                                {
                                                    "doc": "Ontology version",
                                                    "name": "version",
                                                    "type": [
                                                        "null",
                                                        "string"
                                                    ]
                                                }
                                            ],
                                            "name": "Morphology",
                                            "type": "record"
                                        },
                                        "type": "array"
                                    }
                                ]
                            },
                            {
                                "doc": "Topography according to the sample taken",
                                "name": "sampleTopographies",
                                "type": [
                                    "null",
                                    {
                                        "items": {
                                            "fields": [
                                                {
                                                    "doc": "The ontology term id or accession in OBO format ${ONTOLOGY_ID}:${TERM_ID} (http://www.obofoundry.org/id-policy.html)",
                                                    "name": "id",
                                                    "type": [
                                                        "null",
                                                        "string"
                                                    ]
                                                },
                                                {
                                                    "doc": "The ontology term name",
                                                    "name": "name",
                                                    "type": [
                                                        "null",
                                                        "string"
                                                    ]
                                                },
                                                {
                                                    "doc": "Optional value for the ontology term, the type of the value is not checked\n        (i.e.: we could set the pvalue term to \"significant\" or to \"0.0001\")",
                                                    "name": "value",
                                                    "type": [
                                                        "null",
                                                        "string"
                                                    ]
                                                },
                                                {
                                                    "doc": "Ontology version",
                                                    "name": "version",
                                                    "type": [
                                                        "null",
                                                        "string"
                                                    ]
                                                }
                                            ],
                                            "name": "Topography",
                                            "type": "record"
                                        },
                                        "type": "array"
                                    }
                                ]
                            },
                            {
                                "doc": "In GMS, this is the GUID of the sample",
                                "name": "sampleUid",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Participant Id of the sample",
                                "name": "participantId",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Participant UId of the sample",
                                "name": "participantUid",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "In GMS, this is the maskedPID",
                                "name": "maskedPid",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "In GMS, this is how the sample was extracted from the participant",
                                "name": "method",
                                "type": [
                                    "null",
                                    {
                                        "doc": "In GMS, Method is defined as how the sample was taken directly from the patient",
                                        "name": "Method",
                                        "symbols": [
                                            "ASPIRATE",
                                            "BIOPSY",
                                            "NOT_APPLICABLE",
                                            "RESECTION",
                                            "SORTED_OTHER",
                                            "UNKNOWN",
                                            "UNSORTED",
                                            "CD138_SORTED"
                                        ],
                                        "type": "enum"
                                    }
                                ]
                            },
                            {
                                "doc": "In GMS, this is what solvent/medium the sample was stored in",
                                "name": "storageMedium",
                                "type": [
                                    "null",
                                    {
                                        "doc": "In GMS, storage medium of sample",
                                        "name": "StorageMedium",
                                        "symbols": [
                                            "EDTA",
                                            "FF",
                                            "LI_HEP",
                                            "ORAGENE",
                                            "FFPE"
                                        ],
                                        "type": "enum"
                                    }
                                ]
                            },
                            {
                                "doc": "In GMS, this is the sampleType as entered by the clinician in TOMs",
                                "name": "sampleType",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "In GMS, this is the sampleState as entered by the clinician in TOMs",
                                "name": "sampleState",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            }
                        ],
                        "name": "TumourSample",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "List of all germline samples aplicable to this test",
            "name": "germlineSamples",
            "type": [
                "null",
                {
                    "items": {
                        "doc": "A germline sample",
                        "fields": [
                            {
                                "doc": "Sample identifier (e.g, LP00012645_5GH))",
                                "name": "sampleId",
                                "type": "string"
                            },
                            {
                                "doc": "Lab sample identifier",
                                "name": "labSampleId",
                                "type": "string"
                            },
                            {
                                "doc": "LDP Code (Local Delivery Partner)",
                                "name": "LDPCode",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Source of the sample",
                                "name": "source",
                                "type": [
                                    "null",
                                    "SampleSource"
                                ]
                            },
                            {
                                "doc": "Product of the sample",
                                "name": "product",
                                "type": [
                                    "null",
                                    "Product"
                                ]
                            },
                            {
                                "doc": "Preparation method\n        NOTE: In GMS, this has been deprecated in favour of Method and storageMedium",
                                "name": "preparationMethod",
                                "type": [
                                    "null",
                                    "PreparationMethod"
                                ]
                            },
                            {
                                "doc": "Genomics England programme phase",
                                "name": "programmePhase",
                                "type": [
                                    "null",
                                    {
                                        "name": "ProgrammePhase",
                                        "symbols": [
                                            "CRUK",
                                            "OXFORD",
                                            "CLL",
                                            "IIP",
                                            "MAIN",
                                            "EXPT"
                                        ],
                                        "type": "enum"
                                    }
                                ]
                            },
                            {
                                "doc": "The time when the sample was received. In the format YYYY-MM-DDTHH:MM:SS+0000",
                                "name": "clinicalSampleDateTime",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "name": "participantId",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Participant UId of the sample",
                                "name": "participantUid",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "name": "sampleUid",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "name": "maskedPid",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "In GMS, this is how the sample was extracted from the participant",
                                "name": "method",
                                "type": [
                                    "null",
                                    "Method"
                                ]
                            },
                            {
                                "doc": "In GMS, this is what solvent/medium the sample was stored in",
                                "name": "storageMedium",
                                "type": [
                                    "null",
                                    "StorageMedium"
                                ]
                            },
                            {
                                "doc": "In GMS, this is the sampleType as entered by the clinician in TOMs",
                                "name": "sampleType",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "In GMS, this is the sampleState as entered by the clinician in TOMs",
                                "name": "sampleState",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            }
                        ],
                        "name": "GermlineSample",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "List of Analysis panels",
            "name": "analysisPanels",
            "type": [
                "null",
                {
                    "items": {
                        "doc": "An analysis panel",
                        "fields": [
                            {
                                "doc": "The specific disease that a panel tests",
                                "name": "specificDisease",
                                "type": "string"
                            },
                            {
                                "doc": "The name of the panel",
                                "name": "panelName",
                                "type": "string"
                            },
                            {
                                "doc": "Id of the panel",
                                "name": "panelId",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "The version of the panel",
                                "name": "panelVersion",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Deprecated",
                                "name": "reviewOutcome",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Deprecated",
                                "name": "multipleGeneticOrigins",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            }
                        ],
                        "name": "AnalysisPanel",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Organisation assigned for the interpretation of this test",
            "name": "interpreter",
            "type": {
                "fields": [
                    {
                        "doc": "Organisation Id",
                        "name": "organisationId",
                        "type": "string"
                    },
                    {
                        "doc": "Ods code",
                        "name": "organisationCode",
                        "type": "string"
                    },
                    {
                        "doc": "Organisation Name",
                        "name": "organisationName",
                        "type": "string"
                    },
                    {
                        "doc": "National Grouping (GLH) Id",
                        "name": "organisationNationalGroupingId",
                        "type": "string"
                    },
                    {
                        "doc": "National Grouping (GLH) Name",
                        "name": "organisationNationalGroupingName",
                        "type": "string"
                    }
                ],
                "name": "OrganisationNgis",
                "type": "record"
            }
        },
        {
            "doc": "Organisation assigned for the processing of the test",
            "name": "processingLab",
            "type": "OrganisationNgis"
        },
        {
            "doc": "Priority",
            "name": "priority",
            "type": {
                "doc": "Transformed from TOMs from routine=medium, and urgent=high",
                "name": "Priority",
                "symbols": [
                    "low",
                    "routine",
                    "urgent"
                ],
                "type": "enum"
            }
        },
        {
            "doc": "Date of ordering. NOTE: this field is not required from upstream\n        and will be generated by Bioinformatics when all sample data and all\n        required clinical data is received for the first time",
            "name": "pipelineStartDate",
            "type": [
                "null",
                "Date"
            ]
        },
        {
            "doc": "Disease Penetrance applied for that referralTest",
            "name": "diseasePenetrances",
            "type": [
                "null",
                {
                    "items": {
                        "doc": "A disease penetrance definition",
                        "fields": [
                            {
                                "doc": "The disease to which the penetrance applies",
                                "name": "specificDisease",
                                "type": "string"
                            },
                            {
                                "doc": "The penetrance",
                                "name": "penetrance",
                                "type": {
                                    "doc": "Penetrance assumed in the analysis",
                                    "name": "Penetrance",
                                    "symbols": [
                                        "complete",
                                        "incomplete"
                                    ],
                                    "type": "enum"
                                }
                            }
                        ],
                        "name": "DiseasePenetrance",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "List of matched samples (i.e.: pairs tumour-germline)",
            "name": "matchedSamples",
            "type": [
                "null",
                {
                    "items": {
                        "doc": "This defines a pair of germline and tumor, this pair should/must be analyzed together",
                        "fields": [
                            {
                                "doc": "Sample identifier (e.g, LP00012645_5GH)) for the germline",
                                "name": "germlineSampleId",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Sample identifier (e.g, LP00012643_7JS)) for the tumor",
                                "name": "tumourSampleId",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            }
                        ],
                        "name": "MatchedSamples",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        }
    ],
    "name": "ReferralTest",
    "namespace": "org.gel.models.participant.avro",
    "type": "record"
}