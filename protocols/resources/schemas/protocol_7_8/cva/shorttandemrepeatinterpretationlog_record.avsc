{
    "fields": [
        {
            "doc": "Variant details",
            "name": "variant",
            "type": {
                "fields": [
                    {
                        "doc": "Short tandem repeat coordinates",
                        "name": "shortTandemRepeatCoordinates",
                        "type": {
                            "fields": [
                                {
                                    "doc": "The assembly to which this variant corresponds",
                                    "name": "assembly",
                                    "type": {
                                        "doc": "The reference genome assembly",
                                        "name": "Assembly",
                                        "symbols": [
                                            "GRCh38",
                                            "GRCh37"
                                        ],
                                        "type": "enum"
                                    }
                                },
                                {
                                    "doc": "Chromosome without \"chr\" prefix (e.g. X rather than chrX)",
                                    "name": "chromosome",
                                    "type": "string"
                                },
                                {
                                    "doc": "Start genomic position for variant (1-based)",
                                    "name": "start",
                                    "type": "int"
                                },
                                {
                                    "doc": "End genomic position for variant",
                                    "name": "end",
                                    "type": "int"
                                },
                                {
                                    "name": "ciStart",
                                    "type": [
                                        "null",
                                        {
                                            "fields": [
                                                {
                                                    "name": "left",
                                                    "type": "int"
                                                },
                                                {
                                                    "name": "right",
                                                    "type": "int"
                                                }
                                            ],
                                            "name": "ConfidenceInterval",
                                            "type": "record"
                                        }
                                    ]
                                },
                                {
                                    "name": "ciEnd",
                                    "type": [
                                        "null",
                                        "ConfidenceInterval"
                                    ]
                                }
                            ],
                            "name": "Coordinates",
                            "type": "record"
                        }
                    },
                    {
                        "doc": "Short tandem repeat copy number for each allele",
                        "name": "numberOfCopies",
                        "type": [
                            "null",
                            {
                                "items": {
                                    "fields": [
                                        {
                                            "doc": "Number of copies given by the caller in one of the allele",
                                            "name": "numberOfCopies",
                                            "type": "int"
                                        },
                                        {
                                            "name": "confidenceIntervalMaximum",
                                            "type": [
                                                "null",
                                                "int"
                                            ]
                                        },
                                        {
                                            "name": "confidenceIntervalMinimum",
                                            "type": [
                                                "null",
                                                "int"
                                            ]
                                        }
                                    ],
                                    "name": "NumberOfCopies",
                                    "type": "record"
                                },
                                "type": "array"
                            }
                        ]
                    }
                ],
                "name": "ShortTandemRepeatDetails",
                "type": "record"
            }
        },
        {
            "doc": "User who set classification",
            "name": "user",
            "type": {
                "fields": [
                    {
                        "doc": "Azure Active Directory immutable user OID",
                        "name": "userid",
                        "type": [
                            "null",
                            "string"
                        ]
                    },
                    {
                        "doc": "User email address",
                        "name": "email",
                        "type": "string"
                    },
                    {
                        "doc": "Username",
                        "name": "username",
                        "type": "string"
                    },
                    {
                        "name": "role",
                        "type": [
                            "null",
                            "string"
                        ]
                    },
                    {
                        "name": "groups",
                        "type": [
                            "null",
                            {
                                "items": "string",
                                "type": "array"
                            }
                        ]
                    }
                ],
                "name": "User",
                "type": "record"
            }
        },
        {
            "doc": "Date of classification. Format YYYY-MM-DD (e.g. 2020-01-31)",
            "name": "timestamp",
            "type": "string"
        },
        {
            "doc": "GeL group ID. For GMS cases this will be the referral ID. For 100k rare disease cases this will be the family ID. For 100k cancer cases this will be the participant ID.",
            "name": "groupId",
            "type": "string"
        },
        {
            "doc": "Interpretation request ID including CIP prefix and version suffix (e.g. SAP-1234-1)",
            "name": "caseId",
            "type": "string"
        },
        {
            "doc": "Independent validation of variant",
            "name": "variantValidation",
            "type": [
                "null",
                {
                    "fields": [
                        {
                            "doc": "Technology used to perform secondary confirmation of this variant (e.g. Sanger)",
                            "name": "validationTechnology",
                            "type": "string"
                        },
                        {
                            "doc": "Status/outcome of validation",
                            "name": "validationResult",
                            "type": {
                                "name": "ValidationResult",
                                "symbols": [
                                    "NotPerformed",
                                    "Confirmed",
                                    "NotConfirmed",
                                    "Pending"
                                ],
                                "type": "enum"
                            }
                        }
                    ],
                    "name": "VariantValidation",
                    "type": "record"
                }
            ]
        },
        {
            "doc": "User comments attached to this variant in this case",
            "name": "comments",
            "type": [
                "null",
                {
                    "items": {
                        "fields": [
                            {
                                "doc": "Comment text",
                                "name": "comment",
                                "type": "string"
                            },
                            {
                                "doc": "User who created comment",
                                "name": "user",
                                "type": [
                                    "null",
                                    "User"
                                ]
                            },
                            {
                                "doc": "Date and time comment was created (ISO 8601 datetime with seconds and timezone e.g. 2020-11-23T15:52:36+00:00)",
                                "name": "timestamp",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            }
                        ],
                        "name": "UserComment",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Variant classification",
            "name": "variantClassification",
            "type": {
                "name": "ClinicalSignificance",
                "symbols": [
                    "benign",
                    "likely_benign",
                    "likely_pathogenic",
                    "pathogenic",
                    "uncertain_significance",
                    "excluded"
                ],
                "type": "enum"
            }
        },
        {
            "doc": "User has marked the variant as an artefact",
            "name": "Artifact",
            "type": [
                "null",
                "boolean"
            ]
        },
        {
            "doc": "Filter settings applied at time variant was classified",
            "name": "decisionSupportSystemFilters",
            "type": [
                "null",
                {
                    "type": "map",
                    "values": "string"
                }
            ]
        }
    ],
    "name": "ShortTandemRepeatInterpretationLog",
    "namespace": "org.gel.models.report.avro",
    "type": "record"
}