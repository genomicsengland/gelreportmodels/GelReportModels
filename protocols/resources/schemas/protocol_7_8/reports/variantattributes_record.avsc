{
    "doc": "Some additional variant attributes",
    "fields": [
        {
            "doc": "gDNA change, HGVS nomenclature (e.g.: g.476A>T)",
            "name": "genomicChanges",
            "type": [
                "null",
                {
                    "items": "string",
                    "type": "array"
                }
            ]
        },
        {
            "doc": "cDNA change, HGVS nomenclature (e.g.: c.76A>T)",
            "name": "cdnaChanges",
            "type": [
                "null",
                {
                    "items": "string",
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Protein change, HGVS nomenclature (e.g.: p.Lys76Asn)",
            "name": "proteinChanges",
            "type": [
                "null",
                {
                    "items": "string",
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Any additional information in a free text field. For example a quote from a paper",
            "name": "additionalTextualVariantAnnotations",
            "type": [
                "null",
                {
                    "type": "map",
                    "values": "string"
                }
            ]
        },
        {
            "doc": "Additional references for ths variant. For example HGMD ID or Pubmed Id",
            "name": "references",
            "type": [
                "null",
                {
                    "type": "map",
                    "values": "string"
                }
            ]
        },
        {
            "name": "variantIdentifiers",
            "type": [
                "null",
                {
                    "fields": [
                        {
                            "doc": "Variant identifier in dbSNP",
                            "name": "dbSnpId",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Variant identifier in Cosmic",
                            "name": "cosmicIds",
                            "type": [
                                "null",
                                {
                                    "items": "string",
                                    "type": "array"
                                }
                            ]
                        },
                        {
                            "doc": "Variant identifier in ClinVar",
                            "name": "clinVarIds",
                            "type": [
                                "null",
                                {
                                    "items": "string",
                                    "type": "array"
                                }
                            ]
                        },
                        {
                            "name": "otherIds",
                            "type": [
                                "null",
                                {
                                    "items": {
                                        "fields": [
                                            {
                                                "doc": "Source i.e, esenmbl",
                                                "name": "source",
                                                "type": "string"
                                            },
                                            {
                                                "doc": "identifier",
                                                "name": "identifier",
                                                "type": "string"
                                            }
                                        ],
                                        "name": "Identifier",
                                        "type": "record"
                                    },
                                    "type": "array"
                                }
                            ]
                        }
                    ],
                    "name": "VariantIdentifiers",
                    "type": "record"
                }
            ]
        },
        {
            "doc": "A list of population allele frequencies",
            "name": "alleleFrequencies",
            "type": [
                "null",
                {
                    "items": {
                        "doc": "The population allele frequency of a given variant in a given study and optionally population",
                        "fields": [
                            {
                                "doc": "The study from where this data comes from",
                                "name": "study",
                                "type": "string"
                            },
                            {
                                "doc": "The specific population where this allele frequency belongs",
                                "name": "population",
                                "type": "string"
                            },
                            {
                                "doc": "The frequency of the alternate allele",
                                "name": "alternateFrequency",
                                "type": "float"
                            }
                        ],
                        "name": "AlleleFrequency",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Additional numeric variant annotations for this variant. For Example (Allele Frequency, sift, polyphen,\n        mutationTaster, CADD. ..)",
            "name": "additionalNumericVariantAnnotations",
            "type": [
                "null",
                {
                    "type": "map",
                    "values": "float"
                }
            ]
        },
        {
            "doc": "Comments",
            "name": "comments",
            "type": [
                "null",
                {
                    "items": "string",
                    "type": "array"
                }
            ]
        },
        {
            "doc": "List of allele origins for this variant in this report",
            "name": "alleleOrigins",
            "type": [
                "null",
                {
                    "items": {
                        "doc": "Allele origin.\n\n* `SO_0001781`: de novo variant. http://purl.obolibrary.org/obo/SO_0001781\n* `SO_0001778`: germline variant. http://purl.obolibrary.org/obo/SO_0001778\n* `SO_0001775`: maternal variant. http://purl.obolibrary.org/obo/SO_0001775\n* `SO_0001776`: paternal variant. http://purl.obolibrary.org/obo/SO_0001776\n* `SO_0001779`: pedigree specific variant. http://purl.obolibrary.org/obo/SO_0001779\n* `SO_0001780`: population specific variant. http://purl.obolibrary.org/obo/SO_0001780\n* `SO_0001777`: somatic variant. http://purl.obolibrary.org/obo/SO_0001777",
                        "name": "AlleleOrigin",
                        "symbols": [
                            "de_novo_variant",
                            "germline_variant",
                            "maternal_variant",
                            "paternal_variant",
                            "pedigree_specific_variant",
                            "population_specific_variant",
                            "somatic_variant"
                        ],
                        "type": "enum"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Largest reference interrupted homopolymer length intersecting with the indel",
            "name": "ihp",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "doc": "Flag indicating if the variant is recurrently reported",
            "name": "recurrentlyReported",
            "type": [
                "null",
                "boolean"
            ]
        },
        {
            "doc": "Average tier1 number of basecalls filtered from original read depth within 50 bases",
            "name": "fdp50",
            "type": [
                "null",
                "float"
            ]
        },
        {
            "doc": "Map of other attributes where keys are the attribute names and values are the attributes",
            "name": "others",
            "type": [
                "null",
                {
                    "type": "map",
                    "values": "string"
                }
            ]
        }
    ],
    "name": "VariantAttributes",
    "namespace": "org.gel.models.report.avro",
    "type": "record"
}