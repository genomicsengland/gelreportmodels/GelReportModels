{
    "doc": "The variant classification according to different properties.",
    "fields": [
        {
            "doc": "The variant's clinical significance.",
            "name": "clinicalSignificance",
            "type": [
                "null",
                {
                    "doc": "Mendelian variants classification with ACMG terminology as defined in Richards, S. et al. (2015). Standards and\n        guidelines for the interpretation of sequence variants: a joint consensus recommendation of the American College\n        of Medical Genetics and Genomics and the Association for Molecular Pathology. Genetics in Medicine, 17(5),\n        405\u2013423. https://doi.org/10.1038/gim.2015.30.\n\n    Classification for pharmacogenomic variants, variants associated to\n    disease and somatic variants based on the ACMG recommendations and ClinVar classification\n    (https://www.ncbi.nlm.nih.gov/clinvar/docs/clinsig/).\n\n* `benign_variant` : Benign variants interpreted for Mendelian disorders\n* `likely_benign_variant` : Likely benign variants interpreted for Mendelian disorders with a certainty of at least 90%\n* `pathogenic_variant` : Pathogenic variants interpreted for Mendelian disorders\n* `likely_pathogenic_variant` : Likely pathogenic variants interpreted for Mendelian disorders with a certainty of at\nleast 90%\n* `uncertain_significance` : Uncertain significance variants interpreted for Mendelian disorders. Variants with\nconflicting evidences should be classified as uncertain_significance",
                    "name": "ClinicalSignificance",
                    "symbols": [
                        "benign",
                        "likely_benign",
                        "VUS",
                        "likely_pathogenic",
                        "pathogenic",
                        "uncertain_significance"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "The variant's pharmacogenomics classification.",
            "name": "drugResponseClassification",
            "type": [
                "null",
                {
                    "doc": "Pharmacogenomics drug response variant classification\n* `responsive` : A variant that confers response to a treatment\n* `resistant` : A variant that confers resistance to a treatment\n* `toxicity` : A variant that is associated with drug-induced toxicity\n* `indication` : A variant that is required in order for a particular drug to be prescribed\n* `contraindication` : A variant that if present, a particular drug should not be prescribed\n* `dosing` : A variant that results in an alteration in dosing of a particular drug in order to achieve INR, reduce toxicity or increase efficacy\n* `increased_monitoring` : increase vigilance or increased dosage monitoring may be required for a patient with this variant to look for signs of adverse drug reactions\n* `efficacy` : a variant that affects the efficacy of the treatment",
                    "name": "DrugResponseClassification",
                    "symbols": [
                        "responsive",
                        "resistant",
                        "toxicity",
                        "indication",
                        "contraindication",
                        "dosing",
                        "increased_monitoring",
                        "efficacy"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "The variant's trait association.",
            "name": "traitAssociation",
            "type": [
                "null",
                {
                    "doc": "Association of variants to a given trait.\n* `established_risk_allele` : Established risk allele for variants associated to disease\n* `likely_risk_allele` : Likely risk allele for variants associated to disease\n* `uncertain_risk_allele` : Uncertain risk allele for variants associated to disease\n* `protective` : Protective allele",
                    "name": "TraitAssociation",
                    "symbols": [
                        "established_risk_allele",
                        "likely_risk_allele",
                        "uncertain_risk_allele",
                        "protective"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "The variant's tumorigenesis classification.",
            "name": "tumorigenesisClassification",
            "type": [
                "null",
                {
                    "doc": "Variant classification according to its relation to cancer aetiology.\n* `driver` : Driver variants\n* `passenger` : Passenger variants\n* `modifier` : Modifier variants",
                    "name": "TumorigenesisClassification",
                    "symbols": [
                        "driver",
                        "passenger",
                        "modifier"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "The variant functional effect",
            "name": "functionalEffect",
            "type": [
                "null",
                {
                    "doc": "Variant effect with Sequence Ontology terms.\n\n* `SO_0002052`: dominant_negative_variant (http://purl.obolibrary.org/obo/SO_0002052)\n* `SO_0002053`: gain_of_function_variant (http://purl.obolibrary.org/obo/SO_0002053)\n* `SO_0001773`: lethal_variant (http://purl.obolibrary.org/obo/SO_0001773)\n* `SO_0002054`: loss_of_function_variant (http://purl.obolibrary.org/obo/SO_0002054)\n* `SO_0001786`: loss_of_heterozygosity (http://purl.obolibrary.org/obo/SO_0001786)\n* `SO_0002055`: null_variant (http://purl.obolibrary.org/obo/SO_0002055)",
                    "name": "VariantFunctionalEffect",
                    "symbols": [
                        "dominant_negative_variant",
                        "gain_of_function_variant",
                        "lethal_variant",
                        "loss_of_function_variant",
                        "loss_of_heterozygosity",
                        "null_variant"
                    ],
                    "type": "enum"
                }
            ]
        }
    ],
    "name": "VariantClassification",
    "namespace": "org.opencb.biodata.models.variant.avro",
    "type": "record"
}