{
    "doc": "Clinical actions",
    "fields": [
        {
            "name": "trials",
            "type": [
                "null",
                {
                    "items": {
                        "fields": [
                            {
                                "doc": "URL where reference information for this trail can be found",
                                "name": "studyUrl",
                                "type": "string"
                            },
                            {
                                "doc": "Trail/Study indetifier",
                                "name": "studyIdentifier",
                                "type": "string"
                            },
                            {
                                "doc": "Start date of the study",
                                "name": "startDate",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Completion date of the study",
                                "name": "estimateCompletionDate",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Title of the study",
                                "name": "title",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Study Phase",
                                "name": "phase",
                                "type": [
                                    "null",
                                    {
                                        "doc": "N/A: Trials without phases (for example, studies of devices or behavioural interventions).\nEarly Phase 1 (Formerly listed as \"Phase 0\"): Exploratory trials, involving very limited human exposure, with no therapeutic or diagnostic intent (e.g., screening studies, microdose studies). See FDA guidance on exploratory IND studies for more information.\nPhase 1: Includes initial studies to determine the metabolism and pharmacologic actions of drugs in humans, the side effects associated with increasing doses, and to gain early evidence of effectiveness; may include healthy participants and/or patients.\nPhase 1/Phase 2: Trials that are a combination of phases 1 and 2.\nPhase 2: Includes controlled clinical studies conducted to evaluate the effectiveness of the drug for a particular indication or indications in participants with the disease or condition under study and to determine the common short-term side effects and risks.\nPhase 2/Phase 3: Trials that are a combination of phases 2 and 3.\nPhase 3: Includes trials conducted after preliminary evidence suggesting effectiveness of the drug has been obtained, and are intended to gather additional information to evaluate the overall benefit-risk relationship of the drug.\nPhase 4: Studies of FDA-approved drugs to delineate additional information including the drug's risks, benefits, and optimal use.",
                                        "name": "StudyPhase",
                                        "symbols": [
                                            "na",
                                            "early_phase1",
                                            "phase1",
                                            "phase1_phase2",
                                            "phase2",
                                            "phase2_phase3",
                                            "phase3",
                                            "phase4"
                                        ],
                                        "type": "enum"
                                    }
                                ]
                            },
                            {
                                "doc": "Interventions",
                                "name": "interventions",
                                "type": [
                                    "null",
                                    {
                                        "items": {
                                            "doc": "A process or action that is the focus of a clinical study.\nRef. https://prsinfo.clinicaltrials.gov/definitions.html",
                                            "fields": [
                                                {
                                                    "doc": "Intervention type, i.e drug",
                                                    "name": "interventionType",
                                                    "type": {
                                                        "doc": "For each intervention studied in the clinical study, the general type of intervention\n\n* `drug`: Including placebo\n* `device`: Including sham\n* `biological`: Vaccine\n* `procedure`: Surgery\n* `radiation`\n* `behavioral`: For example, psychotherapy, lifestyle counselling\n* `genetic`: Including gene transfer, stem cell and recombinant DNA\n* `dietary_supplement`: For example, vitamins, minerals\n* `combination_product`: Combining a drug and device, a biological product and device; a drug and biological product; or a drug, biological product, and device\n* `diagnostic_test`: For example, imaging, in-vitro\n* `other`\n\n    Ref. https://prsinfo.clinicaltrials.gov/definitions.htm",
                                                        "name": "InterventionType",
                                                        "symbols": [
                                                            "drug",
                                                            "device",
                                                            "procedure",
                                                            "biological",
                                                            "radiation",
                                                            "behavioral",
                                                            "genetic",
                                                            "dietary_supplement",
                                                            "combination_product",
                                                            "diagnostic_test",
                                                            "other"
                                                        ],
                                                        "type": "enum"
                                                    }
                                                },
                                                {
                                                    "doc": "Intervention name: Placebo",
                                                    "name": "interventionName",
                                                    "type": "string"
                                                }
                                            ],
                                            "name": "Intervention",
                                            "type": "record"
                                        },
                                        "type": "array"
                                    }
                                ]
                            },
                            {
                                "doc": "Conditions",
                                "name": "conditions",
                                "type": [
                                    "null",
                                    {
                                        "items": "string",
                                        "type": "array"
                                    }
                                ]
                            },
                            {
                                "doc": "Primary Purpose of the study",
                                "name": "primaryPurpose",
                                "type": [
                                    "null",
                                    {
                                        "doc": "Treatment: One or more interventions are being evaluated for treating a disease, syndrome, or condition.\n    Prevention: One or more interventions are being assessed for preventing the development of a specific disease or health condition.\n    Diagnostic: One or more interventions are being evaluated for identifying a disease or health condition.\n    Supportive Care: One or more interventions are evaluated for maximizing comfort, minimizing side effects, or mitigating against a decline in the participant's health or function.\n    Screening: One or more interventions are assessed or examined for identifying a condition, or risk factors for a condition, in people who are not yet known to have the condition or risk factor.\n    Health Services Research: One or more interventions for evaluating the delivery, processes, management, organization, or financing of healthcare.\n    Basic Science: One or more interventions for examining the basic mechanism of action (for example, physiology or biomechanics of an intervention).\n    Device Feasibility: An intervention of a device product is being evaluated in a small clinical trial (generally fewer than 10 participants) to determine the feasibility of the product; or a clinical trial to test a prototype device for feasibility and not health outcomes. Such studies are conducted to confirm the design and operating specifications of a device before beginning a full clinical trial.\n    Other: None of the other options applies.\n\n    Ref. https://prsinfo.clinicaltrials.gov/definitions.htm",
                                        "name": "PrimaryPurpose",
                                        "symbols": [
                                            "treatment",
                                            "prevention",
                                            "diagnostic",
                                            "supportive_care",
                                            "screening",
                                            "health_services_research",
                                            "basic_science",
                                            "device_feasibility",
                                            "other"
                                        ],
                                        "type": "enum"
                                    }
                                ]
                            },
                            {
                                "doc": "Study Type",
                                "name": "studyType",
                                "type": [
                                    "null",
                                    {
                                        "doc": "* `Interventional (clinical trial)`: Participants are assigned prospectively to an intervention or interventions\naccording to a protocol to evaluate the effect of the intervention(s) on biomedical or other health related outcomes.\n* `Observational`: Studies in human beings in which biomedical and/or health outcomes are assessed in pre-defined groups\nof individuals. Participants in the study may receive diagnostic, therapeutic, or other interventions, but the\ninvestigator does not assign specific interventions to the study participants. This includes when participants\nreceive interventions as part of routine medical care, and a researcher studies the effect of the intervention.\n* `Expanded Access`: An investigational drug product (including biological product)\navailable through expanded access for patients who do not qualify for enrollment in a clinical trial.\nExpanded Access includes all expanded access types under section 561 of the Federal Food, Drug, and\nCosmetic Act: (1) for individual patients, including emergency use; (2) for intermediate-size patient populations;\nand (3) under a treatment IND or treatment protocol. (For more information on data requirements for this Study Type,\nsee Expanded Access Data Element Definitions).",
                                        "name": "StudyType",
                                        "symbols": [
                                            "interventional",
                                            "observational",
                                            "patient_registry",
                                            "expanded_access"
                                        ],
                                        "type": "enum"
                                    }
                                ]
                            },
                            {
                                "doc": "Elegigility Criteria based on Age and Sex",
                                "name": "demogrphicElegibilityCriteria",
                                "type": [
                                    "null",
                                    {
                                        "fields": [
                                            {
                                                "name": "sex",
                                                "type": {
                                                    "doc": "Sex",
                                                    "name": "Sex",
                                                    "namespace": "org.gel.models.participant.avro",
                                                    "symbols": [
                                                        "MALE",
                                                        "FEMALE",
                                                        "UNKNOWN"
                                                    ],
                                                    "type": "enum"
                                                }
                                            },
                                            {
                                                "name": "ageRange",
                                                "type": [
                                                    "null",
                                                    {
                                                        "fields": [
                                                            {
                                                                "name": "minimumAge",
                                                                "type": "int"
                                                            },
                                                            {
                                                                "name": "maximumAge",
                                                                "type": "int"
                                                            },
                                                            {
                                                                "name": "timeunit",
                                                                "type": {
                                                                    "name": "TimeUnit",
                                                                    "symbols": [
                                                                        "years",
                                                                        "months",
                                                                        "weeks",
                                                                        "days",
                                                                        "hours",
                                                                        "minutes",
                                                                        "na"
                                                                    ],
                                                                    "type": "enum"
                                                                }
                                                            }
                                                        ],
                                                        "name": "AgeRange",
                                                        "type": "record"
                                                    }
                                                ]
                                            }
                                        ],
                                        "name": "DemographicElegibilityCriteria",
                                        "type": "record"
                                    }
                                ]
                            },
                            {
                                "doc": "List with all of the locations where participant can enrolle",
                                "name": "locations",
                                "type": [
                                    "null",
                                    {
                                        "items": {
                                            "fields": [
                                                {
                                                    "name": "name",
                                                    "type": [
                                                        "null",
                                                        "string"
                                                    ]
                                                },
                                                {
                                                    "name": "city",
                                                    "type": [
                                                        "null",
                                                        "string"
                                                    ]
                                                },
                                                {
                                                    "name": "country",
                                                    "type": [
                                                        "null",
                                                        "string"
                                                    ]
                                                },
                                                {
                                                    "name": "zip",
                                                    "type": [
                                                        "null",
                                                        "string"
                                                    ]
                                                }
                                            ],
                                            "name": "TrialLocation",
                                            "type": "record"
                                        },
                                        "type": "array"
                                    }
                                ]
                            },
                            {
                                "doc": "If true, the association was made using the variant information,\nif not the association was made at Genomic Entity level",
                                "name": "variantActionable",
                                "type": "boolean"
                            }
                        ],
                        "name": "Trial",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "name": "prognosis",
            "type": [
                "null",
                {
                    "items": {
                        "fields": [
                            {
                                "doc": "URL where reference information for this prognosis can be found",
                                "name": "referenceUrl",
                                "type": "string"
                            },
                            {
                                "doc": "Prognosis classification (defined as favourable or unfavourable),\nin the case that the direction of the prognosis is not known altered_prognosis should be used",
                                "name": "prognosis",
                                "type": [
                                    "null",
                                    {
                                        "name": "PrognosisClassification",
                                        "symbols": [
                                            "altered_prognosis",
                                            "favourable_prognosis",
                                            "unfavourable_prognosis"
                                        ],
                                        "type": "enum"
                                    }
                                ]
                            },
                            {
                                "doc": "Source if known",
                                "name": "source",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "References",
                                "name": "references",
                                "type": [
                                    "null",
                                    {
                                        "items": "string",
                                        "type": "array"
                                    }
                                ]
                            },
                            {
                                "doc": "Conditions",
                                "name": "conditions",
                                "type": [
                                    "null",
                                    {
                                        "items": "string",
                                        "type": "array"
                                    }
                                ]
                            },
                            {
                                "doc": "Full description of the associated prognosis",
                                "name": "description",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "If true, the association was made at the variant level, if not the association was made at Genomic Entity level",
                                "name": "variantActionable",
                                "type": "boolean"
                            }
                        ],
                        "name": "Prognosis",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "name": "therapies",
            "type": [
                "null",
                {
                    "items": {
                        "fields": [
                            {
                                "doc": "URL where reference information for this therapy association can be found",
                                "name": "referenceUrl",
                                "type": "string"
                            },
                            {
                                "doc": "Source",
                                "name": "source",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "References",
                                "name": "references",
                                "type": [
                                    "null",
                                    {
                                        "items": "string",
                                        "type": "array"
                                    }
                                ]
                            },
                            {
                                "doc": "Conditions",
                                "name": "conditions",
                                "type": [
                                    "null",
                                    {
                                        "items": "string",
                                        "type": "array"
                                    }
                                ]
                            },
                            {
                                "doc": "Drug responses",
                                "name": "drugResponse",
                                "type": [
                                    "null",
                                    {
                                        "items": {
                                            "fields": [
                                                {
                                                    "doc": "Treatment agent",
                                                    "name": "TreatmentAgent",
                                                    "type": "string"
                                                },
                                                {
                                                    "doc": "associated effect of the drug",
                                                    "name": "drugResponseClassification",
                                                    "type": {
                                                        "name": "DrugResponseClassification",
                                                        "symbols": [
                                                            "altered_sensitivity",
                                                            "reduced_sensitivity",
                                                            "increased_sensitivity",
                                                            "altered_resistance",
                                                            "increased_resistance",
                                                            "reduced_resistance",
                                                            "increased_risk_of_toxicity",
                                                            "reduced_risk_of_toxicity",
                                                            "altered_toxicity",
                                                            "adverse_drug_reaction",
                                                            "indication",
                                                            "contraindication",
                                                            "dosing_alteration",
                                                            "increased_dose",
                                                            "reduced_dose",
                                                            "increased_monitoring",
                                                            "increased_efficacy",
                                                            "reduced_efficacy",
                                                            "altered_efficacy"
                                                        ],
                                                        "type": "enum"
                                                    }
                                                }
                                            ],
                                            "name": "DrugResponse",
                                            "type": "record"
                                        },
                                        "type": "array"
                                    }
                                ]
                            },
                            {
                                "doc": "Any other clinical intervention",
                                "name": "otherInterventions",
                                "type": [
                                    "null",
                                    {
                                        "items": "Intervention",
                                        "type": "array"
                                    }
                                ]
                            },
                            {
                                "doc": "If true, the association was made at the variant level, if not the association was made at Genomic Entity level",
                                "name": "variantActionable",
                                "type": "boolean"
                            }
                        ],
                        "name": "Therapy",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "name": "diagnostic",
            "type": [
                "null",
                {
                    "items": {
                        "fields": [
                            {
                                "doc": "URL where reference information for this prognosis can be found",
                                "name": "referenceUrl",
                                "type": "string"
                            },
                            {
                                "doc": "Sources if known",
                                "name": "sources",
                                "type": [
                                    "null",
                                    {
                                        "items": "string",
                                        "type": "array"
                                    }
                                ]
                            },
                            {
                                "doc": "References",
                                "name": "references",
                                "type": [
                                    "null",
                                    {
                                        "items": "string",
                                        "type": "array"
                                    }
                                ]
                            },
                            {
                                "doc": "Biomarkers",
                                "name": "biomarkers",
                                "type": [
                                    "null",
                                    {
                                        "items": "string",
                                        "type": "array"
                                    }
                                ]
                            },
                            {
                                "doc": "Associated conditions",
                                "name": "conditions",
                                "type": [
                                    "null",
                                    {
                                        "items": "string",
                                        "type": "array"
                                    }
                                ]
                            },
                            {
                                "doc": "Diagnosis",
                                "name": "diagnosis",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Diagnosis status",
                                "name": "diagnosisStatus",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Other condition",
                                "name": "otherCondition",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "If true, the association was made at the variant level, if not the association was made at Genomic Entity level",
                                "name": "variantActionable",
                                "type": "boolean"
                            }
                        ],
                        "name": "Diagnostic",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "name": "otherAction",
            "type": [
                "null",
                {
                    "items": {
                        "fields": [
                            {
                                "doc": "URL where reference information for this action can be found",
                                "name": "referenceUrl",
                                "type": "string"
                            },
                            {
                                "doc": "Action identifier",
                                "name": "identifier",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Sources if known",
                                "name": "sources",
                                "type": [
                                    "null",
                                    {
                                        "items": "string",
                                        "type": "array"
                                    }
                                ]
                            },
                            {
                                "doc": "Action type",
                                "name": "actionType",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Associated conditions",
                                "name": "conditions",
                                "type": [
                                    "null",
                                    {
                                        "items": "string",
                                        "type": "array"
                                    }
                                ]
                            },
                            {
                                "doc": "Other attributes",
                                "name": "otherAttributes",
                                "type": [
                                    "null",
                                    {
                                        "type": "map",
                                        "values": "string"
                                    }
                                ]
                            },
                            {
                                "doc": "If true, the association was made at the variant level, if not the association was made at Genomic Entity level",
                                "name": "variantActionable",
                                "type": "boolean"
                            }
                        ],
                        "name": "OtherAction",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        }
    ],
    "name": "Actions",
    "namespace": "org.gel.models.report.avro",
    "type": "record"
}