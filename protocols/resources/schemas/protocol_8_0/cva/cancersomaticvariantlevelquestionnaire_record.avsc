{
    "doc": "A record holding the somatic variant level questions for a single variant together with its normalized variant coordinates",
    "fields": [
        {
            "doc": "The coordinates of a given variant: assembly, chromosome, position, reference and alternate",
            "name": "variantCoordinates",
            "type": {
                "doc": "The variant coordinates representing uniquely a small variant.\nNo multi-allelic variant supported, alternate only represents one alternate allele.",
                "fields": [
                    {
                        "doc": "Chromosome without \"chr\" prefix (e.g. X rather than chrX)",
                        "name": "chromosome",
                        "type": "string"
                    },
                    {
                        "doc": "Genomic position",
                        "name": "position",
                        "type": "int"
                    },
                    {
                        "doc": "The reference bases.",
                        "name": "reference",
                        "type": "string"
                    },
                    {
                        "doc": "The alternate bases",
                        "name": "alternate",
                        "type": "string"
                    },
                    {
                        "doc": "The assembly to which this variant corresponds",
                        "name": "assembly",
                        "type": {
                            "doc": "The reference genome assembly",
                            "name": "Assembly",
                            "symbols": [
                                "GRCh38",
                                "GRCh37"
                            ],
                            "type": "enum"
                        }
                    }
                ],
                "name": "VariantCoordinates",
                "namespace": "org.gel.models.report.avro",
                "type": "record"
            }
        },
        {
            "doc": "The questions at variant level",
            "name": "variantLevelQuestions",
            "type": {
                "doc": "The questions for the cancer program exit questionnaire for somatic variants",
                "fields": [
                    {
                        "doc": "Variant coordinates following format `chromosome:position:reference:alternate`",
                        "name": "variantCoordinates",
                        "type": "VariantCoordinates"
                    },
                    {
                        "doc": "Type of potential actionability:",
                        "name": "variantActionability",
                        "type": {
                            "items": {
                                "doc": "The variant actionabilities:\n* `predicts_therapeutic_response`: Predicts therapeutic response\n* `prognostic`: Prognostic\n* `defines_diagnosis_group`: Defines diagnosis group\n* `eligibility_for_trial`: Eligibility for trial\n* `other`:  Other (please specify)",
                                "name": "CancerActionabilitySomatic",
                                "symbols": [
                                    "predicts_therapeutic_response",
                                    "prognostic",
                                    "defines_diagnosis_group",
                                    "eligibility_for_trial",
                                    "other"
                                ],
                                "type": "enum"
                            },
                            "type": "array"
                        }
                    },
                    {
                        "doc": "Other information about variant actionability",
                        "name": "otherVariantActionability",
                        "type": [
                            "null",
                            "string"
                        ]
                    },
                    {
                        "doc": "How has/will this potentially actionable variant been/be used?",
                        "name": "variantUsability",
                        "type": {
                            "doc": "Variant usability for somatic variants:\n* `already_actioned`: Already actioned (i.e. prior to receiving this WGA)\n* `actioned_result_of_this_wga`: actioned as a result of receiving this WGA\n* `not_yet_actioned`: not yet actioned, but potentially actionable in the future",
                            "name": "CancerUsabilitySomatic",
                            "symbols": [
                                "already_actioned",
                                "actioned_result_of_this_wga",
                                "not_yet_actioned"
                            ],
                            "type": "enum"
                        }
                    },
                    {
                        "doc": "Has this variant been tested by another method (either prior to or following receipt of this WGA)?",
                        "name": "variantTested",
                        "type": {
                            "doc": "Was the variant validated with an orthogonal technology?\n* `not_indicated_for_patient_care`: No: not indicated for patient care at this time\n* `no_orthologous_test_available`: No: no orthologous test available\n* `test_performed_prior_to_wga`: Yes: test performed prior to receiving WGA (eg using standard-of-care assay such as panel testing, or sanger sequencing)\n* `technical_validation_following_WGA`: Yes: technical validation performed/planned following receiving this WGA",
                            "name": "CancerTested",
                            "symbols": [
                                "not_indicated_for_patient_care",
                                "no_orthologous_test_available",
                                "test_performed_prior_to_wga",
                                "technical_validation_following_wga"
                            ],
                            "type": "enum"
                        }
                    },
                    {
                        "doc": "Please enter validation assay type e.g Pyrosequencing, NGS panel, COBAS, Sanger sequencing. If not applicable enter NA;",
                        "name": "validationAssayType",
                        "type": "string"
                    }
                ],
                "name": "CancerSomaticVariantLevelQuestions",
                "namespace": "org.gel.models.report.avro",
                "type": "record"
            }
        }
    ],
    "name": "CancerSomaticVariantLevelQuestionnaire",
    "namespace": "org.gel.models.cva.avro",
    "type": "record"
}