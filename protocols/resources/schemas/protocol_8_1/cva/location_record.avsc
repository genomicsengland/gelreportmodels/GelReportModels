{
    "doc": "Location of a variant which may be coordinates or an entity description if coordinates are not available.\n*",
    "fields": [
        {
            "doc": "Genomic coordinates of the variant if available",
            "name": "coordinates",
            "type": [
                "null",
                {
                    "fields": [
                        {
                            "doc": "The assembly to which this variant corresponds",
                            "name": "assembly",
                            "type": {
                                "doc": "The reference genome assembly",
                                "name": "Assembly",
                                "symbols": [
                                    "GRCh38",
                                    "GRCh37"
                                ],
                                "type": "enum"
                            }
                        },
                        {
                            "doc": "Chromosome without \"chr\" prefix (e.g. X rather than chrX)",
                            "name": "chromosome",
                            "type": "string"
                        },
                        {
                            "doc": "Start genomic position for variant (1-based)",
                            "name": "start",
                            "type": "int"
                        },
                        {
                            "doc": "End genomic position for variant",
                            "name": "end",
                            "type": "int"
                        },
                        {
                            "name": "ciStart",
                            "type": [
                                "null",
                                {
                                    "fields": [
                                        {
                                            "name": "left",
                                            "type": "int"
                                        },
                                        {
                                            "name": "right",
                                            "type": "int"
                                        }
                                    ],
                                    "name": "ConfidenceInterval",
                                    "type": "record"
                                }
                            ]
                        },
                        {
                            "name": "ciEnd",
                            "type": [
                                "null",
                                "ConfidenceInterval"
                            ]
                        }
                    ],
                    "name": "Coordinates",
                    "type": "record"
                }
            ]
        },
        {
            "doc": "Entity description if coordinates are not available",
            "name": "entity",
            "type": [
                "null",
                {
                    "doc": "A genomic feature",
                    "fields": [
                        {
                            "doc": "The type of the genomic entity",
                            "name": "type",
                            "type": {
                                "doc": "Types of genomic features:\n\n* `regulatory_region`: a regulatory region\n* `gene`: a gene\n* `transcript`: a transcript\n* `intergenic`: an intergenic region",
                                "name": "GenomicEntityType",
                                "symbols": [
                                    "regulatory_region",
                                    "gene",
                                    "transcript",
                                    "intergenic",
                                    "gene_fusion",
                                    "genomic_region",
                                    "cytobands"
                                ],
                                "type": "enum"
                            }
                        },
                        {
                            "doc": "Ensembl identifier for the feature (e.g, ENST00000544455)",
                            "name": "ensemblId",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "The HGNC gene symbol. This field is optional, BUT it should be filled if possible",
                            "name": "geneSymbol",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Others identifiers for this genomic feature",
                            "name": "otherIds",
                            "type": [
                                "null",
                                {
                                    "items": {
                                        "fields": [
                                            {
                                                "doc": "Source i.e, esenmbl",
                                                "name": "source",
                                                "type": "string"
                                            },
                                            {
                                                "doc": "identifier",
                                                "name": "identifier",
                                                "type": "string"
                                            }
                                        ],
                                        "name": "Identifier",
                                        "type": "record"
                                    },
                                    "type": "array"
                                }
                            ]
                        }
                    ],
                    "name": "GenomicEntity",
                    "type": "record"
                }
            ]
        }
    ],
    "name": "Location",
    "namespace": "org.gel.models.report.avro",
    "type": "record"
}