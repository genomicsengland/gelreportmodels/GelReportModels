{
    "doc": "Structural variant level questions",
    "fields": [
        {
            "doc": "Structural variant type",
            "name": "variantType",
            "type": {
                "name": "StructuralVariantType",
                "symbols": [
                    "ins",
                    "dup",
                    "inv",
                    "amplification",
                    "deletion",
                    "dup_tandem",
                    "del_me",
                    "ins_me",
                    "cnloh"
                ],
                "type": "enum"
            }
        },
        {
            "doc": "Variant coordinates",
            "name": "coordinates",
            "type": {
                "fields": [
                    {
                        "doc": "The assembly to which this variant corresponds",
                        "name": "assembly",
                        "type": {
                            "doc": "The reference genome assembly",
                            "name": "Assembly",
                            "symbols": [
                                "GRCh38",
                                "GRCh37"
                            ],
                            "type": "enum"
                        }
                    },
                    {
                        "doc": "Chromosome without \"chr\" prefix (e.g. X rather than chrX)",
                        "name": "chromosome",
                        "type": "string"
                    },
                    {
                        "doc": "Start genomic position for variant (1-based)",
                        "name": "start",
                        "type": "int"
                    },
                    {
                        "doc": "End genomic position for variant",
                        "name": "end",
                        "type": "int"
                    },
                    {
                        "name": "ciStart",
                        "type": [
                            "null",
                            {
                                "fields": [
                                    {
                                        "name": "left",
                                        "type": "int"
                                    },
                                    {
                                        "name": "right",
                                        "type": "int"
                                    }
                                ],
                                "name": "ConfidenceInterval",
                                "type": "record"
                            }
                        ]
                    },
                    {
                        "name": "ciEnd",
                        "type": [
                            "null",
                            "ConfidenceInterval"
                        ]
                    }
                ],
                "name": "Coordinates",
                "type": "record"
            }
        },
        {
            "doc": "Did you carry out technical confirmation of this variant via an alternative test?",
            "name": "confirmationDecision",
            "type": {
                "name": "ConfirmationDecision",
                "symbols": [
                    "yes",
                    "no",
                    "na"
                ],
                "type": "enum"
            }
        },
        {
            "doc": "Did the test confirm that the variant is present?",
            "name": "confirmationOutcome",
            "type": {
                "name": "ConfirmationOutcome",
                "symbols": [
                    "yes",
                    "no",
                    "na"
                ],
                "type": "enum"
            }
        },
        {
            "doc": "Did you include the variant in your report to the clinician?",
            "name": "reportingQuestion",
            "type": {
                "name": "ReportingQuestion",
                "symbols": [
                    "yes",
                    "no",
                    "na"
                ],
                "type": "enum"
            }
        },
        {
            "doc": "What ACMG pathogenicity score (1-5) did you assign to this variant?",
            "name": "acmgClassification",
            "type": {
                "name": "ACMGClassification",
                "symbols": [
                    "pathogenic_variant",
                    "likely_pathogenic_variant",
                    "variant_of_unknown_clinical_significance",
                    "likely_benign_variant",
                    "benign_variant",
                    "not_assessed",
                    "na"
                ],
                "type": "enum"
            }
        },
        {
            "doc": "Please provide PMIDs for papers which you have used to inform your assessment for this variant, separated by a `;` for multiple papers",
            "name": "publications",
            "type": "string"
        }
    ],
    "name": "StructuralVariantLevelQuestions",
    "namespace": "org.gel.models.report.avro",
    "type": "record"
}