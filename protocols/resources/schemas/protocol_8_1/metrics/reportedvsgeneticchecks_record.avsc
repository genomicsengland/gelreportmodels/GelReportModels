{
    "doc": "Reported versus Genetic checks object\n========================================",
    "fields": [
        {
            "doc": "Summary of the software, versions and samples used for the RvsG checks",
            "name": "ReportedVsGeneticOutcome",
            "type": [
                "null",
                {
                    "doc": "Reported vs Genetic Summary",
                    "name": "ReportedVsGeneticOutcome",
                    "symbols": [
                        "familyPassesGvsRChecks",
                        "familyFailsACheck",
                        "familyMissingACheck"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "Coverage-based sex metrics and inferred karyotype",
            "name": "coverageBasedSex",
            "type": [
                "null",
                {
                    "items": {
                        "doc": "Coverage-based sex\n========================================\nGeneral information about the checks, versions, tools, and number of markers\nTODO: Consider removing Inferred karyotype",
                        "fields": [
                            {
                                "doc": "Sample Id (e.g, LP00012645_5GH))",
                                "name": "sampleId",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Inferred karyotype using coverage information",
                                "name": "inferredKaryotype",
                                "type": [
                                    "null",
                                    {
                                        "doc": "Kariotypic sex\nTODO: Check if we want to have different karyotype definitions for XO clearcut/doubtful",
                                        "name": "KaryotypicSex",
                                        "symbols": [
                                            "UNKNOWN",
                                            "XX",
                                            "XY",
                                            "XO",
                                            "XXY",
                                            "XXX",
                                            "XXYY",
                                            "XXXY",
                                            "XXXX",
                                            "XYY",
                                            "OTHER"
                                        ],
                                        "type": "enum"
                                    }
                                ]
                            },
                            {
                                "doc": "Ratio of the average coverage of chromosome X to the average of the autosomal chromosome coverage",
                                "name": "ratioChrX",
                                "type": [
                                    "null",
                                    "double"
                                ]
                            },
                            {
                                "doc": "Ratio of the average coverage of chromosome Y to the average of the autosomal chromosome coverage",
                                "name": "ratioChrY",
                                "type": [
                                    "null",
                                    "double"
                                ]
                            },
                            {
                                "doc": "Number of copies of chromosome X",
                                "name": "avgCnvChrX",
                                "type": [
                                    "null",
                                    "double"
                                ]
                            },
                            {
                                "doc": "Number of copies of chromosome Y",
                                "name": "avgCnvChrY",
                                "type": [
                                    "null",
                                    "double"
                                ]
                            },
                            {
                                "doc": "Reviewed sex karyotype",
                                "name": "reviewedKaryotype",
                                "type": [
                                    "null",
                                    "KaryotypicSex"
                                ]
                            }
                        ],
                        "name": "CoverageBasedSex",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Per family and per sample mendelian inconsistencies",
            "name": "mendelianInconsistencies",
            "type": [
                "null",
                {
                    "fields": [
                        {
                            "doc": "Number of mendelian inconsitencies per nuclear family. One entry per nuclear family",
                            "name": "perFamilyMendelErrors",
                            "type": [
                                "null",
                                {
                                    "items": {
                                        "doc": "Mendelian inconsistencies\n========================================\n- fmendel reports a line per nuclear family\n- imendel reports a line per member of the family and nuclear family",
                                        "fields": [
                                            {
                                                "doc": "Sample Id of the father",
                                                "name": "fatherId",
                                                "type": [
                                                    "null",
                                                    "string"
                                                ]
                                            },
                                            {
                                                "doc": "Sample Id of the mother",
                                                "name": "motherId",
                                                "type": [
                                                    "null",
                                                    "string"
                                                ]
                                            },
                                            {
                                                "doc": "Number of children in the nuclear family",
                                                "name": "numberOfOffspring",
                                                "type": [
                                                    "null",
                                                    "double"
                                                ]
                                            },
                                            {
                                                "doc": "Number of Mendelian errors in the nuclear family",
                                                "name": "numberOfMendelErrors",
                                                "type": [
                                                    "null",
                                                    "double"
                                                ]
                                            }
                                        ],
                                        "name": "PerFamilyMendelErrors",
                                        "type": "record"
                                    },
                                    "type": "array"
                                }
                            ]
                        },
                        {
                            "doc": "Number of mendelian inconsitencies per sample and nuclear family. One entry per sample and nuclear family",
                            "name": "individualMendelErrors",
                            "type": [
                                "null",
                                {
                                    "items": {
                                        "fields": [
                                            {
                                                "doc": "Sample Id",
                                                "name": "sampleId",
                                                "type": [
                                                    "null",
                                                    "string"
                                                ]
                                            },
                                            {
                                                "doc": "Number of Mendelian errors per sample in a nuclear family",
                                                "name": "numberOfMendelErrors",
                                                "type": [
                                                    "null",
                                                    "double"
                                                ]
                                            },
                                            {
                                                "doc": "Rate of Mendelian errors per sample in a nuclear family to the number of sites tested (number of markers)",
                                                "name": "rateOfMendelErrors",
                                                "type": [
                                                    "null",
                                                    "double"
                                                ]
                                            }
                                        ],
                                        "name": "IndividualMendelErrors",
                                        "type": "record"
                                    },
                                    "type": "array"
                                }
                            ]
                        },
                        {
                            "doc": "Aggregated number of mendelian inconsitencies per sample and family",
                            "name": "totalNumberOfMendelErrors",
                            "type": [
                                "null",
                                {
                                    "fields": [
                                        {
                                            "doc": "Total number of Mendelian errors in the family, this should be the sum of the mendelian errors in each\nnuclear family",
                                            "name": "familyMendelErrors",
                                            "type": [
                                                "null",
                                                "double"
                                            ]
                                        },
                                        {
                                            "doc": "Number of Mendelian errors per sample considering all nuclear families. Should be one entry per sample in\nthe family",
                                            "name": "individualMendelErrors",
                                            "type": [
                                                "null",
                                                {
                                                    "items": {
                                                        "fields": [
                                                            {
                                                                "doc": "Sample Id",
                                                                "name": "sampleId",
                                                                "type": [
                                                                    "null",
                                                                    "string"
                                                                ]
                                                            },
                                                            {
                                                                "doc": "Total number of Mendelian errors per sample considering all nuclear families",
                                                                "name": "totalnumberOfMendelErrors",
                                                                "type": [
                                                                    "null",
                                                                    "double"
                                                                ]
                                                            }
                                                        ],
                                                        "name": "AggregatedIndividualMendelErrors",
                                                        "type": "record"
                                                    },
                                                    "type": "array"
                                                }
                                            ]
                                        }
                                    ],
                                    "name": "TotalNumberOfMendelErrors",
                                    "type": "record"
                                }
                            ]
                        },
                        {
                            "doc": "Summary of the type of mendelian inconstencies happening in the family. One entry per sample, chromosome and\ncode",
                            "name": "locusMendelSummary",
                            "type": [
                                "null",
                                {
                                    "items": {
                                        "fields": [
                                            {
                                                "doc": "Sample Id",
                                                "name": "sampleId",
                                                "type": [
                                                    "null",
                                                    "string"
                                                ]
                                            },
                                            {
                                                "doc": "Chromosome (1-22, X, Y, XY)",
                                                "name": "chr",
                                                "type": [
                                                    "null",
                                                    "string"
                                                ]
                                            },
                                            {
                                                "doc": "Numeric error code (more information here: http://zzz.bwh.harvard.edu/plink/summary.shtml#mendel)",
                                                "name": "code",
                                                "type": [
                                                    "null",
                                                    "double"
                                                ]
                                            },
                                            {
                                                "doc": "Number of errors of the type \"code\" in that chromosome",
                                                "name": "numberOfErrors",
                                                "type": [
                                                    "null",
                                                    "double"
                                                ]
                                            }
                                        ],
                                        "name": "LocusMendelSummary",
                                        "type": "record"
                                    },
                                    "type": "array"
                                }
                            ]
                        }
                    ],
                    "name": "MendelianInconsistencies",
                    "type": "record"
                }
            ]
        },
        {
            "doc": "Within-family relatedness",
            "name": "familyRelatedness",
            "type": [
                "null",
                {
                    "fields": [
                        {
                            "doc": "Pairwise relatedness within the family. One entry per pair of samples",
                            "name": "relatedness",
                            "type": [
                                "null",
                                {
                                    "items": {
                                        "doc": "Relatedness\n========================================",
                                        "fields": [
                                            {
                                                "doc": "Sample Id of one of the samples in the pair",
                                                "name": "sampleId1",
                                                "type": [
                                                    "null",
                                                    "string"
                                                ]
                                            },
                                            {
                                                "doc": "Sample Id of the other sample in the pair",
                                                "name": "sampleId2",
                                                "type": [
                                                    "null",
                                                    "string"
                                                ]
                                            },
                                            {
                                                "doc": "Estimated proportion of autosome with 0 alleles shared",
                                                "name": "ibd0",
                                                "type": [
                                                    "null",
                                                    "double"
                                                ]
                                            },
                                            {
                                                "doc": "Estimated proportion of autosome with 1 allele shared",
                                                "name": "ibd1",
                                                "type": [
                                                    "null",
                                                    "double"
                                                ]
                                            },
                                            {
                                                "doc": "Estimated proportion of autosome with 2 alleles shared",
                                                "name": "ibd2",
                                                "type": [
                                                    "null",
                                                    "double"
                                                ]
                                            },
                                            {
                                                "doc": "Estimated overall proportion of shared autosomal DNA",
                                                "name": "piHat",
                                                "type": [
                                                    "null",
                                                    "double"
                                                ]
                                            }
                                        ],
                                        "name": "RelatednessPair",
                                        "type": "record"
                                    },
                                    "type": "array"
                                }
                            ]
                        }
                    ],
                    "name": "FamilyRelatedness",
                    "type": "record"
                }
            ]
        },
        {
            "doc": "Evaluation of the reported vs genetic information",
            "name": "evaluation",
            "type": [
                "null",
                {
                    "doc": "Evaluation\n========================================\nReportedVsGeneticOutcome: familyPassesGvsRChecks, familyFailsACheck, familyMissingACheck",
                    "fields": [
                        {
                            "doc": "Coverage-based sex evaluation. One entry per sample",
                            "name": "coverageBasedSexCheck",
                            "type": [
                                "null",
                                {
                                    "items": {
                                        "doc": "Evaluation\n========================================",
                                        "fields": [
                                            {
                                                "doc": "Sample Id",
                                                "name": "sampleId",
                                                "type": [
                                                    "null",
                                                    "string"
                                                ]
                                            },
                                            {
                                                "doc": "Reported phenotypic sex",
                                                "name": "reportedPhenotypicSex",
                                                "type": [
                                                    "null",
                                                    {
                                                        "doc": "Phenotypic sex",
                                                        "name": "Sex",
                                                        "symbols": [
                                                            "UNKNOWN",
                                                            "MALE",
                                                            "FEMALE",
                                                            "OTHER"
                                                        ],
                                                        "type": "enum"
                                                    }
                                                ]
                                            },
                                            {
                                                "doc": "Reported karyotypic sex",
                                                "name": "reportedKaryotypicSex",
                                                "type": [
                                                    "null",
                                                    "KaryotypicSex"
                                                ]
                                            },
                                            {
                                                "doc": "Inferred coverage-based sex karyotype",
                                                "name": "inferredSexKaryotype",
                                                "type": [
                                                    "null",
                                                    "KaryotypicSex"
                                                ]
                                            },
                                            {
                                                "doc": "Whether the sample is a sex query (yes, no, unknown, notTested)",
                                                "name": "sexQuery",
                                                "type": [
                                                    "null",
                                                    {
                                                        "doc": "A query",
                                                        "name": "Query",
                                                        "symbols": [
                                                            "yes",
                                                            "no",
                                                            "unknown",
                                                            "notTested"
                                                        ],
                                                        "type": "enum"
                                                    }
                                                ]
                                            },
                                            {
                                                "doc": "Comments",
                                                "name": "comments",
                                                "type": [
                                                    "null",
                                                    "string"
                                                ]
                                            }
                                        ],
                                        "name": "CoverageBasedSexCheck",
                                        "type": "record"
                                    },
                                    "type": "array"
                                }
                            ]
                        },
                        {
                            "doc": "Mendelian inconsitencies evaluation. One entry per sample",
                            "name": "mendelianInconsistenciesCheck",
                            "type": [
                                "null",
                                {
                                    "items": {
                                        "fields": [
                                            {
                                                "doc": "Sample Id",
                                                "name": "sampleId",
                                                "type": [
                                                    "null",
                                                    "string"
                                                ]
                                            },
                                            {
                                                "doc": "Whether the sample is a Mendelian inconsistencies query (yes, no, unknown, notTested)",
                                                "name": "mendelianInconsistenciesQuery",
                                                "type": [
                                                    "null",
                                                    "Query"
                                                ]
                                            },
                                            {
                                                "doc": "Mendelian inconsistencies cannot always be computed for all the samples in the family (depends on family\nstructure). Specify here if this is the case or there was any other issues",
                                                "name": "comments",
                                                "type": [
                                                    "null",
                                                    "string"
                                                ]
                                            }
                                        ],
                                        "name": "MendelianInconsistenciesCheck",
                                        "type": "record"
                                    },
                                    "type": "array"
                                }
                            ]
                        },
                        {
                            "doc": "Within-family relatedness evaluation. One entry per pair of samples",
                            "name": "familyRelatednessCheck",
                            "type": [
                                "null",
                                {
                                    "items": {
                                        "fields": [
                                            {
                                                "doc": "Sample Id of one of the samples in the pair",
                                                "name": "sampleId1",
                                                "type": [
                                                    "null",
                                                    "string"
                                                ]
                                            },
                                            {
                                                "doc": "Sample Id of the other sample in the pair",
                                                "name": "sampleId2",
                                                "type": [
                                                    "null",
                                                    "string"
                                                ]
                                            },
                                            {
                                                "doc": "Reported relationship from sampleId1 to sampleId2 according to the pedigree provided",
                                                "name": "relationshipFromPedigree",
                                                "type": [
                                                    "null",
                                                    {
                                                        "doc": "Familiar relationship from pedrigree",
                                                        "name": "FamiliarRelationship",
                                                        "namespace": "org.gel.models.participant.avro",
                                                        "symbols": [
                                                            "TwinsMonozygous",
                                                            "TwinsDizygous",
                                                            "TwinsUnknown",
                                                            "FullSibling",
                                                            "FullSiblingF",
                                                            "FullSiblingM",
                                                            "Mother",
                                                            "Father",
                                                            "Son",
                                                            "Daughter",
                                                            "ChildOfUnknownSex",
                                                            "MaternalAunt",
                                                            "MaternalUncle",
                                                            "MaternalUncleOrAunt",
                                                            "PaternalAunt",
                                                            "PaternalUncle",
                                                            "PaternalUncleOrAunt",
                                                            "MaternalGrandmother",
                                                            "PaternalGrandmother",
                                                            "MaternalGrandfather",
                                                            "PaternalGrandfather",
                                                            "DoubleFirstCousin",
                                                            "MaternalCousinSister",
                                                            "PaternalCousinSister",
                                                            "MaternalCousinBrother",
                                                            "PaternalCousinBrother",
                                                            "Cousin",
                                                            "Spouse",
                                                            "Other",
                                                            "RelationIsNotClear",
                                                            "Unrelated",
                                                            "Unknown"
                                                        ],
                                                        "type": "enum"
                                                    }
                                                ]
                                            },
                                            {
                                                "doc": "Expected relationship according to IBD",
                                                "name": "possibleRelationship",
                                                "type": [
                                                    "null",
                                                    "string"
                                                ]
                                            },
                                            {
                                                "doc": "Whether the pair of samples are a within-family query (yes, no, unknown, notTested)",
                                                "name": "withinFamilyIBDQuery",
                                                "type": [
                                                    "null",
                                                    "Query"
                                                ]
                                            },
                                            {
                                                "doc": "Comments",
                                                "name": "comments",
                                                "type": [
                                                    "null",
                                                    "string"
                                                ]
                                            }
                                        ],
                                        "name": "FamilyRelatednessCheck",
                                        "type": "record"
                                    },
                                    "type": "array"
                                }
                            ]
                        },
                        {
                            "doc": "Final evaluation summary. Does the family passes RvsG checks or errors are present?",
                            "name": "ReportedVsGeneticOutcome",
                            "type": [
                                "null",
                                "ReportedVsGeneticOutcome"
                            ]
                        }
                    ],
                    "name": "Evaluation",
                    "type": "record"
                }
            ]
        }
    ],
    "name": "ReportedVsGeneticChecks",
    "namespace": "org.gel.models.metrics.avro",
    "type": "record"
}