{
    "doc": "Variant classification based on guidlines, AMP and ACMG are supported",
    "fields": [
        {
            "doc": "Variant classification using ACMG framework",
            "name": "acmgVariantClassification",
            "type": [
                "null",
                {
                    "doc": "Full record for the ACMG variant clasiffication, including all selected evidences and the final classification.",
                    "fields": [
                        {
                            "doc": "Details of ACMG criteria used to score this variant",
                            "name": "acmgEvidences",
                            "type": {
                                "items": {
                                    "doc": "Represents a single ACMG criterion (PVS1, BA1, PM1... etc) used in the classification of a variant,\nalong with the strength applied at and evidence supporting it.",
                                    "fields": [
                                        {
                                            "doc": "Evidence category as defined in ACMG guidelines",
                                            "name": "category",
                                            "type": {
                                                "doc": "Each ACMG criterion is classified in one of these categories",
                                                "name": "AcmgEvidenceCategory",
                                                "symbols": [
                                                    "population_data",
                                                    "computational_and_predictive_data",
                                                    "functional_data",
                                                    "segregation_data",
                                                    "de_novo_data",
                                                    "allelic_data",
                                                    "other_database",
                                                    "other_data"
                                                ],
                                                "type": "enum"
                                            }
                                        },
                                        {
                                            "doc": "Evidence type: benign or pathogenic",
                                            "name": "type",
                                            "type": {
                                                "doc": "Each ACMG cirterion will be classifed as benign or pathogenic",
                                                "name": "AcmgEvidenceType",
                                                "symbols": [
                                                    "benign",
                                                    "pathogenic"
                                                ],
                                                "type": "enum"
                                            }
                                        },
                                        {
                                            "doc": "Default strength for criterion as defined in Table 3 of ACMG guidelines (Richards et al 2015). e.g. PM2 would be \"moderate\"",
                                            "name": "weight",
                                            "type": {
                                                "doc": "Each ACMG criterion is weighted using the following terms:\n\n* `stand_alone`: `A`, stand-alone applied for benign variant critieria `(BA1)`\n* `supporting`: `P`, supporting applied for benign variant critieria `(BP1-6)` and pathogenic variant criteria `(PP1-5)`\n* `moderate`: `M`, moderate applied for pathogenic variant critieria (PM1-6)\n* `strong`: `S`, strong applied for pathogenic variant critieria (PS1-4)\n* `very_strong`: `S`, Very Stong applied for pathogenic variant critieria (PVS1)",
                                                "name": "AcmgEvidenceWeight",
                                                "symbols": [
                                                    "stand_alone",
                                                    "supporting",
                                                    "moderate",
                                                    "strong",
                                                    "very_strong"
                                                ],
                                                "type": "enum"
                                            }
                                        },
                                        {
                                            "doc": "The number suffix at the end of the ACMG criteria code e.g PM2 would be 2",
                                            "name": "modifier",
                                            "type": "int"
                                        },
                                        {
                                            "doc": "The strength this criterion has been used at in this interpretation. e.g. if PM2 was only used at \"supporting\" rather than \"moderate\", the activation strength would be \"supporting\"",
                                            "name": "activationStrength",
                                            "type": {
                                                "doc": "Activation Strength enumeration:\n* `strong`\n* `moderate`\n* `supporting`\n* `very_strong`\n* `stand_alone`",
                                                "name": "ActivationStrength",
                                                "symbols": [
                                                    "strong",
                                                    "moderate",
                                                    "supporting",
                                                    "very_strong",
                                                    "stand_alone"
                                                ],
                                                "type": "enum"
                                            }
                                        },
                                        {
                                            "doc": "The description of the evidence as described in ACMG guidelines e.g. for PM2 the description would be \"Absent from controls (or at extremely low frequency if recessive) in Exome Sequencing Project, 1000 Genomes Project, or Exome Aggregation Consortium\"",
                                            "name": "description",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "doc": "User comments attached to this ACMG criteria in this case",
                                            "name": "comments",
                                            "type": [
                                                "null",
                                                {
                                                    "items": {
                                                        "fields": [
                                                            {
                                                                "doc": "Comment text",
                                                                "name": "comment",
                                                                "type": "string"
                                                            },
                                                            {
                                                                "doc": "User who created comment",
                                                                "name": "user",
                                                                "type": [
                                                                    "null",
                                                                    {
                                                                        "fields": [
                                                                            {
                                                                                "doc": "Azure Active Directory immutable user OID",
                                                                                "name": "userid",
                                                                                "type": [
                                                                                    "null",
                                                                                    "string"
                                                                                ]
                                                                            },
                                                                            {
                                                                                "doc": "User email address",
                                                                                "name": "email",
                                                                                "type": "string"
                                                                            },
                                                                            {
                                                                                "doc": "Username",
                                                                                "name": "username",
                                                                                "type": "string"
                                                                            },
                                                                            {
                                                                                "name": "role",
                                                                                "type": [
                                                                                    "null",
                                                                                    "string"
                                                                                ]
                                                                            },
                                                                            {
                                                                                "name": "groups",
                                                                                "type": [
                                                                                    "null",
                                                                                    {
                                                                                        "items": "string",
                                                                                        "type": "array"
                                                                                    }
                                                                                ]
                                                                            }
                                                                        ],
                                                                        "name": "User",
                                                                        "type": "record"
                                                                    }
                                                                ]
                                                            },
                                                            {
                                                                "doc": "Date and time comment was created (ISO 8601 datetime with seconds and timezone e.g. 2020-11-23T15:52:36+00:00)",
                                                                "name": "timestamp",
                                                                "type": [
                                                                    "null",
                                                                    "string"
                                                                ]
                                                            }
                                                        ],
                                                        "name": "UserComment",
                                                        "type": "record"
                                                    },
                                                    "type": "array"
                                                }
                                            ]
                                        },
                                        {
                                            "doc": "Publications flagged by the user as relevant to this ACMG evidence",
                                            "name": "flaggedPublications",
                                            "type": [
                                                "null",
                                                {
                                                    "items": {
                                                        "doc": "For each publication, either a PMID or DOI should be included, preferably both if available",
                                                        "fields": [
                                                            {
                                                                "doc": "PubMed ID",
                                                                "name": "pmid",
                                                                "type": [
                                                                    "null",
                                                                    "string"
                                                                ]
                                                            },
                                                            {
                                                                "doc": "Digital Object Identifier (DOI) e.g. 10.1056/NEJMra0802968",
                                                                "name": "doi",
                                                                "type": [
                                                                    "null",
                                                                    "string"
                                                                ]
                                                            },
                                                            {
                                                                "doc": "User comments left on this article as part of this variant interpretation",
                                                                "name": "comments",
                                                                "type": [
                                                                    "null",
                                                                    {
                                                                        "items": "UserComment",
                                                                        "type": "array"
                                                                    }
                                                                ]
                                                            }
                                                        ],
                                                        "name": "Publication",
                                                        "type": "record"
                                                    },
                                                    "type": "array"
                                                }
                                            ]
                                        }
                                    ],
                                    "name": "AcmgEvidence",
                                    "type": "record"
                                },
                                "type": "array"
                            }
                        },
                        {
                            "doc": "Final classification selected by user",
                            "name": "clinicalSignificance",
                            "type": {
                                "name": "ClinicalSignificance",
                                "symbols": [
                                    "benign",
                                    "likely_benign",
                                    "likely_pathogenic",
                                    "pathogenic",
                                    "uncertain_significance",
                                    "excluded"
                                ],
                                "type": "enum"
                            }
                        },
                        {
                            "doc": "Classification computed from ACMG scores",
                            "name": "calculatedClinicalSignificance",
                            "type": [
                                "null",
                                "ClinicalSignificance"
                            ]
                        },
                        {
                            "name": "assessment",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "ACMG evidence categories for which the user has indicated there is no evidence available",
                            "name": "noAcmgEvidence",
                            "type": [
                                "null",
                                {
                                    "items": "AcmgEvidenceCategory",
                                    "type": "array"
                                }
                            ]
                        }
                    ],
                    "name": "AcmgVariantClassification",
                    "type": "record"
                }
            ]
        },
        {
            "doc": "Variant classification using AMP framework",
            "name": "ampVariantClassification",
            "type": [
                "null",
                {
                    "doc": "Full Variant classification acording to AMP guideline, including all supporting evidences and the final\nassessment",
                    "fields": [
                        {
                            "doc": "List of AMP evidences",
                            "name": "ampEvidences",
                            "type": {
                                "items": {
                                    "doc": "Evidences as defined in AMP guidelines, they are composed by a evidence type (first column in the evidence table of\nthe guidlines) and a assessment of the evicence, this last one will define the streght of the evidence, supporting\nthe variant to be classified as TierI-IV",
                                    "fields": [
                                        {
                                            "doc": "AMP evidence type according to Guidlines, i.e germline_database_presence",
                                            "name": "type",
                                            "type": {
                                                "doc": "Type of evidence in the AMP guideline",
                                                "name": "AmpEvidenceType",
                                                "symbols": [
                                                    "mutation_type",
                                                    "therapies",
                                                    "variant_frequencies",
                                                    "potential_germline",
                                                    "population_database_presence",
                                                    "germline_database_presence",
                                                    "somatic_database_presence",
                                                    "impact_predictive_software",
                                                    "pathway_involvement",
                                                    "publications"
                                                ],
                                                "type": "enum"
                                            }
                                        },
                                        {
                                            "doc": "Assessment for AMP evidence, i.e Present in ClinVar",
                                            "name": "evidenceAssessment",
                                            "type": "string"
                                        }
                                    ],
                                    "name": "AmpEvidence",
                                    "type": "record"
                                },
                                "type": "array"
                            }
                        },
                        {
                            "doc": "Final Clasification taken in account the evidences",
                            "name": "ampTier",
                            "type": {
                                "doc": "AMP tier:\n* `TierI`: Variants of Strong Clinical Significance\n* `TierII`: Variants of Potential Clinical Significance\n* `TierIII`: Variants of Unknown Clinical Significance\n* `TierIV`: Benign or Likely Benign Variants",
                                "name": "AmpTier",
                                "symbols": [
                                    "tierI",
                                    "tierII",
                                    "tierIII",
                                    "tierIV"
                                ],
                                "type": "enum"
                            }
                        },
                        {
                            "doc": "Clinical or Experimental evicence",
                            "name": "ampClincialOrExperimentalEvidence",
                            "type": [
                                "null",
                                {
                                    "items": {
                                        "doc": "Amp Clinical or Experimental Evidence, the level will define the overal clasification of the variant together with\nthe tiering.",
                                        "fields": [
                                            {
                                                "doc": "As denined in AMP guidelines: therapeutic, diagnosis or prognosis",
                                                "name": "category",
                                                "type": {
                                                    "doc": "Categories of Clinical and/or Experimental Evidence as defined in AMP guidelines",
                                                    "name": "AmpClinicalOrExperimentalEvidenceCategory",
                                                    "symbols": [
                                                        "therapeutic",
                                                        "diagnosis",
                                                        "prognosis"
                                                    ],
                                                    "type": "enum"
                                                }
                                            },
                                            {
                                                "doc": "As denined in AMP guidelines: levelA, levelB, levelC, levelD",
                                                "name": "level",
                                                "type": {
                                                    "doc": "Levels for categories of Clinical and/or Experimental Evidence as defined in AMP guidelines",
                                                    "name": "AmpClinicalOrExperimentalEvidenceLevel",
                                                    "symbols": [
                                                        "levelA",
                                                        "levelB",
                                                        "levelC",
                                                        "levelD"
                                                    ],
                                                    "type": "enum"
                                                }
                                            },
                                            {
                                                "doc": "Description of the evidence",
                                                "name": "description",
                                                "type": [
                                                    "null",
                                                    "string"
                                                ]
                                            }
                                        ],
                                        "name": "AmpClincialOrExperimentalEvidence",
                                        "type": "record"
                                    },
                                    "type": "array"
                                }
                            ]
                        },
                        {
                            "doc": "Final Assessment",
                            "name": "assessment",
                            "type": [
                                "null",
                                "string"
                            ]
                        }
                    ],
                    "name": "AmpVariantClassification",
                    "type": "record"
                }
            ]
        }
    ],
    "name": "GuidelineBasedVariantClassification",
    "namespace": "org.gel.models.report.avro",
    "type": "record"
}