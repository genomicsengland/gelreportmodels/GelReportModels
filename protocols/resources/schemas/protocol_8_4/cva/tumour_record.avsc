{
    "fields": [
        {
            "doc": "TumourId in GMS",
            "name": "tumourId",
            "type": "string"
        },
        {
            "doc": "Local hospital tumour ID from the GLH Laboratory Information Management System (LIMS) in GMS",
            "name": "tumourLocalId",
            "type": "string"
        },
        {
            "doc": "tumourType",
            "name": "tumourType",
            "type": {
                "doc": "NOTE: This has been changed completely, the previous tumour type has been split into TumourPresentation and PrimaryOrMetastatic",
                "name": "TumourType",
                "symbols": [
                    "BRAIN_TUMOUR",
                    "HAEMATOLOGICAL_MALIGNANCY_SOLID_SAMPLE",
                    "HAEMATOLOGICAL_MALIGNANCY_LIQUID_SAMPLE",
                    "SOLID_TUMOUR_METASTATIC",
                    "SOLID_TUMOUR_PRIMARY",
                    "SOLID_TUMOUR",
                    "UNKNOWN"
                ],
                "type": "enum"
            }
        },
        {
            "doc": "Parent Tumour UID if this tumour is metastatic",
            "name": "tumourParentId",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Date of Diagnosis of the specific tumour",
            "name": "tumourDiagnosisDate",
            "type": [
                "null",
                {
                    "doc": "This defines a date record",
                    "fields": [
                        {
                            "doc": "Format YYYY",
                            "name": "year",
                            "type": "int"
                        },
                        {
                            "doc": "Format MM. e.g June is 06",
                            "name": "month",
                            "type": [
                                "null",
                                "int"
                            ]
                        },
                        {
                            "doc": "Format DD e.g. 12th of October is 12",
                            "name": "day",
                            "type": [
                                "null",
                                "int"
                            ]
                        }
                    ],
                    "name": "Date",
                    "type": "record"
                }
            ]
        },
        {
            "doc": "Description of the tumour",
            "name": "tumourDescription",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Morphology of the tumour",
            "name": "tumourMorphologies",
            "type": [
                "null",
                {
                    "items": {
                        "fields": [
                            {
                                "doc": "The ontology term id or accession in OBO format ${ONTOLOGY_ID}:${TERM_ID} (http://www.obofoundry.org/id-policy.html)",
                                "name": "id",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "The ontology term name",
                                "name": "name",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Optional value for the ontology term, the type of the value is not checked\n(i.e.: we could set the pvalue term to \"significant\" or to \"0.0001\")",
                                "name": "value",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Ontology version",
                                "name": "version",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            }
                        ],
                        "name": "Morphology",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Topography of the tumour",
            "name": "tumourTopographies",
            "type": [
                "null",
                {
                    "items": {
                        "fields": [
                            {
                                "doc": "The ontology term id or accession in OBO format ${ONTOLOGY_ID}:${TERM_ID} (http://www.obofoundry.org/id-policy.html)",
                                "name": "id",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "The ontology term name",
                                "name": "name",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Optional value for the ontology term, the type of the value is not checked\n(i.e.: we could set the pvalue term to \"significant\" or to \"0.0001\")",
                                "name": "value",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Ontology version",
                                "name": "version",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            }
                        ],
                        "name": "Topography",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Associated primary topography for metastatic tumours",
            "name": "tumourPrimaryTopographies",
            "type": [
                "null",
                {
                    "items": "Topography",
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Grade of the Tumour",
            "name": "tumourGrade",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Stage of the Tumour",
            "name": "tumourStage",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Prognostic Score of the Tumour",
            "name": "tumourPrognosticScore",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "In GMS, tumour presentation",
            "name": "tumourPresentation",
            "type": [
                "null",
                {
                    "name": "TumourPresentation",
                    "symbols": [
                        "FIRST_PRESENTATION",
                        "RECURRENCE",
                        "UNKNOWN"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "In GMS, primary or metastatic",
            "name": "primaryOrMetastatic",
            "type": [
                "null",
                {
                    "name": "PrimaryOrMetastatic",
                    "symbols": [
                        "PRIMARY",
                        "METASTATIC",
                        "UNKNOWN",
                        "NOT_APPLICABLE"
                    ],
                    "type": "enum"
                }
            ]
        }
    ],
    "name": "Tumour",
    "namespace": "org.gel.models.participant.avro",
    "type": "record"
}