{
    "doc": "Model that captures the clinical questions from birth.",
    "fields": [
        {
            "doc": "Gravida Number - Gravida indicates the number of times a woman is or has been pregnant, regardless of the pregnancy outcome.",
            "name": "gravidaNumber",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "doc": "String capturing the date and time of the delivery in ISO 8601 format with timezone and no microsecond.\nformat: %Y-%m-%dT%H:%M:%S+00:00\ne.g.: 2020-03-20T14:31:43+01:00",
            "name": "dateAndTimeOfDelivery",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Parity Number - Parity, or \"para\", indicates the number of births (including live births and stillbirths) where pregnancies reached viable gestational age.",
            "name": "parityNumber",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "doc": "Number of babies delivered in this pregnancy.",
            "name": "numberOfBabiesDelivered",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "doc": "Pregnancy Term in weeks.",
            "name": "pregnancyTermWeeks",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "doc": "Preterm labour if any.",
            "name": "pretermLabour",
            "type": [
                "null",
                {
                    "items": {
                        "doc": "Preterm labour.",
                        "name": "PretermLabour",
                        "symbols": [
                            "IDIOPATHIC",
                            "CHORIOAMNIONITIS",
                            "ABRUPTION",
                            "STRETCH"
                        ],
                        "type": "enum"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Induced birth if any.",
            "name": "inducedBirth",
            "type": [
                "null",
                {
                    "items": {
                        "doc": "Induced birth.",
                        "name": "InducedBirth",
                        "symbols": [
                            "PROSTAGLANDIN",
                            "BALLOON",
                            "ARTIFICIAL_RUPTURE_OF_MEMBRANES",
                            "ARTIFICIAL_RUPTURE_OF_MEMBRANES_MEMBRANE_SWEEP",
                            "OXYTOCIN"
                        ],
                        "type": "enum"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Mode of delivery.",
            "name": "modeOfDelivery",
            "type": [
                "null",
                {
                    "items": {
                        "doc": "Mode of delivery.",
                        "name": "ModeOfDelivery",
                        "symbols": [
                            "SPONTANEOUS_VAGINAL",
                            "INSTRUMENTAL_BIRTH",
                            "OTHER_ASSISTED_DELIVERY",
                            "CAESAREAN_SECTION_ELECTIVE",
                            "CAESAREAN_SECTION_EMERGENCY",
                            "CAESAREAN_SECTION_CATEGORY_1",
                            "CAESAREAN_SECTION_CATEGORY_2",
                            "CAESAREAN_SECTION_CATEGORY_3_AND_4"
                        ],
                        "type": "enum"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Membrane status.",
            "name": "membraneStatus",
            "type": [
                "null",
                {
                    "items": {
                        "doc": "Membrane status.",
                        "name": "MembraneStatus",
                        "symbols": [
                            "ARTIFICIAL_RUPTURE_FOR_INDUCTION_OF_LABOUR",
                            "BEFORE_LABOUR",
                            "DURING_LABOUR",
                            "AT_BIRTH"
                        ],
                        "type": "enum"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Delivery Complications.",
            "name": "deliveryComplications",
            "type": [
                "null",
                {
                    "items": {
                        "doc": "Delivery complications.\nThe `abnormally_invasive_placenta` includes accrete, increta or percreta.",
                        "name": "DeliveryComplications",
                        "symbols": [
                            "PLACENTA_ABRUPTION",
                            "ABNORMAL_PLACENTATION",
                            "PLACENTA_PRAEVIA",
                            "ABNORMALLY_INVASIVE_PLACENTA",
                            "CORD_RUPTURE",
                            "INFECTION",
                            "MANUAL_REMOVAL_OF_PLACENTA"
                        ],
                        "type": "enum"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Third stage - to establish if type of third stage impacts on the quantity of cord blood available for whole genome sequencing.",
            "name": "thirdStageOfLabour",
            "type": [
                "null",
                {
                    "items": {
                        "doc": "Third stage - to establish if type of third stage impacts on the quantity of cord blood available for whole genome sequencing.\n`physiological` - A physiological or natural third stage means waiting for the placenta to be delivered naturally with delay. A clamp is placed on the umbilical cord until it has stopped pulsating to allow oxygenated blood to pulse from the placenta to the baby.\n`active` - This can refer too:\n    1) giving oxytocin IM/IV to help contract the uterus.\n    2) clamping the cord early (usually before, alongside, or immediately after giving oxytocin IM/IV.\n    3) traction is applied to the cord with counter\u2010pressure on the uterus to deliver the placenta.\n`removal_at_caesarean_section` - birth occured through caesarean section.",
                        "name": "ThirdStageOfLabour",
                        "symbols": [
                            "PHYSIOLOGICAL",
                            "ACTIVE",
                            "REMOVAL_AT_CAESAREAN_SECTION"
                        ],
                        "type": "enum"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Approximate duration of labour duration (this is the onset of regular contractions until delivery of placenta).",
            "name": "durationOfLabour",
            "type": [
                "null",
                {
                    "doc": "Model that captures approximate duration of labour duration (this is the onset of regular contractions until delivery of placenta).",
                    "fields": [
                        {
                            "doc": "First stage (from 3cm and regular contractions) in hours.",
                            "name": "firstStageHourDuration",
                            "type": [
                                "null",
                                "float"
                            ]
                        },
                        {
                            "doc": "Second stage (pushing) in hours.",
                            "name": "secondStageHourDuration",
                            "type": [
                                "null",
                                "float"
                            ]
                        },
                        {
                            "doc": "Third stage (delivery of placenta) in minutes.",
                            "name": "thirdStageMinuteDuration",
                            "type": [
                                "null",
                                "int"
                            ]
                        },
                        {
                            "doc": "Date and time of labour onset.\nString in ISO 8601 format with timezone and no microsecond.\nformat: %Y-%m-%dT%H:%M:%S+00:00\ne.g.: 2020-03-20T14:31:43+01:00",
                            "name": "dateAndTimeOfLabourOnset",
                            "type": [
                                "null",
                                "string"
                            ]
                        }
                    ],
                    "name": "DurationOfLabour",
                    "type": "record"
                }
            ]
        },
        {
            "doc": "Baby oral intake questions before sample (e.g. buccal swab, buccal sponge) was collected.",
            "name": "babyOralIntakeQuestions",
            "type": [
                "null",
                {
                    "doc": "Time between oral intake and sample collection (to establish contamination levels and lead to understanding optimum collection time).",
                    "fields": [
                        {
                            "doc": "Has the baby been fed or taken medication before sample was collected.",
                            "name": "neverBeenFed",
                            "type": "boolean"
                        },
                        {
                            "doc": "Time since last oral intake (feed or medication) in minutes. NOTE: Only fill in if baby was fed before sample collection.",
                            "name": "minutesSinceOralIntake",
                            "type": [
                                "null",
                                "int"
                            ]
                        },
                        {
                            "doc": "Type of last intake (feed or medication). NOTE: Only fill in if baby was fed before sample collection.",
                            "name": "babyIntake",
                            "type": [
                                "null",
                                {
                                    "doc": "Baby intake (feed or medication).",
                                    "name": "BabyIntake",
                                    "symbols": [
                                        "MATERNAL_BREAST_MILK",
                                        "DONOR_BREAST_MILK",
                                        "INFANT_FORMULA",
                                        "MEDICATION"
                                    ],
                                    "type": "enum"
                                }
                            ]
                        }
                    ],
                    "name": "BabyOralIntakeQuestions",
                    "type": "record"
                }
            ]
        },
        {
            "doc": "How long ago (in minutes) since mum had oral intake of food or fluids.",
            "name": "mumsTimeSinceLastOralIntake",
            "type": [
                "null",
                "int"
            ]
        }
    ],
    "name": "BirthClinicalQuestions",
    "namespace": "org.gel.models.participant.avro",
    "type": "record"
}