{
    "fields": [
        {
            "doc": "The somatic variant level questions for the cancer program",
            "name": "cancerSomaticVariantLevelQuestions",
            "type": [
                "null",
                {
                    "doc": "The questions for the cancer program exit questionnaire for somatic variants",
                    "fields": [
                        {
                            "doc": "Variant coordinates following format `chromosome:position:reference:alternate`",
                            "name": "variantCoordinates",
                            "type": {
                                "doc": "The variant coordinates representing uniquely a small variant.\nNo multi-allelic variant supported, alternate only represents one alternate allele.",
                                "fields": [
                                    {
                                        "doc": "Chromosome without \"chr\" prefix (e.g. X rather than chrX)",
                                        "name": "chromosome",
                                        "type": "string"
                                    },
                                    {
                                        "doc": "Genomic position",
                                        "name": "position",
                                        "type": "int"
                                    },
                                    {
                                        "doc": "The reference bases.",
                                        "name": "reference",
                                        "type": "string"
                                    },
                                    {
                                        "doc": "The alternate bases",
                                        "name": "alternate",
                                        "type": "string"
                                    },
                                    {
                                        "doc": "The assembly to which this variant corresponds",
                                        "name": "assembly",
                                        "type": {
                                            "doc": "The reference genome assembly",
                                            "name": "Assembly",
                                            "symbols": [
                                                "GRCh38",
                                                "GRCh37"
                                            ],
                                            "type": "enum"
                                        }
                                    }
                                ],
                                "name": "VariantCoordinates",
                                "type": "record"
                            }
                        },
                        {
                            "doc": "Type of potential actionability:",
                            "name": "variantActionability",
                            "type": {
                                "items": {
                                    "doc": "The variant actionabilities:\n* `predicts_therapeutic_response`: Predicts therapeutic response\n* `prognostic`: Prognostic\n* `defines_diagnosis_group`: Defines diagnosis group\n* `eligibility_for_trial`: Eligibility for trial\n* `other`:  Other (please specify)",
                                    "name": "CancerActionabilitySomatic",
                                    "symbols": [
                                        "predicts_therapeutic_response",
                                        "prognostic",
                                        "defines_diagnosis_group",
                                        "eligibility_for_trial",
                                        "other"
                                    ],
                                    "type": "enum"
                                },
                                "type": "array"
                            }
                        },
                        {
                            "doc": "Other information about variant actionability",
                            "name": "otherVariantActionability",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "How has/will this potentially actionable variant been/be used?",
                            "name": "variantUsability",
                            "type": {
                                "doc": "Variant usability for somatic variants:\n* `already_actioned`: Already actioned (i.e. prior to receiving this WGA)\n* `actioned_result_of_this_wga`: actioned as a result of receiving this WGA\n* `not_yet_actioned`: not yet actioned, but potentially actionable in the future",
                                "name": "CancerUsabilitySomatic",
                                "symbols": [
                                    "already_actioned",
                                    "actioned_result_of_this_wga",
                                    "not_yet_actioned"
                                ],
                                "type": "enum"
                            }
                        },
                        {
                            "doc": "Has this variant been tested by another method (either prior to or following receipt of this WGA)?",
                            "name": "variantTested",
                            "type": {
                                "doc": "Was the variant validated with an orthogonal technology?\n* `not_indicated_for_patient_care`: No: not indicated for patient care at this time\n* `no_orthologous_test_available`: No: no orthologous test available\n* `test_performed_prior_to_wga`: Yes: test performed prior to receiving WGA (eg using standard-of-care assay such as panel testing, or sanger sequencing)\n* `technical_validation_following_WGA`: Yes: technical validation performed/planned following receiving this WGA",
                                "name": "CancerTested",
                                "symbols": [
                                    "not_indicated_for_patient_care",
                                    "no_orthologous_test_available",
                                    "test_performed_prior_to_wga",
                                    "technical_validation_following_wga"
                                ],
                                "type": "enum"
                            }
                        },
                        {
                            "doc": "Please enter validation assay type e.g Pyrosequencing, NGS panel, COBAS, Sanger sequencing. If not applicable enter NA;",
                            "name": "validationAssayType",
                            "type": "string"
                        }
                    ],
                    "name": "CancerSomaticVariantLevelQuestions",
                    "namespace": "org.gel.models.report.avro",
                    "type": "record"
                }
            ]
        },
        {
            "doc": "The variant group level questions for the cancer program",
            "name": "cancerGermlineVariantLevelQuestions",
            "type": [
                "null",
                {
                    "doc": "The questions for the cancer program exit questionnaire for germline variants",
                    "fields": [
                        {
                            "doc": "Variant coordinates following format `chromosome:position:reference:alternate`",
                            "name": "variantCoordinates",
                            "type": "VariantCoordinates"
                        },
                        {
                            "doc": "Type of potential actionability:",
                            "name": "variantActionability",
                            "type": {
                                "items": {
                                    "doc": "An enumeration Variant Actionability:\n* `predicts_therapeutic_response`: Predicts therapeutic response\n* `prognostic`: Prognostic\n* `defines_diagnosis_group`: Defines diagnosis group\n* `eligibility_for_trial`: Eligibility for trial\n* `germline_susceptibility`: Germline susceptibility\n* `other`:  Other (please specify)",
                                    "name": "CancerActionability",
                                    "symbols": [
                                        "germline_susceptibility",
                                        "predicts_therapeutic_response",
                                        "prognostic",
                                        "defines_diagnosis_group",
                                        "eligibility_for_trial",
                                        "other"
                                    ],
                                    "type": "enum"
                                },
                                "type": "array"
                            }
                        },
                        {
                            "name": "otherVariantActionability",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "How has/will this potentially actionable variant been/be used?",
                            "name": "variantUsability",
                            "type": {
                                "doc": "Variant usability for germline variants:\n* `already_actioned`: Already actioned (i.e. prior to receiving this WGA)\n* `actioned_result_of_this_wga`: actioned as a result of receiving this WGA",
                                "name": "CancerUsabilityGermline",
                                "symbols": [
                                    "already_actioned",
                                    "actioned_result_of_this_wga"
                                ],
                                "type": "enum"
                            }
                        },
                        {
                            "doc": "Has this variant been tested by another method (either prior to or following receipt of this WGA)?",
                            "name": "variantTested",
                            "type": "CancerTested"
                        },
                        {
                            "doc": "Please enter validation assay type e.g Pyrosequencing, NGS panel, COBAS, Sanger sequencing. If not applicable enter NA;",
                            "name": "validationAssayType",
                            "type": "string"
                        }
                    ],
                    "name": "CancerGermlineVariantLevelQuestions",
                    "namespace": "org.gel.models.report.avro",
                    "type": "record"
                }
            ]
        },
        {
            "doc": "Cancer case level questions",
            "name": "cancercaseLevelQuestions",
            "type": {
                "doc": "The questions for the cancer program exit questionnaire at case level",
                "fields": [
                    {
                        "doc": "Total time taken to review/collate evidence for variants (hours).\nInclude all literature review time, consultation with relevant experts etc.",
                        "name": "total_review_time",
                        "type": "double"
                    },
                    {
                        "doc": "Time taken to discuss case at MDT (hours).",
                        "name": "mdt1_time",
                        "type": "double"
                    },
                    {
                        "doc": "If the case is discussed at a 2nd MDT please enter time here (hours).",
                        "name": "mdt2_time",
                        "type": [
                            "null",
                            "double"
                        ]
                    },
                    {
                        "doc": "Total time to design ALL validation assay(s) for case (hours).\nOnly applicable if it is necessary to design a new assay to validate the variant.",
                        "name": "validation_assay_time",
                        "type": [
                            "null",
                            "double"
                        ]
                    },
                    {
                        "doc": "Technical Laboratory Validation. Total time for validation test wet work for all variants (hours).",
                        "name": "wet_validation_time",
                        "type": [
                            "null",
                            "double"
                        ]
                    },
                    {
                        "doc": "Analytical Laboratory Validation. Total time for analysis of validation results for all variants (hours).",
                        "name": "analytical_validation_time",
                        "type": [
                            "null",
                            "double"
                        ]
                    },
                    {
                        "doc": "Primary Reporting. Time taken to complete primary reporting stage (hours).",
                        "name": "primary_reporting_time",
                        "type": "double"
                    },
                    {
                        "doc": "Report Authorisation. Time taken to check and authorise report (hours).",
                        "name": "primary_authorisation_time",
                        "type": "double"
                    },
                    {
                        "doc": "Report Distribution.\nPlease enter, where possible/accessible how long it takes for the result to be conveyed to the patient.\nE.g. via letter from the clinician (days).",
                        "name": "report_distribution_time",
                        "type": "double"
                    },
                    {
                        "doc": "Total time from result to report.\nThe total time taken from when the analysis of the WGS results started  to a report being received\nby the patient include any 'waiting' time (days).",
                        "name": "total_time",
                        "type": "double"
                    },
                    {
                        "doc": "Which parts of the WGA were reviewed?",
                        "name": "reviewedInMdtWga",
                        "type": {
                            "doc": "An enumeration for Which parts of the WGA were reviewed?:\n* `domain_1`: Domain 1 only\n* `domain_1_and_2`: Domains 1 and 2\n* `domain_1_2_and_suplementary`: Domains 1, 2 and supplementary analysis",
                            "name": "ReviewedParts",
                            "symbols": [
                                "domain_1",
                                "domain_1_and_2",
                                "domain_1_2_and_suplementary",
                                "somatic_if_relevant"
                            ],
                            "type": "enum"
                        }
                    },
                    {
                        "doc": "Were potentially actionable variants detected?",
                        "name": "actionableVariants",
                        "type": {
                            "doc": "Are the variants actionable?\n* `yes`: yes\n* `no`: no",
                            "name": "CancerActionableVariants",
                            "symbols": [
                                "yes",
                                "no"
                            ],
                            "type": "enum"
                        }
                    }
                ],
                "name": "CancerCaseLevelQuestions",
                "namespace": "org.gel.models.report.avro",
                "type": "record"
            }
        },
        {
            "doc": "Please enter any additional comments you may have about the case here.",
            "name": "additionalComments",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Other actionable variants or entities.\nPlease provide other (potentially) actionable entities: e.g domain 3 small variants,\nSV/CNV, mutational signatures, mutational burden",
            "name": "otherActionableVariants",
            "type": [
                "null",
                {
                    "items": {
                        "fields": [
                            {
                                "doc": "Chr: Pos Ref > Alt",
                                "name": "variantCoordinates",
                                "type": "VariantCoordinates"
                            },
                            {
                                "doc": "Type of potential actionability:",
                                "name": "variantActionability",
                                "type": {
                                    "items": "CancerActionability",
                                    "type": "array"
                                }
                            },
                            {
                                "name": "otherVariantActionability",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "How has/will this potentially actionable variant been/be used?",
                                "name": "variantUsability",
                                "type": "CancerUsabilitySomatic"
                            },
                            {
                                "doc": "Has this variant been tested by another method (either prior to or following receipt of this WGA)?",
                                "name": "variantTested",
                                "type": {
                                    "doc": "An enumeration Variant tested:\n* `not_indicated_for_patient_care`: No: not indicated for patient care at this time\n* `no_orthologous_test_available`: No: no orthologous test available\n* `test_performed_prior_to_wga`: Yes: test performed prior to receiving WGA (eg using standard-of-care assay such as panel testing, or sanger sequencing)\n* `technical_validation_following_wga`: Yes: technical validation performed/planned following receiving this WGA\n* `na`: N/A",
                                    "name": "CancerTestedAdditional",
                                    "symbols": [
                                        "not_indicated_for_patient_care",
                                        "no_orthologous_test_available",
                                        "test_performed_prior_to_wga",
                                        "technical_validation_following_wga",
                                        "na"
                                    ],
                                    "type": "enum"
                                }
                            },
                            {
                                "doc": "Please enter validation assay type e.g Pyrosequencing, NGS panel, COBAS, Sanger sequencing. If not applicable enter NA;",
                                "name": "validationAssayType",
                                "type": "string"
                            }
                        ],
                        "name": "AdditionalVariantsQuestions",
                        "namespace": "org.gel.models.report.avro",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        }
    ],
    "name": "ReportEventQuestionnaireCancer",
    "namespace": "org.gel.models.cva.avro",
    "type": "record"
}