{
    "doc": "A `Call` represents the determination of genotype with respect to a\nparticular `Variant`.\n\nIt may include associated information such as quality\nand phasing. For example, a call might assign a probability of 0.32 to\nthe occurrence of a SNP named rs1234 in a call set with the name NA12345.",
    "fields": [
        {
            "default": null,
            "doc": "The name of the call set this variant call belongs to.\nIf this field is not present, the ordering of the call sets from a\n`SearchCallSetsRequest` over this `VariantSet` is guaranteed to match\nthe ordering of the calls on this `Variant`.\nThe number of results will also be the same.",
            "name": "callSetName",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "default": null,
            "doc": "The ID of the call set this variant call belongs to.\n\n  If this field is not present, the ordering of the call sets from a\n  `SearchCallSetsRequest` over this `VariantSet` is guaranteed to match\n  the ordering of the calls on this `Variant`.\n  The number of results will also be the same.",
            "name": "callSetId",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "default": [],
            "doc": "The genotype of this variant call.\n\n  A 0 value represents the reference allele of the associated `Variant`. Any\n  other value is a 1-based index into the alternate alleles of the associated\n  `Variant`.\n\n  If a variant had a referenceBases field of \"T\", an alternateBases\n  value of [\"A\", \"C\"], and the genotype was [2, 1], that would mean the call\n  represented the heterozygous value \"CA\" for this variant. If the genotype\n  was instead [0, 1] the represented value would be \"TA\". Ordering of the\n  genotype values is important if the phaseset field is present.",
            "name": "genotype",
            "type": {
                "items": "int",
                "type": "array"
            }
        },
        {
            "default": null,
            "doc": "If this field is not null, this variant call's genotype ordering implies\nthe phase of the bases and is consistent with any other variant calls on\nthe same contig which have the same phaseset string.",
            "name": "phaseset",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "default": [],
            "doc": "The genotype likelihoods for this variant call. Each array entry\nrepresents how likely a specific genotype is for this call as\nlog10(P(data | genotype)), analogous to the GL tag in the VCF spec. The\nvalue ordering is defined by the GL tag in the VCF spec.",
            "name": "genotypeLikelihood",
            "type": {
                "items": "double",
                "type": "array"
            }
        },
        {
            "default": null,
            "doc": "Integer or null value indicating the number of reads supporting the\nreference allele. If a missing value '.' is in the VCF, this should\nbe recoded as null/None.",
            "name": "referenceDepth",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "default": [],
            "doc": "An array of values which indicate the read depth assigned to the Alt allele(s)\nif alternateBases are C,GT and the alternateDepth is 1,2 - \"C\" has 1 supporting\nread, \"GT\" has 2. It is possible for the field value to be missing, in which case\nthe \".\" value should be recorded in the Call as null/None\nThis is kept as an array, though the expectation is that this will usually be\nused with normalised variants, so that the array has one element",
            "name": "alternateDepth",
            "type": {
                "items": [
                    "null",
                    "int"
                ],
                "type": "array"
            }
        },
        {
            "default": {},
            "doc": "A map of additional variant call information.",
            "name": "info",
            "type": {
                "type": "map",
                "values": {
                    "items": "string",
                    "type": "array"
                }
            }
        }
    ],
    "name": "Call",
    "namespace": "org.gel.models.report.avro",
    "type": "record"
}