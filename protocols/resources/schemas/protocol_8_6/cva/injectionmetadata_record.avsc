{
    "doc": "Metadata about injected data",
    "fields": [
        {
            "doc": "Report avro models version",
            "name": "reportModelVersion",
            "type": "string"
        },
        {
            "doc": "The entity identifier",
            "name": "id",
            "type": "string"
        },
        {
            "doc": "The entity version. This is a correlative number being the highest value the latest version.",
            "name": "version",
            "type": "int"
        },
        {
            "doc": "The case identifier",
            "name": "caseId",
            "type": "string"
        },
        {
            "doc": "The case version. This is a correlative number being the highest value the latest version.",
            "name": "caseVersion",
            "type": "int"
        },
        {
            "doc": "The family identifier",
            "name": "groupId",
            "type": "string"
        },
        {
            "doc": "The cohort identifier (the same family can have several cohorts)",
            "name": "cohortId",
            "type": "string"
        },
        {
            "doc": "The author of the ReportedVariant, either tiering, exomiser, a given cip (e.g.: omicia) or a given GMCs user name",
            "name": "author",
            "type": "string"
        },
        {
            "doc": "The author version of the ReportedVariant, either tiering, exomiser or a given cip. Only applicable for automated processes.",
            "name": "authorVersion",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "The assembly to which the variants refer",
            "name": "assembly",
            "type": [
                "null",
                {
                    "doc": "The reference genome assembly",
                    "name": "Assembly",
                    "namespace": "org.gel.models.report.avro",
                    "symbols": [
                        "GRCh38",
                        "GRCh37"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "The 100K Genomes program to which the reported variant belongs.",
            "name": "program",
            "type": {
                "doc": "The Genomics England program",
                "name": "Program",
                "namespace": "org.gel.models.report.avro",
                "symbols": [
                    "cancer",
                    "rare_disease"
                ],
                "type": "enum"
            }
        },
        {
            "doc": "The category to which the case belongs.",
            "name": "category",
            "type": {
                "name": "Category",
                "symbols": [
                    "HundredK",
                    "NGIS"
                ],
                "type": "enum"
            }
        },
        {
            "doc": "The creation date of the case (ISO-8601)",
            "name": "caseCreationDate",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "The last modified date of the case (ISO-8601)",
            "name": "caseLastModifiedDate",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "The organisation responsible for this payload (Pedigree and CancerParticipant will correspond to the case\nowner and the ClinicalReport will correspond to the case assignee)",
            "name": "organisation",
            "type": [
                "null",
                {
                    "doc": "An organisation which may own or be assigned to a case",
                    "fields": [
                        {
                            "doc": "ODS code",
                            "name": "ods",
                            "type": "string"
                        },
                        {
                            "doc": "The GMC name",
                            "name": "gmc",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "The site name",
                            "name": "site",
                            "type": [
                                "null",
                                "string"
                            ]
                        }
                    ],
                    "name": "Organisation",
                    "type": "record"
                }
            ]
        },
        {
            "doc": "The NGIS organisation responsible for this payload",
            "name": "organisationNgis",
            "type": [
                "null",
                {
                    "fields": [
                        {
                            "doc": "Organisation Id",
                            "name": "organisationId",
                            "type": "string"
                        },
                        {
                            "doc": "Ods code",
                            "name": "organisationCode",
                            "type": "string"
                        },
                        {
                            "doc": "Organisation Name",
                            "name": "organisationName",
                            "type": "string"
                        },
                        {
                            "doc": "National Grouping (GLH) Id",
                            "name": "organisationNationalGroupingId",
                            "type": "string"
                        },
                        {
                            "doc": "National Grouping (GLH) Name",
                            "name": "organisationNationalGroupingName",
                            "type": "string"
                        }
                    ],
                    "name": "OrganisationNgis",
                    "namespace": "org.gel.models.participant.avro",
                    "type": "record"
                }
            ]
        },
        {
            "doc": "Test unique identifier (only sent for NGIS cases)",
            "name": "referralTestId",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Referral unique identifier (only sent for NGIS cases)",
            "name": "referralId",
            "type": [
                "null",
                "string"
            ]
        }
    ],
    "name": "InjectionMetadata",
    "namespace": "org.gel.models.cva.avro",
    "type": "record"
}