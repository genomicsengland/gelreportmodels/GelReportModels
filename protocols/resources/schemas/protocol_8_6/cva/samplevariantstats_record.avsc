{
    "fields": [
        {
            "doc": "Sample identifier *",
            "name": "id",
            "type": "string"
        },
        {
            "doc": "Number of variants where the sample has the main allele (i.e. 0/1, 1/1, ./1, 1/2, ...)",
            "name": "variantCount",
            "type": "int"
        },
        {
            "default": {},
            "doc": "Number of variants per chromosome *",
            "name": "chromosomeCount",
            "type": {
                "type": "map",
                "values": "int"
            }
        },
        {
            "default": {},
            "doc": "Variants count group by type. e.g. SNP, INDEL, MNP, SNV, ...",
            "name": "typeCount",
            "type": {
                "type": "map",
                "values": "int"
            }
        },
        {
            "default": {},
            "doc": "Number of variants per genotype. Only counts genotypes with the main allele. Phase is ignored. *",
            "name": "genotypeCount",
            "type": {
                "type": "map",
                "values": "int"
            }
        },
        {
            "doc": "Indel length grouped in ranges *",
            "name": "indelLengthCount",
            "type": {
                "fields": [
                    {
                        "name": "lt5",
                        "type": "int"
                    },
                    {
                        "name": "lt10",
                        "type": "int"
                    },
                    {
                        "name": "lt15",
                        "type": "int"
                    },
                    {
                        "name": "lt20",
                        "type": "int"
                    },
                    {
                        "name": "gte20",
                        "type": "int"
                    }
                ],
                "name": "IndelLength",
                "type": "record"
            }
        },
        {
            "doc": "The number of occurrences for each FILTER value in files from this set.\nEach file can contain more than one filter value (usually separated by ';').\n",
            "name": "filterCount",
            "type": {
                "type": "map",
                "values": "int"
            }
        },
        {
            "doc": "TiTvRatio = num. transitions / num. transversions",
            "name": "tiTvRatio",
            "type": "float"
        },
        {
            "doc": "Mean Quality for all the variants with quality",
            "name": "qualityAvg",
            "type": "float"
        },
        {
            "doc": "Standard Deviation of the quality",
            "name": "qualityStdDev",
            "type": "float"
        },
        {
            "doc": "Heterozygosity rate as defined by PLINK: (N\u2013O)/N\n\nN is the number of non-missing genotypes\nO is the observed number of homozygous genotypes for a given individual\n",
            "name": "heterozygosityRate",
            "type": "float"
        },
        {
            "default": {},
            "doc": "Number of mendelian errors grouped by PLINK error codes grouped by Chromosome. *",
            "name": "mendelianErrorCount",
            "type": {
                "type": "map",
                "values": {
                    "type": "map",
                    "values": "int"
                }
            }
        },
        {
            "name": "depthCount",
            "type": {
                "fields": [
                    {
                        "name": "na",
                        "type": "int"
                    },
                    {
                        "name": "lt5",
                        "type": "int"
                    },
                    {
                        "name": "lt10",
                        "type": "int"
                    },
                    {
                        "name": "lt15",
                        "type": "int"
                    },
                    {
                        "name": "lt20",
                        "type": "int"
                    },
                    {
                        "name": "gte20",
                        "type": "int"
                    }
                ],
                "name": "DepthCount",
                "type": "record"
            }
        },
        {
            "default": {},
            "doc": "Variants count group by consequence type. e.g. missense_variant, synonymous_variant, stop_lost, ...\nEach counter is increased at most one per variant. If multiple overlapping transcripts have the same consequence type, it will count as one.",
            "name": "consequenceTypeCount",
            "type": {
                "type": "map",
                "values": "int"
            }
        },
        {
            "default": {},
            "doc": "Variants count group by biotype. e.g. protein-coding, miRNA, lncRNA, ...\nEach counter is increased at most one per variant. If multiple overlapping genes have the same biotypes, it will count as one.",
            "name": "biotypeCount",
            "type": {
                "type": "map",
                "values": "int"
            }
        },
        {
            "default": {},
            "doc": "Variants count group by clinical significance. e.g. benign, likely_benign, likely_pathogenic, pathogenic, uncertain_significance  ...\nEach counter is increased at most one per variant. If multiple variant traits have the same clinical significance, it will count as one.",
            "name": "clinicalSignificanceCount",
            "type": {
                "type": "map",
                "values": "int"
            }
        }
    ],
    "name": "SampleVariantStats",
    "namespace": "org.opencb.biodata.models.variant.metadata",
    "type": "record"
}