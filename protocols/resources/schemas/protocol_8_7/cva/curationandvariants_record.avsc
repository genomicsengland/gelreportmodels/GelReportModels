{
    "doc": "A curation and the variants coordinates to which it corresponds",
    "fields": [
        {
            "name": "variantsCoordinates",
            "type": {
                "doc": "A list of variant coordinates",
                "fields": [
                    {
                        "default": [],
                        "name": "variants",
                        "type": {
                            "items": {
                                "doc": "The variant coordinates representing uniquely a small variant.\nNo multi-allelic variant supported, alternate only represents one alternate allele.",
                                "fields": [
                                    {
                                        "doc": "Chromosome without \"chr\" prefix (e.g. X rather than chrX)",
                                        "name": "chromosome",
                                        "type": "string"
                                    },
                                    {
                                        "doc": "Genomic position",
                                        "name": "position",
                                        "type": "int"
                                    },
                                    {
                                        "doc": "The reference bases.",
                                        "name": "reference",
                                        "type": "string"
                                    },
                                    {
                                        "doc": "The alternate bases",
                                        "name": "alternate",
                                        "type": "string"
                                    },
                                    {
                                        "doc": "The assembly to which this variant corresponds",
                                        "name": "assembly",
                                        "type": {
                                            "doc": "The reference genome assembly",
                                            "name": "Assembly",
                                            "symbols": [
                                                "GRCh38",
                                                "GRCh37"
                                            ],
                                            "type": "enum"
                                        }
                                    }
                                ],
                                "name": "VariantCoordinates",
                                "namespace": "org.gel.models.report.avro",
                                "type": "record"
                            },
                            "type": "array"
                        }
                    }
                ],
                "name": "VariantsCoordinates",
                "type": "record"
            }
        },
        {
            "name": "curation",
            "type": {
                "doc": "The curation record contains all information that might be stored from a curation event.",
                "fields": [
                    {
                        "doc": "The heritable phenotype to which the curation refers.",
                        "name": "heritableTrait",
                        "type": {
                            "doc": "The entity representing a phenotype and its inheritance pattern.",
                            "fields": [
                                {
                                    "doc": "The trait (e.g.: HPO term, MIM term, DO term etc.)",
                                    "name": "trait",
                                    "type": [
                                        "null",
                                        "string"
                                    ]
                                },
                                {
                                    "doc": "The mode of inheritance",
                                    "name": "inheritanceMode",
                                    "type": [
                                        "null",
                                        {
                                            "doc": "An enumeration for the different mode of inheritances:\n\n* `monoallelic_not_imprinted`: MONOALLELIC, autosomal or pseudoautosomal, not imprinted\n* `monoallelic_maternally_imprinted`: MONOALLELIC, autosomal or pseudoautosomal, maternally imprinted (paternal allele expressed)\n* `monoallelic_paternally_imprinted`: MONOALLELIC, autosomal or pseudoautosomal, paternally imprinted (maternal allele expressed)\n* `monoallelic`: MONOALLELIC, autosomal or pseudoautosomal, imprinted status unknown\n* `biallelic`: BIALLELIC, autosomal or pseudoautosomal\n* `monoallelic_and_biallelic`: BOTH monoallelic and biallelic, autosomal or pseudoautosomal\n* `monoallelic_and_more_severe_biallelic`: BOTH monoallelic and biallelic, autosomal or pseudoautosomal (but BIALLELIC mutations cause a more SEVERE disease form), autosomal or pseudoautosomal\n* `xlinked_biallelic`: X-LINKED: hemizygous mutation in males, biallelic mutations in females\n* `xlinked_monoallelic`: X linked: hemizygous mutation in males, monoallelic mutations in females may cause disease (may be less severe, later onset than males)\n* `mitochondrial`: MITOCHONDRIAL\n* `unknown`: Unknown\n* `NA`: Not applicable",
                                            "name": "ModeOfInheritance",
                                            "symbols": [
                                                "monoallelic",
                                                "monoallelic_not_imprinted",
                                                "monoallelic_maternally_imprinted",
                                                "monoallelic_paternally_imprinted",
                                                "biallelic",
                                                "monoallelic_and_biallelic",
                                                "monoallelic_and_more_severe_biallelic",
                                                "xlinked_biallelic",
                                                "xlinked_monoallelic",
                                                "mitochondrial",
                                                "unknown",
                                                "NA"
                                            ],
                                            "type": "enum"
                                        }
                                    ]
                                }
                            ],
                            "name": "HeritableTrait",
                            "namespace": "org.opencb.biodata.models.variant.avro",
                            "type": "record"
                        }
                    },
                    {
                        "doc": "The transcript to which the curation refers",
                        "name": "genomicFeature",
                        "type": [
                            "null",
                            {
                                "doc": "The genomic feature",
                                "fields": [
                                    {
                                        "doc": "Feature Type",
                                        "name": "featureType",
                                        "type": [
                                            "null",
                                            {
                                                "doc": "The feature types",
                                                "name": "FeatureTypes",
                                                "symbols": [
                                                    "regulatory_region",
                                                    "gene",
                                                    "transcript",
                                                    "protein"
                                                ],
                                                "type": "enum"
                                            }
                                        ]
                                    },
                                    {
                                        "doc": "Feature used, this should be a feature ID from Ensembl, (i.e, ENST00000544455)",
                                        "name": "ensemblId",
                                        "type": [
                                            "null",
                                            "string"
                                        ]
                                    },
                                    {
                                        "doc": "Others IDs. Fields like the HGNC symbol if available should be added here",
                                        "name": "xrefs",
                                        "type": [
                                            "null",
                                            {
                                                "type": "map",
                                                "values": "string"
                                            }
                                        ]
                                    }
                                ],
                                "name": "GenomicFeature",
                                "namespace": "org.opencb.biodata.models.variant.avro",
                                "type": "record"
                            }
                        ]
                    },
                    {
                        "doc": "The assembly to which the curation refers",
                        "name": "assembly",
                        "type": [
                            "null",
                            "org.gel.models.report.avro.Assembly"
                        ]
                    },
                    {
                        "doc": "The variant's classification.",
                        "name": "variantClassification",
                        "type": [
                            "null",
                            {
                                "doc": "The variant classification according to different properties.",
                                "fields": [
                                    {
                                        "doc": "The variant's clinical significance.",
                                        "name": "clinicalSignificance",
                                        "type": [
                                            "null",
                                            {
                                                "doc": "Mendelian variants classification with ACMG terminology as defined in Richards, S. et al. (2015). Standards and\n        guidelines for the interpretation of sequence variants: a joint consensus recommendation of the American College\n        of Medical Genetics and Genomics and the Association for Molecular Pathology. Genetics in Medicine, 17(5),\n        405\u2013423. https://doi.org/10.1038/gim.2015.30.\n\n    Classification for pharmacogenomic variants, variants associated to\n    disease and somatic variants based on the ACMG recommendations and ClinVar classification\n    (https://www.ncbi.nlm.nih.gov/clinvar/docs/clinsig/).\n\n* `benign_variant` : Benign variants interpreted for Mendelian disorders\n* `likely_benign_variant` : Likely benign variants interpreted for Mendelian disorders with a certainty of at least 90%\n* `pathogenic_variant` : Pathogenic variants interpreted for Mendelian disorders\n* `likely_pathogenic_variant` : Likely pathogenic variants interpreted for Mendelian disorders with a certainty of at\nleast 90%\n* `uncertain_significance` : Uncertain significance variants interpreted for Mendelian disorders. Variants with\nconflicting evidences should be classified as uncertain_significance",
                                                "name": "ClinicalSignificance",
                                                "symbols": [
                                                    "benign",
                                                    "likely_benign",
                                                    "likely_pathogenic",
                                                    "pathogenic",
                                                    "uncertain_significance"
                                                ],
                                                "type": "enum"
                                            }
                                        ]
                                    },
                                    {
                                        "doc": "The variant's pharmacogenomics classification.",
                                        "name": "drugResponseClassification",
                                        "type": [
                                            "null",
                                            {
                                                "doc": "Pharmacogenomics drug response variant classification",
                                                "name": "DrugResponseClassification",
                                                "symbols": [
                                                    "responsive",
                                                    "altered_sensitivity",
                                                    "reduced_sensitivity",
                                                    "increased_sensitivity",
                                                    "altered_resistance",
                                                    "increased_resistance",
                                                    "reduced_resistance",
                                                    "increased_risk_of_toxicity",
                                                    "reduced_risk_of_toxicity",
                                                    "altered_toxicity",
                                                    "adverse_drug_reaction",
                                                    "indication",
                                                    "contraindication",
                                                    "dosing_alteration",
                                                    "increased_dose",
                                                    "reduced_dose",
                                                    "increased_monitoring",
                                                    "increased_efficacy",
                                                    "reduced_efficacy",
                                                    "altered_efficacy"
                                                ],
                                                "type": "enum"
                                            }
                                        ]
                                    },
                                    {
                                        "doc": "The variant's trait association.",
                                        "name": "traitAssociation",
                                        "type": [
                                            "null",
                                            {
                                                "doc": "Association of variants to a given trait.\n* `established_risk_allele` : Established risk allele for variants associated to disease\n* `likely_risk_allele` : Likely risk allele for variants associated to disease\n* `uncertain_risk_allele` : Uncertain risk allele for variants associated to disease\n* `protective` : Protective allele",
                                                "name": "TraitAssociation",
                                                "symbols": [
                                                    "established_risk_allele",
                                                    "likely_risk_allele",
                                                    "uncertain_risk_allele",
                                                    "protective"
                                                ],
                                                "type": "enum"
                                            }
                                        ]
                                    },
                                    {
                                        "doc": "The variant's tumorigenesis classification.",
                                        "name": "tumorigenesisClassification",
                                        "type": [
                                            "null",
                                            {
                                                "doc": "Variant classification according to its relation to cancer aetiology.\n* `driver` : Driver variants\n* `passenger` : Passenger variants\n* `modifier` : Modifier variants",
                                                "name": "TumorigenesisClassification",
                                                "symbols": [
                                                    "driver",
                                                    "passenger",
                                                    "modifier"
                                                ],
                                                "type": "enum"
                                            }
                                        ]
                                    },
                                    {
                                        "doc": "The variant functional effect",
                                        "name": "functionalEffect",
                                        "type": [
                                            "null",
                                            {
                                                "doc": "Variant effect with Sequence Ontology terms.\n\n* `SO_0002052`: dominant_negative_variant (http://purl.obolibrary.org/obo/SO_0002052)\n* `SO_0002053`: gain_of_function_variant (http://purl.obolibrary.org/obo/SO_0002053)\n* `SO_0001773`: lethal_variant (http://purl.obolibrary.org/obo/SO_0001773)\n* `SO_0002054`: loss_of_function_variant (http://purl.obolibrary.org/obo/SO_0002054)\n* `SO_0001786`: loss_of_heterozygosity (http://purl.obolibrary.org/obo/SO_0001786)\n* `SO_0002055`: null_variant (http://purl.obolibrary.org/obo/SO_0002055)",
                                                "name": "VariantFunctionalEffect",
                                                "symbols": [
                                                    "dominant_negative_variant",
                                                    "gain_of_function_variant",
                                                    "lethal_variant",
                                                    "loss_of_function_variant",
                                                    "loss_of_heterozygosity",
                                                    "null_variant"
                                                ],
                                                "type": "enum"
                                            }
                                        ]
                                    }
                                ],
                                "name": "VariantClassification",
                                "namespace": "org.opencb.biodata.models.variant.avro",
                                "type": "record"
                            }
                        ]
                    },
                    {
                        "doc": "The curation confidence.",
                        "name": "confidence",
                        "type": [
                            "null",
                            {
                                "doc": "Confidence based on the Confidence Information Ontology\n\n* `CIO_0000029`: high confidence level http://purl.obolibrary.org/obo/CIO_0000029\n* `CIO_0000031`: low confidence level http://purl.obolibrary.org/obo/CIO_0000031\n* `CIO_0000030`: medium confidence level http://purl.obolibrary.org/obo/CIO_0000030\n* `CIO_0000039`: rejected http://purl.obolibrary.org/obo/CIO_0000039",
                                "name": "Confidence",
                                "namespace": "org.opencb.biodata.models.variant.avro",
                                "symbols": [
                                    "low_confidence_level",
                                    "medium_confidence_level",
                                    "high_confidence_level",
                                    "rejected"
                                ],
                                "type": "enum"
                            }
                        ]
                    },
                    {
                        "doc": "The automatic consistency status. The value is automatically inferred from evidences.",
                        "name": "automaticConsistencyStatus",
                        "type": {
                            "doc": "The consistency of evidences for a given phenotype. This aggregates all evidences for a given phenotype and all\n    evidences with no phenotype associated (e.g.: in silico impact prediction, population frequency).\n    This is based on the Confidence Information Ontology terms.\n\n* `CIO_0000033`: congruent, all evidences are consistent. http://purl.obolibrary.org/obo/CIO_0000033\n* `CIO_0000034`: conflict, there are conflicting evidences. This should correspond to a `VariantClassification` of\n`uncertain_significance` for mendelian disorders. http://purl.obolibrary.org/obo/CIO_0000034\n* `CIO_0000035`: strongly conflicting. http://purl.obolibrary.org/obo/CIO_0000035\n* `CIO_0000036`: weakly conflicting. http://purl.obolibrary.org/obo/CIO_0000036",
                            "name": "ConsistencyStatus",
                            "namespace": "org.opencb.biodata.models.variant.avro",
                            "symbols": [
                                "congruent",
                                "conflict",
                                "weakly_conflicting",
                                "strongly_conflicting"
                            ],
                            "type": "enum"
                        }
                    },
                    {
                        "doc": "The manual consistency status. The value is optionally provided by a curator.",
                        "name": "manualConsistencyStatus",
                        "type": [
                            "null",
                            "org.opencb.biodata.models.variant.avro.ConsistencyStatus"
                        ]
                    },
                    {
                        "doc": "The penetrance of the phenotype for this genotype. Value in the range [0, 1]",
                        "name": "penetrance",
                        "type": [
                            "null",
                            {
                                "doc": "Penetrance assumed in the analysis",
                                "name": "Penetrance",
                                "namespace": "org.opencb.biodata.models.variant.avro",
                                "symbols": [
                                    "complete",
                                    "incomplete"
                                ],
                                "type": "enum"
                            }
                        ]
                    },
                    {
                        "doc": "Variable expressivity of a given phenotype for the same genotype",
                        "name": "variableExpressivity",
                        "type": [
                            "null",
                            "boolean"
                        ]
                    },
                    {
                        "doc": "Can this variant be reported as a secondary finding?",
                        "name": "reportableAsSecondaryFinding",
                        "type": [
                            "null",
                            "boolean"
                        ]
                    },
                    {
                        "doc": "Is this variant actionable?",
                        "name": "actionable",
                        "type": [
                            "null",
                            "boolean"
                        ]
                    },
                    {
                        "doc": "Confirmation flag to support two-step curation",
                        "name": "confirmed",
                        "type": [
                            "null",
                            "boolean"
                        ]
                    },
                    {
                        "default": [],
                        "doc": "A list of additional properties in the form name-value.",
                        "name": "additionalProperties",
                        "type": {
                            "items": {
                                "doc": "A property in the form of name-value pair.\nNames are restricted to ontology ids, they should be checked against existing ontologies in resources like\nOntology Lookup Service.",
                                "fields": [
                                    {
                                        "doc": "The ontology term id or accession in OBO format ${ONTOLOGY_ID}:${TERM_ID} (http://www.obofoundry.org/id-policy.html)",
                                        "name": "id",
                                        "type": [
                                            "null",
                                            "string"
                                        ]
                                    },
                                    {
                                        "doc": "The ontology term name",
                                        "name": "name",
                                        "type": [
                                            "null",
                                            "string"
                                        ]
                                    },
                                    {
                                        "doc": "Optional value for the ontology term, the type of the value is not checked\n(i.e.: we could set the pvalue term to \"significant\" or to \"0.0001\")",
                                        "name": "value",
                                        "type": [
                                            "null",
                                            "string"
                                        ]
                                    }
                                ],
                                "name": "Property",
                                "namespace": "org.opencb.biodata.models.variant.avro",
                                "type": "record"
                            },
                            "type": "array"
                        }
                    },
                    {
                        "default": [],
                        "doc": "Bibliography",
                        "name": "bibliography",
                        "type": {
                            "items": "string",
                            "type": "array"
                        }
                    }
                ],
                "name": "Curation",
                "type": "record"
            }
        }
    ],
    "name": "CurationAndVariants",
    "namespace": "org.gel.models.cva.avro",
    "type": "record"
}