{
    "doc": "A tumour sample",
    "fields": [
        {
            "doc": "Sample identifier (e.g, LP00012645_5GH))",
            "name": "sampleId",
            "type": "string"
        },
        {
            "doc": "Lab sample identifier",
            "name": "labSampleId",
            "type": "string"
        },
        {
            "doc": "LDP Code (Local Delivery Partner)",
            "name": "LDPCode",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "This is the ID of the tumour from which this tumour sample was taken from",
            "name": "tumourId",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Genomics England programme phase",
            "name": "programmePhase",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Disease type.\nNOTE: Deprecated in GMS",
            "name": "diseaseType",
            "type": [
                "null",
                {
                    "name": "diseaseType",
                    "symbols": [
                        "ADULT_GLIOMA",
                        "BLADDER",
                        "BREAST",
                        "CARCINOMA_OF_UNKNOWN_PRIMARY",
                        "CHILDHOOD",
                        "COLORECTAL",
                        "ENDOCRINE",
                        "ENDOMETRIAL_CARCINOMA",
                        "HAEMONC",
                        "HEPATOPANCREATOBILIARY",
                        "LUNG",
                        "MALIGNANT_MELANOMA",
                        "NASOPHARYNGEAL",
                        "ORAL_OROPHARYNGEAL",
                        "OVARIAN",
                        "PROSTATE",
                        "RENAL",
                        "SARCOMA",
                        "SINONASAL",
                        "TESTICULAR_GERM_CELL_TUMOURS",
                        "UPPER_GASTROINTESTINAL",
                        "OTHER",
                        "NON_HODGKINS_B_CELL_LYMPHOMA_LOW_MOD_GRADE",
                        "CLASSICAL_HODGKINS",
                        "NODULAR_LYMPHOCYTE_PREDOMINANT_HODGKINS",
                        "T_CELL_LYMPHOMA"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "Disease subtype.\nNOTE: Deprecated in GMS",
            "name": "diseaseSubType",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "True or false if this sample is of type: Haematological Cancer",
            "name": "haematologicalCancer",
            "type": [
                "null",
                "boolean"
            ]
        },
        {
            "doc": "This is the Haematological cancer lineage of the tumourSample if this sample is from a haematological cancer",
            "name": "haematologicalCancerLineage",
            "type": [
                "null",
                {
                    "name": "HaematologicalCancerLineage",
                    "symbols": [
                        "MYELOID",
                        "LYMPHOID",
                        "UNKNOWN"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "The time when the sample was received. In the format YYYY-MM-DDTHH:MM:SS+0000",
            "name": "clinicalSampleDateTime",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Tumor type.\nNOTE: Deprecated in GMS in tumourSample but available in tumour record",
            "name": "tumourType",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "This is the tumour content",
            "name": "tumourContent",
            "type": [
                "null",
                {
                    "name": "TumourContent",
                    "symbols": [
                        "High",
                        "Medium",
                        "Low"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "This is the tumour content percentage",
            "name": "tumourContentPercentage",
            "type": [
                "null",
                "float"
            ]
        },
        {
            "doc": "Source of the sample",
            "name": "source",
            "type": [
                "null",
                {
                    "doc": "The source of the sample\nNOTE: IN GMS, BONE_MARROW_ASPIRATE_TUMOUR_CELLS and BONE_MARROW_ASPIRATE_TUMOUR_SORTED_CELLS are deprecated as they have been separated into their respective biotypes",
                    "name": "SampleSource",
                    "symbols": [
                        "AMNIOTIC_FLUID",
                        "BLOOD",
                        "BLOOD_CAPILLARY_HEEL_PRICK",
                        "BLOOD_CLOT",
                        "BLOOD_CORD",
                        "BLOOD_DRIED_BLOOD_SPOT",
                        "BONE_MARROW",
                        "BONE_MARROW_ASPIRATE_TUMOUR_CELLS",
                        "BONE_MARROW_ASPIRATE_TUMOUR_SORTED_CELLS",
                        "BUCCAL_SPONGE",
                        "BUCCAL_SWAB",
                        "BUFFY_COAT",
                        "CHORIONIC_VILLUS_SAMPLE",
                        "FIBROBLAST",
                        "FLUID",
                        "FRESH_TISSUE_IN_CULTURE_MEDIUM",
                        "OTHER",
                        "SALIVA",
                        "TISSUE",
                        "TUMOUR",
                        "URINE"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "The preparation method of the sample\nNOTE: Deprecated in GMS in replace of Method and storageMedium record",
            "name": "preparationMethod",
            "type": [
                "null",
                {
                    "doc": "In 100K, preparation Method of sample\nNOTE: In GMS, this field is deprecated in favour of StorageMedium and Method",
                    "name": "PreparationMethod",
                    "symbols": [
                        "ASPIRATE",
                        "CD128_SORTED_CELLS",
                        "CD138_SORTED_CELLS",
                        "EDTA",
                        "FF",
                        "FFPE",
                        "LI_HEP",
                        "ORAGENE"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "The tissue source of the sample.\nNOTE: DEPRECATED IN GMS in replace of method record",
            "name": "tissueSource",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Product of the sample",
            "name": "product",
            "type": [
                "null",
                {
                    "name": "Product",
                    "symbols": [
                        "DNA",
                        "RNA"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "Morphology according to the sample taken",
            "name": "sampleMorphologies",
            "type": [
                "null",
                {
                    "items": {
                        "fields": [
                            {
                                "doc": "The ontology term id or accession in OBO format ${ONTOLOGY_ID}:${TERM_ID} (http://www.obofoundry.org/id-policy.html)",
                                "name": "id",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "The ontology term name",
                                "name": "name",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Optional value for the ontology term, the type of the value is not checked\n(i.e.: we could set the pvalue term to \"significant\" or to \"0.0001\")",
                                "name": "value",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Ontology version",
                                "name": "version",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            }
                        ],
                        "name": "Morphology",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Topography according to the sample taken",
            "name": "sampleTopographies",
            "type": [
                "null",
                {
                    "items": {
                        "fields": [
                            {
                                "doc": "The ontology term id or accession in OBO format ${ONTOLOGY_ID}:${TERM_ID} (http://www.obofoundry.org/id-policy.html)",
                                "name": "id",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "The ontology term name",
                                "name": "name",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Optional value for the ontology term, the type of the value is not checked\n(i.e.: we could set the pvalue term to \"significant\" or to \"0.0001\")",
                                "name": "value",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Ontology version",
                                "name": "version",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            }
                        ],
                        "name": "Topography",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "In GMS, this is the GUID of the sample",
            "name": "sampleUid",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Participant Id of the sample",
            "name": "participantId",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "Participant UId of the sample",
            "name": "participantUid",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "In GMS, this is the maskedPID",
            "name": "maskedPid",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "In GMS, this is how the sample was extracted from the participant",
            "name": "method",
            "type": [
                "null",
                {
                    "doc": "In GMS, Method is defined as how the sample was taken directly from the patient",
                    "name": "Method",
                    "symbols": [
                        "ASPIRATE",
                        "BIOPSY",
                        "NOT_APPLICABLE",
                        "RESECTION",
                        "SORTED_OTHER",
                        "UNKNOWN",
                        "UNSORTED",
                        "CD138_SORTED"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "In GMS, this is what solvent/medium the sample was stored in",
            "name": "storageMedium",
            "type": [
                "null",
                {
                    "doc": "In GMS, storage medium of sample",
                    "name": "StorageMedium",
                    "symbols": [
                        "EDTA",
                        "FF",
                        "LI_HEP",
                        "ORAGENE",
                        "FFPE",
                        "PAXGENE",
                        "RNALater"
                    ],
                    "type": "enum"
                }
            ]
        },
        {
            "doc": "In GMS, this is the sampleType as entered by the clinician in TOMs",
            "name": "sampleType",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "In GMS, this is the sampleState as entered by the clinician in TOMs",
            "name": "sampleState",
            "type": [
                "null",
                "string"
            ]
        }
    ],
    "name": "TumourSample",
    "namespace": "org.gel.models.participant.avro",
    "type": "record"
}