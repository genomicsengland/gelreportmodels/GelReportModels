{
    "fields": [
        {
            "doc": "Unique identifier of the study.",
            "name": "studyId",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "default": [],
            "doc": "List of files from the study where the variant was present.",
            "name": "files",
            "type": {
                "items": {
                    "fields": [
                        {
                            "doc": "Unique identifier of the source file.",
                            "name": "fileId",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Original call position for the variant, if the file was normalized.\n\n{position}:{reference}:{alternate}(,{other_alternate})*:{allele_index}",
                            "name": "call",
                            "type": [
                                "null",
                                {
                                    "fields": [
                                        {
                                            "doc": "Original variant ID before normalization including all secondary alternates.",
                                            "name": "variantId",
                                            "type": "string"
                                        },
                                        {
                                            "doc": "Alternate allele index of the original multi-allellic variant call in which was decomposed.",
                                            "name": "alleleIndex",
                                            "type": [
                                                "null",
                                                "int"
                                            ]
                                        }
                                    ],
                                    "name": "OriginalCall",
                                    "type": "record"
                                }
                            ]
                        },
                        {
                            "doc": "Optional data that probably depend on the format of the file the\nvariant was initially read from.",
                            "name": "data",
                            "type": {
                                "type": "map",
                                "values": "string"
                            }
                        }
                    ],
                    "name": "FileEntry",
                    "type": "record"
                },
                "type": "array"
            }
        },
        {
            "default": null,
            "doc": "Alternate alleles that appear along with a variant alternate.",
            "name": "secondaryAlternates",
            "type": [
                "null",
                {
                    "items": {
                        "fields": [
                            {
                                "name": "chromosome",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "First position 1-based of the alternate. If null, the start is the same of the variant.",
                                "name": "start",
                                "type": [
                                    "null",
                                    "int"
                                ]
                            },
                            {
                                "doc": "End position 1-based of the alternate. If null, the end is the same of the variant.",
                                "name": "end",
                                "type": [
                                    "null",
                                    "int"
                                ]
                            },
                            {
                                "doc": "Reference allele. If null, the reference is the same of the variant.",
                                "name": "reference",
                                "type": [
                                    "null",
                                    "string"
                                ]
                            },
                            {
                                "doc": "Alternate allele.",
                                "name": "alternate",
                                "type": "string"
                            },
                            {
                                "name": "type",
                                "type": {
                                    "doc": "Type of variation, which depends mostly on its length.\n<ul>\n<li>SNVs involve a single nucleotide, without changes in length</li>\n<li>MNVs involve multiple nucleotides, without changes in length</li>\n<li>Indels are insertions or deletions of less than SV_THRESHOLD (50) nucleotides</li>\n<li>Structural variations are large changes of more than SV_THRESHOLD nucleotides</li>\n<li>Copy-number variations alter the number of copies of a region</li>\n</ul>",
                                    "name": "VariantType",
                                    "symbols": [
                                        "SNV",
                                        "MNV",
                                        "INDEL",
                                        "SV",
                                        "INSERTION",
                                        "DELETION",
                                        "TRANSLOCATION",
                                        "INVERSION",
                                        "COPY_NUMBER",
                                        "COPY_NUMBER_GAIN",
                                        "COPY_NUMBER_LOSS",
                                        "DUPLICATION",
                                        "TANDEM_DUPLICATION",
                                        "BREAKEND",
                                        "NO_VARIATION",
                                        "SYMBOLIC",
                                        "MIXED",
                                        "SNP",
                                        "MNP",
                                        "CNV"
                                    ],
                                    "type": "enum"
                                }
                            }
                        ],
                        "name": "AlternateCoordinate",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Fields stored for each sample.",
            "name": "sampleDataKeys",
            "type": {
                "items": "string",
                "type": "array"
            }
        },
        {
            "doc": "Genotypes and other sample-related information. Each position is related\nwith one sample. The content are lists of values in the same order than the\nsampleDataKeys array. The length of this lists must be the same as the sampleDataKeys field.",
            "name": "samples",
            "type": {
                "items": {
                    "fields": [
                        {
                            "name": "sampleId",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "name": "fileIndex",
                            "type": [
                                "null",
                                "int"
                            ]
                        },
                        {
                            "name": "data",
                            "type": {
                                "items": "string",
                                "type": "array"
                            }
                        }
                    ],
                    "name": "SampleEntry",
                    "type": "record"
                },
                "type": "array"
            }
        },
        {
            "default": [],
            "name": "issues",
            "type": {
                "items": {
                    "fields": [
                        {
                            "name": "type",
                            "type": {
                                "name": "IssueType",
                                "symbols": [
                                    "DUPLICATION",
                                    "DISCREPANCY",
                                    "MENDELIAN_ERROR",
                                    "DE_NOVO",
                                    "COMPOUND_HETEROZYGOUS"
                                ],
                                "type": "enum"
                            }
                        },
                        {
                            "name": "sample",
                            "type": "SampleEntry"
                        },
                        {
                            "name": "data",
                            "type": {
                                "type": "map",
                                "values": "string"
                            }
                        }
                    ],
                    "name": "IssueEntry",
                    "type": "record"
                },
                "type": "array"
            }
        },
        {
            "doc": "Statistics of the genomic variation, such as its alleles/genotype count\nor its minimum allele frequency, grouped by cohort name.",
            "name": "stats",
            "type": {
                "items": {
                    "fields": [
                        {
                            "doc": "Unique cohort identifier within the study.\n",
                            "name": "cohortId",
                            "type": "string"
                        },
                        {
                            "doc": "Count of samples with non-missing genotypes in this variant from the cohort.\nThis value is used as denominator for genotypeFreq.\n",
                            "name": "sampleCount",
                            "type": [
                                "null",
                                "int"
                            ]
                        },
                        {
                            "doc": "Count of files with samples from the cohort that reported this variant.\nThis value is used as denominator for filterFreq.\n",
                            "name": "fileCount",
                            "type": [
                                "null",
                                "int"
                            ]
                        },
                        {
                            "doc": "Total number of alleles in called genotypes. It does not include missing alleles.\nThis value is used as denominator for refAlleleFreq and altAlleleFreq.\n",
                            "name": "alleleCount",
                            "type": [
                                "null",
                                "int"
                            ]
                        },
                        {
                            "doc": "Number of reference alleles found in this variant.\n",
                            "name": "refAlleleCount",
                            "type": [
                                "null",
                                "int"
                            ]
                        },
                        {
                            "doc": "Number of main alternate alleles found in this variants. It does not include secondary alternates.\n",
                            "name": "altAlleleCount",
                            "type": [
                                "null",
                                "int"
                            ]
                        },
                        {
                            "doc": "Reference allele frequency calculated from refAlleleCount and alleleCount, in the range [0,1]\n",
                            "name": "refAlleleFreq",
                            "type": [
                                "null",
                                "float"
                            ]
                        },
                        {
                            "doc": "Alternate allele frequency calculated from altAlleleCount and alleleCount, in the range [0,1]\n",
                            "name": "altAlleleFreq",
                            "type": [
                                "null",
                                "float"
                            ]
                        },
                        {
                            "doc": "Number of missing alleles.\n",
                            "name": "missingAlleleCount",
                            "type": [
                                "null",
                                "int"
                            ]
                        },
                        {
                            "doc": "Number of genotypes with all alleles missing (e.g. ./.). It does not count partially missing genotypes like \"./0\" or \"./1\".\n",
                            "name": "missingGenotypeCount",
                            "type": [
                                "null",
                                "int"
                            ]
                        },
                        {
                            "default": {},
                            "doc": "Number of occurrences for each genotype.\nThis does not include genotype with all alleles missing (e.g. ./.), but it includes partially missing genotypes like \"./0\" or \"./1\".\nTotal sum of counts should be equal to the count of samples.\n",
                            "name": "genotypeCount",
                            "type": {
                                "type": "map",
                                "values": "int"
                            }
                        },
                        {
                            "default": {},
                            "doc": "Genotype frequency for each genotype found calculated from the genotypeCount and samplesCount, in the range [0,1]\nThe sum of frequencies should be 1.\n",
                            "name": "genotypeFreq",
                            "type": {
                                "type": "map",
                                "values": "float"
                            }
                        },
                        {
                            "doc": "The number of occurrences for each FILTER value in files from samples in this cohort reporting this variant.\nAs each file can contain more than one filter value (usually separated by ';'), the total sum of counts could be greater than the count of files.\n",
                            "name": "filterCount",
                            "type": {
                                "type": "map",
                                "values": "int"
                            }
                        },
                        {
                            "doc": "Frequency of each filter calculated from the filterCount and filesCount, in the range [0,1]\n",
                            "name": "filterFreq",
                            "type": {
                                "type": "map",
                                "values": "float"
                            }
                        },
                        {
                            "doc": "The number of files from samples in this cohort reporting this variant with valid QUAL values.\nThis value is used as denominator to obtain the qualityAvg.",
                            "name": "qualityCount",
                            "type": [
                                "null",
                                "int"
                            ]
                        },
                        {
                            "doc": "The average Quality value for files with valid QUAL values from samples in this cohort reporting this variant.\nSome files may not have defined the QUAL value, so the sampling could be less than the filesCount.\n",
                            "name": "qualityAvg",
                            "type": [
                                "null",
                                "float"
                            ]
                        },
                        {
                            "doc": "Minor allele frequency. Frequency of the less common allele between the reference and the main alternate alleles.\nThis value does not take into acconunt secondary alternates.\n",
                            "name": "maf",
                            "type": [
                                "null",
                                "float"
                            ]
                        },
                        {
                            "doc": "Minor genotype frequency. Frequency of the less common genotype seen in this variant.\nThis value takes into account all values from the genotypeFreq map.\n",
                            "name": "mgf",
                            "type": [
                                "null",
                                "float"
                            ]
                        },
                        {
                            "doc": "Allele with minor frequency.\n",
                            "name": "mafAllele",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Genotype with minor frequency.\n",
                            "name": "mgfGenotype",
                            "type": [
                                "null",
                                "string"
                            ]
                        }
                    ],
                    "name": "VariantStats",
                    "type": "record"
                },
                "type": "array"
            }
        },
        {
            "default": [],
            "name": "scores",
            "type": {
                "items": {
                    "fields": [
                        {
                            "doc": "Variant score ID.",
                            "name": "id",
                            "type": "string"
                        },
                        {
                            "doc": "Main cohort used for calculating the score.",
                            "name": "cohort1",
                            "type": "string"
                        },
                        {
                            "default": null,
                            "doc": "Optional secondary cohort used for calculating the score.",
                            "name": "cohort2",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Score value",
                            "name": "score",
                            "type": "float"
                        },
                        {
                            "default": null,
                            "doc": "Score p value",
                            "name": "pValue",
                            "type": [
                                "null",
                                "float"
                            ]
                        }
                    ],
                    "name": "VariantScore",
                    "type": "record"
                },
                "type": "array"
            }
        }
    ],
    "name": "StudyEntry",
    "namespace": "org.opencb.biodata.models.variant.avro",
    "type": "record"
}