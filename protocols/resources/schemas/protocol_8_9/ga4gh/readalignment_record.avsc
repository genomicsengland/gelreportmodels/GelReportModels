{
    "doc": "Each read alignment describes an alignment with additional information\nabout the fragment and the read. A read alignment object is equivalent to a\nline in a SAM file.",
    "fields": [
        {
            "doc": "The read alignment ID. This ID is unique within the read group this\nalignment belongs to. This field may not be provided by all backends.\nIts intended use is to make caching and UI display easier for\ngenome browsers and other light weight clients.",
            "name": "id",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "doc": "The ID of the read group this read belongs to.\n(Every read must belong to exactly one read group.)",
            "name": "readGroupId",
            "type": "string"
        },
        {
            "doc": "The fragment ID that this ReadAlignment belongs to.",
            "name": "fragmentId",
            "type": "string"
        },
        {
            "doc": "The fragment name. Equivalent to QNAME (query template name) in SAM.",
            "name": "fragmentName",
            "type": "string"
        },
        {
            "default": null,
            "doc": "The orientation and the distance between reads from the fragment are\nconsistent with the sequencing protocol (equivalent to SAM flag 0x2)",
            "name": "properPlacement",
            "type": [
                "null",
                "boolean"
            ]
        },
        {
            "default": null,
            "doc": "The fragment is a PCR or optical duplicate (SAM flag 0x400)",
            "name": "duplicateFragment",
            "type": [
                "null",
                "boolean"
            ]
        },
        {
            "default": null,
            "doc": "The number of reads in the fragment (extension to SAM flag 0x1)",
            "name": "numberReads",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "default": null,
            "doc": "The observed length of the fragment, equivalent to TLEN in SAM.",
            "name": "fragmentLength",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "default": null,
            "doc": "The read number in sequencing. 0-based and less than numberReads. This field\nreplaces SAM flag 0x40 and 0x80.",
            "name": "readNumber",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "default": null,
            "doc": "SAM flag 0x200",
            "name": "failedVendorQualityChecks",
            "type": [
                "null",
                "boolean"
            ]
        },
        {
            "default": null,
            "doc": "The alignment for this alignment record. This field will be null if the read\nis unmapped.",
            "name": "alignment",
            "type": [
                "null",
                {
                    "doc": "A linear alignment can be represented by one CIGAR string.",
                    "fields": [
                        {
                            "doc": "The position of this alignment.",
                            "name": "position",
                            "type": {
                                "doc": "A `Position` is an unoriented base in some `Reference`. A `Position` is\nrepresented by a `Reference` name, and a base number on that `Reference`\n(0-based).",
                                "fields": [
                                    {
                                        "doc": "The name of the `Reference` on which the `Position` is located.",
                                        "name": "referenceName",
                                        "type": "string"
                                    },
                                    {
                                        "doc": "The 0-based offset from the start of the forward strand for that `Reference`.\nGenomic positions are non-negative integers less than `Reference` length.",
                                        "name": "position",
                                        "type": "long"
                                    },
                                    {
                                        "doc": "Strand the position is associated with.",
                                        "name": "strand",
                                        "type": {
                                            "doc": "Indicates the DNA strand associate for some data item.\n* `NEG_STRAND`: The negative (-) strand.\n* `POS_STRAND`:  The postive (+) strand.",
                                            "name": "Strand",
                                            "symbols": [
                                                "NEG_STRAND",
                                                "POS_STRAND"
                                            ],
                                            "type": "enum"
                                        }
                                    }
                                ],
                                "name": "Position",
                                "type": "record"
                            }
                        },
                        {
                            "default": null,
                            "doc": "The mapping quality of this alignment. Represents how likely\nthe read maps to this position as opposed to other locations.",
                            "name": "mappingQuality",
                            "type": [
                                "null",
                                "int"
                            ]
                        },
                        {
                            "default": [],
                            "doc": "Represents the local alignment of this sequence (alignment matches, indels, etc)\nversus the reference.",
                            "name": "cigar",
                            "type": {
                                "items": {
                                    "doc": "A structure for an instance of a CIGAR operation.",
                                    "fields": [
                                        {
                                            "doc": "The operation type.",
                                            "name": "operation",
                                            "type": {
                                                "doc": "An enum for the different types of CIGAR alignment operations that exist.\nUsed wherever CIGAR alignments are used. The different enumerated values\nhave the following usage:\n\n* `ALIGNMENT_MATCH`: An alignment match indicates that a sequence can be\n  aligned to the reference without evidence of an INDEL. Unlike the\n  `SEQUENCE_MATCH` and `SEQUENCE_MISMATCH` operators, the `ALIGNMENT_MATCH`\n  operator does not indicate whether the reference and read sequences are an\n  exact match. This operator is equivalent to SAM's `M`.\n* `INSERT`: The insert operator indicates that the read contains evidence of\n  bases being inserted into the reference. This operator is equivalent to\n  SAM's `I`.\n* `DELETE`: The delete operator indicates that the read contains evidence of\n  bases being deleted from the reference. This operator is equivalent to\n  SAM's `D`.\n* `SKIP`: The skip operator indicates that this read skips a long segment of\n  the reference, but the bases have not been deleted. This operator is\n  commonly used when working with RNA-seq data, where reads may skip long\n  segments of the reference between exons. This operator is equivalent to\n  SAM's 'N'.\n* `CLIP_SOFT`: The soft clip operator indicates that bases at the start/end\n  of a read have not been considered during alignment. This may occur if the\n  majority of a read maps, except for low quality bases at the start/end of\n  a read. This operator is equivalent to SAM's 'S'. Bases that are soft clipped\n  will still be stored in the read.\n* `CLIP_HARD`: The hard clip operator indicates that bases at the start/end of\n  a read have been omitted from this alignment. This may occur if this linear\n  alignment is part of a chimeric alignment, or if the read has been trimmed\n  (e.g., during error correction, or to trim poly-A tails for RNA-seq). This\n  operator is equivalent to SAM's 'H'.\n* `PAD`: The pad operator indicates that there is padding in an alignment.\n  This operator is equivalent to SAM's 'P'.\n* `SEQUENCE_MATCH`: This operator indicates that this portion of the aligned\n  sequence exactly matches the reference (e.g., all bases are equal to the\n  reference bases). This operator is equivalent to SAM's '='.\n* `SEQUENCE_MISMATCH`: This operator indicates that this portion of the\n  aligned sequence is an alignment match to the reference, but a sequence\n  mismatch (e.g., the bases are not equal to the reference). This can\n  indicate a SNP or a read error. This operator is equivalent to SAM's 'X'.",
                                                "name": "CigarOperation",
                                                "symbols": [
                                                    "ALIGNMENT_MATCH",
                                                    "INSERT",
                                                    "DELETE",
                                                    "SKIP",
                                                    "CLIP_SOFT",
                                                    "CLIP_HARD",
                                                    "PAD",
                                                    "SEQUENCE_MATCH",
                                                    "SEQUENCE_MISMATCH"
                                                ],
                                                "type": "enum"
                                            }
                                        },
                                        {
                                            "doc": "The number of bases that the operation runs for.",
                                            "name": "operationLength",
                                            "type": "long"
                                        },
                                        {
                                            "default": null,
                                            "doc": "`referenceSequence` is only used at mismatches (`SEQUENCE_MISMATCH`)\nand deletions (`DELETE`). Filling this field replaces the MD tag.\nIf the relevant information is not available, leave this field as `null`.",
                                            "name": "referenceSequence",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        }
                                    ],
                                    "name": "CigarUnit",
                                    "type": "record"
                                },
                                "type": "array"
                            }
                        }
                    ],
                    "name": "LinearAlignment",
                    "type": "record"
                }
            ]
        },
        {
            "default": null,
            "doc": "Whether this alignment is secondary. Equivalent to SAM flag 0x100.\n  A secondary alignment represents an alternative to the primary alignment\n  for this read. Aligners may return secondary alignments if a read can map\n  ambiguously to multiple coordinates in the genome.\n\n  By convention, each read has one and only one alignment where both\n  secondaryAlignment and supplementaryAlignment are false.",
            "name": "secondaryAlignment",
            "type": [
                "null",
                "boolean"
            ]
        },
        {
            "default": null,
            "doc": "Whether this alignment is supplementary. Equivalent to SAM flag 0x800.\n  Supplementary alignments are used in the representation of a chimeric\n  alignment. In a chimeric alignment, a read is split into multiple\n  linear alignments that map to different reference contigs. The first\n  linear alignment in the read will be designated as the representative alignment;\n  the remaining linear alignments will be designated as supplementary alignments.\n  These alignments may have different mapping quality scores.\n\n  In each linear alignment in a chimeric alignment, the read will be hard clipped.\n  The `alignedSequence` and `alignedQuality` fields in the alignment record will\n  only represent the bases for its respective linear alignment.",
            "name": "supplementaryAlignment",
            "type": [
                "null",
                "boolean"
            ]
        },
        {
            "default": null,
            "doc": "The bases of the read sequence contained in this alignment record.\n`alignedSequence` and `alignedQuality` may be shorter than the full read sequence\nand quality. This will occur if the alignment is part of a chimeric alignment,\nor if the read was trimmed. When this occurs, the CIGAR for this read will\nbegin/end with a hard clip operator that will indicate the length of the excised sequence.",
            "name": "alignedSequence",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "default": [],
            "doc": "The quality of the read sequence contained in this alignment record.\n`alignedSequence` and `alignedQuality` may be shorter than the full read sequence\nand quality. This will occur if the alignment is part of a chimeric alignment,\nor if the read was trimmed. When this occurs, the CIGAR for this read will\nbegin/end with a hard clip operator that will indicate the length of the excised sequence.",
            "name": "alignedQuality",
            "type": {
                "items": "int",
                "type": "array"
            }
        },
        {
            "default": null,
            "doc": "The mapping of the primary alignment of the `(readNumber+1)%numberReads`\nread in the fragment. It replaces mate position and mate strand in SAM.",
            "name": "nextMatePosition",
            "type": [
                "null",
                "Position"
            ]
        },
        {
            "default": {},
            "doc": "A map of additional read alignment information.",
            "name": "info",
            "type": {
                "type": "map",
                "values": {
                    "items": "string",
                    "type": "array"
                }
            }
        }
    ],
    "name": "ReadAlignment",
    "namespace": "org.ga4gh.models",
    "type": "record"
}