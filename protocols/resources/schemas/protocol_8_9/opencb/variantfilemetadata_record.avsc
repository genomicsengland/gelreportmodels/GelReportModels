{
    "fields": [
        {
            "doc": "File id. Will match with the {@link org.opencb.biodata.models.variant.avro.FileEntry#getFileId}",
            "name": "id",
            "type": "string"
        },
        {
            "default": null,
            "doc": "Path to the original file",
            "name": "path",
            "type": [
                "null",
                "string"
            ]
        },
        {
            "default": [],
            "doc": "Ordered list of sample ids contained in the file",
            "name": "sampleIds",
            "type": {
                "items": "string",
                "type": "array"
            }
        },
        {
            "default": null,
            "doc": "Global statistics calculated for this file",
            "name": "stats",
            "type": [
                "null",
                {
                    "doc": "Variant statistics for a set of variants.\nThe variants set can be contain a whole study, a cohort, a sample, a region, ...",
                    "fields": [
                        {
                            "doc": "Number of variants in the variant set",
                            "name": "variantCount",
                            "type": "long"
                        },
                        {
                            "doc": "Number of samples in the variant set",
                            "name": "sampleCount",
                            "type": "long"
                        },
                        {
                            "doc": "The number of occurrences for each FILTER value in files from this set.\nEach file can contain more than one filter value (usually separated by ';').\n",
                            "name": "filterCount",
                            "type": {
                                "type": "map",
                                "values": "long"
                            }
                        },
                        {
                            "default": {},
                            "doc": "Number of genotypes found for all samples in variants set",
                            "name": "genotypeCount",
                            "type": {
                                "type": "map",
                                "values": "long"
                            }
                        },
                        {
                            "doc": "Number of files in the variant set",
                            "name": "filesCount",
                            "type": "long"
                        },
                        {
                            "doc": "TiTvRatio = num. transitions / num. transversions",
                            "name": "tiTvRatio",
                            "type": "float"
                        },
                        {
                            "doc": "Mean Quality for all the variants with quality",
                            "name": "qualityAvg",
                            "type": "float"
                        },
                        {
                            "doc": "Standard Deviation of the quality",
                            "name": "qualityStdDev",
                            "type": "float"
                        },
                        {
                            "default": {},
                            "doc": "Variants count group by type. e.g. SNP, INDEL, MNP, SNV, ...",
                            "name": "typeCount",
                            "type": {
                                "type": "map",
                                "values": "long"
                            }
                        },
                        {
                            "default": {},
                            "doc": "Variants count group by biotype. e.g. protein-coding, miRNA, lncRNA, ...",
                            "name": "biotypeCount",
                            "type": {
                                "type": "map",
                                "values": "long"
                            }
                        },
                        {
                            "default": {},
                            "doc": "Variants count group by consequence type. e.g. synonymous_variant, missense_variant, stop_lost, ...",
                            "name": "consequenceTypeCount",
                            "type": {
                                "type": "map",
                                "values": "long"
                            }
                        },
                        {
                            "default": {},
                            "doc": "Number of variants per chromosome",
                            "name": "chromosomeCount",
                            "type": {
                                "type": "map",
                                "values": "long"
                            }
                        },
                        {
                            "default": {},
                            "doc": "Total density of variants within the chromosome. counts / chromosome.length",
                            "name": "chromosomeDensity",
                            "type": {
                                "type": "map",
                                "values": "float"
                            }
                        }
                    ],
                    "name": "VariantSetStats",
                    "type": "record"
                }
            ]
        },
        {
            "default": null,
            "doc": "The Variant File Header",
            "name": "header",
            "type": [
                "null",
                {
                    "doc": "Variant File Header. Contains simple and complex metadata lines describing the content of the file.\nThis header matches with the VCF header.\nA header may have multiple Simple or Complex lines with the same key",
                    "fields": [
                        {
                            "name": "version",
                            "type": "string"
                        },
                        {
                            "default": [],
                            "doc": "complex lines, e.g. INFO=<ID=NS,Number=1,Type=Integer,Description=\"Number of samples with data\">",
                            "name": "complexLines",
                            "type": {
                                "items": {
                                    "fields": [
                                        {
                                            "doc": "Key of group of the Complex Header Line, e.g. INFO, FORMAT, FILTER, ALT, ...",
                                            "name": "key",
                                            "type": "string"
                                        },
                                        {
                                            "doc": "ID or Name of the line",
                                            "name": "id",
                                            "type": "string"
                                        },
                                        {
                                            "default": null,
                                            "doc": "The description",
                                            "name": "description",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "default": null,
                                            "doc": "Arity of the values associated with this metadata line.\nOnly present if the metadata line describes data fields, i.e. key == INFO or FORMAT\nAccepted values:\n  - <Integer>: The field has always this number of values.\n  - A: The field has one value per alternate allele.\n  - R: The field has one value for each possible allele, including the reference.\n  - G: The field has one value for each possible genotype\n  - .: The number of possible values varies, is unknown or unbounded.",
                                            "name": "number",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "default": null,
                                            "doc": "Type of the values associated with this metadata line.\nOnly present if the metadata line describes data fields, i.e. key == INFO or FORMAT\nAccepted values:\n  - Integer\n  - Float\n  - String\n  - Character\n  - Flag",
                                            "name": "type",
                                            "type": [
                                                "null",
                                                "string"
                                            ]
                                        },
                                        {
                                            "default": {},
                                            "doc": "Other optional fields",
                                            "name": "genericFields",
                                            "type": {
                                                "type": "map",
                                                "values": "string"
                                            }
                                        }
                                    ],
                                    "name": "VariantFileHeaderComplexLine",
                                    "type": "record"
                                },
                                "type": "array"
                            }
                        },
                        {
                            "default": [],
                            "doc": "simple lines, e.g. fileDate=20090805",
                            "name": "simpleLines",
                            "type": {
                                "items": {
                                    "fields": [
                                        {
                                            "doc": "Key of group of the Simple Header Line, e.g. source, assembly, pedigreeDB, ...",
                                            "name": "key",
                                            "type": "string"
                                        },
                                        {
                                            "doc": "Value",
                                            "name": "value",
                                            "type": "string"
                                        }
                                    ],
                                    "name": "VariantFileHeaderSimpleLine",
                                    "type": "record"
                                },
                                "type": "array"
                            }
                        }
                    ],
                    "name": "VariantFileHeader",
                    "type": "record"
                }
            ]
        },
        {
            "default": {},
            "doc": "Other user defined attributes related with the file",
            "name": "attributes",
            "type": {
                "type": "map",
                "values": "string"
            }
        }
    ],
    "name": "VariantFileMetadata",
    "namespace": "org.opencb.biodata.models.variant.metadata",
    "type": "record"
}