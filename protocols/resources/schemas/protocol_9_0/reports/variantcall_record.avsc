{
    "doc": "This is intended to hold the genotypes for the family members. This assumes that varinats have been split before.\nIn principle it is a phased zygosity as in VCF spec and called by the analysis provider if further phasing is conducted",
    "fields": [
        {
            "doc": "Participant id",
            "name": "participantId",
            "type": "string"
        },
        {
            "doc": "Sample Id",
            "name": "sampleId",
            "type": "string"
        },
        {
            "doc": "Zygosity. For somatic variants, or variants without zygosity use `na`",
            "name": "zygosity",
            "type": {
                "doc": "It is a representation of the zygosity\n\n* `reference_homozygous`: 0/0, 0|0\n* `heterozygous`: 0/1, 1/0, 1|0, 0|1\n* `alternate_homozygous`: 1/1, 1|1\n* `missing`: ./., .|.\n* `half_missing_reference`: ./0, 0/., 0|., .|0\n* `half_missing_alternate`: ./1, 1/., 1|., .|1\n* `alternate_hemizigous`: 1\n* `reference_hemizigous`: 0\n* `unk`: Anything unexpected",
                "name": "Zygosity",
                "symbols": [
                    "reference_homozygous",
                    "heterozygous",
                    "alternate_homozygous",
                    "missing",
                    "half_missing_reference",
                    "half_missing_alternate",
                    "alternate_hemizigous",
                    "reference_hemizigous",
                    "unk",
                    "na"
                ],
                "type": "enum"
            }
        },
        {
            "doc": "phase alleles for those in phase",
            "name": "phaseGenotype",
            "type": [
                "null",
                {
                    "fields": [
                        {
                            "name": "sortedAlleles",
                            "type": {
                                "items": "string",
                                "type": "array"
                            }
                        },
                        {
                            "name": "phaseSet",
                            "type": "int"
                        }
                    ],
                    "name": "PhaseGenotype",
                    "type": "record"
                }
            ]
        },
        {
            "doc": "Sample Variant Allele Frequency",
            "name": "sampleVariantAlleleFrequency",
            "type": [
                "null",
                "double"
            ]
        },
        {
            "doc": "Depth for Reference Allele",
            "name": "depthReference",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "doc": "Depth for Alternate Allele",
            "name": "depthAlternate",
            "type": [
                "null",
                "int"
            ]
        },
        {
            "doc": "Alleles for copy number variation - add doc",
            "name": "numberOfCopies",
            "type": [
                "null",
                {
                    "items": {
                        "fields": [
                            {
                                "doc": "Number of copies given by the caller in one of the allele",
                                "name": "numberOfCopies",
                                "type": "int"
                            },
                            {
                                "name": "confidenceIntervalMaximum",
                                "type": [
                                    "null",
                                    "int"
                                ]
                            },
                            {
                                "name": "confidenceIntervalMinimum",
                                "type": [
                                    "null",
                                    "int"
                                ]
                            }
                        ],
                        "name": "NumberOfCopies",
                        "type": "record"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "doc": "Describe whether this is a somatic or Germline variant",
            "name": "alleleOrigins",
            "type": [
                "null",
                {
                    "items": {
                        "doc": "Variant origin.\n* `SO_0001781`: de novo variant. http://purl.obolibrary.org/obo/SO_0001781\n* `SO_0001778`: germline variant. http://purl.obolibrary.org/obo/SO_0001778\n* `SO_0001775`: maternal variant. http://purl.obolibrary.org/obo/SO_0001775\n* `SO_0001776`: paternal variant. http://purl.obolibrary.org/obo/SO_0001776\n* `SO_0001779`: pedigree specific variant. http://purl.obolibrary.org/obo/SO_0001779\n* `SO_0001780`: population specific variant. http://purl.obolibrary.org/obo/SO_0001780\n* `SO_0001777`: somatic variant. http://purl.obolibrary.org/obo/SO_0001777",
                        "name": "AlleleOrigin",
                        "symbols": [
                            "de_novo_variant",
                            "germline_variant",
                            "maternal_variant",
                            "paternal_variant",
                            "pedigree_specific_variant",
                            "population_specific_variant",
                            "somatic_variant",
                            "unknown"
                        ],
                        "type": "enum"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "name": "supportingReadTypes",
            "type": [
                "null",
                {
                    "items": {
                        "name": "SupportingReadType",
                        "symbols": [
                            "spanning",
                            "flanking",
                            "inrepeat"
                        ],
                        "type": "enum"
                    },
                    "type": "array"
                }
            ]
        },
        {
            "name": "source",
            "type": [
                "null",
                {
                    "doc": "The source of the variant call",
                    "fields": [
                        {
                            "doc": "Name of source",
                            "name": "name",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "Version of source",
                            "name": "version",
                            "type": [
                                "null",
                                "string"
                            ]
                        },
                        {
                            "doc": "The source date.",
                            "name": "date",
                            "type": [
                                "null",
                                "string"
                            ]
                        }
                    ],
                    "name": "VariantSource",
                    "type": "record"
                }
            ]
        }
    ],
    "name": "VariantCall",
    "namespace": "org.gel.models.report.avro",
    "type": "record"
}