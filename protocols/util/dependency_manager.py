import importlib
import json
import logging
import os.path
import re

from packaging import version
from packaging.version import InvalidVersion

from protocols.resources import RESOURCE_DIR
from protocols.util.singleton import Singleton

VERSION_90 = "9.0"
VERSION_89 = "8.9"
VERSION_86 = "8.6"
VERSION_83 = "8.3"
VERSION_81 = "8.1"
VERSION_80 = "8.0"
VERSION_713 = "7.13"
VERSION_712 = "7.12"
VERSION_7112 = "7.11.2"
VERSION_7111 = "7.11.1"
VERSION_711 = "7.11"
VERSION_7102 = "7.10.2"
VERSION_7101 = "7.10.1"
VERSION_710 = "7.10"
VERSION_79 = "7.9"
VERSION_78 = "7.8"
VERSION_77 = "7.7"
VERSION_76 = "7.6"
VERSION_74 = "7.4"
VERSION_73 = "7.3"
VERSION_70 = "7.0"
VERSION_721 = "7.2.1"
VERSION_72 = "7.2"
VERSION_61 = "6.1"
VERSION_500 = "5.0.0"
VERSION_400 = "4.0.0"
VERSION_300 = "3.0.0"
VERSION_210 = "2.1.0"


RE_SEMANTIC = re.compile(
    r"(?P<major>\d+)(\.(?P<minor>\d+)(\.(?P<patch>\d+)){0,1}){0,1}"
)


class DependencyManager:
    """
    Singleton class that reads builds.json file at the root of the project
    and provides utils for dependency management.
    """

    __metaclass__ = Singleton

    def __init__(self):
        dependencies_json = os.path.join(RESOURCE_DIR, "builds.json")
        if not os.path.exists(dependencies_json):
            raise ValueError(
                "Not found config file '{}'. Try running 'mvn initialize'".format(
                    dependencies_json
                )
            )
        with open(dependencies_json) as dependencies_handle:
            builds = json.load(dependencies_handle)["builds"]

        # prepares resource: version -> namespace -> python package
        self.builds = {}
        for build in builds:
            build_name = DependencyManager.get_python_protocol_name(build)
            self.builds[build["version"]] = {}
            for package in build["packages"]:
                namespace = package["package"]
                _module = DependencyManager.get_python_module(build_name, package)
                self.builds[build["version"]][namespace] = _module

    def get_version_dependencies(self, version):
        if version not in self.builds:
            raise ValueError("Version {} not available in builds.json!".format(version))
        return self.builds[version]

    def get_latest_version_dependencies(self):
        return self.builds[self.get_latest_version()]

    def get_latest_version(self):
        build_versions = []
        for build in self.builds.keys():
            try:
                build_versions.append(version.parse(build))
            except InvalidVersion:
                # LegacyVersion naming conventions (x.x.x-package-name) were deprecated in packaging v22
                # Our latest versions do not use legacy formatting so we're safe to skip those versions
                # when retrieving latest version dependencies.
                logging.warning(
                    "Could not parse version '%s'. Most likely a deprecated LegacyVersion"
                    % build
                )
        latest_version = max(build_versions)
        return latest_version.base_version

    def get_package_version(self, package, build_version):
        return self.builds[build_version][package]

    @staticmethod
    def get_python_package_name(package):
        """
        Returns the python package name built from a Gel package definition.
        :param package: the package dict
        :return: the python package name
        """
        package_base = package["python_package"]
        package_version = DependencyManager.process_version(package["version"])
        package_name = "{}_{}".format(
            package_base, "_".join([str(v) for v in package_version])
        )
        return package_name.lower()

    @staticmethod
    def get_python_protocol_name(build):
        """Replaces the dots in the provided version with underscores for use in folders names

        :param build: Build information that includes the version
        :type build: dict[str, Any]
        :return: Protocol folder name
        :rtype: str
        """
        return "protocol_{}".format(build["version"].split("-", 1)[0].replace(".", "_"))

    @staticmethod
    def get_python_module(build_name, package):
        package_name = package["python_package"]
        _module = importlib.import_module(
            "protocols.{}.{}".format(build_name, package_name)
        )
        return _module

    @staticmethod
    def remove_hotfix_version(version):
        versions = version.split(".")
        if len(versions) < 2:
            raise ValueError(
                "Version needs to have at least a major and minor versions"
            )
        return ".".join(versions[0:2])

    @staticmethod
    def process_version(version):
        result = RE_SEMANTIC.search(version)
        info = {}
        if result:
            info = result.groupdict()
        return tuple(
            [
                int(info.get(v, "")) if info.get(v) else 0
                for v in ["major", "minor", "patch"]
            ]
        )
