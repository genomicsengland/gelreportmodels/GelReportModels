#!/usr/bin/env python
from __future__ import print_function

import argparse
import fnmatch
import glob
import hashlib
import json
import logging
import os
import os.path
import subprocess
import time
from multiprocessing import Pool

import protocols_utils
from protocols_utils.code_generation.process_schemas import (
    PyGenerator,
    RSTGenerator,
    SchemaClass,
)
from protocols_utils.utils.makedir import makedir

logging.basicConfig(level=logging.DEBUG)


BASE_DIR = os.path.dirname(__file__)
AVRO_TOOLS_JAR = os.path.join(
    BASE_DIR, "../..", "resources/bin", "avro-tools-1.11.3.jar"
)
GA4GH_CODE_GENERATION = os.path.join(BASE_DIR, "code_generation", "process_schemas.py")
IDL_EXTENSION = "avdl"
JSON_EXTENSION = "avsc"
AVPR_EXTENSION = "avpr"
HTML_EXTENSION = "html"


class ConversionTools:
    @classmethod
    def entry(cls):
        parser = argparse.ArgumentParser(
            description="GEL models toolbox",
            usage="""conversion_tools.py <command> [<args>]""",
        )
        parser.add_argument(
            "command",
            help="Subcommand to run (idl2json|idl2avpr|json2java|idl2python|avpr2html|update_docs_index|buildVersionPackage)",
        )
        parser.add_argument("--input", help="Input folder")
        parser.add_argument("--output", help="Output folder")
        parser.add_argument("--use_pool", action="store_true")
        # parse_args defaults to [1:] for args, but you need to
        # exclude the rest of the args too, or validation will fail
        args, unknown = parser.parse_known_args()
        if not hasattr(cls, args.command):
            print("Unrecognized command")
            parser.print_help()
            exit(1)
        # use dispatch pattern to invoke method with same name
        getattr(cls, args.command)(
            input_path=args.input, output_path=args.output, use_pool=args.use_pool
        )

    @classmethod
    def run_commands(cls, commands, use_pool=False):
        if use_pool:
            Pool(processes=8).starmap(
                cls.run_command, [(command,) for command in commands]
            )
        else:
            for command in commands:
                cls.run_command(command)

    @classmethod
    def run_command(cls, command, fail_if_error=True, cwd=None, attempt=1):
        """
        Runs a given command
        :param cwd:
        :param fail_if_error:
        :param command:
        :return:
        """
        if attempt > 1:
            logging.info("Running: [{}] Attempt:{}".format(command, attempt))
        else:
            logging.info("Running: [{}]".format(command))

        if cwd is not None:
            sp = subprocess.Popen(
                command,
                shell=True,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                cwd=cwd,
            )
        else:
            sp = subprocess.Popen(
                command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
            )
        stdout, stderr = sp.communicate()

        if stdout is not None and stdout != b"":
            if "errno = 11" in stdout.decode() and attempt < 5:
                # repeat command if there are is a read clash
                time.sleep(1)
                cls.run_command(
                    command, fail_if_error=fail_if_error, cwd=cwd, attempt=attempt + 1
                )
                return
            logging.info(stdout.decode())

        # raise an error if sort return code is other than 0
        if sp.returncode:
            if stderr is not None and stderr != b"":
                logging.error(stderr.decode())
            error_message = "Command [{0}] returned error code [{1}]".format(
                command, str(sp.returncode)
            )
            logging.error(error_message)
            if fail_if_error:
                raise ValueError(error_message)

    @classmethod
    def idl2json(cls, input_path="", output_path="", use_pool=False, unknown_args=None):
        """
        Transform all IDL files in input folder to AVRO schemas in the output folder
        :return:
        """

        logging.info("idl2json")
        # some early models have issues with using the same names for different enums.
        # legacy behaviour of running id2lschemata sequentially, is maintained for these models.
        # to ensure models are rebuilt in the same manner.
        makedir(output_path)

        idl_list = [
            os.path.join(input_path, file)
            for file in os.listdir(input_path)
            if os.path.isfile(os.path.join(input_path, file))
            and file.endswith(IDL_EXTENSION)
        ]

        command_list = [
            "java -jar {} idl2schemata {} {}".format(
                AVRO_TOOLS_JAR,
                idl,
                output_path,
            )
            for idl in idl_list
        ]
        cls.run_commands(command_list, use_pool=use_pool)

        for schema_name in os.listdir(output_path):
            schema_path = os.path.join(output_path, schema_name)
            if not os.path.isfile(schema_path) or not schema_name.endswith(".avsc"):
                continue

            with open(schema_path) as f:
                schema = json.load(f)
            new_path = os.path.join(
                output_path, "{}_{}.avsc".format(schema["name"].lower(), schema["type"])
            )
            with open(new_path, "wt") as f:
                json.dump(
                    schema,
                    f,
                    indent=4,
                    sort_keys=True,
                )

            os.remove(schema_path)

    @classmethod
    def json2java(
        cls, input_path="", output_path="", use_pool=False, unknown_args=None
    ):
        """
        Transform all JSON Avro schemas in a given folder to Java source code.
        :return:
        """

        logging.info("json2java")

        makedir(output_path)
        jsons = [
            os.path.join(input_path, file)
            for file in os.listdir(input_path)
            if os.path.isfile(os.path.join(input_path, file))
            and file.endswith(JSON_EXTENSION)
        ]

        command_list = [
            "java -jar {} compile -string schema {} {}".format(
                AVRO_TOOLS_JAR,
                json,
                output_path,
            )
            for json in jsons
        ]
        cls.run_commands(command_list, use_pool=use_pool)

    @classmethod
    def json2python(cls, json_folder, python_folder, package, dependencies):
        """
        Transforms all IDL JSON schemas in a given folder to Python source code.
        :return:
        """
        logging.info("json2python")
        python_output = os.path.join(
            python_folder,
            package["python_package"],
        )
        makedir(python_output)

        package_init = os.path.join(python_folder, "__init__.py")
        if not os.path.exists(package_init):
            with open(package_init, "w") as f:
                f.write("")

        sg = PyGenerator(
            package,
            dependencies,
            json_folder,
            python_output,
        )
        sg.write()

    @classmethod
    def update_python_linkers(cls, input_folder, build, python_name):
        for package in build["packages"]:
            linker_file = os.path.join(
                input_folder, "{}.py".format(package["python_package"])
            )

            with open(linker_file, "w") as handle:
                handle.write(
                    "from protocols.{}.{} import *\n".format(
                        python_name, package["python_package"]
                    )
                )

    # Documentation functions

    @classmethod
    def json2rst(cls, json_folder, doc_folder, package, dependencies):
        """
        Transforms all IDL JSON schemas in a given folder to Python source code.
        :return:
        """
        logging.info("json2rst")
        doc_output = os.path.join(
            doc_folder,
            "{}-{}".format(package["python_package"], package["version"]),
        )
        makedir(doc_output)

        RSTGenerator(
            package,
            dependencies,
            json_folder,
            doc_output,
        ).write()

    @classmethod
    def sphinx_build(cls, folder):
        """
        Transforms all AVPR schemas in a given folder to HTML documentation.
        :return:
        """
        logging.info("Building documentation...")

        cls.run_command("sphinx-build -b html -d dist/doctrees source dist", cwd=folder)

    @classmethod
    def get_md5_of_path(cls, path):
        """Gets the md5 checksum of the given path. If the path is directory is will process all the directory tree.

        :param path: Path to calculate
        :type path: str
        """

        if not os.path.exists(path):
            return ""

        repo_dir = os.path.dirname(os.path.dirname(protocols_utils.__file__))

        if os.path.isdir(path):
            file_list = []
            for root, _, files in os.walk(path):
                relroot = os.path.relpath(root, repo_dir)
                file_list.extend(
                    [
                        os.path.join(relroot, file)
                        for file in files
                        if file.rsplit(".", 1)[-1] in ["py", "avdl", "avsc"]
                    ]
                )

            data = "\n".join(
                str(p) + "=" + cls.get_md5_of_path(str(p)) for p in sorted(file_list)
            )

        if os.path.isfile(path):
            with open(path, "rt") as handle:
                data = handle.read()

        return cls.get_md5(data)

    @classmethod
    def get_md5(cls, data):
        return hashlib.md5(str(data).encode()).hexdigest()


if __name__ == "__main__":
    ConversionTools.entry()
