import errno
import os


def makedir(path):
    """Makes provided path if it doesn't already exist.

    :param path: Path
    :type path: str
    """
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise
