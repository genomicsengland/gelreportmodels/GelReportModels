@namespace("org.gel.models.participant.avro")
/**
This protocol defines all models for Newborns related data.
*/


protocol Newborns {


    import idl "CommonParticipant.avdl";


    /**
    Preterm labour.
    */
    enum PretermLabour {IDIOPATHIC, CHORIOAMNIONITIS, ABRUPTION, STRETCH}

    /**
    Induced birth.
    */
    enum InducedBirth {PROSTAGLANDIN, BALLOON, ARTIFICIAL_RUPTURE_OF_MEMBRANES, ARTIFICIAL_RUPTURE_OF_MEMBRANES_MEMBRANE_SWEEP, OXYTOCIN}

    /**
    Mode of delivery.
    */
    enum ModeOfDelivery {SPONTANEOUS_VAGINAL, INSTRUMENTAL_BIRTH, OTHER_ASSISTED_DELIVERY, CAESAREAN_SECTION_ELECTIVE, CAESAREAN_SECTION_EMERGENCY, CAESAREAN_SECTION_CATEGORY_1, CAESAREAN_SECTION_CATEGORY_2, CAESAREAN_SECTION_CATEGORY_3_AND_4}

    /**
    Membrane status.
    */
    enum MembraneStatus {ARTIFICIAL_RUPTURE_FOR_INDUCTION_OF_LABOUR, BEFORE_LABOUR, DURING_LABOUR, AT_BIRTH}

    /**
    Delivery complications.
    The `abnormally_invasive_placenta` includes accrete, increta or percreta.
    */
    enum DeliveryComplications {PLACENTA_ABRUPTION, ABNORMAL_PLACENTATION, PLACENTA_PRAEVIA, ABNORMALLY_INVASIVE_PLACENTA, CORD_RUPTURE, INFECTION, MANUAL_REMOVAL_OF_PLACENTA}

    /**
    Third stage - to establish if type of third stage impacts on the quantity of cord blood available for whole genome sequencing.
    `physiological` - A physiological or natural third stage means waiting for the placenta to be delivered naturally with delay. A clamp is placed on the umbilical cord until it has stopped pulsating to allow oxygenated blood to pulse from the placenta to the baby.
    `active` - This can refer too:
        1) giving oxytocin IM/IV to help contract the uterus.
        2) clamping the cord early (usually before, alongside, or immediately after giving oxytocin IM/IV.
        3) traction is applied to the cord with counter‐pressure on the uterus to deliver the placenta.
    `removal_at_caesarean_section` - birth occured through caesarean section.
    */
    enum ThirdStageOfLabour {PHYSIOLOGICAL, ACTIVE, REMOVAL_AT_CAESAREAN_SECTION}

    /**
    Baby intake (feed or medication).
    */
    enum BabyIntake {MATERNAL_BREAST_MILK, DONOR_BREAST_MILK, INFANT_FORMULA, MEDICATION}

    /**
    Model that captures approximate duration of labour duration (this is the onset of regular contractions until delivery of placenta).
    */
    record DurationOfLabour {
        /**
        First stage (from 3cm and regular contractions) in hours.
        */
        union {null, float} firstStageHourDuration;

        /**
        Second stage (pushing) in hours.
        */
        union {null, float} secondStageHourDuration;

        /**
        Third stage (delivery of placenta) in minutes.
        */
        union {null, int} thirdStageMinuteDuration;

        /**
        Date and time of labour onset.
        String in ISO 8601 format with timezone and no microsecond.
        format: %Y-%m-%dT%H:%M:%S+00:00
        e.g.: 2020-03-20T14:31:43+01:00
        */
        union {null, string} dateAndTimeOfLabourOnset;
    }

    /**
    Time between oral intake and sample collection (to establish contamination levels and lead to understanding optimum collection time).
    */
    record BabyOralIntakeQuestions {
        /**
        Has the baby been fed or taken medication before sample was collected.
        */
        boolean neverBeenFed;

        /**
        Time since last oral intake (feed or medication) in minutes. NOTE: Only fill in if baby was fed before sample collection.
        */
        union {null, int} minutesSinceOralIntake;

        /**
        Type of last intake (feed or medication). NOTE: Only fill in if baby was fed before sample collection.
        */
        union {null, BabyIntake} babyIntake;
    }

    /**
    Model that captures the clinical questions from birth.
    */
    record BirthClinicalQuestions {

        /**
        Gravida Number - Gravida indicates the number of times a woman is or has been pregnant, regardless of the pregnancy outcome.
        */
        union {null, int} gravidaNumber;

        /**
        String capturing the date and time of the delivery in ISO 8601 format with timezone and no microsecond.
        format: %Y-%m-%dT%H:%M:%S+00:00
        e.g.: 2020-03-20T14:31:43+01:00
        */
        union {null, string} dateAndTimeOfDelivery;

        /**
        Parity Number - Parity, or "para", indicates the number of births (including live births and stillbirths) where pregnancies reached viable gestational age.
        */
        union {null, int} parityNumber;

        /**
        Number of babies delivered in this pregnancy.
        */
        union {null, int} numberOfBabiesDelivered;

        /**
        Pregnancy Term in weeks.
        */
        union {null, int} pregnancyTermWeeks;

        /**
        Preterm labour if any.
        */
        union {null, array<PretermLabour>} pretermLabour;

        /**
        Induced birth if any.
        */
        union {null, array<InducedBirth>} inducedBirth;

        /**
        Mode of delivery.
        */
        union {null, array<ModeOfDelivery>} modeOfDelivery;

        /**
        Membrane status.
        */
        union {null, array<MembraneStatus>} membraneStatus;

        /**
        Delivery Complications.
        */
        union {null, array<DeliveryComplications>} deliveryComplications;

        /**
        Third stage - to establish if type of third stage impacts on the quantity of cord blood available for whole genome sequencing.
        */
        union {null, array<ThirdStageOfLabour>} thirdStageOfLabour;

        /**
        Approximate duration of labour duration (this is the onset of regular contractions until delivery of placenta).
        */
        union {null, DurationOfLabour} durationOfLabour;

        /**
        Baby oral intake questions before sample (e.g. buccal swab, buccal sponge) was collected.
        */
        union {null, BabyOralIntakeQuestions} babyOralIntakeQuestions;

        /**
        How long ago (in minutes) since mum had oral intake of food or fluids.
        */
        union {null, int} mumsTimeSinceLastOralIntake;
    }
}