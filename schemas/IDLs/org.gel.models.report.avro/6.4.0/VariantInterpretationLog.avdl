@namespace("org.gel.models.report.avro")

protocol VariantInterpretationLogProtocol {

    import idl "CommonInterpreted.avdl";
    import idl "ReportVersionControl.avdl";

    enum ValidationResult{NotPerformed, Confirmed, NotConfirmed, Pending}

    enum ReportCategory{primary_finding, secondary_finding, do_not_report, other}

    record VariantValidation{
        /**
        Technology used to perform secondary confirmation of this variant (e.g. Sanger)
        */
        string validationTechnology;
        /**
        Status/outcome of validation
        */
        ValidationResult validationResult;
    }

    record StructuralVariantDetails{
        /**
        Strctural variant type
        */
        StructuralVariantType variantType;
        /**
        Structural variant coordinates
        */
        Coordinates structuralVariantCoordinates;
        /**
        Number of copies (required for copy number variants)
        */
        union {null, array<NumberOfCopies>} numberOfCopies;
    }

    record ShortTandemRepeatDetails{
        /**
        Short tandem repeat coordinates
        */
        Coordinates shortTandemRepeatCoordinates;
        /**
        Repeat motif e.g. CAG
        */
        string repeatedSequence;
        /**
        Short tandem repeat copy number for each allele
        */
        union {null, array<NumberOfCopies>} numberOfCopies;
    }

    record UniparentalDisomyDetails{
        /**
        Reference assembly
        */
        Assembly assembly;
        /**
        Chromosome where two homologues were inherited from one parent
        */
        string chromosome;
        /**
        indicates whether UPD event involves an entire chromosome
        */
        union {null, boolean} complete;
        /**
        The parent who contributed two chromosomes was the mother (maternal) or the father (paternal)
        */
        UniparentalDisomyOrigin origin;
        /**
        List of all of the UPD fragments for this UPD event
        */
        union {null, array<UniparentalDisomyFragment>} uniparentalDisomyFragments;

    }

    record SelectedReportEvent{
        /**
        interpretation service for the interpreted genome this report event was taken from
        */
        union {null, string} interpretationService;
        /**
        Interpreted genome version, stored as cip_version in CIPAPI, for the interpreted genome this report event was taken from
        */
        union {null, int} cipVersion;
        /**
        Report event
        */
        ReportEvent reportEvent;
    }

    record VariantInterpretationLog{
        /**
        Model version number
        */
        ReportVersionControl versionControl;
        /**
        Variant coordinates
        */
        VariantCoordinates variantCoordinates;
        /**
        Genomic entities associated with this variant interpretation e.g. gene
        */
        union {null, array<GenomicEntity>} genomicEntities;
        /**
        HGVS cDNA change for transcript selected by user. Format <TranscriptID>:<HGVS> e.g. ENST00000621744:c.8837T>C
        */
        union {null, string} selectedCdnaChange;
        /**
        HGVS protein change for transcript selected by user. Format <ProteinID>:<HGVS> e.g. ENSP00000478752:p.Val2946Ala
        */
        union {null, string} selectedProteinChange;
        /**
        User who set classification
        */
        User user;
        /**
        Date and time variant was classified (ISO 8601 datetime with seconds and timezone e.g. 2020-11-23T15:52:36+00:00)
        */
        string timestamp;
        /**
        GeL group ID. For GMS cases this will be the referral ID. For 100k rare disease cases this will be the family ID. For 100k cancer cases this will be the participant ID.
        */
        string groupId;
        /**
        Interpretation request ID including CIP prefix and version suffix (e.g. SAP-1234-1)
        */
        string caseId;
        /**
        Independent validation of variants
        */
        union {null, VariantValidation} variantValidation;
        /**
        User comments attached to this variant in this case
        */
        union {null, array<UserComment>} comments;
        /**
        Guideline-based variant classification
        */
        GuidelineBasedVariantClassification variantClassification;
        /**
        User has marked the variant as an artefact
        */
        union {null, boolean} Artifact;
        /**
        Was this variant recorded as explaining the patient's phenotype?
        */
        union {null, PhenotypesSolved} phenotypesSolved;
        /**
        The decision on whether to report this variant or not as recorded at time of interpretation.
        */
        union {null, ReportCategory} reportCategory;
        /**
        Filter settings applied at time variant was classified
        */
        union {null, map<string>} decisionSupportSystemFilters;
        /**
        Publications flagged by the user as relevant
        */
        union {null, array<Publication>} flaggedPublications;
        /**
        Report event(s) selected by user when interpreting this variant. Contains information such as phenotype and mode of inheritance.
        */
        union {null, array<SelectedReportEvent>} selectedReportEvents;
        /**
        Additional information relevant to this variant interpretation
        */
        union {null, map<string>} additionalInfo;
    }

    record StructuralVariantInterpretationLog{
        /**
        Model version number
        */
        ReportVersionControl versionControl;
        /**
        Variant details
        */
        StructuralVariantDetails variant;
        /**
        Genomic entities associated with this variant interpretation e.g. gene
        */
        union {null, array<GenomicEntity>} genomicEntities;
        /**
        User who set classification
        */
        User user;
        /**
        Date and time variant was classified (ISO 8601 datetime with seconds and timezone e.g. 2020-11-23T15:52:36+00:00)
        */
        string timestamp;
        /**
        GeL group ID. For GMS cases this will be the referral ID. For 100k rare disease cases this will be the family ID. For 100k cancer cases this will be the participant ID.
        */
        string groupId;
        /**
        Interpretation request ID including CIP prefix and version suffix (e.g. SAP-1234-1)
        */
        string caseId;
        /**
        Independent validation of variant
        */
        union {null, VariantValidation} variantValidation;
        /**
        User comments attached to this variant in this case
        */
        union {null, array<UserComment>} comments;
        /**
        Variant classification
        */
        ClinicalSignificance variantClassification;
        /**
        User has marked the variant as an artefact
        */
        union {null, boolean} Artifact;
        /**
        Was this variant recorded as explaining the patient's phenotype?
        */
        union {null, PhenotypesSolved} phenotypesSolved;
        /**
        The decision on whether to report this variant or not as recorded at time of interpretation.
        */
        union {null, ReportCategory} reportCategory;
        /**
        Filter settings applied at time variant was classified
        */
        union {null, map<string>} decisionSupportSystemFilters;
        /**
        Publications flagged by the user as relevant
        */
        union {null, array<Publication>} flaggedPublications;
        /**
        Report event(s) selected by user when interpreting this variant. Contains information such as phenotype and mode of inheritance.
        */
        union {null, array<SelectedReportEvent>} selectedReportEvents;
        /**
        Additional information relevant to this variant interpretation
        */
        union {null, map<string>} additionalInfo;
    }

    record ChromosomalRearrangementInterpretationLog{
        /**
        Model version number
        */
        ReportVersionControl versionControl;
        /**
        Chromosomal rearrangement breakpoints
        */
        union {null, array<BreakPoint>} breakPoints;
        /**
        Genomic entities associated with this variant interpretation e.g. gene
        */
        union {null, array<GenomicEntity>} genomicEntities;
        /**
        User who set classification
        */
        User user;
        /**
        Date and time variant was classified (ISO 8601 datetime with seconds and timezone e.g. 2020-11-23T15:52:36+00:00)
        */
        string timestamp;
        /**
        GeL group ID. For GMS cases this will be the referral ID. For 100k rare disease cases this will be the family ID. For 100k cancer cases this will be the participant ID.
        */
        string groupId;
        /**
        Interpretation request ID including CIP prefix and version suffix (e.g. SAP-1234-1)
        */
        string caseId;
        /**
        Independent validation of variant
        */
        union {null, VariantValidation} variantValidation;
        /**
        User comments attached to this variant in this case
        */
        union {null, array<UserComment>} comments;
        /**
        Variant classification
        */
        ClinicalSignificance variantClassification;
        /**
        User has marked the variant as an artefact
        */
        union {null, boolean} Artifact;
        /**
        Was this variant recorded as explaining the patient's phenotype?
        */
        union {null, PhenotypesSolved} phenotypesSolved;
        /**
        The decision on whether to report this variant or not as recorded at time of interpretation.
        */
        union {null, ReportCategory} reportCategory;
        /**
        Filter settings applied at time variant was classified
        */
        union {null, map<string>} decisionSupportSystemFilters;
        /**
        Publications flagged by the user as relevant to this variant
        */
        union {null, array<Publication>} flaggedPublications;
        /**
        Report event(s) selected by user when interpreting this variant. Contains information such as phenotype and mode of inheritance.
        */
        union {null, array<SelectedReportEvent>} selectedReportEvents;
        /**
        Additional information relevant to this variant interpretation
        */
        union {null, map<string>} additionalInfo;
    }

   record ShortTandemRepeatInterpretationLog{
        /**
        Model version number
        */
        ReportVersionControl versionControl;
        /**
        Variant details
        */
        ShortTandemRepeatDetails variant;
        /**
        Genomic entities associated with this variant interpretation e.g. gene
        */
        union {null, array<GenomicEntity>} genomicEntities;
        /**
        User who set classification
        */
        User user;
        /**
        Date and time variant was classified (ISO 8601 datetime with seconds and timezone e.g. 2020-11-23T15:52:36+00:00)
        */
        string timestamp;
        /**
        GeL group ID. For GMS cases this will be the referral ID. For 100k rare disease cases this will be the family ID. For 100k cancer cases this will be the participant ID.
        */
        string groupId;
        /**
        Interpretation request ID including CIP prefix and version suffix (e.g. SAP-1234-1)
        */
        string caseId;
        /**
        Independent validation of variant
        */
        union {null, VariantValidation} variantValidation;
        /**
        User comments attached to this variant in this case
        */
        union {null, array<UserComment>} comments;
        /**
        Variant classification
        */
        ClinicalSignificance variantClassification;
        /**
        User has marked the variant as an artefact
        */
        union {null, boolean} Artifact;
        /**
        Was this variant recorded as explaining the patient's phenotype?
        */
        union {null, PhenotypesSolved} phenotypesSolved;
        /**
        The decision on whether to report this variant or not as recorded at time of interpretation.
        */
        union {null, ReportCategory} reportCategory;
        /**
        Filter settings applied at time variant was classified
        */
        union {null, map<string>} decisionSupportSystemFilters;
        /**
        Publications flagged by the user as relevant
        */
        union {null, array<Publication>} flaggedPublications;
        /**
        Report event(s) selected by user when interpreting this variant. Contains information such as phenotype and mode of inheritance.
        */
        union {null, array<SelectedReportEvent>} selectedReportEvents;
        /**
        Additional information relevant to this variant interpretation
        */
        union {null, map<string>} additionalInfo;
    }

   record UniparentalDisomyInterpretationLog{
        /**
        Model version number
        */
        ReportVersionControl versionControl;
        /**
        Variant details
        */
        UniparentalDisomyDetails variant;
        /**
        Genomic entities associated with this variant interpretation e.g. gene
        */
        union {null, array<GenomicEntity>} genomicEntities;
        /**
        User who set classification
        */
        User user;
        /**
        Date and time variant was classified (ISO 8601 datetime with seconds and timezone e.g. 2020-11-23T15:52:36+00:00)
        */
        string timestamp;
        /**
        GeL group ID. For GMS cases this will be the referral ID. For 100k rare disease cases this will be the family ID. For 100k cancer cases this will be the participant ID.
        */
        string groupId;
        /**
        Interpretation request ID including CIP prefix and version suffix (e.g. SAP-1234-1)
        */
        string caseId;
        /**
        Independent validation of variant
        */
        union {null, VariantValidation} variantValidation;
        /**
        User comments attached to this variant in this case
        */
        union {null, array<UserComment>} comments;
        /**
        Variant classification
        */
        ClinicalSignificance variantClassification;
        /**
        User has marked the variant as an artefact
        */
        union {null, boolean} Artifact;
        /**
        Was this variant recorded as explaining the patient's phenotype?
        */
        union {null, PhenotypesSolved} phenotypesSolved;
        /**
        The decision on whether to report this variant or not as recorded at time of interpretation.
        */
        union {null, ReportCategory} reportCategory;
        /**
        Filter settings applied at time variant was classified
        */
        union {null, map<string>} decisionSupportSystemFilters;
        /**
        Publications flagged by the user as relevant
        */
        union {null, array<Publication>} flaggedPublications;
        /**
        Report event(s) selected by user when interpreting this variant. Contains information such as phenotype and mode of inheritance.
        */
        union {null, array<SelectedReportEvent>} selectedReportEvents;
        /**
        Additional information relevant to this variant interpretation
        */
        union {null, map<string>} additionalInfo;
    }
}