{
  "type" : "record",
  "name" : "CancerGermlineVariantLevelQuestions",
  "namespace" : "org.gel.models.report.avro",
  "doc" : "The questions for the cancer program exit questionnaire for germline variants",
  "fields" : [ {
    "name" : "variantCoordinates",
    "type" : {
      "type" : "record",
      "name" : "VariantCoordinates",
      "doc" : "The variant coordinates representing uniquely a small variant.\n    No multi-allelic variant supported, alternate only represents one alternate allele.",
      "fields" : [ {
        "name" : "chromosome",
        "type" : "string",
        "doc" : "Chromosome without \"chr\" prefix (e.g. X rather than chrX)"
      }, {
        "name" : "position",
        "type" : "int",
        "doc" : "Genomic position"
      }, {
        "name" : "reference",
        "type" : "string",
        "doc" : "The reference bases."
      }, {
        "name" : "alternate",
        "type" : "string",
        "doc" : "The alternate bases"
      }, {
        "name" : "assembly",
        "type" : {
          "type" : "enum",
          "name" : "Assembly",
          "doc" : "The reference genome assembly",
          "symbols" : [ "GRCh38", "GRCh37" ]
        },
        "doc" : "The assembly to which this variant corresponds"
      } ]
    },
    "doc" : "Variant coordinates following format `chromosome:position:reference:alternate`"
  }, {
    "name" : "variantActionability",
    "type" : {
      "type" : "array",
      "items" : {
        "type" : "enum",
        "name" : "CancerActionability",
        "doc" : "An enumeration Variant Actionability:\n      * `predicts_therapeutic_response`: Predicts therapeutic response\n      * `prognostic`: Prognostic\n      * `defines_diagnosis_group`: Defines diagnosis group\n      * `eligibility_for_trial`: Eligibility for trial\n      * `germline_susceptibility`: Germline susceptibility\n      * `other`:  Other (please specify)",
        "symbols" : [ "germline_susceptibility", "predicts_therapeutic_response", "prognostic", "defines_diagnosis_group", "eligibility_for_trial", "other" ]
      }
    },
    "doc" : "Type of potential actionability:"
  }, {
    "name" : "otherVariantActionability",
    "type" : [ "null", "string" ]
  }, {
    "name" : "variantUsability",
    "type" : {
      "type" : "enum",
      "name" : "CancerUsabilityGermline",
      "doc" : "Variant usability for germline variants:\n* `already_actioned`: Already actioned (i.e. prior to receiving this WGA)\n* `actioned_result_of_this_wga`: actioned as a result of receiving this WGA",
      "symbols" : [ "already_actioned", "actioned_result_of_this_wga" ]
    },
    "doc" : "How has/will this potentially actionable variant been/be used?"
  }, {
    "name" : "variantTested",
    "type" : {
      "type" : "enum",
      "name" : "CancerTested",
      "doc" : "Was the variant validated with an orthogonal technology?\n* `not_indicated_for_patient_care`: No: not indicated for patient care at this time\n* `no_orthologous_test_available`: No: no orthologous test available\n* `test_performed_prior_to_wga`: Yes: test performed prior to receiving WGA (eg using standard-of-care assay such as panel testing, or sanger sequencing)\n* `technical_validation_following_WGA`: Yes: technical validation performed/planned following receiving this WGA",
      "symbols" : [ "not_indicated_for_patient_care", "no_orthologous_test_available", "test_performed_prior_to_wga", "technical_validation_following_wga" ]
    },
    "doc" : "Has this variant been tested by another method (either prior to or following receipt of this WGA)?"
  }, {
    "name" : "validationAssayType",
    "type" : "string",
    "doc" : "Please enter validation assay type e.g Pyrosequencing, NGS panel, COBAS, Sanger sequencing. If not applicable enter NA;"
  } ]
}
