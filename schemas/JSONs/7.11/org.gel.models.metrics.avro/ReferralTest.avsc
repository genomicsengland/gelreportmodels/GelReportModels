{
  "type" : "record",
  "name" : "ReferralTest",
  "namespace" : "org.gel.models.participant.avro",
  "fields" : [ {
    "name" : "referralTestId",
    "type" : "string",
    "doc" : "Test UID"
  }, {
    "name" : "referralTestOrderingDate",
    "type" : [ "null", {
      "type" : "record",
      "name" : "Date",
      "doc" : "This defines a date record",
      "fields" : [ {
        "name" : "year",
        "type" : "int",
        "doc" : "Format YYYY"
      }, {
        "name" : "month",
        "type" : [ "null", "int" ],
        "doc" : "Format MM. e.g June is 06"
      }, {
        "name" : "day",
        "type" : [ "null", "int" ],
        "doc" : "Format DD e.g. 12th of October is 12"
      } ]
    } ],
    "doc" : "The date of which the referralTest was sent to Bioinformatics"
  }, {
    "name" : "clinicalIndicationTest",
    "type" : {
      "type" : "record",
      "name" : "ClinicalIndicationTest",
      "fields" : [ {
        "name" : "clinicalIndicationTestTypeId",
        "type" : "string",
        "doc" : "Clinical indication Test type ID"
      }, {
        "name" : "clinicalIndicationTestTypeCode",
        "type" : "string",
        "doc" : "Clinical indication Test code (e.g. R13-1)"
      }, {
        "name" : "testTypeId",
        "type" : "string",
        "doc" : "Test Type Id"
      }, {
        "name" : "testTypeName",
        "type" : "string",
        "doc" : "Test Type Name"
      }, {
        "name" : "technology",
        "type" : {
          "type" : "record",
          "name" : "Technology",
          "fields" : [ {
            "name" : "testTechnologyId",
            "type" : "string",
            "doc" : "Technology unique identifier"
          }, {
            "name" : "testTechnologyDescription",
            "type" : "string",
            "doc" : "Technology description"
          } ]
        },
        "doc" : "Technology used in ClinicalIndicationTest"
      } ]
    },
    "doc" : "Clinical indication test"
  }, {
    "name" : "tumourSamples",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "TumourSample",
        "doc" : "A tumour sample",
        "fields" : [ {
          "name" : "sampleId",
          "type" : "string",
          "doc" : "Sample identifier (e.g, LP00012645_5GH))"
        }, {
          "name" : "labSampleId",
          "type" : "string",
          "doc" : "Lab sample identifier"
        }, {
          "name" : "LDPCode",
          "type" : [ "null", "string" ],
          "doc" : "LDP Code (Local Delivery Partner)"
        }, {
          "name" : "tumourId",
          "type" : [ "null", "string" ],
          "doc" : "This is the ID of the tumour from which this tumour sample was taken from"
        }, {
          "name" : "programmePhase",
          "type" : [ "null", "string" ],
          "doc" : "Genomics England programme phase"
        }, {
          "name" : "diseaseType",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "diseaseType",
            "symbols" : [ "ADULT_GLIOMA", "BLADDER", "BREAST", "CARCINOMA_OF_UNKNOWN_PRIMARY", "CHILDHOOD", "COLORECTAL", "ENDOCRINE", "ENDOMETRIAL_CARCINOMA", "HAEMONC", "HEPATOPANCREATOBILIARY", "LUNG", "MALIGNANT_MELANOMA", "NASOPHARYNGEAL", "ORAL_OROPHARYNGEAL", "OVARIAN", "PROSTATE", "RENAL", "SARCOMA", "SINONASAL", "TESTICULAR_GERM_CELL_TUMOURS", "UPPER_GASTROINTESTINAL", "OTHER", "NON_HODGKINS_B_CELL_LYMPHOMA_LOW_MOD_GRADE", "CLASSICAL_HODGKINS", "NODULAR_LYMPHOCYTE_PREDOMINANT_HODGKINS", "T_CELL_LYMPHOMA" ]
          } ],
          "doc" : "Disease type.\n        NOTE: Deprecated in GMS"
        }, {
          "name" : "diseaseSubType",
          "type" : [ "null", "string" ],
          "doc" : "Disease subtype.\n        NOTE: Deprecated in GMS"
        }, {
          "name" : "haematologicalCancer",
          "type" : [ "null", "boolean" ],
          "doc" : "True or false if this sample is of type: Haematological Cancer"
        }, {
          "name" : "haematologicalCancerLineage",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "HaematologicalCancerLineage",
            "symbols" : [ "MYELOID", "LYMPHOID", "UNKNOWN" ]
          } ],
          "doc" : "This is the Haematological cancer lineage of the tumourSample if this sample is from a haematological cancer"
        }, {
          "name" : "clinicalSampleDateTime",
          "type" : [ "null", "string" ],
          "doc" : "The time when the sample was received. In the format YYYY-MM-DDTHH:MM:SS+0000"
        }, {
          "name" : "tumourType",
          "type" : [ "null", "string" ],
          "doc" : "Tumor type.\n        NOTE: Deprecated in GMS in tumourSample but available in tumour record"
        }, {
          "name" : "tumourContent",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "TumourContent",
            "symbols" : [ "High", "Medium", "Low" ]
          } ],
          "doc" : "This is the tumour content"
        }, {
          "name" : "tumourContentPercentage",
          "type" : [ "null", "float" ],
          "doc" : "This is the tumour content percentage"
        }, {
          "name" : "source",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "SampleSource",
            "doc" : "The source of the sample\n    NOTE: IN GMS, BONE_MARROW_ASPIRATE_TUMOUR_CELLS and BONE_MARROW_ASPIRATE_TUMOUR_SORTED_CELLS are deprecated as they have been separated into their respective biotypes",
            "symbols" : [ "AMNIOTIC_FLUID", "BLOOD", "BLOOD_CAPILLARY_HEEL_PRICK", "BLOOD_CLOT", "BLOOD_CORD", "BLOOD_DRIED_BLOOD_SPOT", "BONE_MARROW", "BONE_MARROW_ASPIRATE_TUMOUR_CELLS", "BONE_MARROW_ASPIRATE_TUMOUR_SORTED_CELLS", "BUCCAL_SPONGE", "BUCCAL_SWAB", "BUFFY_COAT", "CHORIONIC_VILLUS_SAMPLE", "FIBROBLAST", "FLUID", "FRESH_TISSUE_IN_CULTURE_MEDIUM", "OTHER", "SALIVA", "TISSUE", "TUMOUR", "URINE" ]
          } ],
          "doc" : "Source of the sample"
        }, {
          "name" : "preparationMethod",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "PreparationMethod",
            "doc" : "In 100K, preparation Method of sample\n    NOTE: In GMS, this field is deprecated in favour of StorageMedium and Method",
            "symbols" : [ "ASPIRATE", "CD128_SORTED_CELLS", "CD138_SORTED_CELLS", "EDTA", "FF", "FFPE", "LI_HEP", "ORAGENE" ]
          } ],
          "doc" : "The preparation method of the sample\n        NOTE: Deprecated in GMS in replace of Method and storageMedium record"
        }, {
          "name" : "tissueSource",
          "type" : [ "null", "string" ],
          "doc" : "The tissue source of the sample.\n        NOTE: DEPRECATED IN GMS in replace of method record"
        }, {
          "name" : "product",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "Product",
            "symbols" : [ "DNA", "RNA" ]
          } ],
          "doc" : "Product of the sample"
        }, {
          "name" : "sampleMorphologies",
          "type" : [ "null", {
            "type" : "array",
            "items" : {
              "type" : "record",
              "name" : "Morphology",
              "fields" : [ {
                "name" : "id",
                "type" : [ "null", "string" ],
                "doc" : "The ontology term id or accession in OBO format ${ONTOLOGY_ID}:${TERM_ID} (http://www.obofoundry.org/id-policy.html)"
              }, {
                "name" : "name",
                "type" : [ "null", "string" ],
                "doc" : "The ontology term name"
              }, {
                "name" : "value",
                "type" : [ "null", "string" ],
                "doc" : "Optional value for the ontology term, the type of the value is not checked\n        (i.e.: we could set the pvalue term to \"significant\" or to \"0.0001\")"
              }, {
                "name" : "version",
                "type" : [ "null", "string" ],
                "doc" : "Ontology version"
              } ]
            }
          } ],
          "doc" : "Morphology according to the sample taken"
        }, {
          "name" : "sampleTopographies",
          "type" : [ "null", {
            "type" : "array",
            "items" : {
              "type" : "record",
              "name" : "Topography",
              "fields" : [ {
                "name" : "id",
                "type" : [ "null", "string" ],
                "doc" : "The ontology term id or accession in OBO format ${ONTOLOGY_ID}:${TERM_ID} (http://www.obofoundry.org/id-policy.html)"
              }, {
                "name" : "name",
                "type" : [ "null", "string" ],
                "doc" : "The ontology term name"
              }, {
                "name" : "value",
                "type" : [ "null", "string" ],
                "doc" : "Optional value for the ontology term, the type of the value is not checked\n        (i.e.: we could set the pvalue term to \"significant\" or to \"0.0001\")"
              }, {
                "name" : "version",
                "type" : [ "null", "string" ],
                "doc" : "Ontology version"
              } ]
            }
          } ],
          "doc" : "Topography according to the sample taken"
        }, {
          "name" : "sampleUid",
          "type" : [ "null", "string" ],
          "doc" : "In GMS, this is the GUID of the sample"
        }, {
          "name" : "participantId",
          "type" : [ "null", "string" ],
          "doc" : "Participant Id of the sample"
        }, {
          "name" : "participantUid",
          "type" : [ "null", "string" ],
          "doc" : "Participant UId of the sample"
        }, {
          "name" : "maskedPid",
          "type" : [ "null", "string" ],
          "doc" : "In GMS, this is the maskedPID"
        }, {
          "name" : "method",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "Method",
            "doc" : "In GMS, Method is defined as how the sample was taken directly from the patient",
            "symbols" : [ "ASPIRATE", "BIOPSY", "NOT_APPLICABLE", "RESECTION", "SORTED_OTHER", "UNKNOWN", "UNSORTED", "CD138_SORTED" ]
          } ],
          "doc" : "In GMS, this is how the sample was extracted from the participant"
        }, {
          "name" : "storageMedium",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "StorageMedium",
            "doc" : "In GMS, storage medium of sample",
            "symbols" : [ "EDTA", "FF", "LI_HEP", "ORAGENE", "FFPE" ]
          } ],
          "doc" : "In GMS, this is what solvent/medium the sample was stored in"
        }, {
          "name" : "sampleType",
          "type" : [ "null", "string" ],
          "doc" : "In GMS, this is the sampleType as entered by the clinician in TOMs"
        }, {
          "name" : "sampleState",
          "type" : [ "null", "string" ],
          "doc" : "In GMS, this is the sampleState as entered by the clinician in TOMs"
        } ]
      }
    } ],
    "doc" : "List of all somatic samples applicable to this test"
  }, {
    "name" : "germlineSamples",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "GermlineSample",
        "doc" : "A germline sample",
        "fields" : [ {
          "name" : "sampleId",
          "type" : "string",
          "doc" : "Sample identifier (e.g, LP00012645_5GH))"
        }, {
          "name" : "labSampleId",
          "type" : "string",
          "doc" : "Lab sample identifier"
        }, {
          "name" : "LDPCode",
          "type" : [ "null", "string" ],
          "doc" : "LDP Code (Local Delivery Partner)"
        }, {
          "name" : "source",
          "type" : [ "null", "SampleSource" ],
          "doc" : "Source of the sample"
        }, {
          "name" : "product",
          "type" : [ "null", "Product" ],
          "doc" : "Product of the sample"
        }, {
          "name" : "preparationMethod",
          "type" : [ "null", "PreparationMethod" ],
          "doc" : "Preparation method\n        NOTE: In GMS, this has been deprecated in favour of Method and storageMedium"
        }, {
          "name" : "programmePhase",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "ProgrammePhase",
            "symbols" : [ "CRUK", "OXFORD", "CLL", "IIP", "MAIN", "EXPT" ]
          } ],
          "doc" : "Genomics England programme phase"
        }, {
          "name" : "clinicalSampleDateTime",
          "type" : [ "null", "string" ],
          "doc" : "The time when the sample was received. In the format YYYY-MM-DDTHH:MM:SS+0000"
        }, {
          "name" : "participantId",
          "type" : [ "null", "string" ]
        }, {
          "name" : "participantUid",
          "type" : [ "null", "string" ],
          "doc" : "Participant UId of the sample"
        }, {
          "name" : "sampleUid",
          "type" : [ "null", "string" ]
        }, {
          "name" : "maskedPid",
          "type" : [ "null", "string" ]
        }, {
          "name" : "method",
          "type" : [ "null", "Method" ],
          "doc" : "In GMS, this is how the sample was extracted from the participant"
        }, {
          "name" : "storageMedium",
          "type" : [ "null", "StorageMedium" ],
          "doc" : "In GMS, this is what solvent/medium the sample was stored in"
        }, {
          "name" : "sampleType",
          "type" : [ "null", "string" ],
          "doc" : "In GMS, this is the sampleType as entered by the clinician in TOMs"
        }, {
          "name" : "sampleState",
          "type" : [ "null", "string" ],
          "doc" : "In GMS, this is the sampleState as entered by the clinician in TOMs"
        }, {
          "name" : "sampleCollectionOperationsQuestions",
          "type" : [ "null", {
            "type" : "record",
            "name" : "SampleCollectionOperationsQuestions",
            "doc" : "Operations questions on sample collection for Newborns BaMMs",
            "fields" : [ {
              "name" : "easeOfSampleCollection",
              "type" : [ "null", {
                "type" : "enum",
                "name" : "EaseOfSampleCollection",
                "doc" : "Ease of Sample Collection",
                "symbols" : [ "EASY", "DIFFICULT", "NOT_APPLICABLE" ]
              } ],
              "doc" : "Ease of successful sample collection"
            }, {
              "name" : "minutes",
              "type" : [ "null", "int" ],
              "doc" : "Time taken to collect the sample (from opening collection kit to completion) in minutes"
            } ]
          } ],
          "doc" : "In Newborns BaMMs, this is operational sample collection questions"
        } ]
      }
    } ],
    "doc" : "List of all germline samples aplicable to this test"
  }, {
    "name" : "failedCollectionSamples",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "FailedCollectionSample",
        "doc" : "A sample where there was a failure in collection.\n    Note: since the sample failed at collection, there is no sample ID.",
        "fields" : [ {
          "name" : "reasonsForCollectionFailure",
          "type" : [ "null", {
            "type" : "array",
            "items" : "string"
          } ],
          "doc" : "List of reasons for failure in collection"
        }, {
          "name" : "source",
          "type" : [ "null", "SampleSource" ],
          "doc" : "Source of the sample"
        }, {
          "name" : "product",
          "type" : [ "null", "Product" ],
          "doc" : "Product of the sample"
        }, {
          "name" : "participantId",
          "type" : [ "null", "string" ],
          "doc" : "Participant Id of the sample"
        }, {
          "name" : "participantUid",
          "type" : [ "null", "string" ],
          "doc" : "Participant UId of the sample"
        }, {
          "name" : "maskedPid",
          "type" : [ "null", "string" ],
          "doc" : "Masked PID of the sample, if used"
        }, {
          "name" : "sampleType",
          "type" : [ "null", "string" ],
          "doc" : "This is the unmodified sampleType value (e.g. cord_blood) entered by the clinician"
        } ]
      }
    } ],
    "doc" : "List of all samples that were failed to be collected and their details."
  }, {
    "name" : "analysisPanels",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "AnalysisPanel",
        "doc" : "An analysis panel",
        "fields" : [ {
          "name" : "specificDisease",
          "type" : "string",
          "doc" : "The specific disease that a panel tests"
        }, {
          "name" : "panelName",
          "type" : "string",
          "doc" : "The name of the panel"
        }, {
          "name" : "panelId",
          "type" : [ "null", "string" ],
          "doc" : "Id of the panel"
        }, {
          "name" : "panelVersion",
          "type" : [ "null", "string" ],
          "doc" : "The version of the panel"
        }, {
          "name" : "reviewOutcome",
          "type" : [ "null", "string" ],
          "doc" : "Deprecated"
        }, {
          "name" : "multipleGeneticOrigins",
          "type" : [ "null", "string" ],
          "doc" : "Deprecated"
        } ]
      }
    } ],
    "doc" : "List of Analysis panels"
  }, {
    "name" : "interpreter",
    "type" : {
      "type" : "record",
      "name" : "OrganisationNgis",
      "fields" : [ {
        "name" : "organisationId",
        "type" : "string",
        "doc" : "Organisation Id"
      }, {
        "name" : "organisationCode",
        "type" : "string",
        "doc" : "Ods code"
      }, {
        "name" : "organisationName",
        "type" : "string",
        "doc" : "Organisation Name"
      }, {
        "name" : "organisationNationalGroupingId",
        "type" : "string",
        "doc" : "National Grouping (GLH) Id"
      }, {
        "name" : "organisationNationalGroupingName",
        "type" : "string",
        "doc" : "National Grouping (GLH) Name"
      } ]
    },
    "doc" : "Organisation assigned for the interpretation of this test"
  }, {
    "name" : "processingLab",
    "type" : "OrganisationNgis",
    "doc" : "Organisation assigned for the processing of the test"
  }, {
    "name" : "priority",
    "type" : {
      "type" : "enum",
      "name" : "Priority",
      "doc" : "Transformed from TOMs from routine=medium, and urgent=high",
      "symbols" : [ "low", "routine", "urgent" ]
    },
    "doc" : "Priority"
  }, {
    "name" : "pipelineStartDate",
    "type" : [ "null", "Date" ],
    "doc" : "Date of ordering. NOTE: this field is not required from upstream\n        and will be generated by Bioinformatics when all sample data and all\n        required clinical data is received for the first time"
  }, {
    "name" : "diseasePenetrances",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "DiseasePenetrance",
        "doc" : "A disease penetrance definition",
        "fields" : [ {
          "name" : "specificDisease",
          "type" : "string",
          "doc" : "The disease to which the penetrance applies"
        }, {
          "name" : "penetrance",
          "type" : {
            "type" : "enum",
            "name" : "Penetrance",
            "doc" : "Penetrance assumed in the analysis",
            "symbols" : [ "complete", "incomplete" ]
          },
          "doc" : "The penetrance"
        } ]
      }
    } ],
    "doc" : "Disease Penetrance applied for that referralTest"
  }, {
    "name" : "matchedSamples",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "MatchedSamples",
        "doc" : "This defines a pair of germline and tumor, this pair should/must be analyzed together",
        "fields" : [ {
          "name" : "germlineSampleId",
          "type" : [ "null", "string" ],
          "doc" : "Sample identifier (e.g, LP00012645_5GH)) for the germline"
        }, {
          "name" : "tumourSampleId",
          "type" : [ "null", "string" ],
          "doc" : "Sample identifier (e.g, LP00012643_7JS)) for the tumor"
        } ]
      }
    } ],
    "doc" : "List of matched samples (i.e.: pairs tumour-germline)"
  } ]
}
