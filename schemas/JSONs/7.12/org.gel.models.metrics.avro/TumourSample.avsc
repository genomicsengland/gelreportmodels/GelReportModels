{
  "type" : "record",
  "name" : "TumourSample",
  "namespace" : "org.gel.models.participant.avro",
  "doc" : "A tumour sample",
  "fields" : [ {
    "name" : "sampleId",
    "type" : "string",
    "doc" : "Sample identifier (e.g, LP00012645_5GH))"
  }, {
    "name" : "labSampleId",
    "type" : "string",
    "doc" : "Lab sample identifier"
  }, {
    "name" : "LDPCode",
    "type" : [ "null", "string" ],
    "doc" : "LDP Code (Local Delivery Partner)"
  }, {
    "name" : "tumourId",
    "type" : [ "null", "string" ],
    "doc" : "This is the ID of the tumour from which this tumour sample was taken from"
  }, {
    "name" : "programmePhase",
    "type" : [ "null", "string" ],
    "doc" : "Genomics England programme phase"
  }, {
    "name" : "diseaseType",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "diseaseType",
      "symbols" : [ "ADULT_GLIOMA", "BLADDER", "BREAST", "CARCINOMA_OF_UNKNOWN_PRIMARY", "CHILDHOOD", "COLORECTAL", "ENDOCRINE", "ENDOMETRIAL_CARCINOMA", "HAEMONC", "HEPATOPANCREATOBILIARY", "LUNG", "MALIGNANT_MELANOMA", "NASOPHARYNGEAL", "ORAL_OROPHARYNGEAL", "OVARIAN", "PROSTATE", "RENAL", "SARCOMA", "SINONASAL", "TESTICULAR_GERM_CELL_TUMOURS", "UPPER_GASTROINTESTINAL", "OTHER", "NON_HODGKINS_B_CELL_LYMPHOMA_LOW_MOD_GRADE", "CLASSICAL_HODGKINS", "NODULAR_LYMPHOCYTE_PREDOMINANT_HODGKINS", "T_CELL_LYMPHOMA" ]
    } ],
    "doc" : "Disease type.\n        NOTE: Deprecated in GMS"
  }, {
    "name" : "diseaseSubType",
    "type" : [ "null", "string" ],
    "doc" : "Disease subtype.\n        NOTE: Deprecated in GMS"
  }, {
    "name" : "haematologicalCancer",
    "type" : [ "null", "boolean" ],
    "doc" : "True or false if this sample is of type: Haematological Cancer"
  }, {
    "name" : "haematologicalCancerLineage",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "HaematologicalCancerLineage",
      "symbols" : [ "MYELOID", "LYMPHOID", "UNKNOWN" ]
    } ],
    "doc" : "This is the Haematological cancer lineage of the tumourSample if this sample is from a haematological cancer"
  }, {
    "name" : "clinicalSampleDateTime",
    "type" : [ "null", "string" ],
    "doc" : "The time when the sample was received. In the format YYYY-MM-DDTHH:MM:SS+0000"
  }, {
    "name" : "tumourType",
    "type" : [ "null", "string" ],
    "doc" : "Tumor type.\n        NOTE: Deprecated in GMS in tumourSample but available in tumour record"
  }, {
    "name" : "tumourContent",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "TumourContent",
      "symbols" : [ "High", "Medium", "Low" ]
    } ],
    "doc" : "This is the tumour content"
  }, {
    "name" : "tumourContentPercentage",
    "type" : [ "null", "float" ],
    "doc" : "This is the tumour content percentage"
  }, {
    "name" : "source",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "SampleSource",
      "doc" : "The source of the sample\n    NOTE: IN GMS, BONE_MARROW_ASPIRATE_TUMOUR_CELLS and BONE_MARROW_ASPIRATE_TUMOUR_SORTED_CELLS are deprecated as they have been separated into their respective biotypes",
      "symbols" : [ "AMNIOTIC_FLUID", "BLOOD", "BLOOD_CAPILLARY_HEEL_PRICK", "BLOOD_CLOT", "BLOOD_CORD", "BLOOD_DRIED_BLOOD_SPOT", "BONE_MARROW", "BONE_MARROW_ASPIRATE_TUMOUR_CELLS", "BONE_MARROW_ASPIRATE_TUMOUR_SORTED_CELLS", "BUCCAL_SPONGE", "BUCCAL_SWAB", "BUFFY_COAT", "CHORIONIC_VILLUS_SAMPLE", "FIBROBLAST", "FLUID", "FRESH_TISSUE_IN_CULTURE_MEDIUM", "OTHER", "SALIVA", "TISSUE", "TUMOUR", "URINE" ]
    } ],
    "doc" : "Source of the sample"
  }, {
    "name" : "preparationMethod",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "PreparationMethod",
      "doc" : "In 100K, preparation Method of sample\n    NOTE: In GMS, this field is deprecated in favour of StorageMedium and Method",
      "symbols" : [ "ASPIRATE", "CD128_SORTED_CELLS", "CD138_SORTED_CELLS", "EDTA", "FF", "FFPE", "LI_HEP", "ORAGENE" ]
    } ],
    "doc" : "The preparation method of the sample\n        NOTE: Deprecated in GMS in replace of Method and storageMedium record"
  }, {
    "name" : "tissueSource",
    "type" : [ "null", "string" ],
    "doc" : "The tissue source of the sample.\n        NOTE: DEPRECATED IN GMS in replace of method record"
  }, {
    "name" : "product",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "Product",
      "symbols" : [ "DNA", "RNA" ]
    } ],
    "doc" : "Product of the sample"
  }, {
    "name" : "sampleMorphologies",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "Morphology",
        "fields" : [ {
          "name" : "id",
          "type" : [ "null", "string" ],
          "doc" : "The ontology term id or accession in OBO format ${ONTOLOGY_ID}:${TERM_ID} (http://www.obofoundry.org/id-policy.html)"
        }, {
          "name" : "name",
          "type" : [ "null", "string" ],
          "doc" : "The ontology term name"
        }, {
          "name" : "value",
          "type" : [ "null", "string" ],
          "doc" : "Optional value for the ontology term, the type of the value is not checked\n        (i.e.: we could set the pvalue term to \"significant\" or to \"0.0001\")"
        }, {
          "name" : "version",
          "type" : [ "null", "string" ],
          "doc" : "Ontology version"
        } ]
      }
    } ],
    "doc" : "Morphology according to the sample taken"
  }, {
    "name" : "sampleTopographies",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "Topography",
        "fields" : [ {
          "name" : "id",
          "type" : [ "null", "string" ],
          "doc" : "The ontology term id or accession in OBO format ${ONTOLOGY_ID}:${TERM_ID} (http://www.obofoundry.org/id-policy.html)"
        }, {
          "name" : "name",
          "type" : [ "null", "string" ],
          "doc" : "The ontology term name"
        }, {
          "name" : "value",
          "type" : [ "null", "string" ],
          "doc" : "Optional value for the ontology term, the type of the value is not checked\n        (i.e.: we could set the pvalue term to \"significant\" or to \"0.0001\")"
        }, {
          "name" : "version",
          "type" : [ "null", "string" ],
          "doc" : "Ontology version"
        } ]
      }
    } ],
    "doc" : "Topography according to the sample taken"
  }, {
    "name" : "sampleUid",
    "type" : [ "null", "string" ],
    "doc" : "In GMS, this is the GUID of the sample"
  }, {
    "name" : "participantId",
    "type" : [ "null", "string" ],
    "doc" : "Participant Id of the sample"
  }, {
    "name" : "participantUid",
    "type" : [ "null", "string" ],
    "doc" : "Participant UId of the sample"
  }, {
    "name" : "maskedPid",
    "type" : [ "null", "string" ],
    "doc" : "In GMS, this is the maskedPID"
  }, {
    "name" : "method",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "Method",
      "doc" : "In GMS, Method is defined as how the sample was taken directly from the patient",
      "symbols" : [ "ASPIRATE", "BIOPSY", "NOT_APPLICABLE", "RESECTION", "SORTED_OTHER", "UNKNOWN", "UNSORTED", "CD138_SORTED" ]
    } ],
    "doc" : "In GMS, this is how the sample was extracted from the participant"
  }, {
    "name" : "storageMedium",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "StorageMedium",
      "doc" : "In GMS, storage medium of sample",
      "symbols" : [ "EDTA", "FF", "LI_HEP", "ORAGENE", "FFPE" ]
    } ],
    "doc" : "In GMS, this is what solvent/medium the sample was stored in"
  }, {
    "name" : "sampleType",
    "type" : [ "null", "string" ],
    "doc" : "In GMS, this is the sampleType as entered by the clinician in TOMs"
  }, {
    "name" : "sampleState",
    "type" : [ "null", "string" ],
    "doc" : "In GMS, this is the sampleState as entered by the clinician in TOMs"
  } ]
}
