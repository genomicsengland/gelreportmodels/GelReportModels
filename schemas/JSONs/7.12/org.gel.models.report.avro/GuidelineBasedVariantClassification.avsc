{
  "type" : "record",
  "name" : "GuidelineBasedVariantClassification",
  "namespace" : "org.gel.models.report.avro",
  "doc" : "Variant classification based on guidlines, AMP and ACMG are supported",
  "fields" : [ {
    "name" : "acmgVariantClassification",
    "type" : [ "null", {
      "type" : "record",
      "name" : "AcmgVariantClassification",
      "doc" : "Full record for the ACMG variant clasiffication, including all selected evidences and the final classification.",
      "fields" : [ {
        "name" : "acmgEvidences",
        "type" : {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "AcmgEvidence",
            "doc" : "Represents a single ACMG criterion (PVS1, BA1, PM1... etc) used in the classification of a variant,\n    along with the strength applied at and evidence supporting it.",
            "fields" : [ {
              "name" : "category",
              "type" : {
                "type" : "enum",
                "name" : "AcmgEvidenceCategory",
                "doc" : "Each ACMG criterion is classified in one of these categories",
                "symbols" : [ "population_data", "computational_and_predictive_data", "functional_data", "segregation_data", "de_novo_data", "allelic_data", "other_database", "other_data" ]
              },
              "doc" : "Evidence category as defined in ACMG guidelines"
            }, {
              "name" : "type",
              "type" : {
                "type" : "enum",
                "name" : "AcmgEvidenceType",
                "doc" : "Each ACMG cirterion will be classifed as benign or pathogenic",
                "symbols" : [ "benign", "pathogenic" ]
              },
              "doc" : "Evidence type: benign or pathogenic"
            }, {
              "name" : "weight",
              "type" : {
                "type" : "enum",
                "name" : "AcmgEvidenceWeight",
                "doc" : "Each ACMG criterion is weighted using the following terms:\n\n* `stand_alone`: `A`, stand-alone applied for benign variant critieria `(BA1)`\n* `supporting`: `P`, supporting applied for benign variant critieria `(BP1-6)` and pathogenic variant criteria `(PP1-5)`\n* `moderate`: `M`, moderate applied for pathogenic variant critieria (PM1-6)\n* `strong`: `S`, strong applied for pathogenic variant critieria (PS1-4)\n* `very_strong`: `S`, Very Stong applied for pathogenic variant critieria (PVS1)",
                "symbols" : [ "stand_alone", "supporting", "moderate", "strong", "very_strong" ]
              },
              "doc" : "Default strength for criterion as defined in Table 3 of ACMG guidelines (Richards et al 2015). e.g. PM2 would be \"moderate\""
            }, {
              "name" : "modifier",
              "type" : "int",
              "doc" : "The number suffix at the end of the ACMG criteria code e.g PM2 would be 2"
            }, {
              "name" : "activationStrength",
              "type" : {
                "type" : "enum",
                "name" : "ActivationStrength",
                "doc" : "Activation Strength enumeration:\n* `strong`\n* `moderate`\n* `supporting`\n* `very_strong`\n* `stand_alone`",
                "symbols" : [ "strong", "moderate", "supporting", "very_strong", "stand_alone" ]
              },
              "doc" : "The strength this criterion has been used at in this interpretation. e.g. if PM2 was only used at \"supporting\" rather than \"moderate\", the activation strength would be \"supporting\""
            }, {
              "name" : "description",
              "type" : [ "null", "string" ],
              "doc" : "The description of the evidence as described in ACMG guidelines e.g. for PM2 the description would be \"Absent from controls (or at extremely low frequency if recessive) in Exome Sequencing Project, 1000 Genomes Project, or Exome Aggregation Consortium\""
            }, {
              "name" : "comments",
              "type" : [ "null", {
                "type" : "array",
                "items" : {
                  "type" : "record",
                  "name" : "UserComment",
                  "fields" : [ {
                    "name" : "comment",
                    "type" : "string",
                    "doc" : "Comment text"
                  }, {
                    "name" : "user",
                    "type" : [ "null", {
                      "type" : "record",
                      "name" : "User",
                      "fields" : [ {
                        "name" : "userid",
                        "type" : [ "null", "string" ],
                        "doc" : "Azure Active Directory immutable user OID"
                      }, {
                        "name" : "email",
                        "type" : "string",
                        "doc" : "User email address"
                      }, {
                        "name" : "username",
                        "type" : "string",
                        "doc" : "Username"
                      }, {
                        "name" : "role",
                        "type" : [ "null", "string" ]
                      }, {
                        "name" : "groups",
                        "type" : [ "null", {
                          "type" : "array",
                          "items" : "string"
                        } ]
                      } ]
                    } ],
                    "doc" : "User who created comment"
                  }, {
                    "name" : "timestamp",
                    "type" : [ "null", "string" ],
                    "doc" : "Date and time comment was created (ISO 8601 datetime with seconds and timezone e.g. 2020-11-23T15:52:36+00:00)"
                  } ]
                }
              } ],
              "doc" : "User comments attached to this ACMG criteria in this case"
            }, {
              "name" : "flaggedPublications",
              "type" : [ "null", {
                "type" : "array",
                "items" : {
                  "type" : "record",
                  "name" : "Publication",
                  "doc" : "For each publication, either a PMID or DOI should be included, preferably both if available",
                  "fields" : [ {
                    "name" : "pmid",
                    "type" : [ "null", "string" ],
                    "doc" : "PubMed ID"
                  }, {
                    "name" : "doi",
                    "type" : [ "null", "string" ],
                    "doc" : "Digital Object Identifier (DOI) e.g. 10.1056/NEJMra0802968"
                  }, {
                    "name" : "comments",
                    "type" : [ "null", {
                      "type" : "array",
                      "items" : "UserComment"
                    } ],
                    "doc" : "User comments left on this article as part of this variant interpretation"
                  } ]
                }
              } ],
              "doc" : "Publications flagged by the user as relevant to this ACMG evidence"
            } ]
          }
        },
        "doc" : "Details of ACMG criteria used to score this variant"
      }, {
        "name" : "clinicalSignificance",
        "type" : {
          "type" : "enum",
          "name" : "ClinicalSignificance",
          "symbols" : [ "benign", "likely_benign", "likely_pathogenic", "pathogenic", "uncertain_significance", "excluded" ]
        },
        "doc" : "Final classification selected by user"
      }, {
        "name" : "calculatedClinicalSignificance",
        "type" : [ "null", "ClinicalSignificance" ],
        "doc" : "Classification computed from ACMG scores"
      }, {
        "name" : "assessment",
        "type" : [ "null", "string" ]
      }, {
        "name" : "noAcmgEvidence",
        "type" : [ "null", {
          "type" : "array",
          "items" : "AcmgEvidenceCategory"
        } ],
        "doc" : "ACMG evidence categories for which the user has indicated there is no evidence available"
      } ]
    } ],
    "doc" : "Variant classification using ACMG framework"
  }, {
    "name" : "ampVariantClassification",
    "type" : [ "null", {
      "type" : "record",
      "name" : "AmpVariantClassification",
      "doc" : "Full Variant classification acording to AMP guideline, including all supporting evidences and the final\n    assessment",
      "fields" : [ {
        "name" : "ampEvidences",
        "type" : {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "AmpEvidence",
            "doc" : "Evidences as defined in AMP guidelines, they are composed by a evidence type (first column in the evidence table of\n    the guidlines) and a assessment of the evicence, this last one will define the streght of the evidence, supporting\n    the variant to be classified as TierI-IV",
            "fields" : [ {
              "name" : "type",
              "type" : {
                "type" : "enum",
                "name" : "AmpEvidenceType",
                "doc" : "Type of evidence in the AMP guideline",
                "symbols" : [ "mutation_type", "therapies", "variant_frequencies", "potential_germline", "population_database_presence", "germline_database_presence", "somatic_database_presence", "impact_predictive_software", "pathway_involvement", "publications" ]
              },
              "doc" : "AMP evidence type according to Guidlines, i.e germline_database_presence"
            }, {
              "name" : "evidenceAssessment",
              "type" : "string",
              "doc" : "Assessment for AMP evidence, i.e Present in ClinVar"
            } ]
          }
        },
        "doc" : "List of AMP evidences"
      }, {
        "name" : "ampTier",
        "type" : {
          "type" : "enum",
          "name" : "AmpTier",
          "doc" : "AMP tier:\n* `TierI`: Variants of Strong Clinical Significance\n* `TierII`: Variants of Potential Clinical Significance\n* `TierIII`: Variants of Unknown Clinical Significance\n* `TierIV`: Benign or Likely Benign Variants",
          "symbols" : [ "tierI", "tierII", "tierIII", "tierIV" ]
        },
        "doc" : "Final Clasification taken in account the evidences"
      }, {
        "name" : "ampClincialOrExperimentalEvidence",
        "type" : [ "null", {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "AmpClincialOrExperimentalEvidence",
            "doc" : "Amp Clinical or Experimental Evidence, the level will define the overal clasification of the variant together with\n    the tiering.",
            "fields" : [ {
              "name" : "category",
              "type" : {
                "type" : "enum",
                "name" : "AmpClinicalOrExperimentalEvidenceCategory",
                "doc" : "Categories of Clinical and/or Experimental Evidence as defined in AMP guidelines",
                "symbols" : [ "therapeutic", "diagnosis", "prognosis" ]
              },
              "doc" : "As denined in AMP guidelines: therapeutic, diagnosis or prognosis"
            }, {
              "name" : "level",
              "type" : {
                "type" : "enum",
                "name" : "AmpClinicalOrExperimentalEvidenceLevel",
                "doc" : "Levels for categories of Clinical and/or Experimental Evidence as defined in AMP guidelines",
                "symbols" : [ "levelA", "levelB", "levelC", "levelD" ]
              },
              "doc" : "As denined in AMP guidelines: levelA, levelB, levelC, levelD"
            }, {
              "name" : "description",
              "type" : [ "null", "string" ],
              "doc" : "Description of the evidence"
            } ]
          }
        } ],
        "doc" : "Clinical or Experimental evicence"
      }, {
        "name" : "assessment",
        "type" : [ "null", "string" ],
        "doc" : "Final Assessment"
      } ]
    } ],
    "doc" : "Variant classification using AMP framework"
  } ]
}
