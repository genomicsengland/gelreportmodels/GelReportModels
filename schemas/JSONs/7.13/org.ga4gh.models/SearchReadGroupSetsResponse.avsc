{
  "type" : "record",
  "name" : "SearchReadGroupSetsResponse",
  "namespace" : "org.ga4gh.methods",
  "doc" : "This is the response from `POST /readgroupsets/search` expressed as JSON.",
  "fields" : [ {
    "name" : "readGroupSets",
    "type" : {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "ReadGroupSet",
        "namespace" : "org.ga4gh.models",
        "fields" : [ {
          "name" : "id",
          "type" : "string",
          "doc" : "The read group set ID."
        }, {
          "name" : "datasetId",
          "type" : [ "null", "string" ],
          "doc" : "The ID of the dataset this read group set belongs to.",
          "default" : null
        }, {
          "name" : "name",
          "type" : [ "null", "string" ],
          "doc" : "The read group set name.",
          "default" : null
        }, {
          "name" : "stats",
          "type" : [ "null", {
            "type" : "record",
            "name" : "ReadStats",
            "fields" : [ {
              "name" : "alignedReadCount",
              "type" : [ "null", "long" ],
              "doc" : "The number of aligned reads.",
              "default" : null
            }, {
              "name" : "unalignedReadCount",
              "type" : [ "null", "long" ],
              "doc" : "The number of unaligned reads.",
              "default" : null
            }, {
              "name" : "baseCount",
              "type" : [ "null", "long" ],
              "doc" : "The total number of bases.\n  This is equivalent to the sum of `alignedSequence.length` for all reads.",
              "default" : null
            } ]
          } ],
          "doc" : "Statistical data on reads in this read group set.",
          "default" : null
        }, {
          "name" : "readGroups",
          "type" : {
            "type" : "array",
            "items" : {
              "type" : "record",
              "name" : "ReadGroup",
              "fields" : [ {
                "name" : "id",
                "type" : "string",
                "doc" : "The read group ID."
              }, {
                "name" : "datasetId",
                "type" : [ "null", "string" ],
                "doc" : "The ID of the dataset this read group belongs to.",
                "default" : null
              }, {
                "name" : "name",
                "type" : [ "null", "string" ],
                "doc" : "The read group name.",
                "default" : null
              }, {
                "name" : "description",
                "type" : [ "null", "string" ],
                "doc" : "The read group description.",
                "default" : null
              }, {
                "name" : "sampleId",
                "type" : [ "null", "string" ],
                "doc" : "The sample this read group's data was generated from."
              }, {
                "name" : "experiment",
                "type" : [ "null", {
                  "type" : "record",
                  "name" : "Experiment",
                  "doc" : "An experimental preparation of a `Sample`.",
                  "fields" : [ {
                    "name" : "id",
                    "type" : "string",
                    "doc" : "The experiment UUID. This is globally unique."
                  }, {
                    "name" : "name",
                    "type" : [ "null", "string" ],
                    "doc" : "The name of the experiment.",
                    "default" : null
                  }, {
                    "name" : "description",
                    "type" : [ "null", "string" ],
                    "doc" : "A description of the experiment.",
                    "default" : null
                  }, {
                    "name" : "recordCreateTime",
                    "type" : "string",
                    "doc" : "The time at which this record was created. \n  Format: ISO 8601, YYYY-MM-DDTHH:MM:SS.SSS (e.g. 2015-02-10T00:03:42.123Z)"
                  }, {
                    "name" : "recordUpdateTime",
                    "type" : "string",
                    "doc" : "The time at which this record was last updated.\n  Format: ISO 8601, YYYY-MM-DDTHH:MM:SS.SSS (e.g. 2015-02-10T00:03:42.123Z)"
                  }, {
                    "name" : "runTime",
                    "type" : [ "null", "string" ],
                    "doc" : "The time at which this experiment was performed.\n  Granularity here is variabel (e.g. date only).\n  Format: ISO 8601, YYYY-MM-DDTHH:MM:SS (e.g. 2015-02-10T00:03:42)",
                    "default" : null
                  }, {
                    "name" : "molecule",
                    "type" : [ "null", "string" ],
                    "doc" : "The molecule examined in this experiment. (e.g. genomics DNA, total RNA)",
                    "default" : null
                  }, {
                    "name" : "strategy",
                    "type" : [ "null", "string" ],
                    "doc" : "The experiment technique or strategy applied to the sample.\n  (e.g. whole genome sequencing, RNA-seq, RIP-seq)",
                    "default" : null
                  }, {
                    "name" : "selection",
                    "type" : [ "null", "string" ],
                    "doc" : "The method used to enrich the target. (e.g. immunoprecipitation, size\n  fractionation, MNase digestion)",
                    "default" : null
                  }, {
                    "name" : "library",
                    "type" : [ "null", "string" ],
                    "doc" : "The name of the library used as part of this experiment.",
                    "default" : null
                  }, {
                    "name" : "libraryLayout",
                    "type" : [ "null", "string" ],
                    "doc" : "The configuration of sequenced reads. (e.g. Single or Paired)",
                    "default" : null
                  }, {
                    "name" : "instrumentModel",
                    "type" : [ "null", "string" ],
                    "doc" : "The instrument model used as part of this experiment.\n    This maps to sequencing technology in BAM."
                  }, {
                    "name" : "instrumentDataFile",
                    "type" : [ "null", "string" ],
                    "doc" : "The data file generated by the instrument.\n  TODO: This isn't actually a file is it?\n  Should this be `instrumentData` instead?",
                    "default" : null
                  }, {
                    "name" : "sequencingCenter",
                    "type" : [ "null", "string" ],
                    "doc" : "The sequencing center used as part of this experiment."
                  }, {
                    "name" : "platformUnit",
                    "type" : [ "null", "string" ],
                    "doc" : "The platform unit used as part of this experiment. This is a flowcell-barcode\n  or slide unique identifier.",
                    "default" : null
                  }, {
                    "name" : "info",
                    "type" : {
                      "type" : "map",
                      "values" : {
                        "type" : "array",
                        "items" : "string"
                      }
                    },
                    "doc" : "A map of additional experiment information.",
                    "default" : { }
                  } ]
                } ],
                "doc" : "The experiment used to generate this read group."
              }, {
                "name" : "predictedInsertSize",
                "type" : [ "null", "int" ],
                "doc" : "The predicted insert size of this read group.",
                "default" : null
              }, {
                "name" : "created",
                "type" : [ "null", "long" ],
                "doc" : "The time at which this read group was created in milliseconds from the epoch.",
                "default" : null
              }, {
                "name" : "updated",
                "type" : [ "null", "long" ],
                "doc" : "The time at which this read group was last updated in milliseconds\n  from the epoch.",
                "default" : null
              }, {
                "name" : "stats",
                "type" : [ "null", "ReadStats" ],
                "doc" : "Statistical data on reads in this read group.",
                "default" : null
              }, {
                "name" : "programs",
                "type" : {
                  "type" : "array",
                  "items" : {
                    "type" : "record",
                    "name" : "Program",
                    "fields" : [ {
                      "name" : "commandLine",
                      "type" : [ "null", "string" ],
                      "doc" : "The command line used to run this program.",
                      "default" : null
                    }, {
                      "name" : "id",
                      "type" : [ "null", "string" ],
                      "doc" : "The user specified ID of the program.",
                      "default" : null
                    }, {
                      "name" : "name",
                      "type" : [ "null", "string" ],
                      "doc" : "The name of the program.",
                      "default" : null
                    }, {
                      "name" : "prevProgramId",
                      "type" : [ "null", "string" ],
                      "doc" : "The ID of the program run before this one.",
                      "default" : null
                    }, {
                      "name" : "version",
                      "type" : [ "null", "string" ],
                      "doc" : "The version of the program run.",
                      "default" : null
                    } ]
                  }
                },
                "doc" : "The programs used to generate this read group.",
                "default" : [ ]
              }, {
                "name" : "referenceSetId",
                "type" : [ "null", "string" ],
                "doc" : "The reference set the reads in this read group are aligned to.\n  Required if there are any read alignments.",
                "default" : null
              }, {
                "name" : "info",
                "type" : {
                  "type" : "map",
                  "values" : {
                    "type" : "array",
                    "items" : "string"
                  }
                },
                "doc" : "A map of additional read group information.",
                "default" : { }
              } ]
            }
          },
          "doc" : "The read groups in this set.",
          "default" : [ ]
        } ]
      }
    },
    "doc" : "The list of matching read group sets.",
    "default" : [ ]
  }, {
    "name" : "nextPageToken",
    "type" : [ "null", "string" ],
    "doc" : "The continuation token, which is used to page through large result sets.\n  Provide this value in a subsequent request to return the next page of\n  results. This field will be empty if there aren't any additional results.",
    "default" : null
  } ]
}
