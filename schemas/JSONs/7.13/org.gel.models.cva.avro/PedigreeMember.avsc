{
  "type" : "record",
  "name" : "PedigreeMember",
  "namespace" : "org.gel.models.participant.avro",
  "doc" : "This defines a RD Participant (demographics and pedigree information)",
  "fields" : [ {
    "name" : "pedigreeId",
    "type" : [ "null", "int" ],
    "doc" : "Numbering used to refer to each member of the pedigree"
  }, {
    "name" : "isProband",
    "type" : [ "null", "boolean" ],
    "doc" : "If this field is true, the member should be considered the proband of this family"
  }, {
    "name" : "participantId",
    "type" : [ "null", "string" ],
    "doc" : "participantId.  This is the human readable ID in GMS"
  }, {
    "name" : "participantQCState",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "ParticipantQCState",
      "doc" : "QCState Status",
      "symbols" : [ "noState", "passedMedicalReviewReadyForInterpretation", "passedMedicalReviewNotReadyForInterpretation", "queryToGel", "queryToGMC", "failed" ]
    } ],
    "doc" : "participantQCState"
  }, {
    "name" : "gelSuperFamilyId",
    "type" : [ "null", "string" ],
    "doc" : "superFamily id, this id is built as a concatenation of all\n        families id in this superfamily i.e, fam10024_fam100457.\n        NOTE: Retired for GMS"
  }, {
    "name" : "sex",
    "type" : {
      "type" : "enum",
      "name" : "Sex",
      "doc" : "Sex",
      "symbols" : [ "MALE", "FEMALE", "UNKNOWN" ]
    },
    "doc" : "Sex of the Participant"
  }, {
    "name" : "personKaryotypicSex",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "PersonKaryotipicSex",
      "doc" : "Karyotipic Sex",
      "symbols" : [ "UNKNOWN", "XX", "XY", "XO", "XXY", "XXX", "XXYY", "XXXY", "XXXX", "XYY", "OTHER" ]
    } ],
    "doc" : "Karyotypic sex of the participant as previously established or by looking at the GEL genome"
  }, {
    "name" : "yearOfBirth",
    "type" : [ "null", "int" ],
    "doc" : "Year of Birth"
  }, {
    "name" : "fatherId",
    "type" : [ "null", "int" ],
    "doc" : "refers to the pedigreeId of the father\n        Id of the parent, if unknown then no parent is referenced. Parents may need to be entered even if no data is known\n        about them in order to unambiguously reconstruct the pedigree."
  }, {
    "name" : "motherId",
    "type" : [ "null", "int" ],
    "doc" : "refers to the pedigreeId of the mother\n        Id of the parent, if unknown then no parent is referenced. Parents may need to be entered even if no data is known\n        about them in order to unambiguously reconstruct the pedigree."
  }, {
    "name" : "superFatherId",
    "type" : [ "null", "int" ],
    "doc" : "this id is built using the original familyId and the original pedigreeId of the father\n        NOTE: retired for GMS"
  }, {
    "name" : "superMotherId",
    "type" : [ "null", "int" ],
    "doc" : "this id is built using the original familyId and the original pedigreeId of the mother\n        NOTE: Retired for GMS"
  }, {
    "name" : "twinGroup",
    "type" : [ "null", "int" ],
    "doc" : "Each twin group is numbered, i.e. all members of a group of multiparous births receive the same number"
  }, {
    "name" : "monozygotic",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "TernaryOption",
      "doc" : "This defines a yes/no/unknown case",
      "symbols" : [ "yes", "no", "unknown" ]
    } ],
    "doc" : "A property of the twinning group but should be entered for all members"
  }, {
    "name" : "adoptedStatus",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "AdoptedStatus",
      "doc" : "adoptedin means adopted into the family\n    adoptedout means child belonged to the family and was adopted out",
      "symbols" : [ "notadopted", "adoptedin", "adoptedout" ]
    } ],
    "doc" : "Adopted Status"
  }, {
    "name" : "lifeStatus",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "LifeStatus",
      "doc" : "Life Status",
      "symbols" : [ "ALIVE", "ABORTED", "DECEASED", "UNBORN", "STILLBORN", "MISCARRIAGE" ]
    } ],
    "doc" : "Life Status"
  }, {
    "name" : "consanguineousParents",
    "type" : [ "null", "TernaryOption" ],
    "doc" : "The parents of this participant has a consanguineous relationship"
  }, {
    "name" : "affectionStatus",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "AffectionStatus",
      "doc" : "Affection Status",
      "symbols" : [ "UNAFFECTED", "AFFECTED", "UNCERTAIN" ]
    } ],
    "doc" : "Affection Status"
  }, {
    "name" : "disorderList",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "Disorder",
        "doc" : "This is quite GEL specific. This is the way is stored in ModelCatalogue and PanelApp.\n    Currently all specific disease titles are assigned to a disease subgroup so really only specificDisease needs to be\n    completed but we add the others for generality",
        "fields" : [ {
          "name" : "diseaseGroup",
          "type" : [ "null", "string" ],
          "doc" : "This is Level2 Title for this disorder"
        }, {
          "name" : "diseaseSubGroup",
          "type" : [ "null", "string" ],
          "doc" : "This is Level3 Title for this disorder"
        }, {
          "name" : "specificDisease",
          "type" : [ "null", "string" ],
          "doc" : "This is Level4 Title for this disorder.\n        In GMS, this is the clinicalIndicationFullName/clinicalIndicationCode."
        }, {
          "name" : "ageOfOnset",
          "type" : [ "null", "float" ],
          "doc" : "Age of onset in years"
        } ]
      }
    } ],
    "doc" : "Clinical Data (disorders). If the family member is unaffected as per affectionStatus then this list is empty."
  }, {
    "name" : "hpoTermList",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "HpoTerm",
        "doc" : "This defines an HPO term and its modifiers (possibly multiple)\n    If HPO term presence is unknown we don't have a entry on the list",
        "fields" : [ {
          "name" : "term",
          "type" : "string",
          "doc" : "Identifier of the HPO term"
        }, {
          "name" : "termPresence",
          "type" : [ "null", "TernaryOption" ],
          "doc" : "This is whether the term is present in the participant (default is unknown) yes=term is present in participant,\n        no=term is not present"
        }, {
          "name" : "hpoBuildNumber",
          "type" : [ "null", "string" ],
          "doc" : "hpoBuildNumber"
        }, {
          "name" : "modifiers",
          "type" : [ "null", {
            "type" : "array",
            "items" : {
              "type" : "record",
              "name" : "HpoTermModifiers",
              "doc" : "HPO Modifiers\n    For GMS, hpoModifierCode and hpoModifierVersion will be used",
              "fields" : [ {
                "name" : "laterality",
                "type" : [ "null", {
                  "type" : "enum",
                  "name" : "Laterality",
                  "symbols" : [ "RIGHT", "UNILATERAL", "BILATERAL", "LEFT" ]
                } ]
              }, {
                "name" : "progression",
                "type" : [ "null", {
                  "type" : "enum",
                  "name" : "Progression",
                  "symbols" : [ "PROGRESSIVE", "NONPROGRESSIVE" ]
                } ]
              }, {
                "name" : "severity",
                "type" : [ "null", {
                  "type" : "enum",
                  "name" : "Severity",
                  "symbols" : [ "BORDERLINE", "MILD", "MODERATE", "SEVERE", "PROFOUND" ]
                } ]
              }, {
                "name" : "spatialPattern",
                "type" : [ "null", {
                  "type" : "enum",
                  "name" : "SpatialPattern",
                  "symbols" : [ "DISTAL", "GENERALIZED", "LOCALIZED", "PROXIMAL" ]
                } ]
              }, {
                "name" : "hpoModifierCode",
                "type" : [ "null", "string" ]
              }, {
                "name" : "hpoModifierVersion",
                "type" : [ "null", "string" ]
              } ]
            }
          } ],
          "doc" : "Modifier associated with the HPO term"
        }, {
          "name" : "ageOfOnset",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "AgeOfOnset",
            "symbols" : [ "EMBRYONAL_ONSET", "FETAL_ONSET", "NEONATAL_ONSET", "INFANTILE_ONSET", "CHILDHOOD_ONSET", "JUVENILE_ONSET", "YOUNG_ADULT_ONSET", "LATE_ONSET", "MIDDLE_AGE_ONSET" ]
          } ],
          "doc" : "Age of onset in months"
        } ]
      }
    } ],
    "doc" : "Clinical Data (HPO terms)"
  }, {
    "name" : "ancestries",
    "type" : [ "null", {
      "type" : "record",
      "name" : "Ancestries",
      "doc" : "Ancestries, defined as Ethnic category(ies) and Chi-square test",
      "fields" : [ {
        "name" : "mothersEthnicOrigin",
        "type" : [ "null", {
          "type" : "enum",
          "name" : "EthnicCategory",
          "doc" : "This is the list of ethnicities in ONS16\n\n    * `D`:  Mixed: White and Black Caribbean\n    * `E`:  Mixed: White and Black African\n    * `F`:  Mixed: White and Asian\n    * `G`:  Mixed: Any other mixed background\n    * `A`:  White: British\n    * `B`:  White: Irish\n    * `C`:  White: Any other White background\n    * `L`:  Asian or Asian British: Any other Asian background\n    * `M`:  Black or Black British: Caribbean\n    * `N`:  Black or Black British: African\n    * `H`:  Asian or Asian British: Indian\n    * `J`:  Asian or Asian British: Pakistani\n    * `K`:  Asian or Asian British: Bangladeshi\n    * `P`:  Black or Black British: Any other Black background\n    * `S`:  Other Ethnic Groups: Any other ethnic group\n    * `R`:  Other Ethnic Groups: Chinese\n    * `Z`:  Not stated",
          "symbols" : [ "D", "E", "F", "G", "A", "B", "C", "L", "M", "N", "H", "J", "K", "P", "S", "R", "Z" ]
        } ],
        "doc" : "Mother's Ethnic Origin"
      }, {
        "name" : "mothersOtherRelevantAncestry",
        "type" : [ "null", "string" ],
        "doc" : "Mother's Ethnic Origin Description"
      }, {
        "name" : "fathersEthnicOrigin",
        "type" : [ "null", "EthnicCategory" ],
        "doc" : "Father's Ethnic Origin"
      }, {
        "name" : "fathersOtherRelevantAncestry",
        "type" : [ "null", "string" ],
        "doc" : "Father's Ethnic Origin Description"
      }, {
        "name" : "chiSquare1KGenomesPhase3Pop",
        "type" : [ "null", {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "ChiSquare1KGenomesPhase3Pop",
            "doc" : "Chi-square test for goodness of fit of this sample to 1000 Genomes Phase 3 populations",
            "fields" : [ {
              "name" : "kgSuperPopCategory",
              "type" : {
                "type" : "enum",
                "name" : "KgSuperPopCategory",
                "doc" : "1K Genomes project super populations",
                "symbols" : [ "AFR", "AMR", "EAS", "EUR", "SAS" ]
              },
              "doc" : "1K Super Population"
            }, {
              "name" : "kgPopCategory",
              "type" : [ "null", {
                "type" : "enum",
                "name" : "KgPopCategory",
                "doc" : "1K Genomes project populations",
                "symbols" : [ "ACB", "ASW", "BEB", "CDX", "CEU", "CHB", "CHS", "CLM", "ESN", "FIN", "GBR", "GIH", "GWD", "IBS", "ITU", "JPT", "KHV", "LWK", "MSL", "MXL", "PEL", "PJL", "PUR", "STU", "TSI", "YRI" ]
              } ],
              "doc" : "1K Population"
            }, {
              "name" : "chiSquare",
              "type" : "double",
              "doc" : "Chi-square test for goodness of fit of this sample to this 1000 Genomes Phase 3 population"
            } ]
          }
        } ],
        "doc" : "Chi-square test for goodness of fit of this sample to 1000 Genomes Phase 3 populations"
      } ]
    } ],
    "doc" : "Participant's ancestries, defined as Mother's/Father's Ethnic Origin and Chi-square test for goodness of fit of this sample to 1000 Genomes Phase 3 populations.\n        NOTE: for GMS this field has been deprecated in favour of clinicalEthnicities"
  }, {
    "name" : "consentStatus",
    "type" : [ "null", {
      "type" : "record",
      "name" : "ConsentStatus",
      "doc" : "Consent Status for 100k program",
      "fields" : [ {
        "name" : "programmeConsent",
        "type" : "boolean",
        "doc" : "Is this individual consented to the programme?\n        It could simply be a family member that is not consented but for whom affection status is known",
        "default" : false
      }, {
        "name" : "primaryFindingConsent",
        "type" : "boolean",
        "doc" : "Consent for feedback of primary findings?",
        "default" : false
      }, {
        "name" : "secondaryFindingConsent",
        "type" : "boolean",
        "doc" : "Consent for secondary finding lookup",
        "default" : false
      }, {
        "name" : "carrierStatusConsent",
        "type" : "boolean",
        "doc" : "Consent for carrier status check?",
        "default" : false
      } ]
    } ],
    "doc" : "What has this participant consented to?\n        A participant that has been consented to the programme should also have sequence data associated with them; however\n        this needs to be programmatically checked"
  }, {
    "name" : "testConsentStatus",
    "type" : [ "null", {
      "type" : "record",
      "name" : "GmsConsentStatus",
      "doc" : "Consent Status for GMS",
      "fields" : [ {
        "name" : "programmeConsent",
        "type" : {
          "type" : "enum",
          "name" : "GenericConsent",
          "doc" : "clinicalEthnicities supersedes Ancestries in GMS",
          "symbols" : [ "yes", "no", "undefined", "not_applicable" ]
        },
        "doc" : "Is this individual consented to the programme? It could simply be a family member that is not consented\n        but for whom affection status is known"
      }, {
        "name" : "primaryFindingConsent",
        "type" : "GenericConsent",
        "doc" : "Consent for feedback of primary findings?\n        RD: Primary Findings\n        Cancer: PrimaryFindings is somatic + pertinent germline findings"
      }, {
        "name" : "researchConsent",
        "type" : "GenericConsent",
        "doc" : "Research Consent"
      }, {
        "name" : "healthRelatedFindingConsent",
        "type" : "GenericConsent",
        "doc" : "Consent for secondary health related findings?"
      }, {
        "name" : "carrierStatusConsent",
        "type" : "GenericConsent",
        "doc" : "Consent for carrier status check?"
      }, {
        "name" : "pharmacogenomicsFindingConsent",
        "type" : "GenericConsent",
        "doc" : "Consent for pharmacogenomics consent as secondary findings?"
      } ]
    } ],
    "doc" : "What has this participant consented in the context of a Genomic Test?"
  }, {
    "name" : "samples",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "GermlineSample",
        "doc" : "A germline sample",
        "fields" : [ {
          "name" : "sampleId",
          "type" : "string",
          "doc" : "Sample identifier (e.g, LP00012645_5GH))"
        }, {
          "name" : "labSampleId",
          "type" : "string",
          "doc" : "Lab sample identifier"
        }, {
          "name" : "LDPCode",
          "type" : [ "null", "string" ],
          "doc" : "LDP Code (Local Delivery Partner)"
        }, {
          "name" : "source",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "SampleSource",
            "doc" : "The source of the sample\n    NOTE: IN GMS, BONE_MARROW_ASPIRATE_TUMOUR_CELLS and BONE_MARROW_ASPIRATE_TUMOUR_SORTED_CELLS are deprecated as they have been separated into their respective biotypes",
            "symbols" : [ "AMNIOTIC_FLUID", "BLOOD", "BLOOD_CAPILLARY_HEEL_PRICK", "BLOOD_CLOT", "BLOOD_CORD", "BLOOD_DRIED_BLOOD_SPOT", "BONE_MARROW", "BONE_MARROW_ASPIRATE_TUMOUR_CELLS", "BONE_MARROW_ASPIRATE_TUMOUR_SORTED_CELLS", "BUCCAL_SPONGE", "BUCCAL_SWAB", "BUFFY_COAT", "CHORIONIC_VILLUS_SAMPLE", "FIBROBLAST", "FLUID", "FRESH_TISSUE_IN_CULTURE_MEDIUM", "OTHER", "SALIVA", "TISSUE", "TUMOUR", "URINE" ]
          } ],
          "doc" : "Source of the sample"
        }, {
          "name" : "product",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "Product",
            "symbols" : [ "DNA", "RNA" ]
          } ],
          "doc" : "Product of the sample"
        }, {
          "name" : "preparationMethod",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "PreparationMethod",
            "doc" : "In 100K, preparation Method of sample\n    NOTE: In GMS, this field is deprecated in favour of StorageMedium and Method",
            "symbols" : [ "ASPIRATE", "CD128_SORTED_CELLS", "CD138_SORTED_CELLS", "EDTA", "FF", "FFPE", "LI_HEP", "ORAGENE" ]
          } ],
          "doc" : "Preparation method\n        NOTE: In GMS, this has been deprecated in favour of Method and storageMedium"
        }, {
          "name" : "programmePhase",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "ProgrammePhase",
            "symbols" : [ "CRUK", "OXFORD", "CLL", "IIP", "MAIN", "EXPT" ]
          } ],
          "doc" : "Genomics England programme phase"
        }, {
          "name" : "clinicalSampleDateTime",
          "type" : [ "null", "string" ],
          "doc" : "The time when the sample was received. In the format YYYY-MM-DDTHH:MM:SS+0000"
        }, {
          "name" : "participantId",
          "type" : [ "null", "string" ]
        }, {
          "name" : "participantUid",
          "type" : [ "null", "string" ],
          "doc" : "Participant UId of the sample"
        }, {
          "name" : "sampleUid",
          "type" : [ "null", "string" ]
        }, {
          "name" : "maskedPid",
          "type" : [ "null", "string" ]
        }, {
          "name" : "method",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "Method",
            "doc" : "In GMS, Method is defined as how the sample was taken directly from the patient",
            "symbols" : [ "ASPIRATE", "BIOPSY", "NOT_APPLICABLE", "RESECTION", "SORTED_OTHER", "UNKNOWN", "UNSORTED", "CD138_SORTED" ]
          } ],
          "doc" : "In GMS, this is how the sample was extracted from the participant"
        }, {
          "name" : "storageMedium",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "StorageMedium",
            "doc" : "In GMS, storage medium of sample",
            "symbols" : [ "EDTA", "FF", "LI_HEP", "ORAGENE", "FFPE" ]
          } ],
          "doc" : "In GMS, this is what solvent/medium the sample was stored in"
        }, {
          "name" : "sampleType",
          "type" : [ "null", "string" ],
          "doc" : "In GMS, this is the sampleType as entered by the clinician in TOMs"
        }, {
          "name" : "sampleState",
          "type" : [ "null", "string" ],
          "doc" : "In GMS, this is the sampleState as entered by the clinician in TOMs"
        }, {
          "name" : "sampleCollectionOperationsQuestions",
          "type" : [ "null", {
            "type" : "record",
            "name" : "SampleCollectionOperationsQuestions",
            "doc" : "Operations questions on sample collection for Newborns BaMMs",
            "fields" : [ {
              "name" : "easeOfSampleCollection",
              "type" : [ "null", {
                "type" : "enum",
                "name" : "EaseOfSampleCollection",
                "doc" : "Ease of Sample Collection",
                "symbols" : [ "EASY", "DIFFICULT", "NOT_APPLICABLE" ]
              } ],
              "doc" : "Ease of successful sample collection"
            }, {
              "name" : "minutes",
              "type" : [ "null", "int" ],
              "doc" : "Time taken to collect the sample (from opening collection kit to completion) in minutes"
            } ]
          } ],
          "doc" : "In Newborns BaMMs, this is operational sample collection questions"
        } ]
      }
    } ],
    "doc" : "This is an array containing all the samples that belong to this individual, e.g [\"LP00002255_GA4\"]"
  }, {
    "name" : "inbreedingCoefficient",
    "type" : [ "null", {
      "type" : "record",
      "name" : "InbreedingCoefficient",
      "doc" : "Inbreeding coefficient",
      "fields" : [ {
        "name" : "sampleId",
        "type" : "string",
        "doc" : "This is the sample id against which the coefficient was estimated"
      }, {
        "name" : "program",
        "type" : "string",
        "doc" : "Name of program used to calculate the coefficient"
      }, {
        "name" : "version",
        "type" : "string",
        "doc" : "Version of the programme"
      }, {
        "name" : "estimationMethod",
        "type" : "string",
        "doc" : "Where various methods for estimation exist, which method was used."
      }, {
        "name" : "coefficient",
        "type" : "double",
        "doc" : "Inbreeding coefficient ideally a real number in [0,1]"
      }, {
        "name" : "standardError",
        "type" : [ "null", "double" ],
        "doc" : "Standard error of the Inbreeding coefficient"
      } ]
    } ],
    "doc" : "Inbreeding Coefficient Estimation"
  }, {
    "name" : "additionalInformation",
    "type" : [ "null", {
      "type" : "map",
      "values" : "string"
    } ],
    "doc" : "We could add a map here to store additional information for example URIs to images, ECGs, etc\n        Null by default"
  }, {
    "name" : "lastMenstrualPeriod",
    "type" : [ "null", "string" ],
    "doc" : "Last Menstrual Period"
  }, {
    "name" : "diagnosticDetails",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "DiagnosticDetail",
        "fields" : [ {
          "name" : "diagnosisCodingSystem",
          "type" : "string",
          "doc" : "Diagnosis coding system"
        }, {
          "name" : "diagnosisCodingSystemVersion",
          "type" : [ "null", "string" ],
          "doc" : "Diagnosis Coding System Version"
        }, {
          "name" : "diagnosisCode",
          "type" : "string",
          "doc" : "Diagnosis Code"
        }, {
          "name" : "diagnosisDescription",
          "type" : [ "null", "string" ],
          "doc" : "Diagnosis description"
        }, {
          "name" : "diagnosisCertainty",
          "type" : [ "null", "string" ],
          "doc" : "Diagnosis Certainty"
        }, {
          "name" : "ageAtOnsetInYears",
          "type" : [ "null", "float" ],
          "doc" : "Age at diagnosis"
        } ]
      }
    } ],
    "doc" : "Additional set of diagnostic ontology terms"
  }, {
    "name" : "participantUid",
    "type" : [ "null", "string" ],
    "doc" : "ParticipantGUID in GMS"
  }, {
    "name" : "clinicalEthnicities",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "enum",
        "name" : "ClinicalEthnicity",
        "doc" : "* A     British, Mixed British\n    * B     Irish\n    * C     Any other White background\n    * C2    Northern Irish\n    * C3    Other white, white unspecified\n    * CA    English\n    * CB    Scottish\n    * CC    Welsh\n    * CD    Cornish\n    * CE    Cypriot (part not stated)\n    * CF    Greek\n    * CG    Greek Cypriot\n    * CH    Turkish\n    * CJ    Turkish Cypriot\n    * CK    Italian\n    * CL    Irish Traveller\n    * CM    Traveller\n    * CN    Gypsy/Romany\n    * CP    Polish\n    * CQ    All republics which made up the former USSR\n    * CR    Kosovan\n    * CS    Albanian\n    * CT    Bosnian\n    * CU    Croatian\n    * CV    Serbian\n    * CW    Other republics which made up the former Yugoslavia\n    * CX    Mixed white\n    * CY    Other white European, European unspecified, European mixed\n    * D     White and Black Caribbean\n    * E     White and Black African\n    * F     White and Asian\n    * G     Any other mixed background\n    * GA    Black and Asian\n    * GB    Black and Chinese\n    * GC    Black and White\n    * GD    Chinese and White\n    * GE    Asian and Chinese\n    * GF    Other Mixed, Mixed Unspecified\n    * H     Indian or British Indian\n    * J     Pakistani or British Pakistani\n    * K     Bangladeshi or British Bangladeshi\n    * L     Any other Asian background\n    * LA    Mixed Asian\n    * LB    Punjabi\n    * LC    Kashmiri\n    * LD    East African Asian\n    * LE    Sri Lanka\n    * LF    Tamil\n    * LG    Sinhalese\n    * LH    British Asian\n    * LJ    Caribbean Asian\n    * LK    Other Asian, Asian unspecified\n    * M     Caribbean\n    * N     African\n    * P     Any other Black background\n    * PA    Somali\n    * PB    Mixed Black\n    * PC    Nigerian\n    * PD    Black British\n    * PE    Other Black, Black unspecified\n    * R     Chinese\n    * S     Any other ethnic group\n    * S1    Ashkenazi\n    * S2    Sephardi\n    * SA    Vietnamese\n    * SB    Japanese\n    * SC    Filipino\n    * SD    Malaysian\n    * SE    Any Other Group\n    * Z     Not stated",
        "symbols" : [ "A", "B", "C", "C2", "C3", "CA", "CB", "CC", "CD", "CE", "CF", "CG", "CH", "CJ", "CK", "CL", "CM", "CN", "CP", "CQ", "CR", "CS", "CT", "CU", "CV", "CW", "CX", "CY", "D", "E", "F", "G", "GA", "GB", "GC", "GD", "GE", "GF", "H", "J", "K", "L", "LA", "LB", "LC", "LD", "LE", "LF", "LG", "LH", "LJ", "LK", "M", "N", "P", "PA", "PB", "PC", "PD", "PE", "R", "S", "S1", "S2", "SA", "SB", "SC", "SD", "SE", "Z" ]
      }
    } ],
    "doc" : "ClinicalEthnicity as defined for GMS"
  }, {
    "name" : "previousTreatment",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "PreviousTreatment",
        "doc" : "In GMS, Previous Treatment of Patient",
        "fields" : [ {
          "name" : "previousTreatmentType",
          "type" : [ "null", "string" ]
        }, {
          "name" : "previousTreatmentName",
          "type" : [ "null", "string" ]
        }, {
          "name" : "previousTreatmentDate",
          "type" : [ "null", {
            "type" : "record",
            "name" : "Date",
            "doc" : "This defines a date record",
            "fields" : [ {
              "name" : "year",
              "type" : "int",
              "doc" : "Format YYYY"
            }, {
              "name" : "month",
              "type" : [ "null", "int" ],
              "doc" : "Format MM. e.g June is 06"
            }, {
              "name" : "day",
              "type" : [ "null", "int" ],
              "doc" : "Format DD e.g. 12th of October is 12"
            } ]
          } ]
        } ]
      }
    } ],
    "doc" : "For GMS cases, previous Treatment History"
  } ]
}
