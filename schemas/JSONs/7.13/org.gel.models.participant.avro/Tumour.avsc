{
  "type" : "record",
  "name" : "Tumour",
  "namespace" : "org.gel.models.participant.avro",
  "fields" : [ {
    "name" : "tumourId",
    "type" : "string",
    "doc" : "TumourId in GMS"
  }, {
    "name" : "tumourLocalId",
    "type" : "string",
    "doc" : "Local hospital tumour ID from the GLH Laboratory Information Management System (LIMS) in GMS"
  }, {
    "name" : "tumourType",
    "type" : {
      "type" : "enum",
      "name" : "TumourType",
      "doc" : "NOTE: This has been changed completely, the previous tumour type has been split into TumourPresentation and PrimaryOrMetastatic",
      "symbols" : [ "BRAIN_TUMOUR", "HAEMATOLOGICAL_MALIGNANCY_SOLID_SAMPLE", "HAEMATOLOGICAL_MALIGNANCY_LIQUID_SAMPLE", "SOLID_TUMOUR_METASTATIC", "SOLID_TUMOUR_PRIMARY", "SOLID_TUMOUR", "UNKNOWN" ]
    },
    "doc" : "tumourType"
  }, {
    "name" : "tumourParentId",
    "type" : [ "null", "string" ],
    "doc" : "Parent Tumour UID if this tumour is metastatic"
  }, {
    "name" : "tumourDiagnosisDate",
    "type" : [ "null", {
      "type" : "record",
      "name" : "Date",
      "doc" : "This defines a date record",
      "fields" : [ {
        "name" : "year",
        "type" : "int",
        "doc" : "Format YYYY"
      }, {
        "name" : "month",
        "type" : [ "null", "int" ],
        "doc" : "Format MM. e.g June is 06"
      }, {
        "name" : "day",
        "type" : [ "null", "int" ],
        "doc" : "Format DD e.g. 12th of October is 12"
      } ]
    } ],
    "doc" : "Date of Diagnosis of the specific tumour"
  }, {
    "name" : "tumourDescription",
    "type" : [ "null", "string" ],
    "doc" : "Description of the tumour"
  }, {
    "name" : "tumourMorphologies",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "Morphology",
        "fields" : [ {
          "name" : "id",
          "type" : [ "null", "string" ],
          "doc" : "The ontology term id or accession in OBO format ${ONTOLOGY_ID}:${TERM_ID} (http://www.obofoundry.org/id-policy.html)"
        }, {
          "name" : "name",
          "type" : [ "null", "string" ],
          "doc" : "The ontology term name"
        }, {
          "name" : "value",
          "type" : [ "null", "string" ],
          "doc" : "Optional value for the ontology term, the type of the value is not checked\n        (i.e.: we could set the pvalue term to \"significant\" or to \"0.0001\")"
        }, {
          "name" : "version",
          "type" : [ "null", "string" ],
          "doc" : "Ontology version"
        } ]
      }
    } ],
    "doc" : "Morphology of the tumour"
  }, {
    "name" : "tumourTopographies",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "Topography",
        "fields" : [ {
          "name" : "id",
          "type" : [ "null", "string" ],
          "doc" : "The ontology term id or accession in OBO format ${ONTOLOGY_ID}:${TERM_ID} (http://www.obofoundry.org/id-policy.html)"
        }, {
          "name" : "name",
          "type" : [ "null", "string" ],
          "doc" : "The ontology term name"
        }, {
          "name" : "value",
          "type" : [ "null", "string" ],
          "doc" : "Optional value for the ontology term, the type of the value is not checked\n        (i.e.: we could set the pvalue term to \"significant\" or to \"0.0001\")"
        }, {
          "name" : "version",
          "type" : [ "null", "string" ],
          "doc" : "Ontology version"
        } ]
      }
    } ],
    "doc" : "Topography of the tumour"
  }, {
    "name" : "tumourPrimaryTopographies",
    "type" : [ "null", {
      "type" : "array",
      "items" : "Topography"
    } ],
    "doc" : "Associated primary topography for metastatic tumours"
  }, {
    "name" : "tumourGrade",
    "type" : [ "null", "string" ],
    "doc" : "Grade of the Tumour"
  }, {
    "name" : "tumourStage",
    "type" : [ "null", "string" ],
    "doc" : "Stage of the Tumour"
  }, {
    "name" : "tumourPrognosticScore",
    "type" : [ "null", "string" ],
    "doc" : "Prognostic Score of the Tumour"
  }, {
    "name" : "tumourPresentation",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "TumourPresentation",
      "symbols" : [ "FIRST_PRESENTATION", "RECURRENCE", "UNKNOWN" ]
    } ],
    "doc" : "In GMS, tumour presentation"
  }, {
    "name" : "primaryOrMetastatic",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "PrimaryOrMetastatic",
      "symbols" : [ "PRIMARY", "METASTATIC", "UNKNOWN", "NOT_APPLICABLE" ]
    } ],
    "doc" : "In GMS, primary or metastatic"
  } ]
}
