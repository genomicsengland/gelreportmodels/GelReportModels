{
  "type" : "record",
  "name" : "BirthClinicalQuestions",
  "namespace" : "org.gel.models.participant.avro",
  "doc" : "Model that captures the clinical questions from birth.",
  "fields" : [ {
    "name" : "gravidaNumber",
    "type" : [ "null", "int" ],
    "doc" : "Gravida Number - Gravida indicates the number of times a woman is or has been pregnant, regardless of the pregnancy outcome."
  }, {
    "name" : "dateAndTimeOfDelivery",
    "type" : [ "null", "string" ],
    "doc" : "String capturing the date and time of the delivery in ISO 8601 format with timezone and no microsecond.\n        format: %Y-%m-%dT%H:%M:%S+00:00\n        e.g.: 2020-03-20T14:31:43+01:00"
  }, {
    "name" : "parityNumber",
    "type" : [ "null", "int" ],
    "doc" : "Parity Number - Parity, or \"para\", indicates the number of births (including live births and stillbirths) where pregnancies reached viable gestational age."
  }, {
    "name" : "numberOfBabiesDelivered",
    "type" : [ "null", "int" ],
    "doc" : "Number of babies delivered in this pregnancy."
  }, {
    "name" : "pregnancyTermWeeks",
    "type" : [ "null", "int" ],
    "doc" : "Pregnancy Term in weeks."
  }, {
    "name" : "pretermLabour",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "enum",
        "name" : "PretermLabour",
        "doc" : "Preterm labour.",
        "symbols" : [ "IDIOPATHIC", "CHORIOAMNIONITIS", "ABRUPTION", "STRETCH" ]
      }
    } ],
    "doc" : "Preterm labour if any."
  }, {
    "name" : "inducedBirth",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "enum",
        "name" : "InducedBirth",
        "doc" : "Induced birth.",
        "symbols" : [ "PROSTAGLANDIN", "BALLOON", "ARTIFICIAL_RUPTURE_OF_MEMBRANES", "ARTIFICIAL_RUPTURE_OF_MEMBRANES_MEMBRANE_SWEEP", "OXYTOCIN" ]
      }
    } ],
    "doc" : "Induced birth if any."
  }, {
    "name" : "modeOfDelivery",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "enum",
        "name" : "ModeOfDelivery",
        "doc" : "Mode of delivery.",
        "symbols" : [ "SPONTANEOUS_VAGINAL", "INSTRUMENTAL_BIRTH", "OTHER_ASSISTED_DELIVERY", "CAESAREAN_SECTION_ELECTIVE", "CAESAREAN_SECTION_EMERGENCY", "CAESAREAN_SECTION_CATEGORY_1", "CAESAREAN_SECTION_CATEGORY_2", "CAESAREAN_SECTION_CATEGORY_3_AND_4" ]
      }
    } ],
    "doc" : "Mode of delivery."
  }, {
    "name" : "membraneStatus",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "enum",
        "name" : "MembraneStatus",
        "doc" : "Membrane status.",
        "symbols" : [ "ARTIFICIAL_RUPTURE_FOR_INDUCTION_OF_LABOUR", "BEFORE_LABOUR", "DURING_LABOUR", "AT_BIRTH" ]
      }
    } ],
    "doc" : "Membrane status."
  }, {
    "name" : "deliveryComplications",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "enum",
        "name" : "DeliveryComplications",
        "doc" : "Delivery complications.\n    The `abnormally_invasive_placenta` includes accrete, increta or percreta.",
        "symbols" : [ "PLACENTA_ABRUPTION", "ABNORMAL_PLACENTATION", "PLACENTA_PRAEVIA", "ABNORMALLY_INVASIVE_PLACENTA", "CORD_RUPTURE", "INFECTION", "MANUAL_REMOVAL_OF_PLACENTA" ]
      }
    } ],
    "doc" : "Delivery Complications."
  }, {
    "name" : "thirdStageOfLabour",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "enum",
        "name" : "ThirdStageOfLabour",
        "doc" : "Third stage - to establish if type of third stage impacts on the quantity of cord blood available for whole genome sequencing.\n    `physiological` - A physiological or natural third stage means waiting for the placenta to be delivered naturally with delay. A clamp is placed on the umbilical cord until it has stopped pulsating to allow oxygenated blood to pulse from the placenta to the baby.\n    `active` - This can refer too:\n        1) giving oxytocin IM/IV to help contract the uterus.\n        2) clamping the cord early (usually before, alongside, or immediately after giving oxytocin IM/IV.\n        3) traction is applied to the cord with counter‐pressure on the uterus to deliver the placenta.\n    `removal_at_caesarean_section` - birth occured through caesarean section.",
        "symbols" : [ "PHYSIOLOGICAL", "ACTIVE", "REMOVAL_AT_CAESAREAN_SECTION" ]
      }
    } ],
    "doc" : "Third stage - to establish if type of third stage impacts on the quantity of cord blood available for whole genome sequencing."
  }, {
    "name" : "durationOfLabour",
    "type" : [ "null", {
      "type" : "record",
      "name" : "DurationOfLabour",
      "doc" : "Model that captures approximate duration of labour duration (this is the onset of regular contractions until delivery of placenta).",
      "fields" : [ {
        "name" : "firstStageHourDuration",
        "type" : [ "null", "float" ],
        "doc" : "First stage (from 3cm and regular contractions) in hours."
      }, {
        "name" : "secondStageHourDuration",
        "type" : [ "null", "float" ],
        "doc" : "Second stage (pushing) in hours."
      }, {
        "name" : "thirdStageMinuteDuration",
        "type" : [ "null", "int" ],
        "doc" : "Third stage (delivery of placenta) in minutes."
      }, {
        "name" : "dateAndTimeOfLabourOnset",
        "type" : [ "null", "string" ],
        "doc" : "Date and time of labour onset.\n        String in ISO 8601 format with timezone and no microsecond.\n        format: %Y-%m-%dT%H:%M:%S+00:00\n        e.g.: 2020-03-20T14:31:43+01:00"
      } ]
    } ],
    "doc" : "Approximate duration of labour duration (this is the onset of regular contractions until delivery of placenta)."
  }, {
    "name" : "babyOralIntakeQuestions",
    "type" : [ "null", {
      "type" : "record",
      "name" : "BabyOralIntakeQuestions",
      "doc" : "Time between oral intake and sample collection (to establish contamination levels and lead to understanding optimum collection time).",
      "fields" : [ {
        "name" : "neverBeenFed",
        "type" : "boolean",
        "doc" : "Has the baby been fed or taken medication before sample was collected."
      }, {
        "name" : "minutesSinceOralIntake",
        "type" : [ "null", "int" ],
        "doc" : "Time since last oral intake (feed or medication) in minutes. NOTE: Only fill in if baby was fed before sample collection."
      }, {
        "name" : "babyIntake",
        "type" : [ "null", {
          "type" : "enum",
          "name" : "BabyIntake",
          "doc" : "Baby intake (feed or medication).",
          "symbols" : [ "MATERNAL_BREAST_MILK", "DONOR_BREAST_MILK", "INFANT_FORMULA", "MEDICATION" ]
        } ],
        "doc" : "Type of last intake (feed or medication). NOTE: Only fill in if baby was fed before sample collection."
      } ]
    } ],
    "doc" : "Baby oral intake questions before sample (e.g. buccal swab, buccal sponge) was collected."
  }, {
    "name" : "mumsTimeSinceLastOralIntake",
    "type" : [ "null", "int" ],
    "doc" : "How long ago (in minutes) since mum had oral intake of food or fluids."
  } ]
}
