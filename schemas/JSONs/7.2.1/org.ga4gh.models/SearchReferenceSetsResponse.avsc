{
  "type" : "record",
  "name" : "SearchReferenceSetsResponse",
  "namespace" : "org.ga4gh.methods",
  "doc" : "This is the response from `POST /referencesets/search`\nexpressed as JSON.",
  "fields" : [ {
    "name" : "referenceSets",
    "type" : {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "ReferenceSet",
        "namespace" : "org.ga4gh.models",
        "doc" : "A `ReferenceSet` is a set of `Reference`s which typically comprise a\nreference assembly, such as `GRCh38`. A `ReferenceSet` defines a common\ncoordinate space for comparing reference-aligned experimental data.",
        "fields" : [ {
          "name" : "id",
          "type" : "string",
          "doc" : "The reference set ID. Unique in the repository."
        }, {
          "name" : "md5checksum",
          "type" : "string",
          "doc" : "Order-independent MD5 checksum which identifies this `ReferenceSet`.\n\n  To compute this checksum, make a list of `Reference.md5checksum` for all\n  `Reference`s in this set. Then sort that list, and take the MD5 hash of\n  all the strings concatenated together. Express the hash as a lower-case\n  hexadecimal string."
        }, {
          "name" : "ncbiTaxonId",
          "type" : [ "null", "int" ],
          "doc" : "ID from http://www.ncbi.nlm.nih.gov/taxonomy (e.g. 9606->human) indicating\n  the species which this assembly is intended to model. Note that contained\n  `Reference`s may specify a different `ncbiTaxonId`, as assemblies may\n  contain reference sequences which do not belong to the modeled species, e.g.\n  EBV in a human reference genome.",
          "default" : null
        }, {
          "name" : "description",
          "type" : [ "null", "string" ],
          "doc" : "Optional free text description of this reference set.",
          "default" : null
        }, {
          "name" : "assemblyId",
          "type" : [ "null", "string" ],
          "doc" : "Public id of this reference set, such as `GRCh37`.",
          "default" : null
        }, {
          "name" : "sourceURI",
          "type" : [ "null", "string" ],
          "doc" : "Specifies a FASTA format file/string.",
          "default" : null
        }, {
          "name" : "sourceAccessions",
          "type" : {
            "type" : "array",
            "items" : "string"
          },
          "doc" : "All known corresponding accession IDs in INSDC (GenBank/ENA/DDBJ) ideally\n  with a version number, e.g. `NC_000001.11`."
        }, {
          "name" : "isDerived",
          "type" : "boolean",
          "doc" : "A reference set may be derived from a source if it contains\n  additional sequences, or some of the sequences within it are derived\n  (see the definition of `isDerived` in `Reference`).",
          "default" : false
        } ]
      }
    },
    "doc" : "The list of matching reference sets.",
    "default" : [ ]
  }, {
    "name" : "nextPageToken",
    "type" : [ "null", "string" ],
    "doc" : "The continuation token, which is used to page through large result sets.\n  Provide this value in a subsequent request to return the next page of\n  results. This field will be empty if there aren't any additional results.",
    "default" : null
  } ]
}
