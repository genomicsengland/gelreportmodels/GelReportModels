{
  "type" : "record",
  "name" : "GuidelineBasedVariantClassification",
  "namespace" : "org.gel.models.report.avro",
  "doc" : "Variant classification based on guidlines, AMP and ACMG are supported",
  "fields" : [ {
    "name" : "acmgVariantClassification",
    "type" : [ "null", {
      "type" : "record",
      "name" : "AcmgVariantClassification",
      "doc" : "Full record for the ACMG variant clasiffication, including all selectedd evidences and the final classification.",
      "fields" : [ {
        "name" : "acmgEvidences",
        "type" : {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "AcmgEvidence",
            "doc" : "AcmgEvidence. This should be buit for each one of the evidences assing to a variants following the ACMG guidelines.\n    An AcmgEvidence, should map with one of the criteria defined, i.e, PVS1, BA1, PM1...",
            "fields" : [ {
              "name" : "category",
              "type" : {
                "type" : "enum",
                "name" : "AcmgEvidenceCategory",
                "doc" : "Each ACMG criterion is classified in one of these categories",
                "symbols" : [ "population_data", "computational_and_predictive_data", "functional_data", "segregation_data", "de_novo_data", "allelic_data", "other_database", "other_data" ]
              },
              "doc" : "Evidence category as defined in ACMG guidelines"
            }, {
              "name" : "type",
              "type" : {
                "type" : "enum",
                "name" : "AcmgEvidenceType",
                "doc" : "Each ACMG cirterion will be classifed as bening or pathogenic",
                "symbols" : [ "bening", "pathogenic" ]
              },
              "doc" : "Evidence type: bening or pathogenic"
            }, {
              "name" : "weight",
              "type" : {
                "type" : "enum",
                "name" : "AcmgEvidenceWeight",
                "doc" : "Each ACMG criterion is weighted using the following terms:\n\n* `stand_alone`: `A`, stand-alone applied for benign variant critieria `(BA1)`\n* `supporting`: `P`, supporting applied for benign variant critieria `(BP1-6)` and pathogenic variant criteria `(PP1-5)`\n* `moderate`: `M`, moderate applied for pathogenic variant critieria (PM1-6)\n* `strong`: `S`, strong applied for pathogenic variant critieria (PS1-4)\n* `very_strong`: `S`, Very Stong applied for pathogenic variant critieria (PVS1)",
                "symbols" : [ "stand_alone", "supporting", "moderate", "strong", "very_strong" ]
              },
              "doc" : "Weight categories as described in ACMG guideline"
            }, {
              "name" : "modifier",
              "type" : "int",
              "doc" : "modifier of the strength, together define each creteria, i.e the 2 in PM2"
            }, {
              "name" : "description",
              "type" : [ "null", "string" ],
              "doc" : "Description of the evidence"
            } ]
          }
        }
      }, {
        "name" : "clinicalSignificance",
        "type" : {
          "type" : "enum",
          "name" : "ClinicalSignificance",
          "symbols" : [ "benign", "likely_benign", "likely_pathogenic", "pathogenic", "uncertain_significance" ]
        }
      }, {
        "name" : "assessment",
        "type" : [ "null", "string" ]
      } ]
    } ]
  }, {
    "name" : "ampVariantClassification",
    "type" : [ "null", {
      "type" : "record",
      "name" : "AmpVariantClassification",
      "doc" : "Full Variant classification acording to AMP guideline, including all supporting evidences and the final\n    assessment",
      "fields" : [ {
        "name" : "ampEvidences",
        "type" : {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "AmpEvidence",
            "doc" : "Evidences as defined in AMP guidelines, they are composed by a evidence type (first column in the evidence table of\n    the guidlines) and a assessment of the evicence, this last one will define the streght of the evidence, supporting\n    the variant to be classified as TierI-IV",
            "fields" : [ {
              "name" : "type",
              "type" : {
                "type" : "enum",
                "name" : "AmpEvidenceType",
                "doc" : "Type of evidence in tge AMP guideline",
                "symbols" : [ "mutation_type", "therapies", "variant_frequencies", "potential_germline", "population_database_presence", "germline_database_presence", "somatic_database_presence", "impact_predictive_software", "pathway_involvement", "publications" ]
              },
              "doc" : "AMP evidence type according to Guidlines, i.e germline_database_presence"
            }, {
              "name" : "evidenceAssessment",
              "type" : "string",
              "doc" : "Assessment for AMP evidence, i.e Present in ClinVar"
            } ]
          }
        },
        "doc" : "List of AMP evidences"
      }, {
        "name" : "ampTier",
        "type" : {
          "type" : "enum",
          "name" : "AmpTier",
          "doc" : "AMP tier:\n* `TierI`: Variants of Strong Clinical Significance\n* `TierII`: Variants of Potential Clinical Significance\n* `TierIII`: Variants of Unknown Clinical Significance\n* `TierIV`: Benign or Likely Benign Variants",
          "symbols" : [ "tierI", "tierII", "tierIII", "tierIV" ]
        },
        "doc" : "Final Clasification taken in account the evidences"
      }, {
        "name" : "ampClincialOrExperimentalEvidence",
        "type" : [ "null", {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "AmpClincialOrExperimentalEvidence",
            "doc" : "Amp Clinical or Experimental Evidence, the level will define the overal clasification of the variant together with\n    the tiering.",
            "fields" : [ {
              "name" : "category",
              "type" : {
                "type" : "enum",
                "name" : "AmpClinicalOrExperimentalEvidenceCategory",
                "doc" : "Categories of Clinical and/or Experimental Evidence as defined in AMP guidelines",
                "symbols" : [ "therapeutic", "diagnosis", "prognosis" ]
              },
              "doc" : "As denined in AMP guidelines: therapeutic, diagnosis or prognosis"
            }, {
              "name" : "level",
              "type" : {
                "type" : "enum",
                "name" : "AmpClinicalOrExperimentalEvidenceLevel",
                "doc" : "Levels for categories of Clinical and/or Experimental Evidence as defined in AMP guidelines",
                "symbols" : [ "levelA", "levelB", "levelC", "levelD" ]
              },
              "doc" : "As denined in AMP guidelines: levelA, levelB, levelC, levelD"
            }, {
              "name" : "description",
              "type" : [ "null", "string" ],
              "doc" : "Description of the evidence"
            } ]
          }
        } ],
        "doc" : "Clinical or Experimental evicence"
      }, {
        "name" : "assessment",
        "type" : [ "null", "string" ],
        "doc" : "Final Assessment"
      } ]
    } ]
  } ]
}
