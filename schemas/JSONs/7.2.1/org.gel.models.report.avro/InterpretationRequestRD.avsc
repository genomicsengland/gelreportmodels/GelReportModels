{
  "type" : "record",
  "name" : "InterpretationRequestRD",
  "namespace" : "org.gel.models.report.avro",
  "doc" : "This record represents basic information for this report",
  "fields" : [ {
    "name" : "versionControl",
    "type" : {
      "type" : "record",
      "name" : "ReportVersionControl",
      "fields" : [ {
        "name" : "gitVersionControl",
        "type" : "string",
        "doc" : "This is the version for the entire set of data models as referred to the Git release tag",
        "default" : "6.0.2"
      } ]
    },
    "doc" : "Model version number"
  }, {
    "name" : "interpretationRequestId",
    "type" : "string",
    "doc" : "Identifier for this interpretation request"
  }, {
    "name" : "interpretationRequestVersion",
    "type" : "int",
    "doc" : "Version for this interpretation request"
  }, {
    "name" : "internalStudyId",
    "type" : "string",
    "doc" : "Internal study identifier"
  }, {
    "name" : "familyInternalId",
    "type" : [ "null", "string" ],
    "doc" : "Family internal identifier"
  }, {
    "name" : "genomeAssembly",
    "type" : {
      "type" : "enum",
      "name" : "Assembly",
      "doc" : "The reference genome assembly",
      "symbols" : [ "GRCh38", "GRCh37" ]
    },
    "doc" : "This is the version of the assembly used to align the reads"
  }, {
    "name" : "workspace",
    "type" : {
      "type" : "array",
      "items" : "string"
    },
    "doc" : "The genome shall be assigned to the workspaces(projects or domains with a predefined set of users) to control user access"
  }, {
    "name" : "bams",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "File",
        "doc" : "This defines a file\n    This record is uniquely defined by the sample identfier and an URI\n    Currently sample identifier can be a single string or a list of strings if multiple samples are associated with the same file\n    *",
        "fields" : [ {
          "name" : "sampleId",
          "type" : [ "null", {
            "type" : "array",
            "items" : "string"
          } ],
          "doc" : "Unique identifier(s) of the sample. For example in a multisample vcf this would have an array of all the sample identifiers"
        }, {
          "name" : "uriFile",
          "type" : "string",
          "doc" : "URI path of the file"
        }, {
          "name" : "fileType",
          "type" : {
            "type" : "enum",
            "name" : "FileType",
            "symbols" : [ "BAM", "gVCF", "VCF_small", "VCF_somatic_small", "VCF_CNV", "VCF_somatic_CNV", "VCF_SV", "VCF_somatic_SV", "VCF_SV_CNV", "SVG", "ANN", "BigWig", "MD5Sum", "ROH", "OTHER", "PARTITION", "VARIANT_FREQUENCIES", "COVERAGE" ]
          },
          "doc" : "The type of the file"
        }, {
          "name" : "md5Sum",
          "type" : [ "null", "string" ],
          "doc" : "The MD5 checksum"
        } ]
      }
    } ],
    "doc" : "BAMs Files"
  }, {
    "name" : "vcfs",
    "type" : [ "null", {
      "type" : "array",
      "items" : "File"
    } ],
    "doc" : "VCFs Files where SVs and CNVs are represented"
  }, {
    "name" : "bigWigs",
    "type" : [ "null", {
      "type" : "array",
      "items" : "File"
    } ],
    "doc" : "BigWig Files"
  }, {
    "name" : "pedigreeDiagram",
    "type" : [ "null", "File" ],
    "doc" : "Pedigree Diagram Files as an SGV"
  }, {
    "name" : "annotationFile",
    "type" : [ "null", "File" ],
    "doc" : "Variant Annotation File"
  }, {
    "name" : "otherFiles",
    "type" : [ "null", {
      "type" : "map",
      "values" : "File"
    } ],
    "doc" : "Other files that may be vendor specific\n        map of key: type of file, value: record of type File"
  }, {
    "name" : "pedigree",
    "type" : [ "null", {
      "type" : "record",
      "name" : "Pedigree",
      "namespace" : "org.gel.models.participant.avro",
      "doc" : "This is the concept of a family with associated phenotypes as present in the record RDParticipant",
      "fields" : [ {
        "name" : "versionControl",
        "type" : [ "null", {
          "type" : "record",
          "name" : "VersionControl",
          "fields" : [ {
            "name" : "GitVersionControl",
            "type" : "string",
            "doc" : "This is the version for the entire set of data models as referred to the Git release tag",
            "default" : "1.1.3"
          } ]
        } ],
        "doc" : "Model version number"
      }, {
        "name" : "LDPCode",
        "type" : [ "null", "string" ],
        "doc" : "LDP Code (Local Delivery Partner)"
      }, {
        "name" : "familyId",
        "type" : "string",
        "doc" : "Family identifier which internally translate to a sample set"
      }, {
        "name" : "members",
        "type" : {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "PedigreeMember",
            "doc" : "This defines a RD Participant (demographics and pedigree information)",
            "fields" : [ {
              "name" : "pedigreeId",
              "type" : [ "null", "int" ],
              "doc" : "Numbering used to refer to each member of the pedigree"
            }, {
              "name" : "isProband",
              "type" : [ "null", "boolean" ],
              "doc" : "If this field is true, the member should be considered the proband of this family"
            }, {
              "name" : "participantId",
              "type" : [ "null", "string" ],
              "doc" : "participantId"
            }, {
              "name" : "participantQCState",
              "type" : [ "null", {
                "type" : "enum",
                "name" : "ParticipantQCState",
                "doc" : "QCState Status",
                "symbols" : [ "noState", "passedMedicalReviewReadyForInterpretation", "passedMedicalReviewNotReadyForInterpretation", "queryToGel", "queryToGMC", "failed" ]
              } ],
              "doc" : "participantQCState"
            }, {
              "name" : "gelSuperFamilyId",
              "type" : [ "null", "string" ],
              "doc" : "superFamily id, this id is built as a concatenation of all families id in this superfamily i.e, fam10024_fam100457"
            }, {
              "name" : "sex",
              "type" : {
                "type" : "enum",
                "name" : "Sex",
                "doc" : "Sex",
                "symbols" : [ "MALE", "FEMALE", "UNKNOWN" ]
              },
              "doc" : "Sex of the Participant"
            }, {
              "name" : "personKaryotypicSex",
              "type" : [ "null", {
                "type" : "enum",
                "name" : "PersonKaryotipicSex",
                "doc" : "Karyotipic Sex",
                "symbols" : [ "UNKNOWN", "XX", "XY", "XO", "XXY", "XXX", "XXYY", "XXXY", "XXXX", "XYY", "OTHER" ]
              } ],
              "doc" : "Karyotypic sex of the participant as previously established or by looking at the GEL genome"
            }, {
              "name" : "yearOfBirth",
              "type" : [ "null", "int" ],
              "doc" : "Year of Birth"
            }, {
              "name" : "fatherId",
              "type" : [ "null", "int" ],
              "doc" : "refers to the pedigreeId of the father\n        Id of the parent, if unknown then no parent is referenced. Parents may need to be entered even if no data is known\n        about them in order to unambiguously reconstruct the pedigree."
            }, {
              "name" : "motherId",
              "type" : [ "null", "int" ],
              "doc" : "refers to the pedigreeId of the mother\n        Id of the parent, if unknown then no parent is referenced. Parents may need to be entered even if no data is known\n        about them in order to unambiguously reconstruct the pedigree."
            }, {
              "name" : "superFatherId",
              "type" : [ "null", "int" ],
              "doc" : "this id is built using the original familyId and the original pedigreeId of the father"
            }, {
              "name" : "superMotherId",
              "type" : [ "null", "int" ],
              "doc" : "this id is built using the original familyId and the original pedigreeId of the mother"
            }, {
              "name" : "twinGroup",
              "type" : [ "null", "int" ],
              "doc" : "Each twin group is numbered, i.e. all members of a group of multiparous births receive the same number"
            }, {
              "name" : "monozygotic",
              "type" : [ "null", {
                "type" : "enum",
                "name" : "TernaryOption",
                "doc" : "This defines a yes/no/unknown case",
                "symbols" : [ "yes", "no", "unknown" ]
              } ],
              "doc" : "A property of the twinning group but should be entered for all members"
            }, {
              "name" : "adoptedStatus",
              "type" : [ "null", {
                "type" : "enum",
                "name" : "AdoptedStatus",
                "doc" : "adoptedin means adopted into the family\n    adoptedout means child belonged to the family and was adopted out",
                "symbols" : [ "notadopted", "adoptedin", "adoptedout" ]
              } ],
              "doc" : "Adopted Status"
            }, {
              "name" : "lifeStatus",
              "type" : [ "null", {
                "type" : "enum",
                "name" : "LifeStatus",
                "doc" : "Life Status",
                "symbols" : [ "ALIVE", "ABORTED", "DECEASED", "UNBORN", "STILLBORN", "MISCARRIAGE" ]
              } ],
              "doc" : "Life Status"
            }, {
              "name" : "consanguineousParents",
              "type" : [ "null", "TernaryOption" ],
              "doc" : "The parents of this participant has a consanguineous relationship"
            }, {
              "name" : "affectionStatus",
              "type" : [ "null", {
                "type" : "enum",
                "name" : "AffectionStatus",
                "doc" : "Affection Status",
                "symbols" : [ "UNAFFECTED", "AFFECTED", "UNCERTAIN" ]
              } ],
              "doc" : "Affection Status"
            }, {
              "name" : "disorderList",
              "type" : [ "null", {
                "type" : "array",
                "items" : {
                  "type" : "record",
                  "name" : "Disorder",
                  "doc" : "This is quite GEL specific. This is the way is stored in ModelCatalogue and PanelApp.\n    Currently all specific disease titles are assigned to a disease subgroup so really only specificDisease needs to be\n    completed but we add the others for generality",
                  "fields" : [ {
                    "name" : "diseaseGroup",
                    "type" : [ "null", "string" ],
                    "doc" : "This is Level2 Title for this disorder"
                  }, {
                    "name" : "diseaseSubGroup",
                    "type" : [ "null", "string" ],
                    "doc" : "This is Level3 Title for this disorder"
                  }, {
                    "name" : "specificDisease",
                    "type" : [ "null", "string" ],
                    "doc" : "This is Level4 Title for this disorder"
                  }, {
                    "name" : "ageOfOnset",
                    "type" : [ "null", "float" ],
                    "doc" : "Age of onset in years"
                  } ]
                }
              } ],
              "doc" : "Clinical Data (disorders). If the family member is unaffected as per affectionStatus then this list is empty"
            }, {
              "name" : "hpoTermList",
              "type" : [ "null", {
                "type" : "array",
                "items" : {
                  "type" : "record",
                  "name" : "HpoTerm",
                  "doc" : "This defines an HPO term and its modifiers (possibly multiple)\n    If HPO term presence is unknown we don't have a entry on the list",
                  "fields" : [ {
                    "name" : "term",
                    "type" : "string",
                    "doc" : "Identifier of the HPO term"
                  }, {
                    "name" : "termPresence",
                    "type" : [ "null", "TernaryOption" ],
                    "doc" : "This is whether the term is present in the participant (default is unknown) yes=term is present in participant,\n        no=term is not present"
                  }, {
                    "name" : "hpoBuildNumber",
                    "type" : [ "null", "string" ],
                    "doc" : "hpoBuildNumber"
                  }, {
                    "name" : "modifiers",
                    "type" : [ "null", {
                      "type" : "record",
                      "name" : "HpoTermModifiers",
                      "fields" : [ {
                        "name" : "laterality",
                        "type" : [ "null", {
                          "type" : "enum",
                          "name" : "Laterality",
                          "symbols" : [ "RIGHT", "UNILATERAL", "BILATERAL", "LEFT" ]
                        } ]
                      }, {
                        "name" : "progression",
                        "type" : [ "null", {
                          "type" : "enum",
                          "name" : "Progression",
                          "symbols" : [ "PROGRESSIVE", "NONPROGRESSIVE" ]
                        } ]
                      }, {
                        "name" : "severity",
                        "type" : [ "null", {
                          "type" : "enum",
                          "name" : "Severity",
                          "symbols" : [ "BORDERLINE", "MILD", "MODERATE", "SEVERE", "PROFOUND" ]
                        } ]
                      }, {
                        "name" : "spatialPattern",
                        "type" : [ "null", {
                          "type" : "enum",
                          "name" : "SpatialPattern",
                          "symbols" : [ "DISTAL", "GENERALIZED", "LOCALIZED", "PROXIMAL" ]
                        } ]
                      } ]
                    } ],
                    "doc" : "Modifier associated with the HPO term"
                  }, {
                    "name" : "ageOfOnset",
                    "type" : [ "null", {
                      "type" : "enum",
                      "name" : "AgeOfOnset",
                      "symbols" : [ "EMBRYONAL_ONSET", "FETAL_ONSET", "NEONATAL_ONSET", "INFANTILE_ONSET", "CHILDHOOD_ONSET", "JUVENILE_ONSET", "YOUNG_ADULT_ONSET", "LATE_ONSET", "MIDDLE_AGE_ONSET" ]
                    } ],
                    "doc" : "Age of onset in months"
                  } ]
                }
              } ],
              "doc" : "Clinical Data (HPO terms)"
            }, {
              "name" : "ancestries",
              "type" : [ "null", {
                "type" : "record",
                "name" : "Ancestries",
                "doc" : "Ancestries, defined as Ethnic category(ies) and Chi-square test",
                "fields" : [ {
                  "name" : "mothersEthnicOrigin",
                  "type" : [ "null", {
                    "type" : "enum",
                    "name" : "EthnicCategory",
                    "doc" : "This is the list of ethnicities in ONS16\n\n    * `D`:  Mixed: White and Black Caribbean\n    * `E`:  Mixed: White and Black African\n    * `F`:  Mixed: White and Asian\n    * `G`:  Mixed: Any other mixed background\n    * `A`:  White: British\n    * `B`:  White: Irish\n    * `C`:  White: Any other White background\n    * `L`:  Asian or Asian British: Any other Asian background\n    * `M`:  Black or Black British: Caribbean\n    * `N`:  Black or Black British: African\n    * `H`:  Asian or Asian British: Indian\n    * `J`:  Asian or Asian British: Pakistani\n    * `K`:  Asian or Asian British: Bangladeshi\n    * `P`:  Black or Black British: Any other Black background\n    * `S`:  Other Ethnic Groups: Any other ethnic group\n    * `R`:  Other Ethnic Groups: Chinese\n    * `Z`:  Not stated",
                    "symbols" : [ "D", "E", "F", "G", "A", "B", "C", "L", "M", "N", "H", "J", "K", "P", "S", "R", "Z" ]
                  } ],
                  "doc" : "Mother's Ethnic Origin"
                }, {
                  "name" : "mothersOtherRelevantAncestry",
                  "type" : [ "null", "string" ],
                  "doc" : "Mother's Ethnic Origin Description"
                }, {
                  "name" : "fathersEthnicOrigin",
                  "type" : [ "null", "EthnicCategory" ],
                  "doc" : "Father's Ethnic Origin"
                }, {
                  "name" : "fathersOtherRelevantAncestry",
                  "type" : [ "null", "string" ],
                  "doc" : "Father's Ethnic Origin Description"
                }, {
                  "name" : "chiSquare1KGenomesPhase3Pop",
                  "type" : [ "null", {
                    "type" : "array",
                    "items" : {
                      "type" : "record",
                      "name" : "ChiSquare1KGenomesPhase3Pop",
                      "doc" : "Chi-square test for goodness of fit of this sample to 1000 Genomes Phase 3 populations",
                      "fields" : [ {
                        "name" : "kgSuperPopCategory",
                        "type" : {
                          "type" : "enum",
                          "name" : "KgSuperPopCategory",
                          "doc" : "1K Genomes project super populations",
                          "symbols" : [ "AFR", "AMR", "EAS", "EUR", "SAS" ]
                        },
                        "doc" : "1K Super Population"
                      }, {
                        "name" : "kgPopCategory",
                        "type" : [ "null", {
                          "type" : "enum",
                          "name" : "KgPopCategory",
                          "doc" : "1K Genomes project populations",
                          "symbols" : [ "ACB", "ASW", "BEB", "CDX", "CEU", "CHB", "CHS", "CLM", "ESN", "FIN", "GBR", "GIH", "GWD", "IBS", "ITU", "JPT", "KHV", "LWK", "MSL", "MXL", "PEL", "PJL", "PUR", "STU", "TSI", "YRI" ]
                        } ],
                        "doc" : "1K Population"
                      }, {
                        "name" : "chiSquare",
                        "type" : "double",
                        "doc" : "Chi-square test for goodness of fit of this sample to this 1000 Genomes Phase 3 population"
                      } ]
                    }
                  } ],
                  "doc" : "Chi-square test for goodness of fit of this sample to 1000 Genomes Phase 3 populations"
                } ]
              } ],
              "doc" : "Participant's ancestries, defined as Mother's/Father's Ethnic Origin and Chi-square test for goodness of fit of this sample to 1000 Genomes Phase 3 populations"
            }, {
              "name" : "consentStatus",
              "type" : [ "null", {
                "type" : "record",
                "name" : "ConsentStatus",
                "doc" : "Consent Status",
                "fields" : [ {
                  "name" : "programmeConsent",
                  "type" : "boolean",
                  "doc" : "Is this individual consented to the programme?\n        It could simply be a family member that is not consented but for whom affection status is known",
                  "default" : false
                }, {
                  "name" : "primaryFindingConsent",
                  "type" : "boolean",
                  "doc" : "Consent for feedback of primary findings?",
                  "default" : false
                }, {
                  "name" : "secondaryFindingConsent",
                  "type" : "boolean",
                  "doc" : "Consent for secondary finding lookup",
                  "default" : false
                }, {
                  "name" : "carrierStatusConsent",
                  "type" : "boolean",
                  "doc" : "Consent for carrier status check?",
                  "default" : false
                } ]
              } ],
              "doc" : "What has this participant consented to?\n        A participant that has been consented to the programme should also have sequence data associated with them; however\n        this needs to be programmatically checked"
            }, {
              "name" : "samples",
              "type" : [ "null", {
                "type" : "array",
                "items" : {
                  "type" : "record",
                  "name" : "Sample",
                  "fields" : [ {
                    "name" : "sampleId",
                    "type" : "string",
                    "doc" : "Sample Id (e.g, LP00012645_5GH))"
                  }, {
                    "name" : "labSampleId",
                    "type" : "string",
                    "doc" : "Lab Sample Id"
                  }, {
                    "name" : "source",
                    "type" : [ "null", {
                      "type" : "enum",
                      "name" : "SampleSource",
                      "doc" : "The source of the sample",
                      "symbols" : [ "TUMOUR", "BONE_MARROW_ASPIRATE_TUMOUR_SORTED_CELLS", "BONE_MARROW_ASPIRATE_TUMOUR_CELLS", "BLOOD", "SALIVA", "FIBROBLAST", "TISSUE" ]
                    } ],
                    "doc" : "Source"
                  }, {
                    "name" : "product",
                    "type" : [ "null", {
                      "type" : "enum",
                      "name" : "Product",
                      "symbols" : [ "DNA", "RNA" ]
                    } ],
                    "doc" : "Product"
                  }, {
                    "name" : "preparationMethod",
                    "type" : [ "null", {
                      "type" : "enum",
                      "name" : "PreparationMethod",
                      "symbols" : [ "EDTA", "ORAGENE", "FF", "FFPE", "CD128_SORTED_CELLS", "ASPIRATE" ]
                    } ],
                    "doc" : "preparationMethod"
                  } ]
                }
              } ],
              "doc" : "This is an array containing all the samples that belong to this individual, e.g [\"LP00002255_GA4\"]"
            }, {
              "name" : "inbreedingCoefficient",
              "type" : [ "null", {
                "type" : "record",
                "name" : "InbreedingCoefficient",
                "doc" : "Inbreeding coefficient",
                "fields" : [ {
                  "name" : "sampleId",
                  "type" : "string",
                  "doc" : "This is the sample id against which the coefficient was estimated"
                }, {
                  "name" : "program",
                  "type" : "string",
                  "doc" : "Name of program used to calculate the coefficient"
                }, {
                  "name" : "version",
                  "type" : "string",
                  "doc" : "Version of the programme"
                }, {
                  "name" : "estimationMethod",
                  "type" : "string",
                  "doc" : "Where various methods for estimation exist, which method was used."
                }, {
                  "name" : "coefficient",
                  "type" : "double",
                  "doc" : "Inbreeding coefficient ideally a real number in [0,1]"
                }, {
                  "name" : "standardError",
                  "type" : [ "null", "double" ],
                  "doc" : "Standard error of the Inbreeding coefficient"
                } ]
              } ],
              "doc" : "Inbreeding Coefficient Estimation"
            }, {
              "name" : "additionalInformation",
              "type" : [ "null", {
                "type" : "map",
                "values" : "string"
              } ],
              "doc" : "We could add a map here to store additional information for example URIs to images, ECGs, etc\n        Null by default"
            } ]
          }
        },
        "doc" : "List of members of a pedigree"
      }, {
        "name" : "analysisPanels",
        "type" : [ "null", {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "AnalysisPanel",
            "doc" : "An analysis panel",
            "fields" : [ {
              "name" : "specificDisease",
              "type" : "string",
              "doc" : "The specific disease that a panel tests"
            }, {
              "name" : "panelName",
              "type" : "string",
              "doc" : "The name of the panel"
            }, {
              "name" : "panelVersion",
              "type" : [ "null", "string" ],
              "doc" : "The version of the panel"
            }, {
              "name" : "reviewOutcome",
              "type" : "string",
              "doc" : "The outcome of the review"
            }, {
              "name" : "multipleGeneticOrigins",
              "type" : "string",
              "doc" : "TODO"
            } ]
          }
        } ],
        "doc" : "List of panels"
      }, {
        "name" : "diseasePenetrances",
        "type" : [ "null", {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "DiseasePenetrance",
            "doc" : "A disease penetrance definition",
            "fields" : [ {
              "name" : "specificDisease",
              "type" : "string",
              "doc" : "The disease to which the penetrance applies"
            }, {
              "name" : "penetrance",
              "type" : {
                "type" : "enum",
                "name" : "Penetrance",
                "doc" : "Penetrance assumed in the analysis",
                "symbols" : [ "complete", "incomplete" ]
              },
              "doc" : "The penetrance"
            } ]
          }
        } ],
        "doc" : "List of disease penetrances"
      }, {
        "name" : "readyForAnalysis",
        "type" : "boolean",
        "doc" : "Flag indicating if a pedigree is ready for analysis"
      }, {
        "name" : "familyQCState",
        "type" : [ "null", {
          "type" : "enum",
          "name" : "FamilyQCState",
          "doc" : "FamilyQCState",
          "symbols" : [ "noState", "passedMedicalReviewReadyForInterpretation", "passedMedicalReviewNotReadyForInterpretation", "queryToGel", "queryToGMC", "failed" ]
        } ],
        "doc" : "The famili quality control status"
      } ]
    } ],
    "doc" : "Pedigree of the family."
  }, {
    "name" : "otherFamilyHistory",
    "type" : [ "null", {
      "type" : "record",
      "name" : "OtherFamilyHistory",
      "doc" : "Family history for secondary findings.\n    Arrays of strings describing discrete family history phenotypes.\n    Usually: `EndocrineTumours`, `colorectal`, `BreastOvarian` and `HDOrStroke` but can be others",
      "fields" : [ {
        "name" : "maternalFamilyHistory",
        "type" : [ "null", {
          "type" : "array",
          "items" : "string"
        } ],
        "doc" : "Relevant Maternal family history"
      }, {
        "name" : "paternalFamilyHistory",
        "type" : [ "null", {
          "type" : "array",
          "items" : "string"
        } ],
        "doc" : "Relevant Maternal family history"
      } ]
    } ],
    "doc" : "It is paternal or maternal with reference to the participant."
  }, {
    "name" : "genePanelsCoverage",
    "type" : [ "null", {
      "type" : "map",
      "values" : {
        "type" : "map",
        "values" : {
          "type" : "map",
          "values" : "float"
        }
      }
    } ],
    "doc" : "This map of key: panel_name, value: (map of key: gene, value: (map of metrics of key: metric name, value: float))\n        That is: a map of tables of genes and metrics"
  }, {
    "name" : "interpretationFlags",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "InterpretationFlag",
        "doc" : "A given interpretation flag together with an optional description",
        "fields" : [ {
          "name" : "interpretationFlag",
          "type" : {
            "type" : "enum",
            "name" : "InterpretationFlags",
            "doc" : "Some flags relevant to the interpretation of a case",
            "symbols" : [ "mixed_chemistries", "mixedLab_preparation", "low_tumour_purity", "uniparental_isodisomy", "uniparental_heterodisomy", "unusual_karyotype", "high_cnv_count", "high_estimate_human_contamination_fraction", "mixed_recruiting_gmc", "suspected_mosaicism", "low_quality_sample", "ffpe_tumour_sample", "ff_nano_tumour_sample", "missing_values_for_proband_in_reported_variant", "reissued", "supplementary_report_errors", "internal_use_only", "high_priority", "suspected_increased_number_of_false_positive_heterozygous_loss_calls", "suspected_poor_quality_cnv_calls", "cnv_calls_assumed_xx_karyo", "cnv_calls_assumed_xy_karyo", "other" ]
          },
          "doc" : "The interpretation flag"
        }, {
          "name" : "additionalDescription",
          "type" : [ "null", "string" ],
          "doc" : "The description for the flag"
        } ]
      }
    } ],
    "doc" : "Flags for this case relevant for interpretation"
  }, {
    "name" : "additionalInfo",
    "type" : [ "null", {
      "type" : "map",
      "values" : "string"
    } ],
    "doc" : "Additional information"
  } ]
}
