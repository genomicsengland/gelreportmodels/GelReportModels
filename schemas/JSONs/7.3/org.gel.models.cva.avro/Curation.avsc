{
  "type" : "record",
  "name" : "Curation",
  "namespace" : "org.gel.models.cva.avro",
  "doc" : "The curation record contains all information that might be stored from a curation event.",
  "fields" : [ {
    "name" : "heritableTrait",
    "type" : {
      "type" : "record",
      "name" : "HeritableTrait",
      "namespace" : "org.opencb.biodata.models.variant.avro",
      "doc" : "The entity representing a phenotype and its inheritance pattern.",
      "fields" : [ {
        "name" : "trait",
        "type" : [ "null", "string" ],
        "doc" : "The trait (e.g.: HPO term, MIM term, DO term etc.)"
      }, {
        "name" : "inheritanceMode",
        "type" : [ "null", {
          "type" : "enum",
          "name" : "ModeOfInheritance",
          "doc" : "An enumeration for the different mode of inheritances:\n\n* `monoallelic_not_imprinted`: MONOALLELIC, autosomal or pseudoautosomal, not imprinted\n* `monoallelic_maternally_imprinted`: MONOALLELIC, autosomal or pseudoautosomal, maternally imprinted (paternal allele expressed)\n* `monoallelic_paternally_imprinted`: MONOALLELIC, autosomal or pseudoautosomal, paternally imprinted (maternal allele expressed)\n* `monoallelic`: MONOALLELIC, autosomal or pseudoautosomal, imprinted status unknown\n* `biallelic`: BIALLELIC, autosomal or pseudoautosomal\n* `monoallelic_and_biallelic`: BOTH monoallelic and biallelic, autosomal or pseudoautosomal\n* `monoallelic_and_more_severe_biallelic`: BOTH monoallelic and biallelic, autosomal or pseudoautosomal (but BIALLELIC mutations cause a more SEVERE disease form), autosomal or pseudoautosomal\n* `xlinked_biallelic`: X-LINKED: hemizygous mutation in males, biallelic mutations in females\n* `xlinked_monoallelic`: X linked: hemizygous mutation in males, monoallelic mutations in females may cause disease (may be less severe, later onset than males)\n* `mitochondrial`: MITOCHONDRIAL\n* `unknown`: Unknown\n* `NA`: Not applicable",
          "symbols" : [ "monoallelic", "monoallelic_not_imprinted", "monoallelic_maternally_imprinted", "monoallelic_paternally_imprinted", "biallelic", "monoallelic_and_biallelic", "monoallelic_and_more_severe_biallelic", "xlinked_biallelic", "xlinked_monoallelic", "mitochondrial", "unknown", "NA" ]
        } ],
        "doc" : "The mode of inheritance"
      } ]
    },
    "doc" : "The heritable phenotype to which the curation refers."
  }, {
    "name" : "genomicFeature",
    "type" : [ "null", {
      "type" : "record",
      "name" : "GenomicFeature",
      "namespace" : "org.opencb.biodata.models.variant.avro",
      "doc" : "The genomic feature",
      "fields" : [ {
        "name" : "featureType",
        "type" : [ "null", {
          "type" : "enum",
          "name" : "FeatureTypes",
          "doc" : "The feature types",
          "symbols" : [ "regulatory_region", "gene", "transcript", "protein" ]
        } ],
        "doc" : "Feature Type"
      }, {
        "name" : "ensemblId",
        "type" : [ "null", "string" ],
        "doc" : "Feature used, this should be a feature ID from Ensembl, (i.e, ENST00000544455)"
      }, {
        "name" : "xrefs",
        "type" : [ "null", {
          "type" : "map",
          "values" : "string"
        } ],
        "doc" : "Others IDs. Fields like the HGNC symbol if available should be added here"
      } ]
    } ],
    "doc" : "The transcript to which the curation refers"
  }, {
    "name" : "assembly",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "Assembly",
      "namespace" : "org.gel.models.report.avro",
      "doc" : "The reference genome assembly",
      "symbols" : [ "GRCh38", "GRCh37" ]
    } ],
    "doc" : "The assembly to which the curation refers"
  }, {
    "name" : "variantClassification",
    "type" : [ "null", {
      "type" : "record",
      "name" : "VariantClassification",
      "namespace" : "org.opencb.biodata.models.variant.avro",
      "doc" : "The variant classification according to different properties.",
      "fields" : [ {
        "name" : "clinicalSignificance",
        "type" : [ "null", {
          "type" : "enum",
          "name" : "ClinicalSignificance",
          "doc" : "Mendelian variants classification with ACMG terminology as defined in Richards, S. et al. (2015). Standards and\n        guidelines for the interpretation of sequence variants: a joint consensus recommendation of the American College\n        of Medical Genetics and Genomics and the Association for Molecular Pathology. Genetics in Medicine, 17(5),\n        405–423. https://doi.org/10.1038/gim.2015.30.\n\n    Classification for pharmacogenomic variants, variants associated to\n    disease and somatic variants based on the ACMG recommendations and ClinVar classification\n    (https://www.ncbi.nlm.nih.gov/clinvar/docs/clinsig/).\n\n* `benign_variant` : Benign variants interpreted for Mendelian disorders\n* `likely_benign_variant` : Likely benign variants interpreted for Mendelian disorders with a certainty of at least 90%\n* `pathogenic_variant` : Pathogenic variants interpreted for Mendelian disorders\n* `likely_pathogenic_variant` : Likely pathogenic variants interpreted for Mendelian disorders with a certainty of at\nleast 90%\n* `uncertain_significance` : Uncertain significance variants interpreted for Mendelian disorders. Variants with\nconflicting evidences should be classified as uncertain_significance",
          "symbols" : [ "benign", "likely_benign", "VUS", "likely_pathogenic", "pathogenic", "uncertain_significance" ]
        } ],
        "doc" : "The variant's clinical significance."
      }, {
        "name" : "drugResponseClassification",
        "type" : [ "null", {
          "type" : "enum",
          "name" : "DrugResponseClassification",
          "doc" : "Pharmacogenomics drug response variant classification\n* `responsive` : A variant that confers response to a treatment\n* `resistant` : A variant that confers resistance to a treatment\n* `toxicity` : A variant that is associated with drug-induced toxicity\n* `indication` : A variant that is required in order for a particular drug to be prescribed\n* `contraindication` : A variant that if present, a particular drug should not be prescribed\n* `dosing` : A variant that results in an alteration in dosing of a particular drug in order to achieve INR, reduce toxicity or increase efficacy\n* `increased_monitoring` : increase vigilance or increased dosage monitoring may be required for a patient with this variant to look for signs of adverse drug reactions\n* `efficacy` : a variant that affects the efficacy of the treatment",
          "symbols" : [ "responsive", "resistant", "toxicity", "indication", "contraindication", "dosing", "increased_monitoring", "efficacy" ]
        } ],
        "doc" : "The variant's pharmacogenomics classification."
      }, {
        "name" : "traitAssociation",
        "type" : [ "null", {
          "type" : "enum",
          "name" : "TraitAssociation",
          "doc" : "Association of variants to a given trait.\n* `established_risk_allele` : Established risk allele for variants associated to disease\n* `likely_risk_allele` : Likely risk allele for variants associated to disease\n* `uncertain_risk_allele` : Uncertain risk allele for variants associated to disease\n* `protective` : Protective allele",
          "symbols" : [ "established_risk_allele", "likely_risk_allele", "uncertain_risk_allele", "protective" ]
        } ],
        "doc" : "The variant's trait association."
      }, {
        "name" : "tumorigenesisClassification",
        "type" : [ "null", {
          "type" : "enum",
          "name" : "TumorigenesisClassification",
          "doc" : "Variant classification according to its relation to cancer aetiology.\n* `driver` : Driver variants\n* `passenger` : Passenger variants\n* `modifier` : Modifier variants",
          "symbols" : [ "driver", "passenger", "modifier" ]
        } ],
        "doc" : "The variant's tumorigenesis classification."
      }, {
        "name" : "functionalEffect",
        "type" : [ "null", {
          "type" : "enum",
          "name" : "VariantFunctionalEffect",
          "doc" : "Variant effect with Sequence Ontology terms.\n\n* `SO_0002052`: dominant_negative_variant (http://purl.obolibrary.org/obo/SO_0002052)\n* `SO_0002053`: gain_of_function_variant (http://purl.obolibrary.org/obo/SO_0002053)\n* `SO_0001773`: lethal_variant (http://purl.obolibrary.org/obo/SO_0001773)\n* `SO_0002054`: loss_of_function_variant (http://purl.obolibrary.org/obo/SO_0002054)\n* `SO_0001786`: loss_of_heterozygosity (http://purl.obolibrary.org/obo/SO_0001786)\n* `SO_0002055`: null_variant (http://purl.obolibrary.org/obo/SO_0002055)",
          "symbols" : [ "dominant_negative_variant", "gain_of_function_variant", "lethal_variant", "loss_of_function_variant", "loss_of_heterozygosity", "null_variant" ]
        } ],
        "doc" : "The variant functional effect"
      } ]
    } ],
    "doc" : "The variant's classification."
  }, {
    "name" : "confidence",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "Confidence",
      "namespace" : "org.opencb.biodata.models.variant.avro",
      "doc" : "Confidence based on the Confidence Information Ontology\n\n* `CIO_0000029`: high confidence level http://purl.obolibrary.org/obo/CIO_0000029\n* `CIO_0000031`: low confidence level http://purl.obolibrary.org/obo/CIO_0000031\n* `CIO_0000030`: medium confidence level http://purl.obolibrary.org/obo/CIO_0000030\n* `CIO_0000039`: rejected http://purl.obolibrary.org/obo/CIO_0000039",
      "symbols" : [ "low_confidence_level", "medium_confidence_level", "high_confidence_level", "rejected" ]
    } ],
    "doc" : "The curation confidence."
  }, {
    "name" : "automaticConsistencyStatus",
    "type" : {
      "type" : "enum",
      "name" : "ConsistencyStatus",
      "namespace" : "org.opencb.biodata.models.variant.avro",
      "doc" : "The consistency of evidences for a given phenotype. This aggregates all evidences for a given phenotype and all\n    evidences with no phenotype associated (e.g.: in silico impact prediction, population frequency).\n    This is based on the Confidence Information Ontology terms.\n\n* `CIO_0000033`: congruent, all evidences are consistent. http://purl.obolibrary.org/obo/CIO_0000033\n* `CIO_0000034`: conflict, there are conflicting evidences. This should correspond to a `VariantClassification` of\n`uncertain_significance` for mendelian disorders. http://purl.obolibrary.org/obo/CIO_0000034\n* `CIO_0000035`: strongly conflicting. http://purl.obolibrary.org/obo/CIO_0000035\n* `CIO_0000036`: weakly conflicting. http://purl.obolibrary.org/obo/CIO_0000036",
      "symbols" : [ "congruent", "conflict", "weakly_conflicting", "strongly_conflicting" ]
    },
    "doc" : "The automatic consistency status. The value is automatically inferred from evidences."
  }, {
    "name" : "manualConsistencyStatus",
    "type" : [ "null", "org.opencb.biodata.models.variant.avro.ConsistencyStatus" ],
    "doc" : "The manual consistency status. The value is optionally provided by a curator."
  }, {
    "name" : "penetrance",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "Penetrance",
      "namespace" : "org.opencb.biodata.models.variant.avro",
      "doc" : "Penetrance assumed in the analysis",
      "symbols" : [ "complete", "incomplete" ]
    } ],
    "doc" : "The penetrance of the phenotype for this genotype. Value in the range [0, 1]"
  }, {
    "name" : "variableExpressivity",
    "type" : [ "null", "boolean" ],
    "doc" : "Variable expressivity of a given phenotype for the same genotype"
  }, {
    "name" : "reportableAsSecondaryFinding",
    "type" : [ "null", "boolean" ],
    "doc" : "Can this variant be reported as a secondary finding?"
  }, {
    "name" : "actionable",
    "type" : [ "null", "boolean" ],
    "doc" : "Is this variant actionable?"
  }, {
    "name" : "confirmed",
    "type" : [ "null", "boolean" ],
    "doc" : "Confirmation flag to support two-step curation"
  }, {
    "name" : "additionalProperties",
    "type" : {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "Property",
        "namespace" : "org.opencb.biodata.models.variant.avro",
        "doc" : "A property in the form of name-value pair.\n    Names are restricted to ontology ids, they should be checked against existing ontologies in resources like\n    Ontology Lookup Service.",
        "fields" : [ {
          "name" : "id",
          "type" : [ "null", "string" ],
          "doc" : "The ontology term id or accession in OBO format ${ONTOLOGY_ID}:${TERM_ID} (http://www.obofoundry.org/id-policy.html)"
        }, {
          "name" : "name",
          "type" : [ "null", "string" ],
          "doc" : "The ontology term name"
        }, {
          "name" : "value",
          "type" : [ "null", "string" ],
          "doc" : "Optional value for the ontology term, the type of the value is not checked\n        (i.e.: we could set the pvalue term to \"significant\" or to \"0.0001\")"
        } ]
      }
    },
    "doc" : "A list of additional properties in the form name-value.",
    "default" : [ ]
  }, {
    "name" : "bibliography",
    "type" : {
      "type" : "array",
      "items" : "string"
    },
    "doc" : "Bibliography",
    "default" : [ ]
  } ]
}
