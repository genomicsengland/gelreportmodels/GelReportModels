{
  "type" : "record",
  "name" : "VariantFileMetadata",
  "namespace" : "org.opencb.biodata.models.variant.metadata",
  "fields" : [ {
    "name" : "id",
    "type" : "string",
    "doc" : "File id. Will match with the {@link org.opencb.biodata.models.variant.avro.FileEntry#getFileId}"
  }, {
    "name" : "path",
    "type" : [ "null", "string" ],
    "doc" : "Path to the original file",
    "default" : null
  }, {
    "name" : "sampleIds",
    "type" : {
      "type" : "array",
      "items" : "string"
    },
    "doc" : "Ordered list of sample ids contained in the file",
    "default" : [ ]
  }, {
    "name" : "stats",
    "type" : [ "null", {
      "type" : "record",
      "name" : "VariantSetStats",
      "doc" : "Variant statistics for a set of variants.\n     The variants set can be contain a whole study, a cohort, a sample, a region, ...",
      "fields" : [ {
        "name" : "numVariants",
        "type" : "int",
        "doc" : "Number of variants in the variants set"
      }, {
        "name" : "numSamples",
        "type" : "int",
        "doc" : "Number of samples in the variants set"
      }, {
        "name" : "numPass",
        "type" : "int",
        "doc" : "Number of variants with PASS filter"
      }, {
        "name" : "tiTvRatio",
        "type" : "float",
        "doc" : "TiTvRatio = num. transitions / num. transversions"
      }, {
        "name" : "meanQuality",
        "type" : "float",
        "doc" : "Mean Quality for all the variants with quality"
      }, {
        "name" : "stdDevQuality",
        "type" : "float",
        "doc" : "Standard Deviation of the quality"
      }, {
        "name" : "numRareVariants",
        "type" : {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "VariantsByFrequency",
            "doc" : "Counts the number of variants within a certain frequency range.",
            "fields" : [ {
              "name" : "startFrequency",
              "type" : "float",
              "doc" : "Inclusive frequency range start"
            }, {
              "name" : "endFrequency",
              "type" : "float",
              "doc" : "Exclusive frequency range end"
            }, {
              "name" : "count",
              "type" : "int",
              "doc" : "Number of variants with this frequency"
            } ]
          }
        },
        "doc" : "array of elements to classify variants according to their 'rarity'\n         Typical frequency ranges:\n          - very rare     -> from 0 to 0.001\n          - rare          -> from 0.001 to 0.005\n          - low frequency -> from 0.005 to 0.05\n          - common        -> from 0.05",
        "default" : [ ]
      }, {
        "name" : "variantTypeCounts",
        "type" : {
          "type" : "map",
          "values" : "int"
        },
        "doc" : "Variants count group by type. e.g. SNP, INDEL, MNP, SNV, ...",
        "default" : { }
      }, {
        "name" : "variantBiotypeCounts",
        "type" : {
          "type" : "map",
          "values" : "int"
        },
        "doc" : "Variants count group by biotype. e.g. protein-coding, miRNA, lncRNA, ...",
        "default" : { }
      }, {
        "name" : "consequenceTypesCounts",
        "type" : {
          "type" : "map",
          "values" : "int"
        },
        "doc" : "Variants count group by consequence type. e.g. synonymous_variant, missense_variant, stop_lost, ...",
        "default" : { }
      }, {
        "name" : "chromosomeStats",
        "type" : {
          "type" : "map",
          "values" : {
            "type" : "record",
            "name" : "ChromosomeStats",
            "fields" : [ {
              "name" : "count",
              "type" : "int",
              "doc" : "Number of variants within this chromosome"
            }, {
              "name" : "density",
              "type" : "float",
              "doc" : "Total density of variants within the chromosome. counts / chromosome.length"
            } ]
          }
        },
        "doc" : "Statistics per chromosome.",
        "default" : { }
      } ]
    } ],
    "doc" : "Global statistics calculated for this file",
    "default" : null
  }, {
    "name" : "header",
    "type" : [ "null", {
      "type" : "record",
      "name" : "VariantFileHeader",
      "doc" : "Variant File Header. Contains simple and complex metadata lines describing the content of the file.\n    This header matches with the VCF header.\n    A header may have multiple Simple or Complex lines with the same key",
      "fields" : [ {
        "name" : "version",
        "type" : "string"
      }, {
        "name" : "complexLines",
        "type" : {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "VariantFileHeaderComplexLine",
            "fields" : [ {
              "name" : "key",
              "type" : "string",
              "doc" : "Key of group of the Complex Header Line, e.g. INFO, FORMAT, FILTER, ALT, ..."
            }, {
              "name" : "id",
              "type" : "string",
              "doc" : "ID or Name of the line"
            }, {
              "name" : "description",
              "type" : [ "null", "string" ],
              "doc" : "The description",
              "default" : null
            }, {
              "name" : "number",
              "type" : [ "null", "string" ],
              "doc" : "Arity of the values associated with this metadata line.\n        Only present if the metadata line describes data fields, i.e. key == INFO or FORMAT\n        Accepted values:\n          - <Integer>: The field has always this number of values.\n          - A: The field has one value per alternate allele.\n          - R: The field has one value for each possible allele, including the reference.\n          - G: The field has one value for each possible genotype\n          - .: The number of possible values varies, is unknown or unbounded.",
              "default" : null
            }, {
              "name" : "type",
              "type" : [ "null", "string" ],
              "doc" : "Type of the values associated with this metadata line.\n        Only present if the metadata line describes data fields, i.e. key == INFO or FORMAT\n        Accepted values:\n          - Integer\n          - Float\n          - String\n          - Character\n          - Flag",
              "default" : null
            }, {
              "name" : "genericFields",
              "type" : {
                "type" : "map",
                "values" : "string"
              },
              "doc" : "Other optional fields",
              "default" : { }
            } ]
          }
        },
        "doc" : "complex lines, e.g. INFO=<ID=NS,Number=1,Type=Integer,Description=\"Number of samples with data\">",
        "default" : [ ]
      }, {
        "name" : "simpleLines",
        "type" : {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "VariantFileHeaderSimpleLine",
            "fields" : [ {
              "name" : "key",
              "type" : "string",
              "doc" : "Key of group of the Simple Header Line, e.g. source, assembly, pedigreeDB, ..."
            }, {
              "name" : "value",
              "type" : "string",
              "doc" : "Value"
            } ]
          }
        },
        "doc" : "simple lines, e.g. fileDate=20090805",
        "default" : [ ]
      } ]
    } ],
    "doc" : "The Variant File Header",
    "default" : null
  }, {
    "name" : "attributes",
    "type" : {
      "type" : "map",
      "values" : "string"
    },
    "doc" : "Other user defined attributes related with the file",
    "default" : { }
  } ]
}
