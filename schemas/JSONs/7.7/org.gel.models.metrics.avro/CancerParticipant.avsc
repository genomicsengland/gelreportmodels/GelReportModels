{
  "type" : "record",
  "name" : "CancerParticipant",
  "namespace" : "org.gel.models.participant.avro",
  "doc" : "This defines a Cancer Participant",
  "fields" : [ {
    "name" : "yearOfBirth",
    "type" : [ "null", "int" ],
    "doc" : "Year of birth for the cancer participant"
  }, {
    "name" : "morphology",
    "type" : [ "null", {
      "type" : "array",
      "items" : "string"
    } ],
    "doc" : "Morphology of any tumours\n        NOTE: In GMS, this field is deprecated as morphology will be in sample or with respect to a tumour record"
  }, {
    "name" : "readyForAnalysis",
    "type" : "boolean",
    "doc" : "Flag indicating if the participant is ready for analysis\n        NOTE: In GMS, this will default to true"
  }, {
    "name" : "consentStatus",
    "type" : [ "null", {
      "type" : "record",
      "name" : "ConsentStatus",
      "doc" : "Consent Status for 100k program",
      "fields" : [ {
        "name" : "programmeConsent",
        "type" : "boolean",
        "doc" : "Is this individual consented to the programme?\n        It could simply be a family member that is not consented but for whom affection status is known",
        "default" : false
      }, {
        "name" : "primaryFindingConsent",
        "type" : "boolean",
        "doc" : "Consent for feedback of primary findings?",
        "default" : false
      }, {
        "name" : "secondaryFindingConsent",
        "type" : "boolean",
        "doc" : "Consent for secondary finding lookup",
        "default" : false
      }, {
        "name" : "carrierStatusConsent",
        "type" : "boolean",
        "doc" : "Consent for carrier status check?",
        "default" : false
      } ]
    } ],
    "doc" : "What has this participant consented to?\n        A participant that has been consented to the programme should also have sequence data associated with them; however\n        this needs to be programmatically checked"
  }, {
    "name" : "testConsentStatus",
    "type" : [ "null", {
      "type" : "record",
      "name" : "GmsConsentStatus",
      "doc" : "Consent Status for GMS",
      "fields" : [ {
        "name" : "programmeConsent",
        "type" : {
          "type" : "enum",
          "name" : "GenericConsent",
          "doc" : "clinicalEthnicities supersedes Ancestries in GMS",
          "symbols" : [ "yes", "no", "undefined", "not_applicable" ]
        },
        "doc" : "Is this individual consented to the programme? It could simply be a family member that is not consented\n        but for whom affection status is known"
      }, {
        "name" : "primaryFindingConsent",
        "type" : "GenericConsent",
        "doc" : "Consent for feedback of primary findings?\n        RD: Primary Findings\n        Cancer: PrimaryFindings is somatic + pertinent germline findings"
      }, {
        "name" : "researchConsent",
        "type" : "GenericConsent",
        "doc" : "Research Consent"
      }, {
        "name" : "healthRelatedFindingConsent",
        "type" : "GenericConsent",
        "doc" : "Consent for secondary health related findings?"
      }, {
        "name" : "carrierStatusConsent",
        "type" : "GenericConsent",
        "doc" : "Consent for carrier status check?"
      }, {
        "name" : "pharmacogenomicsFindingConsent",
        "type" : "GenericConsent",
        "doc" : "Consent for pharmacogenomics consent as secondary findings?"
      } ]
    } ],
    "doc" : "What has this participant consented in the context of a Genomic Test?"
  }, {
    "name" : "center",
    "type" : [ "null", "string" ],
    "doc" : "Center\n        NOTE: In GMS, this will be taken from the ReferralTest and duplicated here"
  }, {
    "name" : "individualId",
    "type" : [ "null", "string" ],
    "doc" : "Individual identifier"
  }, {
    "name" : "participantId",
    "type" : [ "null", "string" ],
    "doc" : "This is a human readable participant ID"
  }, {
    "name" : "primaryDiagnosisDisease",
    "type" : [ "null", {
      "type" : "array",
      "items" : "string"
    } ],
    "doc" : "This should be an enumeration when it is well defined\n        blood, breast, prostate, colorectal, cll, aml, renal, ovarian, skin, lymphNode, bone, saliva //for individual - there could be more than I have listed here, in fact there definitely will.\n        In GMS, this field is deprecated"
  }, {
    "name" : "primaryDiagnosisSubDisease",
    "type" : [ "null", {
      "type" : "array",
      "items" : "string"
    } ],
    "doc" : "This should be an enumeration when it is well defined\n        blood, breast, prostate, colorectal, cll, aml, renal, ovarian, skin, lymphNode, bone, saliva //for individual - there could be more than I have listed here, in fact there definitely will.\n        In GMS, this field is deprecated"
  }, {
    "name" : "sex",
    "type" : {
      "type" : "enum",
      "name" : "Sex",
      "doc" : "Sex",
      "symbols" : [ "MALE", "FEMALE", "UNKNOWN" ]
    },
    "doc" : "Sex"
  }, {
    "name" : "additionalInformation",
    "type" : [ "null", {
      "type" : "map",
      "values" : "string"
    } ],
    "doc" : "We could add a map here to store additional information for example URIs to images, ECGs, etc"
  }, {
    "name" : "assignedICD10",
    "type" : [ "null", {
      "type" : "array",
      "items" : "string"
    } ],
    "doc" : "assigned ICD10 code\n        IN GMS, this is deprecated"
  }, {
    "name" : "tumourSamples",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "TumourSample",
        "doc" : "A tumour sample",
        "fields" : [ {
          "name" : "sampleId",
          "type" : "string",
          "doc" : "Sample identifier (e.g, LP00012645_5GH))"
        }, {
          "name" : "labSampleId",
          "type" : "string",
          "doc" : "Lab sample identifier"
        }, {
          "name" : "LDPCode",
          "type" : [ "null", "string" ],
          "doc" : "LDP Code (Local Delivery Partner)"
        }, {
          "name" : "tumourId",
          "type" : [ "null", "string" ],
          "doc" : "This is the ID of the tumour from which this tumour sample was taken from"
        }, {
          "name" : "programmePhase",
          "type" : [ "null", "string" ],
          "doc" : "Genomics England programme phase"
        }, {
          "name" : "diseaseType",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "diseaseType",
            "symbols" : [ "ADULT_GLIOMA", "BLADDER", "BREAST", "CARCINOMA_OF_UNKNOWN_PRIMARY", "CHILDHOOD", "COLORECTAL", "ENDOCRINE", "ENDOMETRIAL_CARCINOMA", "HAEMONC", "HEPATOPANCREATOBILIARY", "LUNG", "MALIGNANT_MELANOMA", "NASOPHARYNGEAL", "ORAL_OROPHARYNGEAL", "OVARIAN", "PROSTATE", "RENAL", "SARCOMA", "SINONASAL", "TESTICULAR_GERM_CELL_TUMOURS", "UPPER_GASTROINTESTINAL", "OTHER", "NON_HODGKINS_B_CELL_LYMPHOMA_LOW_MOD_GRADE", "CLASSICAL_HODGKINS", "NODULAR_LYMPHOCYTE_PREDOMINANT_HODGKINS", "T_CELL_LYMPHOMA" ]
          } ],
          "doc" : "Disease type.\n        NOTE: Deprecated in GMS"
        }, {
          "name" : "diseaseSubType",
          "type" : [ "null", "string" ],
          "doc" : "Disease subtype.\n        NOTE: Deprecated in GMS"
        }, {
          "name" : "haematologicalCancer",
          "type" : [ "null", "boolean" ],
          "doc" : "True or false if this sample is of type: Haematological Cancer"
        }, {
          "name" : "haematologicalCancerLineage",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "HaematologicalCancerLineage",
            "symbols" : [ "MYELOID", "LYMPHOID", "UNKNOWN" ]
          } ],
          "doc" : "This is the Haematological cancer lineage of the tumourSample if this sample is from a haematological cancer"
        }, {
          "name" : "clinicalSampleDateTime",
          "type" : [ "null", "string" ],
          "doc" : "The time when the sample was received. In the format YYYY-MM-DDTHH:MM:SS+0000"
        }, {
          "name" : "tumourType",
          "type" : [ "null", "string" ],
          "doc" : "Tumor type.\n        NOTE: Deprecated in GMS in tumourSample but available in tumour record"
        }, {
          "name" : "tumourContent",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "TumourContent",
            "symbols" : [ "High", "Medium", "Low" ]
          } ],
          "doc" : "This is the tumour content"
        }, {
          "name" : "tumourContentPercentage",
          "type" : [ "null", "float" ],
          "doc" : "This is the tumour content percentage"
        }, {
          "name" : "source",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "SampleSource",
            "doc" : "The source of the sample\n    NOTE: IN GMS, BONE_MARROW_ASPIRATE_TUMOUR_CELLS and BONE_MARROW_ASPIRATE_TUMOUR_SORTED_CELLS are deprecated as they have been separated into their respective biotypes",
            "symbols" : [ "AMNIOTIC_FLUID", "BLOOD", "BONE_MARROW", "BONE_MARROW_ASPIRATE_TUMOUR_CELLS", "BONE_MARROW_ASPIRATE_TUMOUR_SORTED_CELLS", "BUCCAL_SWAB", "CHORIONIC_VILLUS_SAMPLE", "FIBROBLAST", "FLUID", "FRESH_TISSUE_IN_CULTURE_MEDIUM", "OTHER", "SALIVA", "TISSUE", "TUMOUR", "URINE" ]
          } ],
          "doc" : "Source of the sample"
        }, {
          "name" : "preparationMethod",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "PreparationMethod",
            "doc" : "In 100K, preparation Method of sample\n    NOTE: In GMS, this field is deprecated in favour of StorageMedium and Method",
            "symbols" : [ "ASPIRATE", "CD128_SORTED_CELLS", "CD138_SORTED_CELLS", "EDTA", "FF", "FFPE", "LI_HEP", "ORAGENE" ]
          } ],
          "doc" : "The preparation method of the sample\n        NOTE: Deprecated in GMS in replace of Method and storageMedium record"
        }, {
          "name" : "tissueSource",
          "type" : [ "null", "string" ],
          "doc" : "The tissue source of the sample.\n        NOTE: DEPRECATED IN GMS in replace of method record"
        }, {
          "name" : "product",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "Product",
            "symbols" : [ "DNA", "RNA" ]
          } ],
          "doc" : "Product of the sample"
        }, {
          "name" : "sampleMorphologies",
          "type" : [ "null", {
            "type" : "array",
            "items" : {
              "type" : "record",
              "name" : "Morphology",
              "fields" : [ {
                "name" : "id",
                "type" : [ "null", "string" ],
                "doc" : "The ontology term id or accession in OBO format ${ONTOLOGY_ID}:${TERM_ID} (http://www.obofoundry.org/id-policy.html)"
              }, {
                "name" : "name",
                "type" : [ "null", "string" ],
                "doc" : "The ontology term name"
              }, {
                "name" : "value",
                "type" : [ "null", "string" ],
                "doc" : "Optional value for the ontology term, the type of the value is not checked\n        (i.e.: we could set the pvalue term to \"significant\" or to \"0.0001\")"
              }, {
                "name" : "version",
                "type" : [ "null", "string" ],
                "doc" : "Ontology version"
              } ]
            }
          } ],
          "doc" : "Morphology according to the sample taken"
        }, {
          "name" : "sampleTopographies",
          "type" : [ "null", {
            "type" : "array",
            "items" : {
              "type" : "record",
              "name" : "Topography",
              "fields" : [ {
                "name" : "id",
                "type" : [ "null", "string" ],
                "doc" : "The ontology term id or accession in OBO format ${ONTOLOGY_ID}:${TERM_ID} (http://www.obofoundry.org/id-policy.html)"
              }, {
                "name" : "name",
                "type" : [ "null", "string" ],
                "doc" : "The ontology term name"
              }, {
                "name" : "value",
                "type" : [ "null", "string" ],
                "doc" : "Optional value for the ontology term, the type of the value is not checked\n        (i.e.: we could set the pvalue term to \"significant\" or to \"0.0001\")"
              }, {
                "name" : "version",
                "type" : [ "null", "string" ],
                "doc" : "Ontology version"
              } ]
            }
          } ],
          "doc" : "Topography according to the sample taken"
        }, {
          "name" : "sampleUid",
          "type" : [ "null", "string" ],
          "doc" : "In GMS, this is the GUID of the sample"
        }, {
          "name" : "participantId",
          "type" : [ "null", "string" ],
          "doc" : "Participant Id of the sample"
        }, {
          "name" : "participantUid",
          "type" : [ "null", "string" ],
          "doc" : "Participant UId of the sample"
        }, {
          "name" : "maskedPid",
          "type" : [ "null", "string" ],
          "doc" : "In GMS, this is the maskedPID"
        }, {
          "name" : "method",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "Method",
            "doc" : "In GMS, Method is defined as how the sample was taken directly from the patient",
            "symbols" : [ "ASPIRATE", "BIOPSY", "NOT_APPLICABLE", "RESECTION", "SORTED_OTHER", "UNKNOWN", "UNSORTED", "CD138_SORTED" ]
          } ],
          "doc" : "In GMS, this is how the sample was extracted from the participant"
        }, {
          "name" : "storageMedium",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "StorageMedium",
            "doc" : "In GMS, storage medium of sample",
            "symbols" : [ "EDTA", "FF", "LI_HEP", "ORAGENE", "FFPE" ]
          } ],
          "doc" : "In GMS, this is what solvent/medium the sample was stored in"
        }, {
          "name" : "sampleType",
          "type" : [ "null", "string" ],
          "doc" : "In GMS, this is the sampleType as entered by the clinician in TOMs"
        }, {
          "name" : "sampleState",
          "type" : [ "null", "string" ],
          "doc" : "In GMS, this is the sampleState as entered by the clinician in TOMs"
        } ]
      }
    } ],
    "doc" : "List of tumour samples\n        IN GMS, this is deprecated and moved to ReferralTest"
  }, {
    "name" : "germlineSamples",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "GermlineSample",
        "doc" : "A germline sample",
        "fields" : [ {
          "name" : "sampleId",
          "type" : "string",
          "doc" : "Sample identifier (e.g, LP00012645_5GH))"
        }, {
          "name" : "labSampleId",
          "type" : "string",
          "doc" : "Lab sample identifier"
        }, {
          "name" : "LDPCode",
          "type" : [ "null", "string" ],
          "doc" : "LDP Code (Local Delivery Partner)"
        }, {
          "name" : "source",
          "type" : [ "null", "SampleSource" ],
          "doc" : "Source of the sample"
        }, {
          "name" : "product",
          "type" : [ "null", "Product" ],
          "doc" : "Product of the sample"
        }, {
          "name" : "preparationMethod",
          "type" : [ "null", "PreparationMethod" ],
          "doc" : "Preparation method\n        NOTE: In GMS, this has been deprecated in favour of Method and storageMedium"
        }, {
          "name" : "programmePhase",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "ProgrammePhase",
            "symbols" : [ "CRUK", "OXFORD", "CLL", "IIP", "MAIN", "EXPT" ]
          } ],
          "doc" : "Genomics England programme phase"
        }, {
          "name" : "clinicalSampleDateTime",
          "type" : [ "null", "string" ],
          "doc" : "The time when the sample was received. In the format YYYY-MM-DDTHH:MM:SS+0000"
        }, {
          "name" : "participantId",
          "type" : [ "null", "string" ]
        }, {
          "name" : "participantUid",
          "type" : [ "null", "string" ],
          "doc" : "Participant UId of the sample"
        }, {
          "name" : "sampleUid",
          "type" : [ "null", "string" ]
        }, {
          "name" : "maskedPid",
          "type" : [ "null", "string" ]
        }, {
          "name" : "method",
          "type" : [ "null", "Method" ],
          "doc" : "In GMS, this is how the sample was extracted from the participant"
        }, {
          "name" : "storageMedium",
          "type" : [ "null", "StorageMedium" ],
          "doc" : "In GMS, this is what solvent/medium the sample was stored in"
        }, {
          "name" : "sampleType",
          "type" : [ "null", "string" ],
          "doc" : "In GMS, this is the sampleType as entered by the clinician in TOMs"
        }, {
          "name" : "sampleState",
          "type" : [ "null", "string" ],
          "doc" : "In GMS, this is the sampleState as entered by the clinician in TOMs"
        } ]
      }
    } ],
    "doc" : "List of germline samples\n        IN GMS, this is deprecated and moved to ReferralTest"
  }, {
    "name" : "versionControl",
    "type" : [ "null", {
      "type" : "record",
      "name" : "VersionControl",
      "fields" : [ {
        "name" : "GitVersionControl",
        "type" : "string",
        "doc" : "This is the version for the entire set of data models as referred to the Git release tag",
        "default" : "1.3.0"
      } ]
    } ],
    "doc" : "Model version number"
  }, {
    "name" : "participantUid",
    "type" : [ "null", "string" ],
    "doc" : "Individual UID in GMS"
  }, {
    "name" : "tumours",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "Tumour",
        "fields" : [ {
          "name" : "tumourId",
          "type" : "string",
          "doc" : "TumourId in GMS"
        }, {
          "name" : "tumourLocalId",
          "type" : "string",
          "doc" : "Local hospital tumour ID from the GLH Laboratory Information Management System (LIMS) in GMS"
        }, {
          "name" : "tumourType",
          "type" : {
            "type" : "enum",
            "name" : "TumourType",
            "doc" : "NOTE: This has been changed completely, the previous tumour type has been split into TumourPresentation and PrimaryOrMetastatic",
            "symbols" : [ "BRAIN_TUMOUR", "HAEMATOLOGICAL_MALIGNANCY_SOLID_SAMPLE", "HAEMATOLOGICAL_MALIGNANCY_LIQUID_SAMPLE", "SOLID_TUMOUR_METASTATIC", "SOLID_TUMOUR_PRIMARY", "SOLID_TUMOUR", "UNKNOWN" ]
          },
          "doc" : "tumourType"
        }, {
          "name" : "tumourParentId",
          "type" : [ "null", "string" ],
          "doc" : "Parent Tumour UID if this tumour is metastatic"
        }, {
          "name" : "tumourDiagnosisDate",
          "type" : [ "null", {
            "type" : "record",
            "name" : "Date",
            "doc" : "This defines a date record",
            "fields" : [ {
              "name" : "year",
              "type" : "int",
              "doc" : "Format YYYY"
            }, {
              "name" : "month",
              "type" : [ "null", "int" ],
              "doc" : "Format MM. e.g June is 06"
            }, {
              "name" : "day",
              "type" : [ "null", "int" ],
              "doc" : "Format DD e.g. 12th of October is 12"
            } ]
          } ],
          "doc" : "Date of Diagnosis of the specific tumour"
        }, {
          "name" : "tumourDescription",
          "type" : [ "null", "string" ],
          "doc" : "Description of the tumour"
        }, {
          "name" : "tumourMorphologies",
          "type" : [ "null", {
            "type" : "array",
            "items" : "Morphology"
          } ],
          "doc" : "Morphology of the tumour"
        }, {
          "name" : "tumourTopographies",
          "type" : [ "null", {
            "type" : "array",
            "items" : "Topography"
          } ],
          "doc" : "Topography of the tumour"
        }, {
          "name" : "tumourPrimaryTopographies",
          "type" : [ "null", {
            "type" : "array",
            "items" : "Topography"
          } ],
          "doc" : "Associated primary topography for metastatic tumours"
        }, {
          "name" : "tumourGrade",
          "type" : [ "null", "string" ],
          "doc" : "Grade of the Tumour"
        }, {
          "name" : "tumourStage",
          "type" : [ "null", "string" ],
          "doc" : "Stage of the Tumour"
        }, {
          "name" : "tumourPrognosticScore",
          "type" : [ "null", "string" ],
          "doc" : "Prognostic Score of the Tumour"
        }, {
          "name" : "tumourPresentation",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "TumourPresentation",
            "symbols" : [ "FIRST_PRESENTATION", "RECURRENCE", "UNKNOWN" ]
          } ],
          "doc" : "In GMS, tumour presentation"
        }, {
          "name" : "primaryOrMetastatic",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "PrimaryOrMetastatic",
            "symbols" : [ "PRIMARY", "METASTATIC", "UNKNOWN", "NOT_APPLICABLE" ]
          } ],
          "doc" : "In GMS, primary or metastatic"
        } ]
      }
    } ],
    "doc" : "In GMS, for all tumours for the CancerParticipant, independent of any samples"
  }, {
    "name" : "previousTreatment",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "PreviousTreatment",
        "doc" : "In GMS, Previous Treatment of Patient",
        "fields" : [ {
          "name" : "previousTreatmentType",
          "type" : [ "null", "string" ]
        }, {
          "name" : "previousTreatmentName",
          "type" : [ "null", "string" ]
        }, {
          "name" : "previousTreatmentDate",
          "type" : [ "null", "Date" ]
        } ]
      }
    } ],
    "doc" : "In GMS, any previous treatment recorded in TOMs"
  } ]
}
