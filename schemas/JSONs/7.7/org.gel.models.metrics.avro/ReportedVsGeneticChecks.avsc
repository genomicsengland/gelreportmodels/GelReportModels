{
  "type" : "record",
  "name" : "ReportedVsGeneticChecks",
  "namespace" : "org.gel.models.metrics.avro",
  "doc" : "Reported versus Genetic checks object\n    ========================================",
  "fields" : [ {
    "name" : "reportedVsGeneticSummary",
    "type" : [ "null", {
      "type" : "record",
      "name" : "ReportedVsGeneticSummary",
      "fields" : [ {
        "name" : "rvsgCheckVersion",
        "type" : "string",
        "doc" : "Reported vs Genetic checks version. This version should point to specific versions of the RvsG script,\n        configurations and thresholds"
      }, {
        "name" : "runDate",
        "type" : "string",
        "doc" : "In the format YYYY-MM-DDTHH:MM:SS+0000"
      }, {
        "name" : "genomeAssembly",
        "type" : [ "null", {
          "type" : "enum",
          "name" : "SupportedAssembly",
          "doc" : "Supported assemblies",
          "symbols" : [ "GRCh37", "GRCh38" ]
        } ],
        "doc" : "Genome assembly"
      }, {
        "name" : "pathToDirectory",
        "type" : "string",
        "doc" : "Path to the directory containing the checks for this family"
      }, {
        "name" : "numberOfMarkers",
        "type" : [ "null", "double" ],
        "doc" : "Number of markers used to compute the Mendelian Inconsistencies. This corresponds to the number of\n        lines in the PLINK .bim file used for this family"
      }, {
        "name" : "numberOfMarkersLE",
        "type" : [ "null", "double" ],
        "doc" : "Number of markers in approximate linkage equilibrium used to calculate IBD"
      }, {
        "name" : "mendelErrorsTool",
        "type" : "string",
        "doc" : "Tool used to compute mendelian inconsistencies"
      }, {
        "name" : "mendelErrorsToolVersion",
        "type" : "string",
        "doc" : "Version of the tool used to compute mendelian inconsistencies"
      }, {
        "name" : "relatednessTool",
        "type" : "string",
        "doc" : "Tool used to compute within-family relatedness"
      }, {
        "name" : "relatednessToolVersion",
        "type" : "string",
        "doc" : "Version of the tool used to compute mendelian inconsistencies"
      }, {
        "name" : "samplesInfo",
        "type" : {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "SamplesInfo",
            "doc" : "ReportedVsGeneticSummary\n    ========================================\n    General information about the checks, versions, tools, and number of markers",
            "fields" : [ {
              "name" : "participantId",
              "type" : "string",
              "doc" : "Participant Id: This code should be unique per patient and independent of the sample"
            }, {
              "name" : "sampleId",
              "type" : "string",
              "doc" : "Sample Id (e.g, LP00012645_5GH))"
            }, {
              "name" : "deliveryId",
              "type" : "string",
              "doc" : "Delivery Id"
            } ]
          }
        },
        "doc" : "Information about the samples used to calculate the RvsG checks"
      }, {
        "name" : "jiraId",
        "type" : [ "null", "string" ],
        "doc" : "JIRA issue id raised for these checks"
      } ]
    } ],
    "doc" : "Summary of the software, versions and samples used for the RvsG checks"
  }, {
    "name" : "coverageBasedSex",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "CoverageBasedSex",
        "doc" : "Coverage-based sex\n    ========================================\n    General information about the checks, versions, tools, and number of markers\n    TODO: Consider removing Inferred karyotype",
        "fields" : [ {
          "name" : "sampleId",
          "type" : [ "null", "string" ],
          "doc" : "Sample Id (e.g, LP00012645_5GH))"
        }, {
          "name" : "inferredKaryotype",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "KaryotypicSex",
            "doc" : "Kariotypic sex\n    TODO: Check if we want to have different karyotype definitions for XO clearcut/doubtful",
            "symbols" : [ "UNKNOWN", "XX", "XY", "XO", "XXY", "XXX", "XXYY", "XXXY", "XXXX", "XYY", "OTHER" ]
          } ],
          "doc" : "Inferred karyotype using coverage information"
        }, {
          "name" : "ratioChrX",
          "type" : [ "null", "double" ],
          "doc" : "Ratio of the average coverage of chromosome X to the average of the autosomal chromosome coverage"
        }, {
          "name" : "ratioChrY",
          "type" : [ "null", "double" ],
          "doc" : "Ratio of the average coverage of chromosome Y to the average of the autosomal chromosome coverage"
        }, {
          "name" : "avgCnvChrX",
          "type" : [ "null", "double" ],
          "doc" : "Number of copies of chromosome X"
        }, {
          "name" : "avgCnvChrY",
          "type" : [ "null", "double" ],
          "doc" : "Number of copies of chromosome Y"
        }, {
          "name" : "reviewedKaryotype",
          "type" : [ "null", "KaryotypicSex" ],
          "doc" : "Reviewed sex karyotype"
        } ]
      }
    } ],
    "doc" : "Coverage-based sex metrics and inferred karyotype"
  }, {
    "name" : "mendelianInconsistencies",
    "type" : [ "null", {
      "type" : "record",
      "name" : "MendelianInconsistencies",
      "fields" : [ {
        "name" : "perFamilyMendelErrors",
        "type" : [ "null", {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "PerFamilyMendelErrors",
            "doc" : "Mendelian inconsistencies\n    ========================================\n    - fmendel reports a line per nuclear family\n    - imendel reports a line per member of the family and nuclear family",
            "fields" : [ {
              "name" : "fatherId",
              "type" : [ "null", "string" ],
              "doc" : "Sample Id of the father"
            }, {
              "name" : "motherId",
              "type" : [ "null", "string" ],
              "doc" : "Sample Id of the mother"
            }, {
              "name" : "numberOfOffspring",
              "type" : [ "null", "double" ],
              "doc" : "Number of children in the nuclear family"
            }, {
              "name" : "numberOfMendelErrors",
              "type" : [ "null", "double" ],
              "doc" : "Number of Mendelian errors in the nuclear family"
            } ]
          }
        } ],
        "doc" : "Number of mendelian inconsitencies per nuclear family. One entry per nuclear family"
      }, {
        "name" : "individualMendelErrors",
        "type" : [ "null", {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "IndividualMendelErrors",
            "fields" : [ {
              "name" : "sampleId",
              "type" : [ "null", "string" ],
              "doc" : "Sample Id"
            }, {
              "name" : "numberOfMendelErrors",
              "type" : [ "null", "double" ],
              "doc" : "Number of Mendelian errors per sample in a nuclear family"
            }, {
              "name" : "rateOfMendelErrors",
              "type" : [ "null", "double" ],
              "doc" : "Rate of Mendelian errors per sample in a nuclear family to the number of sites tested (number of markers)"
            } ]
          }
        } ],
        "doc" : "Number of mendelian inconsitencies per sample and nuclear family. One entry per sample and nuclear family"
      }, {
        "name" : "totalNumberOfMendelErrors",
        "type" : [ "null", {
          "type" : "record",
          "name" : "TotalNumberOfMendelErrors",
          "fields" : [ {
            "name" : "familyMendelErrors",
            "type" : [ "null", "double" ],
            "doc" : "Total number of Mendelian errors in the family, this should be the sum of the mendelian errors in each\n        nuclear family"
          }, {
            "name" : "individualMendelErrors",
            "type" : [ "null", {
              "type" : "array",
              "items" : {
                "type" : "record",
                "name" : "AggregatedIndividualMendelErrors",
                "fields" : [ {
                  "name" : "sampleId",
                  "type" : [ "null", "string" ],
                  "doc" : "Sample Id"
                }, {
                  "name" : "totalnumberOfMendelErrors",
                  "type" : [ "null", "double" ],
                  "doc" : "Total number of Mendelian errors per sample considering all nuclear families"
                } ]
              }
            } ],
            "doc" : "Number of Mendelian errors per sample considering all nuclear families. Should be one entry per sample in\n        the family"
          } ]
        } ],
        "doc" : "Aggregated number of mendelian inconsitencies per sample and family"
      }, {
        "name" : "locusMendelSummary",
        "type" : [ "null", {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "LocusMendelSummary",
            "fields" : [ {
              "name" : "sampleId",
              "type" : [ "null", "string" ],
              "doc" : "Sample Id"
            }, {
              "name" : "chr",
              "type" : [ "null", "string" ],
              "doc" : "Chromosome (1-22, X, Y, XY)"
            }, {
              "name" : "code",
              "type" : [ "null", "double" ],
              "doc" : "Numeric error code (more information here: http://zzz.bwh.harvard.edu/plink/summary.shtml#mendel)"
            }, {
              "name" : "numberOfErrors",
              "type" : [ "null", "double" ],
              "doc" : "Number of errors of the type \"code\" in that chromosome"
            } ]
          }
        } ],
        "doc" : "Summary of the type of mendelian inconstencies happening in the family. One entry per sample, chromosome and\n        code"
      } ]
    } ],
    "doc" : "Per family and per sample mendelian inconsistencies"
  }, {
    "name" : "familyRelatedness",
    "type" : [ "null", {
      "type" : "record",
      "name" : "FamilyRelatedness",
      "fields" : [ {
        "name" : "relatedness",
        "type" : [ "null", {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "RelatednessPair",
            "doc" : "Relatedness\n    ========================================",
            "fields" : [ {
              "name" : "sampleId1",
              "type" : [ "null", "string" ],
              "doc" : "Sample Id of one of the samples in the pair"
            }, {
              "name" : "sampleId2",
              "type" : [ "null", "string" ],
              "doc" : "Sample Id of the other sample in the pair"
            }, {
              "name" : "ibd0",
              "type" : [ "null", "double" ],
              "doc" : "Estimated proportion of autosome with 0 alleles shared"
            }, {
              "name" : "ibd1",
              "type" : [ "null", "double" ],
              "doc" : "Estimated proportion of autosome with 1 allele shared"
            }, {
              "name" : "ibd2",
              "type" : [ "null", "double" ],
              "doc" : "Estimated proportion of autosome with 2 alleles shared"
            }, {
              "name" : "piHat",
              "type" : [ "null", "double" ],
              "doc" : "Estimated overall proportion of shared autosomal DNA"
            } ]
          }
        } ],
        "doc" : "Pairwise relatedness within the family. One entry per pair of samples"
      } ]
    } ],
    "doc" : "Within-family relatedness"
  }, {
    "name" : "evaluation",
    "type" : [ "null", {
      "type" : "record",
      "name" : "Evaluation",
      "doc" : "Evaluation\n    ========================================\n    reportedVsGeneticSummary: familyPassesGvsRChecks, familyFailsACheck, familyMissingACheck",
      "fields" : [ {
        "name" : "coverageBasedSexCheck",
        "type" : [ "null", {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "CoverageBasedSexCheck",
            "doc" : "Evaluation\n    ========================================",
            "fields" : [ {
              "name" : "sampleId",
              "type" : [ "null", "string" ],
              "doc" : "Sample Id"
            }, {
              "name" : "reportedPhenotypicSex",
              "type" : [ "null", {
                "type" : "enum",
                "name" : "Sex",
                "doc" : "Phenotypic sex",
                "symbols" : [ "UNKNOWN", "MALE", "FEMALE", "OTHER" ]
              } ],
              "doc" : "Reported phenotypic sex"
            }, {
              "name" : "reportedKaryotypicSex",
              "type" : [ "null", "KaryotypicSex" ],
              "doc" : "Reported karyotypic sex"
            }, {
              "name" : "inferredSexKaryotype",
              "type" : [ "null", "KaryotypicSex" ],
              "doc" : "Inferred coverage-based sex karyotype"
            }, {
              "name" : "sexQuery",
              "type" : [ "null", {
                "type" : "enum",
                "name" : "Query",
                "doc" : "A query",
                "symbols" : [ "yes", "no", "unknown", "notTested" ]
              } ],
              "doc" : "Whether the sample is a sex query (yes, no, unknown, notTested)"
            }, {
              "name" : "comments",
              "type" : [ "null", "string" ],
              "doc" : "Comments"
            } ]
          }
        } ],
        "doc" : "Coverage-based sex evaluation. One entry per sample"
      }, {
        "name" : "mendelianInconsistenciesCheck",
        "type" : [ "null", {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "MendelianInconsistenciesCheck",
            "fields" : [ {
              "name" : "sampleId",
              "type" : [ "null", "string" ],
              "doc" : "Sample Id"
            }, {
              "name" : "mendelianInconsistenciesQuery",
              "type" : [ "null", "Query" ],
              "doc" : "Whether the sample is a Mendelian inconsistencies query (yes, no, unknown, notTested)"
            }, {
              "name" : "comments",
              "type" : [ "null", "string" ],
              "doc" : "Mendelian inconsistencies cannot always be computed for all the samples in the family (depends on family\n        structure). Specify here if this is the case or there was any other issues"
            } ]
          }
        } ],
        "doc" : "Mendelian inconsitencies evaluation. One entry per sample"
      }, {
        "name" : "familyRelatednessCheck",
        "type" : [ "null", {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "FamilyRelatednessCheck",
            "fields" : [ {
              "name" : "sampleId1",
              "type" : [ "null", "string" ],
              "doc" : "Sample Id of one of the samples in the pair"
            }, {
              "name" : "sampleId2",
              "type" : [ "null", "string" ],
              "doc" : "Sample Id of the other sample in the pair"
            }, {
              "name" : "relationshipFromPedigree",
              "type" : [ "null", {
                "type" : "enum",
                "name" : "FamiliarRelationship",
                "namespace" : "org.gel.models.participant.avro",
                "doc" : "Familiar relationship from pedrigree",
                "symbols" : [ "TwinsMonozygous", "TwinsDizygous", "TwinsUnknown", "FullSibling", "FullSiblingF", "FullSiblingM", "Mother", "Father", "Son", "Daughter", "ChildOfUnknownSex", "MaternalAunt", "MaternalUncle", "MaternalUncleOrAunt", "PaternalAunt", "PaternalUncle", "PaternalUncleOrAunt", "MaternalGrandmother", "PaternalGrandmother", "MaternalGrandfather", "PaternalGrandfather", "DoubleFirstCousin", "MaternalCousinSister", "PaternalCousinSister", "MaternalCousinBrother", "PaternalCousinBrother", "Cousin", "Spouse", "Other", "RelationIsNotClear", "Unrelated", "Unknown" ]
              } ],
              "doc" : "Reported relationship from sampleId1 to sampleId2 according to the pedigree provided"
            }, {
              "name" : "possibleRelationship",
              "type" : [ "null", "string" ],
              "doc" : "Expected relationship according to IBD"
            }, {
              "name" : "withinFamilyIBDQuery",
              "type" : [ "null", "Query" ],
              "doc" : "Whether the pair of samples are a within-family query (yes, no, unknown, notTested)"
            }, {
              "name" : "comments",
              "type" : [ "null", "string" ],
              "doc" : "Comments"
            } ]
          }
        } ],
        "doc" : "Within-family relatedness evaluation. One entry per pair of samples"
      }, {
        "name" : "reportedVsGeneticSummary",
        "type" : [ "null", {
          "type" : "enum",
          "name" : "reportedVsGeneticSummary",
          "doc" : "Reported vs Genetic Summary",
          "symbols" : [ "familyPassesGvsRChecks", "familyFailsACheck", "familyMissingACheck" ]
        } ],
        "doc" : "Final evaluation summary. Does the family passes RvsG checks or errors are present?"
      } ]
    } ],
    "doc" : "Evaluation of the reported vs genetic information"
  } ]
}
