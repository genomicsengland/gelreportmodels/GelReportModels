{
  "type" : "record",
  "name" : "StudyEntry",
  "namespace" : "org.opencb.biodata.models.variant.avro",
  "fields" : [ {
    "name" : "studyId",
    "type" : [ "null", "string" ],
    "doc" : "* Unique identifier of the study."
  }, {
    "name" : "files",
    "type" : {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "FileEntry",
        "fields" : [ {
          "name" : "fileId",
          "type" : [ "null", "string" ],
          "doc" : "* Unique identifier of the source file."
        }, {
          "name" : "call",
          "type" : [ "null", "string" ],
          "doc" : "* Original call position for the variant, if the file was normalized.\n         *\n         * {position}:{reference}:{alternate}(,{other_alternate})*:{allele_index}"
        }, {
          "name" : "attributes",
          "type" : {
            "type" : "map",
            "values" : "string"
          },
          "doc" : "* Optional attributes that probably depend on the format of the file the\n         * variant was initially read from."
        } ]
      }
    },
    "doc" : "* List of files from the study where the variant was present.",
    "default" : [ ]
  }, {
    "name" : "secondaryAlternates",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "AlternateCoordinate",
        "fields" : [ {
          "name" : "chromosome",
          "type" : [ "null", "string" ]
        }, {
          "name" : "start",
          "type" : [ "null", "int" ],
          "doc" : "* First position 1-based of the alternate. If null, the start is the same of the variant."
        }, {
          "name" : "end",
          "type" : [ "null", "int" ],
          "doc" : "* End position 1-based of the alternate. If null, the end is the same of the variant."
        }, {
          "name" : "reference",
          "type" : [ "null", "string" ],
          "doc" : "* Reference allele. If null, the reference is the same of the variant."
        }, {
          "name" : "alternate",
          "type" : "string",
          "doc" : "* Alternate allele."
        }, {
          "name" : "type",
          "type" : {
            "type" : "enum",
            "name" : "VariantType",
            "doc" : "* Type of variation, which depends mostly on its length.\n     * <ul>\n     * <li>SNVs involve a single nucleotide, without changes in length</li>\n     * <li>MNVs involve multiple nucleotides, without changes in length</li>\n     * <li>Indels are insertions or deletions of less than SV_THRESHOLD (50) nucleotides</li>\n     * <li>Structural variations are large changes of more than SV_THRESHOLD nucleotides</li>\n     * <li>Copy-number variations alter the number of copies of a region</li>\n     * </ul>",
            "symbols" : [ "SNV", "SNP", "MNV", "MNP", "INDEL", "SV", "INSERTION", "DELETION", "TRANSLOCATION", "INVERSION", "CNV", "DUPLICATION", "BREAKEND", "NO_VARIATION", "SYMBOLIC", "MIXED" ]
          }
        } ]
      }
    } ],
    "doc" : "* Alternate alleles that appear along with a variant alternate.",
    "default" : null
  }, {
    "name" : "format",
    "type" : {
      "type" : "array",
      "items" : "string"
    },
    "doc" : "* Fields stored for each sample."
  }, {
    "name" : "samplesData",
    "type" : {
      "type" : "array",
      "items" : {
        "type" : "array",
        "items" : "string"
      }
    },
    "doc" : "* Genotypes and other sample-related information. Each position is related\n         * with one sample. The content are lists of values in the same order than the\n         * format array. The length of this lists must be the same as the format field."
  }, {
    "name" : "stats",
    "type" : {
      "type" : "map",
      "values" : {
        "type" : "record",
        "name" : "VariantStats",
        "fields" : [ {
          "name" : "refAllele",
          "type" : [ "null", "string" ]
        }, {
          "name" : "altAllele",
          "type" : [ "null", "string" ]
        }, {
          "name" : "refAlleleCount",
          "type" : [ "null", "int" ]
        }, {
          "name" : "altAlleleCount",
          "type" : [ "null", "int" ]
        }, {
          "name" : "genotypesCount",
          "type" : {
            "type" : "map",
            "values" : "int",
            "java-key-class" : "org.opencb.biodata.models.feature.Genotype"
          }
        }, {
          "name" : "genotypesFreq",
          "type" : {
            "type" : "map",
            "values" : "float",
            "java-key-class" : "org.opencb.biodata.models.feature.Genotype"
          }
        }, {
          "name" : "missingAlleles",
          "type" : [ "null", "int" ]
        }, {
          "name" : "missingGenotypes",
          "type" : [ "null", "int" ]
        }, {
          "name" : "refAlleleFreq",
          "type" : [ "null", "float" ]
        }, {
          "name" : "altAlleleFreq",
          "type" : [ "null", "float" ]
        }, {
          "name" : "maf",
          "type" : [ "null", "float" ]
        }, {
          "name" : "mgf",
          "type" : [ "null", "float" ]
        }, {
          "name" : "mafAllele",
          "type" : [ "null", "string" ]
        }, {
          "name" : "mgfGenotype",
          "type" : [ "null", "string" ]
        }, {
          "name" : "passedFilters",
          "type" : [ "null", "boolean" ]
        }, {
          "name" : "mendelianErrors",
          "type" : [ "null", "int" ]
        }, {
          "name" : "casesPercentDominant",
          "type" : [ "null", "float" ]
        }, {
          "name" : "controlsPercentDominant",
          "type" : [ "null", "float" ]
        }, {
          "name" : "casesPercentRecessive",
          "type" : [ "null", "float" ]
        }, {
          "name" : "controlsPercentRecessive",
          "type" : [ "null", "float" ]
        }, {
          "name" : "quality",
          "type" : [ "null", "float" ]
        }, {
          "name" : "numSamples",
          "type" : [ "null", "int" ]
        }, {
          "name" : "variantType",
          "type" : [ "null", "VariantType" ],
          "default" : null
        }, {
          "name" : "hw",
          "type" : [ "null", {
            "type" : "record",
            "name" : "VariantHardyWeinbergStats",
            "fields" : [ {
              "name" : "chi2",
              "type" : [ "null", "float" ]
            }, {
              "name" : "pValue",
              "type" : [ "null", "float" ]
            }, {
              "name" : "n",
              "type" : [ "null", "int" ]
            }, {
              "name" : "n_AA_11",
              "type" : [ "null", "int" ]
            }, {
              "name" : "n_Aa_10",
              "type" : [ "null", "int" ]
            }, {
              "name" : "n_aa_00",
              "type" : [ "null", "int" ]
            }, {
              "name" : "e_AA_11",
              "type" : [ "null", "float" ]
            }, {
              "name" : "e_Aa_10",
              "type" : [ "null", "float" ]
            }, {
              "name" : "e_aa_00",
              "type" : [ "null", "float" ]
            }, {
              "name" : "p",
              "type" : [ "null", "float" ]
            }, {
              "name" : "q",
              "type" : [ "null", "float" ]
            } ]
          } ],
          "default" : null
        } ]
      }
    },
    "doc" : "* Statistics of the genomic variation, such as its alleles/genotypes count\n         * or its minimum allele frequency, grouped by cohort name."
  } ]
}
