{
  "type" : "record",
  "name" : "UniparentalDisomyInterpretationLog",
  "namespace" : "org.gel.models.report.avro",
  "fields" : [ {
    "name" : "variant",
    "type" : {
      "type" : "record",
      "name" : "UniparentalDisomyDetails",
      "fields" : [ {
        "name" : "assembly",
        "type" : {
          "type" : "enum",
          "name" : "Assembly",
          "doc" : "The reference genome assembly",
          "symbols" : [ "GRCh38", "GRCh37" ]
        },
        "doc" : "Reference assembly"
      }, {
        "name" : "chromosome",
        "type" : "string",
        "doc" : "Chromosome where two homologues were inherited from one parent"
      }, {
        "name" : "complete",
        "type" : [ "null", "boolean" ],
        "doc" : "indicates whether UPD event involves an entire chromosome"
      }, {
        "name" : "origin",
        "type" : {
          "type" : "enum",
          "name" : "UniparentalDisomyOrigin",
          "symbols" : [ "paternal", "maternal", "unknown" ]
        },
        "doc" : "The parent who contributed two chromosomes was the mother (maternal) or the father (paternal)"
      }, {
        "name" : "uniparentalDisomyFragments",
        "type" : [ "null", {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "UniparentalDisomyFragment",
            "fields" : [ {
              "name" : "coordinates",
              "type" : [ "null", {
                "type" : "record",
                "name" : "Coordinates",
                "fields" : [ {
                  "name" : "assembly",
                  "type" : "Assembly",
                  "doc" : "The assembly to which this variant corresponds"
                }, {
                  "name" : "chromosome",
                  "type" : "string",
                  "doc" : "Chromosome without \"chr\" prefix (e.g. X rather than chrX)"
                }, {
                  "name" : "start",
                  "type" : "int",
                  "doc" : "Start genomic position for variant (1-based)"
                }, {
                  "name" : "end",
                  "type" : "int",
                  "doc" : "End genomic position for variant"
                }, {
                  "name" : "ciStart",
                  "type" : [ "null", {
                    "type" : "record",
                    "name" : "ConfidenceInterval",
                    "fields" : [ {
                      "name" : "left",
                      "type" : "int"
                    }, {
                      "name" : "right",
                      "type" : "int"
                    } ]
                  } ]
                }, {
                  "name" : "ciEnd",
                  "type" : [ "null", "ConfidenceInterval" ]
                } ]
              } ],
              "doc" : "Coordinates can be specified to indicate the part of the chromosome affected"
            }, {
              "name" : "uniparentalDisomyType",
              "type" : {
                "type" : "enum",
                "name" : "UniparentalDisomyType",
                "symbols" : [ "isodisomy", "heterodisomy", "both" ]
              },
              "doc" : "indicates whether the UPD event involves `isodisomy`, `heterodisomy` or `both`"
            } ]
          }
        } ],
        "doc" : "List of all of the UPD fragments for this UPD event"
      } ]
    },
    "doc" : "Variant details"
  }, {
    "name" : "user",
    "type" : {
      "type" : "record",
      "name" : "User",
      "fields" : [ {
        "name" : "userid",
        "type" : [ "null", "string" ],
        "doc" : "Azure Active Directory immutable user OID"
      }, {
        "name" : "email",
        "type" : "string",
        "doc" : "User email address"
      }, {
        "name" : "username",
        "type" : "string",
        "doc" : "Username"
      }, {
        "name" : "role",
        "type" : [ "null", "string" ]
      }, {
        "name" : "groups",
        "type" : [ "null", {
          "type" : "array",
          "items" : "string"
        } ]
      } ]
    },
    "doc" : "User who set classification"
  }, {
    "name" : "timestamp",
    "type" : "string",
    "doc" : "Date of classification. Format YYYY-MM-DD (e.g. 2020-01-31)"
  }, {
    "name" : "groupId",
    "type" : "string",
    "doc" : "GeL group ID. For GMS cases this will be the referral ID. For 100k rare disease cases this will be the family ID. For 100k cancer cases this will be the participant ID."
  }, {
    "name" : "caseId",
    "type" : "string",
    "doc" : "Interpretation request ID including CIP prefix and version suffix (e.g. SAP-1234-1)"
  }, {
    "name" : "variantValidation",
    "type" : [ "null", {
      "type" : "record",
      "name" : "VariantValidation",
      "fields" : [ {
        "name" : "validationTechnology",
        "type" : "string",
        "doc" : "Technology used to perform secondary confirmation of this variant (e.g. Sanger)"
      }, {
        "name" : "validationResult",
        "type" : {
          "type" : "enum",
          "name" : "ValidationResult",
          "symbols" : [ "NotPerformed", "Confirmed", "NotConfirmed", "Pending" ]
        },
        "doc" : "Status/outcome of validation"
      } ]
    } ],
    "doc" : "Independent validation of variant"
  }, {
    "name" : "comments",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "UserComment",
        "fields" : [ {
          "name" : "comment",
          "type" : "string",
          "doc" : "Comment text"
        }, {
          "name" : "user",
          "type" : [ "null", "User" ],
          "doc" : "User who created comment"
        }, {
          "name" : "timestamp",
          "type" : [ "null", "string" ],
          "doc" : "Date and time comment was created (ISO 8601 datetime with seconds and timezone e.g. 2020-11-23T15:52:36+00:00)"
        } ]
      }
    } ],
    "doc" : "User comments attached to this variant in this case"
  }, {
    "name" : "variantClassification",
    "type" : {
      "type" : "enum",
      "name" : "ClinicalSignificance",
      "symbols" : [ "benign", "likely_benign", "likely_pathogenic", "pathogenic", "uncertain_significance", "excluded" ]
    },
    "doc" : "Variant classification"
  }, {
    "name" : "Artifact",
    "type" : [ "null", "boolean" ],
    "doc" : "User has marked the variant as an artefact"
  }, {
    "name" : "decisionSupportSystemFilters",
    "type" : [ "null", {
      "type" : "map",
      "values" : "string"
    } ],
    "doc" : "Filter settings applied at time variant was classified"
  } ]
}
