{
  "type" : "record",
  "name" : "SearchReferencesResponse",
  "namespace" : "org.ga4gh.methods",
  "doc" : "This is the response from `POST /references/search` expressed as JSON.",
  "fields" : [ {
    "name" : "references",
    "type" : {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "Reference",
        "namespace" : "org.ga4gh.models",
        "doc" : "A `Reference` is a canonical assembled contig, intended to act as a\nreference coordinate space for other genomic annotations. A single\n`Reference` might represent the human chromosome 1, for instance.\n\n`Reference`s are designed to be immutable.",
        "fields" : [ {
          "name" : "id",
          "type" : "string",
          "doc" : "The reference ID. Unique within the repository."
        }, {
          "name" : "length",
          "type" : "long",
          "doc" : "The length of this reference's sequence."
        }, {
          "name" : "md5checksum",
          "type" : "string",
          "doc" : "The MD5 checksum uniquely representing this `Reference` as a lower-case\n  hexadecimal string, calculated as the MD5 of the upper-case sequence\n  excluding all whitespace characters (this is equivalent to SQ:M5 in SAM)."
        }, {
          "name" : "name",
          "type" : "string",
          "doc" : "The name of this reference. (e.g. '22')."
        }, {
          "name" : "sourceURI",
          "type" : [ "null", "string" ],
          "doc" : "The URI from which the sequence was obtained. Specifies a FASTA format\n  file/string with one name, sequence pair. In most cases, clients should call\n  the `getReferenceBases()` method to obtain sequence bases for a `Reference`\n  instead of attempting to retrieve this URI.",
          "default" : null
        }, {
          "name" : "sourceAccessions",
          "type" : {
            "type" : "array",
            "items" : "string"
          },
          "doc" : "All known corresponding accession IDs in INSDC (GenBank/ENA/DDBJ) which must include\n  a version number, e.g. `GCF_000001405.26`."
        }, {
          "name" : "isDerived",
          "type" : "boolean",
          "doc" : "A sequence X is said to be derived from source sequence Y, if X and Y\n  are of the same length and the per-base sequence divergence at A/C/G/T bases\n  is sufficiently small. Two sequences derived from the same official\n  sequence share the same coordinates and annotations, and\n  can be replaced with the official sequence for certain use cases.",
          "default" : false
        }, {
          "name" : "sourceDivergence",
          "type" : [ "null", "float" ],
          "doc" : "The `sourceDivergence` is the fraction of non-indel bases that do not match the\n  reference this record was derived from.",
          "default" : null
        }, {
          "name" : "ncbiTaxonId",
          "type" : [ "null", "int" ],
          "doc" : "ID from http://www.ncbi.nlm.nih.gov/taxonomy (e.g. 9606->human).",
          "default" : null
        } ]
      }
    },
    "doc" : "The list of matching references.",
    "default" : [ ]
  }, {
    "name" : "nextPageToken",
    "type" : [ "null", "string" ],
    "doc" : "The continuation token, which is used to page through large result sets.\n  Provide this value in a subsequent request to return the next page of\n  results. This field will be empty if there aren't any additional results.",
    "default" : null
  } ]
}
