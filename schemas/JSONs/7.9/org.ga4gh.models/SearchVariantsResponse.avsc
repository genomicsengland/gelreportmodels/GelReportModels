{
  "type" : "record",
  "name" : "SearchVariantsResponse",
  "namespace" : "org.ga4gh.methods",
  "doc" : "This is the response from `POST /variants/search` expressed as JSON.",
  "fields" : [ {
    "name" : "variants",
    "type" : {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "Variant",
        "namespace" : "org.ga4gh.models",
        "doc" : "A `Variant` represents a change in DNA sequence relative to some reference.\nFor example, a variant could represent a SNP or an insertion.\nVariants belong to a `VariantSet`.\nThis is equivalent to a row in VCF.",
        "fields" : [ {
          "name" : "id",
          "type" : "string",
          "doc" : "The variant ID."
        }, {
          "name" : "variantSetId",
          "type" : "string",
          "doc" : "The ID of the `VariantSet` this variant belongs to. This transitively defines\n  the `ReferenceSet` against which the `Variant` is to be interpreted."
        }, {
          "name" : "names",
          "type" : {
            "type" : "array",
            "items" : "string"
          },
          "doc" : "Names for the variant, for example a RefSNP ID.",
          "default" : [ ]
        }, {
          "name" : "created",
          "type" : [ "null", "long" ],
          "doc" : "The date this variant was created in milliseconds from the epoch.",
          "default" : null
        }, {
          "name" : "updated",
          "type" : [ "null", "long" ],
          "doc" : "The time at which this variant was last updated in\n  milliseconds from the epoch.",
          "default" : null
        }, {
          "name" : "referenceName",
          "type" : "string",
          "doc" : "The reference on which this variant occurs.\n  (e.g. `chr20` or `X`)"
        }, {
          "name" : "start",
          "type" : "long",
          "doc" : "The start position at which this variant occurs (0-based).\n  This corresponds to the first base of the string of reference bases.\n  Genomic positions are non-negative integers less than reference length.\n  Variants spanning the join of circular genomes are represented as\n  two variants one on each side of the join (position 0)."
        }, {
          "name" : "end",
          "type" : "long",
          "doc" : "The end position (exclusive), resulting in [start, end) closed-open interval.\n  This is typically calculated by `start + referenceBases.length`."
        }, {
          "name" : "referenceBases",
          "type" : "string",
          "doc" : "The reference bases for this variant. They start at the given start position."
        }, {
          "name" : "alternateBases",
          "type" : {
            "type" : "array",
            "items" : "string"
          },
          "doc" : "The bases that appear instead of the reference bases. Multiple alternate\n  alleles are possible.",
          "default" : [ ]
        }, {
          "name" : "info",
          "type" : {
            "type" : "map",
            "values" : {
              "type" : "array",
              "items" : "string"
            }
          },
          "doc" : "A map of additional variant information.",
          "default" : { }
        }, {
          "name" : "calls",
          "type" : {
            "type" : "array",
            "items" : {
              "type" : "record",
              "name" : "Call",
              "doc" : "A `Call` represents the determination of genotype with respect to a\nparticular `Variant`.\n\nIt may include associated information such as quality\nand phasing. For example, a call might assign a probability of 0.32 to\nthe occurrence of a SNP named rs1234 in a call set with the name NA12345.",
              "fields" : [ {
                "name" : "callSetName",
                "type" : [ "null", "string" ],
                "doc" : "The name of the call set this variant call belongs to.\n  If this field is not present, the ordering of the call sets from a\n  `SearchCallSetsRequest` over this `VariantSet` is guaranteed to match\n  the ordering of the calls on this `Variant`.\n  The number of results will also be the same.",
                "default" : null
              }, {
                "name" : "callSetId",
                "type" : [ "null", "string" ],
                "doc" : "The ID of the call set this variant call belongs to.\n\n  If this field is not present, the ordering of the call sets from a\n  `SearchCallSetsRequest` over this `VariantSet` is guaranteed to match\n  the ordering of the calls on this `Variant`.\n  The number of results will also be the same.",
                "default" : null
              }, {
                "name" : "genotype",
                "type" : {
                  "type" : "array",
                  "items" : "int"
                },
                "doc" : "The genotype of this variant call.\n\n  A 0 value represents the reference allele of the associated `Variant`. Any\n  other value is a 1-based index into the alternate alleles of the associated\n  `Variant`.\n\n  If a variant had a referenceBases field of \"T\", an alternateBases\n  value of [\"A\", \"C\"], and the genotype was [2, 1], that would mean the call\n  represented the heterozygous value \"CA\" for this variant. If the genotype\n  was instead [0, 1] the represented value would be \"TA\". Ordering of the\n  genotype values is important if the phaseset field is present.",
                "default" : [ ]
              }, {
                "name" : "phaseset",
                "type" : [ "null", "string" ],
                "doc" : "If this field is not null, this variant call's genotype ordering implies\n  the phase of the bases and is consistent with any other variant calls on\n  the same contig which have the same phaseset string.",
                "default" : null
              }, {
                "name" : "genotypeLikelihood",
                "type" : {
                  "type" : "array",
                  "items" : "double"
                },
                "doc" : "The genotype likelihoods for this variant call. Each array entry\n  represents how likely a specific genotype is for this call as\n  log10(P(data | genotype)), analogous to the GL tag in the VCF spec. The\n  value ordering is defined by the GL tag in the VCF spec.",
                "default" : [ ]
              }, {
                "name" : "referenceDepth",
                "type" : [ "null", "int" ],
                "doc" : "Integer or null value indicating the number of reads supporting the\n  reference allele. If a missing value '.' is in the VCF, this should\n  be recoded as null/None.",
                "default" : null
              }, {
                "name" : "alternateDepth",
                "type" : {
                  "type" : "array",
                  "items" : [ "null", "int" ]
                },
                "doc" : "An array of values which indicate the read depth assigned to the Alt allele(s)\n  if alternateBases are C,GT and the alternateDepth is 1,2 - \"C\" has 1 supporting\n  read, \"GT\" has 2. It is possible for the field value to be missing, in which case\n  the \".\" value should be recorded in the Call as null/None\n  This is kept as an array, though the expectation is that this will usually be\n  used with normalised variants, so that the array has one element",
                "default" : [ ]
              }, {
                "name" : "info",
                "type" : {
                  "type" : "map",
                  "values" : {
                    "type" : "array",
                    "items" : "string"
                  }
                },
                "doc" : "A map of additional variant call information.",
                "default" : { }
              } ]
            }
          },
          "doc" : "The variant calls for this particular variant. Each one represents the\n  determination of genotype with respect to this variant. `Call`s in this array\n  are implicitly associated with this `Variant`.",
          "default" : [ ]
        } ]
      }
    },
    "doc" : "The list of matching variants.\n  If the `callSetId` field on the returned calls is not present,\n  the ordering of the call sets from a `SearchCallSetsRequest`\n  over the parent `VariantSet` is guaranteed to match the ordering\n  of the calls on each `Variant`. The number of results will also be\n  the same.",
    "default" : [ ]
  }, {
    "name" : "nextPageToken",
    "type" : [ "null", "string" ],
    "doc" : "The continuation token, which is used to page through large result sets.\n  Provide this value in a subsequent request to return the next page of\n  results. This field will be empty if there aren't any additional results.",
    "default" : null
  } ]
}
