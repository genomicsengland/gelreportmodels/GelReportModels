{
  "type" : "record",
  "name" : "ExitQuestionnaireInjectCancer",
  "namespace" : "org.gel.models.cva.avro",
  "doc" : "Record for cancer exit questionnaire injection as part of the data intake for CVA",
  "fields" : [ {
    "name" : "metadata",
    "type" : {
      "type" : "record",
      "name" : "InjectionMetadata",
      "doc" : "Metadata about injected data",
      "fields" : [ {
        "name" : "reportModelVersion",
        "type" : "string",
        "doc" : "Report avro models version"
      }, {
        "name" : "id",
        "type" : "string",
        "doc" : "The entity identifier"
      }, {
        "name" : "version",
        "type" : "int",
        "doc" : "The entity version. This is a correlative number being the highest value the latest version."
      }, {
        "name" : "caseId",
        "type" : "string",
        "doc" : "The case identifier"
      }, {
        "name" : "caseVersion",
        "type" : "int",
        "doc" : "The case version. This is a correlative number being the highest value the latest version."
      }, {
        "name" : "groupId",
        "type" : "string",
        "doc" : "The family identifier"
      }, {
        "name" : "cohortId",
        "type" : "string",
        "doc" : "The cohort identifier (the same family can have several cohorts)"
      }, {
        "name" : "author",
        "type" : "string",
        "doc" : "The author of the ReportedVariant, either tiering, exomiser, a given cip (e.g.: omicia) or a given GMCs user name"
      }, {
        "name" : "authorVersion",
        "type" : [ "null", "string" ],
        "doc" : "The author version of the ReportedVariant, either tiering, exomiser or a given cip. Only applicable for automated processes."
      }, {
        "name" : "assembly",
        "type" : [ "null", {
          "type" : "enum",
          "name" : "Assembly",
          "namespace" : "org.gel.models.report.avro",
          "doc" : "The reference genome assembly",
          "symbols" : [ "GRCh38", "GRCh37" ]
        } ],
        "doc" : "The assembly to which the variants refer"
      }, {
        "name" : "program",
        "type" : {
          "type" : "enum",
          "name" : "Program",
          "namespace" : "org.gel.models.report.avro",
          "doc" : "The Genomics England program",
          "symbols" : [ "cancer", "rare_disease" ]
        },
        "doc" : "The 100K Genomes program to which the reported variant belongs."
      }, {
        "name" : "category",
        "type" : {
          "type" : "enum",
          "name" : "Category",
          "symbols" : [ "HundredK", "NGIS" ]
        },
        "doc" : "The category to which the case belongs."
      }, {
        "name" : "caseCreationDate",
        "type" : [ "null", "string" ],
        "doc" : "The creation date of the case (ISO-8601)"
      }, {
        "name" : "caseLastModifiedDate",
        "type" : [ "null", "string" ],
        "doc" : "The last modified date of the case (ISO-8601)"
      }, {
        "name" : "organisation",
        "type" : [ "null", {
          "type" : "record",
          "name" : "Organisation",
          "doc" : "An organisation which may own or be assigned to a case",
          "fields" : [ {
            "name" : "ods",
            "type" : "string",
            "doc" : "ODS code"
          }, {
            "name" : "gmc",
            "type" : [ "null", "string" ],
            "doc" : "The GMC name"
          }, {
            "name" : "site",
            "type" : [ "null", "string" ],
            "doc" : "The site name"
          } ]
        } ],
        "doc" : "The organisation responsible for this payload (Pedigree and CancerParticipant will correspond to the case\n        owner and the ClinicalReport will correspond to the case assignee)"
      }, {
        "name" : "organisationNgis",
        "type" : [ "null", {
          "type" : "record",
          "name" : "OrganisationNgis",
          "namespace" : "org.gel.models.participant.avro",
          "fields" : [ {
            "name" : "organisationId",
            "type" : "string",
            "doc" : "Organisation Id"
          }, {
            "name" : "organisationCode",
            "type" : "string",
            "doc" : "Ods code"
          }, {
            "name" : "organisationName",
            "type" : "string",
            "doc" : "Organisation Name"
          }, {
            "name" : "organisationNationalGroupingId",
            "type" : "string",
            "doc" : "National Grouping (GLH) Id"
          }, {
            "name" : "organisationNationalGroupingName",
            "type" : "string",
            "doc" : "National Grouping (GLH) Name"
          } ]
        } ],
        "doc" : "The NGIS organisation responsible for this payload"
      }, {
        "name" : "referralTestId",
        "type" : [ "null", "string" ],
        "doc" : "Test unique identifier (only sent for NGIS cases)"
      }, {
        "name" : "referralId",
        "type" : [ "null", "string" ],
        "doc" : "Referral unique identifier (only sent for NGIS cases)"
      } ]
    },
    "doc" : "Metadata on the report events in the clinical report"
  }, {
    "name" : "cancercaseLevelQuestions",
    "type" : {
      "type" : "record",
      "name" : "CancerCaseLevelQuestions",
      "namespace" : "org.gel.models.report.avro",
      "doc" : "The questions for the cancer program exit questionnaire at case level",
      "fields" : [ {
        "name" : "total_review_time",
        "type" : "double",
        "doc" : "Total time taken to review/collate evidence for variants (hours).\n        Include all literature review time, consultation with relevant experts etc."
      }, {
        "name" : "mdt1_time",
        "type" : "double",
        "doc" : "Time taken to discuss case at MDT (hours)."
      }, {
        "name" : "mdt2_time",
        "type" : [ "null", "double" ],
        "doc" : "If the case is discussed at a 2nd MDT please enter time here (hours)."
      }, {
        "name" : "validation_assay_time",
        "type" : [ "null", "double" ],
        "doc" : "Total time to design ALL validation assay(s) for case (hours).\n        Only applicable if it is necessary to design a new assay to validate the variant."
      }, {
        "name" : "wet_validation_time",
        "type" : [ "null", "double" ],
        "doc" : "Technical Laboratory Validation. Total time for validation test wet work for all variants (hours)."
      }, {
        "name" : "analytical_validation_time",
        "type" : [ "null", "double" ],
        "doc" : "Analytical Laboratory Validation. Total time for analysis of validation results for all variants (hours)."
      }, {
        "name" : "primary_reporting_time",
        "type" : "double",
        "doc" : "Primary Reporting. Time taken to complete primary reporting stage (hours)."
      }, {
        "name" : "primary_authorisation_time",
        "type" : "double",
        "doc" : "Report Authorisation. Time taken to check and authorise report (hours)."
      }, {
        "name" : "report_distribution_time",
        "type" : "double",
        "doc" : "Report Distribution.\n        Please enter, where possible/accessible how long it takes for the result to be conveyed to the patient.\n        E.g. via letter from the clinician (days)."
      }, {
        "name" : "total_time",
        "type" : "double",
        "doc" : "Total time from result to report.\n        The total time taken from when the analysis of the WGS results started  to a report being received\n        by the patient include any 'waiting' time (days)."
      }, {
        "name" : "reviewedInMdtWga",
        "type" : {
          "type" : "enum",
          "name" : "ReviewedParts",
          "doc" : "An enumeration for Which parts of the WGA were reviewed?:\n* `domain_1`: Domain 1 only\n* `domain_1_and_2`: Domains 1 and 2\n* `domain_1_2_and_suplementary`: Domains 1, 2 and supplementary analysis",
          "symbols" : [ "domain_1", "domain_1_and_2", "domain_1_2_and_suplementary", "somatic_if_relevant" ]
        },
        "doc" : "Which parts of the WGA were reviewed?"
      }, {
        "name" : "actionableVariants",
        "type" : {
          "type" : "enum",
          "name" : "CancerActionableVariants",
          "doc" : "Are the variants actionable?\n* `yes`: yes\n* `no`: no",
          "symbols" : [ "yes", "no" ]
        },
        "doc" : "Were potentially actionable variants detected?"
      } ]
    },
    "doc" : "Case level questions"
  }, {
    "name" : "cancerSomaticExitQuestionnaires",
    "type" : {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "CancerSomaticVariantLevelQuestionnaire",
        "doc" : "A record holding the somatic variant level questions for a single variant together with its normalized variant coordinates",
        "fields" : [ {
          "name" : "variantCoordinates",
          "type" : {
            "type" : "record",
            "name" : "VariantCoordinates",
            "namespace" : "org.gel.models.report.avro",
            "doc" : "The variant coordinates representing uniquely a small variant.\n    No multi-allelic variant supported, alternate only represents one alternate allele.",
            "fields" : [ {
              "name" : "chromosome",
              "type" : "string",
              "doc" : "Chromosome without \"chr\" prefix (e.g. X rather than chrX)"
            }, {
              "name" : "position",
              "type" : "int",
              "doc" : "Genomic position"
            }, {
              "name" : "reference",
              "type" : "string",
              "doc" : "The reference bases."
            }, {
              "name" : "alternate",
              "type" : "string",
              "doc" : "The alternate bases"
            }, {
              "name" : "assembly",
              "type" : "Assembly",
              "doc" : "The assembly to which this variant corresponds"
            } ]
          },
          "doc" : "The coordinates of a given variant: assembly, chromosome, position, reference and alternate"
        }, {
          "name" : "variantLevelQuestions",
          "type" : {
            "type" : "record",
            "name" : "CancerSomaticVariantLevelQuestions",
            "namespace" : "org.gel.models.report.avro",
            "doc" : "The questions for the cancer program exit questionnaire for somatic variants",
            "fields" : [ {
              "name" : "variantCoordinates",
              "type" : "VariantCoordinates",
              "doc" : "Variant coordinates following format `chromosome:position:reference:alternate`"
            }, {
              "name" : "variantActionability",
              "type" : {
                "type" : "array",
                "items" : {
                  "type" : "enum",
                  "name" : "CancerActionabilitySomatic",
                  "doc" : "The variant actionabilities:\n* `predicts_therapeutic_response`: Predicts therapeutic response\n* `prognostic`: Prognostic\n* `defines_diagnosis_group`: Defines diagnosis group\n* `eligibility_for_trial`: Eligibility for trial\n* `other`:  Other (please specify)",
                  "symbols" : [ "predicts_therapeutic_response", "prognostic", "defines_diagnosis_group", "eligibility_for_trial", "other" ]
                }
              },
              "doc" : "Type of potential actionability:"
            }, {
              "name" : "otherVariantActionability",
              "type" : [ "null", "string" ],
              "doc" : "Other information about variant actionability"
            }, {
              "name" : "variantUsability",
              "type" : {
                "type" : "enum",
                "name" : "CancerUsabilitySomatic",
                "doc" : "Variant usability for somatic variants:\n* `already_actioned`: Already actioned (i.e. prior to receiving this WGA)\n* `actioned_result_of_this_wga`: actioned as a result of receiving this WGA\n* `not_yet_actioned`: not yet actioned, but potentially actionable in the future",
                "symbols" : [ "already_actioned", "actioned_result_of_this_wga", "not_yet_actioned" ]
              },
              "doc" : "How has/will this potentially actionable variant been/be used?"
            }, {
              "name" : "variantTested",
              "type" : {
                "type" : "enum",
                "name" : "CancerTested",
                "doc" : "Was the variant validated with an orthogonal technology?\n* `not_indicated_for_patient_care`: No: not indicated for patient care at this time\n* `no_orthologous_test_available`: No: no orthologous test available\n* `test_performed_prior_to_wga`: Yes: test performed prior to receiving WGA (eg using standard-of-care assay such as panel testing, or sanger sequencing)\n* `technical_validation_following_WGA`: Yes: technical validation performed/planned following receiving this WGA",
                "symbols" : [ "not_indicated_for_patient_care", "no_orthologous_test_available", "test_performed_prior_to_wga", "technical_validation_following_wga" ]
              },
              "doc" : "Has this variant been tested by another method (either prior to or following receipt of this WGA)?"
            }, {
              "name" : "validationAssayType",
              "type" : "string",
              "doc" : "Please enter validation assay type e.g Pyrosequencing, NGS panel, COBAS, Sanger sequencing. If not applicable enter NA;"
            } ]
          },
          "doc" : "The questions at variant level"
        } ]
      }
    },
    "doc" : "Cancer somatic exit questionnaire"
  }, {
    "name" : "cancerGermlineExitQuestionnaires",
    "type" : {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "CancerGermlineVariantLevelQuestionnaire",
        "doc" : "A record holding the germline variant level questions for a single variant together with its normalized variant coordinates",
        "fields" : [ {
          "name" : "variantCoordinates",
          "type" : "org.gel.models.report.avro.VariantCoordinates",
          "doc" : "The coordinates of a given variant: assembly, chromosome, position, reference and alternate"
        }, {
          "name" : "variantLevelQuestions",
          "type" : {
            "type" : "record",
            "name" : "CancerGermlineVariantLevelQuestions",
            "namespace" : "org.gel.models.report.avro",
            "doc" : "The questions for the cancer program exit questionnaire for germline variants",
            "fields" : [ {
              "name" : "variantCoordinates",
              "type" : "VariantCoordinates",
              "doc" : "Variant coordinates following format `chromosome:position:reference:alternate`"
            }, {
              "name" : "variantActionability",
              "type" : {
                "type" : "array",
                "items" : {
                  "type" : "enum",
                  "name" : "CancerActionability",
                  "doc" : "An enumeration Variant Actionability:\n      * `predicts_therapeutic_response`: Predicts therapeutic response\n      * `prognostic`: Prognostic\n      * `defines_diagnosis_group`: Defines diagnosis group\n      * `eligibility_for_trial`: Eligibility for trial\n      * `germline_susceptibility`: Germline susceptibility\n      * `other`:  Other (please specify)",
                  "symbols" : [ "germline_susceptibility", "predicts_therapeutic_response", "prognostic", "defines_diagnosis_group", "eligibility_for_trial", "other" ]
                }
              },
              "doc" : "Type of potential actionability:"
            }, {
              "name" : "otherVariantActionability",
              "type" : [ "null", "string" ]
            }, {
              "name" : "variantUsability",
              "type" : {
                "type" : "enum",
                "name" : "CancerUsabilityGermline",
                "doc" : "Variant usability for germline variants:\n* `already_actioned`: Already actioned (i.e. prior to receiving this WGA)\n* `actioned_result_of_this_wga`: actioned as a result of receiving this WGA",
                "symbols" : [ "already_actioned", "actioned_result_of_this_wga" ]
              },
              "doc" : "How has/will this potentially actionable variant been/be used?"
            }, {
              "name" : "variantTested",
              "type" : "CancerTested",
              "doc" : "Has this variant been tested by another method (either prior to or following receipt of this WGA)?"
            }, {
              "name" : "validationAssayType",
              "type" : "string",
              "doc" : "Please enter validation assay type e.g Pyrosequencing, NGS panel, COBAS, Sanger sequencing. If not applicable enter NA;"
            } ]
          },
          "doc" : "The questions at variant level"
        } ]
      }
    },
    "doc" : "Germline somatic exit questionnaire"
  }, {
    "name" : "additionalComments",
    "type" : [ "null", "string" ],
    "doc" : "Please enter any additional comments you may have about the case here."
  }, {
    "name" : "otherActionableVariants",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "AdditionalVariantsQuestions",
        "namespace" : "org.gel.models.report.avro",
        "fields" : [ {
          "name" : "variantCoordinates",
          "type" : "VariantCoordinates",
          "doc" : "Chr: Pos Ref > Alt"
        }, {
          "name" : "variantActionability",
          "type" : {
            "type" : "array",
            "items" : "CancerActionability"
          },
          "doc" : "Type of potential actionability:"
        }, {
          "name" : "otherVariantActionability",
          "type" : [ "null", "string" ]
        }, {
          "name" : "variantUsability",
          "type" : "CancerUsabilitySomatic",
          "doc" : "How has/will this potentially actionable variant been/be used?"
        }, {
          "name" : "variantTested",
          "type" : {
            "type" : "enum",
            "name" : "CancerTestedAdditional",
            "doc" : "An enumeration Variant tested:\n      * `not_indicated_for_patient_care`: No: not indicated for patient care at this time\n      * `no_orthologous_test_available`: No: no orthologous test available\n      * `test_performed_prior_to_wga`: Yes: test performed prior to receiving WGA (eg using standard-of-care assay such as panel testing, or sanger sequencing)\n      * `technical_validation_following_wga`: Yes: technical validation performed/planned following receiving this WGA\n      * `na`: N/A",
            "symbols" : [ "not_indicated_for_patient_care", "no_orthologous_test_available", "test_performed_prior_to_wga", "technical_validation_following_wga", "na" ]
          },
          "doc" : "Has this variant been tested by another method (either prior to or following receipt of this WGA)?"
        }, {
          "name" : "validationAssayType",
          "type" : "string",
          "doc" : "Please enter validation assay type e.g Pyrosequencing, NGS panel, COBAS, Sanger sequencing. If not applicable enter NA;"
        } ]
      }
    } ],
    "doc" : "Other actionable variants or entities.\n        Please provide other (potentially) actionable entities: e.g domain 3 small variants,\n        SV/CNV, mutational signatures, mutational burden"
  } ]
}
