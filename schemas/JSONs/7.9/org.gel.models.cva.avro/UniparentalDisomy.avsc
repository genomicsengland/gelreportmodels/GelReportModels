{
  "type" : "record",
  "name" : "UniparentalDisomy",
  "namespace" : "org.gel.models.report.avro",
  "fields" : [ {
    "name" : "assembly",
    "type" : {
      "type" : "enum",
      "name" : "Assembly",
      "doc" : "The reference genome assembly",
      "symbols" : [ "GRCh38", "GRCh37" ]
    },
    "doc" : "The assembly"
  }, {
    "name" : "chromosome",
    "type" : "string",
    "doc" : "Chromosome where two homologues were inherited from one parent"
  }, {
    "name" : "complete",
    "type" : [ "null", "boolean" ],
    "doc" : "indicates Whether the UPD event involves an entire chromosome or part of a chromosome"
  }, {
    "name" : "origin",
    "type" : {
      "type" : "enum",
      "name" : "UniparentalDisomyOrigin",
      "symbols" : [ "paternal", "maternal", "unknown" ]
    },
    "doc" : "The parent who contributed two chromosomes was the mother (maternal) or the father (paternal)"
  }, {
    "name" : "uniparentalDisomyFragments",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "UniparentalDisomyFragment",
        "fields" : [ {
          "name" : "coordinates",
          "type" : [ "null", {
            "type" : "record",
            "name" : "Coordinates",
            "fields" : [ {
              "name" : "assembly",
              "type" : "Assembly",
              "doc" : "The assembly to which this variant corresponds"
            }, {
              "name" : "chromosome",
              "type" : "string",
              "doc" : "Chromosome without \"chr\" prefix (e.g. X rather than chrX)"
            }, {
              "name" : "start",
              "type" : "int",
              "doc" : "Start genomic position for variant (1-based)"
            }, {
              "name" : "end",
              "type" : "int",
              "doc" : "End genomic position for variant"
            }, {
              "name" : "ciStart",
              "type" : [ "null", {
                "type" : "record",
                "name" : "ConfidenceInterval",
                "fields" : [ {
                  "name" : "left",
                  "type" : "int"
                }, {
                  "name" : "right",
                  "type" : "int"
                } ]
              } ]
            }, {
              "name" : "ciEnd",
              "type" : [ "null", "ConfidenceInterval" ]
            } ]
          } ],
          "doc" : "Coordinates can be specified to indicate the part of the chromosome affected"
        }, {
          "name" : "uniparentalDisomyType",
          "type" : {
            "type" : "enum",
            "name" : "UniparentalDisomyType",
            "symbols" : [ "isodisomy", "heterodisomy", "both" ]
          },
          "doc" : "indicates whether the UPD event involves `isodisomy`, `heterodisomy` or `both`"
        } ]
      }
    } ],
    "doc" : "List of all of the UPD fragments for this UPD event"
  }, {
    "name" : "participantId",
    "type" : "string",
    "doc" : "Participant affected by this UPD"
  }, {
    "name" : "uniparentalDisomyEvidences",
    "type" : [ "null", {
      "type" : "record",
      "name" : "UniparentalDisomyEvidences",
      "fields" : [ {
        "name" : "ibds",
        "type" : [ "null", {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "IdentityByDescent",
            "fields" : [ {
              "name" : "relatedSample",
              "type" : "string"
            }, {
              "name" : "ibd0",
              "type" : "float"
            }, {
              "name" : "ibd1",
              "type" : "float"
            }, {
              "name" : "ibd2",
              "type" : "float"
            }, {
              "name" : "pihat",
              "type" : "float"
            } ]
          }
        } ]
      } ]
    } ],
    "doc" : "Evidences for the UPD call"
  } ]
}
