{
  "type" : "record",
  "name" : "VariantAttributes",
  "namespace" : "org.gel.models.report.avro",
  "doc" : "Some additional variant attributes",
  "fields" : [ {
    "name" : "genomicChanges",
    "type" : [ "null", {
      "type" : "array",
      "items" : "string"
    } ],
    "doc" : "gDNA change, HGVS nomenclature (e.g.: g.476A>T)"
  }, {
    "name" : "cdnaChanges",
    "type" : [ "null", {
      "type" : "array",
      "items" : "string"
    } ],
    "doc" : "cDNA change, HGVS nomenclature (e.g.: c.76A>T)"
  }, {
    "name" : "proteinChanges",
    "type" : [ "null", {
      "type" : "array",
      "items" : "string"
    } ],
    "doc" : "Protein change, HGVS nomenclature (e.g.: p.Lys76Asn)"
  }, {
    "name" : "additionalTextualVariantAnnotations",
    "type" : [ "null", {
      "type" : "map",
      "values" : "string"
    } ],
    "doc" : "Any additional information in a free text field. For example a quote from a paper"
  }, {
    "name" : "references",
    "type" : [ "null", {
      "type" : "map",
      "values" : "string"
    } ],
    "doc" : "Additional references for ths variant. For example HGMD ID or Pubmed Id"
  }, {
    "name" : "variantIdentifiers",
    "type" : [ "null", {
      "type" : "record",
      "name" : "VariantIdentifiers",
      "fields" : [ {
        "name" : "dbSnpId",
        "type" : [ "null", "string" ],
        "doc" : "Variant identifier in dbSNP"
      }, {
        "name" : "cosmicIds",
        "type" : [ "null", {
          "type" : "array",
          "items" : "string"
        } ],
        "doc" : "Variant identifier in Cosmic"
      }, {
        "name" : "clinVarIds",
        "type" : [ "null", {
          "type" : "array",
          "items" : "string"
        } ],
        "doc" : "Variant identifier in ClinVar"
      }, {
        "name" : "otherIds",
        "type" : [ "null", {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "Identifier",
            "fields" : [ {
              "name" : "source",
              "type" : "string",
              "doc" : "Source i.e, esenmbl"
            }, {
              "name" : "identifier",
              "type" : "string",
              "doc" : "identifier"
            } ]
          }
        } ]
      } ]
    } ]
  }, {
    "name" : "alleleFrequencies",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "AlleleFrequency",
        "doc" : "The population allele frequency of a given variant in a given study and optionally population",
        "fields" : [ {
          "name" : "study",
          "type" : "string",
          "doc" : "The study from where this data comes from"
        }, {
          "name" : "population",
          "type" : "string",
          "doc" : "The specific population where this allele frequency belongs"
        }, {
          "name" : "alternateFrequency",
          "type" : "float",
          "doc" : "The frequency of the alternate allele"
        } ]
      }
    } ],
    "doc" : "A list of population allele frequencies"
  }, {
    "name" : "additionalNumericVariantAnnotations",
    "type" : [ "null", {
      "type" : "map",
      "values" : "float"
    } ],
    "doc" : "Additional numeric variant annotations for this variant. For Example (Allele Frequency, sift, polyphen,\n        mutationTaster, CADD. ..)"
  }, {
    "name" : "comments",
    "type" : [ "null", {
      "type" : "array",
      "items" : "string"
    } ],
    "doc" : "Comments"
  }, {
    "name" : "alleleOrigins",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "enum",
        "name" : "AlleleOrigin",
        "doc" : "Allele origin.\n\n* `SO_0001781`: de novo variant. http://purl.obolibrary.org/obo/SO_0001781\n* `SO_0001778`: germline variant. http://purl.obolibrary.org/obo/SO_0001778\n* `SO_0001775`: maternal variant. http://purl.obolibrary.org/obo/SO_0001775\n* `SO_0001776`: paternal variant. http://purl.obolibrary.org/obo/SO_0001776\n* `SO_0001779`: pedigree specific variant. http://purl.obolibrary.org/obo/SO_0001779\n* `SO_0001780`: population specific variant. http://purl.obolibrary.org/obo/SO_0001780\n* `SO_0001777`: somatic variant. http://purl.obolibrary.org/obo/SO_0001777",
        "symbols" : [ "de_novo_variant", "germline_variant", "maternal_variant", "paternal_variant", "pedigree_specific_variant", "population_specific_variant", "somatic_variant" ]
      }
    } ],
    "doc" : "List of allele origins for this variant in this report"
  }, {
    "name" : "ihp",
    "type" : [ "null", "int" ],
    "doc" : "Largest reference interrupted homopolymer length intersecting with the indel"
  }, {
    "name" : "recurrentlyReported",
    "type" : [ "null", "boolean" ],
    "doc" : "Flag indicating if the variant is recurrently reported"
  }, {
    "name" : "fdp50",
    "type" : [ "null", "float" ],
    "doc" : "Average tier1 number of basecalls filtered from original read depth within 50 bases"
  }, {
    "name" : "others",
    "type" : [ "null", {
      "type" : "map",
      "values" : "string"
    } ],
    "doc" : "Map of other attributes where keys are the attribute names and values are the attributes"
  } ]
}
