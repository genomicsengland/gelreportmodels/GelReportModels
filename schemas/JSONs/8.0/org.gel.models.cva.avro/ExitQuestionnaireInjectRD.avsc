{
  "type" : "record",
  "name" : "ExitQuestionnaireInjectRD",
  "namespace" : "org.gel.models.cva.avro",
  "doc" : "Record for exit questionnaire injection as part of the data intake for CVA",
  "fields" : [ {
    "name" : "metadata",
    "type" : {
      "type" : "record",
      "name" : "InjectionMetadata",
      "doc" : "Metadata about injected data",
      "fields" : [ {
        "name" : "reportModelVersion",
        "type" : "string",
        "doc" : "Report avro models version"
      }, {
        "name" : "id",
        "type" : "string",
        "doc" : "The entity identifier"
      }, {
        "name" : "version",
        "type" : "int",
        "doc" : "The entity version. This is a correlative number being the highest value the latest version."
      }, {
        "name" : "caseId",
        "type" : "string",
        "doc" : "The case identifier"
      }, {
        "name" : "caseVersion",
        "type" : "int",
        "doc" : "The case version. This is a correlative number being the highest value the latest version."
      }, {
        "name" : "groupId",
        "type" : "string",
        "doc" : "The family identifier"
      }, {
        "name" : "cohortId",
        "type" : "string",
        "doc" : "The cohort identifier (the same family can have several cohorts)"
      }, {
        "name" : "author",
        "type" : "string",
        "doc" : "The author of the ReportedVariant, either tiering, exomiser, a given cip (e.g.: omicia) or a given GMCs user name"
      }, {
        "name" : "authorVersion",
        "type" : [ "null", "string" ],
        "doc" : "The author version of the ReportedVariant, either tiering, exomiser or a given cip. Only applicable for automated processes."
      }, {
        "name" : "assembly",
        "type" : [ "null", {
          "type" : "enum",
          "name" : "Assembly",
          "namespace" : "org.gel.models.report.avro",
          "doc" : "The reference genome assembly",
          "symbols" : [ "GRCh38", "GRCh37" ]
        } ],
        "doc" : "The assembly to which the variants refer"
      }, {
        "name" : "program",
        "type" : {
          "type" : "enum",
          "name" : "Program",
          "namespace" : "org.gel.models.report.avro",
          "doc" : "The Genomics England program",
          "symbols" : [ "cancer", "rare_disease" ]
        },
        "doc" : "The 100K Genomes program to which the reported variant belongs."
      }, {
        "name" : "category",
        "type" : {
          "type" : "enum",
          "name" : "Category",
          "symbols" : [ "HundredK", "NGIS" ]
        },
        "doc" : "The category to which the case belongs."
      }, {
        "name" : "caseCreationDate",
        "type" : [ "null", "string" ],
        "doc" : "The creation date of the case (ISO-8601)"
      }, {
        "name" : "caseLastModifiedDate",
        "type" : [ "null", "string" ],
        "doc" : "The last modified date of the case (ISO-8601)"
      }, {
        "name" : "organisation",
        "type" : [ "null", {
          "type" : "record",
          "name" : "Organisation",
          "doc" : "An organisation which may own or be assigned to a case",
          "fields" : [ {
            "name" : "ods",
            "type" : "string",
            "doc" : "ODS code"
          }, {
            "name" : "gmc",
            "type" : [ "null", "string" ],
            "doc" : "The GMC name"
          }, {
            "name" : "site",
            "type" : [ "null", "string" ],
            "doc" : "The site name"
          } ]
        } ],
        "doc" : "The organisation responsible for this payload (Pedigree and CancerParticipant will correspond to the case\n        owner and the ClinicalReport will correspond to the case assignee)"
      }, {
        "name" : "organisationNgis",
        "type" : [ "null", {
          "type" : "record",
          "name" : "OrganisationNgis",
          "namespace" : "org.gel.models.participant.avro",
          "fields" : [ {
            "name" : "organisationId",
            "type" : "string",
            "doc" : "Organisation Id"
          }, {
            "name" : "organisationCode",
            "type" : "string",
            "doc" : "Ods code"
          }, {
            "name" : "organisationName",
            "type" : "string",
            "doc" : "Organisation Name"
          }, {
            "name" : "organisationNationalGroupingId",
            "type" : "string",
            "doc" : "National Grouping (GLH) Id"
          }, {
            "name" : "organisationNationalGroupingName",
            "type" : "string",
            "doc" : "National Grouping (GLH) Name"
          } ]
        } ],
        "doc" : "The NGIS organisation responsible for this payload"
      }, {
        "name" : "referralTestId",
        "type" : [ "null", "string" ],
        "doc" : "Test unique identifier (only sent for NGIS cases)"
      }, {
        "name" : "referralId",
        "type" : [ "null", "string" ],
        "doc" : "Referral unique identifier (only sent for NGIS cases)"
      } ]
    },
    "doc" : "Metadata on the exit questionnaire"
  }, {
    "name" : "exitQuestionnaireRd",
    "type" : [ "null", {
      "type" : "record",
      "name" : "ExitQuestionnaireRD",
      "doc" : "This is an entity to hold the information in org.gel.models.report.avro.RareDiseaseExitQuestionnaire in\n    a form compatible with CVA.",
      "fields" : [ {
        "name" : "variants",
        "type" : {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "ReportedVariantQuestionnaireRD",
            "doc" : "This object holds all questionnaire questions together with normalized variant coordinates.",
            "fields" : [ {
              "name" : "variantCoordinates",
              "type" : {
                "type" : "record",
                "name" : "VariantCoordinates",
                "namespace" : "org.gel.models.report.avro",
                "doc" : "The variant coordinates representing uniquely a small variant.\n    No multi-allelic variant supported, alternate only represents one alternate allele.",
                "fields" : [ {
                  "name" : "chromosome",
                  "type" : "string",
                  "doc" : "Chromosome without \"chr\" prefix (e.g. X rather than chrX)"
                }, {
                  "name" : "position",
                  "type" : "int",
                  "doc" : "Genomic position"
                }, {
                  "name" : "reference",
                  "type" : "string",
                  "doc" : "The reference bases."
                }, {
                  "name" : "alternate",
                  "type" : "string",
                  "doc" : "The alternate bases"
                }, {
                  "name" : "assembly",
                  "type" : "Assembly",
                  "doc" : "The assembly to which this variant corresponds"
                } ]
              },
              "doc" : "The normalized representation of variants coordinates"
            }, {
              "name" : "reportEvent",
              "type" : {
                "type" : "record",
                "name" : "ReportEventQuestionnaireRD",
                "doc" : "The report event for a questionnaire in RD.",
                "fields" : [ {
                  "name" : "groupOfVariants",
                  "type" : [ "null", "int" ],
                  "doc" : "The identifier used to group variants together"
                }, {
                  "name" : "variantLevelQuestions",
                  "type" : {
                    "type" : "record",
                    "name" : "VariantLevelQuestions",
                    "namespace" : "org.gel.models.report.avro",
                    "doc" : "The variant level questions",
                    "fields" : [ {
                      "name" : "variantCoordinates",
                      "type" : "VariantCoordinates",
                      "doc" : "Variant coordinates"
                    }, {
                      "name" : "confirmationDecision",
                      "type" : {
                        "type" : "enum",
                        "name" : "ConfirmationDecision",
                        "symbols" : [ "yes", "no", "na" ]
                      },
                      "doc" : "Did you carry out technical confirmation of this variant via an alternative test?"
                    }, {
                      "name" : "confirmationOutcome",
                      "type" : {
                        "type" : "enum",
                        "name" : "ConfirmationOutcome",
                        "symbols" : [ "yes", "no", "na" ]
                      },
                      "doc" : "Did the test confirm that the variant is present?"
                    }, {
                      "name" : "reportingQuestion",
                      "type" : {
                        "type" : "enum",
                        "name" : "ReportingQuestion",
                        "symbols" : [ "yes", "no", "na" ]
                      },
                      "doc" : "Did you include the variant in your report to the clinician?"
                    }, {
                      "name" : "acmgClassification",
                      "type" : {
                        "type" : "enum",
                        "name" : "ACMGClassification",
                        "symbols" : [ "pathogenic_variant", "likely_pathogenic_variant", "variant_of_unknown_clinical_significance", "likely_benign_variant", "benign_variant", "not_assessed", "na" ]
                      },
                      "doc" : "What ACMG pathogenicity score (1-5) did you assign to this variant?"
                    }, {
                      "name" : "publications",
                      "type" : "string",
                      "doc" : "Please provide PMIDs for papers which you have used to inform your assessment for this variant, separated by a `;` for multiple papers"
                    } ]
                  },
                  "doc" : "The variant level questions"
                }, {
                  "name" : "variantGroupLevelQuestions",
                  "type" : {
                    "type" : "record",
                    "name" : "VariantGroupLevelQuestions",
                    "namespace" : "org.gel.models.report.avro",
                    "doc" : "The variant group level questions",
                    "fields" : [ {
                      "name" : "variantGroup",
                      "type" : "int",
                      "doc" : "This value groups variants that together could explain the phenotype according to the mode of inheritance used.\n        (e.g.: compound heterozygous). All the variants in the same report sharing the same value will be considered in\n        the same group (i.e.: reported together). This value is an integer unique in the whole report.\n        These values are only relevant within the same report."
                    }, {
                      "name" : "variantLevelQuestions",
                      "type" : [ "null", {
                        "type" : "array",
                        "items" : "VariantLevelQuestions"
                      } ],
                      "doc" : "Variant level questions for each of the variants in the group"
                    }, {
                      "name" : "shortTandemRepeatLevelQuestions",
                      "type" : [ "null", {
                        "type" : "array",
                        "items" : {
                          "type" : "record",
                          "name" : "ShortTandemRepeatLevelQuestions",
                          "doc" : "The variant level questions",
                          "fields" : [ {
                            "name" : "coordinates",
                            "type" : {
                              "type" : "record",
                              "name" : "Coordinates",
                              "fields" : [ {
                                "name" : "assembly",
                                "type" : "Assembly",
                                "doc" : "The assembly to which this variant corresponds"
                              }, {
                                "name" : "chromosome",
                                "type" : "string",
                                "doc" : "Chromosome without \"chr\" prefix (e.g. X rather than chrX)"
                              }, {
                                "name" : "start",
                                "type" : "int",
                                "doc" : "Start genomic position for variant (1-based)"
                              }, {
                                "name" : "end",
                                "type" : "int",
                                "doc" : "End genomic position for variant"
                              }, {
                                "name" : "ciStart",
                                "type" : [ "null", {
                                  "type" : "record",
                                  "name" : "ConfidenceInterval",
                                  "fields" : [ {
                                    "name" : "left",
                                    "type" : "int"
                                  }, {
                                    "name" : "right",
                                    "type" : "int"
                                  } ]
                                } ]
                              }, {
                                "name" : "ciEnd",
                                "type" : [ "null", "ConfidenceInterval" ]
                              } ]
                            },
                            "doc" : "Variant coordinates"
                          }, {
                            "name" : "confirmationDecision",
                            "type" : "ConfirmationDecision",
                            "doc" : "Did you carry out technical confirmation of this variant via an alternative test?"
                          }, {
                            "name" : "confirmationOutcome",
                            "type" : "ConfirmationOutcome",
                            "doc" : "Did the test confirm that the variant is present?"
                          }, {
                            "name" : "reportingQuestion",
                            "type" : "ReportingQuestion",
                            "doc" : "Did you include the variant in your report to the clinician?"
                          }, {
                            "name" : "acmgClassification",
                            "type" : "ACMGClassification",
                            "doc" : "What ACMG pathogenicity score (1-5) did you assign to this variant?"
                          }, {
                            "name" : "publications",
                            "type" : "string",
                            "doc" : "Please provide PMIDs for papers which you have used to inform your assessment for this variant, separated by a `;` for multiple papers"
                          } ]
                        }
                      } ],
                      "doc" : "STR level questions for each of the variants in the group"
                    }, {
                      "name" : "structuralVariantLevelQuestions",
                      "type" : [ "null", {
                        "type" : "array",
                        "items" : {
                          "type" : "record",
                          "name" : "StructuralVariantLevelQuestions",
                          "doc" : "Structural variant level questions",
                          "fields" : [ {
                            "name" : "variantType",
                            "type" : {
                              "type" : "enum",
                              "name" : "StructuralVariantType",
                              "symbols" : [ "ins", "dup", "inv", "amplification", "deletion", "dup_tandem", "del_me", "ins_me", "cnloh" ]
                            },
                            "doc" : "Structural variant type"
                          }, {
                            "name" : "coordinates",
                            "type" : "Coordinates",
                            "doc" : "Variant coordinates"
                          }, {
                            "name" : "confirmationDecision",
                            "type" : "ConfirmationDecision",
                            "doc" : "Did you carry out technical confirmation of this variant via an alternative test?"
                          }, {
                            "name" : "confirmationOutcome",
                            "type" : "ConfirmationOutcome",
                            "doc" : "Did the test confirm that the variant is present?"
                          }, {
                            "name" : "reportingQuestion",
                            "type" : "ReportingQuestion",
                            "doc" : "Did you include the variant in your report to the clinician?"
                          }, {
                            "name" : "acmgClassification",
                            "type" : "ACMGClassification",
                            "doc" : "What ACMG pathogenicity score (1-5) did you assign to this variant?"
                          }, {
                            "name" : "publications",
                            "type" : "string",
                            "doc" : "Please provide PMIDs for papers which you have used to inform your assessment for this variant, separated by a `;` for multiple papers"
                          } ]
                        }
                      } ],
                      "doc" : "Structural level questions for each of the variants in the group"
                    }, {
                      "name" : "actionability",
                      "type" : {
                        "type" : "enum",
                        "name" : "Actionability",
                        "symbols" : [ "yes", "no", "not_yet", "na" ]
                      },
                      "doc" : "Is evidence for this variant/variant pair sufficient to use it for clinical purposes such as prenatal diagnosis or predictive testing?"
                    }, {
                      "name" : "clinicalUtility",
                      "type" : {
                        "type" : "array",
                        "items" : {
                          "type" : "enum",
                          "name" : "ClinicalUtility",
                          "symbols" : [ "none", "change_in_medication", "surgical_option", "additional_surveillance_for_proband_or_relatives", "clinical_trial_eligibility", "informs_reproductive_choice", "unknown", "other" ]
                        }
                      },
                      "doc" : "Has the clinical team identified any changes to clinical care which could potentially arise as a result of this variant/variant pair?"
                    }, {
                      "name" : "phenotypesSolved",
                      "type" : {
                        "type" : "enum",
                        "name" : "PhenotypesSolved",
                        "symbols" : [ "yes", "no", "partially", "unknown" ]
                      },
                      "doc" : "Did you report the variant(s) as being partially or completely causative of the family's presenting phenotype(s)?"
                    }, {
                      "name" : "phenotypesExplained",
                      "type" : [ "null", {
                        "type" : "array",
                        "items" : "string"
                      } ],
                      "doc" : "If you indicated that the variant(s) only partially explained the family’s presenting phenotypes, please indicate which HPO terms you are confident that they DO explain"
                    } ]
                  },
                  "doc" : "The variant group level questions"
                }, {
                  "name" : "familyLevelQuestions",
                  "type" : {
                    "type" : "record",
                    "name" : "FamilyLevelQuestions",
                    "namespace" : "org.gel.models.report.avro",
                    "doc" : "The family level questions",
                    "fields" : [ {
                      "name" : "caseSolvedFamily",
                      "type" : {
                        "type" : "enum",
                        "name" : "CaseSolvedFamily",
                        "symbols" : [ "yes", "no", "partially", "unknown" ]
                      },
                      "doc" : "Have the results reported here explained the genetic basis of the family’s presenting phenotype(s)?"
                    }, {
                      "name" : "segregationQuestion",
                      "type" : {
                        "type" : "enum",
                        "name" : "SegregationQuestion",
                        "symbols" : [ "yes", "no" ]
                      },
                      "doc" : "Have you done any segregation testing in non-participating family members?"
                    }, {
                      "name" : "additionalComments",
                      "type" : "string",
                      "doc" : "Comments regarding report"
                    } ]
                  },
                  "doc" : "The family level questions"
                } ]
              },
              "doc" : "The questionnaire report event"
            } ]
          }
        },
        "doc" : "The list variant group level questions (this list will be unwinded during ingestion)"
      } ]
    } ],
    "doc" : "Exit questionnaire for rare disease"
  }, {
    "name" : "rareDiseaseExitQuestionnaire",
    "type" : [ "null", {
      "type" : "record",
      "name" : "RareDiseaseExitQuestionnaire",
      "namespace" : "org.gel.models.report.avro",
      "doc" : "The rare disease program exit questionnaire",
      "fields" : [ {
        "name" : "eventDate",
        "type" : "string",
        "doc" : "The date when the questionnaire was submitted"
      }, {
        "name" : "reporter",
        "type" : "string",
        "doc" : "The person that submitted the questionnaire"
      }, {
        "name" : "familyLevelQuestions",
        "type" : "FamilyLevelQuestions",
        "doc" : "The set of questions at family level"
      }, {
        "name" : "variantGroupLevelQuestions",
        "type" : {
          "type" : "array",
          "items" : "VariantGroupLevelQuestions"
        },
        "doc" : "The list of variant group level variants (ungrouped variants are to be set in single variant group)"
      } ]
    } ],
    "doc" : "Rare disease exit questionnaire"
  } ]
}
