{
  "type" : "record",
  "name" : "InjectionMetadata",
  "namespace" : "org.gel.models.cva.avro",
  "doc" : "Metadata about injected data",
  "fields" : [ {
    "name" : "reportModelVersion",
    "type" : "string",
    "doc" : "Report avro models version"
  }, {
    "name" : "id",
    "type" : "string",
    "doc" : "The entity identifier"
  }, {
    "name" : "version",
    "type" : "int",
    "doc" : "The entity version. This is a correlative number being the highest value the latest version."
  }, {
    "name" : "caseId",
    "type" : "string",
    "doc" : "The case identifier"
  }, {
    "name" : "caseVersion",
    "type" : "int",
    "doc" : "The case version. This is a correlative number being the highest value the latest version."
  }, {
    "name" : "groupId",
    "type" : "string",
    "doc" : "The family identifier"
  }, {
    "name" : "cohortId",
    "type" : "string",
    "doc" : "The cohort identifier (the same family can have several cohorts)"
  }, {
    "name" : "author",
    "type" : "string",
    "doc" : "The author of the ReportedVariant, either tiering, exomiser, a given cip (e.g.: omicia) or a given GMCs user name"
  }, {
    "name" : "authorVersion",
    "type" : [ "null", "string" ],
    "doc" : "The author version of the ReportedVariant, either tiering, exomiser or a given cip. Only applicable for automated processes."
  }, {
    "name" : "assembly",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "Assembly",
      "namespace" : "org.gel.models.report.avro",
      "doc" : "The reference genome assembly",
      "symbols" : [ "GRCh38", "GRCh37" ]
    } ],
    "doc" : "The assembly to which the variants refer"
  }, {
    "name" : "program",
    "type" : {
      "type" : "enum",
      "name" : "Program",
      "namespace" : "org.gel.models.report.avro",
      "doc" : "The Genomics England program",
      "symbols" : [ "cancer", "rare_disease" ]
    },
    "doc" : "The 100K Genomes program to which the reported variant belongs."
  }, {
    "name" : "category",
    "type" : {
      "type" : "enum",
      "name" : "Category",
      "symbols" : [ "HundredK", "NGIS" ]
    },
    "doc" : "The category to which the case belongs."
  }, {
    "name" : "caseCreationDate",
    "type" : [ "null", "string" ],
    "doc" : "The creation date of the case (ISO-8601)"
  }, {
    "name" : "caseLastModifiedDate",
    "type" : [ "null", "string" ],
    "doc" : "The last modified date of the case (ISO-8601)"
  }, {
    "name" : "organisation",
    "type" : [ "null", {
      "type" : "record",
      "name" : "Organisation",
      "doc" : "An organisation which may own or be assigned to a case",
      "fields" : [ {
        "name" : "ods",
        "type" : "string",
        "doc" : "ODS code"
      }, {
        "name" : "gmc",
        "type" : [ "null", "string" ],
        "doc" : "The GMC name"
      }, {
        "name" : "site",
        "type" : [ "null", "string" ],
        "doc" : "The site name"
      } ]
    } ],
    "doc" : "The organisation responsible for this payload (Pedigree and CancerParticipant will correspond to the case\n        owner and the ClinicalReport will correspond to the case assignee)"
  }, {
    "name" : "organisationNgis",
    "type" : [ "null", {
      "type" : "record",
      "name" : "OrganisationNgis",
      "namespace" : "org.gel.models.participant.avro",
      "fields" : [ {
        "name" : "organisationId",
        "type" : "string",
        "doc" : "Organisation Id"
      }, {
        "name" : "organisationCode",
        "type" : "string",
        "doc" : "Ods code"
      }, {
        "name" : "organisationName",
        "type" : "string",
        "doc" : "Organisation Name"
      }, {
        "name" : "organisationNationalGroupingId",
        "type" : "string",
        "doc" : "National Grouping (GLH) Id"
      }, {
        "name" : "organisationNationalGroupingName",
        "type" : "string",
        "doc" : "National Grouping (GLH) Name"
      } ]
    } ],
    "doc" : "The NGIS organisation responsible for this payload"
  }, {
    "name" : "referralTestId",
    "type" : [ "null", "string" ],
    "doc" : "Test unique identifier (only sent for NGIS cases)"
  }, {
    "name" : "referralId",
    "type" : [ "null", "string" ],
    "doc" : "Referral unique identifier (only sent for NGIS cases)"
  } ]
}
