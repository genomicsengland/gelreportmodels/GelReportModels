{
  "type" : "record",
  "name" : "VariantAttributes",
  "namespace" : "org.gel.models.report.avro",
  "doc" : "Some additional variant attributes",
  "fields" : [ {
    "name" : "evidenceEntries",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "EvidenceEntry",
        "doc" : "An entry for an evidence",
        "fields" : [ {
          "name" : "source",
          "type" : {
            "type" : "record",
            "name" : "EvidenceSource",
            "doc" : "The source of an evidence.",
            "fields" : [ {
              "name" : "name",
              "type" : [ "null", "string" ],
              "doc" : "Name of source"
            }, {
              "name" : "version",
              "type" : [ "null", "string" ],
              "doc" : "Version of source"
            }, {
              "name" : "date",
              "type" : [ "null", "string" ],
              "doc" : "The source date."
            } ]
          },
          "doc" : "Source of the evidence"
        }, {
          "name" : "submissions",
          "type" : {
            "type" : "array",
            "items" : {
              "type" : "record",
              "name" : "EvidenceSubmission",
              "doc" : "The submission information",
              "fields" : [ {
                "name" : "submitter",
                "type" : [ "null", "string" ],
                "doc" : "The submitter"
              }, {
                "name" : "date",
                "type" : [ "null", "string" ],
                "doc" : "The submission date"
              }, {
                "name" : "id",
                "type" : [ "null", "string" ],
                "doc" : "The submission id"
              } ]
            }
          },
          "doc" : "The list of submissions",
          "default" : [ ]
        }, {
          "name" : "somaticInformation",
          "type" : [ "null", {
            "type" : "record",
            "name" : "SomaticInformation",
            "doc" : "The somatic information.",
            "fields" : [ {
              "name" : "primarySite",
              "type" : [ "null", "string" ],
              "doc" : "The primary site"
            }, {
              "name" : "siteSubtype",
              "type" : [ "null", "string" ],
              "doc" : "The primary site subtype"
            }, {
              "name" : "primaryHistology",
              "type" : [ "null", "string" ],
              "doc" : "The primary histology"
            }, {
              "name" : "histologySubtype",
              "type" : [ "null", "string" ],
              "doc" : "The histology subtype"
            }, {
              "name" : "tumourOrigin",
              "type" : [ "null", "string" ],
              "doc" : "The tumour origin"
            }, {
              "name" : "sampleSource",
              "type" : [ "null", "string" ],
              "doc" : "The sample source, e.g. blood-bone marrow, cell-line, pancreatic"
            } ]
          } ],
          "doc" : "The somatic information"
        }, {
          "name" : "url",
          "type" : [ "null", "string" ],
          "doc" : "URL of source if any"
        }, {
          "name" : "id",
          "type" : [ "null", "string" ],
          "doc" : "ID of record in the source"
        }, {
          "name" : "assembly",
          "type" : [ "null", "string" ],
          "doc" : "The reference genome assembly"
        }, {
          "name" : "alleleOrigin",
          "type" : [ "null", {
            "type" : "array",
            "items" : {
              "type" : "enum",
              "name" : "AlleleOrigin",
              "doc" : "Variant origin.\n    * `SO_0001781`: de novo variant. http://purl.obolibrary.org/obo/SO_0001781\n    * `SO_0001778`: germline variant. http://purl.obolibrary.org/obo/SO_0001778\n    * `SO_0001775`: maternal variant. http://purl.obolibrary.org/obo/SO_0001775\n    * `SO_0001776`: paternal variant. http://purl.obolibrary.org/obo/SO_0001776\n    * `SO_0001779`: pedigree specific variant. http://purl.obolibrary.org/obo/SO_0001779\n    * `SO_0001780`: population specific variant. http://purl.obolibrary.org/obo/SO_0001780\n    * `SO_0001777`: somatic variant. http://purl.obolibrary.org/obo/SO_0001777",
              "symbols" : [ "de_novo_variant", "germline_variant", "maternal_variant", "paternal_variant", "pedigree_specific_variant", "population_specific_variant", "somatic_variant" ]
            }
          } ],
          "doc" : "List of allele origins"
        }, {
          "name" : "heritableTraits",
          "type" : {
            "type" : "array",
            "items" : {
              "type" : "record",
              "name" : "HeritableTrait",
              "doc" : "The entity representing a phenotype and its inheritance pattern.",
              "fields" : [ {
                "name" : "trait",
                "type" : [ "null", "string" ],
                "doc" : "The trait (e.g.: HPO term, MIM term, DO term etc.)"
              }, {
                "name" : "inheritanceMode",
                "type" : [ "null", {
                  "type" : "enum",
                  "name" : "ModeOfInheritance",
                  "doc" : "An enumeration for the different mode of inheritances:\n\n* `monoallelic_not_imprinted`: MONOALLELIC, autosomal or pseudoautosomal, not imprinted\n* `monoallelic_maternally_imprinted`: MONOALLELIC, autosomal or pseudoautosomal, maternally imprinted (paternal allele expressed)\n* `monoallelic_paternally_imprinted`: MONOALLELIC, autosomal or pseudoautosomal, paternally imprinted (maternal allele expressed)\n* `monoallelic`: MONOALLELIC, autosomal or pseudoautosomal, imprinted status unknown\n* `biallelic`: BIALLELIC, autosomal or pseudoautosomal\n* `monoallelic_and_biallelic`: BOTH monoallelic and biallelic, autosomal or pseudoautosomal\n* `monoallelic_and_more_severe_biallelic`: BOTH monoallelic and biallelic, autosomal or pseudoautosomal (but BIALLELIC mutations cause a more SEVERE disease form), autosomal or pseudoautosomal\n* `xlinked_biallelic`: X-LINKED: hemizygous mutation in males, biallelic mutations in females\n* `xlinked_monoallelic`: X linked: hemizygous mutation in males, monoallelic mutations in females may cause disease (may be less severe, later onset than males)\n* `mitochondrial`: MITOCHONDRIAL\n* `unknown`: Unknown",
                  "symbols" : [ "monoallelic", "monoallelic_not_imprinted", "monoallelic_maternally_imprinted", "monoallelic_paternally_imprinted", "biallelic", "monoallelic_and_biallelic", "monoallelic_and_more_severe_biallelic", "xlinked_biallelic", "xlinked_monoallelic", "mitochondrial", "unknown", "na" ]
                } ],
                "doc" : "The mode of inheritance"
              } ]
            }
          },
          "doc" : "Heritable traits associated to this evidence",
          "default" : [ ]
        }, {
          "name" : "genomicFeatures",
          "type" : {
            "type" : "array",
            "items" : {
              "type" : "record",
              "name" : "GenomicFeature",
              "doc" : "The genomic feature",
              "fields" : [ {
                "name" : "featureType",
                "type" : [ "null", {
                  "type" : "enum",
                  "name" : "FeatureTypes",
                  "doc" : "The feature types",
                  "symbols" : [ "regulatory_region", "gene", "transcript", "protein" ]
                } ],
                "doc" : "Feature Type"
              }, {
                "name" : "ensemblId",
                "type" : [ "null", "string" ],
                "doc" : "Feature used, this should be a feature ID from Ensembl, (i.e, ENST00000544455)"
              }, {
                "name" : "xrefs",
                "type" : [ "null", {
                  "type" : "map",
                  "values" : "string"
                } ],
                "doc" : "Others IDs. Fields like the HGNC symbol if available should be added here"
              } ]
            }
          },
          "doc" : "The transcript to which the evidence refers",
          "default" : [ ]
        }, {
          "name" : "variantClassification",
          "type" : [ "null", {
            "type" : "record",
            "name" : "VariantClassification",
            "doc" : "The variant classification according to different properties.",
            "fields" : [ {
              "name" : "clinicalSignificance",
              "type" : [ "null", {
                "type" : "enum",
                "name" : "ClinicalSignificance",
                "symbols" : [ "benign", "likely_benign", "likely_pathogenic", "pathogenic", "uncertain_significance", "excluded" ]
              } ],
              "doc" : "The variant's clinical significance."
            }, {
              "name" : "drugResponseClassification",
              "type" : [ "null", {
                "type" : "enum",
                "name" : "DrugResponseClassification",
                "symbols" : [ "altered_sensitivity", "reduced_sensitivity", "increased_sensitivity", "altered_resistance", "increased_resistance", "reduced_resistance", "increased_risk_of_toxicity", "reduced_risk_of_toxicity", "altered_toxicity", "adverse_drug_reaction", "indication", "contraindication", "dosing_alteration", "increased_dose", "reduced_dose", "increased_monitoring", "increased_efficacy", "reduced_efficacy", "altered_efficacy" ]
              } ],
              "doc" : "The variant's pharmacogenomics classification."
            }, {
              "name" : "traitAssociation",
              "type" : [ "null", {
                "type" : "enum",
                "name" : "TraitAssociation",
                "symbols" : [ "established_risk_allele", "likely_risk_allele", "uncertain_risk_allele", "protective" ]
              } ],
              "doc" : "The variant's trait association."
            }, {
              "name" : "tumorigenesisClassification",
              "type" : [ "null", {
                "type" : "enum",
                "name" : "TumorigenesisClassification",
                "symbols" : [ "driver", "passenger", "modifier" ]
              } ],
              "doc" : "The variant's tumorigenesis classification."
            }, {
              "name" : "functionalEffect",
              "type" : [ "null", {
                "type" : "enum",
                "name" : "VariantFunctionalEffect",
                "symbols" : [ "dominant_negative_variant", "gain_of_function_variant", "lethal_variant", "loss_of_function_variant", "loss_of_heterozygosity", "null_variant" ]
              } ],
              "doc" : "The variant functional effect"
            } ]
          } ],
          "doc" : "The variant classification"
        }, {
          "name" : "impact",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "EvidenceImpact",
            "doc" : "Evidence of pathogenicity and benign impact as defined in Richards, S. et al. (2015). Standards and guidelines for the interpretation\n    of sequence variants: a joint consensus recommendation of the American College of Medical Genetics and Genomics and\n    the Association for Molecular Pathology. Genetics in Medicine, 17(5), 405–423. https://doi.org/10.1038/gim.2015.30\n\nEvidence of pathogenicity:\n* `very_strong`:\n    - PVS1 null variant (nonsense, frameshift, canonical ±1 or 2 splice sites, initiation codon, single or multiexon\n    deletion) in a gene where LOF is a known mechanism of disease\n* `strong`:\n    - PS1 Same amino acid change as a previously established pathogenic variant regardless of nucleotide change\n    - PS2 De novo (both maternity and paternity confirmed) in a patient with the disease and no family history\n    - PS3 Well-established in vitro or in vivo functional studies supportive of a damaging effect on the gene or gene\n    product\n    - PS4 The prevalence of the variant in affected individuals is significantly increased compared with the prevalence\n    in controls\n* `moderate`:\n    - PM1 Located in a mutational hot spot and/or critical and well-established functional domain (e.g., active site of\n    an enzyme) without benign variation\n    - PM2 Absent from controls (or at extremely low frequency if recessive) in Exome Sequencing Project, 1000 Genomes\n    Project, or Exome Aggregation Consortium\n    - PM3 For recessive disorders, detected in trans with a pathogenic variant\n    - PM4 Protein length changes as a result of in-frame deletions/insertions in a nonrepeat region or stop-loss\n    variants\n    - PM5 Novel missense change at an amino acid residue where a different missense change determined to be pathogenic\n    has been seen before\n    - PM6 Assumed de novo, but without confirmation of paternity and maternity\n* `supporting`:\n    - PP1 Cosegregation with disease in multiple affected family members in a gene definitively known to cause the\n    disease\n    - PP2 Missense variant in a gene that has a low rate of benign missense variation and in which missense variants are\n    a common mechanism of disease\n    - PP3 Multiple lines of computational evidence support a deleterious effect on the gene or gene product\n    (conservation, evolutionary, splicing impact, etc.)\n    - PP4 Patient’s phenotype or family history is highly specific for a disease with a single genetic etiology\n    - PP5 Reputable source recently reports variant as pathogenic, but the evidence is not available to the laboratory\n    to perform an independent evaluation\n\nEvidence of benign impact:\n* `stand_alone`:\n    - BA1 Allele frequency is >5% in Exome Sequencing Project, 1000 Genomes Project, or Exome Aggregation\n    Consortium\n* `strong`:\n    - BS1 Allele frequency is greater than expected for disorder\n    - BS2 Observed in a healthy adult individual for a recessive (homozygous), dominant (heterozygous), or X-linked\n    (hemizygous) disorder, with full penetrance expected at an early age\n    - BS3 Well-established in vitro or in vivo functional studies show no damaging effect on protein function or\n    splicing\n    - BS4 Lack of segregation in affected members of a family\n* `supporting`:\n    - BP1 Missense variant in a gene for which primarily truncating variants are known to cause disease\n    - BP2 Observed in trans with a pathogenic variant for a fully penetrant dominant gene/disorder or observed in cis\n    with a pathogenic variant in any inheritance pattern\n    - BP3 In-frame deletions/insertions in a repetitive region without a known function\n    - BP4 Multiple lines of computational evidence suggest no impact on gene or gene product (conservation,\n    evolutionary, splicing impact, etc.)\n    - BP5 Variant found in a case with an alternate molecular basis for disease\n    - BP6 Reputable source recently reports variant as benign, but the evidence is not available to the laboratory to\n    perform an independent evaluation\n    - BP7 A synonymous (silent) variant for which splicing prediction algorithms predict no impact to the splice\n    consensus sequence nor the creation of a new splice site AND the nucleotide is not highly conserved",
            "symbols" : [ "very_strong", "strong", "moderate", "supporting", "stand_alone" ]
          } ],
          "doc" : "Impact of evidence. Should be coherent with the classification of impact if provided."
        }, {
          "name" : "confidence",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "Confidence",
            "doc" : "Confidence based on the Confidence Information Ontology\n\n    * `CIO_0000029`: high confidence level http://purl.obolibrary.org/obo/CIO_0000029\n    * `CIO_0000031`: low confidence level http://purl.obolibrary.org/obo/CIO_0000031\n    * `CIO_0000030`: medium confidence level http://purl.obolibrary.org/obo/CIO_0000030\n    * `CIO_0000039`: rejected http://purl.obolibrary.org/obo/CIO_0000039",
            "symbols" : [ "low_confidence_level", "medium_confidence_level", "high_confidence_level", "rejected" ]
          } ],
          "doc" : "The curation confidence."
        }, {
          "name" : "consistencyStatus",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "ConsistencyStatus",
            "doc" : "The consistency of evidences for a given phenotype. This aggregates all evidences for a given phenotype and all\n    evidences with no phenotype associated (e.g.: in silico impact prediction, population frequency).\n    This is based on the Confidence Information Ontology terms.\n\n    * `CIO_0000033`: congruent, all evidences are consistent. http://purl.obolibrary.org/obo/CIO_0000033\n    * `CIO_0000034`: conflict, there are conflicting evidences. This should correspond to a `VariantClassification` of\n    `uncertain_significance` for mendelian disorders. http://purl.obolibrary.org/obo/CIO_0000034\n    * `CIO_0000035`: strongly conflicting. http://purl.obolibrary.org/obo/CIO_0000035\n    * `CIO_0000036`: weakly conflicting. http://purl.obolibrary.org/obo/CIO_0000036",
            "symbols" : [ "congruent", "conflict", "weakly_conflicting", "strongly_conflicting" ]
          } ],
          "doc" : "The consistency status. This is applicable to complex evidences (e.g.: ClinVar)"
        }, {
          "name" : "ethnicity",
          "type" : {
            "type" : "enum",
            "name" : "EthnicCategory",
            "namespace" : "org.gel.models.participant.avro",
            "doc" : "This is the list of ethnicities in ONS16\n\n    * `D`:  Mixed: White and Black Caribbean\n    * `E`:  Mixed: White and Black African\n    * `F`:  Mixed: White and Asian\n    * `G`:  Mixed: Any other mixed background\n    * `A`:  White: British\n    * `B`:  White: Irish\n    * `C`:  White: Any other White background\n    * `L`:  Asian or Asian British: Any other Asian background\n    * `M`:  Black or Black British: Caribbean\n    * `N`:  Black or Black British: African\n    * `H`:  Asian or Asian British: Indian\n    * `J`:  Asian or Asian British: Pakistani\n    * `K`:  Asian or Asian British: Bangladeshi\n    * `P`:  Black or Black British: Any other Black background\n    * `S`:  Other Ethnic Groups: Any other ethnic group\n    * `R`:  Other Ethnic Groups: Chinese\n    * `Z`:  Not stated",
            "symbols" : [ "D", "E", "F", "G", "A", "B", "C", "L", "M", "N", "H", "J", "K", "P", "S", "R", "Z" ]
          },
          "doc" : "Ethnicity"
        }, {
          "name" : "penetrance",
          "type" : [ "null", {
            "type" : "enum",
            "name" : "Penetrance",
            "namespace" : "org.gel.models.participant.avro",
            "doc" : "Penetrance assumed in the analysis",
            "symbols" : [ "complete", "incomplete" ]
          } ],
          "doc" : "The penetrance of the phenotype for this genotype. Value in the range [0, 1]"
        }, {
          "name" : "variableExpressivity",
          "type" : [ "null", "boolean" ],
          "doc" : "Variable expressivity of a given phenotype for the same genotype"
        }, {
          "name" : "description",
          "type" : [ "null", "string" ],
          "doc" : "Evidence description"
        }, {
          "name" : "additionalProperties",
          "type" : {
            "type" : "array",
            "items" : {
              "type" : "record",
              "name" : "Property",
              "doc" : "A property in the form of name-value pair.\n    Names are restricted to ontology ids, they should be checked against existing ontologies in resources like\n    Ontology Lookup Service.",
              "fields" : [ {
                "name" : "id",
                "type" : [ "null", "string" ],
                "doc" : "The ontology term id or accession in OBO format ${ONTOLOGY_ID}:${TERM_ID} (http://www.obofoundry.org/id-policy.html)"
              }, {
                "name" : "name",
                "type" : [ "null", "string" ],
                "doc" : "The ontology term name"
              }, {
                "name" : "value",
                "type" : [ "null", "string" ],
                "doc" : "Optional value for the ontology term, the type of the value is not checked\n        (i.e.: we could set the pvalue term to \"significant\" or to \"0.0001\")"
              } ]
            }
          },
          "doc" : "A list of additional properties in the form name-value.",
          "default" : [ ]
        }, {
          "name" : "bibliography",
          "type" : {
            "type" : "array",
            "items" : "string"
          },
          "doc" : "Bibliography",
          "default" : [ ]
        } ]
      }
    } ],
    "doc" : "An entry for an array of evidences"
  }, {
    "name" : "genomicChanges",
    "type" : [ "null", {
      "type" : "array",
      "items" : "string"
    } ],
    "doc" : "gDNA change, HGVS nomenclature (e.g.: g.476A>T)"
  }, {
    "name" : "cdnaChanges",
    "type" : [ "null", {
      "type" : "array",
      "items" : "string"
    } ],
    "doc" : "cDNA change, HGVS nomenclature (e.g.: c.76A>T)"
  }, {
    "name" : "proteinChanges",
    "type" : [ "null", {
      "type" : "array",
      "items" : "string"
    } ],
    "doc" : "Protein change, HGVS nomenclature (e.g.: p.Lys76Asn)"
  }, {
    "name" : "additionalTextualVariantAnnotations",
    "type" : [ "null", {
      "type" : "map",
      "values" : "string"
    } ],
    "doc" : "Any additional information in a free text field. For example a quote from a paper"
  }, {
    "name" : "references",
    "type" : [ "null", {
      "type" : "map",
      "values" : "string"
    } ],
    "doc" : "Additional references for ths variant. For example HGMD ID or Pubmed Id"
  }, {
    "name" : "variantIdentifiers",
    "type" : [ "null", {
      "type" : "record",
      "name" : "VariantIdentifiers",
      "fields" : [ {
        "name" : "dbSnpId",
        "type" : [ "null", "string" ],
        "doc" : "Variant identifier in dbSNP"
      }, {
        "name" : "cosmicIds",
        "type" : [ "null", {
          "type" : "array",
          "items" : "string"
        } ],
        "doc" : "Variant identifier in Cosmic"
      }, {
        "name" : "clinVarIds",
        "type" : [ "null", {
          "type" : "array",
          "items" : "string"
        } ],
        "doc" : "Variant identifier in ClinVar"
      }, {
        "name" : "otherIds",
        "type" : [ "null", {
          "type" : "array",
          "items" : {
            "type" : "record",
            "name" : "Identifier",
            "fields" : [ {
              "name" : "source",
              "type" : "string",
              "doc" : "Source i.e, esenmbl"
            }, {
              "name" : "identifier",
              "type" : "string",
              "doc" : "identifier"
            } ]
          }
        } ]
      } ]
    } ]
  }, {
    "name" : "alleleFrequencies",
    "type" : [ "null", {
      "type" : "array",
      "items" : {
        "type" : "record",
        "name" : "AlleleFrequency",
        "doc" : "The population allele frequency of a given variant in a given study and optionally population",
        "fields" : [ {
          "name" : "study",
          "type" : "string",
          "doc" : "The study from where this data comes from"
        }, {
          "name" : "population",
          "type" : "string",
          "doc" : "The specific population where this allele frequency belongs"
        }, {
          "name" : "alternateFrequency",
          "type" : "float",
          "doc" : "The frequency of the alternate allele"
        } ]
      }
    } ],
    "doc" : "A list of population allele frequencies"
  }, {
    "name" : "additionalNumericVariantAnnotations",
    "type" : [ "null", {
      "type" : "map",
      "values" : "float"
    } ],
    "doc" : "Additional numeric variant annotations for this variant. For Example (Allele Frequency, sift, polyphen,\n        mutationTaster, CADD. ..)"
  }, {
    "name" : "comments",
    "type" : [ "null", {
      "type" : "array",
      "items" : "string"
    } ],
    "doc" : "Comments"
  }, {
    "name" : "alleleOrigins",
    "type" : [ "null", {
      "type" : "array",
      "items" : "AlleleOrigin"
    } ],
    "doc" : "List of allele origins for this variant in this report"
  }, {
    "name" : "ihp",
    "type" : [ "null", "int" ],
    "doc" : "Largest reference interrupted homopolymer length intersecting with the indel"
  }, {
    "name" : "recurrentlyReported",
    "type" : [ "null", "boolean" ],
    "doc" : "Flag indicating if the variant is recurrently reported"
  }, {
    "name" : "fdp50",
    "type" : [ "null", "float" ],
    "doc" : "Average tier1 number of basecalls filtered from original read depth within 50 bases"
  }, {
    "name" : "others",
    "type" : [ "null", {
      "type" : "map",
      "values" : "string"
    } ],
    "doc" : "Map of other attributes where keys are the attribute names and values are the attributes"
  } ]
}
