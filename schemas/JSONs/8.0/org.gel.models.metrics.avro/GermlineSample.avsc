{
  "type" : "record",
  "name" : "GermlineSample",
  "namespace" : "org.gel.models.participant.avro",
  "doc" : "A germline sample",
  "fields" : [ {
    "name" : "sampleId",
    "type" : "string",
    "doc" : "Sample identifier (e.g, LP00012645_5GH))"
  }, {
    "name" : "labSampleId",
    "type" : "string",
    "doc" : "Lab sample identifier"
  }, {
    "name" : "LDPCode",
    "type" : [ "null", "string" ],
    "doc" : "LDP Code (Local Delivery Partner)"
  }, {
    "name" : "source",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "SampleSource",
      "doc" : "The source of the sample\n    NOTE: IN GMS, BONE_MARROW_ASPIRATE_TUMOUR_CELLS and BONE_MARROW_ASPIRATE_TUMOUR_SORTED_CELLS are deprecated as they have been separated into their respective biotypes",
      "symbols" : [ "AMNIOTIC_FLUID", "BLOOD", "BLOOD_CAPILLARY_HEEL_PRICK", "BLOOD_CLOT", "BLOOD_CORD", "BLOOD_DRIED_BLOOD_SPOT", "BONE_MARROW", "BONE_MARROW_ASPIRATE_TUMOUR_CELLS", "BONE_MARROW_ASPIRATE_TUMOUR_SORTED_CELLS", "BUCCAL_SPONGE", "BUCCAL_SWAB", "BUFFY_COAT", "CHORIONIC_VILLUS_SAMPLE", "FIBROBLAST", "FLUID", "FRESH_TISSUE_IN_CULTURE_MEDIUM", "OTHER", "SALIVA", "TISSUE", "TUMOUR", "URINE" ]
    } ],
    "doc" : "Source of the sample"
  }, {
    "name" : "product",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "Product",
      "symbols" : [ "DNA", "RNA" ]
    } ],
    "doc" : "Product of the sample"
  }, {
    "name" : "preparationMethod",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "PreparationMethod",
      "doc" : "In 100K, preparation Method of sample\n    NOTE: In GMS, this field is deprecated in favour of StorageMedium and Method",
      "symbols" : [ "ASPIRATE", "CD128_SORTED_CELLS", "CD138_SORTED_CELLS", "EDTA", "FF", "FFPE", "LI_HEP", "ORAGENE" ]
    } ],
    "doc" : "Preparation method\n        NOTE: In GMS, this has been deprecated in favour of Method and storageMedium"
  }, {
    "name" : "programmePhase",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "ProgrammePhase",
      "symbols" : [ "CRUK", "OXFORD", "CLL", "IIP", "MAIN", "EXPT" ]
    } ],
    "doc" : "Genomics England programme phase"
  }, {
    "name" : "clinicalSampleDateTime",
    "type" : [ "null", "string" ],
    "doc" : "The time when the sample was received. In the format YYYY-MM-DDTHH:MM:SS+0000"
  }, {
    "name" : "participantId",
    "type" : [ "null", "string" ]
  }, {
    "name" : "participantUid",
    "type" : [ "null", "string" ],
    "doc" : "Participant UId of the sample"
  }, {
    "name" : "sampleUid",
    "type" : [ "null", "string" ]
  }, {
    "name" : "maskedPid",
    "type" : [ "null", "string" ]
  }, {
    "name" : "method",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "Method",
      "doc" : "In GMS, Method is defined as how the sample was taken directly from the patient",
      "symbols" : [ "ASPIRATE", "BIOPSY", "NOT_APPLICABLE", "RESECTION", "SORTED_OTHER", "UNKNOWN", "UNSORTED", "CD138_SORTED" ]
    } ],
    "doc" : "In GMS, this is how the sample was extracted from the participant"
  }, {
    "name" : "storageMedium",
    "type" : [ "null", {
      "type" : "enum",
      "name" : "StorageMedium",
      "doc" : "In GMS, storage medium of sample",
      "symbols" : [ "EDTA", "FF", "LI_HEP", "ORAGENE", "FFPE" ]
    } ],
    "doc" : "In GMS, this is what solvent/medium the sample was stored in"
  }, {
    "name" : "sampleType",
    "type" : [ "null", "string" ],
    "doc" : "In GMS, this is the sampleType as entered by the clinician in TOMs"
  }, {
    "name" : "sampleState",
    "type" : [ "null", "string" ],
    "doc" : "In GMS, this is the sampleState as entered by the clinician in TOMs"
  }, {
    "name" : "sampleCollectionOperationsQuestions",
    "type" : [ "null", {
      "type" : "record",
      "name" : "SampleCollectionOperationsQuestions",
      "doc" : "Operations questions on sample collection for Newborns BaMMs",
      "fields" : [ {
        "name" : "easeOfSampleCollection",
        "type" : [ "null", {
          "type" : "enum",
          "name" : "EaseOfSampleCollection",
          "doc" : "Ease of Sample Collection",
          "symbols" : [ "EASY", "DIFFICULT", "NOT_APPLICABLE" ]
        } ],
        "doc" : "Ease of successful sample collection"
      }, {
        "name" : "minutes",
        "type" : [ "null", "int" ],
        "doc" : "Time taken to collect the sample (from opening collection kit to completion) in minutes"
      } ]
    } ],
    "doc" : "In Newborns BaMMs, this is operational sample collection questions"
  } ]
}
