import os
import sys

from setuptools import find_packages, setup

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

enforced_version = os.environ.get("GEL_REPORT_MODELS_PYTHON_VERSION", None)
interpreter_version = str(sys.version_info[0])
target_version = enforced_version if enforced_version else interpreter_version
if target_version == "2":
    # FileNotFoundError is only available since Python 3.3
    FileNotFoundError = IOError
    from io import open
elif target_version != "3":
    raise ValueError("Not supported python version {}".format(target_version))

with open("VERSION") as handle:
    VERSION = handle.read()

# read the contents of your README file
this_directory = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(this_directory, "README.md"), encoding="utf-8") as f:
    long_description = f.read()

setup(
    name="GelReportModels",
    version=VERSION,
    packages=find_packages(exclude=["tests"]),
    include_package_data=True,
    url="https://gelreportmodels.genomicsengland.co.uk",
    license="Apache",
    author="Bioinformatics Team at Genomics England",
    author_email="antonio.rueda-martin@genomicsengland.co.uk",
    description="Genomics England Bioinformatics team model definitions",
    long_description=long_description,
    long_description_content_type="text/markdown",
    install_requires=[
        "humanize==0.5.1;python_version<'3'",
        "humanize>=0.5.1,<1;python_version>='3.6'",
        "pyyaml==4.2b1;python_version<'3'",
        "pyyaml>=6,<7;python_version>='3.6'",
        "dictdiffer==0.8.1;python_version<'3'",
        "dictdiffer>=0.8.1,<1;python_version>='3.6'",
        "future==0.18.3;python_version<'3'",
        "future>=0.18.3,<1;python_version>='3.6'",
        "avro==1.7.7;python_version<'3'",
        "avro>=1.11.3;python_version>='3.6'",
        "requests==2.23.0; python_version < '3'",
        "requests>=2.27.0,<3; python_version >= '3.6'",
        "pytz>=2022.1",
        "factory-boy==2.9;python_version<'3'",
        "factory-boy>=2.9.2,<4;python_version>='3.6'",
        "packaging==20.9;python_version<'3'",
        "packaging>=21.3;python_version>='3.6'",
        "python-dateutil>=2.4,<2.9;python_version<'3.6'",
        "python-dateutil>=2.9;python_version>'3.6'",
    ],
    extras_require={
        "build_docs": [
            "Sphinx==1.6.2;python_version<'3'",
            "Sphinx==5.2.3;python_version>='3.6'",
            "sphinx_rtd_theme==0.2.4;python_version<'3'",
            "sphinx_rtd_theme==1.0.0;python_version>='3.6'",
        ],
        "test": [
            "pytest",
            "pytest-mock",
            "mock",
            "black>=23.3.0,<24;python_version>='3.6'",
            "isort==5.12.0;python_version>='3.6'",
        ],
    },
    classifiers=[
        # Chose either "3 - Alpha", "4 - Beta" or "5 - Production/Stable" as the current state of your package
        "Development Status :: 5 - Production/Stable",
        "Intended Audience :: Healthcare Industry",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering :: Bio-Informatics",
        "License :: OSI Approved :: Apache Software License",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
    ],
)
