import os
import sys

import pytest

from tests.helpers.mock_open import MockOpen
from tests.helpers.mock_stream import MockStream

REGEN_FIXTURES = os.environ.get("REGEN_FIXTURES", False)


@pytest.fixture(autouse=True)
def mocked_open(mocker):
    _open = open

    m = mocker.patch(
        "{}.open".format("builtins" if sys.version_info.major == 3 else "__builtin__"),
    )
    m.__open__ = _open
    m.__streams__ = {}
    m.__read_data__ = {}

    def mock_open(path, mode="rt", *a, **kwargs):
        if (
            isinstance(m.__read_data__, dict)
            and path in m.__read_data__
            and "r" in mode
        ):
            return MockOpen(return_value=MockStream(m.__read_data__[path]))
        elif (
            isinstance(m.__read_data__, dict) and "*" in m.__read_data__ and "r" in mode
        ):
            return MockOpen(return_value=MockStream(m.__read_data__["*"]))
        elif (
            not isinstance(m.__read_data__, dict)
            and m.__read_data__ is not None
            and "r" in mode
        ):
            return MockOpen(return_value=MockStream(m.__read_data__))
        elif mode == "wt" or mode == "w":
            m.__streams__[path] = MockStream()
            return MockOpen(return_value=m.__streams__[path])
        else:
            return _open(path, mode, *a, **kwargs)

    m.side_effect = mock_open

    return m
