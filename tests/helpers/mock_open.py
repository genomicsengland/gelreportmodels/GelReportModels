from mock import Mock


class MockOpen(Mock):
    def __enter__(self):
        return self.return_value

    def __exit__(self, *a, **kwargs):
        pass
