class MockStream:
    """Basic read/write. StringIO differs in Py2.7 vs 3.10 so can't be used."""

    __data = ""

    def __init__(self, initial_value=""):
        self.__data = initial_value

    def write(self, __s):
        self.__data += __s

    def writelines(self, lines):
        for line in lines:
            self.__data += line

    def read(self):
        return self.__data
