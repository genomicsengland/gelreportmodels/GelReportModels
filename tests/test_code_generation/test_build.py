import io
import json
import os
import sys

import dictdiffer
import pytest

import build
import protocols
from protocols.resources import RESOURCE_DIR
from protocols.util.dependency_manager import DependencyManager
from protocols_utils.utils.conversion_tools import ConversionTools
from tests.helpers.mock_open import MockOpen

TEST_RESOURCE_DIR = os.path.join(os.path.dirname(__file__), "resources")


def get_patched_python_folder():
    return "/path/to/python"


def get_protocol_folder(build_data):
    return os.path.join(
        get_patched_python_folder(),
        DependencyManager.get_python_protocol_name(build_data),
    )


def get_python_folder_for_model(build_data, package):
    return os.path.join(get_protocol_folder(build_data), package["python_package"])


def get_schema_folder_for_model(build_data, package):
    return os.path.join(
        RESOURCE_DIR,
        "schemas",
        DependencyManager.get_python_protocol_name(build_data),
        package["python_package"],
    )


@pytest.fixture(autouse=True)
def mocked_makedir(mocker):
    return mocker.patch("build.makedir")


@pytest.fixture(autouse=True)
def mocked_tools(mocker):
    m = mocker.patch("build.ConversionTools")
    m.get_md5_of_path.side_effect = lambda x: os.path.relpath(
        x, os.path.dirname(protocols.__file__)
    )
    m.get_md5 = ConversionTools.get_md5

    return m


@pytest.fixture(autouse=True)
def patch_python_folder(mocker):
    mocker.patch("build.PYTHON_FOLDER", new=get_patched_python_folder())


@pytest.fixture(autouse=True)
def mocked_distutils(mocker):
    return mocker.patch("distutils.dir_util")


@pytest.fixture(autouse=True)
def mocked_exists(mocker):
    return mocker.patch("os.path.exists", return_value=True)


def default_build_args():
    return ["build.py", "--skip-docs"]


@pytest.fixture(autouse=True)
def mocked_listdir(mocker, mocked_open):
    _listdir = os.listdir
    m = mocker.patch("os.listdir")
    build_data = mock_build()
    m.__paths__ = {
        os.path.join(build.IDL_FOLDER, p["package"], p["version"]): ["test.avdl"]
        for p in build_data["packages"]
    }

    m.__paths__.update(
        {
            os.path.join(build.IDL_FOLDER, p["package"], "current"): ["test.avdl"]
            for p in build_data["packages"]
        }
    )

    m.__paths__.update(
        {
            build.DOCS_SOURCE_FOLDER: [
                p.replace(".fixture", "")
                for p in _listdir(TEST_RESOURCE_DIR)
                if p.endswith("rst.fixture")
            ]
        }
    )

    m.__paths__.update(
        {
            os.path.join(
                RESOURCE_DIR,
                "schemas",
                DependencyManager.get_python_protocol_name(build_data),
                p["python_package"],
            ): ["examplemodel_record.avsc"]
            for p in build_data["packages"]
        }
    )

    m.__paths__.update({"/path/to/python": []})

    mocked_open.__read_data__.update(
        {
            os.path.join(
                RESOURCE_DIR,
                "schemas",
                DependencyManager.get_python_protocol_name(build_data),
                p["python_package"],
                "examplemodel_record.avsc",
            ): """
            {
                "name": "ExampleModel",
                "type": "record"
            }
            """
            for p in build_data["packages"]
        }
    )

    m.side_effect = (
        lambda x: m.__paths__[x]
        if isinstance(m.__paths__, dict) and x in m.__paths__
        else _listdir(x)
    )

    open_data = {
        os.path.join(
            build.IDL_FOLDER, p["package"], p["version"], "test.avdl"
        ): "@namespace('{}')".format(p["package"])
        for p in build_data["packages"]
    }
    open_data.update(
        {
            os.path.join(
                build.IDL_FOLDER, p["package"], "current", "test.avdl"
            ): "@namespace('{}')".format(p["package"])
            for p in build_data["packages"]
        }
    )

    for p in _listdir(TEST_RESOURCE_DIR):
        if p.endswith("rst.fixture"):
            with open(os.path.join(TEST_RESOURCE_DIR, p)) as f:
                open_data[
                    os.path.join(build.DOCS_SOURCE_FOLDER, p.replace(".fixture", ""))
                ] = f.read()
    if not isinstance(mocked_open.__read_data__, dict):
        mocked_open.__read_data__ = {
            "*": mocked_open.__read_data__,
        }
    mocked_open.__read_data__.update(open_data)

    return m


def mock_build(version="1.0", extra=False):
    # hashes have been precalculated base on mocked_tools
    data = {
        "version": version,
        "packages": [
            {
                "package": "org.gel.test",
                "python_package": "test",
                "version": "1.1.1",
                "dependencies": ["org.gel.some_dependency"],
                "catalog_variable_sets": ["ExampleModel"],
                "json_hash": "8f7de3183dd35a3e80417771038eedfc",
            },
            {
                "package": "org.gel.some_dependency",
                "python_package": "dependency",
                "version": "2.2.2",
                "dependencies": [],
                "json_hash": "7ce1d87c81aced26a3cb6a8ad10782ba",
            },
        ],
    }
    if extra:
        for p in data["packages"]:
            p["build_version"] = data["version"]
            p["namespaces"] = []
    return data


def mock_builds():
    return {"builds": [mock_build(), mock_build(version="2.0")]}


@pytest.fixture
def mocked_check_consistency_with_last_release(mocker):
    return mocker.patch("build.check_consistency_with_last_release")


@pytest.fixture()
def mocked_update_version(mocker):
    return mocker.patch("build.update_version")


@pytest.mark.skipif(
    sys.version_info.major < 3,
    reason="Skipped as build script does not support python2",
)
@pytest.mark.usefixtures(
    "mocked_check_consistency_with_last_release", "mocked_update_version"
)
class TestMain:
    @pytest.fixture(autouse=True)
    def mocked_open(self, mocked_open):
        mocked_open.__read_data__.update({build.BUILDS_FILE: json.dumps(mock_builds())})
        return mocked_open

    @pytest.fixture(autouse=True)
    def mocked_build(self, mocker):
        return mocker.patch("build.run_build", return_value=False)

    @pytest.mark.parametrize(
        "args,expected_builds",
        [
            pytest.param([], ["2.0", "1.0"], id="not-specified-ordered-descending"),
            pytest.param(
                ["--latest"],
                ["2.0"],
                id="latest-specified",
            ),
            pytest.param(
                ["--version", "1.0"],
                ["1.0"],
                id="version-specified",
            ),
            pytest.param(
                ["--version", "1.0.1"],
                ["1.0"],
                id="patch-version-specified",
            ),
        ],
    )
    def test_that_main_builds_specified_models(
        self,
        mocker,
        mocked_build,
        args,
        expected_builds,
    ):
        # given mocks
        mocker.patch("sys.argv", new=default_build_args() + args)

        # when main runs
        build.main()

        # assert versions are built
        versions_called = [c.args[0]["version"] for c in mocked_build.call_args_list]
        assert versions_called == expected_builds

    @pytest.mark.parametrize(
        "args",
        [
            pytest.param([], id="not-specified-ordered-descending"),
            pytest.param(
                ["--latest"],
                id="latest-specified",
            ),
            pytest.param(
                ["--version", "1.0"],
                id="version-specified",
            ),
            pytest.param(
                ["--version", "1.0.1"],
                id="patch-version-specified",
            ),
        ],
    )
    def test_that_main_saves_build_file_if_build_return_True(
        self, mocker, args, mocked_build, mocked_open
    ):
        # given mocks
        mocker.patch("sys.argv", new=default_build_args() + args)

        # and run build will return True
        mocked_build.return_value = True

        # when main is called
        build.main()

        # then the builds file is updated
        assert build.BUILDS_FILE in mocked_open.__streams__

        assert json.load(mocked_open.__streams__[build.BUILDS_FILE]) == {
            "builds": sorted(
                mock_builds()["builds"], key=lambda x: x["version"], reverse=True
            )
        }

    @pytest.mark.parametrize(
        "args",
        [
            pytest.param([], id="all-builds"),
            pytest.param(
                ["--latest"],
                id="latest-specified",
            ),
        ],
    )
    def test_that_main_builds_linkers_if_latest_model_built(
        self, mocker, args, mocked_tools
    ):
        # given mocks
        mocker.patch("sys.argv", new=default_build_args() + args)

        # when main runs that calls latest build
        build.main()

        # assert
        mocked_tools.update_python_linkers.assert_called_once_with(
            get_patched_python_folder(),
            sorted(
                mock_builds()["builds"],
                key=lambda x: x["version"],
                reverse=True,
            )[0],
            "protocol_2_0",
        )

    def test_that_main_raises_ValueError_if_specified_version_doesnot_exit(
        self, mocker
    ):
        mocker.patch("sys.argv", new=default_build_args() + ["--version", "3.0"])
        with pytest.raises(ValueError):
            build.main()


@pytest.mark.skipif(
    sys.version_info.major < 3,
    reason="Skipped as build script does not support python2",
)
class TestIDLConversion:
    def test_that_IDLs_are_not_copied_if_there_are_no_new_changes(
        self, mocked_distutils
    ):
        # given source and copied IDL build hashes match the md5sum of the respective folders
        # when run_build is executed with a mock_build
        build_data = mock_build()
        save = build.run_build(build_data, False, False, False)

        # then the idl files are not recopied.
        differences = list(dictdiffer.diff(mock_build(), build_data))
        assert differences == [], json.dumps(build_data)

        mocked_distutils.copy_tree.assert_not_called()
        assert save is False
        #

    def test_that_IDLs_are_copied_and_converted_if_there_is_a_change_in_the_hashes(
        self,
        mocker,
        mocked_distutils,
        mocked_tools,
    ):
        # given packages have an old hash
        build_data = mock_build()
        for package in build_data["packages"]:
            package["json_hash"] = "some-old-hash"

        # when run_build is executed
        save = build.run_build(build_data, False, False, False)

        # the idl folder / json folders are cleared
        assert mocked_distutils.remove_tree.call_args_list == [
            mocker.call(a)
            for p in build_data["packages"]
            for a in [
                os.path.join(
                    build.IDL_FOLDER, "build", build_data["version"], p["package"]
                ),
                get_schema_folder_for_model(build_data, p),
            ]
        ]

        # then the files are re copied
        assert mocked_distutils.copy_tree.call_args_list == [
            # first package
            mocker.call(
                "schemas/IDLs/org.gel.test/1.1.1", "schemas/IDLs/build/1.0/org.gel.test"
            ),
            # dependency to build folder of first package
            mocker.call(
                "schemas/IDLs/org.gel.some_dependency/2.2.2",
                "schemas/IDLs/build/1.0/org.gel.test",
            ),
            # second package
            mocker.call(
                "schemas/IDLs/org.gel.some_dependency/2.2.2",
                "schemas/IDLs/build/1.0/org.gel.some_dependency",
            ),
        ]
        # json is converted
        assert mocked_tools.idl2json.call_args_list == [
            mocker.call(
                os.path.join(
                    build.IDL_FOLDER, "build", build_data["version"], p["package"]
                ),
                get_schema_folder_for_model(build_data, p),
                use_pool=False,
            )
            for p in build_data["packages"]
        ]

        # and save is True
        assert save is True

    @pytest.mark.usefixtures("mocked_distutils")
    def test_that_process_pool_is_used_if_package_version_is_higher_than_seven(
        self, mocker, mocked_tools
    ):
        # given packages have the hash for version 1.0 not 7.0
        build_data = mock_build()
        build_data["version"] = "7.0"

        # when run_build is executed
        save = build.run_build(build_data, False, False, False)

        # json is converted with pools
        assert mocked_tools.idl2json.call_args_list == [
            mocker.call(
                os.path.join(
                    build.IDL_FOLDER, "build", build_data["version"], p["package"]
                ),
                get_schema_folder_for_model(build_data, p),
                use_pool=True,
            )
            for p in build_data["packages"]
        ]

        # and save is True
        assert save is True


@pytest.mark.skipif(
    sys.version_info.major < 3,
    reason="Skipped as build script does not support python2",
)
@pytest.mark.usefixtures(
    "mocked_check_consistency_with_last_release", "mocked_update_version"
)
class TestPythonBuild:
    @pytest.fixture(autouse=True)
    def mocked_AvroSchemaFile(self, mocker):
        m = mocker.patch("build.AvroSchemaFile")
        m.return_value.convert_variable_set.return_value = [{"name": "ExampleModel"}]
        return m

    def test_that_python_build_happens_if_enabled(
        self,
        mocker,
        mocked_tools,
        mocked_open,
        mocked_distutils,
        mocked_makedir,
        mocked_listdir,
    ):
        mocker.patch(
            "build.OPENCGA_CATALOG_FOLDER", new="/path/to/python/catalog_variable_set"
        )

        # when not skipping python build

        build_data = mock_build()

        build.run_build(
            build_data, build_docs=False, build_java=False, build_python=True
        )

        # then python folders are removed
        assert mocked_distutils.remove_tree.call_args_list == [
            mocker.call(get_protocol_folder(build_data)),
            mocker.call("/path/to/python/catalog_variable_set/v1.0"),
        ]

        # catalog folder is recreated
        assert mocked_makedir.call_args_list == [
            mocker.call("/path/to/python/catalog_variable_set/v1.0/org.gel.test_1.1.1")
        ]

        expected_catalog = "/path/to/python/catalog_variable_set/v1.0/org.gel.test_1.1.1/ExampleModel.catalog_vs.json"

        assert expected_catalog in mocked_open.__streams__

        build_data = mock_build(extra=True)

        assert mocked_tools.json2python.call_args_list == [
            mocker.call(
                get_schema_folder_for_model(build_data, p),
                get_protocol_folder(build_data),
                p,
                {},
            )
            for p in build_data["packages"]
        ]


@pytest.mark.skipif(
    sys.version_info.major < 3,
    reason="Skipped as build script does not support python2",
)
@pytest.mark.usefixtures(
    "mocked_check_consistency_with_last_release", "mocked_update_version"
)
class TestJavaBuild:
    @pytest.fixture(autouse=True)
    def mocked_AvroSchemaFile(self, mocker):
        m = mocker.patch("build.AvroSchemaFile")
        m.return_value.convert_variable_set.return_value = [{"name": "ExampleModel"}]
        return m

    def test_that_java_build_happens_if_enabled(
        self,
        mocker,
        mocked_tools,
        mocked_open,
        mocked_distutils,
        mocked_makedir,
    ):
        # when not skipping python build
        build_data = mock_build()
        build.run_build(
            build_data, build_docs=False, build_java=True, build_python=False
        )

        # then java folders are removed
        assert mocked_distutils.remove_tree.call_args_list == [
            mocker.call("target/generated-sources/java")
        ]

        assert mocked_tools.json2java.call_args_list == [
            mocker.call(
                get_schema_folder_for_model(build_data, p),
                "target/generated-sources/java",
            )
            for p in build_data["packages"]
        ]


@pytest.mark.skipif(
    sys.version_info.major < 3,
    reason="Skipped as build script does not support python2",
)
class TestDocsBuild:
    @pytest.fixture(autouse=True)
    def mocked_AvroSchemaFile(self, mocker):
        m = mocker.patch("build.AvroSchemaFile")
        m.return_value.convert_variable_set.return_value = [{"name": "ExampleModel"}]
        return m

    def test_that_doc_build_happens_if_enabled(
        self,
        mocker,
        mocked_tools,
        mocked_open,
        mocked_distutils,
        mocked_makedir,
    ):
        # when not skipping python build
        build_data = mock_build()
        build.run_build(
            build_data, build_docs=True, build_java=False, build_python=False
        )

        # then java folders are removed
        assert mocked_distutils.remove_tree.call_args_list == [
            mocker.call("docs/source/schemas/1.0")
        ]

        build_data = mock_build(extra=True)

        assert mocked_tools.json2rst.call_args_list == [
            mocker.call(
                get_schema_folder_for_model(build_data, p),
                "docs/source/schemas/{}".format(build_data["version"]),
                p,
                {},
            )
            for p in build_data["packages"]
        ]


class TestUpdateVersion:
    @pytest.mark.parametrize(
        "current,expected_patch",
        [
            pytest.param((1, 0, 0), "0", id="major-bump"),
            pytest.param((2, 0, 0), "1", id="minor-bump"),
        ],
    )
    def test_that_call_to_update_version_updates_based_on_latest_build(
        self,
        mocked_open,
        mocked_check_consistency_with_last_release,
        current,
        expected_patch,
    ):
        mocked_check_consistency_with_last_release.return_value = current

        mocked_open.__read_data__ = {
            build.BUILDS_FILE: json.dumps(mock_builds()),
            build.POM_FILE: "<version>X.X.X</version>"
            "<properties>"
            "<something.namespace>org.gel.test</something.namespace>"
            "<something.version>Z.Z.Z</something.version>"
            "<package.version>A.A.A</package.version>"
            "</properties>"
            "<dependencies>"
            "<version>Y.Y.Y</version>"
            "</dependencies>"
            "<build>"
            "<version>Y.Y.Y</version>"
            "</build>",
            build.DOC_VERSION_FILE: "random text\n"
            "version = 'X.X.X'\n"
            "release = 'Y.Y.Y'\n"
            "rest of file",
        }

        build.update_version(mock_build(version="2.0"), current)

        # then
        assert build.POM_FILE in mocked_open.__streams__, mocked_open.__streams__
        assert build.VERSION_FILE in mocked_open.__streams__, mocked_open.__streams__
        assert (
            build.DOC_VERSION_FILE in mocked_open.__streams__
        ), mocked_open.__streams__

        assert mocked_open.__streams__[build.POM_FILE].read().strip() == (
            "<version>2.0.{0}</version>"
            "<properties>"
            "<something.namespace>org.gel.test</something.namespace>"
            "<something.version>1.1.1</something.version>"
            "<package.version>2.0.{0}</package.version>"
            "</properties>"
            "<dependencies>"
            "<version>Y.Y.Y</version>"
            "</dependencies>"
            "<build>"
            "<version>Y.Y.Y</version>"
            "</build>".format(expected_patch)
        )
        assert mocked_open.__streams__[build.VERSION_FILE].read().strip() == (
            "2.0.{}".format(expected_patch)
        )
        assert mocked_open.__streams__[build.DOC_VERSION_FILE].read().strip() == (
            "random text\n"
            'version = "Latest"\n'
            'release = "Latest"\n'
            "rest of file".format(expected_patch)
        )


@pytest.mark.skipif(
    sys.version_info.major < 3,
    reason="Skipped as build script does not support python2",
)
class TestConsistency:
    @pytest.fixture
    def mock_check_output(self, mocker):
        data = {}
        m = mocker.patch(
            "subprocess.check_output",
            side_effect=lambda *x, **k: data.get(x[0], "").encode(),
        )
        m.__data__ = data
        return m

    def test_that_check_consistency_with_last_release_calls_git_commands(
        self, mocker, mock_check_output
    ):
        # given that the latest tag is latest-tag
        mock_check_output.__data__["git tag -l --sort=-version:refname"] = "latest-tag"

        # when consistency check is called
        build.check_consistency_with_last_release([mock_build()])

        # then git is called with the following commands
        assert mock_check_output.call_args_list == [
            mocker.call("git tag -l --sort=-version:refname", shell=True),
            mocker.call(
                "git show latest-tag:protocols/resources/builds.json", shell=True
            ),
            mocker.call("git diff --name-only latest-tag HEAD", shell=True),
            mocker.call("git status --short", shell=True),
        ]

    def test_that_changes_to_old_builds_raise_AssertionError(self, mock_check_output):
        # given that the latest tag is latest-tag
        diffbuild = mock_build()
        diffbuild["packages"][0]["version"] = "old-version"

        mock_check_output.__data__.update(
            {
                "git tag -l --sort=-version:refname": "latest-tag",
                "git show latest-tag:protocols/resources/builds.json": json.dumps(
                    {"builds": [diffbuild]}
                ),
            }
        )

        # then
        with pytest.raises(AssertionError):
            # when consistency check is called
            build.check_consistency_with_last_release([mock_build()])

    def test_that_additions_to_old_builds_raise_AssertionError(
        self, mocker, mock_check_output
    ):
        # given that the latest tag is latest-tag

        diffbuild = mock_build()
        org = mock_build()
        diffbuild["packages"].append(org["packages"][0])
        diffbuild["packages"][0]["package"] = "new-namespace"

        mock_check_output.__data__.update(
            {
                "git tag -l --sort=-version:refname": "latest-tag",
                "git show latest-tag:protocols/resources/builds.json": json.dumps(
                    {"builds": [diffbuild]}
                ),
            }
        )

        # then
        with pytest.raises(AssertionError):
            # when consistency check is called
            build.check_consistency_with_last_release([org])

    def test_that_changes_to_released_models_raise_AssertionError(
        self, mock_check_output
    ):
        # given that the latest tag is latest-tag
        mock_check_output.__data__.update(
            {
                "git tag -l --sort=-version:refname": "latest-tag",
                "git show latest-tag:protocols/resources/builds.json": json.dumps(
                    {"builds": [mock_build()]}
                ),
                "git diff --name-only latest-tag HEAD": os.path.join(
                    build.IDL_FOLDER, "org.gel.test", "1.1.1", "test.avdl"
                ),
            }
        )

        # then
        with pytest.raises(AssertionError):
            # when consistency check is called
            build.check_consistency_with_last_release([mock_build()])


class TestCurrentModels:
    @pytest.fixture(autouse=True)
    def mock_check_output(self, mocker):
        data = {}
        m = mocker.patch(
            "subprocess.check_output",
            side_effect=lambda *x, **k: data.get(x[0], "").encode(),
        )
        m.__data__ = data

        m.__data__.update(
            {
                "git tag -l --sort=-version:refname": "latest-tag",
                "git show latest-tag:protocols/resources/builds.json": json.dumps(
                    {"builds": [mock_build()]}
                ),
                "git diff --name-only latest-tag HEAD": os.path.join(
                    build.IDL_FOLDER, "org.gel.test", "current", "test.avdl"
                ),
            }
        )

        return m

    def test_that_current_model_is_checked_against_version_used_in_last_release(
        self, mocker, mocked_open
    ):
        # given that the latest tag is latest-tag

        mocked_open.__read_data__.update(
            {
                build.CURRENT_MODEL_VERSION_FILE: """{"org.gel.test": "1.1.1"}""",
            }
        )

        build.check_consistency_with_last_release([mock_build()])

        assert mocked_open.call_args_list == [
            mocker.call("schemas/IDLs/current_versions.json"),
            mocker.call("schemas/IDLs/org.gel.test/current/test.avdl"),
            mocker.call("schemas/IDLs/org.gel.test/1.1.1/test.avdl"),
        ]

    def test_that_changes_to_current_models_raise_AssertionError_if_version_used_in_last_release(
        self, mocked_open
    ):
        # given that the latest tag is latest-tag

        mocked_open.__read_data__.update(
            {
                build.CURRENT_MODEL_VERSION_FILE: """{"org.gel.test": "1.1.1"}""",
            }
        )

        for p in mock_build()["packages"]:
            mocked_open.__read_data__[
                os.path.join(build.IDL_FOLDER, p["package"], "current", "test.avdl")
            ] = "something different"

        # then
        with pytest.raises(AssertionError):
            # when consistency check is called
            build.check_consistency_with_last_release([mock_build()])

    def test_that_AssertionError_is_raised_if_the_latest_version_of_model_is_not_used(
        self, mocked_open
    ):
        # given that the latest tag is latest-tag

        mocked_open.__read_data__.update(
            {
                build.CURRENT_MODEL_VERSION_FILE: """{"org.gel.test": "99999.99999.99999"}""",
            }
        )

        for p in mock_build()["packages"]:
            mocked_open.__read_data__[
                os.path.join(build.IDL_FOLDER, p["package"], "current", "test.avdl")
            ] = "something different"
        with pytest.raises(AssertionError):
            build.check_consistency_with_last_release([mock_build()])

    def test_that_current_model_is_copied_if_it_is_used_by_the_current_build(
        self, mocker, mocked_open, mocked_distutils
    ):
        # given that the latest tag is latest-tag

        mocked_open.__read_data__.update(
            {
                build.CURRENT_MODEL_VERSION_FILE: """{"org.gel.test": "1.1.2"}""",
            }
        )

        build_data = mock_build("2.0")

        build_data["packages"][0]["version"] = "1.1.2"

        build.check_consistency_with_last_release([build_data, mock_build()])

        assert mocked_distutils.remove_tree.call_args_list == [
            mocker.call("schemas/IDLs/org.gel.test/1.1.2")
        ]

        assert mocked_distutils.copy_tree.call_args_list == [
            mocker.call(
                "schemas/IDLs/org.gel.test/current", "schemas/IDLs/org.gel.test/1.1.2"
            )
        ]


class TestLocalSkip:
    def test_that_if_GITLAB_CI_then_files_are_not_skipped(self, mocker, mocked_tools):
        # get folder
        mocker.patch("os.environ", new={"GITLAB_CI": "true"})

        build.skip_tree_if_not_in_ci(os.path.dirname(__file__))

        mocked_tools.run_command.assert_not_called()

    def test_that_if_local_files_are_skipped(self, mocker, mocked_tools):
        mocker.patch("os.environ", new={})

        folder = os.path.dirname(__file__)
        build.skip_tree_if_not_in_ci(folder)

        mocked_tools.run_command.assert_called_with(
            "git ls-files --modified -- {} | xargs git update-index --assume-unchanged".format(
                folder
            )
        )
