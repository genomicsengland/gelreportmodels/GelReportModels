import glob
import json
import os
import sys

import pytest

from protocols_utils.code_generation.process_schemas import PyGenerator, RSTGenerator
from tests.conftest import REGEN_FIXTURES

MODULE_PATH = "protocols_utils.code_generation.process_schemas"

RESOURCE_DIR = os.path.join(os.path.dirname(__file__), "resources")


def mock_package(namespace, name, version):
    return {
        "package": namespace,
        "python_package": name,
        "version": version,
        "dependencies": [],
        "namespaces": [namespace],
    }


def mock_namespaces(packages):
    for p in packages:
        p["build_version"] = "0.0"

    return {name: p for p in packages for name in p["namespaces"]}


with open(os.path.join(RESOURCE_DIR, "examples.json")) as f:
    SCHEMA_DATA = json.load(f)


@pytest.mark.skipif(
    sys.version_info.major < 3,
    reason="Skipped as build script does not support python2",
)
def test_that_PyGenerator_converts_json_to_expected_python_files(mocker, mocked_open):
    test_package = mock_package("org.gel.examples", "ModelExample", "1.0.0")
    dependencies = [
        mock_package("org.gel.dependency", "Dependency", "v1.0.0"),
    ]

    _glob = glob.glob

    mocked_open.__read_data__ = {k: json.dumps(v) for k, v in SCHEMA_DATA.items()}

    def mock_glob(path):
        root = os.path.dirname(path)
        return [p for p in SCHEMA_DATA if p.startswith(root)]

    mocker.patch(
        "glob.glob",
        side_effect=mock_glob,
    )

    OUTPUT = "/path/to/protocols_0_0/namespace"

    PyGenerator(
        test_package,
        mock_namespaces([test_package] + dependencies),
        "/path/to/schema",
        OUTPUT,
    ).write()

    stream_str = {
        k.replace(OUTPUT + "/", ""): v.read()
        for k, v in mocked_open.__streams__.items()
    }

    expected_files = [
        os.path.join(root, file)
        for root, folders, files in os.walk(os.path.join(RESOURCE_DIR, "expected"))
        for file in files
        if file.endswith(".py.fixture")
    ]
    found_files = []

    for filename, contents in stream_str.items():
        expected_file = os.path.join(RESOURCE_DIR, "expected", filename) + ".fixture"
        if expected_file in expected_files:
            with mocked_open.__open__(expected_file) as f:
                expected_contents = f.read()
                match = expected_contents == contents
        else:
            match = False
        if REGEN_FIXTURES and not match:
            if not os.path.exists(os.path.dirname(expected_file)):
                os.makedirs(os.path.dirname(expected_file))
            with mocked_open.__open__(expected_file, "wt") as f:
                f.write(contents)
            match = True
        if not match:
            with mocked_open.__open__(expected_file + ".expected", "wt") as f:
                f.write(contents)

        assert match, "Outputted contents of {} does not match expected.".format(
            filename
        )

        found_files.append(str(expected_file))

    # assert all files have been accounted for
    if not REGEN_FIXTURES:
        assert sorted(expected_files) == sorted(found_files)


@pytest.mark.skipif(
    sys.version_info.major < 3,
    reason="Skipped as build script does not support python2",
)
def test_that_PyGenerator_raises_KeyError_if_class_depends_on_missing_namespace(
    mocker, mocked_open
):
    test_package = mock_package("org.gel.examples", "ModelExample", "1.0.0")
    dependencies = []

    _glob = glob.glob

    mocked_open.__read_data__ = {k: json.dumps(v) for k, v in SCHEMA_DATA.items()}

    def mock_glob(path):
        root = os.path.dirname(path)
        return [p for p in SCHEMA_DATA if p.startswith(root)]

    mocker.patch(
        "glob.glob",
        side_effect=mock_glob,
    )

    OUTPUT = "/path/to/protocols_0_0/namespace"
    with pytest.raises(KeyError):
        PyGenerator(
            test_package,
            mock_namespaces([test_package] + dependencies),
            "/path/to/schema",
            OUTPUT,
        ).write()


@pytest.mark.skipif(
    sys.version_info.major < 3,
    reason="Skipped as build script does not support python2",
)
def test_that_RSTGenerator_converts_json_to_expected_rst_files(mocker, mocked_open):
    test_package = mock_package("org.gel.examples", "ModelExample", "1.0.0")
    dependencies = [
        mock_package("org.gel.dependency", "Dependency", "v1.0.0"),
    ]

    _glob = glob.glob

    mocked_open.__read_data__ = {k: json.dumps(v) for k, v in SCHEMA_DATA.items()}

    def mock_glob(path):
        root = os.path.dirname(path)
        return [p for p in SCHEMA_DATA if p.startswith(root)]

    mocker.patch(
        "glob.glob",
        side_effect=mock_glob,
    )

    OUTPUT = "/path/to/protocols_0_0/namespace"

    RSTGenerator(
        test_package,
        mock_namespaces([test_package] + dependencies),
        "/path/to/schema",
        OUTPUT,
    ).write()

    stream_str = {
        k.replace(OUTPUT + "/", ""): v.read()
        for k, v in mocked_open.__streams__.items()
    }

    expected_files = [
        os.path.join(root, file)
        for root, folders, files in os.walk(os.path.join(RESOURCE_DIR, "expected"))
        for file in files
        if file.endswith("rst.fixture")
    ]

    found_files = []

    for filename, contents in stream_str.items():
        expected_file = os.path.join(RESOURCE_DIR, "expected", filename) + ".fixture"
        if expected_file in expected_files:
            with mocked_open.__open__(expected_file) as f:
                expected_contents = f.read()
                match = expected_contents == contents
        else:
            match = False
        if REGEN_FIXTURES and not match:
            if not os.path.exists(os.path.dirname(expected_file)):
                os.makedirs(os.path.dirname(expected_file))
            with mocked_open.__open__(expected_file, "wt") as f:
                f.write(contents)
            match = True
        if not match:
            with mocked_open.__open__(expected_file + ".expected", "wt") as f:
                f.write(contents)

        assert match, "Outputted contents of {} does not match expected.".format(
            filename
        )
        found_files.append(str(expected_file))

    # assert all files have been accounted for
    if not REGEN_FIXTURES:
        assert sorted(expected_files) == sorted(found_files)
