from unittest import TestCase
from itertools import product

from protocols.protocol_8_6 import metrics as metrics_1_2_4
from protocols.protocol_8_7 import metrics as metrics_1_2_5

from protocols.migration.migration_metrics_124_to_metrics_125 import (
    MigrateMetrics124To125,
)
from protocols.migration.migration_metrics_125_to_metrics_124 import (
    MigrateMetrics125To124,
)

from tests.test_migration.base_test_migration import BaseRoundTripper


class TestRoundTripMigrateMetrics124to125(TestCase):
    def __init__(self, *args, **kwargs):
        super(TestRoundTripMigrateMetrics124to125, self).__init__(*args, **kwargs)
        self.original_type_core = metrics_1_2_4.CoverageSummary

        # Define test cases for round-trip
        self.originals_config = {
            "avg": [0.5],
            "bases": [0.5],
            "med": [0.5],
            "scope": ["autosomes"],
            "gte15x": [None, 0.5],
            "gte30x": [None, 0.5],
            "gte50x": [None, 0.5],
            "localRMSD": [None, 0.5],
            "lt15x": [None, 0.5],
            "pct25": [None, 0.5],
            "pct75": [None, 0.5],
            "sd": [None, 0.5],
            "validate": [None],
        }

    def test_that_124_125_coverage_summary_round_trip_works(self):
        original_type = metrics_1_2_4.CoverageSummary
        new_type = metrics_1_2_5.CoverageSummary

        for config in self.product_dict(**self.originals_config):
            original = original_type(**config)

            migrated = MigrateMetrics124To125().migrate_coverage_summary(original)
            round_tripped = MigrateMetrics125To124().migrate_coverage_summary(migrated)

            self.assertTrue(isinstance(migrated, new_type))
            self.assertTrue(isinstance(round_tripped, original_type))
            self.assertFalse(
                BaseRoundTripper().diff_round_tripped(
                    original=original,
                    round_tripped=round_tripped,
                    ignore_fields=["normstep005", "chunksize"],
                )
            )

    def test_that_124_125_variants_coverage_round_trip_works(self):
        original_type = metrics_1_2_4.VariantsCoverage
        new_type = metrics_1_2_5.VariantsCoverage

        for config in self.product_dict(**self.originals_config):
            original_core = self.original_type_core(**config)
            original = original_type(bedName="test", coverageSummary=[original_core])

            migrated = MigrateMetrics124To125().migrate_variants_coverage(original)
            round_tripped = MigrateMetrics125To124().migrate_variants_coverage(migrated)

            self.assertTrue(isinstance(migrated, new_type))
            self.assertTrue(isinstance(round_tripped, original_type))
            self.assertFalse(
                BaseRoundTripper().diff_round_tripped(
                    original=original,
                    round_tripped=round_tripped,
                    ignore_fields=["normstep005", "chunksize"],
                )
            )

    def test_that_124_125_whole_genome_coverage_round_trip_works(self):
        original_type = metrics_1_2_4.WholeGenomeCoverage
        new_type = metrics_1_2_5.WholeGenomeCoverage

        for config in self.product_dict(**self.originals_config):
            original_core = self.original_type_core(**config)
            original = original_type(bedName="test", coverageSummary=[original_core])

            migrated = MigrateMetrics124To125().migrate_whole_genome_coverage(original)
            round_tripped = MigrateMetrics125To124().migrate_whole_genome_coverage(
                migrated
            )

            self.assertTrue(isinstance(migrated, new_type))
            self.assertTrue(isinstance(round_tripped, original_type))
            self.assertFalse(
                BaseRoundTripper().diff_round_tripped(
                    original=original,
                    round_tripped=round_tripped,
                    ignore_fields=["normstep005", "chunksize"],
                )
            )

    def test_that_124_125_exome_coverage_round_trip_works(self):
        original_type = metrics_1_2_4.ExomeCoverage
        new_type = metrics_1_2_5.ExomeCoverage

        for config in self.product_dict(**self.originals_config):
            original_core = self.original_type_core(**config)
            original = original_type(bedName="test", coverageSummary=[original_core])

            migrated = MigrateMetrics124To125().migrate_exome_coverage(original)
            round_tripped = MigrateMetrics125To124().migrate_exome_coverage(migrated)

            self.assertTrue(isinstance(migrated, new_type))
            self.assertTrue(isinstance(round_tripped, original_type))
            self.assertFalse(
                BaseRoundTripper().diff_round_tripped(
                    original=original,
                    round_tripped=round_tripped,
                    ignore_fields=["normstep005", "chunksize"],
                )
            )

    def product_dict(self, **kwargs):
        keys = kwargs.keys()
        for instance in product(*kwargs.values()):
            yield dict(zip(keys, instance))
