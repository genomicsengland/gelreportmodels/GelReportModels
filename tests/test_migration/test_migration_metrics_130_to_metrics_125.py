from unittest import TestCase
from itertools import product

from protocols.protocol_8_8 import metrics as metrics_1_3_0
from protocols.protocol_8_7 import metrics as metrics_1_2_5

from protocols.migration.migration_metrics_125_to_metrics_130 import (
    MigrateMetrics125To130,
)
from protocols.migration.migration_metrics_130_to_metrics_125 import (
    MigrateMetrics130To125,
)

from tests.test_migration.base_test_migration import BaseRoundTripper


class TestRoundTripMigrateMetrics130to125(TestCase):
    def __init__(self, *args, **kwargs):
        super(TestRoundTripMigrateMetrics130to125, self).__init__(*args, **kwargs)
        self.original_type_core = metrics_1_3_0.CancerSummaryMetrics

        # Define test cases for round-trip
        self.originals_config = {
            "samtools_reads_mapped": [100.1, 100.1],
            "samtools_reads_mapped_normal": [99.9, 99.9],
            "samtools_pairs_on_different_chromosomes": [10.1, 10.1],
            "samtools_pairs_on_different_chromosomes_normal": [1.1, 1.1],
            "samtools_insert_size_average": [300.1, 300.1],
            "samtools_insert_size_average_normal": [200.4, 200.4],
            "variantstats_total_snvs": [199, 199],
            "variantstats_total_indels": [200, 200],
            "variantstats_total_svs": [500, None],
            "tumor_contamination_cont_est": ["yes", "yes"],
            "tumor_contamination_con_pair": ["no", "no"],
            "mean": [100.1, 100.1],
            "mean_normal": [22.2, 22.2],
            "local_rmsd_normal": [0.1, None],
            "local_rmsd": [0.9, None],
            "norm_step_005_normal": [0.1, 0.1],
            "norm_step_005": [0.9, 0.9],
            "cosmic_30x_cov": [2.5, 2.5],
        }

    def test_that_130_125_cancer_summary_metrics_round_trip_works(self):
        original_type = metrics_1_3_0.CancerSummaryMetrics
        new_type = metrics_1_2_5.CancerSummaryMetrics

        for config in self.product_dict(**self.originals_config):
            original = original_type(**config)

            migrated = MigrateMetrics130To125().migrate_cancer_summary_metrics(original)
            round_tripped = MigrateMetrics125To130().migrate_cancer_summary_metrics(
                migrated
            )

            self.assertTrue(isinstance(migrated, new_type))
            self.assertTrue(isinstance(round_tripped, original_type))

            ignore_fields = ["norm_step_005_normal", "norm_step_005"]
            for optional_field in ["local_rmsd", "local_rmsd_normal"]:
                if not config[optional_field]:
                    ignore_fields.append(optional_field)

            self.assertFalse(
                BaseRoundTripper().diff_round_tripped(
                    original=original,
                    round_tripped=round_tripped,
                    ignore_fields=ignore_fields,
                )
            )

    def product_dict(self, **kwargs):
        keys = kwargs.keys()
        for instance in product(*kwargs.values()):
            yield dict(zip(keys, instance))
