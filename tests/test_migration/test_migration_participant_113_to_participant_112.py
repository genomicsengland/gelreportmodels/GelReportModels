from protocols.migration.migration_participant_113_to_participant_112 import (
    MigrateParticipant113To112,
)
from protocols.protocol_7_2 import participant as participant_1_1_2
from protocols.protocol_7_2_1 import participant as participant_1_1_3
from tests.test_migration.base_test_migration import TestCaseMigration


class TestMigrateParticipant113To112(TestCaseMigration):
    old_participant = participant_1_1_3
    new_participant = participant_1_1_2

    def test_migrate_pedigree(self):
        pedigree_1_1_3 = self.get_valid_object(
            object_type=self.old_participant.Pedigree,
            version=self.version_7_2_1,
            fill_nullables=True,
        )
        pedigree_1_1_2 = MigrateParticipant113To112().migrate_pedigree(
            old_pedigree=pedigree_1_1_3
        )
        self.assertIsInstance(pedigree_1_1_2, self.new_participant.Pedigree)
        self.assertTrue(
            self.new_participant.Pedigree.validate(pedigree_1_1_2.toJsonDict())
        )
        pedigree_member = pedigree_1_1_2.members[0]
        self.assertTrue(
            self.new_participant.PedigreeMember.validate(pedigree_member.toJsonDict())
        )
        sample = pedigree_member.samples[0]
        self.assertTrue(self.new_participant.Sample.validate(sample.toJsonDict()))
        self.assertIsInstance(sample.labSampleId, int)

    def test_migrate_pedigree_lab_sample_id_is_large_integer(self):
        pedigree_1_1_3 = self.get_valid_object(
            object_type=self.old_participant.Pedigree,
            version=self.version_7_2_1,
            fill_nullables=True,
        )
        pedigree_1_1_3.members[0].samples[0].labSampleId = "4028677963"
        pedigree_1_1_2 = MigrateParticipant113To112().migrate_pedigree(
            old_pedigree=pedigree_1_1_3
        )
        self.assertIsInstance(pedigree_1_1_2, self.new_participant.Pedigree)
        self.assertTrue(
            self.new_participant.Pedigree.validate(pedigree_1_1_2.toJsonDict())
        )
        pedigree_member = pedigree_1_1_2.members[0]
        self.assertTrue(
            self.new_participant.PedigreeMember.validate(pedigree_member.toJsonDict())
        )
        sample = pedigree_member.samples[0]
        self.assertTrue(self.new_participant.Sample.validate(sample.toJsonDict()))
        self.assertIsInstance(sample.labSampleId, int)

    def test_migrate_cancer_participant(self):
        cancer_participant_1_1_3 = self.get_valid_object(
            object_type=self.old_participant.CancerParticipant,
            version=self.version_7_2_1,
            fill_nullables=True,
        )
        cancer_participant_1_1_2 = (
            MigrateParticipant113To112().migrate_cancer_participant(
                old_cancer_participant=cancer_participant_1_1_3
            )
        )
        self.assertIsInstance(
            cancer_participant_1_1_2, self.new_participant.CancerParticipant
        )
        self.assertTrue(
            self.new_participant.CancerParticipant.validate(
                cancer_participant_1_1_2.toJsonDict()
            )
        )
        self.assertIsInstance(
            cancer_participant_1_1_2.tumourSamples[0].labSampleId, int
        )
        self.assertIsInstance(
            cancer_participant_1_1_2.germlineSamples[0].labSampleId, int
        )
