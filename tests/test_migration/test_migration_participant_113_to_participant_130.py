from protocols.migration.migration_participant_113_to_participant_130 import (
    MigrateParticipant113To130,
)
from protocols.protocol_7_2_1 import participant as participant_1_1_3
from protocols.protocol_7_7 import participant as participant_1_3_0
from tests.test_migration.base_test_migration import TestCaseMigration


class TestMigrateParticipant113To130(TestCaseMigration):
    old_participant = participant_1_1_3
    new_participant = participant_1_3_0

    def test_migrate_pedigree(self):
        pedigree_1_1_3 = self.get_valid_object(
            object_type=self.old_participant.Pedigree,
            version=self.version_7_2_1,
            fill_nullables=True,
        )
        pedigree_1_3_0 = MigrateParticipant113To130().migrate_pedigree(
            old_pedigree=pedigree_1_1_3
        )
        self.assertIsInstance(pedigree_1_3_0, self.new_participant.Pedigree)
        self.assertTrue(
            self.new_participant.Pedigree.validate(pedigree_1_3_0.toJsonDict())
        )
        self.assertIsInstance(pedigree_1_3_0.members[0].hpoTermList, list)

    def test_migrate_pedigree_with_null_values(self):
        pedigree_1_1_3 = self.get_valid_object(
            object_type=self.old_participant.Pedigree,
            version=self.version_7_2_1,
            fill_nullables=True,
        )
        pedigree_1_1_3.members[0].hpoTermList[0].modifiers = None
        pedigree_1_3_0 = MigrateParticipant113To130().migrate_pedigree(
            old_pedigree=pedigree_1_1_3
        )
        self.assertIsInstance(pedigree_1_3_0, self.new_participant.Pedigree)
        self.assertTrue(
            self.new_participant.Pedigree.validate(pedigree_1_3_0.toJsonDict())
        )
        self.assertIsInstance(pedigree_1_3_0.members[0].hpoTermList, list)

    def test_migrate_cancer_participant(self):
        cancer_particpant_1_1_3 = self.get_valid_object(
            object_type=self.old_participant.CancerParticipant,
            version=self.version_7_2_1,
            fill_nullables=True,
        )
        cancer_particpant_1_3_0 = (
            MigrateParticipant113To130().migrate_cancer_participant(
                old_participant=cancer_particpant_1_1_3
            )
        )
        self.assertIsInstance(
            cancer_particpant_1_3_0, self.new_participant.CancerParticipant
        )
        self.assertTrue(
            self.new_participant.CancerParticipant.validate(
                cancer_particpant_1_3_0.toJsonDict()
            )
        )

    def test_migrate_cancer_participant_nullable(self):
        cancer_particpant_1_1_3 = self.get_valid_object(
            object_type=self.old_participant.CancerParticipant,
            version=self.version_7_2_1,
            fill_nullables=False,
        )
        cancer_particpant_1_3_0 = (
            MigrateParticipant113To130().migrate_cancer_participant(
                old_participant=cancer_particpant_1_1_3
            )
        )
        self.assertIsInstance(
            cancer_particpant_1_3_0, self.new_participant.CancerParticipant
        )
        self.assertTrue(
            self.new_participant.CancerParticipant.validate(
                cancer_particpant_1_3_0.toJsonDict()
            )
        )

    def test_migrate_cancer_participant_morphologies_and_topographies_null(self):
        cancer_particpant_1_1_3 = self.get_valid_object(
            object_type=self.old_participant.CancerParticipant,
            version=self.version_7_2_1,
            fill_nullables=True,
        )
        cancer_particpant_1_1_3.tumourSamples[0].morphologyICD = None
        cancer_particpant_1_1_3.tumourSamples[0].morphologySnomedCT = None
        cancer_particpant_1_1_3.tumourSamples[0].morphologySnomedRT = None
        cancer_particpant_1_1_3.tumourSamples[0].topographyICD = None
        cancer_particpant_1_1_3.tumourSamples[0].topographySnomedCT = None
        cancer_particpant_1_1_3.tumourSamples[0].topographySnomedRT = None
        cancer_particpant_1_3_0 = (
            MigrateParticipant113To130().migrate_cancer_participant(
                old_participant=cancer_particpant_1_1_3
            )
        )
        self.assertIsInstance(
            cancer_particpant_1_3_0, self.new_participant.CancerParticipant
        )
        self.assertTrue(
            self.new_participant.CancerParticipant.validate(
                cancer_particpant_1_3_0.toJsonDict()
            )
        )
