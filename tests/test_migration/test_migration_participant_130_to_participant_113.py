from protocols.migration.migration_participant_130_to_participant_113 import (
    MigrateParticipant130To113,
)
from protocols.protocol_7_2_1 import participant as participant_1_1_3
from protocols.protocol_7_7 import participant as participant_1_3_0
from tests.test_migration.base_test_migration import TestCaseMigration


class TestMigrateParticipant130To113(TestCaseMigration):
    old_participant = participant_1_3_0
    new_participant = participant_1_1_3

    def test_migrate_pedigree(self):
        pedigree_1_3_0 = self.get_valid_object(
            object_type=self.old_participant.Pedigree,
            version=self.version_7_7,
            fill_nullables=True,
        )
        pedigree_1_1_3 = MigrateParticipant130To113().migrate_pedigree(
            old_pedigree=pedigree_1_3_0
        )
        self.assertIsInstance(pedigree_1_1_3, self.new_participant.Pedigree)
        self.assertTrue(
            self.new_participant.Pedigree.validate(pedigree_1_1_3.toJsonDict())
        )

    def test_migrate_pedigree_with_null_values(self):
        pedigree_1_3_0 = self.get_valid_object(
            object_type=self.old_participant.Pedigree,
            version=self.version_7_7,
            fill_nullables=True,
        )
        pedigree_1_3_0.members[0].hpoTermList[0].modifiers = None
        pedigree_1_1_3 = MigrateParticipant130To113().migrate_pedigree(
            old_pedigree=pedigree_1_3_0
        )
        self.assertIsInstance(pedigree_1_1_3, self.new_participant.Pedigree)
        self.assertTrue(
            self.new_participant.Pedigree.validate(pedigree_1_1_3.toJsonDict())
        )

    def test_migrate_cancer_participant(self):
        cancer_participant_1_3_0 = self.get_valid_object(
            object_type=self.old_participant.CancerParticipant,
            version=self.version_7_7,
            fill_nullables=True,
        )
        cancer_participant_1_1_3 = (
            MigrateParticipant130To113().migrate_cancer_participant(
                old_participant=cancer_participant_1_3_0, organisation_code="tmp"
            )
        )
        self.assertIsInstance(
            cancer_participant_1_1_3, self.new_participant.CancerParticipant
        )
        self.assertTrue(
            self.new_participant.CancerParticipant.validate(
                cancer_participant_1_1_3.toJsonDict()
            )
        )

    def test_migrate_cancer_participant_null_values(self):
        cancer_participant_1_3_0 = self.get_valid_object(
            object_type=self.old_participant.CancerParticipant,
            version=self.version_7_7,
            fill_nullables=False,
        )
        cancer_participant_1_1_3 = (
            MigrateParticipant130To113().migrate_cancer_participant(
                old_participant=cancer_participant_1_3_0, organisation_code="tmp"
            )
        )
        self.assertIsInstance(
            cancer_participant_1_1_3, self.new_participant.CancerParticipant
        )
        self.assertTrue(
            self.new_participant.CancerParticipant.validate(
                cancer_participant_1_1_3.toJsonDict()
            )
        )

    def test_migrate_cancer_participant_sample_morphology_and_topology_null(self):
        cancer_participant_1_3_0 = self.get_valid_object(
            object_type=self.old_participant.CancerParticipant,
            version=self.version_7_7,
            fill_nullables=True,
        )
        cancer_participant_1_3_0.tumourSamples[0].sampleMorphologies = None
        cancer_participant_1_3_0.tumourSamples[0].sampleTopographies = None
        cancer_participant_1_1_3 = (
            MigrateParticipant130To113().migrate_cancer_participant(
                old_participant=cancer_participant_1_3_0, organisation_code="tmp"
            )
        )
        self.assertIsInstance(
            cancer_participant_1_1_3, self.new_participant.CancerParticipant
        )
        self.assertTrue(
            self.new_participant.CancerParticipant.validate(
                cancer_participant_1_1_3.toJsonDict()
            )
        )

    def test_migrate_tumour_sample(self):
        tumour_sample_1_3_0 = self.get_valid_object(
            object_type=self.old_participant.TumourSample,
            version=self.version_7_7,
            fill_nullables=True,
        )
        tumour_sample_1_1_3 = MigrateParticipant130To113().migrate_tumour_sample(
            old_sample=tumour_sample_1_3_0, organisation_code="tmp"
        )
        self.assertIsInstance(tumour_sample_1_1_3, self.new_participant.TumourSample)
        self.assertTrue(
            self.new_participant.TumourSample.validate(tumour_sample_1_1_3.toJsonDict())
        )

    def test_migrate_tumour_sample_null_name_and_value(self):
        tumour_sample_1_3_0 = self.get_valid_object(
            object_type=self.old_participant.TumourSample,
            version=self.version_7_7,
            fill_nullables=True,
        )
        tumour_sample_1_3_0.sampleMorphologies = [
            self.old_participant.Morphology.fromJsonDict({"id": sample.id})
            for sample in tumour_sample_1_3_0.sampleMorphologies
        ]

        tumour_sample_1_1_3 = MigrateParticipant130To113().migrate_tumour_sample(
            old_sample=tumour_sample_1_3_0, organisation_code="tmp"
        )
        self.assertIsInstance(tumour_sample_1_1_3, self.new_participant.TumourSample)
        self.assertTrue(
            self.new_participant.TumourSample.validate(tumour_sample_1_1_3.toJsonDict())
        )

    def test_migrate_tumour_sample_null_except_tumour_id(self):
        tumour_sample_1_3_0 = self.get_valid_object(
            object_type=self.old_participant.TumourSample,
            version=self.version_7_7,
            fill_nullables=False,
        )
        # The tumour id is nullable but is actually required
        tumour_sample_1_3_0.tumourId = "tmp"
        tumour_sample_1_1_3 = MigrateParticipant130To113().migrate_tumour_sample(
            old_sample=tumour_sample_1_3_0, organisation_code="tmp"
        )
        self.assertIsInstance(tumour_sample_1_1_3, self.new_participant.TumourSample)
        self.assertTrue(
            self.new_participant.TumourSample.validate(tumour_sample_1_1_3.toJsonDict())
        )

    def test_migrate_tumour_sample_null_raises_exception(self):
        tumour_sample_1_3_0 = self.get_valid_object(
            object_type=self.old_participant.TumourSample,
            version=self.version_7_7,
            fill_nullables=False,
        )
        tumour_sample_1_1_3 = MigrateParticipant130To113().migrate_tumour_sample(
            old_sample=tumour_sample_1_3_0, organisation_code="tmp"
        )
        self.assertIsInstance(tumour_sample_1_1_3, self.new_participant.TumourSample)
        self.assertFalse(
            self.new_participant.TumourSample.validate(tumour_sample_1_1_3.toJsonDict())
        )
