import unittest

from protocols.migration.migration_participant_130_to_participant_140 import (
    MigrateParticipant130To140,
)
from protocols.protocol_7_7 import participant as participant_1_3_0
from protocols.protocol_7_11 import participant as participant_1_4_0
from tests.test_migration.base_test_migration import TestCaseMigration


class TestMigrateParticipant130To140(TestCaseMigration):
    old_participant = participant_1_3_0
    new_participant = participant_1_4_0

    def test_migrate_pedigree(self):
        pedigree_1_3_0 = self.get_valid_object(
            object_type=self.old_participant.Pedigree,
            version=self.version_7_7,
            fill_nullables=False,
        )
        pedigree_1_4_0 = MigrateParticipant130To140().migrate_pedigree(
            old_pedigree=pedigree_1_3_0
        )
        self.assertIsInstance(pedigree_1_4_0, self.new_participant.Pedigree)
        self.assertTrue(
            self.new_participant.Pedigree.validate(pedigree_1_4_0.toJsonDict())
        )

    def test_migrate_cancer_participant(self):
        cancer_participant_1_3_0 = self.get_valid_object(
            object_type=self.old_participant.CancerParticipant,
            version=self.version_7_11,
            fill_nullables=False,
        )
        cancer_participant_1_4_0 = (
            MigrateParticipant130To140().migrate_cancer_participant(
                old_cancer_participant=cancer_participant_1_3_0
            )
        )
        self.assertIsInstance(
            cancer_participant_1_4_0, self.new_participant.CancerParticipant
        )
        self.assertTrue(
            self.new_participant.CancerParticipant.validate(
                cancer_participant_1_4_0.toJsonDict()
            )
        )

    def test_migrate_referral(self):
        referral_1_3_0 = self.get_valid_object(
            object_type=self.old_participant.Referral,
            version=self.version_7_7,
            fill_nullables=False,
        )
        referral_1_4_0 = MigrateParticipant130To140().migrate_referral(
            old_referral=referral_1_3_0
        )
        self.assertIsInstance(referral_1_4_0, self.new_participant.Referral)
        referral_1_4_0.raise_if_invalid()
