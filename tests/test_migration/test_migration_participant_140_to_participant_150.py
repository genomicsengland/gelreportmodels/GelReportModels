from protocols.protocol_7_11 import participant as participant_1_4_0
from protocols.protocol_8_0 import participant as participant_1_5_0

from tests.test_migration.base_test_migration import TestCaseMigration
from protocols.migration.migration_participant_140_to_participant_150 import (
    MigrateParticipant140To150,
)


class TestMigrateParticipant140To150(TestCaseMigration):
    old_participant = participant_1_4_0
    new_participant = participant_1_5_0

    def test_migrate_germline(self):
        sample = self.get_valid_object(
            object_type=self.old_participant.GermlineSample,
            version=self.version_7_11,
            fill_nullables=True,
        )
        new_sample = MigrateParticipant140To150().migrate_germline_sample(
            old_sample=sample
        )
        self.assertIsInstance(new_sample, self.new_participant.GermlineSample)
        self.assertTrue(
            self.new_participant.GermlineSample.validate(new_sample.toJsonDict())
        )

    def test_migrate_tumour(self):
        sample = self.get_valid_object(
            object_type=self.old_participant.TumourSample,
            version=self.version_7_11,
            fill_nullables=True,
        )
        new_sample = MigrateParticipant140To150().migrate_tumour_sample(
            old_sample=sample
        )
        self.assertIsInstance(new_sample, self.new_participant.TumourSample)
        self.assertTrue(
            self.new_participant.TumourSample.validate(new_sample.toJsonDict())
        )
