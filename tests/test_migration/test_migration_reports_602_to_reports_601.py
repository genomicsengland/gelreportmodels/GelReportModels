from protocols.migration.migration_reports_602_to_reports_601 import (
    MigrateReports602To601,
)
from protocols.protocol_7_2 import reports as reports_6_0_1
from protocols.protocol_7_2_1 import reports as reports_6_0_2
from tests.test_migration.base_test_migration import TestCaseMigration


class TestMigrateReport602To601(TestCaseMigration):
    old_reports = reports_6_0_2
    new_reports = reports_6_0_1

    def test_migrate_interpretation_request_rd(self):
        i_rd_6_0_2 = self.get_valid_object(
            object_type=self.old_reports.InterpretationRequestRD,
            version=self.version_7_2_1,
            fill_nullables=True,
        )
        i_rd_6_0_1 = MigrateReports602To601().migrate_interpretation_request_rd(
            old_instance=i_rd_6_0_2
        )
        self.assertIsInstance(i_rd_6_0_1, self.new_reports.InterpretationRequestRD)
        self.assertTrue(
            self.new_reports.InterpretationRequestRD.validate(i_rd_6_0_1.toJsonDict())
        )
        self.assertIsInstance(
            i_rd_6_0_1.pedigree.members[0].samples[0].labSampleId, int
        )

    def test_migrate_interpretation_request_cancer(self):
        cancer_6_0_2 = self.get_valid_object(
            object_type=self.old_reports.CancerInterpretationRequest,
            version=self.version_7_2_1,
            fill_nullables=True,
        )
        cancer_6_0_1 = MigrateReports602To601().migrate_interpretation_request_cancer(
            old_instance=cancer_6_0_2
        )
        self.assertIsInstance(
            cancer_6_0_1, self.new_reports.CancerInterpretationRequest
        )
        self.assertTrue(
            self.new_reports.CancerInterpretationRequest.validate(
                cancer_6_0_1.toJsonDict()
            )
        )
        self.assertIsInstance(
            cancer_6_0_1.cancerParticipant.tumourSamples[0].labSampleId, int
        )
        self.assertIsInstance(
            cancer_6_0_1.cancerParticipant.germlineSamples[0].labSampleId, int
        )
