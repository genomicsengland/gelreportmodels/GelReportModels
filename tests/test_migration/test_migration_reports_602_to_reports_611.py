from protocols.migration.migration_reports_602_to_reports_611 import (
    MigrateReports602To611,
)
from protocols.protocol_7_2_1 import reports as reports_6_0_2
from protocols.protocol_7_7 import reports as reports_6_1_1
from tests.test_migration.base_test_migration import TestCaseMigration


class TestMigrateReport602To611(TestCaseMigration):
    old_reports = reports_6_0_2
    new_reports = reports_6_1_1

    def test_migrate_interpretation_request_rd(self):
        i_rd_6_0_2 = self.get_valid_object(
            object_type=self.old_reports.InterpretationRequestRD,
            version=self.version_7_2_1,
            fill_nullables=True,
        )
        i_rd_6_1_1 = MigrateReports602To611().migrate_interpretation_request_rd(
            old_instance=i_rd_6_0_2
        )
        self.assertIsInstance(i_rd_6_1_1, self.new_reports.InterpretationRequestRD)
        self.assertTrue(
            self.new_reports.InterpretationRequestRD.validate(i_rd_6_1_1.toJsonDict())
        )

    def test_migrate_cancer_interpretation_request(self):
        i_rd_6_0_2 = self.get_valid_object(
            object_type=self.old_reports.CancerInterpretationRequest,
            version=self.version_7_2_1,
            fill_nullables=True,
        )
        i_rd_6_1_1 = MigrateReports602To611().migrate_cancer_interpretation_request(
            old_instance=i_rd_6_0_2
        )
        self.assertIsInstance(i_rd_6_1_1, self.new_reports.CancerInterpretationRequest)
        self.assertTrue(
            self.new_reports.CancerInterpretationRequest.validate(
                i_rd_6_1_1.toJsonDict()
            )
        )
