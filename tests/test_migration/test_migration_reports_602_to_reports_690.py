from protocols.migration.migration_reports_602_to_reports_690 import (
    MigrateReports602To690,
)
from protocols.protocol_7_2_1 import reports as reports_6_0_2
from protocols.protocol_8_6 import reports as reports_6_9_0
from tests.test_migration.base_test_migration import TestCaseMigration


class TestMigrateReport602To690(TestCaseMigration):
    old_reports = reports_6_0_2
    new_reports = reports_6_9_0

    def test_migrate_clinical_report_rd(self):
        i_cr_602 = self.get_valid_object(
            object_type=self.old_reports.ClinicalReport,
            version=self.version_7_2_1,
            fill_nullables=True,
        )

        i_cr_690 = MigrateReports602To690().migrate_clinical_report_rd(
            old_instance=i_cr_602
        )

        self.assertIsInstance(i_cr_690, self.new_reports.ClinicalReport)
        self.assertTrue(self.new_reports.ClinicalReport.validate(i_cr_690.toJsonDict()))

    def test_migrate_clinical_report_cancer(self):
        i_cr_602 = self.get_valid_object(
            object_type=self.old_reports.ClinicalReport,
            version=self.version_7_2_1,
            fill_nullables=True,
        )

        i_cr_690 = MigrateReports602To690().migrate_clinical_report_cancer(
            old_instance=i_cr_602
        )

        self.assertIsInstance(i_cr_690, self.new_reports.ClinicalReport)
        self.assertTrue(self.new_reports.ClinicalReport.validate(i_cr_690.toJsonDict()))

    def test_migrate_exit_questionnaire_rd(self):
        i_eqrd_602 = self.get_valid_object(
            object_type=self.old_reports.RareDiseaseExitQuestionnaire,
            version=self.version_7_2_1,
            fill_nullables=True,
        )

        i_eqrd_690 = MigrateReports602To690().migrate_exit_questionnaire_rd(
            old_instance=i_eqrd_602
        )

        self.assertIsInstance(i_eqrd_690, self.new_reports.RareDiseaseExitQuestionnaire)
        self.assertTrue(
            self.new_reports.RareDiseaseExitQuestionnaire.validate(
                i_eqrd_690.toJsonDict()
            )
        )

    def test_migrate_exit_questionnaire_cancer(self):
        i_eq_cancer_602 = self.get_valid_object(
            object_type=self.old_reports.CancerExitQuestionnaire,
            version=self.version_7_2_1,
            fill_nullables=True,
        )

        i_eq_cancer_690 = MigrateReports602To690().migrate_exit_questionnaire_cancer(
            old_instance=i_eq_cancer_602
        )

        self.assertIsInstance(i_eq_cancer_690, self.new_reports.CancerExitQuestionnaire)
        self.assertTrue(
            self.new_reports.CancerExitQuestionnaire.validate(
                i_eq_cancer_690.toJsonDict()
            )
        )

    def test_migrate_exit_questionnaire_cancer_no_variant_coordinates(self):
        i_eq_cancer_602 = self.get_valid_object(
            object_type=self.old_reports.CancerExitQuestionnaire,
            version=self.version_7_2_1,
            fill_nullables=True,
        )

        i_eq_cancer_602.otherActionableVariants[0].variantCoordinates = None
        i_eq_cancer_690 = MigrateReports602To690().migrate_exit_questionnaire_cancer(
            old_instance=i_eq_cancer_602
        )

        self.assertIsInstance(i_eq_cancer_690, self.new_reports.CancerExitQuestionnaire)
        self.assertTrue(
            self.new_reports.CancerExitQuestionnaire.validate(
                i_eq_cancer_690.toJsonDict()
            )
        )
