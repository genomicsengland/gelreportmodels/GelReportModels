from protocols.migration.migration_reports_6100_to_reports_690 import (
    MigrateReports6100To690,
)
from protocols.protocol_8_8 import reports as reports_6_9_0
from protocols.protocol_8_9 import reports as reports_6_10_0
from tests.test_migration.base_test_migration import TestCaseMigration


class TestMigrateReports6100To690(TestCaseMigration):
    old_reports = reports_6_10_0
    new_reports = reports_6_9_0

    def test_given_empty_nullable_fields_returns_a_valid_version_6_9_VariantAttributes_object(
        self,
    ):
        valid_variant_attribute_6_10_0 = self.get_valid_object(
            object_type=self.old_reports.VariantAttributes,
            version=self.version_8_9,
            fill_nullables=False,
        )

        self.assertTrue(
            self.old_reports.VariantAttributes.validate(
                valid_variant_attribute_6_10_0.toJsonDict()
            )
        )
        # Migrate
        valid_variant_attribute_6_9_0 = (
            MigrateReports6100To690().migrate_variant_attributes(
                old_instance=valid_variant_attribute_6_10_0
            )
        )
        self.assertTrue(
            self.new_reports.VariantAttributes.validate(
                valid_variant_attribute_6_9_0.toJsonDict()
            )
        )

    def test_given_populated_nullable_fields_returns_a_valid_version_6_9_VariantAttributes_object(
        self,
    ):
        valid_variant_attribute_6_10_0 = self.get_valid_object(
            object_type=self.old_reports.VariantAttributes,
            version=self.version_8_9,
            fill_nullables=True,
        )

        self.assertTrue(
            self.old_reports.VariantAttributes.validate(
                valid_variant_attribute_6_10_0.toJsonDict()
            )
        )
        # Migrate
        valid_variant_attribute_6_9_0 = (
            MigrateReports6100To690().migrate_variant_attributes(
                old_instance=valid_variant_attribute_6_10_0
            )
        )
        self.assertTrue(
            self.new_reports.VariantAttributes.validate(
                valid_variant_attribute_6_9_0.toJsonDict()
            )
        )

    def test_given_empty_nullable_fields_returns_a_valid_version_6_9_InterpretedGenome_object(
        self,
    ):
        valid_interpreted_genome_6_10_0 = self.get_valid_object(
            object_type=self.old_reports.InterpretedGenome,
            version=self.version_8_9,
            fill_nullables=False,
        )

        self.assertTrue(
            self.old_reports.InterpretedGenome.validate(
                valid_interpreted_genome_6_10_0.toJsonDict()
            )
        )
        # Migrate
        valid_interpreted_genome_6_9_0 = (
            MigrateReports6100To690().migrate_interpreted_genome(
                old_instance=valid_interpreted_genome_6_10_0
            )
        )
        self.assertTrue(
            self.new_reports.InterpretedGenome.validate(
                valid_interpreted_genome_6_9_0.toJsonDict()
            )
        )

    def test_given_populated_nullable_fields_returns_a_valid_version_6_9_InterpretedGenome_object(
        self,
    ):
        valid_interpreted_genome_6_10_0 = self.get_valid_object(
            object_type=self.old_reports.InterpretedGenome,
            version=self.version_8_9,
            fill_nullables=True,
        )

        self.assertTrue(
            self.old_reports.InterpretedGenome.validate(
                valid_interpreted_genome_6_10_0.toJsonDict()
            )
        )
        # Migrate
        valid_interpreted_genome_6_9_0 = (
            MigrateReports6100To690().migrate_interpreted_genome(
                old_instance=valid_interpreted_genome_6_10_0
            )
        )

        self.assertTrue(
            self.new_reports.InterpretedGenome.validate(
                valid_interpreted_genome_6_9_0.toJsonDict()
            )
        )
