from protocols.migration.migration_reports_610_to_reports_611 import (
    MigrateReports610To611,
)
from protocols.protocol_7_3 import reports as reports_6_1_0
from protocols.protocol_7_7 import reports as reports_6_1_1
from tests.test_migration.base_test_migration import TestCaseMigration


class TestMigrateReport610To611(TestCaseMigration):
    old_reports = reports_6_1_0
    new_reports = reports_6_1_1

    def test_migrate_interpretation_request_rd(self):
        i_rd_6_1_0 = self.get_valid_object(
            object_type=self.old_reports.InterpretationRequestRD,
            version=self.version_7_3,
            fill_nullables=True,
        )
        i_rd_6_1_1 = MigrateReports610To611().migrate_interpretation_request_rd(
            old_instance=i_rd_6_1_0
        )
        self.assertIsInstance(i_rd_6_1_1, self.new_reports.InterpretationRequestRD)
        self.assertTrue(
            self.new_reports.InterpretationRequestRD.validate(i_rd_6_1_1.toJsonDict())
        )
        self.assertIsInstance(
            i_rd_6_1_1.pedigree.members[0].samples[0].labSampleId, str
        )

    def test_migrate_interpretation_request_cancer(self):
        cancer_6_1_0 = self.get_valid_object(
            object_type=self.old_reports.CancerInterpretationRequest,
            version=self.version_7_3,
            fill_nullables=True,
        )
        cancer_6_1_1 = MigrateReports610To611().migrate_interpretation_request_cancer(
            old_instance=cancer_6_1_0
        )
        self.assertIsInstance(
            cancer_6_1_1, self.new_reports.CancerInterpretationRequest
        )
        self.assertTrue(
            self.new_reports.CancerInterpretationRequest.validate(
                cancer_6_1_1.toJsonDict()
            )
        )
        self.assertIsInstance(
            cancer_6_1_1.cancerParticipant.tumourSamples[0].labSampleId, str
        )
        self.assertIsInstance(
            cancer_6_1_1.cancerParticipant.germlineSamples[0].labSampleId, str
        )
