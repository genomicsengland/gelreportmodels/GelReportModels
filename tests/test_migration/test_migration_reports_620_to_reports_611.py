import unittest

from protocols.migration.migration_reports_620_to_reports_611 import (
    MigrateReports620To611,
)
from protocols.protocol_7_7 import reports as reports_6_1_1
from protocols.protocol_7_8 import reports as reports_6_2_0
from tests.test_migration.base_test_migration import TestCaseMigration


class TestMigrateReport620To611(TestCaseMigration):
    old_reports = reports_6_2_0
    new_reports = reports_6_1_1

    def run_generic_tests(self, vil_6_1_1):
        self.assertIsInstance(vil_6_1_1, self.new_reports.VariantInterpretationLog)
        vil_6_1_1.raise_if_invalid()

    def test_migrate_variant_interpretation_log_role_validation(self):
        vil_6_2_0 = self.get_valid_object(
            object_type=self.old_reports.VariantInterpretationLog,
            version=self.version_7_8,
            fill_nullables=True,
        )
        # Set some values
        vil_6_2_0.user.role = None
        vil_6_2_0.user.groups = None
        vil_6_2_0.variantValidation.validationResult = "Pending"
        # Test we haven't broken the model in previous step
        self.assertTrue(
            self.old_reports.VariantInterpretationLog.validate(vil_6_2_0.toJsonDict())
        )
        # Migrate
        vil_6_1_1 = MigrateReports620To611().migrate_variant_interpretation_log(
            old_instance=vil_6_2_0
        )
        # Test
        self.run_generic_tests(vil_6_1_1)
        self.assertEqual(vil_6_1_1.user.role, "")
        self.assertEqual(vil_6_1_1.variantValidation.validationResult, "NotPerformed")

    def test_migrate_variant_interpretation_log_acmg_evidence(self):
        vil_6_2_0 = self.get_valid_object(
            object_type=self.old_reports.VariantInterpretationLog,
            version=self.version_7_8,
            fill_nullables=True,
        )
        # Hard code clinical significance to enum available in both 6.2.0 and 6.1.1
        vil_6_2_0.variantClassification.acmgVariantClassification.clinicalSignificance = (
            "pathogenic"
        )
        acmg_evidences_6_2_0 = (
            vil_6_2_0.variantClassification.acmgVariantClassification.acmgEvidences
        )
        self.assertGreater(len(acmg_evidences_6_2_0), 1)
        # Set some values
        for acmg_evidence in acmg_evidences_6_2_0:
            acmg_evidence.type = "benign"
        for acmg_evidence in acmg_evidences_6_2_0:
            acmg_evidence.activationStrength = "very_strong"
        # Test we haven't broken the model in previous step
        self.assertTrue(
            self.old_reports.VariantInterpretationLog.validate(vil_6_2_0.toJsonDict())
        )
        # Migrate
        vil_6_1_1 = MigrateReports620To611().migrate_variant_interpretation_log(
            old_instance=vil_6_2_0
        )
        # Test
        self.run_generic_tests(vil_6_1_1)
        acmg_evidences_6_1_1 = (
            vil_6_1_1.variantClassification.acmgVariantClassification.acmgEvidences
        )
        self.assertEqual(len(acmg_evidences_6_2_0), len(acmg_evidences_6_1_1))
        for acmg_evidence in acmg_evidences_6_1_1:
            self.assertEqual(acmg_evidence.type, "bening")
            self.assertEqual(acmg_evidence.activationStrength, None)

    def test_migrate_variant_interpretation_log_comments(self):
        vil_6_2_0 = self.get_valid_object(
            object_type=self.old_reports.VariantInterpretationLog,
            version=self.version_7_8,
            fill_nullables=True,
        )
        self.assertGreater(len(vil_6_2_0.comments), 1)
        [self.assertTrue(comment.comment) for comment in vil_6_2_0.comments]
        # Migrate
        vil_6_1_1 = MigrateReports620To611().migrate_variant_interpretation_log(
            old_instance=vil_6_2_0
        )
        # Test
        self.run_generic_tests(vil_6_1_1)
        # Check all comments have copied over
        for i, vil_6_2_0_comment in enumerate(vil_6_2_0.comments):
            self.assertEqual(vil_6_1_1.comments[i], vil_6_2_0_comment.comment)

    def test_migrate_variant_interpretation_log_excluded(self):
        vil_6_2_0 = self.get_valid_object(
            object_type=self.old_reports.VariantInterpretationLog,
            version=self.version_7_8,
            fill_nullables=True,
        )
        vil_6_2_0.variantClassification.acmgVariantClassification.clinicalSignificance = (
            "excluded"
        )
        # Test we haven't broken the model in previous step
        self.assertTrue(
            self.old_reports.VariantInterpretationLog.validate(vil_6_2_0.toJsonDict())
        )
        # Migrate
        vil_6_1_1 = MigrateReports620To611().migrate_variant_interpretation_log(
            old_instance=vil_6_2_0
        )
        # Test
        self.run_generic_tests(vil_6_1_1)
        self.assertIs(vil_6_1_1.variantClassification.acmgVariantClassification, None)

    def test_migrate_interpreted_genome(self):
        interpreted_genome_6_2_0 = self.get_valid_object(
            object_type=self.old_reports.InterpretedGenome,
            version=self.version_7_8,
            fill_nullables=True,
        )

        # At this point, this should not be a valid 6.1.1 Interpreted_Genome.
        self.assertFalse(
            self.new_reports.InterpretedGenome.validate(
                interpreted_genome_6_2_0.toJsonDict()
            )
        )

        interpreted_genome_6_1_1 = MigrateReports620To611().migrate_interpreted_genome(
            old_instance=interpreted_genome_6_2_0
        )

        self.assertTrue(
            self.new_reports.InterpretedGenome.validate(
                interpreted_genome_6_1_1.toJsonDict()
            )
        )

    def test_migrate_interpreted_genome_without_nullables(self):
        interpreted_genome_6_2_0 = self.get_valid_object(
            object_type=self.old_reports.InterpretedGenome,
            version=self.version_7_8,
            fill_nullables=False,
        )

        interpreted_genome_6_1_1 = MigrateReports620To611().migrate_interpreted_genome(
            old_instance=interpreted_genome_6_2_0
        )

        self.assertTrue(
            self.new_reports.InterpretedGenome.validate(
                interpreted_genome_6_1_1.toJsonDict()
            )
        )

    def test_migrate_interpreted_genome_null_variant_classification(self):
        interpreted_genome_6_2_0 = self.get_valid_object(
            object_type=self.old_reports.InterpretedGenome,
            version=self.version_7_8,
            fill_nullables=True,
        )

        interpreted_genome_6_2_0.chromosomalRearrangements[0].reportEvents[
            0
        ].guidelineBasedVariantClassification = None
        interpreted_genome_6_2_0.chromosomalRearrangements[0].reportEvents[
            0
        ].variantClassification = None

        # At this point, this should not be a valid 6.1.1 Interpreted_Genome.
        self.assertFalse(
            self.new_reports.InterpretedGenome.validate(
                interpreted_genome_6_2_0.toJsonDict()
            )
        )

        interpreted_genome_6_1_1 = MigrateReports620To611().migrate_interpreted_genome(
            old_instance=interpreted_genome_6_2_0
        )

        interpreted_genome_6_1_1.raise_if_invalid()
