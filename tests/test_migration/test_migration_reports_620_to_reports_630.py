from protocols.migration.migration_reports_620_to_reports_630 import (
    MigrateReports620To630,
)
from protocols.protocol_7_8 import reports as reports_6_2_0
from protocols.protocol_7_9 import reports as reports_6_3_0
from tests.test_migration.base_test_migration import TestCaseMigration


class TestMigrateReport620To630(TestCaseMigration):
    old_reports = reports_6_2_0
    new_reports = reports_6_3_0

    def test_migrate_variant_interpretation_log(self):
        vil_6_2_0 = self.get_valid_object(
            object_type=self.old_reports.VariantInterpretationLog,
            version=self.version_7_8,
            fill_nullables=True,
        )
        vil_6_2_0.timestamp = "2020-01-31"
        # Test we haven't broken the model in previous step
        self.assertTrue(
            self.old_reports.VariantInterpretationLog.validate(vil_6_2_0.toJsonDict())
        )
        # Migrate
        vil_6_3_0 = MigrateReports620To630().migrate_variant_interpretation_log(
            old_instance=vil_6_2_0
        )
        # Test
        self.assertIsInstance(vil_6_3_0, self.new_reports.VariantInterpretationLog)
        self.assertTrue(
            self.new_reports.VariantInterpretationLog.validate(vil_6_3_0.toJsonDict())
        )
        self.assertIsInstance(
            vil_6_3_0.versionControl, self.new_reports.ReportVersionControl
        )
        self.assertEqual(vil_6_3_0.versionControl.gitVersionControl, "6.3.0")
        self.assertEqual(vil_6_3_0.timestamp, "2020-01-31T00:00:00+00:00")
