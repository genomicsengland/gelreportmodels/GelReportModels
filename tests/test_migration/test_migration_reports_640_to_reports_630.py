from protocols.migration.migration_reports_640_to_reports_630 import (
    MigrateReports640To630,
)
from protocols.protocol_7_9 import reports as reports_6_3_0
from protocols.protocol_7_10 import reports as reports_6_4_0
from tests.test_migration.base_test_migration import TestCaseMigration


class TestMigrateReport640To630(TestCaseMigration):
    old_reports = reports_6_4_0
    new_reports = reports_6_3_0

    def test_migrate_interpreted_genome(self):
        valid_genome_6_4_0 = self.get_valid_object(
            object_type=self.old_reports.InterpretedGenome,
            version=self.version_7_10,
            fill_nullables=False,
        )
        self.assertTrue(
            self.old_reports.InterpretedGenome.validate(valid_genome_6_4_0.toJsonDict())
        )
        # Migrate
        valid_genome_6_3_0 = MigrateReports640To630().migrate_interpreted_genome(
            old_instance=valid_genome_6_4_0
        )
        self.assertTrue(
            self.new_reports.InterpretedGenome.validate(valid_genome_6_3_0.toJsonDict())
        )

    def test_migrate_interpreted_genome_filled_nullables(self):
        valid_genome_6_4_0 = self.get_valid_object(
            object_type=self.old_reports.InterpretedGenome,
            version=self.version_7_10,
            fill_nullables=True,
        )
        self.assertTrue(
            self.old_reports.InterpretedGenome.validate(valid_genome_6_4_0.toJsonDict())
        )
        # Migrate
        valid_genome_6_3_0 = MigrateReports640To630().migrate_interpreted_genome(
            old_instance=valid_genome_6_4_0
        )
        self.assertTrue(
            self.new_reports.InterpretedGenome.validate(valid_genome_6_3_0.toJsonDict())
        )

    def test_migrate_interpreted_genome_new_enum_value(self):
        valid_genome_6_4_0 = self.get_valid_object(
            object_type=self.old_reports.InterpretedGenome,
            version=self.version_7_10,
            fill_nullables=True,
        )

        valid_genome_6_4_0.structuralVariants[
            0
        ].variantType = self.old_reports.StructuralVariantType.cnloh

        self.assertTrue(
            self.old_reports.InterpretedGenome.validate(valid_genome_6_4_0.toJsonDict())
        )
        # Migrate
        valid_genome_6_3_0 = MigrateReports640To630().migrate_interpreted_genome(
            old_instance=valid_genome_6_4_0
        )

        self.assertTrue(
            self.new_reports.InterpretedGenome.validate(valid_genome_6_3_0.toJsonDict())
        )
        self.assertTrue(
            valid_genome_6_3_0.structuralVariants[0].variantType
            == self.new_reports.StructuralVariantType.deletion
        )

    def test_migrate_report_event(self):
        valid_report_event_6_4_0 = self.get_valid_object(
            object_type=self.old_reports.ReportEvent,
            version=self.version_7_10,
            fill_nullables=False,
        )

        self.assertTrue(
            self.old_reports.ReportEvent.validate(valid_report_event_6_4_0.toJsonDict())
        )
        # Migrate
        valid_report_event_6_3_0 = MigrateReports640To630().migrate_report_events(
            old_instance=valid_report_event_6_4_0
        )
        self.assertTrue(
            self.new_reports.ReportEvent.validate(valid_report_event_6_3_0.toJsonDict())
        )

    def test_migrate_report_event_filled_nullables(self):
        valid_report_event_6_4_0 = self.get_valid_object(
            object_type=self.old_reports.ReportEvent,
            version=self.version_7_10,
            fill_nullables=True,
        )

        self.assertTrue(
            self.old_reports.ReportEvent.validate(valid_report_event_6_4_0.toJsonDict())
        )
        # Migrate
        valid_report_event_6_3_0 = MigrateReports640To630().migrate_report_events(
            old_instance=valid_report_event_6_4_0
        )
        self.assertTrue(
            self.new_reports.ReportEvent.validate(valid_report_event_6_3_0.toJsonDict())
        )

    def test_migrate_variant_attributes(self):
        valid_variant_attribute_6_4_0 = self.get_valid_object(
            object_type=self.old_reports.VariantAttributes,
            version=self.version_7_11,
            fill_nullables=False,
        )

        self.assertTrue(
            self.old_reports.VariantAttributes.validate(
                valid_variant_attribute_6_4_0.toJsonDict()
            )
        )
        # Migrate
        valid_variant_attribute_6_3_0 = (
            MigrateReports640To630().migrate_variant_attributes(
                old_instance=valid_variant_attribute_6_4_0
            )
        )
        self.assertTrue(
            self.new_reports.VariantAttributes.validate(
                valid_variant_attribute_6_3_0.toJsonDict()
            )
        )

    def test_migrate_variant_attributes_filled_nullable(self):
        valid_variant_attribute_6_4_0 = self.get_valid_object(
            object_type=self.old_reports.VariantAttributes,
            version=self.version_7_10,
            fill_nullables=True,
        )

        self.assertTrue(
            self.old_reports.VariantAttributes.validate(
                valid_variant_attribute_6_4_0.toJsonDict()
            )
        )
        # Migrate
        valid_variant_attribute_6_3_0 = (
            MigrateReports640To630().migrate_variant_attributes(
                old_instance=valid_variant_attribute_6_4_0
            )
        )
        self.assertTrue(
            self.new_reports.VariantAttributes.validate(
                valid_variant_attribute_6_3_0.toJsonDict()
            )
        )
