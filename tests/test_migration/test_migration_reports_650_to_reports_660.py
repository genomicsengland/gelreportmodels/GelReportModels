from protocols.migration.migration_reports_650_to_reports_660 import (
    MigrateReports650To660,
)
from protocols.protocol_7_13 import reports as reports_6_5_0
from protocols.protocol_8_0 import reports as reports_6_6_0
from tests.test_migration.base_test_migration import TestCaseMigration


class TestMigrateReport650To660(TestCaseMigration):
    new_model = reports_6_6_0
    old_model = reports_6_5_0

    def test_migrate_interpreted_genome(self):
        valid_genome = self.get_valid_object(
            object_type=self.old_model.InterpretedGenome,
            version=self.version_7_13,
            fill_nullables=False,
        )
        self.assertTrue(
            self.old_model.InterpretedGenome.validate(valid_genome.toJsonDict())
        )
        # Migrate
        valid_genome_converted = MigrateReports650To660().migrate_interpreted_genome(
            old_instance=valid_genome
        )
        self.assertTrue(
            self.new_model.InterpretedGenome.validate(
                valid_genome_converted.toJsonDict()
            )
        )

    def test_migrate_interpreted_genome_filled_nullables(self):
        valid_genome = self.get_valid_object(
            object_type=self.old_model.InterpretedGenome,
            version=self.version_7_13,
            fill_nullables=True,
        )
        self.assertTrue(
            self.old_model.InterpretedGenome.validate(valid_genome.toJsonDict())
        )
        # Migrate
        valid_genome_converted = MigrateReports650To660().migrate_interpreted_genome(
            old_instance=valid_genome
        )
        print(
            self.new_model.InterpretedGenome.validate_debug(
                valid_genome_converted.toJsonDict()
            )
        )
        self.assertTrue(
            self.new_model.InterpretedGenome.validate(
                valid_genome_converted.toJsonDict()
            )
        )

    def test_migrate_report_event(self):
        valid_report_event = self.get_valid_object(
            object_type=self.old_model.ReportEvent,
            version=self.version_7_13,
            fill_nullables=False,
        )

        self.assertTrue(
            self.old_model.ReportEvent.validate(valid_report_event.toJsonDict())
        )
        # Migrate
        valid_report_event_6_6_0 = MigrateReports650To660().migrate_report_events(
            old_instance=valid_report_event
        )
        self.assertTrue(
            self.new_model.ReportEvent.validate(
                valid_report_event_6_6_0.toJsonDict(validate=True)
            )
        )

    def test_migrate_report_event_filled_nullables(self):
        valid_report_event_old_model = self.get_valid_object(
            object_type=self.old_model.ReportEvent,
            version=self.version_7_13,
            fill_nullables=True,
        )

        self.assertTrue(
            self.old_model.ReportEvent.validate(
                valid_report_event_old_model.toJsonDict()
            )
        )
        # Migrate
        valid_report_event_new_model = MigrateReports650To660().migrate_report_events(
            old_instance=valid_report_event_old_model
        )
        self.assertTrue(
            self.new_model.ReportEvent.validate(
                valid_report_event_new_model.toJsonDict()
            )
        )

    def test_migrate_report_event_results_in_empty_manual_flags(self):
        valid_report_event = self.get_valid_object(
            object_type=self.old_model.ReportEvent,
            version=self.version_7_13,
            fill_nullables=True,
        )
        self.assertTrue(
            self.old_model.ReportEvent.validate(valid_report_event.toJsonDict())
        )
        # Migrate
        valid_report_event_converted = MigrateReports650To660().migrate_report_events(
            old_instance=valid_report_event
        )
        self.assertTrue(
            self.new_model.ReportEvent.validate(
                valid_report_event_converted.toJsonDict()
            )
        )
        self.assertIsNone(valid_report_event_converted.reportEventFlags)

    def test_copies_coordinates_to_both_locations_in_structural_variants_for_backward_compatibility(
        self,
    ):
        valid_variant_old_model = self.get_valid_object(
            object_type=self.old_model.StructuralVariant,
            version=self.version_7_13,
            fill_nullables=True,
        )
        self.assertTrue(
            self.old_model.StructuralVariant.validate(
                valid_variant_old_model.toJsonDict()
            )
        )

        # Migrate
        valid_variant_new_model = MigrateReports650To660()._migrate_structural_variant(
            valid_variant_old_model
        )

        self.assertTrue(
            self.new_model.StructuralVariant.validate(
                valid_variant_new_model.toJsonDict(validate=True)
            )
        )
        self.assertEqual(
            valid_variant_old_model.coordinates.toJsonDict(),
            valid_variant_new_model.coordinates.toJsonDict(),
        )
        self.assertEqual(
            valid_variant_old_model.coordinates.toJsonDict(),
            valid_variant_new_model.location.coordinates.toJsonDict(),
        )
