from protocols.migration.migration_reports_660_to_reports_650 import (
    MigrateReports660To650,
)
from protocols.protocol_7_13 import reports as reports_6_5_0
from protocols.protocol_8_0 import reports as reports_6_6_0
from tests.test_migration.base_test_migration import TestCaseMigration


class TestMigrateReport660To650(TestCaseMigration):
    old_model = reports_6_6_0
    new_model = reports_6_5_0

    def test_migrate_interpreted_genome(self):
        valid_genome_6_6_0 = self.get_valid_object(
            object_type=self.old_model.InterpretedGenome,
            version=self.version_8_0,
            fill_nullables=False,
        )
        self.assertTrue(
            self.old_model.InterpretedGenome.validate(valid_genome_6_6_0.toJsonDict())
        )
        # Migrate
        valid_genome_6_5_0 = MigrateReports660To650().migrate_interpreted_genome(
            old_instance=valid_genome_6_6_0
        )
        self.assertTrue(
            self.new_model.InterpretedGenome.validate(valid_genome_6_5_0.toJsonDict())
        )

    def test_migrate_interpreted_genome_filled_nullables(self):
        valid_genome_6_6_0 = self.get_valid_object(
            object_type=self.old_model.InterpretedGenome,
            version=self.version_8_0,
            fill_nullables=True,
        )
        self.assertTrue(
            self.old_model.InterpretedGenome.validate(valid_genome_6_6_0.toJsonDict())
        )
        # Migrate
        valid_genome_6_5_0 = MigrateReports660To650().migrate_interpreted_genome(
            old_instance=valid_genome_6_6_0
        )
        self.assertTrue(
            self.new_model.InterpretedGenome.validate(valid_genome_6_5_0.toJsonDict())
        )

    def test_migrate_report_event(self):
        valid_report_event_6_6_0 = self.get_valid_object(
            object_type=self.old_model.ReportEvent,
            version=self.version_8_0,
            fill_nullables=False,
        )

        self.assertTrue(
            valid_report_event_6_6_0.reportEventFlags is None,
            valid_report_event_6_6_0.reportEventFlags,
        )
        self.assertTrue(
            self.old_model.ReportEvent.validate(valid_report_event_6_6_0.toJsonDict())
        )
        # Migrate
        valid_report_event_6_5_0 = MigrateReports660To650().migrate_report_events(
            old_instance=valid_report_event_6_6_0
        )
        self.assertTrue(
            self.new_model.ReportEvent.validate(valid_report_event_6_5_0.toJsonDict())
        )

    def test_migrate_report_event_filled_nullables(self):
        valid_report_event_6_6_0 = self.get_valid_object(
            object_type=self.old_model.ReportEvent,
            version=self.version_8_0,
            fill_nullables=True,
        )

        self.assertTrue(
            self.old_model.ReportEvent.validate(valid_report_event_6_6_0.toJsonDict())
        )
        # Migrate
        valid_report_event_6_5_0 = MigrateReports660To650().migrate_report_events(
            old_instance=valid_report_event_6_6_0
        )
        self.assertTrue(
            self.new_model.ReportEvent.validate(valid_report_event_6_5_0.toJsonDict())
        )

    def test_migrate_report_event_with_flags(self):
        valid_report_event_6_6_0 = self.get_valid_object(
            object_type=self.old_model.ReportEvent,
            version=self.version_8_0,
            fill_nullables=True,
        )
        for flag in [
            _ for _ in dir(reports_6_6_0.CancerReportEventFlag) if not _.startswith("_")
        ]:
            valid_report_event_6_6_0.reportEventFlags = [flag]
            self.assertTrue(
                self.old_model.ReportEvent.validate(
                    valid_report_event_6_6_0.toJsonDict()
                )
            )
            # Migrate
            valid_report_event_6_5_0 = MigrateReports660To650().migrate_report_events(
                old_instance=valid_report_event_6_6_0
            )

            self.assertTrue(
                self.new_model.ReportEvent.validate(
                    valid_report_event_6_5_0.toJsonDict(validate=True)
                )
            )

    def test_migrate_structural_variant_copies_coordinate_to_old_location(self):
        valid_variant_new_model = self.get_valid_object(
            object_type=reports_6_6_0.StructuralVariant,
            version=self.version_8_0,
            fill_nullables=True,
        )
        valid_variant_new_model.coordinates = None
        self.assertTrue(
            reports_6_6_0.StructuralVariant.validate(
                valid_variant_new_model.toJsonDict()
            )
        )

        # Migrate
        valid_genome_old_model = MigrateReports660To650()._migrate_structural_variant(
            valid_variant_new_model
        )

        self.assertTrue(
            reports_6_5_0.StructuralVariant.validate(
                valid_genome_old_model.toJsonDict()
            )
        )
        self.assertIsNotNone(valid_genome_old_model.coordinates)
        self.assertEqual(
            valid_genome_old_model.coordinates.toJsonDict(),
            valid_variant_new_model.location.coordinates.toJsonDict(),
        )

    def test_migrate_structural_variant_does_not_overwrite_if_coordinate_exists_in_old_location(
        self,
    ):
        valid_variant_new_model = self.get_valid_object(
            object_type=reports_6_6_0.StructuralVariant,
            version=self.version_8_0,
            fill_nullables=True,
        )
        self.assertTrue(
            reports_6_6_0.StructuralVariant.validate(
                valid_variant_new_model.toJsonDict()
            )
        )

        # Migrate
        valid_genome_old_model = MigrateReports660To650()._migrate_structural_variant(
            valid_variant_new_model
        )

        self.assertTrue(
            reports_6_5_0.StructuralVariant.validate(
                valid_genome_old_model.toJsonDict()
            )
        )
        self.assertIsNotNone(valid_genome_old_model.coordinates)
        self.assertEqual(
            valid_genome_old_model.coordinates.toJsonDict(),
            valid_variant_new_model.coordinates.toJsonDict(),
        )

    def test_migrate_structural_variant_is_discarded_if_it_cannot_be_represented(self):
        valid_variant_new_model = self.get_valid_object(
            object_type=reports_6_6_0.StructuralVariant,
            version=self.version_8_0,
            fill_nullables=True,
        )
        valid_variant_new_model.coordinates = None
        valid_variant_new_model.location.coordinates = None
        self.assertTrue(
            reports_6_6_0.StructuralVariant.validate(
                valid_variant_new_model.toJsonDict()
            )
        )

        # Migrate
        valid_genome_old_model = MigrateReports660To650()._migrate_structural_variant(
            valid_variant_new_model
        )

        self.assertIsNone(valid_genome_old_model)

    def test_has_exact_coordinate(self):
        coordinates = self.get_valid_object(
            object_type=reports_6_6_0.Coordinates, version=self.version_8_0
        )
        location = self.get_valid_object(
            object_type=reports_6_6_0.Location, version=self.version_8_0
        )
        self._check_has_exact_coordinate_with_combination(coordinates, None, None, True)
        self._check_has_exact_coordinate_with_combination(
            None, location, coordinates, True
        )
        self._check_has_exact_coordinate_with_combination(
            coordinates, location, coordinates, True
        )
        self._check_has_exact_coordinate_with_combination(None, None, None, False)
        self._check_has_exact_coordinate_with_combination(None, location, None, False)

    def _check_has_exact_coordinate_with_combination(
        self, coordinate1, location, coordinate2, expected_result
    ):
        migration = MigrateReports660To650()
        valid_variant_new_model = self.get_valid_object(
            object_type=reports_6_6_0.StructuralVariant,
            version=self.version_8_0,
        )
        valid_variant_new_model.coordinates = coordinate1
        valid_variant_new_model.location = location
        if location:
            location.coordinates = coordinate2

        self.assertEqual(
            migration._has_exact_coordinates(valid_variant_new_model), expected_result
        )
