from protocols.migration.migration_reports_670_to_reports_660 import (
    MigrateReports670To660,
)
from protocols.protocol_8_0 import reports as reports_6_6_0
from protocols.protocol_8_1 import reports as reports_6_7_0
from tests.test_migration.base_test_migration import TestCaseMigration


class TestMigrateReports670To660(TestCaseMigration):
    old_reports = reports_6_7_0
    new_reports = reports_6_6_0

    def test_given_empty_nullable_fields_returns_a_valid_version_6_6_0_InterpretedGenome_object(
        self,
    ):
        valid_interpreted_genome_6_7_0 = self.get_valid_object(
            object_type=self.old_reports.InterpretedGenome,
            version=self.version_8_1,
            fill_nullables=False,
        )

        self.assertTrue(
            self.old_reports.InterpretedGenome.validate(
                valid_interpreted_genome_6_7_0.toJsonDict()
            )
        )
        # Migrate
        valid_interpreted_genome_6_6_0 = (
            MigrateReports670To660().migrate_interpreted_genome(
                old_instance=valid_interpreted_genome_6_7_0
            )
        )
        self.assertTrue(
            self.new_reports.InterpretedGenome.validate(
                valid_interpreted_genome_6_6_0.toJsonDict()
            )
        )

    def test_given_populated_nullable_fields_returns_a_valid_version_6_6_0_InterpretedGenome_object(
        self,
    ):
        valid_interpreted_genome_6_7_0 = self.get_valid_object(
            object_type=self.old_reports.InterpretedGenome,
            version=self.version_8_1,
            fill_nullables=True,
        )

        self.assertTrue(
            self.old_reports.InterpretedGenome.validate(
                valid_interpreted_genome_6_7_0.toJsonDict()
            )
        )
        # Migrate
        valid_interpreted_genome_6_6_0 = (
            MigrateReports670To660().migrate_interpreted_genome(
                old_instance=valid_interpreted_genome_6_7_0
            )
        )

        self.assertTrue(
            self.new_reports.InterpretedGenome.validate(
                valid_interpreted_genome_6_6_0.toJsonDict()
            )
        )
