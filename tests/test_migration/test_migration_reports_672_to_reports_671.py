from protocols.migration.migration_reports_672_to_reports_671 import (
    MigrateReports672To671,
)
from protocols.protocol_8_2 import reports as reports_6_7_1
from protocols.protocol_8_3 import reports as reports_6_7_2
from tests.test_migration.base_test_migration import TestCaseMigration


class TestMigrateReports672To671(TestCaseMigration):
    old_reports = reports_6_7_2
    new_reports = reports_6_7_1

    def test_given_empty_nullable_fields_returns_a_valid_version_6_7_1_InterpretedGenome_object(
        self,
    ):
        valid_interpreted_genome_6_7_2 = self.get_valid_object(
            object_type=self.old_reports.InterpretedGenome,
            version=self.version_8_3,
            fill_nullables=False,
        )

        self.assertTrue(
            self.old_reports.InterpretedGenome.validate(
                valid_interpreted_genome_6_7_2.toJsonDict()
            )
        )
        # Migrate
        valid_interpreted_genome_6_7_1 = (
            MigrateReports672To671().migrate_interpreted_genome(
                old_instance=valid_interpreted_genome_6_7_2
            )
        )
        self.assertTrue(
            self.new_reports.InterpretedGenome.validate(
                valid_interpreted_genome_6_7_1.toJsonDict()
            )
        )

    def test_given_populated_nullable_fields_returns_a_valid_version_6_7_1_InterpretedGenome_object(
        self,
    ):
        valid_interpreted_genome_6_7_2 = self.get_valid_object(
            object_type=self.old_reports.InterpretedGenome,
            version=self.version_8_3,
            fill_nullables=True,
        )

        self.assertTrue(
            self.old_reports.InterpretedGenome.validate(
                valid_interpreted_genome_6_7_2.toJsonDict()
            )
        )
        # Migrate
        valid_interpreted_genome_6_7_1 = (
            MigrateReports672To671().migrate_interpreted_genome(
                old_instance=valid_interpreted_genome_6_7_2
            )
        )

        self.assertTrue(
            self.new_reports.InterpretedGenome.validate(
                valid_interpreted_genome_6_7_1.toJsonDict()
            )
        )

    def test_structural_variants_of_unknown_type_are_removed_on_converting_to_6_7_1(
        self,
    ):
        #  Unknown STR type was not supported in 6.7.1 and before - as it is a required field these variant are removed
        #  when migrating.
        valid_interpreted_genome_6_7_2 = self.get_valid_object(
            object_type=self.old_reports.InterpretedGenome,
            version=self.version_8_3,
            fill_nullables=True,
        )
        valid_interpreted_genome_6_7_2.structuralVariants[
            0
        ].variantType = self.old_reports.StructuralVariantType.unknown
        valid_interpreted_genome_6_7_2.structuralVariants[
            1
        ].variantType = self.old_reports.StructuralVariantType.amplification
        # Migrate
        valid_interpreted_genome_6_7_1 = (
            MigrateReports672To671().migrate_interpreted_genome(
                old_instance=valid_interpreted_genome_6_7_2
            )
        )
        reported_str_types = [
            s.variantType for s in valid_interpreted_genome_6_7_1.structuralVariants
        ]
        assert reported_str_types == ["amplification"]

    def test_given_all_structural_variants_are_of_unknown_type_an_empty_list_is_returned_on_the_migrated_object(
        self,
    ):
        #  Unknown STR type was not supported in 6.7.1 and before - as it is a required field these variant are removed
        #  when migrating. If all STRs are unknown, a default empty list is returned.
        valid_interpreted_genome_6_7_2 = self.get_valid_object(
            object_type=self.old_reports.InterpretedGenome,
            version=self.version_8_3,
            fill_nullables=True,
        )
        valid_interpreted_genome_6_7_2.structuralVariants[
            0
        ].variantType = self.old_reports.StructuralVariantType.unknown
        valid_interpreted_genome_6_7_2.structuralVariants[
            1
        ].variantType = self.old_reports.StructuralVariantType.unknown
        # Migrate
        valid_interpreted_genome_6_7_1 = (
            MigrateReports672To671().migrate_interpreted_genome(
                old_instance=valid_interpreted_genome_6_7_2
            )
        )

        assert valid_interpreted_genome_6_7_1.structuralVariants == []
