from protocols.migration.migration_reports_690_to_602 import MigrateReports690To602
from protocols.protocol_7_2_1 import reports as reports_6_0_2
from protocols.protocol_8_6 import reports as reports_6_9_0
from tests.test_migration.base_test_migration import TestCaseMigration


class TestMigrateReport690To602(TestCaseMigration):
    old_reports = reports_6_9_0
    new_reports = reports_6_0_2

    def test_migrate_interpreted_genome(self):
        ig_690 = self.get_valid_object(
            object_type=self.old_reports.InterpretedGenome,
            version=self.version_8_6,
            fill_nullables=True,
        )

        ig_602 = MigrateReports690To602().migrate_interpreted_genome(
            old_instance=ig_690
        )

        self.assertIsInstance(ig_602, self.new_reports.InterpretedGenome)
        self.assertTrue(
            self.new_reports.InterpretedGenome.validate(ig_602.toJsonDict())
        )

    def test_migrate_empty_fields_interpreted_genome(self):
        ig_690 = self.get_valid_object(
            object_type=self.old_reports.InterpretedGenome,
            version=self.version_8_6,
            fill_nullables=False,
        )

        ig_602 = MigrateReports690To602().migrate_interpreted_genome(
            old_instance=ig_690
        )

        self.assertIsInstance(ig_602, self.new_reports.InterpretedGenome)
        self.assertTrue(
            self.new_reports.InterpretedGenome.validate(ig_602.toJsonDict())
        )
