from protocols.migration.migration_reports_690_to_672 import MigrateReports690To672
from protocols.protocol_8_3 import reports as reports_6_7_2
from protocols.protocol_8_6 import reports as reports_6_9_0
from tests.test_migration.base_test_migration import TestCaseMigration


class TestMigrateReport690To672(TestCaseMigration):
    old_reports = reports_6_9_0
    new_reports = reports_6_7_2

    def test_migrate_interpreted_genome(self):
        ig_690 = self.get_valid_object(
            object_type=self.old_reports.InterpretedGenome,
            version=self.version_8_6,
            fill_nullables=True,
        )

        ig_672 = MigrateReports690To672().migrate_interpreted_genome(
            old_instance=ig_690
        )

        self.assertIsInstance(ig_672, self.new_reports.InterpretedGenome)
        self.assertTrue(
            self.new_reports.InterpretedGenome.validate(ig_672.toJsonDict())
        )

    def test_migrate_empty_fields_interpreted_genome(self):
        ig_690 = self.get_valid_object(
            object_type=self.old_reports.InterpretedGenome,
            version=self.version_8_6,
            fill_nullables=False,
        )

        ig_672 = MigrateReports690To672().migrate_interpreted_genome(
            old_instance=ig_690
        )

        self.assertIsInstance(ig_672, self.new_reports.InterpretedGenome)
        self.assertTrue(
            self.new_reports.InterpretedGenome.validate(ig_672.toJsonDict())
        )
