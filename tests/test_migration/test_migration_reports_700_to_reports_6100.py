from protocols.migration.migration_reports_700_to_reports_6100 import (
    MigrateReports700To6100,
)
from protocols.protocol_8_9 import reports as reports_6_10_0
from protocols.protocol_9_0 import reports as reports_7_0_0
from tests.test_migration.base_test_migration import TestCaseMigration


class TestMigrateReports700To6100(TestCaseMigration):
    old_reports = reports_7_0_0
    new_reports = reports_6_10_0

    def test_given_empty_nullable_fields_returns_a_valid_version_6_10_rd_exit_questionnaire_object(
        self,
    ):
        valid_eq_7_0_0 = self.get_valid_object(
            object_type=self.old_reports.RareDiseaseExitQuestionnaire,
            version=self.version_9_0,
            fill_nullables=False,
        )

        self.assertTrue(
            self.old_reports.RareDiseaseExitQuestionnaire.validate(
                valid_eq_7_0_0.toJsonDict()
            )
        )
        # Migrate
        valid_eq_6_10_0 = (
            MigrateReports700To6100().migrate_rd_exit_questionnaire(
                old_instance=valid_eq_7_0_0
            )
        )

        self.assertTrue(
            self.new_reports.RareDiseaseExitQuestionnaire.validate(
                valid_eq_6_10_0.toJsonDict()
            )
        )

    def test_given_populated_nullable_fields_returns_a_valid_version_6_10_rd_exit_questionnaire_object(
        self,
    ):
        valid_eq_7_0_0 = self.get_valid_object(
            object_type=self.old_reports.RareDiseaseExitQuestionnaire,
            version=self.version_9_0,
            fill_nullables=True,
        )

        self.assertTrue(
            self.old_reports.RareDiseaseExitQuestionnaire.validate(
                valid_eq_7_0_0.toJsonDict()
            )
        )
        # Migrate
        valid_eq_6_10_0 = (
            MigrateReports700To6100().migrate_rd_exit_questionnaire(
                old_instance=valid_eq_7_0_0
            )
        )
        self.assertTrue(
            self.new_reports.RareDiseaseExitQuestionnaire.validate(
                valid_eq_6_10_0.toJsonDict()
            )
        )

    def test_given_populated_sub_models_with_empty_nullable_fields_returns_a_valid_version_6_10_rd_exit_questionnaire_object(
        self,
    ):
        valid_eq_7_0_0 = self.get_valid_object(
            object_type=self.old_reports.RareDiseaseExitQuestionnaire,
            version=self.version_9_0,
            fill_nullables=False,
        )
        valid_vglq_7_0_0 = self.get_valid_object(
            object_type=self.old_reports.VariantGroupLevelQuestions,
            version=self.version_9_0,
            fill_nullables=False,
        )
        valid_vlq_7_0_0 = self.get_valid_object(
            object_type=self.old_reports.VariantLevelQuestions,
            version=self.version_9_0,
            fill_nullables=False,
        )
        valid_strlq_7_0_0 = self.get_valid_object(
            object_type=self.old_reports.ShortTandemRepeatLevelQuestions,
            version=self.version_9_0,
            fill_nullables=False,
        )
        valid_svlq_7_0_0 = self.get_valid_object(
            object_type=self.old_reports.StructuralVariantLevelQuestions,
            version=self.version_9_0,
            fill_nullables=False,
        )

        valid_vglq_7_0_0.variantLevelQuestions = [valid_vlq_7_0_0]
        valid_vglq_7_0_0.shortTandemRepeatLevelQuestions = [valid_strlq_7_0_0]
        valid_vglq_7_0_0.structuralVariantLevelQuestions = [valid_svlq_7_0_0]

        valid_eq_7_0_0.variantGroupLevelQuestions = [valid_vglq_7_0_0]


        self.assertTrue(
            self.old_reports.RareDiseaseExitQuestionnaire.validate(
                valid_eq_7_0_0.toJsonDict()
            )
        )
        # Migrate
        valid_eq_6_10_0 = (
            MigrateReports700To6100().migrate_rd_exit_questionnaire(
                old_instance=valid_eq_7_0_0
            )
        )
        self.assertTrue(
            self.new_reports.RareDiseaseExitQuestionnaire.validate(
                valid_eq_6_10_0.toJsonDict()
            )
        )
