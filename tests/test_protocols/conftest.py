from protocols.protocol import ProtocolElement


class SimpleNamespace:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)


class GRMModelNestMock(ProtocolElement):
    # mock of how models inherit ProtocolElement in GRM code
    __slots__ = ["fake"]
    schema = SimpleNamespace(
        **{
            "type": "record",
            "name": "GRMModelMock",
            "namespace": "fake.namespace",
            "doc": "",
            "fields": [
                SimpleNamespace(
                    **{
                        "name": "fake",
                        "type": SimpleNamespace(**{"type": "string"}),
                        "has_default": False,
                    }
                ),
            ],
        }
    )

    def __init__(self, fake_positional, validate=False, **kwargs):
        ProtocolElement.__init__(
            self, **{k: v for k, v in locals().items() if k not in ["self", "kwargs"]}
        )


class GRMModelMock(ProtocolElement):
    # mock of how models inherit ProtocolElement in GRM code
    __slots__ = ["fake_positional", "fake_kwarg", "fake_nest"]
    schema = SimpleNamespace(
        **{
            "type": "record",
            "name": "GRMModelMock",
            "namespace": "fake.namespace",
            "doc": "",
            "fields": [
                SimpleNamespace(
                    **{
                        "name": "fake_positional",
                        "type": SimpleNamespace(**{"type": "string"}),
                        "has_default": False,
                    }
                ),
                SimpleNamespace(
                    **{
                        "name": "fake_kwarg",
                        "type": SimpleNamespace(**{"type": "string"}),
                        "has_default": True,
                        "default": "string",
                    }
                ),
                SimpleNamespace(
                    **{
                        "name": "fake_nest",
                        "type": SimpleNamespace(
                            **{
                                "type": "union",
                                "schemas": [
                                    SimpleNamespace(**{"type": "null"}),
                                    GRMModelNestMock.schema,
                                ],
                            }
                        ),
                        "has_default": True,
                        "default": None,
                    }
                ),
            ],
        }
    )

    @classmethod
    def getEmbeddedByNamespace(cls):
        """Gets the embeddedType by Namespace

        :return: Dictionary of embedded types.
        :rtype: dict
        """
        return {
            "fake.namespace.GRMModelMock": GRMModelMock,
            "fake.namespace.GRMModelNestMock": GRMModelNestMock,
        }

    def __init__(
        self, fake_positional, fake_kwarg=None, fake_nest=None, validate=False
    ):
        ProtocolElement.__init__(
            self, **{k: v for k, v in locals().items() if k not in ["self", "kwargs"]}
        )


class GRMChildMock(GRMModelMock):
    def __init__(self, extra_arg=None, **kwargs):
        GRMModelMock.__init__(self, **kwargs)

        self.extra_arg = extra_arg
