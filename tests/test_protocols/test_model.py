import glob
import importlib
import json
import os

import pytest

import build
import protocols
from protocols.util.dependency_manager import DependencyManager

PROTOCOL_PATH = os.path.dirname(protocols.__file__)


def load_protocol_names():
    with open(build.BUILDS_FILE) as f:
        # running most recent version only as py2.7 can't handle all the tests
        return [DependencyManager.get_python_protocol_name(json.load(f)["builds"][0])]

        # return [
        #     DependencyManager.get_python_protocol_name(b)
        #     for b in json.load(f)["builds"][0]
        # ]


def process_path_to_import(path):
    """Takes a path relative to the PROCOTOL_PATH and converts it to python import

    :param path: Path to file
    :type path: str
    :return: Import Module
    :rtype: str
    """
    return "protocols" + ".".join(
        path.replace(PROTOCOL_PATH, "").split(os.path.sep)
    ).replace(".py", "")


module_paths = [process_path_to_import(p) for p in glob.glob(PROTOCOL_PATH + "/*.py")]

expected_folders = load_protocol_names()
for build_folder in glob.glob(PROTOCOL_PATH + "/protocol_*"):
    if os.path.basename(build_folder) not in expected_folders:
        continue
    module_paths.extend(
        process_path_to_import(os.path.join(current, file))
        for current, _, files in os.walk(build_folder)
        for file in files
        if file.endswith(".py") and not current.endswith("__pycache__")
    )
module_paths = sorted(module_paths)


@pytest.mark.parametrize(
    "module_path",
    module_paths,
)
def test_modules_import_without_error(module_path):
    importlib.import_module(module_path)


@pytest.mark.parametrize(
    "module_path",
    module_paths,
)
def test_embedded_import_without_error(module_path):
    module = importlib.import_module(module_path)
    for name, ob in vars(module).items():
        if hasattr(ob, "getEmbeddedTypes"):
            ob.getEmbeddedTypes()
        if hasattr(ob, "getEmbeddedEnums"):
            ob.getEmbeddedEnums()
