from unittest import TestCase

from protocols.protocol_3_0_0 import reports as reports_3_0_0
from protocols.protocol_7_0 import reports as reports_6_0_0
from protocols.util.dependency_manager import VERSION_300
from protocols.util.factories.avro_factory import GenericFactoryAvro

from .conftest import GRMModelMock, GRMChildMock, SimpleNamespace


class TestValidate(TestCase):
    def test_validate_debug(self):
        object_type = reports_3_0_0.ReportedSomaticStructuralVariants
        variant = GenericFactoryAvro.get_factory_avro(
            clazz=object_type, version=VERSION_300
        )()
        variant.reportedStructuralVariantCancer.end = None

        # Check variant is not a valid ReportedSomaticStructuralVariants object
        self.assertFalse(variant.validate(jsonDict=variant.toJsonDict()))

        # Check the result of validation_result is False
        validation_result = variant.validate(
            jsonDict=variant.toJsonDict(), verbose=True
        )
        self.assertFalse(validation_result.result)

        # Check the validation returns the expected messages
        expected_message_2 = 'Class: [ReportedStructuralVariantCancer] expects field: [end] with schema type: ["int"] but received value: [None]'
        self.assertEqual(validation_result.messages[2], expected_message_2)
        expected_message_1 = "-2147483648 <= None <= 2147483647"
        self.assertEqual(validation_result.messages[1], expected_message_1)
        expected_message_0 = (
            'Schema: ["int"] has type: [int] but received datum: [None]'
        )
        self.assertEqual(validation_result.messages[0], expected_message_0)

    def test_case_insensitive_migrations(self):
        report_event = reports_6_0_0.ReportEvent.migrateFromJsonDict(
            {"DeNovoQualityScore": 5.0}
        )
        self.assertEqual(5.0, report_event.deNovoQualityScore)


class TestFromJsonDict:
    def test_that_fromJsonDict_handles_positional_arguments_in_dict(self):
        instance = GRMModelMock.fromJsonDict(
            {"fake_positional": "positional", "fake_kwarg": "kwarg"}
        )
        assert instance.fake_positional == "positional"
        assert instance.fake_kwarg == "kwarg"

    def test_that_fromJsonDict_does_not_raise_error_if_missing_positional_args(self):
        instance = GRMModelMock.fromJsonDict({"fake_kwarg": "kwarg"})
        assert instance.fake_positional is None
        assert instance.fake_kwarg == "kwarg"

    def test_that_kwargs_for_child_instances_are_passed_through(self, mocker):
        mocked_init_func = mocker.patch(
            "tests.test_protocols.conftest.GRMChildMock.__init__", return_value=None
        )

        instance = GRMChildMock.fromJsonDict({}, extra_arg=True)

        mocked_init_func.assert_called_with(
            fake_kwarg="string",
            fake_nest=None,
            fake_positional=None,
            validate=False,
            extra_arg=True,
        )


class TestEquality:
    def test_that_two_equal_instances_pass_equality(self):
        assert GRMModelMock(fake_positional="test") == GRMModelMock(
            fake_positional="test"
        )

    def test_that_mock_with_correct_fields_passes_equality(self, mocker):
        mocked_instance = mocker.Mock(
            schema=GRMModelMock.schema,
            fake_positional="test",
            fake_nest=None,
            fake_kwarg="test",
            spec_set=GRMModelMock,
        )
        assert (
            GRMModelMock(fake_positional="test", fake_kwarg="test") == mocked_instance
        )

    def test_that_mock_with_incorrect_fields_fails_equality(self, mocker):
        mocked_instance = mocker.Mock(
            schema=mocker.Mock(fields=[SimpleNamespace(name="extra_field")]),
            fake_positional="test",
            fake_kwarg="test",
            spec_set=GRMModelMock,
        )

        assert (
            GRMModelMock(fake_positional="test", fake_kwarg="test") != mocked_instance
        )
