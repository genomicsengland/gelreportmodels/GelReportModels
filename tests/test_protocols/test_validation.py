import pytest

from protocols.protocol import DefaultValidation, ValidationError

from .conftest import GRMModelMock, GRMModelNestMock

TEST_VALIDATION_PATH = "tests.test_protocols.test_validation"
IMPORT_PATH = "protocols.protocol"
PROTOCOL_CLASS = "{IMPORT_PATH}.ProtocolElement".format(IMPORT_PATH=IMPORT_PATH)


class TestValidation:
    @pytest.fixture
    def invalid_instance(self):
        return GRMModelMock(fake_positional=["wrong-type"], validate=False)

    @pytest.fixture(autouse=True)
    def mock_enable(self, mocker):
        # ensure consistency
        return mocker.patch(
            "{IMPORT_PATH}.DefaultValidation.is_enabled".format(
                IMPORT_PATH=IMPORT_PATH
            ),
            return_value=False,
        )

    def test_that_raise_if_invalid_on_valid_model_does_not_raise(self):
        GRMModelMock(
            fake_positional="valid-type", fake_kwarg="test", validate=False
        ).raise_if_invalid()

    def test_that_raise_if_invalid_raises_ValidationError_for_invalid_models(
        self, invalid_instance
    ):
        with pytest.raises(ValidationError):
            invalid_instance.raise_if_invalid()

    def test_that_raise_if_invalid_does_not_raise_for_valid_models(
        self,
    ):
        assert (
            GRMModelMock(
                fake_positional="value", fake_kwarg="another-value"
            ).raise_if_invalid()
            is None
        )

    def test_that_initialisation_of_invalid_models_raises_ValidationError_when_validate_is_True(
        self,
    ):
        with pytest.raises(ValidationError):
            GRMModelMock(fake_positional=["wrong-type"], validate=True)

    def test_that_initialisation_of_invalid_models_does_not_raise_ValidationError_when_validate_is_False(
        self,
    ):
        result = GRMModelMock(fake_positional=["wrong-type"], validate=False)

        assert isinstance(result, GRMModelMock)
        assert result.fake_positional == ["wrong-type"]

    def test_that_initialisation_from_invalid_json_raises_ValidationError_when_validate_is_True(
        self,
    ):
        with pytest.raises(ValidationError):
            GRMModelMock.fromJsonDict(
                dict(fake_positional=["wrong-type"]), validate=True
            )

    def test_that_initialisation_from_invalid_json_does_not_raise_ValidationError_when_validate_is_False(
        self,
    ):
        result = GRMModelMock.fromJsonDict(
            dict(fake_positional=["wrong-type"]), validate=False
        )

        assert isinstance(result, GRMModelMock)
        assert result.fake_positional == ["wrong-type"]

    def test_that_toJsonDict_on_invalid_model_raises_ValidationError_when_validate_is_True(
        self, invalid_instance
    ):
        with pytest.raises(ValidationError):
            invalid_instance.toJsonDict(validate=True)

    def test_that_toJsonDict_on_invalid_model_does_not_raise_ValidationError_when_validate_is_False(
        self, invalid_instance
    ):
        result = invalid_instance.toJsonDict(validate=False)

        assert result == {
            "fake_positional": ["wrong-type"],
            "fake_nest": None,
            "fake_kwarg": None,
        }

    def test_that_toJsonString_on_invalid_model_raises_ValidationError_when_validate_is_True(
        self, invalid_instance
    ):
        invalid_instance.fake_positional = ["wrong-type"]

        with pytest.raises(ValidationError):
            invalid_instance.toJsonString(validate=True)

    def test_that_toJsonString_on_invalid_model_does_not_raise_ValidationError_when_validate_is_False(
        self, invalid_instance
    ):
        result = invalid_instance.toJsonString(validate=False)

        assert (
            result
            == '{"fake_kwarg": null, "fake_nest": null, "fake_positional": ["wrong-type"]}'
        )

    def test_that_validation_occurs_if_validate_is_not_provided_and_DefaultValidation_is_enabled_is_True(
        self,
        invalid_instance,
        mocker,
    ):
        # given mocks
        mocker.patch(
            "{IMPORT_PATH}.DefaultValidation.is_enabled".format(
                IMPORT_PATH=IMPORT_PATH
            ),
            return_value=True,
        )

        with pytest.raises(ValidationError):
            invalid_instance.toJsonDict()

    def test_that_validation_does_not_occurs_if_validate_is_not_provided_and_DefaultValidation_is_enabled_is_False(
        self,
        invalid_instance,
        mocker,
    ):
        # given mocks
        mocker.patch(
            "{IMPORT_PATH}.DefaultValidation.is_enabled".format(
                IMPORT_PATH=IMPORT_PATH
            ),
            return_value=False,
        )

        invalid_instance.toJsonDict()

    def test_that_AttributeErrors_fail_validation(
        self,
        mocker,
        invalid_instance,
    ):
        mocker.patch(
            "{TEST_VALIDATION_PATH}.GRMModelMock.validate_debug".format(
                TEST_VALIDATION_PATH=TEST_VALIDATION_PATH
            ),
            side_effect=AttributeError(),
        )
        with pytest.raises(ValidationError):
            invalid_instance.raise_if_invalid()

    def test_that_failed_validation_raises_ValidationError(self, invalid_instance):
        with pytest.raises(ValidationError):
            invalid_instance.raise_if_invalid()

    def test_that_the_model_accepts_validate_kwarg(self):
        # assert init accepts validate
        instance = GRMModelMock(fake_positional="test", validate=False)

        # assert toJsonDict accepts validate
        asjson = instance.toJsonDict(validate=False)

        # assert fromJsonDict accepts validate
        from_json = GRMModelMock.fromJsonDict(asjson, validate=False)

        # assert toJsonString accepts validate
        asstring = from_json.toJsonString(validate=False)

        # assert fronJsonString accepts validate
        from_string = GRMModelMock.fromJsonString(asstring, validate=False)

    def test_that_mocks_without_spec_fail_validation(self, mocker):
        with pytest.raises(ValidationError):
            GRMModelMock(
                "test", fake_kwarg="test", fake_nest=mocker.Mock(fake="true")
            ).raise_if_invalid()

    def test_that_mocks_with_spec_fail_validation(self, mocker):
        with pytest.raises(ValidationError):
            GRMModelMock(
                "test",
                fake_kwarg="test",
                fake_nest=mocker.Mock(fake="true", spec_set=GRMModelNestMock),
            ).raise_if_invalid()


class TestBehaviour:
    def test_that_DefaultValidation_function_sets_to_behaviour(self):
        #
        assert not DefaultValidation.is_enabled()

        DefaultValidation.enable()

        assert DefaultValidation.is_enabled()

        DefaultValidation.disable()

        assert not DefaultValidation.is_enabled()
