from unittest import TestCase

from protocols.protocol_4_0_0 import reports as reports_4_0_0
from protocols.protocol_5_0_0 import reports as reports_4_2_0
from protocols.util.dependency_manager import DependencyManager
from importlib import import_module


class TestDependencyManager(TestCase):
    def test_dependency_manager(self):
        dependency_manager = DependencyManager()
        latest_dependencies = dependency_manager.get_latest_version_dependencies()
        assert isinstance(latest_dependencies, dict)
        latest_reports = latest_dependencies["org.gel.models.report.avro"]

        protocol_name = DependencyManager.get_python_protocol_name(
            {"version": dependency_manager.get_latest_version()}
        )
        latest_protocol = import_module("protocols." + protocol_name)

        assert latest_reports == latest_protocol.reports

    def test_loading_specific_version(self):
        dependency_manager = DependencyManager()
        dependencies_400 = dependency_manager.get_version_dependencies("4.0.0")
        assert isinstance(dependencies_400, dict)
        assert dependencies_400["org.gel.models.report.avro"] == reports_4_0_0
        assert dependencies_400["org.gel.models.report.avro"] != reports_4_2_0

    def test_generate_protocol_packageName(self):
        build = {"version": "7.13"}
        protocol_name = DependencyManager().get_python_protocol_name(build)

        self.assertEqual(protocol_name, "protocol_7_13")
