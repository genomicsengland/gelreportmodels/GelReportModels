from protocols.protocol_3_0_0 import reports as reports_3_0_0
from protocols.protocol_4_0_0 import ga4gh as ga4gh_3_0_0
from protocols.protocol_4_0_0 import reports as reports_4_0_0
from protocols.protocol_5_0_0 import reports as reports_4_2_0
from protocols.protocol_6_1 import cva as cva_1_0_0
from protocols.protocol_6_1 import reports as reports_5_0_0
from protocols.util.dependency_manager import (
    VERSION_61,
    VERSION_300,
    VERSION_400,
    VERSION_500,
)
from protocols.util.factories.avro_factory import FactoryAvro, GenericFactoryAvro
from protocols.util.factories.ga4gh_factories import CallFactory, GA4GHVariantFactory
from protocols.util.factories.reports_3_0_0_factories import (
    CancerReportedVariantsFactory,
)
from protocols.util.factories.reports_4_2_0_factories import (
    CancerExitQuestionnaireFactory,
)

import pytest


class TestGA4GHVariantFactory:
    variant_factory = GA4GHVariantFactory
    call_factory = CallFactory

    def test_create_random_variant(self):
        """

        Variant is created and is a valid one
        """
        variant = self.variant_factory()
        assert variant.validate(variant.toJsonDict())

    def test_overwrite_properties(self):
        """

        Variant is created with some defined properties, simple and complex
        :return:
        """
        variant = self.variant_factory(referenceBases="T", referenceName="X", start=12)
        assert variant.referenceBases == "T"
        assert variant.referenceName == "X"
        assert variant.start == 12
        variant.raise_if_invalid()

        calls = self.call_factory.create_batch(1, genotypeLikelihood=[1])
        variant = self.variant_factory(calls=calls)
        variant.raise_if_invalid()
        for call in variant.calls:
            assert call.genotypeLikelihood[0] == 1

    # def test_extending_factories(self):
    #     """
    #
    #     Factories can be extended to solve complex situation, in this test cases we are going to create a factory
    #     that is able to produce a list variants in a region (X: 1000-1020) each 2 bases - Note this is a very silly
    #     example which won't work in real life because relay on the counter to set the beginning of the sequence.
    #     """
    #
    #     class JumpingGA4GHVariantFactory(GA4GHVariantFactory):
    #         class Meta:
    #             model = Variant
    #
    #         referenceName = 'X'
    #         @factory.sequence
    #         def start(n):
    #             pos = n*2 + 1000
    #             if pos >= 1020:
    #                 return 1020
    #             else:
    #                 return pos
    #         end = start
    #
    #     # Reset the content, because the library is does not
    #     GA4GHVariantFactory._meta._counter = None
    #     for i, v in enumerate(JumpingGA4GHVariantFactory.create_batch(5)):
    #         self.assertEqual(v.start, i*2 + 1000)

    def test_cancer_exitquestionnaire_factory(self):
        batch = CancerExitQuestionnaireFactory.create_batch(1)
        batch[0].raise_if_invalid()


class ReportVersionControlFactory(FactoryAvro):
    def __init__(self, *args, **kwargs):
        super(ReportVersionControlFactory, self).__init__(*args, **kwargs)

    class Meta:
        model = reports_4_2_0.ReportVersionControl

    _version = VERSION_500
    gitVersionControl = "4.3.0-SNAPSHOT"


@pytest.mark.skipif(
    True,
    reason="Has issues. Needs to be moved to https://gitlab.com/genomicsengland/gelreportmodels/grm-test-factory",
)
class TestGenericFactory:
    @pytest.mark.parametrize(
        "clazz,version",
        [
            (reports_4_2_0.InterpretationRequestRD, VERSION_500),
            (reports_4_0_0.InterpretationRequestRD, VERSION_400),
            (reports_3_0_0.InterpretationRequestRD, VERSION_300),
            (cva_1_0_0.TieredVariantInjectRD, VERSION_61),
            (cva_1_0_0.TieredVariantInjectCancer, VERSION_61),
        ],
    )
    def test_factory_produces_valid_instances(self, clazz, version):
        # get an interpretation request RD for reports 4.2.0
        interpretation_request_factory = GenericFactoryAvro.get_factory_avro(
            reports_4_2_0.InterpretationRequestRD, version=VERSION_500
        )
        instance = interpretation_request_factory()
        instance.raise_if_invalid()
        # get an interpretation request RD for reports 4.0.0
        interpretation_request_factory = GenericFactoryAvro.get_factory_avro(
            reports_4_0_0.InterpretationRequestRD, version=VERSION_400
        )
        instance = interpretation_request_factory()
        instance.raise_if_invalid()
        # get an interpretation request RD for reports 3.1.0
        interpretation_request_factory = GenericFactoryAvro.get_factory_avro(
            clazz, version=version
        )
        instance = interpretation_request_factory()
        instance.raise_if_invalid()

    def test_batch(self):
        # builds a batch of 5 interpretation requests
        interpretation_request_factory = GenericFactoryAvro.get_factory_avro(
            reports_4_2_0.InterpretationRequestRD, version=VERSION_500
        )
        instances = interpretation_request_factory.create_batch(5)
        for instance in instances:
            instance.raise_if_invalid()

        assert len({instance.interpretationRequestId for instance in instances}) == 5

    def test_register_custom_factory(self):
        ## registering GA4GH variant factory
        GenericFactoryAvro.register_factory(
            ga4gh_3_0_0.Variant, GA4GHVariantFactory, version="4.0.0"
        )
        factory = GenericFactoryAvro.get_factory_avro(ga4gh_3_0_0.Variant, "4.0.0")
        instances = factory.create_batch(5)
        for instance in instances:
            instance.raise_if_invalid()
            assert instance.referenceBases in ["A", "C", "G", "T"]

        ## register CancerReportedVariantsFactory
        GenericFactoryAvro.register_factory(
            reports_3_0_0.ReportedVariantCancer,
            CancerReportedVariantsFactory,
            version="3.0.0",
        )
        factory = GenericFactoryAvro.get_factory_avro(
            reports_3_0_0.CancerInterpretationRequest, "3.0.0"
        )
        instances = factory.create_batch(5)
        for instance in instances:
            instance.raise_if_invalid()
            for tiered_variant in instance.TieredVariants:
                assert tiered_variant.reportedVariantCancer.reference in [
                    "A",
                    "C",
                    "G",
                    "T",
                ]

                assert tiered_variant.reportedVariantCancer.alternate in [
                    "A",
                    "C",
                    "G",
                    "T",
                ]

    def test_nullable_fields(self):
        # creates a factory for File not filling nullable fields and registers it in cache
        # as a factory that fill nullable fields
        # NOTE: this is the workaround to circumvent the loop in model definition
        file_factory = GenericFactoryAvro.get_factory_avro(
            reports_4_2_0.File, VERSION_500, False, False
        )
        GenericFactoryAvro.register_factory(
            reports_4_2_0.File, file_factory, VERSION_500, True
        )
        # get an interpretation request RD for reports 4.2.0
        interpretation_request_factory = GenericFactoryAvro.get_factory_avro(
            reports_4_2_0.InterpretationRequestRD,
            version=VERSION_500,
            fill_nullables=True,
        )
        instance = interpretation_request_factory()
        instance.raise_if_invalid()

    def test_custom_fields(self):
        # get an interpretation request RD for reports 4.2.0
        interpretation_request_factory = GenericFactoryAvro.get_factory_avro(
            reports_4_2_0.InterpretationRequestRD, version=VERSION_500
        )
        instance = interpretation_request_factory(analysisReturnUri="myURI")
        instance.raise_if_invalid()
        assert instance.analysisReturnUri == "myURI"

    def test_custom_fields2(self):
        # get an interpretation request RD for reports 4.2.0
        version_control_factory = GenericFactoryAvro.get_factory_avro(
            reports_4_2_0.ReportVersionControl, version=VERSION_500
        )
        instance_vc = version_control_factory(gitVersionControl="4.3.0-SNAPSHOT")
        instance_vc.raise_if_invalid()
        interpretation_request_factory = GenericFactoryAvro.get_factory_avro(
            reports_4_2_0.InterpretationRequestRD, version=VERSION_500
        )
        instance_ir = interpretation_request_factory(versionControl=instance_vc)
        instance_ir.raise_if_invalid()
        assert instance_ir.versionControl.gitVersionControl == "4.3.0-SNAPSHOT"

    def test_that_custom_factory_creates_valid_instances(self):
        # creates a custom factory and registers it

        GenericFactoryAvro.register_factory(
            reports_4_2_0.ReportVersionControl,
            ReportVersionControlFactory,
            version=VERSION_500,
        )

        interpretation_request_factory = GenericFactoryAvro.get_factory_avro(
            reports_4_2_0.InterpretationRequestRD, version=VERSION_500
        )
        instance_ir = interpretation_request_factory()
        instance_ir.raise_if_invalid()
        assert instance_ir.versionControl.gitVersionControl == "4.3.0-SNAPSHOT"

    def test_that_factories_with_nullable_fields_produce_valid_instances(self):
        # now creates another factory generating values for nullable fields
        file_factory = GenericFactoryAvro.get_factory_avro(
            reports_4_2_0.File, VERSION_500, False, False
        )
        GenericFactoryAvro.register_factory(
            reports_4_2_0.File, file_factory, VERSION_500, True
        )
        interpretation_request_factory = GenericFactoryAvro.get_factory_avro(
            reports_4_2_0.InterpretationRequestRD,
            version=VERSION_500,
            fill_nullables=True,
        )
        instance_ir2 = interpretation_request_factory()
        instance_ir2.raise_if_invalid()

    def test_that_factories_fill_nullables_with_specified_class(self):
        # now registers the factory for ReportVersionControl when filling nullables
        GenericFactoryAvro.register_factory(
            reports_4_2_0.ReportVersionControl,
            ReportVersionControlFactory,
            version=VERSION_500,
            fill_nullables=True,
        )
        interpretation_request_factory = GenericFactoryAvro.get_factory_avro(
            reports_4_2_0.InterpretationRequestRD,
            version=VERSION_500,
            fill_nullables=True,
        )
        instance = interpretation_request_factory()
        instance.raise_if_invalid()
        assert instance.versionControl.gitVersionControl == "4.3.0-SNAPSHOT"

    def test_custom_fields(self):
        # get an interpretation request RD for reports 4.2.0
        interpretation_request_factory = GenericFactoryAvro.get_factory_avro(
            reports_4_2_0.InterpretationRequestRD, version=VERSION_500
        )
        instance = interpretation_request_factory(analysisReturnUri="myURI")
        instance.raise_if_invalid()
        assert instance.analysisReturnUri == "myURI"

    def test_build_version_with_hotfix(self):
        # get an interpretation request RD for reports 5.0.0
        interpretation_request_factory = GenericFactoryAvro.get_factory_avro(
            reports_5_0_0.InterpretationRequestRD,
            version=VERSION_61,
        )
        instance = interpretation_request_factory()
        instance.raise_if_invalid()

        # now try the same with the build version including the hotfix version
        interpretation_request_factory = GenericFactoryAvro.get_factory_avro(
            reports_5_0_0.InterpretationRequestRD, version="6.1.0"
        )
        instance = interpretation_request_factory()
        instance.raise_if_invalid()
