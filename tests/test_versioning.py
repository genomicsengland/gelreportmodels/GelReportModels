import os
import re
import subprocess
import xml.etree.ElementTree as ET

import pytest


def run_command(command):
    results = subprocess.check_output(
        command,
        shell=True,
    ).decode("utf-8")
    for line in results.splitlines():
        yield line


VERSION_PARSER = re.compile(
    r"^(?P<major>\d+)\.(?P<minor>\d+)(\.(?P<patch>\d+)){0,1}", re.MULTILINE
)

FILE_CHANGES = list(run_command("git diff --name-status master"))

TAGS = list(run_command("git tag -l"))

ROOTDIR = next(run_command("git rev-parse --show-toplevel"), "")


def parse_version(version):
    match = next(VERSION_PARSER.finditer(version), None)
    result = match.groupdict() if match else {}
    for part in ["major", "minor", "patch"]:
        result[part] = result.get(part) or 0

    return result


def higher(version, against):
    for part in ["major", "minor", "patch"]:
        if int(version[part]) > int(against[part]):
            return True
    return False


def get_version():
    with open(ROOTDIR + "/VERSION") as handle:
        return handle.read()


def test_version_file_contains_full_semantic_version():
    version = get_version()
    parsed = parse_version(version)
    assert (
        "{}.{}.{}".format(parsed["major"], parsed["minor"], parsed["patch"]) == version
    )


@pytest.mark.skipif(os.environ.get("CI_COMMIT_TAG") is None, reason="Not in CI")
def test_that_if_version_is_higher_than_previously_tagged_version():
    tags = [parse_version(t.lstrip("v")) for t in TAGS]

    version = parse_version(get_version())

    assert version, "No valid semantic version found. VERSION = {}".format(version)
    higher_tags = [t for t in tags if not higher(version, t)]
    assert (
        higher_tags == []
    ), "{} specified in 'VERSION' file is not the highest version. Please check tags and change.".format(
        version
    )


def test_that_pom_version_matches_setup():
    version = get_version()

    with open(ROOTDIR + "/pom.xml") as handle:
        pom_contents = ET.fromstring(handle.read())
    ns = pom_contents.tag.split("}")[0] + "}"
    versions = [
        v
        for v in pom_contents.findall("./{ns}version".format(ns=ns))
        + pom_contents.findall("./{ns}properties/{ns}models.version".format(ns=ns))
    ]

    assert versions

    for child in versions:
        assert child.text == version, "{} does not match version file ({})".format(
            child.tag, version
        )


def get_version_idls():
    grm = os.path.dirname(os.path.dirname(__file__))
    schemas = os.path.join(grm, "schemas", "IDLS")
    out = set()
    for root, dirs, files in os.walk(schemas):
        relroot = root.replace(schemas, "").strip("/")
        if relroot.startswith("build") or "current" in root:
            continue
        for file in files:
            if "version" in file.lower() and file.endswith(".avdl"):
                out.add(
                    (os.path.join(relroot, file), os.path.join(root, file)),
                )
    return out


@pytest.mark.parametrize(
    "rel,file", [pytest.param(k, p, id=k) for k, p in get_version_idls()]
)
def test_that_version_in_idl_matches_parent_structure(rel, file):
    version = os.path.basename(os.path.dirname(rel))
    with open(file) as handle:
        assert ('"' + version + '"') in handle.read()
